#pragma once
#include "CUvDoc.h"
#include "CUvPage.h"

class CLoaderContext
{
public:
    CLoaderContext();
    ~CLoaderContext();

    CUvDoc* Load(const wchar_t *fileName);

public:
    wchar_t *pFileName;
    void *pLoaderEntry;
    CUvDoc *pUvDoc;
    int nCurrentPageNo;
    CUvPage *pUvPage;
};

