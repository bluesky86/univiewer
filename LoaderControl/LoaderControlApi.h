#pragma once
//#include "header.h"
//#include "LoaderControl.h"
#include "CUvPage.h"

#ifdef LOADERCONTROL_EXPORTS
#define LOADERCTRL_API __declspec(dllexport)
#else
#define LOADERCTRL_API __declspec(dllimport)
#endif

typedef void* HLoaderControl;
typedef void* HLoaderContext;

// Loader Control
LOADERCTRL_API HLoaderControl LoaderControl_Get();
LOADERCTRL_API HLoaderControl LoaderControl_Init(const wchar_t* dllDir);
LOADERCTRL_API void LoaderControl_Close();

// Loader Context
LOADERCTRL_API HLoaderContext LoaderControl_OpenContext(const wchar_t *wFileName);
LOADERCTRL_API void LoaderControl_CloseContext(HLoaderContext loaderContext);
LOADERCTRL_API CUvPage* LoaderControl_LoadPage(HLoaderContext loaderContext, S32 incr, S32 pageNo);
LOADERCTRL_API CUvPage* LoaderControl_GetCurrentPage(HLoaderContext loaderContext);

