#include "stdafx.h"
// LoaderControl.cpp : Defines the exported functions for the DLL application.
//

#include "header.h"
#include "LoaderControl.h"
#include "tinyxml2.h"
#include "UvmUtils.h"
#include "Error.h"
#include "DebugNew.h"
//#include "windows.h"
#include "CSurface.h"
#include "Codepage.h"

extern CLoaderControl *g_LoaderControl;

/////////////////////////////////////////////////
// CLoaderControl
/////////////////////////////////////////////////

// This is the constructor of a class that has been exported.
// see LoaderControl.h for the class definition
//#define _CRTDBG_MAP_ALLOC
//#include <cstdlib>
//#include <crtdbg.h>
CLoaderControl::CLoaderControl()
{
    //_crtBreakAlloc = 161;

    //loaderEntries = NEW CVector<CLoaderEntry*>;
    loaderEntries = NEW std::vector<CLoaderEntry*>;

    saveAsFilter = 0;
}

CLoaderControl::~CLoaderControl()
{
    UV_ClearError();

    if (loaderEntries)
    {
        size_t size = loaderEntries->size();
        for (size_t i = 0; i < size; i++)
        {
            // comment it out since all library will be free when instance is destroyed.
            // otherwise crash since has been freed already
            //if (loaderEntries[i]->hDllProc)
            //    FreeLibrary(loaderEntries[i]->hDllProc);
            delete (*loaderEntries)[(U32)i];
        }
        //loaderEntries->clear();

        delete loaderEntries;
    }

    if (saveAsFilter) delete[] saveAsFilter;

    //saveAsFilterColorType.clear();
}

//bool CLoaderControl::loadConfigFile(const wchar_t* dllDir, const wchar_t *configFile)
//{
//    char filePath[FILEPATH_MAX_LENGTH];
//    size_t len;
//    len = UV_Wchar2Char(filePath, FILEPATH_MAX_LENGTH, dllDir);
//    if (len == 0)
//    {
//        UV_SetError(UV_ERROR_CHAR_CONVERT_FAILED);
//        return false;
//    }
//    strcpy_s(libDirectory, FILEPATH_MAX_LENGTH, filePath);
//    len = UV_Wchar2Char(&filePath[len - 1], FILEPATH_MAX_LENGTH - len, configFile);
//    if (len == 0)
//    {
//        UV_SetError(UV_ERROR_CHAR_CONVERT_FAILED);
//        return false;
//    }
//
//    tinyxml2::XMLDocument xmlDoc;
//    tinyxml2::XMLError err = xmlDoc.LoadFile(filePath);
//    if (err != tinyxml2::XML_SUCCESS)
//    {
//        UV_SetError(UV_ERROR_LOADER_CONFIG_FAILED);
//        return false;
//    }
//
//    return true;
//}

bool CLoaderControl::loadFmtConfigFile(const wchar_t* dllDir, const wchar_t *configFile)
{
    char filePath[FILEPATH_MAX_LENGTH];
    size_t len;
    len = UV_Wchar2Char(filePath, FILEPATH_MAX_LENGTH, dllDir);
    if (len == 0)
    {
        UV_SetError(UV_ERROR_CHAR_CONVERT_FAILED);
        return false;
    }
    strcpy_s(libDirectory, FILEPATH_MAX_LENGTH, filePath);
    len = UV_Wchar2Char(&filePath[len - 1], FILEPATH_MAX_LENGTH - len, configFile);
    if (len == 0)
    {
        UV_SetError(UV_ERROR_CHAR_CONVERT_FAILED);
        return false;
    }

    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLError err = xmlDoc.LoadFile(filePath);
    if (err != tinyxml2::XML_SUCCESS)
    {
        UV_SetError(UV_ERROR_LOADER_CONFIG_FAILED);
        return false;
    }

    tinyxml2::XMLElement* xmlLoaderListElement = xmlDoc.FirstChildElement()->FirstChildElement("LoaderList");
    tinyxml2::XMLElement* xmlLoaderEntry = xmlLoaderListElement->FirstChildElement("LoaderEntry");
    while (xmlLoaderEntry)
    {
        const char *xmlFileName = xmlLoaderEntry->Attribute("filename");
        const char *xmlLoaderConfigFile = xmlLoaderEntry->Attribute("config");
        const char *xmlFmtIsImage = xmlLoaderEntry->Attribute("ImageFile");
        if (xmlFileName)
        {
            CLoaderEntry *loader = NEW CLoaderEntry();
            size_t len = strlen(xmlFileName) + 1;
            loader->dllName = NEW char[len];
            strcpy_s(loader->dllName, len, xmlFileName);
            len = strlen(xmlLoaderConfigFile) + 1;
            loader->configFile = NEW char[len];
            strcpy_s(loader->configFile, len, xmlLoaderConfigFile);
            if (xmlFmtIsImage && strcmp(xmlFmtIsImage, "yes") == 0) loader->bIsImage = true;
            else loader->bIsImage = false;

            tinyxml2::XMLElement* xmlExt = xmlLoaderEntry->FirstChildElement()->FirstChildElement("ExtEntry");
            if (xmlExt == NULL)
            {
                UV_SetError(UV_ERROR_LOADER_CONFIG_EXT_NOT_DEFINED, loader->dllName);
                delete loader;
                xmlLoaderEntry = xmlLoaderEntry->NextSiblingElement("LoaderEntry");
                continue;
            }

            const char *xmlExtDescription;
            while (xmlExt)
            {
                xmlFileName = xmlExt->Attribute("ext");
                _strlwr_s((char*)xmlFileName, strlen(xmlFileName) + 1);
                xmlExtDescription = xmlExt->Attribute("description");
                const char *xmlFmtConvert = xmlExt->Attribute("convertDoc");
                const char *xmlFmtWrite1Bit = xmlExt->Attribute("ToBilevle");
                const char *xmlFmtWrite8Bit = xmlExt->Attribute("To8bits");
                const char *xmlFmtWriteGrayScale = xmlExt->Attribute("ToGrayScale");
                const char *xmlFmtWriteRgb = xmlExt->Attribute("ToTrueColor");
                if (xmlFileName)
                {
                    CFmtEntry *fmt = NEW CFmtEntry();
                    len = strlen(xmlFileName) + 1;
                    fmt->ext = NEW char[len];
                    strcpy_s(fmt->ext, len, xmlFileName);
                    len = strlen(xmlExtDescription) + 1;
                    fmt->description = NEW char[len];
                    strcpy_s(fmt->description, len, xmlExtDescription);
                    if (xmlFmtConvert && strcmp(xmlFmtConvert, "yes") == 0) fmt->bWriteCompound = true;
                    else fmt->bWriteCompound = false;
                    if (xmlFmtWrite1Bit && strcmp(xmlFmtWrite1Bit, "yes") == 0) fmt->bWrite1Bit = true;
                    else fmt->bWrite1Bit = false;
                    if (xmlFmtWrite8Bit && strcmp(xmlFmtWrite8Bit, "yes") == 0) fmt->bWrite8Bit = true;
                    else fmt->bWrite8Bit = false;
                    if (xmlFmtWriteGrayScale && strcmp(xmlFmtWriteGrayScale, "yes") == 0) fmt->bWrite8BitGrayScale = true;
                    else fmt->bWrite8BitGrayScale = false;
                    if (xmlFmtWriteRgb && strcmp(xmlFmtWriteRgb, "yes") == 0) fmt->bWriteRgb = true;
                    else fmt->bWriteRgb = false;

                    loader->entries.push_back(fmt);
                }

                xmlExt = xmlExt->NextSiblingElement("ExtEntry");
            }

            // load configure xml of entry


            // push to back
            (*loaderEntries).push_back(loader);
        }

        xmlLoaderEntry = xmlLoaderEntry->NextSiblingElement("LoaderEntry");
    }

    // load DLLs
    loadDLL();

    // build save as filter
    BuildSaveAsFilter();

    return true;
}

int CLoaderControl::Recognize(const wchar_t *wFileName)
{
    for (unsigned int i = 0; i < (*loaderEntries).size(); i++)
    {
        if ((*loaderEntries)[i]->hDllProc)
        {
            if ((*loaderEntries)[i]->fRecognizer && (*loaderEntries)[i]->fRecognizer(wFileName))
            {
                if ((*loaderEntries)[i]->fOpenDoc)
                {
                    return i;
                }
            }
        }
    }

    return -1;
}

int CLoaderControl::Recognize(unsigned char *pData, int len)
{
    for (unsigned int i = 0; i < (*loaderEntries).size(); i++)
    {
        if ((*loaderEntries)[i]->hDllProc)
        {
            if ((*loaderEntries)[i]->fRecognizerBuffer && (*loaderEntries)[i]->fRecognizerBuffer(pData, len))
            {
                if ((*loaderEntries)[i]->fOpenDoc)
                {
                    return i;
                }
            }
        }
    }

    return -1;
}

CLoaderContext* CLoaderControl::NewFile()
{
    CLoaderContext *pUvContext = NEW CLoaderContext();
    pUvContext->nCurrentPageNo = 0;
    // TODO: ###############################################################
    //pUvContext->pUvPage = NEW CUvPage();
    //pUvContext->pUvPage->getLayer(0)->setBbox(RenderDoc::CRdRect(0, 800, 0, 600));
    //pUvContext->pUvPage->rect = RenderDoc::CRdRect(0, 800, 0, 600);

    return pUvContext;
}

CLoaderContext* CLoaderControl::OpenFile(const wchar_t *wFileName)
{
    CLoaderContext *pUvContext = NEW CLoaderContext();
    pUvContext->pFileName = NEW wchar_t[wcslen(wFileName) + 1];
    wcscpy_s(pUvContext->pFileName, wcslen(wFileName) + 1, wFileName);

    size_t pos = reverseCharPos(pUvContext->pFileName, L'.');
    wchar_t *ext = NULL;
    if (pos >= 0)
        ext = &pUvContext->pFileName[pos + 1];

    // Clear last error
    UV_SetError(UV_ERROR_NONE);

    // load file by file ext
    CLoaderEntry *pEntry = NULL;
    if (ext)
    {
        pEntry = FindLoader(ext);
        if (pEntry)
        {
            if (pEntry->fOpenDoc)
            {
                pUvContext->pUvDoc = pEntry->fOpenDoc(pUvContext->pFileName);
                if (pUvContext->pUvDoc && pUvContext->pUvDoc->nTotalPages > 0)
                {
                    pUvContext->pLoaderEntry = pEntry;

                    // doc fonts
                    MakeRenderFonts( pUvContext->pUvDoc->fontList, &pUvContext->pUvDoc->pRenderFonts );

                    return pUvContext;
                }
            }
            else
            {
                UV_SetError(UV_ERROR_API_OPENDOC_DIDNOT_LOADED, pEntry->dllName);
            }
        }
    }

    // either file name doesn't contain ext or cooresponding loader failed to load
    // try all loaders again
    int index = Recognize(wFileName);
    if (index != -1)
    {
        pUvContext->pUvDoc = pEntry->fOpenDoc(pUvContext->pFileName);
        if (pUvContext->pUvDoc && pUvContext->pUvDoc->nTotalPages > 0)
        {
            pUvContext->pLoaderEntry = (*loaderEntries)[index];
            
            // doc fonts
            MakeRenderFonts( pUvContext->pUvDoc->fontList, &pUvContext->pUvDoc->pRenderFonts );

            return pUvContext;
        }
    }

    // get error message from loader
    if (!pUvContext->pUvDoc || pUvContext->pUvDoc->nTotalPages <= 0)
    {
        if (pEntry && pEntry->fGetLastError)
            UV_SetError(UV_ERROR_LOADER_DEFINED, pEntry->fGetLastError());
        else if (UV_GetLastError() == UV_ERROR_NONE)
            UV_SetError(UV_ERROR_NO_LOADER_CAN_USED_FOR_THIS_FILE, wFileName);
    }

    delete pUvContext;
    return NULL;

}

// if pageNo is -1, incr will be used to increase current page number
CUvPage* CLoaderControl::LoadPage(CLoaderContext* pUvContext, int incr, int pageNo)
{
    // Clear last error
    UV_SetError(UV_ERROR_NONE);

    if (pUvContext == nullptr || pUvContext->pUvDoc == nullptr) return nullptr;

    CLoaderEntry *pLoaderEntry = (CLoaderEntry*)pUvContext->pLoaderEntry;
    if (pLoaderEntry == nullptr) return nullptr;

    int nPage = pUvContext->nCurrentPageNo;
    if (pageNo == -1)
        nPage += incr;
    else nPage = pageNo;
    if (nPage < 0) nPage = 0;
    if (nPage > pUvContext->pUvDoc->nTotalPages - 1) nPage = pUvContext->pUvDoc->nTotalPages - 1;
    if (pUvContext->nCurrentPageNo == nPage && pUvContext->nCurrentPageNo >= 0)
        return pUvContext->pUvPage;

    CUvPage *pPage = pLoaderEntry->fGetPage(pUvContext->pUvDoc, nPage);
    if (pPage)
    {
        // release the previous page
        // TO DO: consider cache pages
        if(pUvContext->pUvPage)
        {
            if(pUvContext->pUvPage->pRenderFonts)
                FreeRenderFonts( pUvContext->pUvPage->pRenderFonts );
            delete pUvContext->pUvPage;
        }

        pPage->parent = pUvContext->pUvDoc;

        pUvContext->nCurrentPageNo = nPage;
        pUvContext->pUvPage = pPage;
  
        // pages fonts
        MakeRenderFonts( pPage->fontList, &pPage->pRenderFonts );
        
        return pPage;
    }
    else
    {
        UV_SetError(UV_ERROR_LOADER_DEFINED, pLoaderEntry->fGetLastError());
        return nullptr;
    }
}

void CLoaderControl::CloseFile(CLoaderContext* pUvContext)
{
    if (pUvContext == NULL) return;

    CLoaderEntry *pLoaderEntry = (CLoaderEntry*)pUvContext->pLoaderEntry;
    if (pLoaderEntry == NULL)
    {
        delete pUvContext;
        return;
    }

    if (pUvContext->pUvPage)
    {
        //if (pUvContext->pUvPage->fontList) RemoveFont(pUvContext->pUvPage->fontList);

        //// release font of doc
        //if (pUvContext->renderDoc.fontList) RemoveFont(pUvContext->renderDoc.fontList);

        if(pUvContext->pUvPage->pRenderFonts)
            FreeRenderFonts( pUvContext->pUvPage->pRenderFonts );

        delete pUvContext->pUvPage;
        pUvContext->pUvPage = NULL;
    }
    if(pUvContext->pUvDoc && pUvContext->pUvDoc->pRenderFonts)
        FreeRenderFonts( pUvContext->pUvDoc->pRenderFonts );

    pLoaderEntry->fCloseDoc(pUvContext->pUvDoc);

    ///////////////////////////////
    if(pUvContext)
        delete pUvContext;
}

CLoaderEntry* CLoaderControl::FindLoader(wchar_t* ext, CFmtEntry **fmtEntry)
{
    size_t len = wcslen(ext);
    char *mbExt = NEW char[len + 32];
    wcstombs_s(&len, mbExt, len + 1, ext, len);
    _strlwr_s(mbExt, len + 1);
    for (unsigned int i = 0; i < (*loaderEntries).size(); i++)
    {
        for (unsigned int j = 0; j < (*loaderEntries)[i]->entries.size(); j++)
        {
            if (strcmp((*loaderEntries)[i]->entries[j]->ext, mbExt) == 0)
            {
                delete[] mbExt;
                if (fmtEntry) *fmtEntry = (*loaderEntries)[i]->entries[j];
                return (*loaderEntries)[i];
            }
        }
    }

    delete[] mbExt;
    return NULL;
}

unsigned char CLoaderControl::GetColorType(int filterIndex)
{
    if (filterIndex < 0 || filterIndex >(int)saveAsFilterColorType.size())
        filterIndex = 0;
    return saveAsFilterColorType[filterIndex];
}

bool CLoaderControl::loadDLL()
{
    char filePath[FILEPATH_MAX_LENGTH];
    size_t dirLen = strlen(libDirectory);
    bool res;
    for (unsigned int i = 0; i < (*loaderEntries).size(); i++)
    {
        res = true;
        strcpy_s(filePath, FILEPATH_MAX_LENGTH, libDirectory);
        strcpy_s(&filePath[dirLen], FILEPATH_MAX_LENGTH - dirLen, (*loaderEntries)[i]->dllName);
        (*loaderEntries)[i]->hDllProc = (Handle)LoadLibraryA(filePath);
        if ((*loaderEntries)[i]->hDllProc)
        {
            // should define a config file for each loader, which API is supported
            (*loaderEntries)[i]->fLoadXmlConfig = (loader_LoadXmlConfig)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "LoadXmlConfig");
            (*loaderEntries)[i]->fCloseLoader = (loader_CloseLoader)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "CloseLoader");
            (*loaderEntries)[i]->fRecognizer = (loader_Recognizer)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "Recognizer");
            (*loaderEntries)[i]->fRecognizerBuffer = (loader_RecognizerBuffer)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "RecognizerBuffer");
            (*loaderEntries)[i]->fOpenDoc = (loader_OpenDoc)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "OpenDoc");
            (*loaderEntries)[i]->fGetPage = (loader_GetPage)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "GetPage");
            (*loaderEntries)[i]->fCloseDoc = (loader_CloseDoc)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "CloseDoc");
            (*loaderEntries)[i]->fGetLastError = (loader_GetLastError)GetProcAddress((HMODULE)(*loaderEntries)[i]->hDllProc, "GetLoaderLastError");
            //(*loaderEntries)[i]->fLoadRdImage = (loader_LoadRdImage)GetProcAddress((HMODULE)loaderEntries[i]->hDllProc, "LoadRdImage");
            //(*loaderEntries)[i]->fWriteImage = (loader_WriteImage)GetProcAddress((HMODULE)loaderEntries[i]->hDllProc, "WriteImage");
            //(*loaderEntries)[i]->fConvertDoc = (loader_ConvertDoc)GetProcAddress((HMODULE)loaderEntries[i]->hDllProc, "ConvertDoc");


            // load loader's xml config file
            if ((*loaderEntries)[i]->fLoadXmlConfig)
            {
                strcpy_s(&filePath[dirLen], FILEPATH_MAX_LENGTH - dirLen, (*loaderEntries)[i]->configFile);
                (*loaderEntries)[i]->fLoadXmlConfig(filePath);
            }
        }
        else
            res = false;
        if (res == false)
        {
            UV_SetError(UV_ERROR_LOADER_API_LOAD_FAILED, (*loaderEntries)[i]->dllName);
            //return false;
        }
    }

    return true;
}

void CLoaderControl::BuildSaveAsFilter()
{
    if (saveAsFilter) delete[] saveAsFilter;
    int totalLen = 1024;
    saveAsFilter = NEW wchar_t[totalLen];
    saveAsFilter[0] = saveAsFilter[1] = 0;
    wchar_t ele[1024];
    int len = 0;
    for (unsigned int i = 0; i < (*loaderEntries).size(); i++)
    {
        for (unsigned int j = 0; j < (*loaderEntries)[i]->entries.size(); j++)
        {
            wcscpy_s(ele, 1024, L"");
            wchar_t *pDesc = UV_Char2Wchar((*loaderEntries)[i]->entries[j]->description);
            wchar_t *pExt = UV_Char2Wchar((*loaderEntries)[i]->entries[j]->ext);
            //if (loaderEntries[i]->fWriteImage)
            //{
            //    if (loaderEntries[i]->entries[j]->bWrite1Bit)
            //    {
            //        wsprintf(ele, L"%s%s bilevel (*.%s)|*.%s|", ele, pDesc, pExt, pExt);
            //        saveAsFilterColorType.push_back(FILE_COLORTYPE_BILEVLE);
            //    }
            //    if (loaderEntries[i]->entries[j]->bWrite8Bit)
            //    {
            //        wsprintf(ele, L"%s%s 8-bit (*.%s)|*.%s|", ele, pDesc, pExt, pExt);
            //        saveAsFilterColorType.push_back(FILE_COLORTYPE_256);
            //    }
            //    if (loaderEntries[i]->entries[j]->bWrite8BitGrayScale)
            //    {
            //        wsprintf(ele, L"%s%s 8-bit grayscale (*.%s)|*.%s|", ele, pDesc, pExt, pExt);
            //        saveAsFilterColorType.push_back(FILE_COLORTYPE_GRAYSCALE);
            //    }
            //    if (loaderEntries[i]->entries[j]->bWriteRgb)
            //    {
            //        wsprintf(ele, L"%s%s true color (*.%s)|*.%s|", ele, pDesc, pExt, pExt);
            //        saveAsFilterColorType.push_back(FILE_COLORTYPE_TRUECOLOR);
            //    }
            //}
            //if (loaderEntries[i]->fConvertDoc &&loaderEntries[i]->entries[j]->bWriteCompound)
            //{
            //    wsprintf(ele, L"%s%s (*.%s)|*.%s|", ele, pDesc, pExt, pExt);
            //    saveAsFilterColorType.push_back(FILE_COMPOUND_FILE);
            //}

            delete[] pDesc;
            delete[] pExt;
            //RD_DeleteString(pDesc);
            //RD_DeleteString(pExt);

            int eleLen = (int)wcslen(ele);
            if (len + eleLen > totalLen - 1)
            {
                wchar_t *pNewData = NEW wchar_t[len + eleLen + 1024];
                wsprintf(pNewData, L"%s%s", saveAsFilter, ele);
                delete[] saveAsFilter;
                saveAsFilter = pNewData;
            }
            else
                wsprintf(saveAsFilter, L"%s%s", saveAsFilter, ele);
            len += eleLen;
        }
    }
    wsprintf(saveAsFilter, L"%s%s", saveAsFilter, L"|");
}

//RenderDoc::CRdImage* CLoaderControl::DecodeImage(S32 loaderId, U8 *pData, S32 len)
//{
//    CLoaderEntry* pLoaderEntry = loaderEntries[loaderId];
//    if (pLoaderEntry->fLoadRdImage)
//    {
//        return pLoaderEntry->fLoadRdImage(pData, len);
//    }
//    return NULL;
//}


bool CLoaderControl::InitLoaderControl(const wchar_t* dllDir)
{
    //// load application config
    //if (!loadConfigFile(dllDir, L"skyviewer-config.xml"))
    //    int i = 0;// AfxMessageBox(RD_GetLastErrorMsg());

    // load error config
    LoadErrorConfig(dllDir, L"error-config.xml");

    // load loader control
    if (!loadFmtConfigFile(dllDir, L"loader-config.xml"))
        return false;
        //int i = 0;// AfxMessageBox(L"error");// RD_GetLastErrorMsg());


    return true;
}

