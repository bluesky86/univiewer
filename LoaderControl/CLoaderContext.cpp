#include "stdafx.h"
#include "CLoaderContext.h"
#include "LoaderControl.h"


CLoaderContext::CLoaderContext()
    :pLoaderEntry(nullptr), nCurrentPageNo(-1), pFileName(nullptr), pUvPage(nullptr), pUvDoc(nullptr)
{
}


CLoaderContext::~CLoaderContext()
{
    if (pUvDoc) delete pUvDoc;
    if (pFileName) delete[]pFileName;

    //if (pUvPage)
    //{
    //    //if (pUvContext->pUvPage->fontList) RemoveFont(pUvContext->pUvPage->fontList);

    //    //// release font of doc
    //    //if (pUvContext->renderDoc.fontList) RemoveFont(pUvContext->renderDoc.fontList);

    //    delete pUvPage;
    //}

    //if(pLoaderEntry && ((CLoaderEntry*)pLoaderEntry)->fCloseDoc) ((CLoaderEntry*)pLoaderEntry)->fCloseDoc(pUvDoc);
}

CUvDoc* CLoaderContext::Load(const wchar_t *fileName)
{
    if (fileName == nullptr) return nullptr;

    if (pFileName) delete[] pFileName;

    return nullptr;
}


