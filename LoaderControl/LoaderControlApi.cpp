#include "stdafx.h"
#include "header.h"
#include "LoaderControl.h"

//LOADERCTRL_API
CLoaderControl *g_LoaderControl = nullptr;
//CLoaderControl g_LoaderControl;

LOADERCTRL_API HLoaderControl LoaderControl_Get()
{
    return g_LoaderControl;
    //return &g_LoaderControl;
}

LOADERCTRL_API void LoaderControl_Close()
{
    if (g_LoaderControl)
        delete g_LoaderControl;
}

LOADERCTRL_API HLoaderControl LoaderControl_Init(const wchar_t* dllDir)
{
    if (g_LoaderControl == nullptr)
        g_LoaderControl = NEW CLoaderControl();

    if (g_LoaderControl->InitLoaderControl(dllDir)) return &g_LoaderControl;
    else
    {
        delete g_LoaderControl;
        g_LoaderControl = nullptr;
        return nullptr;
    }

    //return &g_LoaderControl;
}

LOADERCTRL_API HLoaderContext LoaderControl_OpenContext(const wchar_t *wFileName)
{
    if (g_LoaderControl)
        return (HLoaderContext)g_LoaderControl->OpenFile(wFileName);

    return nullptr;
}

LOADERCTRL_API void LoaderControl_CloseContext(HLoaderContext loaderContext)
{
    if (g_LoaderControl) g_LoaderControl->CloseFile((CLoaderContext*)loaderContext);
}

LOADERCTRL_API CUvPage* LoaderControl_LoadPage(HLoaderContext loaderContext, S32 incr, S32 pageNo)
{
    if (g_LoaderControl)
        return g_LoaderControl->LoadPage((CLoaderContext*)loaderContext, incr, pageNo);

    return nullptr;
}

LOADERCTRL_API CUvPage* LoaderControl_GetCurrentPage(HLoaderContext loaderContext)
{
    if (loaderContext)
        return ((CLoaderContext*)loaderContext)->pUvPage;

    return nullptr;
}