// header.h : include file for standard system include files,
// or project specific include files
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

#ifdef LOADERCONTROL_EXPORTS
#define LOADERCTRL_API __declspec(dllexport)
#else
#define LOADERCTRL_API __declspec(dllimport)
#endif

typedef void* HLoaderControl;
typedef void* HLoaderContext;


// TODO: reference additional headers your program requires here
