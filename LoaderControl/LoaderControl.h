#pragma once
//#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class

#include "header.h"
//#include <vector>
#include "ILoader.h"
#include "CLoaderContext.h"
#include "Vector.h"

//#include "types.h"
//#include "Vector.h"
//#include "RenderDoc.h"
//#include "ILoader.h"

#define RD_OPEN_FILE_FILTER            _T("All Files (*.*)|*.*||")
//#define RD_OPEN_FILE_FILTER            L"All Files (*.*)|*.*|PDF Files (*.pdf)|*.pdf|PNG Files (*.png)|*.png|Tiff Files (*.tif)|*.tif|Jpeg Files (*.jpg)|*.jpg|Jpeg Files (*.jpeg)|*.jpeg|Jpeg Files (*.jfif)|*.jfif|Jpeg Files (*.jfif-tbnl)|*.jfif-tbnl|Jpeg Files (*.jpe)|*.jpe|JP2 Files (*.jp2)|*.jp2||"
//#define RD_SAVE_FILE_FILTER            L"Jpeg Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|Tiff Files (*.tif)|*.tif|All Files (*.*)|*.*||"
//#define RD_SAVE_FILE_FILTER            L"Jpeg File (*.jpg)|*.jpg|\
//8-bit Jpeg File (*.jpg)|*.jpg|\
//PNG File (*.png)|*.png|\
//Tiff File (*.tif)|*.tif|\
//1-bit Tiff File (*.tif)|*.tif|\
//8-bit Tiff File (*.tif)|*.tif||"

typedef void* Handle;

class LOADERCTRL_API CFmtEntry {
public:
    CFmtEntry(void) {
        ext = nullptr;
        description = nullptr;
        bWrite1Bit = false;
        bWrite8Bit = false;
        bWrite8BitGrayScale = false;
        bWriteRgb = false;
        bWriteCompound = false;
    };
    ~CFmtEntry()
    {
        if (ext) delete[] ext;
        if (description) delete[] description;
    }

public:
    char *ext;
    char *description;
    bool bWrite1Bit;
    bool bWrite8Bit;
    bool bWrite8BitGrayScale;
    bool bWriteRgb;
    bool bWriteCompound;
};

class LOADERCTRL_API CLoaderEntry {
public:
    CLoaderEntry(void) :dllName(0), configFile(0), hDllProc(0), bIsImage(0), fRecognizer(0), fOpenDoc(0), fGetPage(0), fCloseDoc(0), fGetLastError(0)//, fWriteImage(0), fConvertDoc(0)
    {};
    ~CLoaderEntry()
    {
        if (fCloseLoader) fCloseLoader();
        if (dllName) delete[] dllName;
        if (configFile) delete[] configFile;
        for (unsigned int i = 0; i < entries.size(); i++)
            delete entries[i];
        entries.clear();
    }

public:
    //bool loadConfigFile(wchar_t *configFile);

public:
    char *dllName;// [DLLNAME_MAX_LENGTTH];
    char *configFile;
    std::vector<CFmtEntry*> entries;
    Handle hDllProc;
    bool bIsImage;
    loader_CloseLoader fCloseLoader;
    loader_LoadXmlConfig fLoadXmlConfig;
    loader_Recognizer fRecognizer;
    loader_RecognizerBuffer fRecognizerBuffer;
    loader_OpenDoc fOpenDoc;
    loader_OpenDocFromBuffer fOpenDocFromBuffer;
    loader_GetPage fGetPage;
    loader_CloseDoc fCloseDoc;
    loader_GetLastError fGetLastError;
    //loader_LoadRdImage fLoadRdImage;
    //loader_WriteImage fWriteImage;
    //loader_ConvertDoc fConvertDoc;
};

// This class is exported from the LoaderControl.dll
class LOADERCTRL_API CLoaderControl {
public:
    CLoaderControl(void);
    ~CLoaderControl();

    bool InitLoaderControl(const wchar_t* dllDir);

    // format recognization
    int Recognize(const wchar_t *wFileName);
    int Recognize(unsigned char *pData, int len);

    // load file for render: called by mainframe
    CLoaderContext* NewFile();
    CLoaderContext* OpenFile(const wchar_t *wFileName);
    //CLoaderContext* OpenFile(U8 *pData, size_t dataLen);
    CUvPage* LoadPage(CLoaderContext* pUvContent, int incr, int pageNo = -1);
    void CloseFile(CLoaderContext* pLoaderDoc);

private:

    //// load application config
    //bool loadConfigFile(const wchar_t* dllDir, const wchar_t *configFile);

    // load loader config from xml file
    bool loadFmtConfigFile(const wchar_t* dllDir, const wchar_t *configFile);

    // load image
    //RenderDoc::CRdImage *DecodeImage(int loaderId, unsigned char *pData, int len);

    CLoaderEntry* FindLoader(wchar_t* ext, CFmtEntry **fmtEntry = NULL);
    unsigned char GetColorType(int filterIndex);

public:
    //CVector<CLoaderEntry*> *loaderEntries;
    std::vector<CLoaderEntry*> *loaderEntries;
    wchar_t *saveAsFilter;

private:
    bool loadDLL();
    void BuildSaveAsFilter();

private:
    char libDirectory[FILEPATH_MAX_LENGTH];
    std::vector<unsigned char> saveAsFilterColorType;
};


