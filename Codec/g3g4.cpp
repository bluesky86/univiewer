#include "stdafx.h"
#include "g3g4_internal.h"
#include "Codec.h"
#include "DebugNew.h"

typedef unsigned char u_char;

#define G4G3_MODE_G4   1
#define G4G3_MODE_G3   2
#define G4G3_MODE_G31D 3
#define G4G3_MODE_G32D 4
#define G4G3_MODE_G4MT 5
#define G4G3_MODE_G3MT 6
#define G4G3_MODE_XG4  7


#define INBUFSIZE     4096
#define END_BUFF_SIZE   16

/* 8TT + 4Width1s for num transitions at end of line for fast tables */
#define G4G3_NUMENDTT   12

static U8 bittest[8] = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };
static U8 g4g3_eols[] = { 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01 };

/* States for G32D & XG4 */
#define G3_CHECK_EOL  1
#define G3_G4         2
#define G3_G3         3
#define XG4_NEW_LINE  1
#define XG4_TEST_LINE 2
#define XG4_G3        3
#define XG4_G4        4
#define XG4_RAW       5

#define ICL_MAXTBL      512000

typedef struct _context_g3_g4 {
    /* G4G3 context */
    TRL_G4G3                g4g3;
    /* Translator context */
    U8                      pad[32];       /* to keep refline > 32 bytes from 0 */
    S32                     width;
    S32                     width1;
    S32                     twidth;        /* Tile width     */
    S32                     length;
    S32                     tlength;
    S32                     linect;
    S32                     byteoffset;
    int                     G3_state;      /* FOR G3 2D & 1D - state flag */
    int                     bitSS;         /* bit pos for SS save */
    S32                     byteSS;        /* byte pos for SS save */
    unsigned                numSS;         /* Numbytes for last SS gen  */
    U8PTR                   dataSS;        /* Data pointer for last SS gen */
    bool                    AddBlock;      /* TRUE if adding Blocks       */
    bool                    AddSS;         /* TRUE if adding Start States */
    bool                    tiffg3;        /* TRUE when using TIFF G3 data */
    ICL_TBLTYPE G4G3_FAR   *codeline;
    ICL_TBLTYPE G4G3_FAR   *refline;       /* Must be > 28 bytes in from 0 */
    //RSL_CONTEXTPTR          pCon;          /* RSL context for adding or using SS */
    IM_BOOL                 logged_msg;    /* TRUE once 1 msg gets logged */
    U8PTR                   tendbuff;      /* If currinbuff > tendbuff then not enough room for tt lines */
    U8PTR                   raw_buff;      /* Buffer to hold line of raw data      - for XG4 */
    int                     raw_state;     /* State var for PBIT decompression     - for XG4 */
    int                     raw_pos;       /* How far in raw_buff we currently are - for XG4 */
    ICL_TBLTYPE_PTR         tt_buff;       /* Buffer to hold XORed TT line of data - for XG4 */
    /* DATA buffers */
    U8                      inbuff[INBUFSIZE + END_BUFF_SIZE];
    U8                      linea[2L * ICL_MAXTBL];

    int                     out_bytesPerRow;
    int                     out_pos;
    U8                      *out;
    CUvImage                *pUvImage;
} CONTEXT_TRL, FAR *CONTEXT_TRLPTR;


UNIV_CODEC_API void* SetupG3G4Decoding(CUvImage *pUvImage, int *outNumBits, int *out_size)
{
    CONTEXT_TRLPTR c;
    S32            maxlength;

    S32 width = 0, length = 0, twidth = 0, tlength = 0;
    bool bTiff = false;

    //width = twidth = pUvImage->data[0]->width;// pUvImage->imageWidth;
    //length = tlength = pUvImage->imageHeight;
    width = pUvImage->imageWidth; 
    length = pUvImage->imageHeight;
    twidth = pUvImage->tileWidth;
    tlength = pUvImage->tileHeight;

    DOUBLE         len = 2.0L*ICL_MAXTBL - sizeof(ICL_TBLTYPE) * (twidth + 4) - G4G3_TTINFOHDRSIZE;

    if (width >= MAX_S32 || len > MAX_S32)
        return(nullptr);

    c = NEW CONTEXT_TRL;

    if (c == NULL)
        return(nullptr);

    maxlength = (S32)len;
    if (maxlength < ICL_MAXTBL)
        maxlength = ICL_MAXTBL;
    c->tendbuff = c->linea + maxlength;
    c->logged_msg = FALSE;
    c->width = width;
    c->width1 = (width + 1);
    c->twidth = twidth;
    c->length = length;
    c->tlength = tlength;
    c->linect = 0;
    c->codeline = (ICL_TBLTYPE G4G3_FAR *)(c->linea);
    c->refline = (ICL_TBLTYPE G4G3_FAR *)(U8 G4G3_FAR *)&(c->linea[ICL_MAXTBL]);
    *c->codeline++ = 1;
    *c->codeline++ = 1;
    *c->refline++ = 1;
    *c->refline++ = 1;
    c->tiffg3 = false;
    c->AddBlock = false;
    c->AddSS = false;

    /* Use RSL length vs tlength for strip calculation - We cannot use SS in striped data */
    //c->pCon = pCon;

    c->byteoffset = 0;
    c->bitSS = 0;
    c->byteSS = 0;
    c->G3_state = G3_CHECK_EOL;
    c->raw_buff = NULL;
    c->tt_buff = NULL;
    c->g4g3.indatacur = c->inbuff;
    c->g4g3.indataend = c->inbuff;
    c->g4g3.refline = c->refline;
    c->g4g3.refend = c->refline;
    c->g4g3.codeline = c->codeline;
    c->g4g3.codestart = c->codeline;
    c->g4g3.width = twidth + 1;
    c->g4g3.inbit = 0;
    c->g4g3.run = 0;
    c->g4g3.lastpass = 0;
    c->g4g3.tt = 1;
    c->g4g3.eolfound = FALSE;
    c->g4g3.resink = FALSE;
    c->g4g3.g4cLineType = G4C_LINE_DUP;
    /* The next two booleans should never = TRUE unless the indatatype = RSL_IDF_G3MTIFF */
    c->g4g3.bTiled = FALSE;
    c->g4g3.bTileLine = FALSE;

    c->refline[0] = (ICL_TBLTYPE)(c->g4g3.width);
    c->refline[1] = (ICL_TBLTYPE)(c->g4g3.width);
    c->refline[2] = (ICL_TBLTYPE)(c->g4g3.width);
    c->refline[3] = (ICL_TBLTYPE)(c->g4g3.width);

    c->g4g3.command = G4G3_COMMAND_START;

    /*if (pCon) {
        if (RSL_GetUsingSSCon(c->pCon, 0)) {
            if (indatatype == RSL_IDF_G4M || indatatype == RSL_IDF_G3M2D || indatatype == RSL_IDF_XG4) {
                // We have a G4 SS so setup the reference line 
                unsigned int sz = RSL_GetSSNumDataCon(c->pCon, 0);
                if (sz > ICL_MAXTBL)
                    sz = ICL_MAXTBL;

                MEMCPY(c->refline, RSL_GetSSDataCon(c->pCon, 0), sz);
                c->g4g3.refend = c->refline + (RSL_GetSSNumDataCon(c->pCon, 0) / sizeof(ICL_TBLTYPE)) - G4G3_NUMENDTT;
            }
            c->g4g3.inbit = RSL_GetSSBitCon(c->pCon, 0);
            c->linect = RSL_GetSSLineCon(c->pCon, 0);
            c->length += c->linect;
            c->byteoffset = RSL_GetSSByteOffset(c->pCon, 0);
            if (indatatype == RSL_IDF_XG4) {
                c->g4g3.g4cLineType = c->g4g3.inbit >> 8;
                c->g4g3.inbit &= 0xff;
            }
        }

        if (RSL_GetModeCon(c->pCon) & RSL_MODE_ADDBLOCK) {
            c->AddBlock = TRUE;
        }
        if (RSL_GetModeCon(c->pCon) & RSL_MODE_ADDSS) {
            c->AddSS = TRUE;
        }
    }*/

    switch (pUvImage->encodingType)
    {
    case uvImageEncoding_G3M: // G3M and G3MTIFF
        c->g4g3.mode = G4G3_MODE_G31D;
        c->g4g3.state = G4G3_STATE_WHITE;
        c->tiffg3 = bTiff;
        if (c->tiffg3 && length > tlength)
            c->g4g3.bTiled = TRUE;
        break;
    case uvImageEncoding_G3MT:
        c->g4g3.mode = G4G3_MODE_G3MT;
        c->g4g3.state = G4G3_STATE_WHITE;
        break;
    case uvImageEncoding_G3M2D:
        c->g4g3.mode = G4G3_MODE_G32D;
        c->g4g3.state = G4G3_STATE_WHITE;
        break;
    case uvImageEncoding_G4M:
        c->g4g3.mode = G4G3_MODE_G4;
        c->g4g3.state = G4G3_STATE_G4V_GETCODE;
        break;
    case uvImageEncoding_G4MT:
        c->g4g3.mode = G4G3_MODE_G4MT;
        c->g4g3.state = G4G3_STATE_G4V_GETCODE;
        break;
    case uvImageEncoding_XG4:
        c->g4g3.state = G4G3_STATE_G4V_GETCODE;
        c->raw_buff = NEW U8[c->width1 + 20];
        c->tt_buff = (S32*) NEW U8[ICL_MAXTBL];
        if (c->raw_buff == NULL || c->tt_buff == NULL)
        {
            if (c->raw_buff)
                delete[] c->raw_buff;
            if (c->tt_buff)
                delete[] c->tt_buff;
            return(nullptr);
        }
    default:
        delete c;
        return(nullptr);
    }

    c->out_pos = 0;
    c->pUvImage = pUvImage;

    CalculateSize(pUvImage, outNumBits, out_size, &c->out_bytesPerRow);

    c->out_bytesPerRow /= (*outNumBits / 8);

    return (void*)c;
}

UNIV_CODEC_API void CleanupG3G4Decoding(void *cp)
{
    CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;
    if (c == nullptr) return;

    if (c->raw_buff)
        delete[] c->raw_buff;
    if (c->tt_buff)
        delete[] c->tt_buff;

    delete c;
}

int ConvertTTTo8Bit(CUvImage *pUvImage, int in_size, int *in_data, int width, char *out)
{
    if (in_size <= 0 || in_data == nullptr || width == 0) return 0;

    int pos = 0;
    int pos1 = 0;
    //int pos2;
    int value = 0;
    //if (!pUvImage->palette) value = 255;
    for (int i = 0; i < in_size && pos < width; i++)
    {
        pos1 = ((int)in_data[i]) - 1;
        if (pUvImage->palette)
            value = 1 - value;
        else
            value = 255 - value;
        memset(&out[pos], value, pos1 - pos);


        //if (pUvImage->palette)
        //{
        //    value = 1 - value;
        //    for (int j = pos; j < pos1; j++)
        //    {
        //        pos2 = j * 4;
        //        out[pos2] = pUvImage->palette[value].r;
        //        out[pos2 + 1] = pUvImage->palette[value].g;
        //        out[pos2 + 2] = pUvImage->palette[value].b;
        //        out[pos2 + 3] = pUvImage->palette[value].a;
        //    }
        //}
        //else
        //{
        //    memset(&out[pos], value, pos1 - pos);
        //    value = 255 - value;
        //}
        pos = pos1;
    }

    return 1;
}

// 0 - failed, 1 - success, -1 : error
int DecodeNonTiled(CONTEXT_TRLPTR c, int data_len, char *pData)
{
    int ret = 0;

    S32                  num;
    S32                  inbytes;
    U8PTR                indata;
    ICL_TBLTYPE G4G3_FAR *pTmpTBL;

    //////////////////////////////////////////////////////
    // initialize input
    //////////////////////////////////////////////////////
    inbytes = data_len;
    indata = (U8PTR)pData;
         
    // Copy down existing data
    num = (S32)(c->g4g3.indataend - c->g4g3.indatacur);

    if (num)
    {
        S32 i = 0;
        // A memcpy will work, but num is 2 or less
        while (i < num)
        {
            c->inbuff[i] = c->g4g3.indatacur[i];
            i += 1;
        }
    }
    c->g4g3.indatacur = c->inbuff;
    if (inbytes <= (INBUFSIZE - num)) {
        memcpy(c->inbuff + num, indata, inbytes);
        c->g4g3.indataend = c->inbuff + num + inbytes;
        inbytes = 0;
    }
    else {
        memcpy(c->inbuff + num, indata, INBUFSIZE - num);
        indata += INBUFSIZE - num;
        inbytes = inbytes - (INBUFSIZE - num);
        c->g4g3.indataend = c->inbuff + INBUFSIZE;
    }


NEWLINE: // loop row by row (goto NEWLINE;)
    //////////////////////////////////////////////////////
    // decode the raw data
    //////////////////////////////////////////////////////
    switch (c->g4g3.mode)
    {
    case G4G3_MODE_G31D:
        if (c->G3_state == G3_CHECK_EOL)
        {
            if (!c->tiffg3)
            {
                ret = trl_PassEOL((TRL_G4G3PTR)&c->g4g3);
                /*
                 * Intergraph doesn't pad to image width, and this would
                 * cause the G3 engine to send a pre-mature EOF if we
                 * don't re-set eolfound to FALSE.  - MarkG 8/13/96
                 */
                c->g4g3.eolfound = FALSE;
                if (ret == G4G3_EOD)
                {
                    break;
                }
            }
            c->g4g3.state = G4G3_STATE_WHITE;
        }
        c->G3_state = G3_CHECK_EOL;
        ret = trl_G31DExpand((TRL_G4G3PTR)&c->g4g3);
        if (ret != G4G3_EOL) {
            c->G3_state = G3_G3;
        }
        break;
    case G4G3_MODE_G4:
        ret = trl_G4Expand((TRL_G4G3PTR)&c->g4g3);
        break;
    case G4G3_MODE_G32D:
        switch (c->G3_state) {
        case G3_CHECK_EOL:
            ret = trl_PassEOL((TRL_G4G3PTR)&c->g4g3);
            if (ret != G4G3_EOF) {
                break;
            }

            if (*c->g4g3.indatacur & bittest[c->g4g3.inbit++]) {
                c->g4g3.state = G4G3_STATE_WHITE;
                /* Reset these here since G4 left them uninit. */
                c->g4g3.run = 0;
                c->g4g3.tt = 1;
                ret = trl_G31DExpand((TRL_G4G3PTR)&c->g4g3);
                if (ret != G4G3_EOL) {
                    c->G3_state = G3_G3;
                }
            }
            else {
                c->g4g3.state = G4G3_STATE_G4V_GETCODE;
                ret = trl_G4Expand((TRL_G4G3PTR)&c->g4g3);
                if (ret != G4G3_EOL) {
                    c->G3_state = G3_G4;
                }
            }
            break;

        case G3_G3:
            ret = trl_G31DExpand((TRL_G4G3PTR)&c->g4g3);
            if (ret != G4G3_EOD) {
                c->G3_state = G3_CHECK_EOL;
            }
            break;

        case G3_G4:
            ret = trl_G4Expand((TRL_G4G3PTR)&c->g4g3);
            if (ret != G4G3_EOD) {
                c->G3_state = G3_CHECK_EOL;
            }
            break;
        } /* switch  G3_state */
        break;
    case G4G3_MODE_XG4:
        switch (c->G3_state)
        {
            int newcomp;

        case XG4_NEW_LINE:
            /* Check if we have enough bits to test the line type */
            while (c->g4g3.inbit > 7) {
                c->g4g3.indatacur += 1;
                c->g4g3.inbit -= 8;
            }
            if (c->g4g3.indatacur + (c->g4g3.inbit + 5) / 8 >= c->g4g3.indataend) {
                c->G3_state = XG4_TEST_LINE;
                ret = G4G3_EOD;
                break;
            }
        case XG4_TEST_LINE:
            c->G3_state = XG4_NEW_LINE;
            newcomp = (*c->g4g3.indatacur & bittest[c->g4g3.inbit++]);
            if (c->g4g3.inbit > 7) {
                c->g4g3.indatacur += 1;
                c->g4g3.inbit -= 8;
            }
            if (newcomp == 0) {
                /* 1 bit is same compression as last line, 0 --> new type */
                newcomp = (*c->g4g3.indatacur & bittest[c->g4g3.inbit++]) ? 2 : 0;
                if (c->g4g3.inbit > 7) {
                    c->g4g3.indatacur += 1;
                    c->g4g3.inbit -= 8;
                }
                newcomp += (*c->g4g3.indatacur & bittest[c->g4g3.inbit++]) ? 1 : 0;
                if (c->g4g3.inbit > 7) {
                    c->g4g3.indatacur += 1;
                    c->g4g3.inbit -= 8;
                }
                if (newcomp == 1)
                    c->g4g3.g4cLineType = G4C_LINE_G4;
                else if (newcomp == 2)
                    c->g4g3.g4cLineType = G4C_LINE_G3;
                else if (newcomp == 3)
                    c->g4g3.g4cLineType = G4C_LINE_XORG3;
                else {
                    newcomp = (*c->g4g3.indatacur & bittest[c->g4g3.inbit++]) ? 2 : 0;
                    if (c->g4g3.inbit > 7) {
                        c->g4g3.indatacur += 1;
                        c->g4g3.inbit -= 8;
                    }
                    newcomp += (*c->g4g3.indatacur & bittest[c->g4g3.inbit++]) ? 1 : 0;
                    if (newcomp == 1)
                        c->g4g3.g4cLineType = G4C_LINE_DUP;
                    else if (newcomp == 2)
                        c->g4g3.g4cLineType = G4C_LINE_BLANK;
                    else if (newcomp == 3)
                        c->g4g3.g4cLineType = G4C_LINE_RAW;
                    else {
                        newcomp = newcomp;
                        /* error */
                    }
                }
            }

            switch (c->g4g3.g4cLineType) {
            case G4C_LINE_G3:
            case G4C_LINE_XORG3:
                c->g4g3.state = G4G3_STATE_WHITE;
                /* Reset these here since G4 left them uninit. */
                c->g4g3.run = 0;
                c->g4g3.tt = 1;
                ret = trl_G31DExpand((TRL_G4G3PTR)&c->g4g3);
                if (ret != G4G3_EOL) {
                    c->G3_state = XG4_G3;
                }
                break;

            case G4C_LINE_G4:
                c->g4g3.state = G4G3_STATE_G4V_GETCODE;
                ret = trl_G4Expand((TRL_G4G3PTR)&c->g4g3);
                if (ret != G4G3_EOL) {
                    c->G3_state = XG4_G4;
                }
                break;

            case G4C_LINE_BLANK:
                c->codeline[0] = (ICL_TBLTYPE)c->width1;
                c->codeline[1] = (ICL_TBLTYPE)c->width1;
                c->codeline[2] = (ICL_TBLTYPE)c->width1;
                c->codeline[3] = (ICL_TBLTYPE)c->width1;
                c->g4g3.codeline = c->codeline + 4;
                ret = G4G3_EOL;
                break;

            case G4C_LINE_DUP:
                pTmpTBL = c->refline;
                c->g4g3.codeline = c->codeline;
                while (*pTmpTBL < (ICL_TBLTYPE)c->width1) {
                    *c->g4g3.codeline++ = *pTmpTBL++;
                }
                *c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
                *c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
                *c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
                *c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
                ret = G4G3_EOL;
                break;

            case G4C_LINE_RAW:
                c->raw_state = 0;
                c->raw_pos = 0;
                c->G3_state = XG4_RAW;
                goto NEWLINE;

            default:
                break;
            } /* switch g4cLineType */

            break;


        case XG4_G3:
            ret = trl_G31DExpand((TRL_G4G3PTR)&c->g4g3);
            if (ret != G4G3_EOD) {
                c->G3_state = XG4_NEW_LINE;
            }
            break;

        case XG4_G4:
            ret = trl_G4Expand((TRL_G4G3PTR)&c->g4g3);
            if (ret != G4G3_EOD) {
                c->G3_state = XG4_NEW_LINE;
            }
            break;

        case XG4_RAW:
            break;
            //{
            //    S32 numin_used = 0;
            //    S32 numout = 0;
            //    S32 num_needed = (c->g4g3.width + 6) / 8 - c->raw_pos; /* g4g3.width is width + 1 */
            //    if (c->g4g3.inbit > 7) {
            //        c->g4g3.inbit -= 8;
            //        c->g4g3.indatacur += 1;
            //    }
            //    if (c->g4g3.inbit > 0) {
            //        c->g4g3.inbit = 0;
            //        c->g4g3.indatacur += 1;
            //    }
            //    TRL_PBIT_to_MSB(c->g4g3.indatacur, c->raw_buff + c->raw_pos,
            //        c->g4g3.indataend - c->g4g3.indatacur, num_needed,
            //        &numin_used, &numout, &c->raw_state);
            //    c->g4g3.indatacur += numin_used;
            //    if (numout < num_needed) {
            //        c->raw_pos = (int)(c->raw_pos + numout);
            //        ret = G4G3_EOD;
            //        break;
            //    }
            //    numout = (S32)TRL_MSB_to_TBL(c->raw_buff, c->codeline, c->width);
            //    c->g4g3.codeline = c->codeline + numout;
            //}
            //*c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
            //*c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
            //*c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
            //*c->g4g3.codeline++ = (ICL_TBLTYPE)c->width1;
            //ret = G4G3_EOL;
            //c->G3_state = XG4_NEW_LINE;
            //break;

        } /* switch  G3_state */
    default:
        break;
    }

    //////////////////////////////////////////////////////
    // post process of decoding one row
    //////////////////////////////////////////////////////
    switch (ret)
    {
        /* Normal case - send out the line */
    case G4G3_EOL:

        c->linect += 1;
        /* Check to see if we are entering the last line of a tile */
        if (c->g4g3.bTiled && ((c->linect + 1) % c->tlength) == 0)
            c->g4g3.bTileLine = TRUE;
        else
            c->g4g3.bTileLine = FALSE;

        /* If we have reached the last line then quit. */
        if ((c->linect > c->length)) {
            return 1;
        }

        //if (c->g4g3.g4cLineType == G4C_LINE_XORG3) {
        //    int             destnum;
        //    TRL_XOR_TBL(c->tt_buff, &destnum,
        //        c->codeline, c->g4g3.codeline - c->codeline,
        //        c->refline, c->g4g3.refend - c->refline);
        //    MEMCPY(c->codeline, c->tt_buff, destnum * sizeof(ICL_TBLTYPE));
        //    c->g4g3.codeline = c->codeline + destnum;
        //}

        num = (S32)((U8PTR)c->g4g3.codeline - (U8PTR)c->codeline);
        ConvertTTTo8Bit(c->pUvImage, num, c->codeline, c->width, (char*)&c->out[c->out_pos]);

        //memcpy(&c->out[c->out_pos], c->codeline, c->out_bytesPerRow);
        c->out_pos += c->out_bytesPerRow;

        if (c->tiffg3 && (c->g4g3.inbit & 7)) {
            /* TIFF data is byte aligned per line */
            c->g4g3.inbit += 8 - (c->g4g3.inbit & 7);
        }

        //if (c->AddSS && (((c->linect) % RSL_KFACTOR) == 0)) {
        //    c->byteSS = c->byteoffset + (c->g4g3.indatacur - (U8 G4G3_FAR *)c->inbuff);
        //    c->bitSS = c->g4g3.inbit;
        //    c->dataSS = (U8PTR)c->codeline;
        //    /* For XG4 add line type to bitSS */
        //    if (c->g4g3.mode == G4G3_MODE_XG4) {
        //        c->bitSS |= (c->g4g3.g4cLineType << 8);
        //    }
        //    /* Don't need refline for G31D data */
        //    if (c->g4g3.mode == G4G3_MODE_G31D) {
        //        c->numSS = 0;
        //    }
        //    else {
        //        c->numSS = (U8PTR)c->g4g3.codeline - (U8PTR)c->codeline;
        //    }
        //    RSL_AddSS(c->pCon, 0, c->byteSS, c->linect, c->bitSS, c->numSS, c->dataSS);
        //}

        /* Swap code and ref lines and continue */
        pTmpTBL = c->refline;
        c->refline = c->codeline;
        c->codeline = pTmpTBL;
        c->g4g3.refend = c->g4g3.codeline - G4G3_NUMENDTT;
        c->g4g3.codeline = c->codeline;
        c->g4g3.codestart = c->codeline;
        c->g4g3.refline = c->refline;
        c->g4g3.command = G4G3_COMMAND_NEWLINE;

        goto NEWLINE;

    case G4G3_EOD:

        c->byteoffset += (S32)((U8PTR)c->g4g3.indatacur - c->inbuff);
        c->g4g3.command = G4G3_COMMAND_CONTINUE;

        if (!inbytes) {
            return 1; /* Get more input data */
        }
        /* Optimization so only one memcopy is needed- MarkG 8/8/96 */
        num = (S32)(c->g4g3.indataend - c->g4g3.indatacur);
        inbytes += num;
        indata -= num;
        /* Calculate buffer size (min function doesn't work on S32's) */
        num = (((S32)INBUFSIZE <= inbytes) ? INBUFSIZE : inbytes);
        memcpy(c->inbuff, (U8*)indata, num);

        inbytes -= num;
        indata += num;
        c->g4g3.indatacur = c->inbuff;
        c->g4g3.indataend = c->inbuff + num;
        goto NEWLINE;

    case G4G3_EOF:
        return 1;
        /* Errors */
    case G4G3_BADWHITELEN:
    case G4G3_BADBLACKLEN:
    case G4G3_BADLINE:
        /*
         * We can't recover from G4 because it uses refline.  We
         * also can't recover from G3TIFF as it has no EOL markers
         */
        if (c->tiffg3 || (c->g4g3.mode == G4G3_MODE_G4)) {

            return -1;

        }
        else {

            /* Lets try to send out what we got if it is OK */
            ICL_TBLTYPE G4G3_FAR *codeline = c->codeline + 1;

            c->linect += 1;

            while (codeline < c->g4g3.codeline) {

                if ((*(codeline - 1) >= *codeline) || ((S32)*codeline >= c->width1))
                    break;
                codeline++;
            }
            if (codeline > c->g4g3.codeline)
                codeline -= 1;

            *codeline++ = (ICL_TBLTYPE)c->width1;
            *codeline++ = (ICL_TBLTYPE)c->width1;
            *codeline++ = (ICL_TBLTYPE)c->width1;
            *codeline++ = (ICL_TBLTYPE)c->width1;

            num = (S32)((U8PTR)c->g4g3.codeline - (U8PTR)c->codeline);
            ConvertTTTo8Bit(c->pUvImage, num, c->codeline, c->width, (char*)&c->out[c->out_pos]);
            //memcpy(&c->out[c->out_pos], c->codeline, c->out_bytesPerRow);
            c->out_pos += c->out_bytesPerRow;

            /* Swap code and ref lines and continue */
            pTmpTBL = c->refline;
            c->refline = c->codeline;
            c->codeline = pTmpTBL;
            c->g4g3.refend = c->g4g3.codeline - G4G3_NUMENDTT;
            c->g4g3.codeline = c->codeline;
            c->g4g3.codestart = c->codeline;
            c->g4g3.refline = c->refline;
            c->g4g3.resink = TRUE;
            c->G3_state = G3_CHECK_EOL;
            goto NEWLINE;
        }
        /* We should handle ALL return codes */
    default:
        return -1;
    }


    return 1;
}

int DecodeTiled(CONTEXT_TRLPTR c, int data_len, char *pData)
{
    int                  ret = 0;
//    S32                  num;
//    S32                  inbytes;
//    U8PTR                indata;
//    int                  column = 1 - (int)c->twidth; /* = Sum of all tile widths prior to current tile */
//    U8 G4G3_FAR          *StartPointer = NULL;          /* Where to start copying in Tile Block Buffer    */
//    G4G3_TTBLKHDRPTR FAR *pOldNextBlockPointer = NULL;          /* Previous block's next pointer                  */
//    G4G3_TTBLKHDRPTR     pBlockHeader = NULL;          /* Pointer to Header of current tile TT Block     */
//    G4G3_TTINFO G4G3_FAR *pTTInfo = NULL;          /* Pointer to a line of TT Information for a tile */
//
//GETNEXTTILE:
//    //////////////////////////////////////////////////////
//    // initialize input
//    //////////////////////////////////////////////////////
//    inbytes = data_len;
//    indata = (U8PTR)pData;
//
//    if (indata == nullptr) return -1;
//
//    column += (int)c->twidth;
//    c->g4g3.width = column + c->twidth;
//    if (c->g4g3.width > c->width1)
//        c->g4g3.width = c->width1;
//
//    /* Initially, the previous next block pointer is out->data */
//    pOldNextBlockPointer = (G4G3_TTBLKHDRPTR FAR *)&(out->data);
//
//    /* Fill in the input buffer to the G4 Engine */
//    /* Calculate buffer size (min function doesn't work on S32's) */
//    num = ((inbytes < (S32)INBUFSIZE) ? inbytes : INBUFSIZE);
//    memcpy(c->inbuff, indata, num);
//    c->g4g3.indatacur = c->inbuff;
//    c->g4g3.indataend = c->inbuff + num;
//
//    /* Indicate that no data has been read by G4 Engine */
//    c->byteoffset = 0;
//
//    /* Initialize the previous reference line to blank */
//    c->g4g3.refline = c->refline;
//    *(c->g4g3.refline++) = (ICL_TBLTYPE)column;
//    *(c->g4g3.refline++) = (ICL_TBLTYPE)column;
//    c->g4g3.refline[0] = (ICL_TBLTYPE)(c->g4g3.width);
//    c->g4g3.refline[1] = (ICL_TBLTYPE)(c->g4g3.width);
//    c->g4g3.refline[2] = (ICL_TBLTYPE)(c->g4g3.width);
//    c->g4g3.refline[3] = (ICL_TBLTYPE)(c->g4g3.width);
//    c->g4g3.refend = &(c->g4g3.refline[4]);
//    c->g4g3.command = G4G3_COMMAND_START;
//    c->linect = 0; /* No lines read so far */
//    if (c->g4g3.mode == G4G3_MODE_G4MT) {
//
//        c->g4g3.state = G4G3_STATE_G4V_GETCODE;
//
//    }
//    else {
//
//        c->g4g3.state = G4G3_STATE_WHITE;
//        c->G3_state = G3_CHECK_EOL;
//    }
//    c->g4g3.inbit = 0;
//    c->g4g3.run = 0;
//    c->g4g3.lastpass = 0;
//    c->g4g3.tt = 0;
//    c->g4g3.eolfound = FALSE;
//    c->g4g3.resink = FALSE;
//    c->g4g3.g4cLineType = G4C_LINE_DUP;
//    StartPointer = (U8 G4G3_FAR *)c->g4g3.refend;
//    pTTInfo = (G4G3_TTINFO G4G3_FAR *)StartPointer;
//    pTTInfo->StartData[0] = (ICL_TBLTYPE)column;
//    pTTInfo->StartData[1] = (ICL_TBLTYPE)column;
//    c->g4g3.codeline = pTTInfo->Data;
//    c->g4g3.codestart = pTTInfo->Data;
//    c->g4g3.refend -= G4G3_NUMENDTT;
//    c->codeline = pTTInfo->Data;
//
//GET_NEXT_TILE_LINE:
//
//    //////////////////////////////////////////////////////
//    // decode the raw data
//    //////////////////////////////////////////////////////
//    if (c->g4g3.mode == G4G3_MODE_G4MT)
//    {
//        ret = trl_G4Expand((TRL_G4G3PTR)&c->g4g3);
//    }
//    else
//    {
//        if (c->G3_state == G3_CHECK_EOL) 
//        {
//            ret = trl_PassEOL((TRL_G4G3PTR)&c->g4g3);
//            /*
//             * If G3 data < width, then we will encounter another
//             * EOL in G31Expand.  Setting eolfound = false prevents
//             * the engive for interpreting this as and EOF
//             */
//            if (ret == G4G3_EOF)
//            {
//                /* Need to reset state to start on White after finding EOF (12Oct99,BrianH) */
//                c->g4g3.state = G4G3_STATE_WHITE;
//            }
//            c->g4g3.eolfound = FALSE;
//            if (ret != G4G3_EOD)
//            {
//                /* Only reset tt (indicates what point on the decompressed line you are at)
//                 *  when you are at the begining of a new line.  cdefonte Sept. 15, 1999 - PMR 13774
//              *
//              * Set tt to column so that tiled g3 will start at tile offset.
//              * See PMR 10438 - MarkG, May 20, 1998
//              *
//              * If EOD while checkin for EOL, process EOD then call trl_G31DExpand.
//              * See PMR 32178 - BrianH, Oct 12, 1999
//              */
//                c->g4g3.tt = (ICL_TBLTYPE)column;
//                c->G3_state = G3_CHECK_EOL;
//                ret = trl_G31DExpand((TRL_G4G3PTR)&c->g4g3);
//                if (ret != G4G3_EOL)
//                {
//                    c->G3_state = G3_G3;
//                }
//            }
//        }
//        else
//        {
//            c->G3_state = G3_CHECK_EOL;
//            ret = trl_G31DExpand((TRL_G4G3PTR)&c->g4g3);
//            if (ret != G4G3_EOL)
//            {
//                c->G3_state = G3_G3;
//            }
//        }
//    }
//
//    //////////////////////////////////////////////////////
//    // post process of decoding one row
//    //////////////////////////////////////////////////////
//    switch (ret) {
//    case G4G3_EOL:
//SAVE_TILE_TT_LINE:
//        c->linect += 1;
//
//        /* The next TTInfo line starts following the codeline data. */
//        pTTInfo->Length = (U8 G4G3_FAR *)(c->g4g3.codeline) - (U8 G4G3_FAR *)pTTInfo;
//        pTTInfo = (G4G3_TTINFOPTR)((U8PTR)pTTInfo + pTTInfo->Length);
//        pTTInfo->StartData[0] = (ICL_TBLTYPE)column;
//        pTTInfo->StartData[1] = (ICL_TBLTYPE)column;
//
//        /* Start a new block if there isn't enough room for any more TTInfo */
//        if ((U8PTR)pTTInfo >= c->tendbuff) {
//
//            /* Allocate a buffer if it doesn't already exist */
//            if (*pOldNextBlockPointer == NULL) {
//
//                /* Return complete if we run out of memory */
//                *pOldNextBlockPointer = (G4G3_TTBLKHDRPTR)new char[2L * ICL_MAXTBL];
//                if (*pOldNextBlockPointer == NULL) {
//
//                    return 1;
//
//                }
//                (*pOldNextBlockPointer)->Next = NULL;
//            }
//            /* Copy TT Line Info before re-using buffer. */
//            num = ((U8PTR)pTTInfo - (U8PTR)StartPointer);
//            pBlockHeader = *pOldNextBlockPointer;
//            memcpy((U8PTR) &(pBlockHeader->TTInfo), StartPointer, num);
//            pBlockHeader->End = (G4G3_TTINFOPTR)((U8PTR)&(pBlockHeader->TTInfo) + num);
//            StartPointer = (U8 G4G3_FAR *)c->linea;
//            pTTInfo = (G4G3_TTINFO G4G3_FAR *) StartPointer;
//            pTTInfo->StartData[0] = (ICL_TBLTYPE)column;
//            pTTInfo->StartData[1] = (ICL_TBLTYPE)column;
//            pOldNextBlockPointer = &(pBlockHeader->Next);
//
//            /* Write out->numbytes if we are the first block */
//            if (out->numbytes == 0) {
//
//                out->numbytes = (U8PTR)pBlockHeader->End - (U8PTR)pBlockHeader;
//            }
//
//        } /* if starting new block */
//
//        /* Don't read in more lines of data than expected */
//        if (c->linect >= c->tlength) {
//
//            pTTInfo->Length = 0; /* Indicate that this is last line */
//
//            /* Allocate the output buffer if necessary */
//            if (*pOldNextBlockPointer == NULL) {
//
//                /* Return complete if we run out of memory */
//                if ((*pOldNextBlockPointer = (G4G3_TTBLKHDRPTR)new char[2L * ICL_MAXTBL]) == NULL) {
//
//                    return 1;
//                }
//                (*pOldNextBlockPointer)->Next = NULL;
//            }
//
//            /* Copy TT Line Info before re-using buffer. */
//            num = ((U8 G4G3_FAR *)pTTInfo->Data - StartPointer);
//            pBlockHeader = *pOldNextBlockPointer;
//            memcpy((U8PTR)&(pBlockHeader->TTInfo), StartPointer, num);
//            pBlockHeader->End = (G4G3_TTINFOPTR)((U8PTR)&(pBlockHeader->TTInfo) + num);
//
//            /* Write out->numbytes if we are the first block */
//            if (out->numbytes == 0) {
//
//                out->numbytes = (U8PTR)pBlockHeader->End - (U8PTR)pBlockHeader;
//            }
//
//            goto GETNEXTTILE; /* Read in the next tile of data */
//        }
//
//        /* Prepare reference/code line to get next code line from G4 Engine */
//        c->g4g3.refline = c->codeline;
//        c->g4g3.refend = c->g4g3.codeline - G4G3_NUMENDTT;
//        c->g4g3.codeline = pTTInfo->Data;
//        c->g4g3.codestart = pTTInfo->Data;
//        c->codeline = pTTInfo->Data;
//        c->g4g3.command = G4G3_COMMAND_NEWLINE;
//
//        goto GET_NEXT_TILE_LINE;
//
//    case G4G3_EOD:
//
//        if (c->g4g3.indatacur == c->inbuff) {
//            goto BAD_INPUT_DATA;
//        }
//        c->byteoffset += (U8PTR)c->g4g3.indatacur - c->inbuff;
//
//        /* Calculate buffer size (min function doesn't work on S32's) */
//        num = (((S32)INBUFSIZE <= inbytes - c->byteoffset) ? INBUFSIZE : (inbytes - c->byteoffset));
//        if (num < 0) /* safty, because extra EOL's below can cause negative number here */
//            num = 0;
//        memcpy(c->inbuff, (U8*)indata + c->byteoffset, num);
//        c->g4g3.indatacur = c->inbuff;
//        c->g4g3.indataend = c->inbuff + num;
//        c->g4g3.command = G4G3_COMMAND_CONTINUE;
//        /* Attempt to "close off" the strip/tile by added EOL's   *
//         * PMR 32178, 00000002.tif (12Oct99,BrianH)               */
//        if (c->byteoffset + num == inbytes) {
//            memcpy(c->g4g3.indataend, g4g3_eols, sizeof(g4g3_eols));
//            c->g4g3.indataend += sizeof(g4g3_eols);
//        }
//        goto GET_NEXT_TILE_LINE;
//
//    case G4G3_EOF:
//BAD_INPUT_DATA:
//        if ((c->linect < c->tlength) && (c->g4g3.mode != G4G3_MODE_G4MT)) {
//
//            /* Attempt to recover current line and continue */
//            ICL_TBLTYPE G4G3_FAR *codeline = pTTInfo->Data + 1;
//
//            while (codeline < c->g4g3.codeline) {
//
//                if ((*(codeline - 1) >= *codeline) || (*codeline >= (ICL_TBLTYPE)(c->g4g3.width)))
//                    break;
//                codeline++;
//            }
//            if (codeline > c->g4g3.codeline)
//                codeline -= 1;
//
//            /* Add line terminators */
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            c->linect += 1;
//            pTTInfo->Length = (U8PTR)codeline - (U8PTR)pTTInfo;
//
//            /* Advance TTInfo pointer to follow the previous TTInfo Line */
//            pTTInfo = (G4G3_TTINFOPTR)((U8PTR)pTTInfo + pTTInfo->Length);
//
//            /* If we can't fit more lines in this block then start new one. */
//            if ((U8PTR)pTTInfo >= c->tendbuff) {
//
//                if (*pOldNextBlockPointer == NULL) {
//
//                    /* Return complete if we run out of memory */
//                    if ((*pOldNextBlockPointer = (G4G3_TTBLKHDRPTR)new char[2L * ICL_MAXTBL]) == NULL) {
//
//                        return 1;
//                    }
//                    (*pOldNextBlockPointer)->Next = NULL;
//                }
//                /* Copy TT Line Info before re-using buffer. */
//                num = ((U8 G4G3_FAR *)pTTInfo - StartPointer);
//                pBlockHeader = *pOldNextBlockPointer;
//                memcpy((U8PTR) &(pBlockHeader->TTInfo), StartPointer, num);
//                pBlockHeader->End = (G4G3_TTINFOPTR)((U8PTR)&(pBlockHeader->TTInfo) + num);
//                StartPointer = (U8 G4G3_FAR *)(c->linea);
//                pTTInfo = (G4G3_TTINFO G4G3_FAR *) StartPointer;
//                pOldNextBlockPointer = &(pBlockHeader->Next); /* Reset previous block pointer */
//
//                /* Write out->numbytes if we are the first block */
//                if (out->numbytes == 0) {
//                    out->numbytes = (U8PTR)pBlockHeader->End - (U8PTR)pBlockHeader;
//                }
//            } /* if buffer full */
//        }
//
//        /*
//         * If G4 Engine quits pre-maturely, pad with blank TT lines
//         * unless we are dealing with strips.  It is not uncommon for
//         * people to write incorrect strip lengths and we will introduce
//         * unwanted spaces if we pad to the strip length - MarkG 10/29/96
//         */
//        while (c->linect < c->tlength && c->twidth < c->width) {
//
//            c->linect += 1;
//            pTTInfo->Data[0] = (ICL_TBLTYPE)(c->g4g3.width);
//            pTTInfo->Data[1] = (ICL_TBLTYPE)(c->g4g3.width);
//            pTTInfo->Data[2] = (ICL_TBLTYPE)(c->g4g3.width);
//            pTTInfo->Data[3] = (ICL_TBLTYPE)(c->g4g3.width);
//            pTTInfo->Length = (U8PTR)&(pTTInfo->Data[4]) - (U8PTR)pTTInfo;
//
//            /* Advance TTInfo pointer to follow the previous TTInfo Line */
//            pTTInfo = (G4G3_TTINFOPTR)((U8PTR)pTTInfo + pTTInfo->Length);
//
//            /* If we can't fit more lines in this block then start new one. */
//            if ((U8PTR)pTTInfo >= c->tendbuff) {
//
//                if (*pOldNextBlockPointer == NULL) {
//
//                    /* Return complete if we run out of memory */
//                    if ((*pOldNextBlockPointer = (G4G3_TTBLKHDRPTR)new char[2L * ICL_MAXTBL]) == NULL) {
//
//                        return 1;
//                    }
//                    (*pOldNextBlockPointer)->Next = NULL;
//                }
//                /* Copy TT Line Info before re-using buffer. */
//                num = ((U8 G4G3_FAR *)pTTInfo - StartPointer);
//                pBlockHeader = *pOldNextBlockPointer;
//                memcpy((U8PTR) &(pBlockHeader->TTInfo), StartPointer, num);
//                pBlockHeader->End = (G4G3_TTINFOPTR)((U8PTR)&(pBlockHeader->TTInfo) + num);
//                StartPointer = (U8 G4G3_FAR *)(c->linea);
//                pTTInfo = (G4G3_TTINFO G4G3_FAR *) StartPointer;
//                pOldNextBlockPointer = &(pBlockHeader->Next); /* Reset previous block pointer */
//
//                /* Write out->numbytes if we are the first block */
//                if (out->numbytes == 0) {
//                    out->numbytes = (U8PTR)pBlockHeader->End - (U8PTR)pBlockHeader;
//                }
//            } /* if buffer full */
//
//        } /* while lines are left to pad */
//
//        /* A TTInfo Structure with length = 0 means that we are done */
//        pTTInfo->Length = 0;
//
//        /* Write this information to out->data */
//        if (*pOldNextBlockPointer == NULL) {
//
//            /* Return complete if we run out of memory */
//            if ((*pOldNextBlockPointer = (G4G3_TTBLKHDRPTR)new char[2L * ICL_MAXTBL]) == NULL) {
//
//                return 1;
//            }
//            (*pOldNextBlockPointer)->Next = NULL;
//        }
//        /* Copy TT Line Info before re-using buffer. */
//        num = ((U8 G4G3_FAR *)(pTTInfo->Data) - StartPointer);
//        pBlockHeader = *pOldNextBlockPointer;
//        memcpy((U8PTR) &(pBlockHeader->TTInfo), StartPointer, num);
//        pBlockHeader->End = (G4G3_TTINFOPTR)((U8PTR)&(pBlockHeader->TTInfo) + num);
//
//        /* Write out->numbytes if we are the first block */
//        if (out->numbytes == 0) {
//
//            out->numbytes = (U8PTR)pBlockHeader->End - (U8PTR)pBlockHeader;
//        }
//
//        goto GETNEXTTILE; /* Read in the next tile of data */
//
//    default:
//
//        /* We can't recover from a G4 bad data error */
//        if (c->g4g3.mode == G4G3_MODE_G4MT) {
//
//            goto BAD_INPUT_DATA;
//        }
//
//        /* Attempt to recover current line and continue */
//        {
//            ICL_TBLTYPE G4G3_FAR *codeline = pTTInfo->Data + 1;
//
//            while (codeline < c->g4g3.codeline) {
//
//                if ((*(codeline - 1) >= *codeline) || (*codeline >= (ICL_TBLTYPE)(c->g4g3.width)))
//                    break;
//                codeline++;
//            }
//            if (codeline > c->g4g3.codeline)
//                codeline -= 1;
//
//            /* Add line terminators */
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            *codeline++ = (ICL_TBLTYPE)(c->g4g3.width);
//            c->g4g3.codeline = codeline;
//            c->g4g3.codestart = codeline;
//            c->g4g3.resink = TRUE;
//            c->G3_state = G3_CHECK_EOL;
//
//            goto SAVE_TILE_TT_LINE;
//        }
//
//    } /* end switch (ret) */

    return 1;
}

// if first_ref_len == 0 and first_ref_data == nullptr, it is frist row but no ref line
UNIV_CODEC_API char* DecodeG3G4(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len)
{
    if (cp == nullptr) return nullptr;

    int ret = 0;
    
    CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;

    if (first_ref_len > 0 && first_ref_data)
        memcpy(c->refline, first_ref_data, first_ref_len);
    else if(first_ref_len == 1) // first line but no refline specified
        memset(c->refline, 0, ICL_MAXTBL);

    c->out_pos = 0;
    c->out = (U8*)out_buffer;

    if (c->g4g3.mode == G4G3_MODE_G3MT || c->g4g3.mode == G4G3_MODE_G4MT)
        ret = DecodeTiled(c, data_len, pData);
    else
        ret = DecodeNonTiled(c, data_len, pData);

    if (ret == 1)
    {
        *out_len = c->out_pos;
        return (char*)c->out;
    }

    return nullptr;
}