#include "stdafx.h"
#include "Codec.h"
#include "DebugNew.h"
#include "type.h"

/*****************************************************************************************************************************************************************
LZW decoding Algorithm
two varaibles:
      pW - previous word (symbol from input stream)
      cW - current word (symbol from input stream)
two mapped value
      str(pW) - the string mapped from pW
      str(cW) - the string mapped from cW

steps:
1. Read next symbol cW: read n-bit data from input. The n is the value of code size ( 9, 10, 11, or 12)
2. if cW is clear code (value 256)
   a. Initialize/Reinitialize dictionary: 0->0, 1->1, ..., 255->255 ( represent the value that a byte can represent)
   b. set index 256 of dictionary to 256 (clear code)
   c. set index 257 of dictionary to 257 (end of data)
   d. set the start index of symbol to 258
   e. set pW = 0
   f. set codeSize to 9
3. find cW from dictionary
   a. cW is in dictionary
      i.   write cW to output
      ii.  set P = str(pW), C = the first character of str(cW)
      iii. insert item P+C into dictionary
   b. cW is NOT in dictionary
      i.   set P = str(pw), C = the frist character of str(pW)
      ii.  insert item P+C into dictionary. this new symbol must be cW
      iii. write P+C to output
4 goto step 1 until end of input symbol stream

When reading next symbol cW, the code size must be adjusted if the next index value of dictionary exceeds max value which code size can represent.
The code size is between 9 and 12. if code size is 9 and the next index value is 511, the code size must be set to 10. That means the next cW is 10-bit value.

******************************************************************************************************************************************************************/

/* LZW constants  */
#define  MAXBITS        12
#define  MAXCODE       5000      // must be > 4096 
#define  MAX_DIBSIZE   0xfff0L
#define  MIN_BUF_SIZE  10000
#define  MAX_LZW_WIDTH 55000     /* Make sure MAX_LZW_WIDTH is the same value in the file
                                    raw2lzw.c as well so the cap for loading and saving is the same. */

                                    // 21Mar05,BrianH:  used for Start State management - no real reason for this number - they look good and these start states are cheap.
#define LZW_SS_LINE_INTERVAL  64

typedef enum
{
    LZW_TYPE_None,
    LZW_TYPE_GIF,
    LZW_TYPE_CMYK,
    LZW_TYPE_DiffEncoded,
} LZW_TYPE;

typedef struct
{
    U32 bitsleft;
    U32 csmask;
    int codesize;
    int bitsin;
} DECOMPSTRUCT;

typedef struct __TABLEELEMENT__
{
    char ch;
    int prev;
    int count;
} TABLEELEMENT;

typedef struct __STARTSTATE__
{
    DECOMPSTRUCT      dcs;                 /* LZW decomp struct                */
    S32               spos;                /* Output buffer pos index          */
    U8                holdbuf[2];
    int               numheld;
} STARTSTATE;

// Structure for each tile.
typedef struct _tileLZW {
    //ICL_MSGPTR in;             // Tile input message.
    int        iState;         // State variable for PBIT to MSB - should always be 0.
    U8PTR      pInPos;         // Current input data pointer.
    U8PTR      buffer;//[MAX_LZW_WIDTH];
    int        bufSize;
} LZW_TILE, FAR * LZW_TILEPTR;

typedef struct _context_lzwtoraw {
    S32               bytesperline;        /* Width per line in bytes          */
    S32               spos;                /* Output buffer pos index          */
    DECOMPSTRUCT      dcs;                 /* LZW decomp struct                */
    U8PTR             buffer;//[MAX_LZW_WIDTH];/* Output decompress buffer        */
    U8PTR             inbuf;
    S32               inpnt;
    S32               insize;
    U8                holdbuf[2];
    int               numheld;
    //ICL_MSGPTR        out;
    //ICL_PLPTR         pl;
    int               nextcode;
    int               maxcode;
    int               oldcode;
    int               eodcode;
    int               clearcode;
    int               bitsperpel;
    int               diffencoded;         /* Is the LZW Data Difference Encoded      */
    IM_BOOL           bResumeAfterClear;   /* Are we resuming after a clear code      */
    IM_BOOL           bCMYK;               /* To indicate that the image file is cmyk */
    IM_BOOL           bGIF;                /* To indicate that we are from GIF format */
    //IM_BOOL           bTile;               /* True if data source is tiled            */
    IM_BOOL           bStrip;              /* True if data source is stripped         */

    // Start state varialbes:
    int               LineCtSS;          // Counter of current line for SS generation.
    int               DataOffsetSS;      // Input data byte offset.
    int               NextSS;            // Line number of the next start state.
    int               AddSS;             // TRUE if adding start states.
    int               FromSS;            // TRUE if we are to process the start stat - only used on firt INPROCESS message if we started with a start state.
    int               IntervalSS;        // Build a start state every X lines.
    int               SkipFirstLineSS;   // TRUE if we came in from a start state - first line is only partially decompressed so we must skip it.

    int               tlength;
    int               twidth;


    TABLEELEMENT      strtable[MAXCODE];   /* LZW string table                 */
    LZW_TILEPTR       pTile;
    //RSL_CONTEXTPTR    pCon;                /* Context pointer for Start state usage */
} CONTEXT_TRL, FAR * CONTEXT_TRLPTR;


static int  decode_lzw_strip(CONTEXT_TRLPTR c);
static int  getcode(CONTEXT_TRLPTR c);
//static void _SaveStartState(CONTEXT_TRLPTR c);



UNIV_CODEC_API char* DecodeLzw(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len)
{

    CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;

    c->insize = data_len;
    c->inbuf = (U8PTR)pData;
    //c->DataOffsetSS += in->numbytes;
    c->dcs.bitsleft += (U32)(c->insize * 8L);
    c->inpnt = 0;

    c->buffer = (U8*)out_buffer;

    decode_lzw_strip(c);

    *out_len = c->spos;

    return (char*)c->buffer;

}


//int EPOINT iTRL_OpenLZWtoRAW(ICL_PLPTR pl, S32 width, S32 length, S32 twidth, S32 tlength, int bitsperpixel, int lzwflags, RSL_CONTEXTPTR pCon)
UNIV_CODEC_API void* SetupLzwDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size)
{

    CONTEXT_TRLPTR c;
    //RSL_IDF        rslType = RSL_IDF_8BIT;
    int            bitsperpixel = pUvImage->numBits;
    int            oldbits = bitsperpixel;
    //int            idx;
    //U32            bufSize;
    int            bytespercolor = 1;
    LZW_TYPE       lzwflags = LZW_TYPE_None;
    int twidth, tlength;

    twidth = pUvImage->tileWidth;
    tlength = pUvImage->tileHeight;

    int bytesPerRow = 0;
    CalculateSize(pUvImage, outNumBits, out_size, &bytesPerRow);

    S32 linelen = twidth;

    if (bitsperpixel == 32)
    {
        linelen = linelen * 4;
        bitsperpixel = 8;
        bytespercolor = 4;
    }
    else if (bitsperpixel == 24)
    {
        linelen = linelen * 3;
        bitsperpixel = 8;
        bytespercolor = 3;
    }
    else if (bitsperpixel == 8)
    {
        linelen = linelen;
        bitsperpixel = 8;
        bytespercolor = 1;
    }
    else if (bitsperpixel == 1)
    {
        linelen = (linelen + 7) / 8;
        bitsperpixel = 8;
        bytespercolor = 1;
    }
    else if (bitsperpixel > 1 && bitsperpixel < 8)
    {
        //rslType = RSL_IDF_8BIT;
        if (lzwflags == LZW_TYPE_GIF) {
            linelen = linelen;
            bytespercolor = 1;
        }
        else {
            // Needed for TIFF case.
            linelen = ((linelen + 7 / bitsperpixel) * bitsperpixel) / 8;
            bitsperpixel = 8;
        }
    }
    else
    {
        return(nullptr);
    }

    c = (CONTEXT_TRLPTR)NEW CONTEXT_TRL;

    c->bytesperline = linelen;
    c->spos = 0;
    c->numheld = 0;
    c->nextcode = 0;
    c->maxcode = 0;
    c->oldcode = 0;
    c->bitsperpel = bitsperpixel + 1;
    c->clearcode = 1 << bitsperpixel;
    c->eodcode = c->clearcode + 1;
    c->dcs.codesize = c->bitsperpel;
    c->dcs.csmask = (1L << c->dcs.codesize) - 1L;
    c->dcs.bitsin = 0;
    c->dcs.bitsleft = 0L;
    c->bResumeAfterClear = FALSE;
    c->tlength = tlength;
    //c->pCon = pCon;

    c->bGIF = FALSE;
    if (lzwflags == LZW_TYPE_GIF)
    {
        c->bGIF = TRUE;
    }
    /* PMR 32291 - To decode cmyk images fully */
    c->bCMYK = FALSE;
    if (lzwflags == LZW_TYPE_CMYK)
    {
        c->bCMYK = TRUE;
    }

    //bufSize = (twidth * bytespercolor) * 3 / 2; // allow for a little overflow
    //// sometimes we have problems with small buffer sizes crashing 
    //if (bufSize < MIN_BUF_SIZE)
    //    bufSize = MIN_BUF_SIZE;

    //c->buffer = (U8PTR)NEW U8[bufSize];

    c->buffer = nullptr;

    if (lzwflags == LZW_TYPE_DiffEncoded) {
        if (oldbits == 32) {
            c->diffencoded = 4;/* This will also indicate that we are dealing with RGBA data. pmr 13160 (JackZ) Jan 5, 1999 */
        }
        else if (oldbits == 24) {
            c->diffencoded = 3;
        }
        else {
            c->diffencoded = 1;
        }
    }
    else {
        c->diffencoded = 0;
    }

    return c;
}

UNIV_CODEC_API void CleanupLzwDecoding(void *cp)
{
    if (cp)
        delete (CONTEXT_TRLPTR)cp;
}

// return: 1- success, 0 - fail, -1 - complete
static int decode_lzw_strip(CONTEXT_TRLPTR c) {


    int            code;
    S32            i;// , ct;
    S32            tbufoffset;
    //int            ret;
    char           curbyte = 0;

    if (c->bResumeAfterClear) {
        c->bResumeAfterClear = FALSE;
        goto RESUME_AFTER_CLEAR;
    }
    //if (c->FromSS) {
    //    c->FromSS = FALSE;
    //    c->SkipFirstLineSS = TRUE;
    //    code = c->clearcode;
    //    goto RESUME_AFTER_SS;
    //}
    /* initialize string table */
    for (;;)
    {

        code = getcode(c);

        if (code == c->eodcode) {

            /*
             * Force each strip of TIFF LZW data to start fresh.  Ignore this logic
             * if we are monolithic as data will be sent in several buffers by ReadDisk
             * - MarkG February 5, 1998
             */

            //if (!c->bGIF && c->bTile)
            {
                c->dcs.bitsleft = 0;
                c->dcs.bitsin = 0;
                c->dcs.codesize = c->bitsperpel;
                c->dcs.csmask = (1L << c->dcs.codesize) - 1L;
                c->numheld = 0;
                //if (c->tileperrow == 1)
                    c->spos = 0;
            }
            if (c->dcs.bitsleft < (U32)c->dcs.codesize) {
                /* We need memory return success */
                return 1;
            }
            /* We have more memory so treat this as strip */
            if (c->dcs.bitsin) {
                /* Advance input to next byte              */
                c->dcs.bitsleft -= 8 - c->dcs.bitsin;
                c->inpnt++;
                c->dcs.bitsin = 0;
            }
            c->dcs.codesize = c->bitsperpel;
            c->dcs.csmask = (1L << c->dcs.codesize) - 1L;
            code = getcode(c);

            if (code != c->clearcode) {
                return(-1);
            }
        }
        /* reset decompressor */
    //RESUME_AFTER_SS:
        if (code == c->clearcode)
        {
            //if (c->AddSS && (c->LineCtSS >= c->NextSS)) {
            //    _SaveStartState(c);
            //}

            c->nextcode = 0;
            while (c->nextcode < c->clearcode)
            {
                c->strtable[c->nextcode].ch = (char)c->nextcode;
                c->strtable[c->nextcode].count = 1;
                c->strtable[c->nextcode].prev = -1;
                c->nextcode++;
            }

            c->dcs.codesize = c->bitsperpel;
            c->dcs.csmask = (1L << c->dcs.codesize) - 1L;
            c->nextcode = c->clearcode + 2;
            c->maxcode = 1 << c->dcs.codesize;
            c->oldcode = 0;

        RESUME_AFTER_CLEAR:

            code = getcode(c);

            if (code == c->clearcode) {
                goto RESUME_AFTER_CLEAR;
            }
            if (code == c->eodcode) {
                if (!c->bGIF && c->numheld == 0 && c->dcs.bitsleft != 0) {
                    continue;
                }
                else {
                    c->bResumeAfterClear = TRUE;
                    return (1);
                }
            }
            /* write in string */
            c->buffer[c->spos++] = (char)code;

            c->oldcode = code;
        }
        else        /* not a clear code */
        {

            /* setup c->strtable for new string */
            c->strtable[c->nextcode].count = c->strtable[c->oldcode].count + 1;

            if (code < c->nextcode)       /* code is in table */
            {
                /* write string */
                i = code;
                c->spos += c->strtable[code].count;
                tbufoffset = c->spos - 1;

                while (i >= 0)
                {
                    curbyte = c->buffer[tbufoffset] = c->strtable[i].ch;
                    tbufoffset--;
                    i = c->strtable[i].prev;
                }
                /* add new string to table */
                c->strtable[c->nextcode].ch = curbyte;
                c->strtable[c->nextcode].prev = c->oldcode;
            }
            else if (code == c->nextcode)    /* code is next code */
            {
                /* make new code, add to string table and write it */
                i = c->oldcode;
                c->spos += c->strtable[c->nextcode].count;
                tbufoffset = c->spos - 2;

                while (i >= 0)
                {
                    curbyte = c->buffer[tbufoffset] = c->strtable[i].ch;
                    tbufoffset--;
                    i = c->strtable[i].prev;
                }

                c->strtable[c->nextcode].ch = curbyte;
                c->strtable[c->nextcode].prev = c->oldcode;
                c->buffer[c->spos - 1L] = curbyte;
            }
            else
                return(0);

            c->oldcode = code;
            c->nextcode++;
            if (c->bGIF) {
                if (c->nextcode == c->maxcode)
                {
                    if (c->dcs.codesize != 12)
                        c->dcs.codesize++;
                    c->dcs.csmask = (1L << c->dcs.codesize) - 1L;
                    c->maxcode <<= 1;
                }
            }
            else {
                if (c->nextcode == c->maxcode - 1)
                {
                    if (c->dcs.codesize != 12)
                        c->dcs.codesize++;
                    c->dcs.csmask = (1L << c->dcs.codesize) - 1L;
                    c->maxcode <<= 1;
                }
            }
        }        /* end if not a clear code */

    }
}

static int getcode(CONTEXT_TRLPTR c) {
    int  code, shift;
    U8PTR    ptr;
    U32 ultmp;

    ptr = (U8PTR)&ultmp;

    /* read from file if necessary */
    if (c->dcs.bitsleft < (U32)c->dcs.codesize) {
        if (c->dcs.bitsleft > 8L) {
            /* If c->numheld == 1 then do not overwrite this byte of data. The tile size must have been equal
               to 1 and since c->inpnt is likely to have shifted then you would read false data. PMR 59222 tremblr */
            if (c->numheld != 1)
                c->holdbuf[0] = c->inbuf[c->inpnt++];
            c->holdbuf[1] = c->inbuf[c->inpnt++];
            c->numheld = 2;
        }
        else if (c->dcs.bitsleft > 0L) {
            c->holdbuf[0] = c->inbuf[c->inpnt++];
            c->numheld = 1;
        }
        /* Not the best way to break out but this will work */
        return(c->eodcode);
    }

    if (c->bGIF) {
        shift = c->dcs.bitsin;
        if (c->numheld) {
            if (c->numheld == 2) {
                *ptr++ = c->holdbuf[0];
                *ptr++ = c->holdbuf[1];
                *ptr = c->inbuf[c->inpnt];
            }
            else {
                *ptr++ = c->holdbuf[0];
                *ptr++ = c->inbuf[c->inpnt];
                *ptr = c->inbuf[c->inpnt + 1];
            }
            c->inpnt -= c->numheld;
            c->numheld = 0;
        }
        else {
            *ptr++ = c->inbuf[c->inpnt];
            *ptr++ = c->inbuf[c->inpnt + 1];
            *ptr = c->inbuf[c->inpnt + 2];
        }
    }
    else {
        shift = 24 - c->dcs.codesize - c->dcs.bitsin;

        if (c->numheld) {
            if (c->numheld == 2) {
                *ptr++ = c->inbuf[c->inpnt];
                *ptr++ = c->holdbuf[1];
                *ptr = c->holdbuf[0];
            }
            else {
                *ptr++ = c->inbuf[c->inpnt + 1];
                *ptr++ = c->inbuf[c->inpnt];
                *ptr = c->holdbuf[0];
            }
            c->inpnt -= c->numheld;
            c->numheld = 0;
        }
        else {
            *ptr++ = c->inbuf[c->inpnt + 2];
            *ptr++ = c->inbuf[c->inpnt + 1];
            *ptr = c->inbuf[c->inpnt];
        }
    }



    ultmp >>= shift;
    code = (int)(ultmp & c->dcs.csmask);

    /* fix code reading values */

    c->dcs.bitsin += c->dcs.codesize;

    while (c->dcs.bitsin > 7)
    {
        c->dcs.bitsin -= 8;
        c->inpnt++;
    }
    c->dcs.bitsleft -= c->dcs.codesize;

    return(code);
}




//static void _SaveStartState(CONTEXT_TRLPTR c)
//{
//    STARTSTATE ss;
//    int        bytePos = 0;
//
//    ss.dcs = c->dcs;
//    ss.spos = c->spos;
//    ss.numheld = c->numheld;
//    ss.holdbuf[0] = c->holdbuf[0];
//    ss.holdbuf[1] = c->holdbuf[1];
//
//
//    bytePos = c->DataOffsetSS - c->insize + c->inpnt;
//
//    RSL_AddSS(c->pCon, 0, bytePos, c->LineCtSS + 1, 0, sizeof(STARTSTATE), (U8PTR)&ss);
//
//    c->NextSS += c->IntervalSS;
//}
//

