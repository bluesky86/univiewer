#include "stdafx.h"
#include "CUvImage.h"
#include "CUvGroup.h"
#include "DebugNew.h"
#include "Codec.h"


U8 gMaskValue[8] = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };
U8 gBitValue[8] = { 0x7F, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD, 0xFE };

#define STANDARD_PALETTE_VAL_DIF              51
#define STANDARD_COLOR_SIZE                   6
const U8 STANDARD_PALETTE[] = { 00, 51, 102, 153, 204, 255 };

U8 GetNearestStdColorIndex(S16 value)
{
    U8 retValue = 0;
    U8 pos = value / STANDARD_PALETTE_VAL_DIF;
    if ((value % STANDARD_PALETTE_VAL_DIF) == 0)
        retValue = pos;
    else
    {
        if (value - STANDARD_PALETTE[pos] > STANDARD_PALETTE[pos + 1] - value)
            retValue = pos + 1;
        else retValue = pos;
    }
    return retValue;
}

U8 GetNearestStdColorIndex(S16 red, S16 green, S16 blue)
{
    return GetNearestStdColorIndex(blue) * 36 + GetNearestStdColorIndex(green) * 6 + GetNearestStdColorIndex(red);
}

CUvColor* CreateStdPalette()
{
    U8 index = 0;
    CUvColor * pal = NEW CUvColor[216];
    memset(pal, 0, 216 * sizeof(CUvColor));
    for (U8 nBlue = 0; nBlue < STANDARD_COLOR_SIZE; nBlue++)
    {
        for (U8 nGreen = 0; nGreen < STANDARD_COLOR_SIZE; nGreen++)
        {
            for (U8 nRed = 0; nRed < STANDARD_COLOR_SIZE; nRed++)
            {
                pal[index].r = STANDARD_PALETTE[nRed];
                pal[index].g = STANDARD_PALETTE[nGreen];
                pal[index++].b = STANDARD_PALETTE[nBlue];
            }
        }
    }
    return pal;
}

void DeleteStdPalette(CUvColor * pal)
{
    delete[] pal;
}

// source image is 1-bit, 24-bit, or 32-bit
// output:  srcBitCount, new bits, byteWidth
UNIV_CODEC_API U8* convertTo8Bits(U8 &srcBitCount, CUvColor *palette, S32 &numCorlor, S32 width, S32 height, S32 &byteWidth, U8 *pBits, U8 *pNewBits, int newPads, bool bFreeOldBits)
{
    if (pBits == NULL) return NULL;
    if (srcBitCount == 8) return NULL;

    S32 newByteWidth = (S32)(width * (srcBitCount == 1 ? 8 : srcBitCount) + newPads - 1) / newPads * newPads / 8;

    if (!pNewBits) pNewBits = NEW U8[newByteWidth * height];
    S32 newPos, oldPos;
    U8 v;

    switch (srcBitCount)
    {
    case 1:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j;
                if (pBits[byteWidth*i + j / 8] & gMaskValue[j % 8])
                    v = 1;
                else
                    v = 0;
                if (palette)
                    pNewBits[newPos] = (palette[v].r + palette[v].g + palette[v].b) / 3;
                //pNewBits[newPos] = v;
                else
                    pNewBits[newPos] = v * 255;
            }
        }
        {
            CUvColor *pal = NEW CUvColor[256];
            for (int i = 0; i < 256; i++)
                pal[i].r = pal[i].g = pal[i].b = i;

            palette = pal;
            numCorlor = 256;
        }
        break;
    case 24:
        palette = CreateStdPalette();
        numCorlor = 216;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                oldPos = i * byteWidth + j * 3;
                newPos = i * newByteWidth + j;
                pNewBits[newPos] = GetNearestStdColorIndex(pBits[oldPos], pBits[oldPos + 1], pBits[oldPos]);
            }
        }
        break;
    case 32:
        palette = CreateStdPalette();
        numCorlor = 216;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                oldPos = i * byteWidth + j * 4;
                newPos = i * newByteWidth + j;
                pNewBits[newPos] = GetNearestStdColorIndex(pBits[oldPos], pBits[oldPos + 1], pBits[oldPos]);
            }
        }
        break;
    }

    if (bFreeOldBits) delete[] pBits;

    srcBitCount = 8;
    byteWidth = newByteWidth;

    return pNewBits;
}

// source image is 1-bit, 8-bit, 24-bit, or 32-bit
UNIV_CODEC_API U8* convertTo24Bits(U8 &srcBitCount, CUvColor *palette, S32 width, S32 height, S32 &byteWidth, U8 *pBits, U8 *pNewBits, int newPads, bool bFlipRGB, bool bFreeOldBits)
{
    if (pBits == NULL) return NULL;

    S32 newByteWidth = (S32)(width * 24 + newPads - 1) / newPads * newPads / 8;

    if (!pNewBits) pNewBits = NEW U8[newByteWidth * height];
    S32 newPos, oldPos;
    U8 v;

    switch (srcBitCount)
    {
    case 1:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j * 3;
                if (pBits[byteWidth*i + j / 8] & gMaskValue[j % 8])
                    v = 1;
                else
                    v = 0;
                if (palette)
                {
                    if (bFlipRGB)
                    {
                        pNewBits[newPos] = palette[v].b;
                        pNewBits[newPos + 1] = palette[v].g;
                        pNewBits[newPos + 2] = palette[v].r;
                    }
                    else
                    {
                        pNewBits[newPos] = palette[v].r;
                        pNewBits[newPos + 1] = palette[v].g;
                        pNewBits[newPos + 2] = palette[v].b;
                    }
                }
                else
                {
                    pNewBits[newPos] = v * 255;
                    pNewBits[newPos + 1] = v * 255;
                    pNewBits[newPos + 2] = v * 255;
                }
            }
        }
        break;
    case 8:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j * 3;
                oldPos = i * byteWidth + j;
                if (palette)
                {
                    if (bFlipRGB)
                    {
                        pNewBits[newPos] = palette[pBits[oldPos]].b;
                        pNewBits[newPos + 1] = palette[pBits[oldPos]].g;
                        pNewBits[newPos + 2] = palette[pBits[oldPos]].r;
                    }
                    else
                    {
                        pNewBits[newPos] = palette[pBits[oldPos]].r;
                        pNewBits[newPos + 1] = palette[pBits[oldPos]].b;
                        pNewBits[newPos + 2] = palette[pBits[oldPos]].b;
                    }
                }
                else
                {
                    pNewBits[newPos] = pBits[oldPos];
                    pNewBits[newPos + 1] = pBits[oldPos];
                    pNewBits[newPos + 2] = pBits[oldPos];
                }
            }
        }
        break;
    case 24:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j * 3;
                oldPos = i * byteWidth + j * 3;
                if (bFlipRGB)
                {
                    pNewBits[newPos] = pBits[oldPos + 2];
                    pNewBits[newPos + 1] = pBits[oldPos + 1];
                    pNewBits[newPos + 2] = pBits[oldPos];
                }
                else
                {
                    pNewBits[newPos] = pBits[oldPos];
                    pNewBits[newPos + 1] = pBits[oldPos + 1];
                    pNewBits[newPos + 2] = pBits[oldPos + 2];
                }
            }
        }
        break;
    case 32:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j * 3;
                oldPos = i * byteWidth + j * 4;
                if (bFlipRGB)
                {
                    pNewBits[newPos] = (U8)(pBits[oldPos + 2] * pBits[oldPos + 3] / 255.0 + (255 - pBits[oldPos + 3]));
                    pNewBits[newPos + 1] = (U8)(pBits[oldPos + 1] * pBits[oldPos + 3] / 255.0 + (255 - pBits[oldPos + 3]));
                    pNewBits[newPos + 2] = (U8)(pBits[oldPos] * pBits[oldPos + 3] / 255.0 + (255 - pBits[oldPos + 3]));
                }
                else
                {
                    pNewBits[newPos] = (U8)(pBits[oldPos] * pBits[oldPos + 3] / 255.0 + (255 - pBits[oldPos + 3]));
                    pNewBits[newPos + 1] = (U8)(pBits[oldPos + 1] * pBits[oldPos + 3] / 255.0 + (255 - pBits[oldPos + 3]));
                    pNewBits[newPos + 2] = (U8)(pBits[oldPos + 2] * pBits[oldPos + 3] / 255.0 + (255 - pBits[oldPos + 3]));
                }
            }
        }
        break;
    }

    if (bFreeOldBits) delete[] pBits;

    srcBitCount = 24;
    byteWidth = newByteWidth;

    return pNewBits;
}

// source image is 1-bit, 8-bit, or 24-bit
UNIV_CODEC_API U8* convertTo32Bits(U8 &srcBitCount, CUvColor *palette, S32 width, S32 height, S32 &byteWidth, U8 *pBits, U8 *pNewBits, int newPads, bool bFlipRGB, bool bFreeOldBits)
{
    if (pBits == NULL) return NULL;
    if (srcBitCount != 1 && srcBitCount != 8 && srcBitCount != 24) return NULL;

    S32 newByteWidth = (S32)(width * 32 + newPads - 1) / newPads * newPads / 8;

    if(!pNewBits) pNewBits = NEW U8[newByteWidth * height];
    S32 newPos, oldPos;
    U8 v;

    switch (srcBitCount)
    {
    case 1:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j * 4;
                if (pBits[byteWidth*i + j / 8] & gMaskValue[j % 8])
                    v = 1;
                else
                    v = 0;
                if (palette)
                {
                    if (bFlipRGB)
                    {
                        pNewBits[newPos] = palette[v].b;
                        pNewBits[newPos + 1] = palette[v].b;
                        pNewBits[newPos + 2] = palette[v].r;
                    }
                    else
                    {
                        pNewBits[newPos] = palette[v].r;
                        pNewBits[newPos + 1] = palette[v].g;
                        pNewBits[newPos + 2] = palette[v].b;
                    }
                }
                else
                {
                    pNewBits[newPos] = v * 255;
                    pNewBits[newPos + 1] = v * 255;
                    pNewBits[newPos + 2] = v * 255;
                }
                pNewBits[newPos + 3] = 255;
            }
        }
        break;
    case 8:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j * 4;
                oldPos = i * byteWidth + j;
                if (palette)
                {
                    if (bFlipRGB)
                    {
                        pNewBits[newPos] = palette[pBits[oldPos]].b;
                        pNewBits[newPos + 1] = palette[pBits[oldPos]].g;
                        pNewBits[newPos + 2] = palette[pBits[oldPos]].r;
                    }
                    else
                    {
                        pNewBits[newPos] = palette[pBits[oldPos]].r;
                        pNewBits[newPos + 1] = palette[pBits[oldPos]].g;
                        pNewBits[newPos + 2] = palette[pBits[oldPos]].b;
                    }
                }
                else
                {
                    pNewBits[newPos] = pBits[oldPos];
                    pNewBits[newPos + 1] = pBits[oldPos];
                    pNewBits[newPos + 2] = pBits[oldPos];
                }
                pNewBits[newPos + 3] = 255;
            }
        }
        break;
    case 24:
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                newPos = i * newByteWidth + j * 4;
                oldPos = i * byteWidth + j * 3;
                if (bFlipRGB)
                {
                    pNewBits[newPos] = pBits[oldPos + 2];
                    pNewBits[newPos + 1] = pBits[oldPos + 1];
                    pNewBits[newPos + 2] = pBits[oldPos];
                }
                else
                {
                    pNewBits[newPos] = pBits[oldPos];
                    pNewBits[newPos + 1] = pBits[oldPos + 1];
                    pNewBits[newPos + 2] = pBits[oldPos + 2];
                }
                pNewBits[newPos + 3] = 255;
            }
        }
        break;

    }

    if (bFreeOldBits) delete[] pBits;

    srcBitCount = 32;
    byteWidth = newByteWidth;

    return pNewBits;
}
