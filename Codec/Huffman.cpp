#include "stdafx.h"
#include "Codec.h"
#include "DebugNew.h"
#include "type.h"

void* TRL_OpenDCTtoMCU(JPGHEAD jpeghead, S32 width, S32 outBits);
int FAR TRL_TransDCTtoMCU(STDPTR ptr, U8PTR inData, int inSize, U8PTR outData);
void CloseDCTtoMCU(void *cp);
int TRL_TransTifComp7Read(JFIF_PARMPTR parmPtr, U8PTR inData, S32 in_numBytes);
int pl_SetDCTtoMCU(void** c_ptr, JPGHEAD jpeghead, S32 width);

#define  CHECK_BYTES_SIZE   4
#define  BLOCKLENGTH        8
#define  COMP_BLOCKSIZE     64
#define  BLK_SIZE           (COMP_BLOCKSIZE * sizeof(int))

#define B32BIT_SHIFT
#define STACK FAR

#define IN_BUFF_SIZE  100000

static int bit_tbl[17] = {
   0x0000, 0x0001, 0x0003, 0x0007, 0x000F, 0x001F, 0x003F, 0x007F,
   0x00FF, 0x01FF, 0x03FF, 0x07FF, 0x0FFF, 0x1FFF, 0x3FFF, 0x7FFF,
   0xFFFF
};


int _pil_zig_zag[]
= {
   0,1,8,16,9,2,3,10,17,24,32,25,18,11,4,5,
    12,19,26,33,40,48,41,34,27,20,13,6,7,14,
    21,28,35,42,49,56,57,50,43,36,29,22,15,
    23,30,37,44,51,58,59,52,45,38,31,39,46,
    53,60,61,54,47,55,62,63,
    /* 29Oct97(Brian) Add 16 entries for overrun */
    63,63,63,63,63,63,63,63,63,63,63,63,63,63,63,63
};

/* These values are also in dct2smpl, although not a accurate */
/* FCx = cos(x * PI / 16) - 0 is 1/2                          */
/* We keep 6bits of precision and FCx is to be divided by 2   */
/* so FC is 64 / 4 - the bit shift to integer is done in      */
/* dct2smpl in the dct_fast_fast fn.                          */
#define  FC     16
#define  FC0    0.7071068
#define  FC1    0.9807853
#define  FC2    0.9238796
#define  FC3    0.8314697
#define  FC4    0.7071068
#define  FC5    0.5555703
#define  FC6    0.3826835
#define  FC7    0.1950904
static   DOUBLE a_mult[8][8] = {
   (DOUBLE)(FC0*FC0*FC), (DOUBLE)(FC0*FC1*FC), (DOUBLE)(FC0*FC2*FC), (DOUBLE)(FC0*FC3*FC), (DOUBLE)(FC0*FC4*FC), (DOUBLE)(FC0*FC5*FC), (DOUBLE)(FC0*FC6*FC), (DOUBLE)(FC0*FC7*FC),
   (DOUBLE)(FC1*FC0*FC), (DOUBLE)(FC1*FC1*FC), (DOUBLE)(FC1*FC2*FC), (DOUBLE)(FC1*FC3*FC), (DOUBLE)(FC1*FC4*FC), (DOUBLE)(FC1*FC5*FC), (DOUBLE)(FC1*FC6*FC), (DOUBLE)(FC1*FC7*FC),
   (DOUBLE)(FC2*FC0*FC), (DOUBLE)(FC2*FC1*FC), (DOUBLE)(FC2*FC2*FC), (DOUBLE)(FC2*FC3*FC), (DOUBLE)(FC2*FC4*FC), (DOUBLE)(FC2*FC5*FC), (DOUBLE)(FC2*FC6*FC), (DOUBLE)(FC2*FC7*FC),
   (DOUBLE)(FC3*FC0*FC), (DOUBLE)(FC3*FC1*FC), (DOUBLE)(FC3*FC2*FC), (DOUBLE)(FC3*FC3*FC), (DOUBLE)(FC3*FC4*FC), (DOUBLE)(FC3*FC5*FC), (DOUBLE)(FC3*FC6*FC), (DOUBLE)(FC3*FC7*FC),
   (DOUBLE)(FC4*FC0*FC), (DOUBLE)(FC4*FC1*FC), (DOUBLE)(FC4*FC2*FC), (DOUBLE)(FC4*FC3*FC), (DOUBLE)(FC4*FC4*FC), (DOUBLE)(FC4*FC5*FC), (DOUBLE)(FC4*FC6*FC), (DOUBLE)(FC4*FC7*FC),
   (DOUBLE)(FC5*FC0*FC), (DOUBLE)(FC5*FC1*FC), (DOUBLE)(FC5*FC2*FC), (DOUBLE)(FC5*FC3*FC), (DOUBLE)(FC5*FC4*FC), (DOUBLE)(FC5*FC5*FC), (DOUBLE)(FC5*FC6*FC), (DOUBLE)(FC5*FC7*FC),
   (DOUBLE)(FC6*FC0*FC), (DOUBLE)(FC6*FC1*FC), (DOUBLE)(FC6*FC2*FC), (DOUBLE)(FC6*FC3*FC), (DOUBLE)(FC6*FC4*FC), (DOUBLE)(FC6*FC5*FC), (DOUBLE)(FC6*FC6*FC), (DOUBLE)(FC6*FC7*FC),
   (DOUBLE)(FC7*FC0*FC), (DOUBLE)(FC7*FC1*FC), (DOUBLE)(FC7*FC2*FC), (DOUBLE)(FC7*FC3*FC), (DOUBLE)(FC7*FC4*FC), (DOUBLE)(FC7*FC5*FC), (DOUBLE)(FC7*FC6*FC), (DOUBLE)(FC7*FC7*FC)
};


/* Watch word/byte order closely ! */
typedef union _inbits {
    S32      z;
    U16      w[2];
    U8       b[4];
} INBITS;
/* Added WORD/BYTE defines for UNIX Platform (22Mar99,BrianH) */
#define WORD_0 0
#define WORD_1 1
#define BYTE_0 0
#define BYTE_1 1
#define BYTE_2 2
#define BYTE_3 3


/* MCU-- Minimum coded units */

typedef struct _context_huffman {
    INBITS        inbits;            /* --Keeping next bit condition after Huffman decoding */
    S8PTR         inptr;             /* Indata pointer                   */
    INTPTR        pOut_Y;            /* Buffer for Y component    */
    INTPTR        pOut_U;            /* Buffer for U component    */
    INTPTR        pOut_V;            /* Buffer for V component    */
    int           extraBytesSize;    /* -Used to hold extra bytes which are not enough to decode one MCU      */
    int           extraBytesIndex;   /* -After geting another chuck data, copy these bytes in front of indata */
    int           startWidth;        /* -Memory width where to break                       */
    int           bits_in;           /* --Keeping NO of bits which are left */
    int           bytesCnt;          /* Count bytes                      */
    int           numbytes;          /* Number bytes of Indata           */
    int           lineCt;            /* count lines               */
    int           outLinNum;         /* Total of lines            */
    int           width;
    int           outputSize;        /* Size (in bytes) of output buffer size */
    int           components;
    int           restart;
    int           dri;
    int           inbuffindex;
    int           mcuLength;         /* Length & Width of an MCU Block */
    int           mcuWidth;
    //int           skipX1;            /* Skip MCU's before this value */
    //int           skipX2;            /* Skip MCU's after this value */
    //int           skipY1;            /* Skip MCU's before this line */
    //S32           accumByteCnt;
    //IM_BOOL       AddSS;             /* TRUE if Adding Start States         */
    IM_BOOL       bEndFound;         /* EOF is found --- 0xFFD9 for JFIF */
    IM_BOOL       bGetNewDataIn;     /* Require to read New data in      */
    IM_BOOL       bStripped;         /* Stipped data, each new buffer is start of new block */
    //RSL_CONTEXTPTR pCon;             /* Context ptr, needed for Start States*/
    JFIF_PARMPTR  globalParmPtr;
    JFIF_PARM     jfifParm;
    //struct _sdb FAR *cursdb;
    JPGHEAD       jpeghead;
    JPGPRMS       y_params;          /* params for Y component           */
    JPGPRMS       u_params;          /* params for U component           */
    JPGPRMS       v_params;          /* params for V component           */
    JPGPRMS       k_params;          /* params for V component           */
    U8            inbuff[IN_BUFF_SIZE];
    IM_BOOL       tiffJpeg7;
    U8PTR         outData;
    void          *dct;
} CONTEXT_TRL, FAR * CONTEXT_TRLPTR;

int  _setComParm(CONTEXT_TRLPTR c, JFIF_PARMPTR parmPtr);
int  jpeg_getbits(int n, int STACK *getBits, CONTEXT_TRLPTR c);

int  jpeg_new_decode(int STACK *run, int STACK *val, JPGHUFFDECODE STACK *huff, CONTEXT_TRLPTR c, unsigned indataleft, unsigned adobe_jpg);
int  jpeg_loadbits8(CONTEXT_TRLPTR c);



//int FAR TRL_TransHUFFMANtoDCT(ICL_PLPTR pl, CONTEXT_TRLPTR c, int state)
// input decoding struct
UNIV_CODEC_API char* DecodeJpeg(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len)
{
    CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;

    INBITS      tmp_inbits;
    int         tmp_index = 0;
    int         retDecode = SUCCESS, ret = SUCCESS;
    int         tmp_bits_in = 0;
    int         dc, ac;
    int         run, i;
    S16         tmp_ydc = 0, tmp_udc = 0, tmp_vdc = 0, tmp_kdc = 0;
    int         width, w, startWidth, tmp_width = 0;
    int         color, components;
    int         m, n;
    U8PTR       indatacur;
    unsigned    indataleft;
    INTPTR      strip = NULL;
    INTPTR      x;
    JPGPRMS STACK *prms = NULL;
    JPGPRMS     y_params = c->y_params;
    JPGPRMS     u_params = c->u_params;
    JPGPRMS     v_params = c->v_params;
    JPGPRMS     k_params = c->k_params;
    JPGHEAD     jpeghead = c->jpeghead;

    width = c->width;
    tmp_inbits.z = 0;
    c->bEndFound = FALSE;
    components = c->components;
    *out_len = 0;// c->outputSize;

    if (c->bStripped) 
    {
        int offset = TRL_TransTifComp7Read(&c->jfifParm, (U8PTR)pData, data_len);
        if (offset > 0)
        {
            memcpy(&c->jpeghead, &c->jfifParm.jpeghead, sizeof(JPGHEAD));
            _setComParm(c, &c->jfifParm);
            pl_SetDCTtoMCU(&c->dct, c->jpeghead, width);
        }

        indatacur = (U8PTR)pData + offset;
        indataleft = (unsigned)data_len - offset;

        // Reset input indicators, start next block as if new image.
        c->extraBytesSize = 0;
        c->extraBytesIndex = 0;
        c->startWidth = 0;
        c->bits_in = 0;
        c->y_params.dc_predict = 0;
        c->u_params.dc_predict = 0;
        c->v_params.dc_predict = 0;
        c->k_params.dc_predict = 0;
        c->inbits.z = 0;

        c->lineCt = 0;
    }
    else
    {
        indatacur = (U8PTR)pData;
        indataleft = (unsigned)data_len;
    }

    y_params = c->y_params;
    u_params = c->u_params;
    v_params = c->v_params;
    k_params = c->k_params;

    while (indataleft > 0) {
        startWidth = c->startWidth;
        c->numbytes = (int)(min((indataleft + c->extraBytesSize), IN_BUFF_SIZE));
        /* Sept. 13/0 Peter Mage - ensure extraBytesSize > 0 before doing the MEMCPY, this fixes PMR 43715 */
        if ((c->extraBytesSize > 0) && c->extraBytesIndex) {
            memcpy(c->inbuff, c->inbuff + c->extraBytesIndex, c->extraBytesSize);
        }
        memcpy(c->inbuff + c->extraBytesSize, indatacur, c->numbytes - c->extraBytesSize);
        indatacur += c->numbytes - c->extraBytesSize;
        indataleft -= c->numbytes - c->extraBytesSize;
        /* Reset stuff to allow us to continue */
        //c->accumByteCnt += c->extraBytesIndex;
        c->extraBytesSize = 0;
        c->extraBytesIndex = 0;
        c->inbuffindex = 0;
        tmp_index = 0;
        c->bGetNewDataIn = FALSE;
        c->bytesCnt = 0;
        c->bEndFound = FALSE;
        if (c->bStripped) {
            c->bEndFound = TRUE; // Stripped data always ends at end of buffer even if no EOF found.
        }
        // DCS-5803: some jpeg file (pdf embeded image) puts "/r/n" at end of file after the End of Image(EOI), we need to tolerate it
        //           Otherwise, the image will be partly displayed
        for (i = 0; i < 10 && i < c->numbytes; i++)
        {
            if (((U8)c->inbuff[c->numbytes - i - 1] == 0x0D) || ((U8)c->inbuff[c->numbytes - i - 1] == 0x0A))
                continue;
            if (((U8)c->inbuff[c->numbytes - i - 2] == 0xFF) && ((U8)c->inbuff[c->numbytes - i - 1] == 0xD9)) {
                c->bEndFound = TRUE;
                break;
            }
        }

        while (c->bGetNewDataIn != TRUE) {
            if (c->lineCt >= c->outLinNum)
                break;
            strip = (INTPTR)c->outData;
            for (w = startWidth; w < width; w += c->mcuWidth) {

                x = strip;
                // Clear all output block here, cuts overhead
                memset(x, 0, c->outputSize);
                for (color = 0; color < components; color++) {
                    if (color == 0) {
                        prms = (JPGPRMS STACK *)&y_params;
                    }
                    else if (color == 1) {
                        prms = (JPGPRMS STACK *)&u_params;
                    }
                    else if (color == 2) {
                        prms = (JPGPRMS STACK *)&v_params;
                    }
                    else if (color == 3) {
                        prms = (JPGPRMS STACK *)&k_params;
                    }

                    {
                        /* do an mcu - number of 8 x 8 blocks in a component (color) */
                        for (n = 1; n <= (int)prms->mcu_v; n++) {
                            for (m = 0; m < (int)prms->mcu_h; m++) {
                                retDecode = jpeg_new_decode((int STACK *)&run, (int STACK *)&dc, &prms->huffdc, c, indataleft, jpeghead.adobe_jpeg);   /* decode dc coefficient */
                                if (retDecode != SUCCESS)
                                    goto END;
                                prms->dc_predict = (S16)(prms->dc_predict + dc);
                                if (prms->dc_predict != 0) {
                                    x[0] = (int)((prms->dc_predict * prms->quantbl2[0]));
                                }
                                for (i = 1; i < COMP_BLOCKSIZE; i++) {
                                    if (c->bits_in >= 8) {       /* 6Nov97(Brian) - Do the quick test to grab a code here */
                                        INBITS hcodes;                              /* This should save time by less decode calls */
                                        hcodes.z = prms->huffac.tbl[c->inbits.b[BYTE_1]];
                                        if (hcodes.b[0] == 255) {
                                            /* we have a code and value */
                                            run = hcodes.b[2];
                                            ac = (S8)hcodes.b[3];
                                            c->inbits.z <<= hcodes.b[1];
                                            c->bits_in -= hcodes.b[1];
                                            goto NEXT1;
                                        }
                                    }
                                    retDecode = jpeg_new_decode((int STACK *)&run, (int STACK *)&ac, &prms->huffac, c, indataleft, jpeghead.adobe_jpeg);  /* decode ac coefficients */
                                    if (retDecode != SUCCESS)
                                        goto END;
NEXT1:
                                    if (ac == 0) {
                                        if (run == 0)
                                            break;        /* run == 0, ac ==0 --> end of block marker */
                                        i += run;
                                    }
                                    else {
                                        /* 29Oct97(Brian) At this point i may be >= COMP_BLOCKSIZE, for optimization     *
                                            *                I added 16 entries to zig_zag and quantbl2 which all map to 63 */
                                        i += run;
                                        x[_pil_zig_zag[i]] = (int)((ac * prms->quantbl2[i]));
                                    }
                                } /* for i */
                                x += COMP_BLOCKSIZE;
                            }  /* for n */
                        }  /* for m -- end of mcu */

                    }

                } /* end of color loop */

                if (c->dri) {
                    c->restart++;              /* check restart interval */
                    if (c->restart >= c->dri) {
                        int  code, bits;
                        jpeg_getbits(16, (int STACK *)&bits, c);
                        while ((bits & 0xFFF0) != 0xFFD0) {
                            if (c->bits_in > 0) {        /* enough bits available already */
                                c->inbits.z <<= 1;
                                c->bits_in -= 1;
                                code = c->inbits.w[WORD_1];
                            }
                            else {
                                /* Should check return value, if it is Failure, we need more data */
                                /* So, we need to break out and get more data --April 26, 2000    */
                                if ((jpeg_getbits(1, (int STACK *)&code, c)) == FAILURE)
                                    break;
                                if (c->bytesCnt > c->numbytes)
                                    break;
                            }
                            bits = (int)((bits << 1) | (code & 1));
                        }
                        c->restart = 0;
                        y_params.dc_predict = 0;
                        u_params.dc_predict = 0;
                        v_params.dc_predict = 0;
                        k_params.dc_predict = 0;
                    }
                } /* if c->dri */

                //{
                //    FILE *fp;
                //    fopen_s(&fp, "F:\\Test\\tmp1\\univ.dat", "ab");
                //    fwrite(c->outData, 1, c->outputSize, fp);
                //    fclose(fp);
                //}

                *out_len += TRL_TransDCTtoMCU(c->dct, c->outData, c->outputSize, (U8PTR)&out_buffer[*out_len]);

                if (c->bEndFound == FALSE) {         /* After decodine one MCU, keep this info in case that      */
                    tmp_index = c->inbuffindex;       /* the rest of bytes are too small to decode next MCU, then */
                    tmp_bits_in = c->bits_in;           /* when reading in another chuck of data, copy back and     */
                    tmp_ydc = y_params.dc_predict;  /* restart from the beginning of the MCU                    */
                    tmp_udc = u_params.dc_predict;
                    tmp_vdc = v_params.dc_predict;
                    tmp_kdc = k_params.dc_predict;
                    tmp_width = w + c->mcuWidth;      /* Fool startWidth to next MCU */
                    if (tmp_width >= width)
                        tmp_width = 0;                   /* Wrap to beginning of next line */
                    tmp_inbits = c->inbits;
                }
            } /* for w < width */
            startWidth = 0;
            c->lineCt += 1;
            //if (c->AddSS && (c->lineCt & 3) == 0) {
            //    // Build S.S. every 2nd MCU Block.
            //    // Changed to every 4th MCU Block since MCU clipping introduced
            //    S32 lineCt = c->lineCt * c->mcuLength;
            //    S32 byteCt = c->inbuffindex + c->accumByteCnt;
            //    S32 val[5];
            //    val[0] = c->inbits.z;
            //    val[1] = y_params.dc_predict;
            //    val[2] = u_params.dc_predict;
            //    val[3] = v_params.dc_predict;
            //    val[4] = k_params.dc_predict;
            //    RSL_AddSS(c->pCon, 0, byteCt, lineCt, c->bits_in, 32, (U8PTR)val);
            //}
        }
END:
        if (retDecode != SUCCESS) {             /* Copying info for decoding next MCU */
            c->inbits = tmp_inbits;
            c->bits_in = tmp_bits_in;
            c->extraBytesSize = (int)(c->numbytes - tmp_index);
            c->extraBytesIndex = tmp_index;
            c->y_params.dc_predict = (S16)tmp_ydc;
            c->u_params.dc_predict = (S16)tmp_udc;
            c->v_params.dc_predict = (S16)tmp_vdc;
            c->k_params.dc_predict = (S16)tmp_kdc;
            y_params.dc_predict = (S16)tmp_ydc;
            u_params.dc_predict = (S16)tmp_udc;
            v_params.dc_predict = (S16)tmp_vdc;
            k_params.dc_predict = (S16)tmp_kdc;
            c->startWidth = tmp_width;
        }
        else {
            /* Need to copy the decoding info for MCU if we are in the middle of a file. Aug20,02 tremblr */
            if (c->lineCt != c->outLinNum) {
                c->inbits = tmp_inbits;
                c->bits_in = tmp_bits_in;
                c->extraBytesSize = (int)(c->numbytes - tmp_index);
                c->extraBytesIndex = tmp_index;
                c->y_params.dc_predict = (S16)tmp_ydc;
                c->u_params.dc_predict = (S16)tmp_udc;
                c->v_params.dc_predict = (S16)tmp_vdc;
                c->k_params.dc_predict = (S16)tmp_kdc;
                y_params.dc_predict = (S16)tmp_ydc;
                u_params.dc_predict = (S16)tmp_udc;
                v_params.dc_predict = (S16)tmp_vdc;
                k_params.dc_predict = (S16)tmp_kdc;
            }
            c->startWidth = 0;
        }
    } /* While indataleft */

    return out_buffer;

} 

// return: decoding struct
UNIV_CODEC_API void* SetupJpegDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size)
{
    int         ret;
    CONTEXT_TRLPTR  c = NEW CONTEXT_TRL;

    memset(c, 0, sizeof(CONTEXT_TRL));

    c->globalParmPtr = pUvImage->jpegParams;

    memcpy(&c->jfifParm, pUvImage->jpegParams, sizeof(JFIF_PARM));


    JPGHEAD     jpeghead = c->jfifParm.jpeghead;

    memcpy(&c->jpeghead, &c->jfifParm.jpeghead, sizeof(JPGHEAD));

    c->bits_in = 0;
    c->lineCt = 0;
    c->extraBytesSize = 0;
    c->restart = 0;
    c->startWidth = 0;
    c->dri = (int)c->jfifParm.dri;
    c->width = jpeghead.width;
    c->components = jpeghead.components;
    //c->pCon = pCon;
    //c->AddSS = FALSE;
    //c->accumByteCnt = 0;
    c->bStripped = (pUvImage->encodingType == uvImageEncoding_SJPEG);

    memset(&c->inbits, 0, sizeof(INBITS));

    c->mcuWidth = BLOCKLENGTH * jpeghead.mcu_h1;
    c->mcuLength = BLOCKLENGTH * jpeghead.mcu_v1;

    c->outLinNum = ((jpeghead.height + c->mcuLength - 1) / c->mcuLength) * c->mcuLength;
    c->outLinNum = c->outLinNum / c->mcuLength; //+ jpeghead.mcu_v1;

    /* make sure width is a multiple of an MCU block */
    c->width = ((c->width + c->mcuWidth - 1) / c->mcuWidth) * c->mcuWidth;

    /* Calculate number of MCU blocks to skip processing (zoom in view) */
    //if (pCompReq && pCompGet) {
    //    c->width = (int)(pCompGet->x2 - pCompGet->x1 + 1);
    //    c->width = ((c->width + c->mcuWidth - 1) / c->mcuWidth) * c->mcuWidth;
    //    if (pCompReq->x1 < pCompGet->x1) pCompReq->x1 = pCompGet->x1;
    //    if (pCompReq->y1 < pCompGet->y1) pCompReq->y1 = pCompGet->y1;
    //    if (pCompReq->x2 > pCompGet->x2) pCompReq->x2 = pCompGet->x2;
    //    if (pCompReq->y2 > pCompGet->y2) pCompReq->y2 = pCompGet->y2;
    //    c->skipX1 = (int)(pCompReq->x1 - pCompGet->x1) / c->mcuWidth * c->mcuWidth;
    //    c->skipX2 = (int)(pCompReq->x2 - pCompGet->x1) / c->mcuWidth * c->mcuWidth;
    //    c->skipY1 = (int)pCompReq->y1 / c->mcuLength;
    //    c->lineCt = (int)pCompGet->y1 / c->mcuLength;
    //    pCompGet->x2 = pCompGet->x1 + c->skipX2 + c->mcuWidth - 1;
    //    pCompGet->x1 = pCompGet->x1 + c->skipX1;
    //    pCompGet->y1 = (MPL_COMPCOORD)(c->skipY1 * c->mcuLength);
    //    if (c->dri && c->lineCt > 0) // Set the restart level
    //        c->restart = ((c->width / c->mcuWidth) * c->lineCt) % c->dri;
    //}
    //else {
        //c->skipX1 = 0;
        //c->skipX2 = c->width;
        //c->skipY1 = 0;
    //}

    ret = _setComParm(c, &c->jfifParm);

    /* the size for output is needed to calculate */
    if (c->components == 3) {
        c->outputSize = (unsigned)(BLK_SIZE * (jpeghead.mcu_h1 * jpeghead.mcu_v1 +
            jpeghead.mcu_h2 * jpeghead.mcu_v2 +
            jpeghead.mcu_h3 * jpeghead.mcu_v3));
    }
    else if (c->components == 4) {  /* <Mar13,2009 HocH> CMYK support */
        c->outputSize = (unsigned)(BLK_SIZE * (jpeghead.mcu_h1 * jpeghead.mcu_v1 +
            jpeghead.mcu_h2 * jpeghead.mcu_v2 +
            jpeghead.mcu_h3 * jpeghead.mcu_v3 +
            jpeghead.mcu_h4 * jpeghead.mcu_v4));
    }
    else {
        c->outputSize = (unsigned)(BLK_SIZE * jpeghead.mcu_h1 * jpeghead.mcu_v1);
    }

    //// Added usage of Start States
    //if (pCon && (RSL_GetModeCon(pCon) & RSL_MODE_ADDSS)) {
    //    c->AddSS = TRUE;
    //}
    //if (pCon && RSL_GetUsingSSCon(c->pCon, 0)) {
    //    S32 val[5];
    //    c->bits_in = RSL_GetSSBitCon(c->pCon, 0);
    //    c->accumByteCnt = RSL_GetSSByteOffset(c->pCon, 0);
    //    MEMCPY(val, RSL_GetSSDataCon(c->pCon, 0), 20);
    //    c->inbits.z = val[0];
    //    c->y_params.dc_predict = (S16)val[1];
    //    c->u_params.dc_predict = (S16)val[2];
    //    c->v_params.dc_predict = (S16)val[3];
    //    c->k_params.dc_predict = (S16)val[4];
    //    val[0] = 0;
    //}

    c->outData = NEW U8[c->outputSize * 2];

    int bytesPerRow = 0;
    CalculateSize(pUvImage, outNumBits, out_size, &bytesPerRow);

    c->dct = TRL_OpenDCTtoMCU(jpeghead, c->width, *outNumBits);


    return(c);

}



////////////////////////////////////////////////////////
//  @Name     : _buildHuffTbl
//  @Class    : Internal
//  @Synopsis : Build the huffman table
////////////////////////////////////////////////////////
int _buildHuffTbl(JPGHUFFDECODE FAR *huff) {

    INBITS val;
    int    i, j, code, nold, bits, n, nb, nc;
    int    bits_used;

    for (i = 0; i < 256; i++) {
        j = 0;
        code = 0;
        nold = 0;
        bits_used = 0;
        for (n = 1; n <= 8; n++) {  /* search through code lengths */
            nc = huff->ncodes[n - 1];
            if (!nc)
                continue;                          /* no codes of this length */
            nb = (int)(n - nold);
            bits_used += nb;

            bits = i >> (8 - n);
            bits &= bit_tbl[nb];

            code = (int)((code << nb) | bits);
            if (code < nc)
                break;                             /* found a word */
            code = (int)(code - nc);              /* not found */
            nold = n;
            j = (int)(j + nc);
        }

        if (n == 9) {
            /* Code not found, save state */
            val.b[0] = (U8)(nold + (bits_used << 4));
            val.b[1] = (U8)j;
            val.w[1] = (U16)code;
        }
        else {
            /* Code found */
            val.b[0] = (U8)254; /* CODE */
            val.b[1] = (U8)(n); /* numbits to back up by */
            val.b[2] = (U8)(huff->val[j + code] >> 4); /* run value */
            val.b[3] = (U8)(huff->val[j + code] & 0xF); /* num bits in next code value */

            n = val.b[3];
            if (val.b[1] + n <= 8) {
                val.b[0] = (U8)255;
                val.b[1] = (U8)(val.b[1] + n);
                if (n) {
                    code = (i >> (8 - val.b[1])) & bit_tbl[n];
                    if (code & (1 << (n - 1)))                       /* look at top bit */
                        val.b[3] = (U8)code;                      /* positive */
                    else
                        val.b[3] = (U8)((code + 1) | ((-1) << n));  /* negative */
                }
            }
        }
        huff->tbl[i] = val.z;

    } /* for i */

    return(SUCCESS);
}


////////////////////////////////////////////////////////
//  @Name     : _setComParm
//  @Class    : Internal
//  @Synopsis : Set up three structure for DC predict, quan TBL, DC, AC TBL
////////////////////////////////////////////////////////
int _setComParm(CONTEXT_TRLPTR  c, JFIF_PARMPTR parmPtr)
{
    int            comp, i;
    int            dc_ac = 0;
    int            quant_table = 0;
    JPGPRMSPTR     params = NULL;
    JPGSCANPTR     jpgscan = &parmPtr->jpgscan;
    JPGHEAD        jpeghead = parmPtr->jpeghead;

    DOUBLEPTR      b = &a_mult[0][0];


    for (comp = 0; comp < jpgscan->components; comp++) {
        switch (comp) {
        case 0:
            params = &c->y_params;  /* set luma parameters */
            params->mcu_h = jpeghead.mcu_h1;
            params->mcu_v = jpeghead.mcu_v1;
            dc_ac = jpgscan->dc_ac1;
            quant_table = parmPtr->quant_table1;
            break;
        case 1:
            params = &c->u_params;  /* set U chroma parameters */
            params->mcu_h = jpeghead.mcu_h2;
            params->mcu_v = jpeghead.mcu_v2;
            dc_ac = jpgscan->dc_ac2;
            quant_table = parmPtr->quant_table2;
            break;
        case 2:
            params = &c->v_params;  /* set V chroma parameters */
            params->mcu_h = jpeghead.mcu_h3;
            params->mcu_v = jpeghead.mcu_v3;
            dc_ac = jpgscan->dc_ac3;
            quant_table = parmPtr->quant_table3;
            break;
        case 3:
            params = &c->k_params;  /* set K chroma parameters */
            params->mcu_h = jpeghead.mcu_h4;
            params->mcu_v = jpeghead.mcu_v4;
            dc_ac = jpgscan->dc_ac4;
            quant_table = parmPtr->quant_table4;
            break;
        }

        params->dc_predict = 0;

        memcpy(params->quantbl, &parmPtr->quant[quant_table][0], sizeof(U16) * COMP_BLOCKSIZE);
        if (((dc_ac >> 4) & 0xF) == 0) {    /* DC table Id */
            params->huffdc = parmPtr->huffdc0;
        }
        else if (((dc_ac >> 4) & 0xF) == 1) {
            params->huffdc = parmPtr->huffdc1;
        }
        else if (((dc_ac >> 4) & 0xF) == 2) { // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
            params->huffdc = parmPtr->huffdc2;
        }
        else { // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
            params->huffdc = parmPtr->huffdc3;
        }
        if ((dc_ac & 0xF) == 0) {             /* AC table Id  */
            params->huffac = parmPtr->huffac0;
        }
        else if ((dc_ac & 0xF) == 1) {
            params->huffac = parmPtr->huffac1;
        }
        else if ((dc_ac & 0xF) == 2) { // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
            params->huffac = parmPtr->huffac2;
        }
        else { // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
            params->huffac = parmPtr->huffac3;
        }

        // DCS-9161: the white is not real white, I have no idea except the round or truncate issue between double to integer
        // here, the b[_pil_zig_zag[0]] is 48.00000255419905, if it is rounded to 48, the white color becomes 0xFE, if rounded
        // to 49, the white color beomes 0xFF. Not sure if it is correct to do so. But I tested all the jpeg image in M:/project
        // it seems nothing wrong.
        for (i = 0; i < COMP_BLOCKSIZE; i++) {
            DOUBLE vd = params->quantbl[i] * b[_pil_zig_zag[i]];
            S32 v32 = (S32)vd;
            if (v32 < vd) v32++;
            params->quantbl2[i] = v32;
        }
        /* 29Oct97(Brian) Add 16 entries for overrun */
        while (i < COMP_BLOCKSIZE + 16) {
            params->quantbl2[i++] = params->quantbl2[COMP_BLOCKSIZE - 1];
        }
        _buildHuffTbl(&params->huffdc);
        _buildHuffTbl(&params->huffac);
    }

    return(SUCCESS);
}

int jpeg_new_decode(int STACK *run, int STACK *val, JPGHUFFDECODE STACK *huff, CONTEXT_TRLPTR  c, unsigned indataleft, unsigned adobe_jpg)
{
    int     j, n, nc, nold, code, nb, bits;
    int     ret = SUCCESS;
    INBITS  hcodes;

    if (c->bEndFound == TRUE) {
        if (c->bytesCnt > c->numbytes)
            return(SUCCESS);
    }

    if (c->bits_in < 8) {        /* not enough bits available, get more */
        if ((ret = jpeg_loadbits8(c)) != SUCCESS) {
            if (adobe_jpg == ADOBE_JPEG) {
                if (indataleft > CHECK_BYTES_SIZE || c->bytesCnt > c->numbytes)
                    return(ret);
                else
                    ret = SUCCESS;
            }
            else
                return(ret);
        }
    }
    bits = c->inbits.b[BYTE_1];


    hcodes.z = huff->tbl[bits];
    if (hcodes.b[0] == 255) {
        /* we have a code and value */
        *run = hcodes.b[2];
        *val = (S8)hcodes.b[3];
        c->inbits.z <<= hcodes.b[1];
        c->bits_in -= hcodes.b[1];
    }
    else {
        if (hcodes.b[0] == 254) {
            /* we have a code!, no value */
            *run = hcodes.b[2];
            n = hcodes.b[3];
            c->inbits.z <<= hcodes.b[1];
            c->bits_in -= hcodes.b[1];
        }
        else {
            /* 19May98(Brian) In this case we are decoding a code longer than 8 bits. */
            /* So we need to start this loop as if we decoded the previous bits_used  */
            /* bits.  The hufftable contains the 5 pieces of info we need to restart  */
            /* this loop correctly.  Today I added the bits_used to fix PMR 10331.    */
            int bits_used = hcodes.b[0] >> 4;
            nold = hcodes.b[0] & 0x0F;
            j = hcodes.b[1];
            code = hcodes.w[1];
            c->inbits.z <<= bits_used;
            c->bits_in -= bits_used;

            for (n = bits_used + 1; n <= 16; n++) {  /* search through code lengths */
                nc = huff->ncodes[n - 1];
                if (!nc)
                    continue;                          /* no codes of this length */
                nb = (int)(n - nold);

                if (nb <= c->bits_in) {        /* enough bits available already */
                    c->inbits.z <<= nb;
                    c->bits_in -= nb;
                    bits = c->inbits.w[WORD_1] & bit_tbl[nb];
                }
                else if ((ret = jpeg_getbits(nb, (int STACK *)&bits, c)) != SUCCESS) {
                    if (adobe_jpg == ADOBE_JPEG) {
                        if (indataleft > CHECK_BYTES_SIZE || c->bytesCnt > c->numbytes)
                            return(ret);
                        else
                            ret = SUCCESS;
                    }
                    else
                        return(ret);
                }
                code = (int)((code << nb) | bits);
                if (code < nc)
                    break;                             /* found a word */
                code = (int)(code - nc);              /* not found */
                nold = n;
                j = (int)(j + nc);
            }
            n = huff->val[j + code];
            *run = (int)(n >> 4);                    /* run length in upper nibble */
            n &= 0xF;                                /* no. of bits in lower nibble */
        }

        if (n) {
            if (n <= c->bits_in) {        /* enough bits available already */
                c->inbits.z <<= n;
                c->bits_in -= n;
                code = c->inbits.w[WORD_1] & bit_tbl[n];
            }
            else if ((ret = jpeg_getbits(n, (int STACK *)&code, c)) != SUCCESS) {
                if (adobe_jpg == ADOBE_JPEG) {
                    if (indataleft > CHECK_BYTES_SIZE || c->bytesCnt > c->numbytes)
                        return(ret);
                    else
                        ret = SUCCESS;
                }
                else
                    return(ret);
            }
            if (code & (1 << (n - 1)))               /* look at top bit */
                *val = code;                      /* positive */
            else
                *val = (int)((code + 1) | ((-1) << n));   /* negative */
        }
        else {
            *val = 0;
        }

    }
    if (c->bits_in < 8) {        /* not enough bits available, get more */
        ret = jpeg_loadbits8(c);
        if (adobe_jpg == ADOBE_JPEG) {
            if (indataleft <= CHECK_BYTES_SIZE && c->bytesCnt <= c->numbytes)
                ret = SUCCESS;
        }
    }
    return (ret);
}

///////////////////////////////////////////////////////
//  @Name     : jpeg_getbits
//  @Class    : Internal
//  @Synopsis :
///////////////////////////////////////////////////////

int jpeg_getbits(int n, int STACK *getBits, CONTEXT_TRLPTR c)
{
    int origN = n;

    c->inbits.z <<= c->bits_in;
    n -= c->bits_in;
    if (n == 0) {
        *getBits = c->inbits.w[WORD_1] & bit_tbl[origN];
        return(SUCCESS);
    }

    c->inbits.b[BYTE_1] = c->inbuff[c->inbuffindex++];
    if (c->inbits.b[BYTE_1] == 0xFF)   /* check for fill bits */
    {
        if (c->inbuff[c->inbuffindex] == 0) {
            c->inbuffindex++;
            c->bytesCnt++;
        }
    }

    c->inbits.b[BYTE_0] = c->inbuff[c->inbuffindex++];
    c->bytesCnt += 2;

    if (c->inbits.b[BYTE_0] == 0xFF) {   /* check for fill bits */
        if (c->inbuff[c->inbuffindex] == 0) {
            c->inbuffindex++;
            c->bytesCnt++;
        }
    }

    c->inbits.z <<= n;
    c->bits_in = (int)(16 - n);

    *getBits = c->inbits.w[WORD_1] & bit_tbl[origN];

    if (c->bEndFound == TRUE) {
        if (c->bytesCnt >= c->numbytes) {
            c->bGetNewDataIn = TRUE;
        }
    }
    else {
        if ((c->bytesCnt + CHECK_BYTES_SIZE) >= c->numbytes) {
            c->bGetNewDataIn = TRUE;
            return(FAILURE);
        }
    }

    return(SUCCESS);
}

int jpeg_loadbits8(CONTEXT_TRLPTR c)
{
    int bits;

    bits = c->inbuff[c->inbuffindex++];
    if (bits == 0xFF)   /* check for fill bits */
    {
        if (c->inbuff[c->inbuffindex] == 0) {
            c->inbuffindex++;
            c->bytesCnt++;
        }
    }

    c->bytesCnt += 1;
    /* Works for both MSB-LSB */
    c->inbits.z |= bits << (8 - c->bits_in);
    c->bits_in += 8;

    if (c->bEndFound == TRUE) {
        if (c->bytesCnt >= c->numbytes) {
            c->bGetNewDataIn = TRUE;
        }
    }
    else {
        if ((c->bytesCnt + CHECK_BYTES_SIZE) >= c->numbytes) {
            c->bGetNewDataIn = TRUE;
            return(FAILURE);
        }
    }

    return(SUCCESS);
}




// input decoding struct
UNIV_CODEC_API void CleanupJpegDecoding(void *cp)
{
    CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;

    CloseDCTtoMCU(c->dct);

    delete[] c->outData;
    delete c;
}


