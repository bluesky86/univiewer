#include "stdafx.h"

#include "g3g4_internal.h"


/* A value which points to a GETCODE in the code table */
#define GETCODEOFFSET   6

#define G4G3_TERM_BIT   0x8000
#define G4G3_MAKEUP_BIT 0x4000
#define G4G3_DTERM_BIT  0xC000

#define G4G3_TERM_NUM      0x7800
#define G4G3_TERM_NUMPOS   11
#define G4G3_TERM_RUN      0x07ff
#define G4G3_MAKEUP_NUM    0x000f
#define G4G3_MAKEUP_RUN    0x3fc0
#define G4G3_DTERM_WRUN    0x3f00
#define G4G3_DTERM_BRUN    0x000f
#define G4G3_DTERM_NUM     0x00f0
#define G4G3_DTERM_NUMPOS  4
#define G4G3_DTERM_WRUNPOS 8

/* EOF mask and test - MSB/LSB spicific */
/* G4G3_USE_MSB */
#define G4G3_EOLAND  0x1ffe
#define G4G3_EOLTEST 0x0002

#define G4MACRO_VXo(x)    memcpy( codeline, refline, sizeof(ICL_TBLTYPE)*(x) ); codeline+=(x); refline+=(x);


/* Single codes MACRO definitions */
#define G4MACRO_INBITADD(x)   inbit += (x)
#define G4MACRO_Vo            *codeline++ = *refline++;
#define G4MACRO_V2o           *codeline++ = *refline++; *codeline++ = *refline++;
#define G4MACRO_V3o           *codeline++ = *refline++; *codeline++ = *refline++; *codeline++ = *refline++;
#define G4MACRO_V4o           *codeline++ = *refline++; *codeline++ = *refline++; *codeline++ = *refline++; *codeline++ = *refline++;
#define G4MACRO_V5o           *codeline++ = *refline++; *codeline++ = *refline++; *codeline++ = *refline++; *codeline++ = *refline++; *codeline++ = *refline++;

/* Using MEMCPY on V6o and up gives 1.0% improvement, on V5o and lower we get image corruption MSDEV4.2 -O2a */
#define G4MACRO_V6o           G4MACRO_VXo(6)
#define G4MACRO_V7o           G4MACRO_VXo(7)
#define G4MACRO_V8o           G4MACRO_VXo(8)

#define G4MACRO_Vr1           *codeline = (ICL_TBLTYPE)(*refline++ + 1); if (*refline <= *codeline++) refline += 2;
#define G4MACRO_Vr2           *codeline = (ICL_TBLTYPE)(*refline++ + 2); if (*refline <= *codeline++) refline += 2;
#define G4MACRO_Vr3           *codeline = (ICL_TBLTYPE)(*refline++ + 3); while(*refline <= *codeline) refline += 2; codeline++;
#define G4MACRO_Vl1           *codeline++ = (ICL_TBLTYPE)(*refline++ - 1);
#define G4MACRO_Vl2           *codeline   = (ICL_TBLTYPE)(*refline++ - 2); if (*(refline-2) > *codeline++) refline -= 2;
#define G4MACRO_Vl3           *codeline   = (ICL_TBLTYPE)(*refline++ - 3); if (*(refline-2) > *codeline++) refline -= 2;
#define G4MACRO_PASS          refline += 2; lastpass = *(codeline-1);
#define G4MACRO_2PASS         refline += 4; lastpass = *(codeline-1);
#define G4MACRO_HORZ          goto G4V_HORZA;
#define G4MACRO_ERROR         goto G4V_ERROR;
#define G4MACRO_VEOF          goto G4V_EOF;
#define G4MACRO_Vo_FIRST      *codeline++ = *refline++;
#define G4MACRO_Vr1_FIRST     *codeline = (ICL_TBLTYPE)(*refline++ + 1); if (*refline <= *codeline++) refline += 2;
#define G4MACRO_Vr2_FIRST     *codeline = (ICL_TBLTYPE)(*refline++ + 2); if (*refline <= *codeline++) refline += 2;
#define G4MACRO_Vr3_FIRST     *codeline = (ICL_TBLTYPE)(*refline++ + 3); while(*refline <= *codeline) refline += 2; codeline++;
#define G4MACRO_Vl1_FIRST     *codeline++ = (ICL_TBLTYPE)(*refline++ - 1);
#define G4MACRO_Vl2_FIRST     *codeline   = (ICL_TBLTYPE)(*refline++ - 2); if (*(refline-2) > *codeline++) refline -= 2;
#define G4MACRO_Vl3_FIRST     *codeline   = (ICL_TBLTYPE)(*refline++ - 3); if (*(refline-2) > *codeline++) refline -= 2;

/* GH_VoCT must be 2 or greater 1 turns it off */
#define GH_VoCT  1

#define GH_Vo    1
#define GH_xVo   2
#define GH_Vr1   3
#define GH_Vr2   4
#define GH_Vr3   5
#define GH_Vl1   6
#define GH_Vl2   7
#define GH_Vl3   8
#define GH_HORZ  9
#define GH_PASS  10
#define GH_EXT   11
#define GH_EOL   12

/*
 * Histogram output:
 * set this if to 0 to get a histogram of the g4 codes
 */

#define  GH_START()
#define  GH_ADD( code )
#define  GH_PRINT()
#define  GH_NEWLINE()



 
 int NEAR trl_G31DExpand(TRL_G4G3PTR c) {

     int                  code, index;
     int                  state = 0;
     int                  ret;
     int                  tmp;
     // setting last_tt to 1 will work for all cases except when there
     //   is a strieam of 0 run lengths and we exit and re-enter at that
     //   point (this is bad data anyway)
     ICL_TBLTYPE          last_tt = 1;


     // localize variables
     int                  inbit = c->inbit;
     S32                  width = c->width;
     int                  run = c->run;
     ICL_TBLTYPE          tt = c->tt;
     U8          G4G3_FAR *indatacur = c->indatacur;
     U8          G4G3_FAR *indataend = c->indataend - 2;
     ICL_TBLTYPE G4G3_FAR *codeline = c->codeline;


     // switch on command
     switch (c->command) {
     case G4G3_COMMAND_START:
     case G4G3_COMMAND_NEWLINE:
     case G4G3_COMMAND_NEXTBAND:
         goto G3WHITE;
         break;

     case G4G3_COMMAND_CONTINUE:
         if (c->state == G4G3_STATE_BLACK)
             goto G3BLACK;
         if (c->state == G4G3_STATE_WHITE)
             goto G3WHITE;
         if (c->state == G4G3_STATE_EOLTEST)
             goto EOLTEST;
         break;

     case G4G3_COMMAND_FLUSH:
         indataend += 2;
         if (c->state == G4G3_STATE_BLACK)
             goto G3BLACK;
         if (c->state == G4G3_STATE_WHITE)
             goto G3WHITE;
         break;

     default:
         break;
     }

 G3WHITE:
     // White processing

     // Check for end of input data
     indatacur += inbit >> 3;
     inbit &= 7;
     if (indatacur >= indataend) {
         state = G4G3_STATE_WHITE;
         goto ENDOFDATA;
     }

     // Get code - MSB 8bit data stream read
     index = (*indatacur << 8) + *(indatacur + 1);
     if (inbit > 3) {
         index = (index << (inbit - 3)) + ((U16)*(indatacur + 2) >> (11 - inbit));
     }
     else if (inbit < 3) {
         index = index >> (3 - inbit);
     }

     // Get white table code Use 13 bits of index
     code = trl_g4g3_horz_white[index & 0x1fff];

     if (!(code & G4G3_TERM_BIT)) {
         // Terminating code found
         tmp = (code & G4G3_TERM_NUM) >> G4G3_TERM_NUMPOS;
         if (tmp == 0)
             goto HTESTERROR;

         inbit += tmp;
         tmp = (code & G4G3_TERM_RUN) + run;
         tt = (ICL_TBLTYPE)(tt + tmp);
         run = 0;

         if ((U32)tt >= (U32)width) {
             goto ENDOFLINE;
         }
         *codeline++ = tt;

         // If transition produces a run length of 0, and it's not the
         //   first transition, then swallow 2 transitions

            // We want to check if we are at the leftmost edge of a tile, not of the entire image. PMR 34951
         if ((tmp == 0) && (tt != c->tt )) {
             if (last_tt == tt) codeline += 1;
             codeline -= 2;
             last_tt = tt;
         }

         goto G3BLACK;
     }
     else if (!(code & G4G3_MAKEUP_BIT)) {
         // Makeup code found
         run += (code & G4G3_MAKEUP_RUN);
         inbit += (code & G4G3_MAKEUP_NUM);
         goto G3WHITE;
     }
     else {
         // White & Black term codes
         tmp = ((code & G4G3_DTERM_WRUN) >> G4G3_DTERM_WRUNPOS) + run;
         tt = (ICL_TBLTYPE)(tt + tmp);
         run = code & G4G3_DTERM_BRUN;
         inbit += (code & G4G3_DTERM_NUM) >> G4G3_DTERM_NUMPOS;

         if ((U32)tt >= (U32)width) {
             // First run is black on next line, ERROR
             // Lets hope we have an EOL or byte advance to eat this

             // Back out of the second transition
             inbit -= trl_g4g3_htbl_btl[run];
             goto ENDOFLINE;
         }
         *codeline++ = tt;
         // If transition produces a run length of 0, and it's not the
         //   first transition, then swallow 2 transitions

            // We want to check if we are at the leftmost edge of a tile, not of the entire image. PMR 34951
         if ((tmp == 0) && (tt != c->tt)) {
             if (last_tt == tt) codeline += 1;
             codeline -= 2;
             last_tt = tt;
         }
         tt = (ICL_TBLTYPE)(tt + run);
         run = 0;
         if ((U32)tt >= (U32)width) {
             goto ENDOFLINE;
         }
         *codeline++ = tt;

         goto G3WHITE;
     }


 G3BLACK:
     // Black processing - Same as white, different jumps.

     // Check for end of input data
     indatacur += inbit >> 3;
     inbit &= 7;

     if (indatacur >= indataend) {
         // For some tiled FMT_D_G3TIFFM images, we will get a line on the right side of the image
         // this happens at the end of a tile, because we miss a tt. Create a fix for this PMR 62569
         if (c->bTiled && c->bTileLine) {
             *codeline++ = tt + 1;
             *codeline++ = (ICL_TBLTYPE)width;
             *codeline++ = (ICL_TBLTYPE)width;
             *codeline++ = (ICL_TBLTYPE)width;
             *codeline++ = (ICL_TBLTYPE)width;
         }
         state = G4G3_STATE_BLACK;
         goto ENDOFDATA;
     }

     // Get code - MSB 8bit data stream read
     index = (*indatacur << 8) + *(indatacur + 1);
     if (inbit > 3) {
         index = (index << (inbit - 3)) + ((U16)*(indatacur + 2) >> (11 - inbit));
     }
     else if (inbit < 3) {
         index = index >> (3 - inbit);
     }

     // Get black table code Use 13 bits of index
     code = trl_g4g3_horz_black[index & 0x1fff];

     if (!(code & G4G3_TERM_BIT)) {
         // Terminating code found
         tmp = (code & G4G3_TERM_NUM) >> G4G3_TERM_NUMPOS;
         if (tmp == 0)
             goto HTESTERROR;

         inbit += tmp;
         tmp = (code & G4G3_TERM_RUN) + run;
         tt = (ICL_TBLTYPE)(tt + tmp);
         run = 0;
         if ((U32)tt >= (U32)width) {
             goto ENDOFLINE;
         }
         *codeline++ = tt;

         // If transition produces a run length of 0, and it's not the
         //   first transition, then swallow 2 transitions

            // We want to check if we are at the leftmost edge of a tile, not of the entire image. PMR 34951
            //   (20March2000)Catherine
         if ((tmp == 0) && (tt != c->tt)) {
             if (last_tt == tt) codeline += 1;
             codeline -= 2;
             last_tt = tt;
         }

         goto G3WHITE;
     }
     else if (!(code & G4G3_MAKEUP_BIT)) {
         // Makeup code found
         run += (code & G4G3_MAKEUP_RUN);
         inbit += (code & G4G3_MAKEUP_NUM);
         goto G3BLACK;
     }
     else {
         // White & Black term codes //
         tt = (ICL_TBLTYPE)(tt + run + (code & G4G3_DTERM_BRUN));
         run = ((code & G4G3_DTERM_WRUN) >> G4G3_DTERM_WRUNPOS);
         inbit += (code & G4G3_DTERM_NUM) >> G4G3_DTERM_NUMPOS;
         if ((U32)tt >= (U32)width) {
             // Back out of the second transition
             inbit -= trl_g4g3_htbl_wtl[run];
             goto ENDOFLINE;
         }
         *codeline++ = tt;
         tt = (ICL_TBLTYPE)(tt + run);
         if ((U32)tt >= (U32)width) {
             run = 0;
             goto ENDOFLINE;
         }
         *codeline++ = tt;
         // If transition produces a run length of 0, and it's not the
         //   first transition, then swallow 2 transitions

            // We want to check if we are at the leftmost edge of a tile, not of the entire image. PMR 34951
            //   (20March2000)Catherine
         if ((run == 0) && (tt != c->tt)) {
             if (last_tt == tt) codeline += 1;
             codeline -= 2;
             last_tt = tt;
         }
         run = 0;

         goto G3BLACK;
     }


 HTESTERROR:
     // Reset run and tt since we are not continue processing the line
     run = 0;
     tt = 1;
     // We missed the code table, may have an EOL
     if ((index & G4G3_EOLAND) == G4G3_EOLTEST) {
         inbit += 12;
         if (c->eolfound) {
             ret = G4G3_EOF;
             goto STATESAVE;
         }
         goto ENDOFLINE;

     }
     if ((index & G4G3_EOLAND) == 0) {
         // We have got an EOL, search to next on bit
         inbit += 13;

         // Do a byte scan for more bits
     EOLTEST:
         indatacur += inbit >> 3;
         inbit &= 7;
         if (indatacur >= indataend) {
             state = G4G3_STATE_EOLTEST;
             ret = G4G3_EOD;
             goto STATESAVE;
         }

         while (*indatacur == 0) {
             if (indatacur >= indataend) {
                 state = G4G3_STATE_EOLTEST;
                 ret = G4G3_EOD;
                 goto STATESAVE;
             }
             indatacur++;
         }
         // Find out what bit is on
         inbit = trl_g4g3_bittbl[*indatacur];

         goto ENDOFLINE;
     }
     ret = G4G3_BADLINE;
     goto STATESAVE;

 ENDOFLINE:
     run = 0;
     tt = 1;
     ret = G4G3_EOL;
     state = G4G3_COMMAND_NEWLINE;
     *codeline++ = (ICL_TBLTYPE)width;
     *codeline++ = (ICL_TBLTYPE)width;
     *codeline++ = (ICL_TBLTYPE)width;
     *codeline++ = (ICL_TBLTYPE)width;
     // Reset EOL marker
     c->eolfound = FALSE;
     goto STATESAVE;

 ENDOFDATA:
     ret = G4G3_EOD;
     goto STATESAVE;

 STATESAVE:
     c->run = run;
     c->tt = tt;
     c->indatacur = indatacur;
     c->inbit = inbit;
     c->codeline = codeline;
     c->state = state;
     return(ret);

 } // trl_G31DExpand
 



int NEAR trl_G4Expand(TRL_G4G3PTR c) {

    register ICL_TBLTYPE G4G3_FAR *refline = c->refline;
    register ICL_TBLTYPE G4G3_FAR *codeline = c->codeline;

    int                  code, index;
    int                  tmp;
    int                  state = 0;
    int                  ret;

    /* localize variables */
    int                  inbit;
    S32                  width;
    U8          G4G3_FAR *indatacur;
    U8          G4G3_FAR *indataend;
    ICL_TBLTYPE G4G3_FAR *refend;
    ICL_TBLTYPE G4G3_FAR *refend2;
    ICL_TBLTYPE G4G3_FAR *codestart = c->codestart;
    int                  run = 0;
    ICL_TBLTYPE          tt = 0;
    int                  hcnt = 0;
    ICL_TBLTYPE          lastpass;


    /* initialize stuff */
    inbit = c->inbit;
    width = c->width;
    lastpass = c->lastpass;
    indatacur = c->indatacur;
    indataend = c->indataend - 2;
    refend = c->refend;
    refend2 = refend + 12;
    ret = SUCCESS;

    /* switch on command */
    switch (c->command) {
    case G4G3_COMMAND_START:
        GH_START();
    case G4G3_COMMAND_NEXTBAND:
        c->state = G4G3_STATE_G4V_GETCODE;
        break;

    case G4G3_COMMAND_NEWLINE:
        GH_NEWLINE();
        break;

    case G4G3_COMMAND_FLUSH:
        indataend += 2;
        /* intentional fall through */
    case G4G3_COMMAND_CONTINUE:
        if ((c->state == G4G3_STATE_BLACK) || (c->state == G4G3_STATE_WHITE)) {
            hcnt = c->hcnt;
            run = c->run;
            tt = c->tt;
        }
        break;

    default:
        break;
    }

    switch (c->state) {
    case G4G3_STATE_WHITE:
        goto G4WHITE;
    case G4G3_STATE_BLACK:
        goto G4BLACK;
    case G4G3_STATE_VERT:
        goto G4START;
    case G4G3_STATE_G4V_GETCODE:
        goto G4GETCODE;
    case G4G3_STATE_EOLTEST:
        goto G4EOLTEST;
    default:
        break;
    } /* switch */






G4START:
    /* vcode should be GETCODE first time in */


G4GETCODE:
    indatacur += inbit >> 3;
    inbit &= 7;
    /* if EOD set state & return */
    if (indatacur >= indataend) {
        state = G4G3_STATE_G4V_GETCODE;
        goto G4EOD;
    }

    /* Get code - MSB 8bit data stream read */
    if (inbit) {
        index = (*indatacur << 8) + *(indatacur + 1);
        index >>= (8 - inbit);
    }
    else {
        index = *indatacur;
    }
    /* GOTO Switch on successful get */
    if (refline < refend) {
        switch (trl_g4g3_vert_compx[index & 0x0FF]) {
        case C_VEOF:
            goto G4C_VEOF;
        case C_ERROR:
            goto G4C_ERROR;
        case C_Vl3:
            goto G4C_Vl3;
        case C_Vl3Vo:
            goto G4C_Vl3Vo;
        case C_Vr3:
            goto G4C_Vr3;
        case C_Vr3Vo:
            goto G4C_Vr3Vo;
        case C_Vl2:
            goto G4C_Vl2;
        case C_Vl2Vo:
            goto G4C_Vl2Vo;
        case C_Vl2V2o:
            goto G4C_Vl2V2o;
        case C_Vr2:
            goto G4C_Vr2;
        case C_Vr2Vo:
            goto G4C_Vr2Vo;
        case C_Vr2V2o:
            goto G4C_Vr2V2o;
        case C_PASS:
            goto G4C_PASS;
        case C_2PASS:
            goto G4C_2PASS;
        case C_HORZ:
            goto G4C_HORZ;
        case C_Vl1:
            goto G4C_Vl1;
        case C_Vl1PASS:
            goto G4C_Vl1PASS;
        case C_Vl1HORZ:
            goto G4C_Vl1HORZ;
        case C_2Vl1:
            goto G4C_2Vl1;
        case C_2Vl1Vo:
            goto G4C_2Vl1Vo;
        case C_2Vl1V2o:
            goto G4C_2Vl1V2o;
        case C_Vl1Vr1:
            goto G4C_Vl1Vr1;
        case C_Vl1Vr1Vo:
            goto G4C_Vl1Vr1Vo;
        case C_Vl1Vr1V2o:
            goto G4C_Vl1Vr1V2o;
        case C_Vl1Vo:
            goto G4C_Vl1Vo;
        case C_Vl1VoPASS:
            goto G4C_Vl1VoPASS;
        case C_Vl1VoHORZ:
            goto G4C_Vl1VoHORZ;
        case C_Vl1VoVl1:
            goto G4C_Vl1VoVl1;
        case C_Vl1VoVl1Vo:
            goto G4C_Vl1VoVl1Vo;
        case C_Vl1VoVr1:
            goto G4C_Vl1VoVr1;
        case C_Vl1VoVr1Vo:
            goto G4C_Vl1VoVr1Vo;
        case C_Vl1V2o:
            goto G4C_Vl1V2o;
        case C_Vl1V2oHORZ:
            goto G4C_Vl1V2oHORZ;
        case C_Vl1V2oVl1:
            goto G4C_Vl1V2oVl1;
        case C_Vl1V2oVr1:
            goto G4C_Vl1V2oVr1;
        case C_Vl1V2oVo:
            goto G4C_Vl1V2oVo;
        case C_Vl12V2o:
            goto G4C_Vl12V2o;
        case C_Vl12V2oVo:
            goto G4C_Vl12V2oVo;
        case C_Vr1:
            goto G4C_Vr1;
        case C_Vr1PASS:
            goto G4C_Vr1PASS;
        case C_Vr1HORZ:
            goto G4C_Vr1HORZ;
        case C_Vr1Vl1:
            goto G4C_Vr1Vl1;
        case C_Vr1Vl1Vo:
            goto G4C_Vr1Vl1Vo;
        case C_Vr1Vl1V2o:
            goto G4C_Vr1Vl1V2o;
        case C_2Vr1:
            goto G4C_2Vr1;
        case C_2Vr1Vo:
            goto G4C_2Vr1Vo;
        case C_2Vr1V2o:
            goto G4C_2Vr1V2o;
        case C_Vr1Vo:
            goto G4C_Vr1Vo;
        case C_Vr1VoPASS:
            goto G4C_Vr1VoPASS;
        case C_Vr1VoHORZ:
            goto G4C_Vr1VoHORZ;
        case C_Vr1VoVl1:
            goto G4C_Vr1VoVl1;
        case C_Vr1VoVl1Vo:
            goto G4C_Vr1VoVl1Vo;
        case C_Vr1VoVr1:
            goto G4C_Vr1VoVr1;
        case C_Vr1VoVr1Vo:
            goto G4C_Vr1VoVr1Vo;
        case C_Vr1V2o:
            goto G4C_Vr1V2o;
        case C_Vr1V2oHORZ:
            goto G4C_Vr1V2oHORZ;
        case C_Vr1V2oVl1:
            goto G4C_Vr1V2oVl1;
        case C_Vr1V2oVr1:
            goto G4C_Vr1V2oVr1;
        case C_Vr1V2oVo:
            goto G4C_Vr1V2oVo;
        case C_Vr12V2o:
            goto G4C_Vr12V2o;
        case C_Vr12V2oVo:
            goto G4C_Vr12V2oVo;
        case C_Vo:
            goto G4C_Vo;
        case C_VoVl3:
            goto G4C_VoVl3;
        case C_VoVr3:
            goto G4C_VoVr3;
        case C_VoVl2:
            goto G4C_VoVl2;
        case C_VoVl2Vo:
            goto G4C_VoVl2Vo;
        case C_VoVr2:
            goto G4C_VoVr2;
        case C_VoVr2Vo:
            goto G4C_VoVr2Vo;
        case C_VoPASS:
            goto G4C_VoPASS;
        case C_VoHORZ:
            goto G4C_VoHORZ;
        case C_VoVl1:
            goto G4C_VoVl1;
        case C_VoVl1PASS:
            goto G4C_VoVl1PASS;
        case C_VoVl1HORZ:
            goto G4C_VoVl1HORZ;
        case C_Vo2Vl1:
            goto G4C_Vo2Vl1;
        case C_Vo2Vl1Vo:
            goto G4C_Vo2Vl1Vo;
        case C_VoVl1Vr1:
            goto G4C_VoVl1Vr1;
        case C_VoVl1Vr1Vo:
            goto G4C_VoVl1Vr1Vo;
        case C_VoVl1Vo:
            goto G4C_VoVl1Vo;
        case C_VoVl1VoHORZ:
            goto G4C_VoVl1VoHORZ;
        case C_VoVl1VoVl1:
            goto G4C_VoVl1VoVl1;
        case C_VoVl1VoVr1:
            goto G4C_VoVl1VoVr1;
        case C_VoVl1V2o:
            goto G4C_VoVl1V2o;
        case C_VoVl1V2oVo:
            goto G4C_VoVl1V2oVo;
        case C_VoVl12V2o:
            goto G4C_VoVl12V2o;
        case C_VoVr1:
            goto G4C_VoVr1;
        case C_VoVr1PASS:
            goto G4C_VoVr1PASS;
        case C_VoVr1HORZ:
            goto G4C_VoVr1HORZ;
        case C_VoVr1Vl1:
            goto G4C_VoVr1Vl1;
        case C_VoVr1Vl1Vo:
            goto G4C_VoVr1Vl1Vo;
        case C_Vo2Vr1:
            goto G4C_Vo2Vr1;
        case C_Vo2Vr1Vo:
            goto G4C_Vo2Vr1Vo;
        case C_VoVr1Vo:
            goto G4C_VoVr1Vo;
        case C_VoVr1VoHORZ:
            goto G4C_VoVr1VoHORZ;
        case C_VoVr1VoVl1:
            goto G4C_VoVr1VoVl1;
        case C_VoVr1VoVr1:
            goto G4C_VoVr1VoVr1;
        case C_VoVr1V2o:
            goto G4C_VoVr1V2o;
        case C_VoVr1V2oVo:
            goto G4C_VoVr1V2oVo;
        case C_VoVr12V2o:
            goto G4C_VoVr12V2o;
        case C_V2o:
            goto G4C_V2o;
        case C_V2oVl2:
            goto G4C_V2oVl2;
        case C_V2oVr2:
            goto G4C_V2oVr2;
        case C_V2oPASS:
            goto G4C_V2oPASS;
        case C_V2oHORZ:
            goto G4C_V2oHORZ;
        case C_V2oVl1:
            goto G4C_V2oVl1;
        case C_V2oVl1HORZ:
            goto G4C_V2oVl1HORZ;
        case C_V2o2Vl1:
            goto G4C_V2o2Vl1;
        case C_V2oVl1Vr1:
            goto G4C_V2oVl1Vr1;
        case C_V2oVl1Vo:
            goto G4C_V2oVl1Vo;
        case C_V2oVl1V2o:
            goto G4C_V2oVl1V2o;
        case C_V2oVl1V2oVo:
            goto G4C_V2oVl1V2oVo;
        case C_V2oVr1:
            goto G4C_V2oVr1;
        case C_V2oVr1HORZ:
            goto G4C_V2oVr1HORZ;
        case C_V2oVr1Vl1:
            goto G4C_V2oVr1Vl1;
        case C_V2o2Vr1:
            goto G4C_V2o2Vr1;
        case C_V2oVr1Vo:
            goto G4C_V2oVr1Vo;
        case C_V2oVr1V2o:
            goto G4C_V2oVr1V2o;
        case C_V2oVr1V2oVo:
            goto G4C_V2oVr1V2oVo;
        case C_V2oVo:
            goto G4C_V2oVo;
        case C_V2oVoPASS:
            goto G4C_V2oVoPASS;
        case C_V2oVoHORZ:
            goto G4C_V2oVoHORZ;
        case C_V2oVoVl1:
            goto G4C_V2oVoVl1;
        case C_V2oVoVl1Vo:
            goto G4C_V2oVoVl1Vo;
        case C_V2oVoVl1V2o:
            goto G4C_V2oVoVl1V2o;
        case C_V2oVoVr1:
            goto G4C_V2oVoVr1;
        case C_V2oVoVr1Vo:
            goto G4C_V2oVoVr1Vo;
        case C_V2oVoVr1V2o:
            goto G4C_V2oVoVr1V2o;
        case C_2V2o:
            goto G4C_2V2o;
        case C_2V2oPASS:
            goto G4C_2V2oPASS;
        case C_2V2oHORZ:
            goto G4C_2V2oHORZ;
        case C_2V2oVl1:
            goto G4C_2V2oVl1;
        case C_2V2oVl1Vo:
            goto G4C_2V2oVl1Vo;
        case C_2V2oVr1:
            goto G4C_2V2oVr1;
        case C_2V2oVr1Vo:
            goto G4C_2V2oVr1Vo;
        case C_2V2oVo:
            goto G4C_2V2oVo;
        case C_2V2oVoHORZ:
            goto G4C_2V2oVoHORZ;
        case C_2V2oVoVl1:
            goto G4C_2V2oVoVl1;
        case C_2V2oVoVr1:
            goto G4C_2V2oVoVr1;
        case C_3V2o:
            goto G4C_3V2o;
        case C_3V2oVo:
            goto G4C_3V2oVo;
        case C_4V2o:
            goto G4C_4V2o;
        } /* Switch verttbl */
    }

    if (*refline >= width) {
        while (refline > c->refline && *(refline - 1) >= width) {
            refline--;
        }
    }
    /* Single switch codes - G4_SINGLE_SW: */
    switch (trl_g4g3_vert_singl[index & 0x00FF]) {

    case VS_V2o:
        goto G4V_V2o;
    case VS_Vo:
        goto G4V_Vo;
    case VS_Vl1:
        goto G4V_Vl1;
    case VS_Vl2:
        goto G4V_Vl2;
    case VS_Vl3:
        goto G4V_Vl3;
    case VS_Vr1:
        goto G4V_Vr1;
    case VS_Vr2:
        goto G4V_Vr2;
    case VS_Vr3:
        goto G4V_Vr3;
    case VS_PASS:
        goto G4V_PASS;
    case VS_HORZ:
        goto G4V_HORZ;
    case VS_ERROR:
        goto G4V_ERROR;
    case VS_VEOF:
        goto G4V_EOF;

    } /* Switch verttbl */


        /* Complex switch codes */
G4C_VEOF:
    G4MACRO_INBITADD(8);
    G4MACRO_VEOF
    goto G4GETCODE;

G4C_ERROR:
    G4MACRO_INBITADD(8);
    G4MACRO_ERROR
    goto G4GETCODE;


G4C_Vl3:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl3_FIRST
    goto G4GETCODE;

G4C_Vl3Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl3_FIRST
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vr3:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr3_FIRST
    goto G4GETCODE;

G4C_Vr3Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr3_FIRST
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vl2:
    G4MACRO_INBITADD(6);
    G4MACRO_Vl2_FIRST
    goto G4GETCODE;

G4C_Vl2Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl2_FIRST
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vl2V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl2_FIRST
    G4MACRO_V2o
    goto G4GETCODE;

G4C_Vr2:
    G4MACRO_INBITADD(6);
    G4MACRO_Vr2_FIRST
    goto G4GETCODE;

G4C_Vr2Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr2_FIRST
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vr2V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr2_FIRST
    G4MACRO_V2o
    goto G4GETCODE;

G4C_PASS:
    G4MACRO_INBITADD(4);
    G4MACRO_PASS
    goto G4GETCODE;

G4C_2PASS:
    G4MACRO_INBITADD(8);
    G4MACRO_2PASS
    goto G4GETCODE;

G4C_HORZ:
    G4MACRO_INBITADD(3);
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_Vl1:
    G4MACRO_INBITADD(3);
    G4MACRO_Vl1_FIRST
        goto G4GETCODE;

G4C_Vl1PASS:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl1_FIRST
    G4MACRO_PASS
    goto G4GETCODE;

G4C_Vl1HORZ:
    G4MACRO_INBITADD(6);
    G4MACRO_Vl1_FIRST
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_2Vl1:
    G4MACRO_INBITADD(6);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_2Vl1Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_2Vl1V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vl1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_Vl1Vr1:
    G4MACRO_INBITADD(6);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_Vl1Vr1Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vl1Vr1V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vr1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_Vl1Vo:
    G4MACRO_INBITADD(4);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vl1VoPASS:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vo
    G4MACRO_PASS
    goto G4GETCODE;

G4C_Vl1VoHORZ:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vo
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_Vl1VoVl1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vo
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_Vl1VoVl1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vo
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vl1VoVr1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vo
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_Vl1VoVr1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_Vo
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vl1V2o:
    G4MACRO_INBITADD(5);
    G4MACRO_Vl1_FIRST
    G4MACRO_V2o
    goto G4GETCODE;

G4C_Vl1V2oHORZ:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_V2o
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_Vl1V2oVl1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_V2o
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_Vl1V2oVr1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_V2o
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_Vl1V2oVo:
    G4MACRO_INBITADD(6);
    G4MACRO_Vl1_FIRST
    G4MACRO_V3o
    goto G4GETCODE;

G4C_Vl12V2o:
    G4MACRO_INBITADD(7);
    G4MACRO_Vl1_FIRST
    G4MACRO_V4o
    goto G4GETCODE;

G4C_Vl12V2oVo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vl1_FIRST
    G4MACRO_V5o
    goto G4GETCODE;

G4C_Vr1:
    G4MACRO_INBITADD(3);
    G4MACRO_Vr1_FIRST
    goto G4GETCODE;

G4C_Vr1PASS:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr1_FIRST
    G4MACRO_PASS
    goto G4GETCODE;

G4C_Vr1HORZ:
    G4MACRO_INBITADD(6);
    G4MACRO_Vr1_FIRST
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_Vr1Vl1:
    G4MACRO_INBITADD(6);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_Vr1Vl1Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vr1Vl1V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vl1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_2Vr1:
    G4MACRO_INBITADD(6);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_2Vr1Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_2Vr1V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vr1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_Vr1Vo:
    G4MACRO_INBITADD(4);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vr1VoPASS:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vo
    G4MACRO_PASS
    goto G4GETCODE;

G4C_Vr1VoHORZ:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vo
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_Vr1VoVl1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vo
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_Vr1VoVl1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vo
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vr1VoVr1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vo
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_Vr1VoVr1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_Vo
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vr1V2o:
    G4MACRO_INBITADD(5);
    G4MACRO_Vr1_FIRST
    G4MACRO_V2o
    goto G4GETCODE;

G4C_Vr1V2oHORZ:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_V2o
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_Vr1V2oVl1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_V2o
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_Vr1V2oVr1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_V2o
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_Vr1V2oVo:
    G4MACRO_INBITADD(6);
    G4MACRO_Vr1_FIRST
    G4MACRO_V3o
    goto G4GETCODE;

G4C_Vr12V2o:
    G4MACRO_INBITADD(7);
    G4MACRO_Vr1_FIRST
    G4MACRO_V4o
    goto G4GETCODE;

G4C_Vr12V2oVo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vr1_FIRST
    G4MACRO_V5o
    goto G4GETCODE;

G4C_Vo:
    G4MACRO_INBITADD(1);
    G4MACRO_Vo_FIRST
    goto G4GETCODE;

G4C_VoVl3:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl3
    goto G4GETCODE;

G4C_VoVr3:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr3
    goto G4GETCODE;

G4C_VoVl2:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl2
    goto G4GETCODE;

G4C_VoVl2Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl2
    G4MACRO_Vo
    goto G4GETCODE;

G4C_VoVr2:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr2
    goto G4GETCODE;

G4C_VoVr2Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr2
    G4MACRO_Vo
    goto G4GETCODE;

G4C_VoPASS:
    G4MACRO_INBITADD(5);
    G4MACRO_Vo_FIRST
    G4MACRO_PASS
    goto G4GETCODE;

G4C_VoHORZ:
    G4MACRO_INBITADD(4);
    G4MACRO_Vo_FIRST
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_VoVl1:
    G4MACRO_INBITADD(4);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_VoVl1PASS:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_PASS
    goto G4GETCODE;

G4C_VoVl1HORZ:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_Vo2Vl1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_Vo2Vl1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_VoVl1Vr1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_VoVl1Vr1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_VoVl1Vo:
    G4MACRO_INBITADD(5);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_VoVl1VoHORZ:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vo
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_VoVl1VoVl1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vo
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_VoVl1VoVr1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_Vo
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_VoVl1V2o:
    G4MACRO_INBITADD(6);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_VoVl1V2oVo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_V3o
    goto G4GETCODE;

G4C_VoVl12V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vl1
    G4MACRO_V4o
    goto G4GETCODE;

G4C_VoVr1:
    G4MACRO_INBITADD(4);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_VoVr1PASS:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_PASS
    goto G4GETCODE;

G4C_VoVr1HORZ:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_VoVr1Vl1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_VoVr1Vl1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_Vo2Vr1:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_Vo2Vr1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_VoVr1Vo:
    G4MACRO_INBITADD(5);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_VoVr1VoHORZ:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vo
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_VoVr1VoVl1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vo
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_VoVr1VoVr1:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_Vo
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_VoVr1V2o:
    G4MACRO_INBITADD(6);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_VoVr1V2oVo:
    G4MACRO_INBITADD(7);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_V3o
    goto G4GETCODE;

G4C_VoVr12V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_Vo_FIRST
    G4MACRO_Vr1
    G4MACRO_V4o
    goto G4GETCODE;

G4C_V2o:
    G4MACRO_INBITADD(2);
    G4MACRO_V2o
    goto G4GETCODE;

G4C_V2oVl2:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vl2
    goto G4GETCODE;

G4C_V2oVr2:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vr2
    goto G4GETCODE;

G4C_V2oPASS:
    G4MACRO_INBITADD(6);
    G4MACRO_V2o
    G4MACRO_PASS
    goto G4GETCODE;

G4C_V2oHORZ:
    G4MACRO_INBITADD(5);
    G4MACRO_V2o
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_V2oVl1:
    G4MACRO_INBITADD(5);
    G4MACRO_V2o
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_V2oVl1HORZ:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vl1
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_V2o2Vl1:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vl1
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_V2oVl1Vr1:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vl1
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_V2oVl1Vo:
    G4MACRO_INBITADD(6);
    G4MACRO_V2o
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_V2oVl1V2o:
    G4MACRO_INBITADD(7);
    G4MACRO_V2o
    G4MACRO_Vl1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_V2oVl1V2oVo:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vl1
    G4MACRO_V3o
    goto G4GETCODE;

G4C_V2oVr1:
    G4MACRO_INBITADD(5);
    G4MACRO_V2o
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_V2oVr1HORZ:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vr1
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_V2oVr1Vl1:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vr1
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_V2o2Vr1:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vr1
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_V2oVr1Vo:
    G4MACRO_INBITADD(6);
    G4MACRO_V2o
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_V2oVr1V2o:
    G4MACRO_INBITADD(7);
    G4MACRO_V2o
    G4MACRO_Vr1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_V2oVr1V2oVo:
    G4MACRO_INBITADD(8);
    G4MACRO_V2o
    G4MACRO_Vr1
    G4MACRO_V3o
    goto G4GETCODE;

G4C_V2oVo:
    G4MACRO_INBITADD(3);
    G4MACRO_V3o
    goto G4GETCODE;

G4C_V2oVoPASS:
    G4MACRO_INBITADD(7);
    G4MACRO_V3o
    G4MACRO_PASS
    goto G4GETCODE;

G4C_V2oVoHORZ:
    G4MACRO_INBITADD(6);
    G4MACRO_V3o
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_V2oVoVl1:
    G4MACRO_INBITADD(6);
    G4MACRO_V3o
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_V2oVoVl1Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_V3o
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_V2oVoVl1V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_V3o
    G4MACRO_Vl1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_V2oVoVr1:
    G4MACRO_INBITADD(6);
    G4MACRO_V3o
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_V2oVoVr1Vo:
    G4MACRO_INBITADD(7);
    G4MACRO_V3o
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_V2oVoVr1V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_V3o
    G4MACRO_Vr1
    G4MACRO_V2o
    goto G4GETCODE;

G4C_2V2o:
    G4MACRO_INBITADD(4);
    G4MACRO_V4o
    goto G4GETCODE;

G4C_2V2oPASS:
    G4MACRO_INBITADD(8);
    G4MACRO_V4o
    G4MACRO_PASS
    goto G4GETCODE;

G4C_2V2oHORZ:
    G4MACRO_INBITADD(7);
    G4MACRO_V4o
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_2V2oVl1:
    G4MACRO_INBITADD(7);
    G4MACRO_V4o
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_2V2oVl1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_V4o
    G4MACRO_Vl1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_2V2oVr1:
    G4MACRO_INBITADD(7);
    G4MACRO_V4o
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_2V2oVr1Vo:
    G4MACRO_INBITADD(8);
    G4MACRO_V4o
    G4MACRO_Vr1
    G4MACRO_Vo
    goto G4GETCODE;

G4C_2V2oVo:
    G4MACRO_INBITADD(5);
    G4MACRO_V5o
    goto G4GETCODE;

G4C_2V2oVoHORZ:
    G4MACRO_INBITADD(8);
    G4MACRO_V5o
    G4MACRO_HORZ
    goto G4GETCODE;

G4C_2V2oVoVl1:
    G4MACRO_INBITADD(8);
    G4MACRO_V5o
    G4MACRO_Vl1
    goto G4GETCODE;

G4C_2V2oVoVr1:
    G4MACRO_INBITADD(8);
    G4MACRO_V5o
    G4MACRO_Vr1
    goto G4GETCODE;

G4C_3V2o:
    G4MACRO_INBITADD(6);
    G4MACRO_V6o
    goto G4GETCODE;

G4C_3V2oVo:
    G4MACRO_INBITADD(7);
    G4MACRO_V7o
    goto G4GETCODE;

G4C_4V2o:
    G4MACRO_INBITADD(8);
    G4MACRO_V8o
    goto G4GETCODE;






    /* Single code processing */

G4V_Vl1:
    GH_ADD(GH_Vl1);
    G4MACRO_INBITADD(3);
    G4MACRO_Vl1
    goto G4GETCODE;

G4V_Vl2:
    GH_ADD(GH_Vl2);
    G4MACRO_INBITADD(6);
    G4MACRO_Vl2
    goto G4GETCODE;

G4V_Vl3:
    GH_ADD(GH_Vl3);
    G4MACRO_INBITADD(7);
    G4MACRO_Vl3
    goto G4GETCODE;

G4V_V2o:
    GH_ADD(GH_Vo);
    G4MACRO_INBITADD(1);
    if ((*codeline++ = *refline++) >= (ICL_TBLTYPE)width) {
        goto G4EOL;
    }
    /* Fall through to Vo case */
G4V_Vo:
    GH_ADD(GH_Vo);
    G4MACRO_INBITADD(1);
    if ((*codeline++ = *refline++) >= (ICL_TBLTYPE)width) {
        goto G4EOL;
    }
    goto G4GETCODE;

G4V_Vr1:
    GH_ADD(GH_Vr1);
    G4MACRO_INBITADD(3);
    if ((*codeline = (ICL_TBLTYPE)(*refline++ + 1)) >= (ICL_TBLTYPE)width) {
        codeline++; /* G4EOL needs to be at EOL */
        goto G4EOL;
    }
    if (*refline <= *codeline++)
        refline += 2;
    goto G4GETCODE;

G4V_Vr2:
    GH_ADD(GH_Vr2);
    G4MACRO_INBITADD(6);
    if ((*codeline = (ICL_TBLTYPE)(*refline++ + 2)) >= (ICL_TBLTYPE)width) {
        codeline++; /* G4EOL needs to be at EOL */
        goto G4EOL;
    }
    if (*refline <= *codeline++)
        refline += 2;
    goto G4GETCODE;

G4V_Vr3:
    GH_ADD(GH_Vr3);
    G4MACRO_INBITADD(7);
    tt = *codeline++ = (ICL_TBLTYPE)(*refline++ + 3);
    if ((U32)tt >= (U32)width) {
        goto G4EOL;
    }
    while (*refline <= tt)
        refline += 2;
    goto G4GETCODE;

G4V_PASS:
    GH_ADD(GH_PASS);
    G4MACRO_INBITADD(4);
    G4MACRO_PASS
        goto G4GETCODE;

G4V_HORZ:
    GH_ADD(GH_HORZ);
    G4MACRO_INBITADD(3);

G4V_HORZA:
    tt = *(codeline - 1);
    run = 0;
    if (lastpass == tt) {
        /* PASS HORZ, fix up a0 reference */
        tt = *(refline - 1);
    }
    hcnt = 1;

    /* If there are an odd number of transitions processed then Black run. */
    if ((codeline - codestart) & 1) {
        goto G4BLACK;
    }
    else {
        goto G4WHITE;
    }

G4HORZRET:
    if ((U32)tt >= (U32)width) {                  /* Check tt for width so we don't infinite check refline if tt == width */
        state = G4G3_STATE_G4V_GETCODE;
        goto G4EOL;
    }
    while (*refline <= tt) {
        refline += 2;
    }
    goto G4GETCODE;

G4V_ERROR:
    ret = G4G3_BADLINE;
    goto G4SAVESTATE;


G4V_EOF:
    GH_PRINT();
    GH_ADD(GH_EOL);
    c->indatacur = indatacur;
    c->inbit = inbit;
G4EOLTEST:
    ret = trl_PassEOL(c);
    if (ret == G4G3_EOF) {
        c->eolfound = TRUE;
    }
    if (ret == G4G3_EOD) {
        /* 44.adz and 94.adz has 8Kb of 0's in file, caused infinite loop so I added this if and G4EOLTEST label. */
        /* (PMR32067, Cms12418-2299) (27Sep99,BrianH) */
        state = c->state;
        inbit = c->inbit;
        indatacur = c->indatacur;
    }
    goto G4SAVESTATE;


    /******** END OF STATES ***********/



    /* EXIT for this function */

G4SAVESTATE:
    c->indatacur = indatacur;
    c->inbit = inbit;
    c->codeline = codeline;
    c->refline = refline;
    c->state = state;
    c->lastpass = lastpass;

    return(ret);










    /* EOL */
G4EOL:
    /* Add 3 Width1's to give us 4 */
    *(codeline - 1) = (ICL_TBLTYPE)width;
    *codeline++ = (ICL_TBLTYPE)width;
    *codeline++ = (ICL_TBLTYPE)width;
    *codeline++ = (ICL_TBLTYPE)width;
    /* For PASS HORZ, reset flag */
    lastpass = 0;
    /* Return EOL */
    state = G4G3_STATE_G4V_GETCODE;
    ret = G4G3_EOL;
    goto G4SAVESTATE;


G4EOD:
    /* Return EOD */
    ret = G4G3_EOD;
    goto G4SAVESTATE;


G4HERROR:
    ret = G4G3_BADLINE;
    goto G4SAVESTATE;


G4WHITE:
    /* White processing */

    /* Check for end of input data */
    indatacur += inbit >> 3;
    inbit &= 7;

    /* if EOD set state & return */
    if (indatacur >= indataend) {
        state = G4G3_STATE_WHITE;
        c->run = run;
        c->tt = tt;
        c->hcnt = hcnt;
        goto G4EOD;
    }


    /* Get code - MSB 8bit data stream read */
    index = (*indatacur << 8) + *(indatacur + 1);
    if (inbit > 3) {
        index = (index << (inbit - 3)) + ((U16)*(indatacur + 2) >> (11 - inbit));
    }
    else if (inbit < 3) {
        index = index >> (3 - inbit);
    }

    /* Get white table code Use 13 bits of index */
    code = trl_g4g3_horz_white[index & 0x1fff];

    if (!(code & G4G3_TERM_BIT)) {
        /* Terminating code found */
        tmp = (code & G4G3_TERM_NUM) >> G4G3_TERM_NUMPOS;
        if (tmp == 0)
            goto G4HERROR;

        inbit += tmp;
        tt = (ICL_TBLTYPE)(tt + run + (code & G4G3_TERM_RUN));
        run = 0;
        *codeline++ = tt;
        if (hcnt-- < 1)
            goto G4HORZRET;
        goto G4BLACK;
    }
    else if (!(code & G4G3_MAKEUP_BIT)) {
        /* Makeup code found */
        run += (code & G4G3_MAKEUP_RUN);
        inbit += (code & G4G3_MAKEUP_NUM);
        goto G4WHITE;
    }
    else {
        /* White & Black term codes */
        tt = (ICL_TBLTYPE)(tt + run + ((code & G4G3_DTERM_WRUN) >> G4G3_DTERM_WRUNPOS));
        run = code & G4G3_DTERM_BRUN;
        inbit += (code & G4G3_DTERM_NUM) >> G4G3_DTERM_NUMPOS;
        *codeline++ = tt;
        if (hcnt-- < 1) {
            inbit -= trl_g4g3_htbl_btl[run];
            goto G4HORZRET;
        }
        tt = (ICL_TBLTYPE)(tt + run);
        *codeline++ = tt;
        goto G4HORZRET;
    }


G4BLACK:
    /* Black processing - Same as white, different jumps. */

    /* Check for end of input data */
    indatacur += inbit >> 3;
    inbit &= 7;

    /* if EOD set state & return */
    if (indatacur >= indataend) {
        state = G4G3_STATE_BLACK;
        c->run = run;
        c->tt = tt;
        c->hcnt = hcnt;
        goto G4EOD;
    }

    /* Get code - MSB 8bit data stream read */
    index = (*indatacur << 8) + *(indatacur + 1);
    if (inbit > 3) {
        index = (index << (inbit - 3)) + ((U16)*(indatacur + 2) >> (11 - inbit));
    }
    else if (inbit < 3) {
        index = index >> (3 - inbit);
    }

    /* Get black table code Use 13 bits of index */
    code = trl_g4g3_horz_black[index & 0x1fff];

    if (!(code & G4G3_TERM_BIT)) {
        /* Terminating code found */
        tmp = (code & G4G3_TERM_NUM) >> G4G3_TERM_NUMPOS;
        if (tmp == 0)
            goto G4HERROR;

        inbit += tmp;
        tt = (ICL_TBLTYPE)(tt + run + (code & G4G3_TERM_RUN));
        run = 0;
        *codeline++ = tt;
        if (hcnt-- < 1)
            goto G4HORZRET;
        goto G4WHITE;
    }
    else if (!(code & G4G3_MAKEUP_BIT)) {
        /* Makeup code found */
        run += (code & G4G3_MAKEUP_RUN);
        inbit += (code & G4G3_MAKEUP_NUM);
        goto G4BLACK;
    }
    else {
        /* White & Black term codes */
        tt = (ICL_TBLTYPE)(tt + run + (code & G4G3_DTERM_BRUN));
        run = ((code & G4G3_DTERM_WRUN) >> G4G3_DTERM_WRUNPOS);
        inbit += (code & G4G3_DTERM_NUM) >> G4G3_DTERM_NUMPOS;
        *codeline++ = tt;
        if (hcnt-- < 1) {
            inbit -= trl_g4g3_htbl_wtl[run];
            goto G4HORZRET;
        }
        tt = (ICL_TBLTYPE)(tt + run);
        *codeline++ = tt;
        goto G4HORZRET;
    }


}


int NEAR trl_PassEOL(TRL_G4G3PTR c) {


    int         inbit = c->inbit;
    int         index;
    U8 G4G3_FAR *indatacur = c->indatacur;
    U8 G4G3_FAR *indataend = c->indataend - 3; /* we use -3 so the bit adjustment at end will succeed */

    if (c->state == G4G3_STATE_EOLTEST)
        goto EOLTEST;

    /* Get index */
GETINDEX:
    indatacur += inbit >> 3;
    inbit &= 7;
    /* if EOD set state & return */
    if (indatacur >= indataend) {
        c->state = G4G3_STATE_G4V_GETCODE;
        c->inbit = inbit;
        c->indatacur = indatacur;
        return(G4G3_EOD);
    }

    /* Get index */
    index = (*indatacur << 8) + *(indatacur + 1);
    if (inbit > 5) {
        index = (index << (inbit - 5)) + ((U16)*(indatacur + 2) >> (13 - inbit));
    }
    else if (inbit < 5) {
        index = index >> (5 - inbit);
    }
    /* Check for 11 0s */
    if (index & 0x07ff) {
        /* FALSE */
        if (c->resink) {
            /* if resinking inbit += 1; go back to get index */
            inbit += 1;
            goto GETINDEX;
        }
        /* if not, return not found */
        c->inbit = inbit;
        c->indatacur = indatacur;
        return(G4G3_BADEOL);
    }
    /* TRUE,  set eol */
    c->eolfound = TRUE;
    inbit += 11;

    indatacur += inbit >> 3;
    inbit &= 7;
    if (indatacur >= indataend) {
        /* if EOD, set state and return  EOD */
        c->state = G4G3_STATE_EOLTEST;
        c->inbit = inbit;
        c->indatacur = indatacur;
        return(G4G3_EOD);
    }

EOLTEST:
    /* Find first non 0 byte */
    while (!*indatacur) {
        if (indatacur >= indataend) {
            /* if EOD, set state and return  EOD */
            c->state = G4G3_STATE_EOLTEST;
            c->inbit = inbit;
            c->indatacur = indatacur;
            return(G4G3_EOD);
        }
        indatacur += 1;
    }

    /* Set inbit to first non zero bit pos */
    c->inbit = trl_g4g3_bittbl[*indatacur];
    if (c->inbit == 8) {
        c->inbit = 0;
        indatacur += 1;
    }
    c->indatacur = indatacur;
    return(G4G3_EOF);

} /* trl_PassEOL */



