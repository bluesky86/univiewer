#pragma once

typedef unsigned char   U8;
typedef char            S8;
typedef unsigned short  U16;
typedef unsigned int    U32;
typedef int             S32;
typedef int             IM_BOOL, *IM_BOOLPTR;
typedef double          DOUBLE;
typedef DOUBLE *  DOUBLEPTR;
typedef UINT *    UINTPTR;
typedef U32 *     U32PTR;
typedef INT *     INTPTR;
typedef S8 *      S8PTR;
typedef VOID *    STDPTR;
typedef S32 *     S32PTR;
typedef U16 *     U16PTR;

typedef S32 ICL_TBLTYPE, FAR *ICL_TBLTYPE_PTR;

typedef U16 * G4G3_U16PTR;
typedef U8  * G4G3_U8PTR, * U8PTR;

#define MAX_S32    0x7FFFFFFFL

#define  SUCCESS     0         /* standard success return code.       */
#define  WARNING     1         /* standard warning return code.       */
#define  FAILURE     -1        /* standard failure return code.       */
#define  USER_ABORT  -2        /* standard user abort return code.    */
