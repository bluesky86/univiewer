#include "stdafx.h"
#include "Codec.h"
#include "DebugNew.h"
#include "type.h"

typedef struct _context_nibble {
    CUvImage *pUvImage;
    ICL_TBLTYPE_PTR buffer;
} CONTEXT_TRL, FAR *CONTEXT_TRLPTR;

UNIV_CODEC_API void* SetupNibbleDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size)
{
    CONTEXT_TRLPTR c = NEW CONTEXT_TRL;

    c->pUvImage = pUvImage;

    int bytesPerRow = 0;
    CalculateSize(pUvImage, outNumBits, out_size, &bytesPerRow);

    c->buffer = NEW ICL_TBLTYPE[c->pUvImage->tileWidth + 4];

    return c;
}

UNIV_CODEC_API void CleanupNibbleDecoding(void *cp)
{
    if (cp)
    {
        delete[]((CONTEXT_TRLPTR)cp)->buffer;
        delete (CONTEXT_TRLPTR)cp;
    }
}

S32 _NRLEtoTBL(U8PTR indata, S32 numbytes, ICL_TBLTYPE_PTR outTT);
int ConvertTTTo8Bit(CUvImage *pUvImage, int in_size, int *in_data, int width, char *out);

#define U32_Align( num )  (((num) + 3) & ~3)

UNIV_CODEC_API char* DecodeNibble(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len)
{
    CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;

    int out_size = 0;
    int in_pos = 0;
    char *in_data = pData;
    //char *out_data = out_buffer;
    int lineCount = 0;
    *out_len = 0;
    while (in_pos < data_len && lineCount < c->pUvImage->tileHeight)
    {
        int in_bytes = *(U32PTR)(in_data + in_pos);
        out_size = _NRLEtoTBL((U8PTR)in_data + in_pos + sizeof(U32), in_bytes, c->buffer);
        ConvertTTTo8Bit(c->pUvImage, out_size, c->buffer, c->pUvImage->tileWidth, (char*)&out_buffer[*out_len]);

        in_pos = U32_Align(in_pos + in_bytes + sizeof(U32));
        //out_data += out_size;
        *out_len += c->pUvImage->tileWidth;
        lineCount++;
    }

    return out_buffer;
}

static S32 _NRLEtoTBL(U8PTR indata, S32 numbytes, ICL_TBLTYPE_PTR outTT) {

    ICL_TBLTYPE       HighNibble, LowNibble;
    U8PTR             indataend;
    ICL_TBLTYPE_PTR   outTTStart = outTT;


    indataend = indata + numbytes;

    /* Special processing as there is no previous transition  */

    HighNibble = (ICL_TBLTYPE)(*indata >> 4);
    LowNibble = (ICL_TBLTYPE)(*indata++ & 15);

    while (LowNibble == 0) {

        if (indata >= indataend) {

            *outTT++ = HighNibble;
            if (LowNibble) {
                *outTT = *(outTT - 1) + LowNibble;
                *outTT++;
            }

            goto END_OF_DATA;
        }
        HighNibble = (HighNibble << 4) | (*indata >> 4);
        LowNibble = (ICL_TBLTYPE)(*indata++ & 15);

    }

    *outTT++ = HighNibble;

    if (indata >= indataend) {

        if (LowNibble) {
            *outTT = *(outTT - 1) + LowNibble;
            *outTT++;
        }

        goto END_OF_DATA;
    }

    /* Main loop that decompresses NRLE adding accum value to previous transition */

    for (; ; ) {

        HighNibble = (ICL_TBLTYPE)(*indata >> 4);

        while (HighNibble == 0) {

            LowNibble = (LowNibble << 4) | (*indata++ & 15);

            if (indata >= indataend) {

                *outTT = *(outTT - 1) + LowNibble;
                *outTT++;
                goto END_OF_DATA;
            }

            HighNibble = (ICL_TBLTYPE)(*indata >> 4);
        }

        *outTT = *(outTT - 1) + LowNibble;
        *outTT++;

        LowNibble = (ICL_TBLTYPE)(*indata++ & 15);

        while (LowNibble == 0) {

            if (indata >= indataend) {
                *outTT = *(outTT - 1) + HighNibble;
                *outTT++;
                goto END_OF_DATA;
            }

            HighNibble = (HighNibble << 4) | (ICL_TBLTYPE)(*indata >> 4);
            LowNibble = (ICL_TBLTYPE)(*indata++ & 15);
        }

        *outTT = *(outTT - 1) + HighNibble;
        *outTT++;

        if (indata >= indataend) {
            if (LowNibble) {
                *outTT = *(outTT - 1) + LowNibble;
                *outTT++;
            }
            goto END_OF_DATA;
        }
    }
END_OF_DATA:

    return (S32)(outTT - outTTStart);


} /* _NRLEtoTBL */

