#include "stdafx.h"
#include "Codec.h"
#include "DebugNew.h"

typedef struct _context_raw
{
    CUvImage *pUvImage;
    int in_num_bits;
    int out_num_bits;
    int out_bytesPerRow;

    int in_bytes_per_tile_row;
}CONTEXT_TRL, FAR *CONTEXT_TRLPTR;

UNIV_CODEC_API void CalculateSize(CUvImage *pUvImage, int *outNumBits, int *out_size, int *out_bytesPerRow)
{
    if (*outNumBits == 1 && pUvImage->jpegParams && pUvImage->jpegParams->jpeghead.components > 1)
        *outNumBits = 8 * pUvImage->jpegParams->jpeghead.components;
    else
        *outNumBits = (*outNumBits > 8) ? *outNumBits : 8;

    if (pUvImage->palette && pUvImage->numColors > 0)
        *outNumBits = 32;
    if (*outNumBits == 24) *outNumBits = 32;
    int pad = *outNumBits;
    *out_bytesPerRow = (pUvImage->imageWidth * *outNumBits + pad - 1) / pad * pad / 8;
    *out_size = (*out_bytesPerRow) * pUvImage->imageHeight;

}

UNIV_CODEC_API void* SetupRawDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size)
{
    CONTEXT_TRLPTR c = NEW CONTEXT_TRL;

    c->pUvImage = pUvImage;

    c->in_num_bits = pUvImage->numBits;

    CalculateSize(pUvImage, outNumBits, out_size, &c->out_bytesPerRow);
    c->out_num_bits = *outNumBits;

    int pad = c->in_num_bits == 1 ? 8 : c->in_num_bits;
    c->in_bytes_per_tile_row = (pUvImage->tileWidth * c->in_num_bits + pad - 1) / pad * pad / 8;

    return c;
}

// input decoding struct
UNIV_CODEC_API void CleanupRawDecoding(void *cp)
{
    delete (CONTEXT_TRLPTR)cp;
}

// input decoding struct
UNIV_CODEC_API char* DecodeRaw(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len)
{
    CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;
    if (c->in_num_bits == c->out_num_bits)
    {
        *out_len = data_len;
        memcpy(out_buffer, pData, data_len);
        return out_buffer;
    }

    U8 in_num_bits = (U8)c->in_num_bits;
    int numColors;
    int width;
    if (c->pUvImage->bTiled)
        width = c->pUvImage->tileWidth;
    else
        width = c->pUvImage->imageWidth;

    int byteWidth = c->in_bytes_per_tile_row;
    int height = data_len / byteWidth;

    char *res = nullptr;
    switch (c->out_num_bits)
    {
    case 8:
        res = (char*)convertTo8Bits(in_num_bits, c->pUvImage->palette, numColors, width, height, byteWidth, (U8*)pData, (U8*)out_buffer, in_num_bits, false);
        *out_len = byteWidth * height;
        break;
    case 24:
        res = (char*)convertTo24Bits(in_num_bits, c->pUvImage->palette, width, height, byteWidth, (U8*)pData, (U8*)out_buffer, in_num_bits, c->pUvImage->bFlipRGB, false);
        *out_len = byteWidth * height;
        break;
    case 32:
        res = (char*)convertTo32Bits(in_num_bits, c->pUvImage->palette, width, height, byteWidth, (U8*)pData, (U8*)out_buffer, c->out_num_bits, c->pUvImage->bFlipRGB, false);
        *out_len = byteWidth * height;
        break;
    default:
        *out_len = data_len;
        memcpy(out_buffer, pData, data_len);
        res = out_buffer;
        break;
    }

    return res;
}
