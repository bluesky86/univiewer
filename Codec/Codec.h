#pragma once
#include "CUvImage.h"

#define UNIV_CODEC_API __declspec(dllexport)

// helpers
UNIV_CODEC_API void CalculateSize(CUvImage *pUvImage, int *outNumBits, int *out_size, int *out_bytesPerRow);
UNIV_CODEC_API U8* convertTo8Bits(U8 &srcBitCount, CUvColor *palette, S32 &numCorlor, S32 width, S32 height, S32 &byteWidth, U8 *pBits, U8 *pNewBits, int newPads, bool bFreeOldBits);
UNIV_CODEC_API U8* convertTo24Bits(U8 &srcBitCount, CUvColor *palette, S32 width, S32 height, S32 &byteWidth, U8 *pBits, U8 *pNewBits, int newPads, bool bFlipRGB, bool bFreeOldBits);
UNIV_CODEC_API U8* convertTo32Bits(U8 &srcBitCount, CUvColor *palette, S32 width, S32 height, S32 &byteWidth, U8 *pBits, U8 *pNewBits, int newPads, bool bFlipRGB, bool bFreeOldBits);

//////////////////////////////////////////////////////////////////////////////////
// G3G4
// return: decoding struct
UNIV_CODEC_API void* SetupG3G4Decoding(CUvImage *pUvImage, int *outNumBits, int *out_size);
// input decoding struct
UNIV_CODEC_API void CleanupG3G4Decoding(void *cp);
// input decoding struct
UNIV_CODEC_API char* DecodeG3G4(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len);

//////////////////////////////////////////////////////////////////////////////////
// RAW
UNIV_CODEC_API void* SetupRawDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size);
// input decoding struct
UNIV_CODEC_API void CleanupRawDecoding(void *cp);
// input decoding struct
UNIV_CODEC_API char* DecodeRaw(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len);

//////////////////////////////////////////////////////////////////////////////////
// LZW
// return: decoding struct
UNIV_CODEC_API void* SetupLzwDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size);
// input decoding struct
UNIV_CODEC_API void CleanupLzwDecoding(void *cp);
// input decoding struct
UNIV_CODEC_API char* DecodeLzw(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len);

//////////////////////////////////////////////////////////////////////////////////
// CC1
// return: decoding struct
UNIV_CODEC_API void* SetupCC1Decoding(CUvImage *pUvImage, int *outNumBits, int *out_size);
// input decoding struct
UNIV_CODEC_API void CleanupCC1Decoding(void *cp);
// input decoding struct
UNIV_CODEC_API char* DecodeCC1(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len);

//////////////////////////////////////////////////////////////////////////////////
// Nibble
// return: decoding struct
UNIV_CODEC_API void* SetupNibbleDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size);
// input decoding struct
UNIV_CODEC_API void CleanupNibbleDecoding(void *cp);
// input decoding struct
UNIV_CODEC_API char* DecodeNibble(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len);

//////////////////////////////////////////////////////////////////////////////////
// Jpeg
// return: decoding struct
UNIV_CODEC_API void* SetupJpegDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size);
// input decoding struct
UNIV_CODEC_API void CleanupJpegDecoding(void *cp);
// input decoding struct
UNIV_CODEC_API char* DecodeJpeg(void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len);

//////////////////////////////////////////////////////////////////////////////////
// 24-bit plane
// return: decoding struct
UNIV_CODEC_API void* SetupBPDecoding( CUvImage *pUvImage, int *outNumBits, int *out_size );
// input decoding struct
UNIV_CODEC_API void CleanupBPDecoding( void *cp );
// input decoding struct
UNIV_CODEC_API char* DecodeBP( void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len );

