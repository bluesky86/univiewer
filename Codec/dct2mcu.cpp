#include "stdafx.h"
#include "Codec.h"
#include "DebugNew.h"
#include "type.h"

#define  STATIC            
#define  BLOCKLENGTH       8
#define  BLOCKWIDTH        8
#define  BLOCKLW           (BLOCKLENGTH * BLOCKWIDTH)
#undef   BLOCKSIZE
#define  BLOCKSIZE         (BLOCKLW * sizeof(int))
/* Can handle up to 8x8 sample rate */
#define  MAX_NUM_BLOCKS    66 


#define  A0      91681L
#define  A1     121094L
#define  A2      70936L
#define  A3     171254L
#define  COEF2( c, val ) (( val != 0 )? (int)( ((S32)c*(S32)(val))>>16 ): 0 )


/* convertModes */
#define MCU_DEFAULT 0
#define MCU_Y       1
#define MCU_H1V1    2
#define MCU_H2V1    3
#define MCU_H2V2    4
#define MCU_DIV8    5


/* For 16Bit Based ptr usage */
typedef union _ptrtag {
    STDPTR    ptr;
#ifdef IM_MSWIN_31
    struct _valtag {
        VOID NEAR *off;
        __segment  seg;
    } val;
#else
    struct _valtag {
        STDPTR    off;
    } val;
#endif
} PTRTYPE;

typedef int * DCT_INTPTR;
typedef U8  * DCT_U8PTR;
typedef U32 * DCT_U32PTR;
#undef  BASED
#define BASED 





typedef struct _context_dct {
    int         numBlocks;        /* Number of 8*8 blocks in an MCU block         */
    int         curBlock;         /* Current 8*8 block in the MCU                 */
    int         mcuLength;        /* Number of lines in the MCU block             */
    int         mcuWidth;         /* Number of pixels wide in the MCU block       */
    int         numMCUh;          /* Number of MCU blocks in the width of image   */
    int         numMCUv;          /* Number of MCU blocks in the length of image  */
    int         curMCUh;          /* Current MCU block in progress                */
    int         components;       /* 1 - greyscale, 3 - 24bit colour, 4 - 24bit   */
    int         width;            /* I think we can live with < 32K on 16bit here */
    int         length;           /* I think we can live with < 32K on 16bit here */
    int         sampH, sampV;     /* sample rates for Cb&Cr                       */
    int         sampHinc, sampVinc;/* sample rate increments for Cb&Cr Blocks     */
    int         convertMode;      /* Which conversion function we use             */
    int         colour_conversion; /* 0 - RGB->RGB or maybe CMY->RGB, 1 - YCbCr->RGB */
    int         YCbCrMCU[BLOCKLW*MAX_NUM_BLOCKS];   /* MCU of YCbCr data          */
    int         tifV7height;      /* The height of the tile for Tiff version 7 files */
    int         tifV7End;
    int         count;
    U8          RGB_MCU[BLOCKLW*MAX_NUM_BLOCKS * 3];  /* MCU of R then G and B data */
    U8PTR       outLines[64];     /* Array of output RGB lines                    */
    S32         outNumBits;
    S32         imageWidth;
    S32         imageHeight;
    S32         lineCt;
} CONTEXT_TRL, BASED * CONTEXT_TRLPTR;

STATIC  void _div8_transform(CONTEXT_TRLPTR c, INTPTR pInData);
STATIC  int  _dct_div8_transform(INTPTR src);
STATIC  void _dct_transform(INTPTR src, DCT_INTPTR dest);
STATIC  void _MCU_YCbCr_to_RGB(CONTEXT_TRLPTR c);
STATIC  void _MCU11_RGB_to_RGB(CONTEXT_TRLPTR c);
STATIC  void _MCU11_YCbCr_to_RGB(CONTEXT_TRLPTR c);
STATIC  void _MCU11_CMYK_to_RGB(CONTEXT_TRLPTR c);
STATIC  void _MCU21_YCbCr_to_RGB(CONTEXT_TRLPTR c);
STATIC  void _MCU22_YCbCr_to_RGB(CONTEXT_TRLPTR c);
STATIC  void _MCU_RGB_to_Lines(CONTEXT_TRLPTR c);
STATIC  void _MCU11_RGB_to_Lines(CONTEXT_TRLPTR c);
STATIC  void _MCU21_RGB_to_Lines(CONTEXT_TRLPTR c);
STATIC  void _MCU22_RGB_to_Lines(CONTEXT_TRLPTR c);
STATIC  void _MCU_Y_to_Lines(CONTEXT_TRLPTR c);

STATIC void _CleanUp(CONTEXT_TRLPTR c)
{
    int i;

    if (c) {
        for (i = 0; i < c->mcuLength; i++) {
            if (c->outLines[i]) {
                delete[] (c->outLines[i]);
                c->outLines[i] = NULL;
            }
        }

        delete(c);
    }
}


////////////////////////////////////////////////////////////////////////////////
// @Synopsis : convert huffman code to MCU YCbCr Blocks.
////////////////////////////////////////////////////////////////////////////////
int FAR TRL_TransDCTtoMCU(STDPTR ptr, U8PTR inData, int inSize, U8PTR outData)
{
    int         out_size = 0;
    int         line_count = 0;
    CONTEXT_TRLPTR c;
    PTRTYPE pt;

    pt.ptr = ptr;
    c = (CONTEXT_TRLPTR)pt.val.off;


    if (c->convertMode == MCU_DIV8)
    {
        _div8_transform(c, (INTPTR)inData);
    } /* if convertMode == MCU_DIV8 */
    else
    {
        for (c->curBlock = 0; c->curBlock < c->numBlocks; c->curBlock++) 
        {
            _dct_transform(((INTPTR)inData) + BLOCKLW * c->curBlock, (DCT_INTPTR)&c->YCbCrMCU[0] + BLOCKLW * c->curBlock);
        }

        /* We just filled a MCU, convert to RGB and pass it on */
        switch (c->convertMode) {
        case MCU_H1V1:
            /*<5Nov99 cherylh> For now, there are just to colour transformations:  YCbCr->RGB and RGB->RGB, but there may be
                more transforms in the future. */
            if (c->colour_conversion == RGB2RGB_CCONVERSION)
                _MCU11_RGB_to_RGB(c);
            else if (c->colour_conversion == YCBCR_CCONVERSION)
                _MCU11_YCbCr_to_RGB(c);
            /* < Mar 13, 2009 Hoc > Add CMYK color space support */
            else if (c->colour_conversion == CMYK2RGB_CCONVERSION)
                _MCU11_CMYK_to_RGB(c);
            else
                _MCU11_YCbCr_to_RGB(c);

            _MCU11_RGB_to_Lines(c);
            break;
        case MCU_H2V2:
            _MCU22_YCbCr_to_RGB(c);
            _MCU_RGB_to_Lines(c);
            break;
        case MCU_H2V1:
            _MCU21_YCbCr_to_RGB(c);
            _MCU21_RGB_to_Lines(c);
            break;
        case MCU_Y:
            _MCU_Y_to_Lines(c);
            break;
        default:
            _MCU_YCbCr_to_RGB(c);
            _MCU_RGB_to_Lines(c);
            break;
        }
    }  /* else convertMode == MCU_DIV8 */
    c->curBlock = 0;
    c->curMCUh += 1;
    if (c->curMCUh == c->numMCUh)
    {
        int bytesPerPoint = (c->outNumBits == 32) ? 4 : c->components;
        /* Send out the lines */
        c->curMCUh = 0;
        for (int i = 0; i < c->mcuLength; i++)
        {
            c->count += 1;
            if (c->count >= c->tifV7height && c->count <= c->tifV7End);
            // output nothing because these lines chould be cropped
            else
            {
                if (c->lineCt >= c->imageHeight) break;
                int ptPos = line_count * c->imageWidth * bytesPerPoint;
                for (int j = 0; j < c->imageWidth; j++)
                {
                    outData[ptPos + j * bytesPerPoint] = c->outLines[i][j];
                    if (c->components == 3 || c->components == 4)
                    {
                        outData[ptPos + j * bytesPerPoint + 1] = (c->outLines[i] + c->width)[j];
                        outData[ptPos + j * bytesPerPoint + 2] = (c->outLines[i] + c->width * 2)[j];
                        if (c->outNumBits == 32)
                            outData[ptPos + j * bytesPerPoint + 3] = 255;
                    }
                }
                line_count++;
                c->lineCt++;
                out_size += c->imageWidth * bytesPerPoint;
            }

            if (c->count == c->tifV7End)
                c->count = 0;

        }  /* for i */
    }  /* if curMCUh */
        

    return out_size;

} 

int pl_SetDCTtoMCU(void** c_ptr, JPGHEAD jpeghead, S32 width)
{
    int             samp1, samp2, samp3, samp4, max_samp;
    int             i;

    CONTEXT_TRLPTR  c = (CONTEXT_TRLPTR)*c_ptr;

    /* Calculate the number of 8*8 blocks per MCU */
     /* We are in real trouble if v2,h2,v3,h3 are not 1's */
    samp1 = jpeghead.mcu_v1 * jpeghead.mcu_h1;
    samp2 = jpeghead.mcu_v2 * jpeghead.mcu_h2;
    samp3 = jpeghead.mcu_v3 * jpeghead.mcu_h3;
    samp4 = jpeghead.mcu_v4 * jpeghead.mcu_h4;
    max_samp = max(samp1, max(samp2, max(samp3, samp4)));
    c->numBlocks = samp1 + 2;
    if (jpeghead.components == 4)  // Add CMYK color space support
        c->numBlocks++;

    /* Calculate the number of MCU blocks in width, length */
    c->sampH = jpeghead.mcu_h1;
    c->numMCUh = (jpeghead.width + (c->sampH * BLOCKWIDTH - 1)) / (c->sampH * BLOCKWIDTH);
    c->numMCUh = (int)(width + (c->sampH * BLOCKWIDTH - 1)) / (c->sampH * BLOCKWIDTH);
    c->sampV = jpeghead.mcu_v1;
    c->numMCUv = (jpeghead.height + (c->sampV * BLOCKLENGTH - 1)) / (c->sampV * BLOCKLENGTH);
    c->curMCUh = 0;
    c->sampHinc = BLOCKWIDTH / c->sampH;
    c->sampVinc = BLOCKLENGTH / c->sampV * BLOCKWIDTH;

    c->components = jpeghead.components;
    c->width = c->numMCUh * BLOCKWIDTH  * c->sampH;
    c->length = c->numMCUv * BLOCKLENGTH * c->sampV;
    c->mcuLength = BLOCKLENGTH * c->sampV;
    c->mcuWidth = BLOCKWIDTH * c->sampH;

    if (jpeghead.tifV7height) {
        c->tifV7height = jpeghead.tifV7height;
        c->tifV7End = (jpeghead.tifV7height % 8 == 0 ? jpeghead.tifV7height : ((jpeghead.tifV7height / 8) + 1) * 8);
        c->count = 0;// (S32)pComp->y1;
    }
    else {
        c->tifV7height = 0;
        c->tifV7End = 0;
        c->count = 0;
    }

    /* Set the ConvertMode */
    c->convertMode = MCU_DEFAULT;
    if (c->sampH == 1 && c->sampV == 1)
        c->convertMode = MCU_H1V1;
    if (c->sampH == 2 && c->sampV == 1)
        c->convertMode = MCU_H2V1;
    if (c->sampH == 2 && c->sampV == 2)
        c->convertMode = MCU_H2V2;
    if (c->components == 1) {
        c->convertMode = MCU_Y;
        c->numBlocks = 1;
    }

    //  Get the colour_conversion flag.
    c->colour_conversion = (int)jpeghead.colour_conversion;

    /* Allocate output buffers */
    for (i = 0; i < c->mcuLength; i++) {
        if (c->outLines[i]) delete[] (c->outLines[i]);
        c->outLines[i] = NULL;
        c->outLines[i] = NEW U8[(c->width + 3) * c->components]; /* Add 3 since we move 4 bytes at a time */
        if (c->outLines[i] == NULL)
        {
            _CleanUp(c);
            return(FAILURE);
        }
    }
    return (SUCCESS);
}

////////////////////////////////////////////////////////////////////////////////
// @Synopsis : convert MCU DCT data to MCU YCbCr Blocks.
////////////////////////////////////////////////////////////////////////////////

void* TRL_OpenDCTtoMCU(JPGHEAD jpeghead, S32 width, S32 outNumBits)
{
    CONTEXT_TRLPTR  c = NULL;
    PTRTYPE         pt;

    pt.ptr = NEW CONTEXT_TRL;

    /* Need to clear the output line buffers */
    memset(pt.ptr, 0, sizeof(CONTEXT_TRL));

    c = (CONTEXT_TRLPTR)pt.val.off;

    c->imageHeight = jpeghead.height;
    c->imageWidth = jpeghead.width;

    if (pl_SetDCTtoMCU((void**)&c, jpeghead, width) == FAILURE)
    {
        _CleanUp(c);
        return nullptr;
    }

    c->outNumBits = outNumBits;
    c->lineCt = 0;

    //if (c->components == 3 || jpeghead.colour_conversion == CMYK2RGB_CCONVERSION)
    //    rslType = RSL_IDF_24BP;
    //else
    //    rslType = RSL_IDF_8BIT;

    return c;

}

void CloseDCTtoMCU(void *cp)
{
    CONTEXT_TRLPTR  c = (CONTEXT_TRLPTR)cp;
    _CleanUp(c);
}




STATIC void _div8_transform(CONTEXT_TRLPTR c, INTPTR pInData) {

    U8PTR out;

    int Y, Cb, Cr, K;
    int R, G, B;
    int tt, ta, tb, tc;

    int i, j;

    if (c->components == 1) {
        /* Greyscale only - not sampled 1 Y block to process */
        Y = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++) + 128;
        if (Y & 0xff00) {
            if ((Y & 0xff00) == 0xff00)
                Y = 0;
            else
                Y = 255;
        }
        *(c->outLines[0] + c->curMCUh) = (U8)Y;
    }

    else {
        const int offset = c->curMCUh * c->mcuWidth;
        const int width = c->width;
        int   outLine = 0;

        if (c->sampH * c->sampV > 1) {
            if (c->components == 4) {  /* < Mar 13, 2009 Hoc > Add CMYK color space support */
               /* Decode Cb block */
                _dct_transform(pInData + BLOCKLW * (c->numBlocks - 3), (DCT_INTPTR)&c->YCbCrMCU[0]);
                /* Decode Cr block */
                _dct_transform(pInData + BLOCKLW * (c->numBlocks - 2), (DCT_INTPTR)&c->YCbCrMCU[BLOCKLW]);
                /* Decode Y block */
                _dct_transform(pInData + BLOCKLW * (c->numBlocks - 1), (DCT_INTPTR)&c->YCbCrMCU[BLOCKLW + BLOCKLW]);
            }
            else {  /* existing code */
            /* Decode Cb block */
                _dct_transform(pInData + BLOCKLW * (c->numBlocks - 2), (DCT_INTPTR)&c->YCbCrMCU[0]);
                /* Decode Cr block */
                _dct_transform(pInData + BLOCKLW * (c->numBlocks - 1), (DCT_INTPTR)&c->YCbCrMCU[BLOCKLW]);
            }
            for (i = 0; i < BLOCKLW; i += c->sampVinc) {
                out = c->outLines[outLine++] + offset;
                for (j = 0; j < BLOCKWIDTH; j += c->sampHinc) {
                    if (c->components == 4) {  /* < Mar 13, 2009 Hoc > Add CMYK color space support */
                        Y = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++);
                        Cb = c->YCbCrMCU[i + j];
                        Cr = c->YCbCrMCU[i + j + BLOCKLW];
                        K = c->YCbCrMCU[i + j + BLOCKLW + BLOCKLW];
                        R = min(255, Y + K);
                        G = min(255, Cb + K);
                        B = min(255, Cr + K);
                    }
                    else {  /* existing code */
                        Y = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++) + 128;
                        Cb = c->YCbCrMCU[i + j];
                        Cr = c->YCbCrMCU[i + j + BLOCKLW];
                        tt = Cr * 23;
                        ta = (2 * tt - Cr) / 32;
                        tb = -(11 * Cb + tt) / 32;
                        tc = 7 * Cb / 4;
                        R = Y + ta;
                        G = Y + tb;
                        B = Y + tc;
                    }
                    if (R & 0xff00) {
                        if ((R & 0xff00) == 0xff00)
                            R = 0;
                        else
                            R = 255;
                    }
                    if (G & 0xff00) {
                        if ((G & 0xff00) == 0xff00)
                            G = 0;
                        else
                            G = 255;
                    }
                    if (B & 0xff00) {
                        if ((B & 0xff00) == 0xff00)
                            B = 0;
                        else
                            B = 255;
                    }
                    out[0] = (U8)R;
                    out[width] = (U8)G;
                    out[width * 2] = (U8)B;
                    out += 1;
                }
            }
        }
        else {
            if (c->components == 4) {  /* < Mar 13, 2009 Hoc > Add CMYK color space support */
                Y = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++);
                Cb = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++);
                Cr = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++);
                K = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++);
                R = min(255, Y + K);
                G = min(255, Cb + K);
                B = min(255, Cr + K);
            }
            else {  /* existing code */
                Y = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++) + 128;
                Cb = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++);
                Cr = _dct_div8_transform(pInData + BLOCKLW * c->curBlock++);
                tt = Cr * 23;
                ta = (2 * tt - Cr) / 32;
                tb = -(11 * Cb + tt) / 32;
                tc = 7 * Cb / 4;
                R = Y + ta;
                G = Y + tb;
                B = Y + tc;
            }
            if (R & 0xff00) {
                if ((R & 0xff00) == 0xff00)
                    R = 0;
                else
                    R = 255;
            }
            if (G & 0xff00) {
                if ((G & 0xff00) == 0xff00)
                    G = 0;
                else
                    G = 255;
            }
            if (B & 0xff00) {
                if ((B & 0xff00) == 0xff00)
                    B = 0;
                else
                    B = 255;
            }
            out = c->outLines[0] + offset;
            out[0] = (U8)R;
            out[width] = (U8)G;
            out[width * 2] = (U8)B;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// @Synopsis : Same as _dct_transform but only output first term.
//              The first term is the sum of all 64 terms.
////////////////////////////////////////////////////////////////////////////////
STATIC  int _dct_div8_transform(INTPTR src)
{
    int i = 0;
    int sum = 0;

    for (i = 0; i < 64; i += 4) {
        int a = src[i] + src[i + 1];
        int b = src[i + 2] + src[i + 3];
        sum += a + b;
    }
    sum = sum >> 6;

    return(sum);
}



////////////////////////////////////////////////////////////////////////////////
// @Synopsis : Convert  DCT coef. in 8 x 8 block to MCU data
//              This IDCT is based on the algorithm designed by Winograd
//              ( Book: JPEG -- Still image data compression standard P50 )
//              29Oct97(Brian)  Some of the transformation has been moved
//              to the previous translator to remove an extra multiply.
////////////////////////////////////////////////////////////////////////////////
STATIC  void _dct_transform(INTPTR src, DCT_INTPTR dest)
{
    int       i = 0;
    int       z5, z10, z11, z12, z13;
    int       tmp10, tmp11, tmp12, tmp13;
    int       tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7;
    INTPTR dataptr;
    DCT_INTPTR outptr;

    outptr = dest;
    dataptr = src;

    /* Pass 1: process rows. */
    for (i = BLOCKLENGTH - 1; i >= 0; i--) {
        /* even part */
        tmp0 = dataptr[0];
        tmp1 = dataptr[2];
        tmp2 = dataptr[4];
        tmp3 = dataptr[6];

        tmp10 = tmp0 + tmp2;
        tmp11 = tmp0 - tmp2;
        tmp13 = tmp1 + tmp3;

        tmp12 = COEF2(A0, (tmp1 - tmp3)) - tmp13;
        tmp0 = tmp10 + tmp13;
        tmp1 = tmp11 + tmp12;
        tmp2 = tmp11 - tmp12;
        tmp3 = tmp10 - tmp13;

        /* Odd part */
        tmp4 = dataptr[1];
        tmp5 = dataptr[3];
        tmp6 = dataptr[5];
        tmp7 = dataptr[7];

        z13 = tmp6 + tmp5;
        z10 = tmp6 - tmp5;
        z11 = tmp4 + tmp7;
        z12 = tmp4 - tmp7;

        tmp7 = z11 + z13;
        tmp11 = COEF2(A0, (z11 - z13));

        z5 = COEF2(A1, (z10 + z12));
        tmp10 = COEF2(A2, z12) - z5;
        tmp12 = z5 - COEF2(A3, z10);

        tmp6 = tmp12 - tmp7;
        tmp5 = tmp11 - tmp6;
        tmp4 = tmp10 + tmp5;

        dest[0] = tmp0 + tmp7;
        dest[7] = tmp0 - tmp7;
        dest[1] = tmp1 + tmp6;
        dest[6] = tmp1 - tmp6;
        dest[2] = tmp2 + tmp5;
        dest[5] = tmp2 - tmp5;
        dest[4] = tmp3 + tmp4;
        dest[3] = tmp3 - tmp4;

        dataptr += BLOCKLENGTH;
        dest += BLOCKLENGTH;		/* advance pointer to next row */
    }

    /* Pass 2: process columns. */
    dest = outptr;
    for (i = BLOCKLENGTH - 1; i >= 0; i--) {
        /* even part */
        tmp0 = dest[0];
        tmp1 = dest[16];
        tmp2 = dest[32];
        tmp3 = dest[48];

        tmp10 = tmp0 + tmp2;               /* phase 3 */
        tmp11 = tmp0 - tmp2;

        tmp13 = tmp1 + tmp3;              /* phases 5-3 */

        tmp12 = COEF2(A0, (tmp1 - tmp3)) - tmp13;

        tmp0 = tmp10 + tmp13;	            /* phase 2 */
        tmp3 = tmp10 - tmp13;
        tmp1 = tmp11 + tmp12;
        tmp2 = tmp11 - tmp12;

        /* Odd part */
        tmp4 = dest[8];
        tmp5 = dest[24];
        tmp6 = dest[40];
        tmp7 = dest[56];

        z13 = tmp6 + tmp5;
        z10 = tmp6 - tmp5;
        z11 = tmp4 + tmp7;
        z12 = tmp4 - tmp7;

        tmp7 = z11 + z13;
        tmp11 = COEF2(A0, (z11 - z13));

        z5 = COEF2(A1, (z10 + z12));
        tmp10 = COEF2(A2, z12) - z5;
        tmp12 = z5 - COEF2(A3, z10);

        tmp6 = tmp12 - tmp7;
        tmp5 = tmp11 - tmp6;
        tmp4 = tmp10 + tmp5;

        dest[0] = (tmp0 + tmp7) >> 6;
        dest[56] = (tmp0 - tmp7) >> 6;
        dest[8] = (tmp1 + tmp6) >> 6;
        dest[48] = (tmp1 - tmp6) >> 6;
        dest[16] = (tmp2 + tmp5) >> 6;
        dest[40] = (tmp2 - tmp5) >> 6;
        dest[32] = (tmp3 + tmp4) >> 6;
        dest[24] = (tmp3 - tmp4) >> 6;

        dest++;			/* advance pointer to next column */
    }
}



////////////////////////////////////////////////////////////////////////////////
// @Synopsis : Convert a MCU set of Y values to Line data
// @Assumptions: 1 Y block, 8*8 output.
////////////////////////////////////////////////////////////////////////////////
STATIC   void _MCU_Y_to_Lines(CONTEXT_TRLPTR c) {

    const int offset = c->curMCUh * c->mcuWidth;

    int Y;
    int i, j;
    DCT_INTPTR Yin = &c->YCbCrMCU[0];
    U8PTR      Yout;

    for (i = 0; i < BLOCKWIDTH; i++) {
        Yout = c->outLines[i] + offset;
        for (j = 0; j < BLOCKLENGTH; j++) {
            Y = *Yin++ + 128;
            if (Y & 0xff00) {
                if ((Y & 0xff00) == 0xff00)
                    Y = 0;
                else
                    Y = 255;
            }
            *Yout++ = (U8)Y;
        }
    }
}  /* _MCU_Y_to_Lines */

STATIC   void _MCU_YCbCr_to_RGB(CONTEXT_TRLPTR c) {

    const DCT_INTPTR CbBlock = &c->YCbCrMCU[(c->numBlocks - 2) * BLOCKLW];
    const DCT_INTPTR CrBlock = &c->YCbCrMCU[(c->numBlocks - 1) * BLOCKLW];
    DCT_INTPTR YBase;
    DCT_U8PTR  RGB_Base;

    const int sampH = c->sampH;
    const int sampV = c->sampV;
    const int sampHV = sampH * sampV;
    const int rgb_BlockSize = sampHV * BLOCKLW;
    int Y, Cb, Cr;                   /* YCbCr values */
    int r, g, b;                     /* RGB values */
    int tt, ta, tb, tc;              /* tmp vars to hold calculations */
    int i, k, l;                       /* loop counters */
    int index;                       /* index into Y block */


    for (i = 0; i < BLOCKLW; i++) {
        Cb = CbBlock[i];
        Cr = CrBlock[i];

        tt = Cr * 23;
        ta = (2 * tt - Cr) / 32;
        tb = -(11 * Cb + tt) / 32;
        tc = 7 * Cb / 4;

        index = (i % BLOCKWIDTH) / (BLOCKWIDTH / sampH) * BLOCKLW;           /* Which Y block horizontally */
        index += (i % (BLOCKWIDTH / sampH)) * sampH;                          /* Which Y value horizontal   */
        index += (i / BLOCKLENGTH) / (BLOCKLENGTH / sampV) * BLOCKLW * sampH; /* Which Y block vertically   */
        index += ((i / BLOCKLENGTH) % (BLOCKLENGTH / sampV)) * BLOCKWIDTH * sampV;/* Which Y value vertical */
        YBase = &c->YCbCrMCU[index];
        RGB_Base = c->RGB_MCU + (i & 7) * sampH + (i / 8) * sampHV * BLOCKWIDTH;

        for (k = 0; k < sampV; k++) {
            for (l = 0; l < sampH; l++) {
                Y = YBase[l] + 128;

                r = Y + ta;
                g = Y + tb;
                b = Y + tc;

                /* Clamp r,g,b to 0 to 255 range */
                if (r & 0xff00) {
                    if ((r & 0xff00) == 0xff00)
                        r = 0;
                    else
                        r = 255;
                }
                if (g & 0xff00) {
                    if ((g & 0xff00) == 0xff00)
                        g = 0;
                    else
                        g = 255;
                }
                if (b & 0xff00) {
                    if ((b & 0xff00) == 0xff00)
                        b = 0;
                    else
                        b = 255;
                }

                /* Output r,g,b */
                RGB_Base[l] = (U8)r;
                RGB_Base[l + rgb_BlockSize] = (U8)g;
                RGB_Base[l + rgb_BlockSize * 2] = (U8)b;
            }  /* for l */
            YBase += BLOCKWIDTH;
            RGB_Base += BLOCKWIDTH * sampH;
        }  /* for k */
    }  /* for i */

}  /* _MCU_YCbCr_to_RGB */



STATIC   void _MCU11_YCbCr_to_RGB(CONTEXT_TRLPTR c) {

    const DCT_INTPTR CbBlock = &c->YCbCrMCU[(c->numBlocks - 2) * BLOCKLW];
    const DCT_INTPTR CrBlock = &c->YCbCrMCU[(c->numBlocks - 1) * BLOCKLW];
    const DCT_INTPTR YBlock = &c->YCbCrMCU[0];
    const DCT_U8PTR  RGB_Block = &c->RGB_MCU[0];

    int Y, Cb, Cr;                   /* YCbCr values */
    int r, g, b;                     /* RGB values */
    int tt, ta, tb, tc;              /* tmp vars to hold calculations */
    int i;                           /* loop counters */


    for (i = 0; i < BLOCKLW; i++) {
        Cb = CbBlock[i];
        Cr = CrBlock[i];
        Y = YBlock[i] + 128;

        tt = Cr * 23;
        ta = (2 * tt - Cr) / 32;
        tb = -(11 * Cb + tt) / 32;
        tc = 7 * Cb / 4;


        r = Y + ta;
        g = Y + tb;
        b = Y + tc;

        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }

        /* Output r,g,b */
        RGB_Block[i] = (U8)r;
        RGB_Block[i + BLOCKLW] = (U8)g;
        RGB_Block[i + BLOCKLW * 2] = (U8)b;
    }  /* for i */

}  /* _MCU11_YCbCr_to_RGB */


////////////////////////////////////////////////////////////////////////////////
//  @Synopsis : Convert a MCU set of CMYK data to an MCU unit of RGB data
//  @Assumptions: Many C, M, Y, and K blocks.
////////////////////////////////////////////////////////////////////////////////
STATIC  void _MCU11_CMYK_to_RGB(CONTEXT_TRLPTR c)
{
    const DCT_INTPTR CBlock = &c->YCbCrMCU[0];
    const DCT_INTPTR MBlock = &c->YCbCrMCU[(c->numBlocks - 3) * BLOCKLW];
    const DCT_INTPTR YBlock = &c->YCbCrMCU[(c->numBlocks - 2) * BLOCKLW];
    const DCT_INTPTR KBlock = &c->YCbCrMCU[(c->numBlocks - 1) * BLOCKLW];
    const DCT_U8PTR  RGB_Block = &c->RGB_MCU[0];

    int C, M, Y, K;                  /* CMYK values */
    int r, g, b;                     /* RGB values */
    int i;                           /* loop counters */

    for (i = 0; i < BLOCKLW; i++) {
        C = CBlock[i];
        M = MBlock[i];
        Y = YBlock[i];
        K = KBlock[i];

        r = min(255, C + K);
        g = min(255, M + K);
        b = min(255, Y + K);

        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }

        /* Output r,g,b */
        RGB_Block[i] = (U8)r;
        RGB_Block[i + BLOCKLW] = (U8)g;
        RGB_Block[i + BLOCKLW * 2] = (U8)b;
    }  /* for i */
}  /* _MCU11_CMYK_to_RGB */

STATIC   void _MCU11_RGB_to_RGB(CONTEXT_TRLPTR c) {

    const DCT_INTPTR CbBlock = &c->YCbCrMCU[(c->numBlocks - 2) * BLOCKLW];
    const DCT_INTPTR CrBlock = &c->YCbCrMCU[(c->numBlocks - 1) * BLOCKLW];
    const DCT_INTPTR YBlock = &c->YCbCrMCU[0];
    const DCT_U8PTR  RGB_Block = &c->RGB_MCU[0];

    int r, g, b;                     /* RGB values */
    int i;                           /* loop counters */


    for (i = 0; i < BLOCKLW; i++) {
        g = CbBlock[i] + 128;
        b = CrBlock[i] + 128;
        r = YBlock[i] + 128;

        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }

        /* Output r,g,b */
        RGB_Block[i] = (U8)r;
        RGB_Block[i + BLOCKLW] = (U8)g;
        RGB_Block[i + BLOCKLW * 2] = (U8)b;
    }  /* for i */

}  /* _MCU11_RGB_to_RGB */

STATIC   void _MCU21_YCbCr_to_RGB(CONTEXT_TRLPTR c) {

    const DCT_INTPTR CbBlock = &c->YCbCrMCU[(c->numBlocks - 2) * BLOCKLW];
    const DCT_INTPTR CrBlock = &c->YCbCrMCU[(c->numBlocks - 1) * BLOCKLW];
    DCT_INTPTR YBase;
    DCT_U8PTR  RGB_Base;

    const int rgb_BlockSize = 2 * BLOCKLW;
    const int tbl[8] = { 0, 0, 0, BLOCKLW - BLOCKWIDTH, 0, 0, 0, -BLOCKLW };
    int Y, Cb, Cr;                   /* YCbCr values */
    int r, g, b;                     /* RGB values */
    int tt, ta, tb, tc;              /* tmp vars to hold calculations */
    int i;                           /* loop counters */


    RGB_Base = c->RGB_MCU;
    YBase = &c->YCbCrMCU[0];
    for (i = 0; i < BLOCKLW; i++) {
        Cb = CbBlock[i];
        Cr = CrBlock[i];

        tt = Cr * 23;
        ta = (2 * tt - Cr) / 32;
        tb = -(11 * Cb + tt) / 32;
        tc = 7 * Cb / 4;

        Y = *YBase++ + 128;
        r = Y + ta;
        g = Y + tb;
        b = Y + tc;
        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }
        /* Output r,g,b */
        RGB_Base[0] = (U8)r;
        RGB_Base[0 + rgb_BlockSize] = (U8)g;
        RGB_Base[0 + rgb_BlockSize * 2] = (U8)b;
        RGB_Base += 1;

        Y = *YBase++ + 128;
        r = Y + ta;
        g = Y + tb;
        b = Y + tc;
        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }
        /* Output r,g,b */
        RGB_Base[0] = (U8)r;
        RGB_Base[rgb_BlockSize] = (U8)g;
        RGB_Base[rgb_BlockSize * 2] = (U8)b;
        RGB_Base += 1;

        YBase += tbl[i & 7];
    }  /* for i */

}  /* _MCU21_YCbCr_to_RGB */

STATIC   void _MCU22_YCbCr_to_RGB(CONTEXT_TRLPTR c) {

    const DCT_INTPTR CbBlock = &c->YCbCrMCU[(c->numBlocks - 2) * BLOCKLW];
    const DCT_INTPTR CrBlock = &c->YCbCrMCU[(c->numBlocks - 1) * BLOCKLW];
    DCT_INTPTR YBase;
    DCT_U8PTR  RGB_Base;

    const int rgb_BlockSize = 4 * BLOCKLW;
    const int rgb_LineSize = 2 * BLOCKWIDTH;
    const int tbl[8] = { 0, 0, 0, BLOCKLW - BLOCKWIDTH, 0, 0, 0, -BLOCKLW + BLOCKWIDTH };
    const int tbl2[8] = { -2 * BLOCKWIDTH + 1,-2 * BLOCKWIDTH + 1,-2 * BLOCKWIDTH + 1,-2 * BLOCKWIDTH + 1,
                     -2 * BLOCKWIDTH + 1,-2 * BLOCKWIDTH + 1,-2 * BLOCKWIDTH + 1,1 };
    int Y, Cb, Cr;                   /* YCbCr values */
    int r, g, b;                     /* RGB values */
    int tt, ta, tb, tc;              /* tmp vars to hold calculations */
    int i;                           /* loop counters */


    RGB_Base = c->RGB_MCU;
    YBase = &c->YCbCrMCU[0];
    for (i = 0; i < BLOCKLW; i++) {
        Cb = CbBlock[i];
        Cr = CrBlock[i];

        tt = Cr * 23;
        ta = (2 * tt - Cr) / 32;
        tb = -(11 * Cb + tt) / 32;
        tc = 7 * Cb / 4;

        Y = *YBase++ + 128;
        r = Y + ta;
        g = Y + tb;
        b = Y + tc;
        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }
        /* Output r,g,b */
        RGB_Base[0] = (U8)r;
        RGB_Base[0 + rgb_BlockSize] = (U8)g;
        RGB_Base[0 + rgb_BlockSize * 2] = (U8)b;
        RGB_Base += 1;

        Y = *YBase + 128;
        YBase += 7;
        r = Y + ta;
        g = Y + tb;
        b = Y + tc;
        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }
        /* Output r,g,b */
        RGB_Base[0] = (U8)r;
        RGB_Base[rgb_BlockSize] = (U8)g;
        RGB_Base[rgb_BlockSize * 2] = (U8)b;
        RGB_Base += rgb_LineSize - 1;

        Y = *YBase++ + 128;
        r = Y + ta;
        g = Y + tb;
        b = Y + tc;
        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }
        /* Output r,g,b */
        RGB_Base[0] = (U8)r;
        RGB_Base[0 + rgb_BlockSize] = (U8)g;
        RGB_Base[0 + rgb_BlockSize * 2] = (U8)b;
        RGB_Base += 1;

        Y = *YBase + 128;
        YBase -= 7;
        r = Y + ta;
        g = Y + tb;
        b = Y + tc;
        /* Clamp r,g,b to 0 to 255 range */
        if (r & 0xff00) {
            if ((r & 0xff00) == 0xff00)
                r = 0;
            else
                r = 255;
        }
        if (g & 0xff00) {
            if ((g & 0xff00) == 0xff00)
                g = 0;
            else
                g = 255;
        }
        if (b & 0xff00) {
            if ((b & 0xff00) == 0xff00)
                b = 0;
            else
                b = 255;
        }
        /* Output r,g,b */
        RGB_Base[0] = (U8)r;
        RGB_Base[rgb_BlockSize] = (U8)g;
        RGB_Base[rgb_BlockSize * 2] = (U8)b;

        RGB_Base += tbl2[i & 7];
        if (i != 31)
            YBase += tbl[i & 7];
        else
            YBase += 8;
    }  /* for i */

}  /* _MCU22_YCbCr_to_RGB */

////////////////////////////////////////////////////////////////////////////////
// @Synopsis : Convert a MCU set of YCbCr data to an MCU unit of RGB data
//  @Assumptions: Many Y's, 1Cr 1Cb block.
////////////////////////////////////////////////////////////////////////////////
STATIC void _MCU_RGB_to_Lines(CONTEXT_TRLPTR c) {

    const int width8 = c->sampH * BLOCKWIDTH / 8;
    const int length = c->sampV * BLOCKLENGTH;
    const int rgb_BlockSize = c->sampH * c->sampV * BLOCKLW;
    const int width = c->width;
    const int offset = c->curMCUh * c->mcuWidth;

    DCT_U8PTR RGB_Block = &c->RGB_MCU[0];
    U32   a, b;
    U8PTR out;
    int   i, j;

    for (i = 0; i < length; i++) {
        RGB_Block = &c->RGB_MCU[i * c->sampH * BLOCKWIDTH];
        out = c->outLines[i] + offset;
        for (j = 0; j < width8; j++) {
            /* copy out Red */
            a = *(U32PTR)RGB_Block;
            b = *(U32PTR)(RGB_Block + 4);
            *(S32PTR)out = a;
            *(S32PTR)(out + 4) = b;
            /* copy out Green */
            out += width;
            RGB_Block += rgb_BlockSize;
            a = *(U32PTR)RGB_Block;
            b = *(U32PTR)(RGB_Block + 4);
            *(S32PTR)out = a;
            *(S32PTR)(out + 4) = b;
            /* copy out Green */
            out += width;
            RGB_Block += rgb_BlockSize;
            a = *(U32PTR)RGB_Block;
            b = *(U32PTR)(RGB_Block + 4);
            *(S32PTR)out = a;
            *(S32PTR)(out + 4) = b;

            /* end of loop fixups */
            out -= 2 * width;
            RGB_Block -= 2 * rgb_BlockSize;
            out += 8;
            RGB_Block += 8;
        }  /* for j */
    }  /* for i */

} /* _MCU_RGB_to_Lines */

STATIC void _MCU11_RGB_to_Lines(CONTEXT_TRLPTR c) {

    const int width = c->width;
    const int offset = c->curMCUh * BLOCKWIDTH;

    DCT_U8PTR   RGB_Block;
    U8PTR       out;
    int         i;

    for (i = 0; i < BLOCKLENGTH; i++) {
        out = c->outLines[i] + offset;
        RGB_Block = &c->RGB_MCU[i*BLOCKWIDTH];
        /* copy out Red */
        *(U32PTR)out = *(DCT_U32PTR)RGB_Block;
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + 4);
        /* copy out Green */
        out += width;
        *(U32PTR)out = *(DCT_U32PTR)(RGB_Block + BLOCKLW);
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + BLOCKLW + 4);
        /* copy out Green */
        out += width;
        *(U32PTR)out = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 2);
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 2 + 4);

    }  /* for i */

} /* _MCU11_RGB_to_Lines */

STATIC void _MCU21_RGB_to_Lines(CONTEXT_TRLPTR c) {

    const int width = c->width;
    const int offset = c->curMCUh * BLOCKWIDTH * 2;

    DCT_U8PTR   RGB_Block;
    U8PTR       out;
    int         i;

    for (i = 0; i < BLOCKLENGTH; i++) {
        out = c->outLines[i] + offset;
        RGB_Block = &c->RGB_MCU[i*BLOCKWIDTH * 2];
        /* copy out Red */
        *(U32PTR)out = *(DCT_U32PTR)RGB_Block;
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + 4);
        *(U32PTR)(out + 8) = *(DCT_U32PTR)(RGB_Block + 8);
        *(U32PTR)(out + 12) = *(DCT_U32PTR)(RGB_Block + 12);
        /* copy out Green */
        out += width;
        *(U32PTR)out = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 2);
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 2 + 4);
        *(U32PTR)(out + 8) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 2 + 8);
        *(U32PTR)(out + 12) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 2 + 12);
        /* copy out Green */
        out += width;
        *(U32PTR)out = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4);
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4 + 4);
        *(U32PTR)(out + 8) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4 + 8);
        *(U32PTR)(out + 12) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4 + 12);

    }  /* for i */

} /* _MCU21_RGB_to_Lines */

STATIC void _MCU22_RGB_to_Lines(CONTEXT_TRLPTR c) {

    const int width = c->width;
    const int offset = c->curMCUh * BLOCKWIDTH * 2;

    DCT_U8PTR   RGB_Block;
    U8PTR       out;
    int         i;

    for (i = 0; i < BLOCKLENGTH * 2; i++) {
        out = c->outLines[i] + offset;
        RGB_Block = &c->RGB_MCU[i*BLOCKWIDTH * 2];
        /* copy out Red */
        *(U32PTR)out = *(DCT_U32PTR)RGB_Block;
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + 4);
        *(U32PTR)(out + 8) = *(DCT_U32PTR)(RGB_Block + 8);
        *(U32PTR)(out + 12) = *(DCT_U32PTR)(RGB_Block + 12);
        /* copy out Green */
        out += width;
        *(U32PTR)out = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4);
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4 + 4);
        *(U32PTR)(out + 8) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4 + 8);
        *(U32PTR)(out + 12) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 4 + 12);
        /* copy out Green */
        out += width;
        *(U32PTR)out = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 8);
        *(U32PTR)(out + 4) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 8 + 4);
        *(U32PTR)(out + 8) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 8 + 8);
        *(U32PTR)(out + 12) = *(DCT_U32PTR)(RGB_Block + BLOCKLW * 8 + 12);

    }  /* for i */

} /* _MCU22_RGB_to_Lines */


