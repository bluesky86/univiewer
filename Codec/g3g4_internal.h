#pragma once
#include "type.h"

#ifndef  INC_G4G3
#define  INC_G4G3

/* Use inline ASM code for MSWIN */
#define G4G3_FAR

#define  SUCCESS     0         /* standard success return code.       */
#define  WARNING     1         /* standard warning return code.       */
#define  FAILURE     -1        /* standard failure return code.       */
#define  USER_ABORT  -2        /* standard user abort return code.    */

/* Set the tile and transition table limits. */
#define G4G3_MAX_TT_BUFSIZE    64000

/*
 * Tables
 */
extern U8   trl_g4g3_vert_compx[];
extern U8   trl_g4g3_vert_singl[];
extern U16  trl_g4g3_horz_black[];
extern U16  trl_g4g3_horz_white[];
extern U8   trl_g4g3_htbl_btl[];
extern U8   trl_g4g3_htbl_wtl[];
extern U8   trl_g4g3_bittbl[];


/* Defines for line type to be compressed */
/* Note this table is also defined in g4pcomp.h */
#define G4C_LINE_NONE    0
#define G4C_LINE_BLANK   1
#define G4C_LINE_DUP     2
#define G4C_LINE_G4      3
#define G4C_LINE_G3      4
#define G4C_LINE_XORG3   5
#define G4C_LINE_RAW     6




/*
 * IMPORTANT NOTE:  This structure assumes that the sizeof(unsigned)
 * is the same as ICL_TBLTYPE or a contant multiple of it.  If this is
 * not true than the G4 Engine will not work for tiles. - MarkG
 */
typedef struct _ttinfo {

    unsigned    Length;       /* Length of Current TTInfo Line */
    ICL_TBLTYPE StartData[2]; /* start position of tile */
    ICL_TBLTYPE Data[1];      /* 1 Line of Transition Table Data for current tile */

} G4G3_TTINFO, FAR *G4G3_TTINFOPTR;


#define G4G3_TTINFOHDRSIZE       (sizeof(G4G3_TTINFO) - sizeof(ICL_TBLTYPE))

typedef struct _ttblkhdr {

    G4G3_TTINFOPTR       End;      /* End of Current block    */
    struct _ttblkhdr FAR *Next;    /* Pointer to next block   */
    G4G3_TTINFO          TTInfo;   /* First TTInfo Record     */

} G4G3_TTBLKHDR, FAR *G4G3_TTBLKHDRPTR;

typedef struct _g4g3tag {
    U8          G4G3_FAR *indatacur;
    U8          G4G3_FAR *indataend;
    ICL_TBLTYPE G4G3_FAR *refline;
    ICL_TBLTYPE G4G3_FAR *codeline;
    ICL_TBLTYPE G4G3_FAR *codestart;
    ICL_TBLTYPE G4G3_FAR *refend;
    int                  inbit;
    int                  mode;
    int                  command;
    int                  state;
    int                  hcnt;
    S32                  width;      /* width of image + 1 for TT based */
    int                  run;
    int                  g4cLineType;
    ICL_TBLTYPE          lastpass;
    ICL_TBLTYPE          tt;
    IM_BOOL              eolfound;
    IM_BOOL              resink;
    BOOL                 bTiled;
    BOOL                 bTileLine;
} TRL_G4G3, G4G3_FAR *TRL_G4G3PTR;

/*
 * Function prototyes.
 */
extern int NEAR trl_G31DExpand(TRL_G4G3PTR c);
extern int NEAR trl_G4Expand(TRL_G4G3PTR c);
extern int NEAR trl_PassEOL(TRL_G4G3PTR c);

/*
 * Definitions
 */

 /* Mode definitions - Hard coded in asm */
#define G4G3_MODE_G4   1
#define G4G3_MODE_G3   2
#define G4G3_MODE_G31D 3
#define G4G3_MODE_G32D 4
#define G4G3_MODE_G4MT 5
#define G4G3_MODE_G3MT 6
#define G4G3_MODE_XG4  7

/* Command definitions - Hard coded in asm */
#define G4G3_COMMAND_START        1
#define G4G3_COMMAND_CONTINUE     2
#define G4G3_COMMAND_NEXTBAND     3
#define G4G3_COMMAND_FLUSH        4
#define G4G3_COMMAND_NEWLINE      5

/* State definitions - Hard coded in asm */
#define G4G3_STATE_WHITE        1
#define G4G3_STATE_BLACK        2
#define G4G3_STATE_EOLTEST      3
#define G4G3_STATE_VERT         4
#define G4G3_STATE_G4V_13Vo     5
#define G4G3_STATE_G4V_12Vo     6
#define G4G3_STATE_G4V_11Vo     7
#define G4G3_STATE_G4V_10Vo     8
#define G4G3_STATE_G4V_9Vo      9
#define G4G3_STATE_G4V_8Vo      10
#define G4G3_STATE_G4V_7Vo      11
#define G4G3_STATE_G4V_6Vo      12
#define G4G3_STATE_G4V_5Vo      13
#define G4G3_STATE_G4V_4Vo      14
#define G4G3_STATE_G4V_3Vo      15
#define G4G3_STATE_G4V_2Vo      16
#define G4G3_STATE_G4V_Vo       17
#define G4G3_STATE_G4V_2VoVl1   18
#define G4G3_STATE_G4V_VoVl1    19
#define G4G3_STATE_G4V_Vl1      20
#define G4G3_STATE_G4V_2VoVr1   21
#define G4G3_STATE_G4V_VoVr1    22
#define G4G3_STATE_G4V_Vr1      23
#define G4G3_STATE_G4V_GETCODE  24


/*
 * Return codes - SUCCESS --> All OK.
 */
#define G4G3_EOD         (1)
#define G4G3_EOF         (2)
#define G4G3_EOL         (3)

#define G4G3_BADCOMMAND  (-1)
#define G4G3_BADMODE     (-2)
#define G4G3_BADEOL      (-3)

#define G4G3_BADWHITELEN (-11)
#define G4G3_BADBLACKLEN (-12)
#define G4G3_BADLINE     (-13)


 /*
  * Code table defines
  */

  /* Single codes definitions */
#define VS_Vr1        1
#define VS_Vr2        2
#define VS_Vr3        3
#define VS_Vl1        4
#define VS_Vl2        5
#define VS_Vl3        6
#define VS_VEOF       7
#define VS_PASS       8
#define VS_HORZ       9
#define VS_Vo         10
#define VS_ERROR      11
#define VS_V2o        12


/* Complex codes definitions */
#define C_VEOF                 0
#define C_ERROR                1
#define C_Vl3                  2
#define C_Vl3Vo                3
#define C_Vr3                  4
#define C_Vr3Vo                5
#define C_Vl2                  6
#define C_Vl2Vo                7
#define C_Vl2V2o               8
#define C_Vr2                  9
#define C_Vr2Vo                10
#define C_Vr2V2o               11
#define C_PASS                 12
#define C_2PASS                13
#define C_HORZ                 14
#define C_Vl1                  15
#define C_Vl1PASS              16
#define C_Vl1HORZ              17
#define C_2Vl1                 18
#define C_2Vl1Vo               19
#define C_2Vl1V2o              20
#define C_Vl1Vr1               21
#define C_Vl1Vr1Vo             22
#define C_Vl1Vr1V2o            23
#define C_Vl1Vo                24
#define C_Vl1VoPASS            25
#define C_Vl1VoHORZ            26
#define C_Vl1VoVl1             27
#define C_Vl1VoVl1Vo           28
#define C_Vl1VoVr1             29
#define C_Vl1VoVr1Vo           30
#define C_Vl1V2o               31
#define C_Vl1V2oHORZ           32
#define C_Vl1V2oVl1            33
#define C_Vl1V2oVr1            34
#define C_Vl1V2oVo             35
#define C_Vl12V2o              36
#define C_Vl12V2oVo            37
#define C_Vr1                  38
#define C_Vr1PASS              39
#define C_Vr1HORZ              40
#define C_Vr1Vl1               41
#define C_Vr1Vl1Vo             42
#define C_Vr1Vl1V2o            43
#define C_2Vr1                 44
#define C_2Vr1Vo               45
#define C_2Vr1V2o              46
#define C_Vr1Vo                47
#define C_Vr1VoPASS            48
#define C_Vr1VoHORZ            49
#define C_Vr1VoVl1             50
#define C_Vr1VoVl1Vo           51
#define C_Vr1VoVr1             52
#define C_Vr1VoVr1Vo           53
#define C_Vr1V2o               54
#define C_Vr1V2oHORZ           55
#define C_Vr1V2oVl1            56
#define C_Vr1V2oVr1            57
#define C_Vr1V2oVo             58
#define C_Vr12V2o              59
#define C_Vr12V2oVo            60
#define C_Vo                   61
#define C_VoVl3                62
#define C_VoVr3                63
#define C_VoVl2                64
#define C_VoVl2Vo              65
#define C_VoVr2                66
#define C_VoVr2Vo              67
#define C_VoPASS               68
#define C_VoHORZ               69
#define C_VoVl1                70
#define C_VoVl1PASS            71
#define C_VoVl1HORZ            72
#define C_Vo2Vl1               73
#define C_Vo2Vl1Vo             74
#define C_VoVl1Vr1             75
#define C_VoVl1Vr1Vo           76
#define C_VoVl1Vo              77
#define C_VoVl1VoHORZ          78
#define C_VoVl1VoVl1           79
#define C_VoVl1VoVr1           80
#define C_VoVl1V2o             81
#define C_VoVl1V2oVo           82
#define C_VoVl12V2o            83
#define C_VoVr1                84
#define C_VoVr1PASS            85
#define C_VoVr1HORZ            86
#define C_VoVr1Vl1             87
#define C_VoVr1Vl1Vo           88
#define C_Vo2Vr1               89
#define C_Vo2Vr1Vo             90
#define C_VoVr1Vo              91
#define C_VoVr1VoHORZ          92
#define C_VoVr1VoVl1           93
#define C_VoVr1VoVr1           94
#define C_VoVr1V2o             95
#define C_VoVr1V2oVo           96
#define C_VoVr12V2o            97
#define C_V2o                  98
#define C_V2oVl2               99
#define C_V2oVr2               100
#define C_V2oPASS              101
#define C_V2oHORZ              102
#define C_V2oVl1               103
#define C_V2oVl1HORZ           104
#define C_V2o2Vl1              105
#define C_V2oVl1Vr1            106
#define C_V2oVl1Vo             107
#define C_V2oVl1V2o            108
#define C_V2oVl1V2oVo          109
#define C_V2oVr1               110
#define C_V2oVr1HORZ           111
#define C_V2oVr1Vl1            112
#define C_V2o2Vr1              113
#define C_V2oVr1Vo             114
#define C_V2oVr1V2o            115
#define C_V2oVr1V2oVo          116
#define C_V2oVo                117
#define C_V2oVoPASS            118
#define C_V2oVoHORZ            119
#define C_V2oVoVl1             120
#define C_V2oVoVl1Vo           121
#define C_V2oVoVl1V2o          122
#define C_V2oVoVr1             123
#define C_V2oVoVr1Vo           124
#define C_V2oVoVr1V2o          125
#define C_2V2o                 126
#define C_2V2oPASS             127
#define C_2V2oHORZ             128
#define C_2V2oVl1              129
#define C_2V2oVl1Vo            130
#define C_2V2oVr1              131
#define C_2V2oVr1Vo            132
#define C_2V2oVo               133
#define C_2V2oVoHORZ           134
#define C_2V2oVoVl1            135
#define C_2V2oVoVr1            136
#define C_3V2o                 137
#define C_3V2oVo               138
#define C_4V2o                 139



#endif


