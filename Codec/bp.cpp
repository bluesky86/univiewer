#include "stdafx.h"
#include "Codec.h"
#include "DebugNew.h"
#include "type.h"

typedef struct _context_24bp
{
    //CUvImage *pUvImage;
    //int in_num_bits;
    //int out_num_bits;
    int out_bytesPerRow;
}CONTEXT_TRL, FAR *CONTEXT_TRLPTR;

UNIV_CODEC_API void* SetupBPDecoding( CUvImage *pUvImage, int *outNumBits, int *out_size )
{
    //CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)NEW CONTEXT_TRL;

    //c->pUvImage = pUvImage;

    int out_bytesPerRow;

    CalculateSize( pUvImage, outNumBits, out_size, &out_bytesPerRow );

    //c->out_num_bits = *outNumBits;

    return nullptr;
}
// input decoding struct
UNIV_CODEC_API void CleanupBPDecoding( void *cp )
{
    //delete (CONTEXT_TRLPTR)cp;
}

// input decoding struct
#define U32_Align( num )  (((num) + 3) & ~3)
UNIV_CODEC_API char* DecodeBP( void *cp, int data_len, char *pData, int first_ref_len, char *first_ref_data, char *out_buffer, int *out_len )
{
    //CONTEXT_TRLPTR c = (CONTEXT_TRLPTR)cp;

    int offset = 0;
    int d_offset = 0;
    int numBytes = 0;
    while(offset < data_len)
    {
        numBytes = *(unsigned int*)&pData[offset];
        if(numBytes + offset > data_len) break;
        memcpy_s( out_buffer + d_offset, numBytes, pData + offset + 4, numBytes );
        offset = U32_Align( offset + numBytes + 4 );
        d_offset += numBytes;
    }
    *out_len = d_offset;

    return out_buffer;

}
