#include "stdafx.h"
#include "Codec.h"
#include "DebugNew.h"
#include "type.h"

/*  Marker and its size */
#define  HEL_JFIF_SOI               0xFFD8
#define  HEL_JFIF_EOI               0xFFD9
#define  HEL_JFIF_APP               0xFFE0
#define  HEL_JFIF_APP_SIZE          0x10
#define  HEL_JFIF_APP14             0xFFEE
#define  HEL_JFIF_APP14_SIZE        0x0C
#define  HEL_JFIF_ID_JF             0x4A46
#define  HEL_JFIF_ID_IF             0x4946
#define  HEL_JFIF_VERSION           0x0102 
#define  HEL_JFIF_DQT               0xFFDB
#define  HEL_JFIF_DQT_SIZE          0x43
#define  HEL_JFIF_SOF0              0xFFC0         /* baseline DCT sequential */
#define  HEL_JFIF_SOF1              0xFFC1         /* baseline DCT sequential */
#define  HEL_JFIF_SOF2              0xFFC2         /* Progressive JPEG        */
#define  HEL_JFIF_SOF0_COLOR_SIZE   0x11
#define  HEL_JFIF_SOF0_GREY_SIZE    0xB
#define  HEL_JFIF_SOF0_SAMPLE       0x08
#define  HEL_JFIF_DHT               0xFFC4
#define  HEL_JFIF_DHT_DC_SIZE       0x1F
#define  HEL_JFIF_DHT_AC_SIZE       0xB5
#define  HEL_JFIF_SOS               0xFFDA
#define  HEL_JFIF_SOS_COLOR_SIZE    0x0C
#define  HEL_JFIF_SOS_GREY_SIZE     0x08
#define  MARKERSIZE                 2
#define  TAGSIZE                    2
#define  HEL_JFIF_DRI               0xFFDD
#define  HEL_JFIF_COMMENT           0xFFFE
#define  HEL_JFIF_APP13             0xFFED

/* Offset from each marker */
#define  APP_JF_OFFSET                 2
#define  APP_IF_OFFSET                 4
#define  APP_JFIF_END_OFFSET           6
#define  APP_VERSION_OFFSET            7
#define  APP_UNITS_OFFSET              9
#define  APP_XRES_OFFSET               10
#define  APP_YRES_OFFSET               12
#define  DQT_SELECTOR_OFFSET           2
#define  DQT_VALUE_OFFSET              3
#define  SOF0_SAMPLE_OFFSET            2
#define  SOF0_WIDTH_OFFSET             3
#define  SOF0_LENGTH_OFFSET            5
#define  SOF0_NO_COMS_OFFSET           7
#define  SOF0_Y_ID_OFFSET              8
#define  SOF0_Y_SAMPLE_OFFSET          9
#define  SOF0_Y_DQT_OFFSET             10
#define  SOF0_U_ID_OFFSET              11
#define  SOF0_U_SAMPLE_OFFSET          12
#define  SOF0_U_DQT_OFFSET             13
#define  SOF0_V_ID_OFFSET              14
#define  SOF0_V_SAMPLE_OFFSET          15
#define  SOF0_V_DQT_OFFSET             16
#define  SOF0_K_ID_OFFSET              17
#define  SOF0_K_SAMPLE_OFFSET          18
#define  SOF0_K_DQT_OFFSET             19
#define  DHT_SELECTOR_OFFSET           2
#define  DHT_VALUE_OFFSET              3
#define  SOS_NO_COMS_OFFSET            2
#define  SOS_Y_ID_OFFSET               3
#define  SOS_Y_DC_AC_SELECTOR_OFFSET   4
#define  SOS_U_ID_OFFSET               5
#define  SOS_U_DC_AC_SELECTOR_OFFSET   6
#define  SOS_V_ID_OFFSET               7
#define  SOS_V_DC_AC_SELECTOR_OFFSET   8
#define  SOS_SS_OFFSET                 9
#define  SOS_SE_OFFSET                 10
#define  SOS_AH_AL_OFFSET              11

/* Table Id and selector */
#define  DQT_Y_SELECTOR             0x00           /* Y Quantization Table for baseline JPEG */
#define  DQT_UV_SELECTOR            0x01           /* UV Quantization Table for baseline JPEG */
#define  Y_ID                       0x01
#define  U_ID                       0x02
#define  V_ID                       0x03
#define  DHT_DC                     0x00           /* Huffman Table for baseline JPEG - DC class */
#define  DHT_AC                     0x10           /* Huffman Table for baseline JPEG - AC class */
#define  DHT_DC_Y_SELECTOR          0x00           /* Huffman Table for baseline JPEG - Y        */
#define  DHT_AC_Y_SELECTOR          0x00           /* Huffman Table for baseline JPEG - Y        */
#define  DHT_DC_UV_SELECTOR         0x01           /* Huffman Table for baseline JPEG - UV       */
#define  DHT_AC_UV_SELECTOR         0x01           /* Huffman Table for baseline JPEG - UV       */

#define  HEL_JFIF_HEADERSIZE         623
#define  HEL_JFIF_HEADERSIZE_NOFIX   32000
#define  HEL_JFIF_MATCHSIZE          11
#define  DOTS_PER_INCH               1

typedef union flr_st {
    U16    data16;
    U32    data32;
    UINT64 data64;
    FLOAT  datafloat;
    DOUBLE datadouble;
    U8     data[10];
} FLR_DATA;

U16 FLR_FromM16(STDPTR data)
{
    FLR_DATA out;

    out.data[0] = *(((U8PTR)data) + 1);
    out.data[1] = *((U8PTR)data);

    return(out.data16);
} /* FLR_FromM16( ) */

static int pl_get_quant_table(U8PTR data, U16 markerFiledLength, JFIF_PARMPTR parms, U32PTR offset)
{
    U8                ptr[64];
    U8                qh;
    int               i, id, count = 0;
    int               precision;
    U16PTR            quantbl = NULL;

    markerFiledLength -= 2;     /* subtract size of marker itself */

    while (markerFiledLength > 0) {
        qh = data[count];
        (*offset)++;
        markerFiledLength--;
        count++;
        precision = (int)((qh >> 4) & 0xF);
        id = (int)(qh & 0xF);
        quantbl = &parms->quant[id][0];

        /* for integers just read right in */
        if (precision == 1) {
            if (markerFiledLength < 128)
                return(FAILURE);
            memcpy(quantbl, &data[count], 128);
            *offset += 128;
            count += 128;
            for (i = 0; i < 64; i++) {
                quantbl[i] = FLR_FromM16(&quantbl[i]);
            }
            markerFiledLength -= 128;
        }
        else if (precision == 0) {
            if (markerFiledLength < 64)
                return(FAILURE);
            memcpy(ptr, &data[count], 64);
            *offset += 64;
            count += 64;
            for (i = 0; i < 64; i++) {
                quantbl[i] = (U16)(ptr[i] & 0xFF);
            }
            markerFiledLength -= 64;
        }
        else {
            return(FAILURE);
        }

    }

    return(SUCCESS);
}

static int pl_get_huff_table(U8PTR data, U16 size, JFIF_PARMPTR parms, U32PTR offset)
{
    U16               hsize, i;
    U8                id;
    int               count = 0, ret = SUCCESS;
    char              values[256];
    JPGHUFFDECODEPTR  hufftable = NULL;

    size -= 2;

    while (size > 0) {
        id = data[count++];
        (*offset)++;
        size--;
        switch (id) {
        case 0:    /* 0x00 */
            hufftable = &parms->huffdc0;
            break;
        case 1:    /* 0x01 */
            hufftable = &parms->huffdc1;
            break;
        case 2:    /* 0x02 */
            hufftable = &parms->huffdc2;
            break;
        case 3:    /* 0x03 */
            hufftable = &parms->huffdc3;
            break;
        case 16:   /* 0x10 */
            hufftable = &parms->huffac0;
            break;
        case 17:    /* 0x11 */
            hufftable = &parms->huffac1;
            break;
        case 18:    /* 0x12 */
            hufftable = &parms->huffac2;
            break;
        case 19:    /* 0x13 */
            hufftable = &parms->huffac3;
            break;
        default:
            return(FAILURE);
        }

        memset(hufftable, 0, sizeof(JPGHUFFDECODE));

        /* read in no of codes first then convert data to integers */
        memcpy((U8PTR)hufftable->ncodes, &data[count], 16);
        count += 16;
        *offset += 16;
        size -= 16;

        /* calculate number of values */
        hsize = 0;
        for (i = 0; i < 16; i++) {
            hsize = (U16)(hsize + (hufftable->ncodes[i] & 0xFF));
        }

        memcpy(values, &data[count], hsize);
        count += hsize;
        *offset += hsize;
        size -= hsize;

        for (i = 0; i < hsize; i++) {
            hufftable->val[i] = (U16)(values[i] & 0xFF);
        }
        hufftable->max_val = hsize;
    }

    return(SUCCESS);
}

static int _getHeaderData(U8PTR inData, int size, U32PTR offset, U8PTR outdata)
{
    memcpy(outdata, inData, size);
    *offset += size;
    return(SUCCESS);
}

int TRL_TransTifComp7Read(JFIF_PARMPTR parmPtr, U8PTR inData, S32 in_numBytes)
{

    int        ret = 0;
    U16 marker, markerSize;
    U32 offset = 0;
    U32 res = 0;
    IM_BOOL bDone = FALSE;
    U8PTR sosPtr = NULL;

    if (in_numBytes < 8)
        return 0;

    memcpy(&marker, &inData[offset], 2);
    marker = FLR_FromM16(&marker);

    if (marker != HEL_JFIF_SOI)
        return 0;

    offset += 2;

    while (!bDone && (S32)offset < in_numBytes - 1)
    {
        memcpy(&marker, &inData[offset], 2);
        marker = FLR_FromM16(&marker);
        offset += 2;

        memcpy(&markerSize, &inData[offset], 2);
        markerSize = FLR_FromM16(&markerSize);
        switch (marker)
        {
        case HEL_JFIF_APP:
        case HEL_JFIF_COMMENT:
            offset += markerSize;
            break;
        case HEL_JFIF_DQT:
            offset += 2;
            ret = pl_get_quant_table(&inData[offset], markerSize, parmPtr, &offset);
            if (ret != SUCCESS)
                return (0);
            break;
        case HEL_JFIF_DHT:      /* get huffman table */
            offset += 2;
            ret = pl_get_huff_table(&inData[offset], markerSize, parmPtr, &offset);
            if (ret != SUCCESS)
                return (0);
            break;
        case HEL_JFIF_SOS:      /* get Scan info */
            res = offset + markerSize;
            sosPtr = &inData[res];
            offset += 2;
            memcpy((U8PTR)&parmPtr->jpgscan, &inData[offset], markerSize - 2);
            bDone = TRUE;
            break;
        case HEL_JFIF_SOF0:                   /* Baseline JPEG, get header info */
        case HEL_JFIF_SOF1:
        {
            U16 tmp16;
            U8PTR jbi;
            jbi = &inData[offset + 2];
            memcpy(&tmp16, jbi + SOF0_WIDTH_OFFSET - 2, 2);
            parmPtr->jpeghead.height = FLR_FromM16(&tmp16);
            memcpy(&tmp16, jbi + SOF0_LENGTH_OFFSET - 2, 2);
            parmPtr->jpeghead.width = FLR_FromM16(&tmp16);
            parmPtr->jpeghead.mcu_h1 = (U16)((*(jbi + SOF0_Y_SAMPLE_OFFSET - 2) >> 4) & 0xF);  /* Y component */
            parmPtr->jpeghead.mcu_v1 = (U16)((*(jbi + SOF0_Y_SAMPLE_OFFSET - 2) & 0xF));
            parmPtr->jpeghead.mcu_h2 = (U16)((*(jbi + SOF0_U_SAMPLE_OFFSET - 2) >> 4) & 0xF);  /* U component */
            parmPtr->jpeghead.mcu_v2 = (U16)((*(jbi + SOF0_U_SAMPLE_OFFSET - 2) & 0xF));
            parmPtr->jpeghead.mcu_h3 = (U16)((*(jbi + SOF0_V_SAMPLE_OFFSET - 2) >> 4) & 0xF);  /* V component */
            parmPtr->jpeghead.mcu_v3 = (U16)((*(jbi + SOF0_V_SAMPLE_OFFSET - 2) & 0xF));
            parmPtr->jpeghead.mcu_h4 = parmPtr->jpeghead.mcu_v4 = 1;
            if (markerSize > 18) {
                parmPtr->jpeghead.mcu_h4 = (U16)((*(jbi + SOF0_K_SAMPLE_OFFSET - 2) >> 4) & 0xF);  /* K component */
                parmPtr->jpeghead.mcu_v4 = (U16)((*(jbi + SOF0_K_SAMPLE_OFFSET - 2) & 0xF));
            }
            parmPtr->quant_table1 = *(jbi + SOF0_Y_DQT_OFFSET - 2);
            parmPtr->quant_table2 = *(jbi + SOF0_U_DQT_OFFSET - 2);
            parmPtr->quant_table3 = *(jbi + SOF0_V_DQT_OFFSET - 2);
            parmPtr->quant_table4 = 0;
            if (markerSize > 18)
                parmPtr->quant_table4 = *(jbi + SOF0_K_DQT_OFFSET - 2);

            offset += markerSize;
        }
        break;
        case HEL_JFIF_DRI:       /* get restart Interval */
            offset += 2;
            ret = _getHeaderData (&inData[offset], 2, &offset, (U8PTR)&parmPtr->dri );
            if ( ret != SUCCESS ) 
                return( 0 );
            parmPtr->dri = FLR_FromM16( &parmPtr->dri);
            break;
        default:
            offset += markerSize;
            break;
        }
    }

    return res;

} /* TRL_TransTifComp7Read() */




