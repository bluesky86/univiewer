// Pdf2Uv.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Pdf2Uv.h"


// This is an example of an exported variable
PDF2UV_API int nPdf2Uv=0;

// This is an example of an exported function.
PDF2UV_API int fnPdf2Uv(void)
{
    return 42;
}

// This is the constructor of a class that has been exported.
CPdf2Uv::CPdf2Uv()
{
    return;
}
