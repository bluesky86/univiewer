// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the PDF2UV_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// PDF2UV_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef PDF2UV_EXPORTS
#define PDF2UV_API __declspec(dllexport)
#else
#define PDF2UV_API __declspec(dllimport)
#endif

// This class is exported from the dll
class PDF2UV_API CPdf2Uv {
public:
	CPdf2Uv(void);
	// TODO: add your methods here.
};

extern PDF2UV_API int nPdf2Uv;

PDF2UV_API int fnPdf2Uv(void);
