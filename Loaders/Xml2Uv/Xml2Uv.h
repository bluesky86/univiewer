#pragma once
#include "CUvDoc.h"
#include "CUvPage.h"

#define XML2UV_API __declspec(dllexport)

extern "C"
{
    XML2UV_API bool Recognizer(const wchar_t *fileName);
    XML2UV_API bool RecognizerBuffer(unsigned char *pData, int datalen);

    XML2UV_API CUvDoc* OpenDoc(const wchar_t *fileName);
    XML2UV_API CUvDoc* OpenDoc(const wchar_t *fileName);

    XML2UV_API CUvPage* GetPage(CUvDoc *pDoc, int pageNo);

    XML2UV_API void CloseDoc(CUvDoc *pDoc);

    XML2UV_API wchar_t* GetLoaderLastError();

};
