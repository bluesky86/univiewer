#include "stdafx.h"
#include "CXml2UvFile.h"
#include "tinyxml2.h"
#include "Error.h"
#include "UvFile.h"
#include "UvmUtils.h"
//#include "CUvDoc.h"
//#include "CUvPage.h"
#include "CUvPoint.h"
#include "CUvPath.h"
#include "CUvRefer.h"
#include "CUvImage.h"
#include "CUvText.h"
#include "CUvFont.h"
#include <string>
#include <vector>
#include "DebugNew.h"

void SetError(const wchar_t *msg);

CUvDoc *gCurrentDoc = nullptr;
CUvPage *gCurrentPage = nullptr;

CXml2UvFile::CXml2UvFile()
{
}


CXml2UvFile::~CXml2UvFile()
{
}

bool CXml2UvFile::Recognizer(const wchar_t *fileName)
{
    UvModel::UV_FILE *fp = UvModel::uv_wfopen_2mem(fileName, L"rb");

    if (fp)
    {
        bool res = RecognizerBuffer(fp->buffer, (int)fp->filesize);
        uv_fclose(fp, true);
        return res;
    }

    return false;
}

bool CXml2UvFile::RecognizerBuffer(unsigned char *pData, int datalen)
{
    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLError err = xmlDoc.LoadFile(pData, datalen);
    if (err != tinyxml2::XML_SUCCESS)
    {
        SetError(L"Failed to parse xml contents!");
        return false;
    }

    tinyxml2::XMLElement* xmlXml2Uv = xmlDoc.FirstChildElement("Xml2Uv");
    if (xmlXml2Uv) return true;

    return false;
}


CUvDoc* CXml2UvFile::OpenDoc(const wchar_t *fileName)
{
    char filePath[FILEPATH_MAX_LENGTH];
    size_t len;
    len = UV_Wchar2Char(filePath, FILEPATH_MAX_LENGTH, fileName);
    if (len == 0)
    {
        UV_SetError(UV_ERROR_CHAR_CONVERT_FAILED);
        return nullptr;
    }

    tinyxml2::XMLError err = xmlDoc.LoadFile(filePath);
    if (err != tinyxml2::XML_SUCCESS)
    {
        SetError(L"Failed to load xml contents!");
        return false;
    }

    tinyxml2::XMLElement* xmlUvContents = xmlDoc.FirstChildElement()->FirstChildElement("UvContents");
    if (!xmlUvContents)
    {
        SetError(L"There is no UvContents in xml!");
        return nullptr;
    }

    pUvDoc = NEW CUvDoc();

    gCurrentDoc = pUvDoc;

    const char *xmlTotalPage = xmlUvContents->Attribute("totalPage");
    if (xmlTotalPage == nullptr)
    {
        pUvDoc->nTotalPages = 0;
        tinyxml2::XMLElement* xmlUvPageEntry = xmlUvContents->FirstChildElement("UvPage");
        while (xmlUvPageEntry)
        {
            pUvDoc->nTotalPages++;
            xmlUvPageEntry = xmlUvPageEntry->NextSiblingElement("UvPage");
        }
    }
    else
    {
        pUvDoc->nTotalPages = atoi(xmlTotalPage);
    }

    // line parttern
    tinyxml2::XMLElement* xmlLinePatterns = xmlUvContents->FirstChildElement("LinePatterns");
    if (xmlLinePatterns != nullptr)
    {
        ParseLinePatterns(xmlLinePatterns, &pUvDoc->linePatterns);
    }

    // Reference
    tinyxml2::XMLElement* xmlReferences = xmlUvContents->FirstChildElement("References");
    if (xmlReferences != nullptr)
    {
        ParseReferences(xmlReferences, &pUvDoc->refers, pUvDoc);
    }

    // Font
    tinyxml2::XMLElement* xmlFonts = xmlUvContents->FirstChildElement("Fonts");
    if (xmlFonts != nullptr)
    {
        ParseFonts(xmlFonts, pUvDoc->fontList->getFonts());
    }

    return pUvDoc;
}

CUvPage* CXml2UvFile::GetPage(int pageNo)
{
    tinyxml2::XMLElement* xmlUvContents = xmlDoc.FirstChildElement()->FirstChildElement("UvContents");
    tinyxml2::XMLElement* xmlUvPageEntry = xmlUvContents->FirstChildElement("UvPage");
    for(int i = 0; xmlUvPageEntry && i<pageNo; i++)
        xmlUvPageEntry = xmlUvPageEntry->NextSiblingElement("UvPage");

    if (!xmlUvPageEntry) return nullptr;

    CUvPage *pUvPage = NEW CUvPage();
    pUvPage->pageNo = pageNo;
    //pUvPage->parent = pUvDoc;

    gCurrentPage = pUvPage;

    // page attributes
    //const char *xmlAttrValue;
    //xmlAttrValue = xmlUvPageEntry->Attribute("width");
    //if (xmlAttrValue == nullptr) pUvPage->right = .0f;
    //else pUvPage->right = (float)atof(xmlAttrValue);

    //xmlAttrValue = xmlUvPageEntry->Attribute("height");
    //if (xmlAttrValue == nullptr) pUvPage->bottom = .0f;
    //else pUvPage->bottom = (float)atof(xmlAttrValue);

    // line parttern
    tinyxml2::XMLElement* xmlLinePatterns = xmlUvPageEntry->FirstChildElement("LinePatterns");
    if (xmlLinePatterns != nullptr)
    {
        ParseLinePatterns(xmlLinePatterns, &pUvPage->linePatterns);
    }

    // Reference
    tinyxml2::XMLElement* xmlReferences = xmlUvPageEntry->FirstChildElement("References");
    if (xmlReferences != nullptr)
    {
        ParseReferences(xmlReferences, &pUvPage->refers, pUvPage);
    }

    // Font
    tinyxml2::XMLElement* xmlFonts = xmlUvPageEntry->FirstChildElement("Fonts");
    if (xmlFonts != nullptr)
    {
        ParseFonts(xmlFonts, pUvPage->fontList->getFonts());
    }

    // group attributes
    ParseGroup(xmlUvPageEntry, pUvPage, pUvDoc);


    return pUvPage;
}

CUvShape* ParseOneShape(tinyxml2::XMLElement* xmlUvEle, CUvShape *parent)
{
    CUvShape *pShape = nullptr;
    const char *xmlAttrValue = xmlUvEle->Attribute("name");
    if (xmlAttrValue)
    {
        if (strcmp(xmlAttrValue, "group") == 0)
        {
            pShape = NEW CUvGroup();
            ParseGroup(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "line") == 0)
        {
            pShape = NEW CUvLine();
            ParseLine(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "points") == 0)
        {
            pShape = NEW CUvPointFsShape();
            ParsePoints(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "poly") == 0)
        {
            pShape = NEW CUvPoly();
            ParsePoly(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "rectangle") == 0)
        {
            pShape = NEW CUvRectFShape();
            ParseRectFShape(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "oval") == 0)
        {
            pShape = NEW CUvOval();
            ParseOval(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "circle") == 0)
        {
            pShape = NEW CUvCircle();
            ParseCircle(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "arc") == 0)
        {
            pShape = NEW CUvCircleArc();
            ParseCircleArc(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "path") == 0)
        {
            pShape = NEW CUvPath();
            ParsePath(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "refer") == 0)
        {
            pShape = NEW CUvRefer();
            ParseRefer(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "img") == 0)
        {
            pShape = NEW CUvImage();
            ParseImage(xmlUvEle, pShape, parent);
        }
        else if (strcmp(xmlAttrValue, "text") == 0)
        {
            pShape = NEW CUvText();
            ParseText(xmlUvEle, pShape, parent);
        }
    }

    return pShape;
}


std::vector<int>* ParseIntArray(std::string src)
{
    std::vector<int>* pArray = NEW std::vector<int>;

    size_t pos1, pos2, pos3;
    pos1 = src.find('(', 0);
    std::string strValue;

    if (pos1 == std::string::npos) return pArray;

    pos2 = src.find(')', pos1);
    if (pos2 == std::string::npos) return pArray;

    pos1++;
    pos3 = src.find(',', pos1);
    while (pos3 != std::string::npos)
    {
        strValue = src.substr(pos1, pos3 - pos1);
        pArray->push_back(atoi(strValue.c_str()));
        pos1 = pos3 + 1;
        pos3 = src.find(',', pos1);
    }
    pos3 = src.rfind(',');
    strValue = src.substr(pos3 + 1, pos2 - pos3 - 1);
    pArray->push_back(atoi(strValue.c_str()));

    return pArray;
}

void ParsePercentageArray(std::string src, CVector<float> *pArray)
{
    if (pArray == nullptr) return;

    size_t pos1, pos2, pos3, pos5;
    pos1 = src.find('(', 0);
    std::string strValue;
    const char *charValue;

    if (pos1 == std::string::npos) return;

    pos2 = src.find(')', pos1);
    if (pos2 == std::string::npos) return;

    pos1++;
    pos3 = src.find(',', pos1);
    while (pos3 != std::string::npos)
    {
        strValue = src.substr(pos1, pos3 - pos1);
        charValue = strValue.c_str();
        pos5 = strValue.find("%");
        if (pos5 != std::string::npos)
            pArray->push_back((float)atof(&charValue[pos5 + 1]) / 100);
        else
            pArray->push_back((float)atof(charValue));

        pos1 = pos3 + 1;
        pos3 = src.find(',', pos1);
    }
    pos3 = src.rfind(',');
    strValue = src.substr(pos3 + 1, pos2 - pos3 - 1);
    charValue = strValue.c_str();
    pos5 = strValue.find("%");
    if (pos5 != std::string::npos)
        pArray->push_back((float)atof(&charValue[pos5 + 1]) / 100);
    else
        pArray->push_back((float)atof(charValue));

    return;
}

void ParseColorArray(std::string src, CVector<CUvColor>* pArray)
{
    if (pArray == nullptr) return;

    size_t pos1, pos2, pos3, pos4, pos5;
    std::string strValue;
    const char *charValue;

    pos1 = pos4 = 0;
    pos2 = src.length();
    if (pos2 < 3) return;

    pos3 = src.find(',', pos1);
    while (pos3 != std::string::npos)
    {
        strValue = src.substr(pos1, pos3 - pos1);
        charValue = strValue.c_str();
        pos5 = strValue.find("0x");
        if (pos5 != std::string::npos)
            pArray->push_back(CUvColor(&charValue[pos5 + 2]));
        else
            pArray->push_back(CUvColor(0, 0, 0));

        pos1 = pos4 = pos3 + 1;
        pos3 = src.find(',', pos1);
    }

    strValue = src.substr(pos4, pos2 - pos3);
    charValue = strValue.c_str();
    pos5 = strValue.find("0x");
    if(pos5 != std::string::npos)
        pArray->push_back(CUvColor(&charValue[pos5 + 2]));
    else
        pArray->push_back(CUvColor(0, 0, 0));

}

void ParseMatrixFromStr(std::string src, CUvMatrix &mat)
{
    float v00 = mat.mat[0][0];
    float v01 = mat.mat[0][1]; 
    float v10 = mat.mat[1][0]; 
    float v11 = mat.mat[1][1]; 
    float v20 = mat.mat[2][0];
    float v21 = mat.mat[2][1];

    size_t pos1, pos2;
    pos1 = src.find('(', 0);
    std::string strValue;

    if (pos1 == std::string::npos) return;

    pos2 = src.find(')', pos1);
    if (pos2 == std::string::npos) return;

    // v00
    size_t pos3 = src.find(',', pos1);
    if (pos3 == std::string::npos) return;
    strValue = src.substr(pos1 + 1, pos3 - pos1 - 1);
    v00 = (float)atof(strValue.c_str());

    // v01
    pos1 = pos3 + 1;
    pos3 = src.find(',', pos1);
    if (pos3 == std::string::npos) return;
    strValue = src.substr(pos1, pos3 - pos1);
    v01 = (float)atof(strValue.c_str());

    // v10
    pos1 = pos3 + 1;
    pos3 = src.find(',', pos1);
    if (pos3 == std::string::npos) return;
    strValue = src.substr(pos1, pos3 - pos1);
    v10 = (float)atof(strValue.c_str());

    // v11
    pos1 = pos3 + 1;
    pos3 = src.find(',', pos1);
    if (pos3 == std::string::npos) return;
    strValue = src.substr(pos1, pos3 - pos1);
    v11 = (float)atof(strValue.c_str());

    // v20
    pos1 = pos3 + 1;
    pos3 = src.find(',', pos1);
    if (pos3 == std::string::npos) return;
    strValue = src.substr(pos1, pos3 - pos1);
    v20 = (float)atof(strValue.c_str());

    // v21
    strValue = src.substr(pos3 + 1, pos2 - pos3 - 1);
    v21 = (float)atof(strValue.c_str());

    mat.set(v00, v01, v10, v11, v20, v21);
}

CUvShape* GetReferredShape(CUvShape *pShape, const char *src)
{
    if (pShape == nullptr || src == nullptr || strlen(src) < 2) return nullptr;

    CUvRefers *pRefers;
    int id;
    if (src[1] == 'p' || src[1] == 'P')
    {
        if (strlen(src) < 3) return nullptr;
        id = atoi(&src[2]);
        pRefers = pShape ? (&((CUvPage*)getUvPage(pShape))->refers) : nullptr;
    }
    else
    {
        id = atoi(&src[1]);
        pRefers = pShape ? (&((CUvDoc*)getUvDoc(pShape))->refers) : nullptr;
    }

    if (pRefers != nullptr)
        return pRefers->uvShapes[id];

    return nullptr;
}

void ParseGroup(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
     // RectF attribute
    ParseRectFShape(xmlUvEle, pUvEle, pParent);

    CUvGroup *pGroup = (CUvGroup*)pUvEle;

    const char *xmlAttrValue;

    // parse background color
    xmlAttrValue = xmlUvEle->Attribute("bkColor");
    if (xmlAttrValue && xmlAttrValue[0] == '0' && xmlAttrValue[1] == 'x') pGroup->bkColor = CUvColor(&xmlAttrValue[2]);

    // parse matrix
    xmlAttrValue = xmlUvEle->Attribute("matrix");
    if(xmlAttrValue) ParseMatrixFromStr(xmlAttrValue, pGroup->mat);

    // parse clip
    //pGroup->pClipPath->reset();
    //pGroup->pClipPath->addRect(*pGroup, true);
    xmlAttrValue = xmlUvEle->Attribute("clip");
    pGroup->pClipPath = (CUvPath*)GetReferredShape(pGroup, xmlAttrValue);
    if (pGroup->pClipPath && pGroup->pClipPath->type != uvPath_ShapeType)
        pGroup->pClipPath = nullptr;

    // parse children
    tinyxml2::XMLElement* xmlGroupEle = xmlUvEle->FirstChildElement("Ele");
    while (xmlGroupEle)
    {
        CUvShape *pShape = nullptr;
        pShape = ParseOneShape(xmlGroupEle, pUvEle);

        if (pShape)
        {
            pGroup->components.push_back(pShape);
            //pShape->parent = pGroup;
        }

        xmlGroupEle = xmlGroupEle->NextSiblingElement("Ele");
    }
}

void ParseRectF(tinyxml2::XMLElement* xmlUvEle, CUvRectF *pUvRectF)
{
    pUvRectF->left = pUvRectF->right = pUvRectF->top = pUvRectF->bottom = .0f;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("left");
    if (xmlAttrValue) pUvRectF->left = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("right");
    if (xmlAttrValue) pUvRectF->right = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("top");
    if (xmlAttrValue) pUvRectF->top = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("bottom");
    if (xmlAttrValue) pUvRectF->bottom = (float)atof(xmlAttrValue);
}

void ParseRectFShape(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    // pasre CUvRectF
    ParseRectF(xmlUvEle, (CUvRectFShape*)pUvEle);
}

void ParsePointFsFromStr(std::string src, CUvPointFs *pUvPointFs)
{
    size_t pos1, pos2;
    pos1 = src.find('(', 0);
    std::string xStr, yStr;
    float x, y;
    while (pos1 != std::string::npos)
    {
        pos2 = src.find(')', pos1);
        if (pos2 == std::string::npos) break;

        size_t pos3 = src.find(',', pos1);
        if (pos3 == std::string::npos) break;

        xStr = src.substr(pos1 + 1, pos3 - pos1 - 1);
        yStr = src.substr(pos3 + 1, pos2 - pos3 - 1);
        x = (float)atof(xStr.c_str());
        y = (float)atof(yStr.c_str());
        pUvPointFs->points.push_back(CUvPointF(x, y));

        pos1 = src.find('(', pos2);
    }
}

void ParsePoints(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvPointFsShape *pUvPointFs = (CUvPointFsShape*)pUvEle;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("points");
    if (xmlAttrValue)
        ParsePointFsFromStr(xmlAttrValue, pUvPointFs);
}

void ParsePoly(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvPoly *pPoly = (CUvPoly*)pUvEle;
    pPoly->points = NEW CUvPointFs();

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("points");
    if (xmlAttrValue)
        ParsePointFsFromStr(xmlAttrValue, pPoly->points);

    xmlAttrValue = xmlUvEle->Attribute("isPolygon");
    if (xmlAttrValue && strcmp(xmlAttrValue, "yes") == 0)
        pPoly->bPolygon = true;
    else
        pPoly->bPolygon = false;
}

void ParseLine(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvLine *pUvLine = (CUvLine*)pUvEle;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("x1");
    if (xmlAttrValue) pUvLine->point[0].x = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("y1");
    if (xmlAttrValue) pUvLine->point[0].y = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("x2");
    if (xmlAttrValue) pUvLine->point[1].x = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("y2");
    if (xmlAttrValue) pUvLine->point[1].y = (float)atof(xmlAttrValue);
}

void ParseShape(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    ParseStrokeFill(xmlUvEle, &pUvEle->pStrokeFill, pUvEle->type == uvText_ShapeType);
    pUvEle->parent = pParent;
}

void ParseStrokeFill(tinyxml2::XMLElement* xmlUvEle, CUvStrokeFill **pUvEle, bool isTextObj)
{
    const char *xmlAttrValue;
    //xmlAttrValue = xmlUvEle->Attribute("inheritStyle");
    //if (xmlAttrValue && strcmp(xmlAttrValue, "yes") == 0)
    //    return;

    UvStrokeFill strokeFillType = uvStroke_StrokeFill;

    xmlAttrValue = xmlUvEle->Attribute("strokeFill");
    if (xmlAttrValue)
    {
        if (strcmp(xmlAttrValue, "Stroke") == 0)
            strokeFillType = uvStroke_StrokeFill;
        else if (strcmp(xmlAttrValue, "Fill") == 0)
            strokeFillType = uvFill_StrokeFill;
        else if (strcmp(xmlAttrValue, "StrokeAndFill") == 0)
            strokeFillType = uvStrokeAndFill_StrokeFill;
        else if (strcmp(xmlAttrValue, "Parent") == 0) // inherit from parent
        {
            //strokeFillType = uvFromParent_StrokeFill;
            *pUvEle = nullptr;
            return;
        }
        else if (strcmp(xmlAttrValue, "Refer") == 0) // using reference for CUvRefer object
        {
            //strokeFillType = uvFromRefer_StrokeFill;
            *pUvEle = nullptr;
            return;
        }
        else
            strokeFillType = uvStroke_StrokeFill;
    }

    CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();
    *pUvEle = pStrokeFill;

    if (isTextObj)
        pStrokeFill->SetStrokeFill(uvFill_StrokeFill);
    else
        pStrokeFill->SetStrokeFill(strokeFillType);

    xmlAttrValue = xmlUvEle->Attribute("color");
    if (xmlAttrValue && xmlAttrValue[0] == '0' && xmlAttrValue[1] == 'x') pStrokeFill->SetStrokeColor(CUvColor(&xmlAttrValue[2]));

    xmlAttrValue = xmlUvEle->Attribute("strokeWidth");
    if (xmlAttrValue)
        pStrokeFill->SetStrokeWidth(atoi(xmlAttrValue));

    xmlAttrValue = xmlUvEle->Attribute("mitLimit");
    if (xmlAttrValue)
        pStrokeFill->SetMitLimit(atoi(xmlAttrValue));

    xmlAttrValue = xmlUvEle->Attribute("strokeCap");
    if (xmlAttrValue)
    {
        if (strcmp(xmlAttrValue, "Round") == 0)
            pStrokeFill->SetStrokeCap(uvRound_StrokeCap);
        else if (strcmp(xmlAttrValue, "Square") == 0)
            pStrokeFill->SetStrokeCap(uvSquare_StrokeCap);
        else
            pStrokeFill->SetStrokeCap(uvButt_StrokeCap);
    }

    xmlAttrValue = xmlUvEle->Attribute("strokeJoin");
    if (xmlAttrValue)
    {
        if (strcmp(xmlAttrValue, "Round") == 0)
            pStrokeFill->SetStrokeJoin(uvRound_StrokeJoin);
        else if (strcmp(xmlAttrValue, "Bevel") == 0)
            pStrokeFill->SetStrokeJoin(uvBevel_StrokeJoin);
        else
            pStrokeFill->SetStrokeJoin(uvMiter_StrokeJoin);
    }

    xmlAttrValue = xmlUvEle->Attribute("linePattern");
    if (xmlAttrValue)
    {
        if (strcmp(xmlAttrValue, "Solid") == 0)
            pStrokeFill->SetLineStyle(uvSolid_DashStyle);
        else if (strcmp(xmlAttrValue, "Dash") == 0)
            pStrokeFill->SetLineStyle(uvDash_DashStyle);
        else if (strcmp(xmlAttrValue, "Dot") == 0)
            pStrokeFill->SetLineStyle(uvDot_DashStyle);
        else if (strcmp(xmlAttrValue, "DashDot") == 0)
            pStrokeFill->SetLineStyle(uvDashDot_DashStyle);
        else if (strcmp(xmlAttrValue, "DashDotDot") == 0)
            pStrokeFill->SetLineStyle(uvDashDotDot_DashStyle);
        else if (xmlAttrValue[0] == '#') // refer to global pattern
        {
            if (strlen(xmlAttrValue) > 1 && xmlAttrValue[1] == 'p') // refer to page pattern
            {
                pStrokeFill->SetLineStyle(uvPageCustom_DashStyle + atoi(&xmlAttrValue[2]));
            }
            else // refer to doc pattern
            {
                pStrokeFill->SetLineStyle(uvDocCustom_DashStyle + atoi(&xmlAttrValue[1]));
            }
        }
        else // custom pattern
        {
            std::vector<int>* pArray = ParseIntArray(xmlAttrValue);
            int num = (int)pArray->size();
            pStrokeFill->SetLineStyle(num);
            if (num > 0)
            {
                float *intervals = NEW float[num];
                for (int i = 0; i < num; i++)
                    intervals[i] = (float)(*pArray)[i];
                pStrokeFill->SetDashIntervals(intervals);
            }
            delete pArray;
        }
    }

    // fill
    UvStrokeFill *vStrokeFill = pStrokeFill->GetStrokeFill();
    if (vStrokeFill && *vStrokeFill == uvFill_StrokeFill || *vStrokeFill == uvStrokeAndFill_StrokeFill)
    {
        //pStrokeFill->pFillArgs = NEW CUvFill();

        xmlAttrValue = xmlUvEle->Attribute("fillMode");
        if (xmlAttrValue)
        {
            if (strcmp(xmlAttrValue, "Winding") == 0)
                pStrokeFill->SetFillMode(uvWinding_FillMode);
            else if (strcmp(xmlAttrValue, "EvenOdd") == 0)
                pStrokeFill->SetFillMode(uvEvenOdd_FillMode);
            else if (strcmp(xmlAttrValue, "InverseWinding") == 0)
                pStrokeFill->SetFillMode(uvInverseWinding_FillMode);
            else if (strcmp(xmlAttrValue, "InverseEvenOdd") == 0)
                pStrokeFill->SetFillMode(uvInverseEvenOdd_FillMode);
        }

        UvFillType fillType = uvColor_FillType;
        xmlAttrValue = xmlUvEle->Attribute("fillType");
        if (xmlAttrValue)
        {
            if (strcmp(xmlAttrValue, "LinearGradient") == 0)
                fillType = uvLinearGradient_FillType;
            else if (strcmp(xmlAttrValue, "RadialGradient") == 0)
                fillType = uvRadialGradient_FillType;
            else if (strcmp(xmlAttrValue, "Image") == 0)
                fillType = uvRaster_FillType;
            else if (strcmp(xmlAttrValue, "Object") == 0)
                fillType = uvObject_FillType;
        }

        pStrokeFill->SetFillType(fillType);

        if (fillType == uvColor_FillType)
        {
            //
            xmlAttrValue = xmlUvEle->Attribute("fillColor");
            if (xmlAttrValue)
            {
                CVector<CUvColor> colorArray;
                ParseColorArray(xmlAttrValue, &colorArray);
                pStrokeFill->CopyFillColors(colorArray);
            }
            else if(pStrokeFill->GetStrokeColor())// set to stroke color
            {
                pStrokeFill->AppendFillColor(*pStrokeFill->GetStrokeColor());
            }
        }
        else if (fillType == uvLinearGradient_FillType)
        {
            xmlAttrValue = xmlUvEle->Attribute("fillColor");
            if (xmlAttrValue)
            {
                CVector<CUvColor> colorArray;
                ParseColorArray(xmlAttrValue, &colorArray);
                pStrokeFill->CopyFillColors(colorArray);
            }
            else if (pStrokeFill->GetStrokeColor())// set to stroke color
            {
                pStrokeFill->AppendFillColor(*pStrokeFill->GetStrokeColor());
            }
            xmlAttrValue = xmlUvEle->Attribute("gradientColorPos");
            if (xmlAttrValue)
            {
                CVector<float> colorPos;
                ParsePercentageArray(xmlAttrValue, &colorPos);
                pStrokeFill->SetLinearGradientColorPos(&colorPos);
            }

            CUvPointF startPoint, endPoint;
            xmlAttrValue = xmlUvEle->Attribute("startPoint");
            if (xmlAttrValue)
            {
                CUvPointFs points;
                ParsePointFsFromStr(xmlAttrValue, &points);
                if (points.points.size() > 0) startPoint = points.points[0];
            }
            xmlAttrValue = xmlUvEle->Attribute("endPoint");
            if (xmlAttrValue)
            {
                CUvPointFs points;
                ParsePointFsFromStr(xmlAttrValue, &points);
                if (points.points.size() > 0) endPoint = points.points[0];
            }
            pStrokeFill->SetLinearGradientPoints(startPoint, endPoint);
        }
        else if (fillType == uvRadialGradient_FillType)
        {
            xmlAttrValue = xmlUvEle->Attribute("fillColor");
            if (xmlAttrValue)
            {
                CVector<CUvColor> colorArray;
                ParseColorArray(xmlAttrValue, &colorArray);
                pStrokeFill->CopyFillColors(colorArray);
            }
            else if (pStrokeFill->GetStrokeColor())// set to stroke color
            {
                pStrokeFill->AppendFillColor(*pStrokeFill->GetStrokeColor());
            }
            xmlAttrValue = xmlUvEle->Attribute("gradientColorPos");
            if (xmlAttrValue)
            {
                CVector<float> colorPos;
                ParsePercentageArray(xmlAttrValue, &colorPos);
                pStrokeFill->SetRadialGradientColorPos(&colorPos);
            }

            xmlAttrValue = xmlUvEle->Attribute("center");
            if (xmlAttrValue)
            {
                CUvPointFs points;
                ParsePointFsFromStr(xmlAttrValue, &points);
                if (points.points.size() > 0) pStrokeFill->SetRadialGradientCenter(points.points[0]);
            }
            xmlAttrValue = xmlUvEle->Attribute("radius");
            if (xmlAttrValue)
                pStrokeFill->SetRadialGradientRadius((float)atof(xmlAttrValue));
        }
        else if (fillType == uvRaster_FillType || fillType == uvObject_FillType)
        {
            xmlAttrValue = xmlUvEle->Attribute("horiTileMode");
            if (xmlAttrValue)
            {
                if (strcmp(xmlAttrValue, "Repeat") == 0)
                    pStrokeFill->SetHoriTileMode(uvRepeat_TileMode);
                else if (strcmp(xmlAttrValue, "Clamp") == 0)
                    pStrokeFill->SetHoriTileMode(uvClamp_TileMode);
                else if (strcmp(xmlAttrValue, "Mirror") == 0)
                    pStrokeFill->SetHoriTileMode(uvMirror_TileMode);
                else
                    pStrokeFill->SetHoriTileMode(uvNone_TileMode);
            }
            xmlAttrValue = xmlUvEle->Attribute("vertTileMode");
            if (xmlAttrValue)
            {
                if (strcmp(xmlAttrValue, "Repeat") == 0)
                    pStrokeFill->SetVertTileMode(uvRepeat_TileMode);
                else if (strcmp(xmlAttrValue, "Clamp") == 0)
                    pStrokeFill->SetVertTileMode(uvClamp_TileMode);
                else if (strcmp(xmlAttrValue, "Mirror") == 0)
                    pStrokeFill->SetVertTileMode(uvMirror_TileMode);
                else
                    pStrokeFill->SetVertTileMode(uvNone_TileMode);
            }

            int targetWidth = 0, targetHeight = 0;
            xmlAttrValue = xmlUvEle->Attribute("fillObjWidth");
            if (xmlAttrValue) targetWidth = atoi(xmlAttrValue);

            xmlAttrValue = xmlUvEle->Attribute("fillObjHeight");
            if (xmlAttrValue) targetHeight = atoi(xmlAttrValue);

            pStrokeFill->SetTargetFillObjWidth(targetWidth);
            pStrokeFill->SetTargetFillObjHeight(targetHeight);

            xmlAttrValue = xmlUvEle->Attribute("fillObject");
            if (xmlAttrValue)
            {
                CUvShape *pUvShape = GetReferredShape(gCurrentPage, xmlAttrValue);
                if(pUvShape)
                    pStrokeFill->SetFillObj(pUvShape, true);
                else if (pUvShape == nullptr && fillType == uvRaster_FillType) // raster fill and non reference object
                {
                    CUvImage *pUvImage = NEW CUvImage();

                    pUvImage->href = xmlAttrValue;
                    pUvImage->x = 0;
                    pUvImage->y = 0;
                    pUvImage->width = 100;// targetWidth;
                    pUvImage->height = 100;// targetHeight;
                    pUvImage->widthIsPercentage = true;
                    pUvImage->heightIsPercentage = true;

                    pStrokeFill->SetFillObj(pUvImage, false);
                }
            }
        }
    }

}

void ParseOval(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvOval *pUvShape = (CUvOval*)pUvEle;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("left");
    if (xmlAttrValue) pUvShape->rect.left = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("top");
    if (xmlAttrValue) pUvShape->rect.top = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("right");
    if (xmlAttrValue) pUvShape->rect.right = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("bottom");
    if (xmlAttrValue) pUvShape->rect.bottom = (float)atof(xmlAttrValue);

    xmlAttrValue = xmlUvEle->Attribute("clockwise");
    if (xmlAttrValue && strcmp(xmlAttrValue, "yes") == 0) pUvShape->bClockwise = true;
    else pUvShape->bClockwise = false;
}

void ParseCircle(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvCircle *pUvShape = (CUvCircle*)pUvEle;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("x");
    if (xmlAttrValue) pUvShape->center.x = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("y");
    if (xmlAttrValue) pUvShape->center.y = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("r");
    if (xmlAttrValue) pUvShape->radius = (float)atof(xmlAttrValue);

    xmlAttrValue = xmlUvEle->Attribute("clockwise");
    if (xmlAttrValue && strcmp(xmlAttrValue, "yes") == 0) pUvShape->bClockwise = true;
    else pUvShape->bClockwise = false;
}

void ParseCircleArc(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseRectFShape(xmlUvEle, pUvEle, pParent);

    CUvCircleArc *pUvShape = (CUvCircleArc*)pUvEle;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("startAngle");
    if (xmlAttrValue) pUvShape->startAngle = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("sweepAngle");
    if (xmlAttrValue) pUvShape->sweepAngle = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("useCenter");
    if (xmlAttrValue && strcmp(xmlAttrValue, "yes") == 0) pUvShape->useCenter = true;
    else pUvShape->useCenter = false;
}

void ParsePath(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent, CUvMatrix *pMatForAddPath /*= nullptr*/)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvPath *pUvShape = (CUvPath*)pUvEle;

    const char *xmlAttrValue, *xmlValue;

    if (pMatForAddPath)
    {
        // parse matrix
        xmlValue = xmlUvEle->Attribute("matrix");
        if (xmlValue) ParseMatrixFromStr(xmlValue, *pMatForAddPath);
    }

    // parse children
    tinyxml2::XMLElement* xmlPathEle = xmlUvEle->FirstChildElement("Ele");
    while (xmlPathEle)
    {
        //CUvShape *pShape = nullptr;
        xmlAttrValue = xmlPathEle->Attribute("name");
        if (xmlAttrValue)
        {
            if (strcmp(xmlAttrValue, "moveTo") == 0)
            {
                xmlValue = xmlPathEle->Attribute("point");
                if (xmlValue)
                {
                    CUvPointFs points;
                    ParsePointFsFromStr(xmlValue, &points);
                    if(points.points.size() > 0) pUvShape->moveTo(points.points[0].x, points.points[0].y);
                }
            }
            else if (strcmp(xmlAttrValue, "lineTo") == 0)
            {
                xmlValue = xmlPathEle->Attribute("point");
                if (xmlValue)
                {
                    CUvPointFs points;
                    ParsePointFsFromStr(xmlValue, &points);
                    if (points.points.size() > 0) pUvShape->lineTo(points.points[0].x, points.points[0].y);
                }
            }
            else if (strcmp(xmlAttrValue, "quadTo") == 0)
            {
                xmlValue = xmlPathEle->Attribute("points");
                if (xmlValue)
                {
                    CUvPointFs points;
                    ParsePointFsFromStr(xmlValue, &points);
                    if (points.points.size() > 1) pUvShape->quadTo(points.points[0].x, points.points[0].y, points.points[1].x, points.points[1].y);
                }
            }
            else if (strcmp(xmlAttrValue, "cubicTo") == 0)
            {
                xmlValue = xmlPathEle->Attribute("points");
                if (xmlValue)
                {
                    CUvPointFs points;
                    ParsePointFsFromStr(xmlValue, &points);
                    if (points.points.size() > 1) pUvShape->quadTo(points.points[0].x, points.points[0].y, points.points[1].x, points.points[1].y);
                }
            }
            else if (strcmp(xmlAttrValue, "angleArcTo") == 0)
            {
                CUvRectF rect;
                ParseRectF(xmlPathEle, &rect);

                float startAngle = .0f;
                float sweepAngle = .0f;
                xmlValue = xmlPathEle->Attribute("startAngle");
                if (xmlValue) startAngle = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("sweepAngle");
                if (xmlValue) sweepAngle = (float)atof(xmlValue);
                pUvShape->arcTo(rect, startAngle, sweepAngle);
            }
            else if (strcmp(xmlAttrValue, "tangentArcTo") == 0)
            {
                CUvPointFs points;
                float radius = .0f;
                xmlValue = xmlPathEle->Attribute("radius");
                if (xmlValue) radius = (float)atof(xmlValue);

                xmlValue = xmlPathEle->Attribute("points");
                if (xmlValue)
                    ParsePointFsFromStr(xmlValue, &points);

                if(points.points.size() > 1) pUvShape->arcTo(points.points[0], points.points[1], radius);
            }
            else if (strcmp(xmlAttrValue, "ovalArcTo") == 0)
            {
                float xRadius = .0f;
                float yRadius = .0f;
                float xRotate = .0f;
                float xEnd = .0f;
                float yEnd = .0f;
                bool bLargeArc = false;
                bool bClockwise = false;

                xmlValue = xmlPathEle->Attribute("xRadius");
                if (xmlValue) xRadius = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("yRadius");
                if (xmlValue) yRadius = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("xRotate");
                if (xmlValue) xRotate = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("xEnd");
                if (xmlValue) xEnd = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("yEnd");
                if (xmlValue) yEnd = (float)atof(xmlValue);

                xmlValue = xmlPathEle->Attribute("largeArc");
                if (xmlValue && strcmp(xmlValue, "yes") == 0) bLargeArc = true;

                xmlValue = xmlPathEle->Attribute("clockWise");
                if (xmlValue && strcmp(xmlValue, "yes") == 0) bClockwise = true;

                pUvShape->ovalArcTo(xRadius, yRadius, xRotate, xEnd, yEnd, bLargeArc, bClockwise);
            }
            else if (strcmp(xmlAttrValue, "addPoly") == 0)
            {
                bool isPolygon = false;
                xmlValue = xmlPathEle->Attribute("isPolygon");
                if (xmlValue && strcmp(xmlValue, "yes") == 0) isPolygon = true;

                xmlValue = xmlPathEle->Attribute("points");
                if (xmlValue)
                {
                    CUvPointFs points;
                    ParsePointFsFromStr(xmlValue, &points);
                    if (points.points.size() > 1) pUvShape->addPoly(points.points, isPolygon);
                }
            }
            else if (strcmp(xmlAttrValue, "addRect") == 0)
            {
                CUvRectF rect;
                ParseRectF(xmlPathEle, &rect);

                bool bClockwise = false;
                xmlValue = xmlPathEle->Attribute("clockWise");
                if (xmlValue && strcmp(xmlValue, "yes") == 0) bClockwise = true;

                pUvShape->addRect(rect, bClockwise);
            }
            else if (strcmp(xmlAttrValue, "addOval") == 0)
            {
                CUvRectF rect;
                ParseRectF(xmlPathEle, &rect);

                bool bClockwise = false;
                xmlValue = xmlPathEle->Attribute("clockWise");
                if (xmlValue && strcmp(xmlValue, "yes") == 0) bClockwise = true;

                pUvShape->addOval(rect, bClockwise);
            }
            else if (strcmp(xmlAttrValue, "addCircle") == 0)
            {
                bool bClockwise = false;
                xmlValue = xmlPathEle->Attribute("clockWise");
                if (xmlValue && strcmp(xmlValue, "yes") == 0) bClockwise = true;

                float x = .0f, y = .0f, radius = .0f;
                xmlValue = xmlPathEle->Attribute("x");
                if (xmlValue) x = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("y");
                if (xmlValue) y = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("radius");
                if (xmlValue) radius = (float)atof(xmlValue);

                pUvShape->addCircle(CUvPointF(x, y), radius, bClockwise);
            }
            else if (strcmp(xmlAttrValue, "addArc") == 0)
            {
                CUvRectF rect;
                ParseRectF(xmlPathEle, &rect);

                float startAngle = .0f;
                float sweepAngle = .0f;
                xmlValue = xmlPathEle->Attribute("startAngle");
                if (xmlValue) startAngle = (float)atof(xmlValue);
                xmlValue = xmlPathEle->Attribute("sweepAngle");
                if (xmlValue) sweepAngle = (float)atof(xmlValue);
                pUvShape->addArc(rect, startAngle, sweepAngle);
            }
            else if (strcmp(xmlAttrValue, "addPath") == 0)
            {
                CUvMatrix mat;
                CUvPath childPath;// = NEW CUvPath();
                ParsePath(xmlPathEle, &childPath, pUvShape, &mat);
                pUvShape->addPath(childPath, mat, true);
            }
        }

        xmlPathEle = xmlPathEle->NextSiblingElement("Ele");
    }
}

void ParseLinePatterns(tinyxml2::XMLElement* xmlUvEle, void* pTargetVector)
{
    if (pTargetVector == nullptr) return;

    std::map<int, CUvLinePattern*> *pTarget = (std::map<int, CUvLinePattern*>*) pTargetVector;

    const char *xmlAttrValue, *xmlValue;
    tinyxml2::XMLElement* xmlPattern = xmlUvEle->FirstChildElement("LinePattern");
    while (xmlPattern)
    {
        xmlValue = xmlPattern->Attribute("id");
        if (xmlValue)
        {
            CUvLinePattern *pUvPattern = NEW CUvLinePattern();
            xmlAttrValue = xmlPattern->Attribute("pattern");
            if (xmlAttrValue == nullptr)
                delete pUvPattern;
            else
            {
                std::vector<int> *pArray = ParseIntArray(xmlAttrValue);
                for (size_t i = 0; i < pArray->size(); i++)
                    pUvPattern->intervals.push_back((float)(*pArray)[i]);

                if (pArray) delete pArray;
            }

            pTarget->insert(std::pair<int, CUvLinePattern*>(atoi(xmlValue), pUvPattern));
        }
        xmlPattern = xmlPattern->NextSiblingElement("Ele");
    }

}

void ParseFonts(tinyxml2::XMLElement* xmlUvEle, void* pTargetVector)
{
    if (pTargetVector == nullptr) return;

    std::vector<CUvFont*> *pFonts = (std::vector<CUvFont*>*) pTargetVector;

    const char *xmlValue;
    tinyxml2::XMLElement* xmlFamilies = xmlUvEle->FirstChildElement("Families");
    if (xmlFamilies == nullptr)  return;

    tinyxml2::XMLElement* xmlFamily = xmlFamilies->FirstChildElement("Family");
    while (xmlFamily)
    {
        CUvFont *pFont = NEW CUvFont();
        xmlValue = xmlFamily->Attribute("name");
        if (xmlValue)
            pFont->setFamilyName((const U8*)xmlValue);

        xmlValue = xmlFamily->Attribute("href");
        if (xmlValue)
            pFont->setHref((const U8*)xmlValue);

        CUvFontFace *pFace = NEW CUvFontFace();
        //const char** pFaceName = (const char**)pFace->getFaceNameAddr();
        //UV_AllocCopyStr((const char*)pFont->getFamilyName(), pFaceName);
        pFace->setFaceName((U8*)pFont->getFamilyName());
        pFace->setFamilyName(pFont->getFamilyName());
        pFace->setParent(pFont);
        pFont->appendFace(pFace);

        tinyxml2::XMLElement* xmlFace = xmlFamily->FirstChildElement("Face");
        while (xmlFace)
        {
            pFace = NEW CUvFontFace();
            xmlValue = xmlFace->Attribute("name");
            if (xmlValue)
            {
                //pFaceName = (const char**)pFace->getFaceNameAddr();
                //UV_AllocCopyStr(xmlValue, pFaceName);
                pFace->setFaceName((U8*)xmlValue);

                xmlValue = xmlFace->Attribute("italic");
                if (xmlValue) pFace->setItalic(true);
                xmlValue = xmlFace->Attribute("bold");
                if (xmlValue) pFace->setBold(true);

                pFace->setFamilyName(pFont->getFamilyName());
                pFace->setParent(pFont);

                pFont->appendFace(pFace);
            }
            else
                delete pFace;

            xmlFace = xmlFace->NextSiblingElement("Face");
        }

        tinyxml2::XMLElement* xmlData = xmlFamily->FirstChildElement("Data");
        if (xmlData)
        {
            xmlValue = xmlData->GetText();
            if (xmlValue)
            {
                char *pData = nullptr;
                size_t len = ConvertHexFontToBinary(xmlValue, &pData);
                pFont->setData((U8*)pData, len);
            }
        }

        pFonts->push_back(pFont);

        xmlFamily = xmlFamily->NextSiblingElement("Family");
    }
}

void ParseReferences(tinyxml2::XMLElement* xmlUvEle, void* pTargetVector, CUvShape *parent)
{
    if (pTargetVector == nullptr) return;

    std::map<int, CUvShape*> *pTarget = (std::map<int, CUvShape*>*) pTargetVector;

    const char *xmlValue;
    tinyxml2::XMLElement* xmlReference = xmlUvEle->FirstChildElement("Reference");
    while (xmlReference)
    {
        CUvShape *pShape = ParseOneShape(xmlReference, parent);

        if (pShape)
        {
            xmlValue = xmlReference->Attribute("id");
            if (xmlValue)
                pTarget->insert(std::pair<int, CUvShape*>(atoi(xmlValue), pShape));
            else
                delete pShape;
        }
        xmlReference = xmlReference->NextSiblingElement("Reference");
    }
}

void ParseRefer(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent, CUvMatrix *pMat)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvRefer *pUvRefer = (CUvRefer*)pUvEle;


    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("id");
    pUvRefer->pRefer = GetReferredShape(pParent, xmlAttrValue);

    xmlAttrValue = xmlUvEle->Attribute("matrix");
    if (xmlAttrValue)
        ParseMatrixFromStr(xmlAttrValue, pUvRefer->mat);
}

void ParseImage(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvImage *pUvImage = (CUvImage*)pUvEle;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("href");
    if(xmlAttrValue) pUvImage->href = xmlAttrValue;

    xmlAttrValue = xmlUvEle->Attribute("x");
    if (xmlAttrValue)
        pUvImage->x = atoi(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("y");
    if (xmlAttrValue)
        pUvImage->y = atoi(xmlAttrValue);

    xmlAttrValue = xmlUvEle->Attribute("width");
    if (xmlAttrValue)
    {
        size_t pos;
        std::string strValue = xmlAttrValue;
        pos = strValue.find('%', 0);
        if (pos != std::string::npos)
        {
            CVector<float> wArray;
            ParsePercentageArray(xmlAttrValue, &wArray);
            pUvImage->widthIsPercentage = true;
            if (wArray.size() > 0)
                pUvImage->width = (int)wArray[0];
            else
                pUvImage->width = 100;
        }
        else
        {
            pUvImage->widthIsPercentage = false;
            pUvImage->width = atoi(xmlAttrValue);
        }
    }
    else
    {
        pUvImage->widthIsPercentage = true;
        pUvImage->width = 100;
    }
    xmlAttrValue = xmlUvEle->Attribute("height");
    if (xmlAttrValue)
    {
        size_t pos;
        std::string strValue = xmlAttrValue;
        pos = strValue.find('%', 0);
        if (pos != std::string::npos)
        {
            CVector<float> wArray;
            ParsePercentageArray(xmlAttrValue, &wArray);
            pUvImage->heightIsPercentage = true;
            if (wArray.size() > 0)
                pUvImage->height = (int)wArray[0];
            else
                pUvImage->height = 100;
        }
        else
        {
            pUvImage->heightIsPercentage = false;
            pUvImage->height = atoi(xmlAttrValue);
        }
    }
    else
    {
        pUvImage->heightIsPercentage = true;
        pUvImage->height = 100;
    }
}

void ParseText(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent)
{
    // Shape attribute
    ParseShape(xmlUvEle, pUvEle, pParent);

    CUvText *pUvText = (CUvText*)pUvEle;

    const char *xmlAttrValue;
    xmlAttrValue = xmlUvEle->Attribute("value");
    if (xmlAttrValue)
    {
        pUvText->textLen = (S32)strlen(xmlAttrValue);
        pUvText->text = NEW U8[pUvText->textLen + 2];
        strcpy_s((char*)pUvText->text, pUvText->textLen + 2, (char*)xmlAttrValue);
    }

    xmlAttrValue = xmlUvEle->Attribute("x");
    if (xmlAttrValue)
        pUvText->x = (float)atof(xmlAttrValue);
    xmlAttrValue = xmlUvEle->Attribute("y");
    if (xmlAttrValue)
        pUvText->y = (float)atof(xmlAttrValue);

    xmlAttrValue = xmlUvEle->Attribute("textSize");
    if (xmlAttrValue)
        pUvText->fontSize = (float) atof(xmlAttrValue);

    xmlAttrValue = xmlUvEle->Attribute("font");
    if (xmlAttrValue)
    {
        CUvFontFace *pFontFace = nullptr;
        CUvPage *pUvPage = (CUvPage*)getUvPage(pParent);
        if (pUvPage)
            pFontFace = pUvPage->fontList->findFace((const U8*)xmlAttrValue);
        
        if (pFontFace == nullptr)
        {
            CUvDoc *pUvDoc = (CUvDoc*)getUvDoc(pParent);
            if(pUvDoc)
                pFontFace = pUvDoc->fontList->findFace((const U8*)xmlAttrValue);
        }

        pUvText->pFontFace = pFontFace;
    }
}