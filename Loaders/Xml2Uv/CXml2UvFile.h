#pragma once
#include "tinyxml2.h"
#include "CUvDoc.h"
#include "CUvPage.h"
#include "CUvShape.h"
#include "CUvColor.h"
#include "CUvRefer.h"
#include "CUvRect.h"

class CXml2UvFile
{
public:
    CXml2UvFile();
    ~CXml2UvFile();

    static bool Recognizer(const wchar_t *fileName);
    static bool RecognizerBuffer(unsigned char *pData, int datalen);
    CUvDoc* OpenDoc(const wchar_t *fileName);
    CUvPage* GetPage(int pageNo);

public:
    tinyxml2::XMLDocument xmlDoc;

private:
    CUvDoc * pUvDoc; // release by loader control
};

CUvShape* GetReferredShape(CUvShape *pShape, const char *src);

CUvShape* ParseOneShape(tinyxml2::XMLElement* xmlUvEle, CUvShape *parent);

void ParseLinePatterns(tinyxml2::XMLElement* xmlUvEle, void* pTargetVector);
void ParseReferences(tinyxml2::XMLElement* xmlUvEle, void* pTargetVector, CUvShape *parent);
void ParseFonts(tinyxml2::XMLElement* xmlUvEle, void* pTargetVector);

void ParseGroup(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseRectFShape(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseRectF(tinyxml2::XMLElement* xmlUvEle, CUvRectF *pUvRectF);
void ParsePoints(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParsePoly(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseLine(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseShape(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseStrokeFill(tinyxml2::XMLElement* xmlUvEle, CUvStrokeFill **pUvEle, bool isTextObj);
void ParseOval(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseCircle(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseCircleArc(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParsePath(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent, CUvMatrix *pMatForAddPath = nullptr);
void ParseRefer(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent, CUvMatrix *pMat= nullptr);
void ParseImage(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
void ParseText(tinyxml2::XMLElement* xmlUvEle, CUvShape *pUvEle, CUvShape *pParent);
