// Xml2Uv.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "header.h"
#include "Xml2Uv.h"
#include "CXml2UvFile.h"
#include "DebugNew.h"


extern "C"
{
    static wchar_t Xml2Uv_Error[256];

    XML2UV_API wchar_t* GetLoaderLastError()
    {
        return Xml2Uv_Error;
    }

    XML2UV_API bool Recognizer(const wchar_t *fileName)
    {
        return CXml2UvFile::Recognizer(fileName);
    }

    XML2UV_API bool RecognizerBuffer(unsigned char *pData, int datalen)
    {
        return CXml2UvFile::RecognizerBuffer(pData, datalen);
    }

    XML2UV_API CUvDoc* OpenDoc(const wchar_t *fileName)
    {
        if (fileName == nullptr) return nullptr;

        CXml2UvFile *pXmlFile = NEW CXml2UvFile();
        CUvDoc *pUvDoc = pXmlFile->OpenDoc(fileName);
        if (pUvDoc == nullptr)
            delete pXmlFile;
        else
            pUvDoc->pLoaderInfo = (void*)pXmlFile;

        return pUvDoc;
    }

    XML2UV_API CUvPage* GetPage(CUvDoc *pDoc, int pageNo)
    {
        if(pDoc == nullptr || pDoc->pLoaderInfo == nullptr) return nullptr;

        return ((CXml2UvFile*)(pDoc->pLoaderInfo))->GetPage(pageNo);
    }

    XML2UV_API void CloseDoc(CUvDoc *pDoc)
    {
        if (pDoc == nullptr || pDoc->pLoaderInfo == nullptr) return;

        delete (CXml2UvFile*)pDoc->pLoaderInfo;
    }

};

void SetError(const wchar_t *msg)
{
    wcscpy_s(Xml2Uv_Error, 256, msg);
}

