#pragma once
#include "CUvDoc.h"
#include "CUvPage.h"
//#include "CUvShape.h"
#include "CUvColor.h"
#include "CUvImage.h"
//#include "CUvRect.h"

#include "EgfObj.h"
#include "EgfFile.h"

class CEgf2UvFile
{
public:
    CEgf2UvFile();
    ~CEgf2UvFile();

    static bool Recognizer(const wchar_t *fileName);
    static bool RecognizerBuffer(unsigned char *pData, int datalen);
    CUvDoc* OpenDoc(const wchar_t *fileName);
    CUvPage* GetPage(int pageNo);

    CEgfFile *pEgfFile;

private:
    CUvDoc * pUvDoc; // release by loader control


private:
    CUvShape* EgfPage2Uv(CEgfObj *pEgfObj);
    CUvShape* ConvertOneEgf2Uv(CEgfObj *pEgfObj, CUvShape *parent, bool pgonIsGroupMask);
    CUvShape* EgfGroup2Uv(CEgfObj *pEgfObj, CUvShape *parent, CUvShape *pUvPage = nullptr);
    CUvShape* EgfLine2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfPgon2Uv(CEgfObj *pEgfObj, CUvShape *parent, bool isGroupMask);
    CUvShape* EgfPline2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfEllipse2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfArc2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfPath2Uv(CEgfObj *pEgfObj, CUvShape *parent, bool bSubPath);
    CUvShape* EgfText2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfPaste2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfEmf2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfSText2Uv(CEgfObj *pEgfObj, CUvShape *parent);
    CUvShape* EgfExtText2Uv(CEgfObj *pEgfObj, CUvShape *parent);

    // helpers
    void SetStrokeFillParmsFromStyles(CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill, bool bTextObj);
    void ConvertThickness(CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    void ConvertMer(wchar_t* attrName, CEgfObj *pEgfObj, CUvRectF *pUvRectF);
    void ConvertPoint(wchar_t* attrName, CEgfObj *pEgfObj, CUvPointF *pUvPointF);
    CUvPointF* ConvertPoints(wchar_t* attrName, CEgfObj *pEgfObj, int *count);
    void ConvertColor(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    void ConvertFillColor(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    void ConvertLineStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    bool ConvertCapStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    void ConvertJoinStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    void ConvertFillStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    void ConvertGradientFill(CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    void ConvertFillMode(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill);
    int ConvertAngle(wchar_t* attrName, CEgfObj *pEgfObj);

    void ConvertDashPatterns(CEgfObj *pDashs, CUvLinePatterns &uvDashPatterns);
    void ConvertEmbedFonts(CEgfObj *pObj, CUvFonts *pUvFonts);

    CUvColor MapStrokeFillColor(int *pFillColor);

    // 
    bool GetBoolValue(wchar_t* attrName, CEgfObj *pEgfObj);
    unsigned char GetByteValue(wchar_t* attrName, CEgfObj *pEgfObj);
    int GetIntValue(wchar_t* attrName, CEgfObj *pEgfObj);
    float GetFloatValue(wchar_t* attrName, CEgfObj *pEgfObj);
    char* GetStringValue(wchar_t* attrName, CEgfObj *pEgfObj);
    RSL_RAST_DATA_PTR GetRasterStruct(wchar_t* attrName, CEgfObj *pEgfObj);
    JFIF_PARMPTR GetJpegStruct(wchar_t* attrName, CEgfObj *pEgfObj);
};


