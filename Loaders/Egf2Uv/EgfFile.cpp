#include "stdafx.h"
#include "EgfFile.h"

void SetError(const wchar_t *msg);
void ClearError();


CEgfFile::CEgfFile()
    :m_pChild(0), m_pGdiplusGradientParmsTable(0), m_pGdiplusStrokeParmsTable(0), m_pGdiplusArcParmsTable(0), m_pGdiplusFillParmsTable(0),
    m_pGdiFillParmsTable(0), m_pGdiStrokeParmsTable(0), m_pGdiEllipseParmsTable(0), m_pTextParmsTable(0), m_pEmbeddedFontList(0),
    m_pDashPattern(0), m_pHatchPattern(0), m_pRasterFillTables(0), m_pEmbeddedTrueTypeFonts(0)
{
}

CEgfFile::~CEgfFile()
{
    Clear();
}

void CEgfFile::Clear()
{
    if (m_pChild)
    {
        delete m_pChild;
        m_pChild = NULL;
    }

    if (m_pGdiplusGradientParmsTable)
    {
        delete m_pGdiplusGradientParmsTable;
        m_pGdiplusGradientParmsTable = NULL;
    }
    if (m_pGdiplusStrokeParmsTable)
    {
        delete m_pGdiplusStrokeParmsTable;
        m_pGdiplusStrokeParmsTable = NULL;
    }
    if (m_pGdiplusArcParmsTable)
    {
        delete m_pGdiplusArcParmsTable;
        m_pGdiplusArcParmsTable = NULL;
    }
    if (m_pGdiplusFillParmsTable)
    {
        delete m_pGdiplusFillParmsTable;
        m_pGdiplusFillParmsTable = NULL;
    }
    if (m_pGdiFillParmsTable)
    {
        delete m_pGdiFillParmsTable;
        m_pGdiFillParmsTable = NULL;
    }
    if (m_pGdiStrokeParmsTable)
    {
        delete m_pGdiStrokeParmsTable;
        m_pGdiStrokeParmsTable = NULL;
    }
    if (m_pGdiEllipseParmsTable)
    {
        delete m_pGdiEllipseParmsTable;
        m_pGdiEllipseParmsTable = NULL;
    }
    if (m_pTextParmsTable)
    {
        delete m_pTextParmsTable;
        m_pTextParmsTable = NULL;
    }
    if (m_pEmbeddedFontList)
    {
        delete m_pEmbeddedFontList;
        m_pEmbeddedFontList = NULL;
    }
    if (m_pDashPattern)
    {
        delete m_pDashPattern;
        m_pDashPattern = NULL;
    }
    if (m_pHatchPattern)
    {
        delete m_pHatchPattern;
        m_pHatchPattern = NULL;
    }
    if (m_pRasterFillTables)
    {
        delete m_pRasterFillTables;
        m_pRasterFillTables = NULL;
    }
    if (m_pEmbeddedTrueTypeFonts)
    {
        delete m_pEmbeddedTrueTypeFonts;
        m_pEmbeddedTrueTypeFonts = NULL;
    }
}

bool CEgfFile::Parse(std::ifstream &ifs)
{
    /////////////////////////////////////////////////////////////
    //  load object
    /////////////////////////////////////////////////////////////
    int objType = 0, objDataType = 0, objSize = 0;

    Clear();
    ClearError();

    int version;
    if (CEgfObj::ReadInt(ifs, version) == false)
        return false;

    while (CEgfObj::LoadObjHdr(ifs, objType, objDataType, objSize))
    {
        CEgfObj *pObj = CEgfObj::CreateEgfObj(objType, objDataType, objSize);
        size_t offset = ifs.tellg();
        if (pObj->Parse(ifs, 0, objDataType))
        {
            switch (objDataType)
            {
            case SDT_Styles_GdiplusGradientParmsTable:
                m_pGdiplusGradientParmsTable = pObj;
                break;
            case SDT_Styles_GdiplusStrokeParmsTable:
                m_pGdiplusStrokeParmsTable = pObj;
                break;
            case SDT_Styles_GdiplusArcParmsTable:
                m_pGdiplusArcParmsTable = pObj;
                break;
            case SDT_Styles_GdiplusFillParmsTable:
                m_pGdiplusFillParmsTable = pObj;
                break;
            case SDT_Styles_GdiFillParmsTable:
                m_pGdiFillParmsTable = pObj;
                break;
            case SDT_Styles_GdiStrokeParmsTable:
                m_pGdiStrokeParmsTable = pObj;
                break;
            case SDT_Styles_GdiEllipseParmsTable:
                m_pGdiEllipseParmsTable = pObj;
                break;
            case SDT_Styles_TextParmsTable:
                m_pTextParmsTable = pObj;
                break;
            case SDT_Styles_EmbeddedFontList:
                m_pEmbeddedFontList = pObj;
                break;
            case SDT_Styles_EmbeddedTrueTypeFontTable:
                m_pEmbeddedTrueTypeFonts = pObj;
                break;
            case SDT_Styles_DashPatterns:
                m_pDashPattern = pObj;
                //ConvertEgfDashPatterns(pObj);
                break;
            case SDT_Styles_HatchPatterns:
                m_pHatchPattern = pObj;
                break;
            case SDT_Styles_RasterFillTable:
                m_pRasterFillTables = pObj;
                break;
            default: // default to group object
                m_pChild = pObj;
                break;
            }
        }
        else
        {
            // recovery to next section
            offset += objSize;
            ifs.seekg(offset);

            SetError(L"Parsing object failed! Skip to next section!");
#ifdef _DEBUG
            //NEW char[1]; // debug
#endif
            //break;
        }
    }


    return true;
}

void CEgfFile::Search(int objMetaId, CSearchCriteria *pPairs, CVector<CEgfObj*> *pList)
{
    if (m_pChild)
        m_pChild->Search(objMetaId, pPairs, pList);
}

void CEgfFile::ConvertEgfDashPatterns(CEgfObj *pObj)
{

}

CEgfObj* CEgfFile::FindObjFromTree(CVector<int> *index)
{
    if (index == nullptr) return nullptr;

    CEgfObj* pObj = nullptr;
    CEgfObj *pEgfObj = m_pChild;
    for (S32 i = 1; i < (S32)index->size(); i++)
    {
        pObj = pEgfObj->GetChild((*index)[i]);
        if (pObj == nullptr) return nullptr;
        pEgfObj = pObj;
    }

    return pObj;
}
