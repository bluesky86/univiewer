// Egf2Uv.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Egf2Uv.h"
#include "CEgf2UvFile.h"
#include "EgfObj.h"
#include "DebugNew.h"
#include <string>
#include "UvmUtils.h"
#include "tinyxml2.h"
#include "EgfApi.h"

extern CEgfMetaList gEgfMetaList;  // egf meta array
extern std::map<int, int> *gMetaSeqToIndexMap;  // egf meta sequence to array index of gEgfMetaList
extern std::map<int, int> *gEgfIdToIndexMap;   // egf id in imagenation to index of gEgfMetaList
extern CEgfSearchMetaList gEgfSearchMetaList;

//bool LoadNumId(CEgfEleMeta *pEleMeta, tinyxml2::XMLElement* xmlEgfObj);

extern "C"
{
    static wchar_t Egf2Uv_Error[256];
    static int Egf2Uv_ErrorNo = 0;

    EGF2UV_API wchar_t* GetLoaderLastError()
    {
        return Egf2Uv_Error;
    }

    EGF2UV_API bool Recognizer(const wchar_t *fileName)
    {
        return CEgf2UvFile::Recognizer(fileName);
    }

    EGF2UV_API bool RecognizerBuffer(unsigned char *pData, int datalen)
    {
        return CEgf2UvFile::RecognizerBuffer(pData, datalen);
    }

    EGF2UV_API CUvDoc* OpenDoc(const wchar_t *fileName)
    {
        if (fileName == nullptr) return nullptr;

        CEgf2UvFile *pUvFile = NEW CEgf2UvFile();
        CUvDoc *pUvDoc = pUvFile->OpenDoc(fileName);
        if (pUvDoc == nullptr)
            delete pUvFile;
        else
            pUvDoc->pLoaderInfo = (void*)pUvFile;

        return pUvDoc;
    }

    EGF2UV_API CUvPage* GetPage(CUvDoc *pDoc, int pageNo)
    {
        if (pDoc == nullptr || pDoc->pLoaderInfo == nullptr) return nullptr;

        return ((CEgf2UvFile*)(pDoc->pLoaderInfo))->GetPage(pageNo);
    }

    EGF2UV_API void CloseDoc(CUvDoc *pDoc)
    {
        if (pDoc == nullptr || pDoc->pLoaderInfo == nullptr) return;

        delete (CEgf2UvFile*)pDoc->pLoaderInfo;
    }

    EGF2UV_API void CloseLoader(const char *filePath)
    {
        ReleaseGlobalVarialbes();
    }

    EGF2UV_API bool LoadXmlConfig(const char *filePath)
    {
        return _LoadXmlConfig(filePath);
    }
    EGF2UV_API bool LoadXmlSearchConfig(const char *filePath)
    {
        return _LoadXmlSearchConfig(filePath);
    }

};

void SetError(const wchar_t *msg)
{
    wcscpy_s(Egf2Uv_Error, 256, msg);
    Egf2Uv_ErrorNo = 1;
}

void ClearError()
{
    memset(Egf2Uv_Error, 0, sizeof(wchar_t) * 256);
    Egf2Uv_ErrorNo = 0;
}
