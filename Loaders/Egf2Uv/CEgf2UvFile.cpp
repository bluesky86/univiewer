#include "stdafx.h"
#include "CEgf2UvFile.h"
#include "tinyxml2.h"
#include "Error.h"
#include "UvFile.h"
#include "UvmUtils.h"
#include "CUvPoint.h"
#include "CUvPath.h"
#include "CUvRefer.h"
#include "CUvImage.h"
#include "CUvText.h"
#include "CUvFont.h"
#include <string>
#include <vector>
#include "DebugNew.h"
#include <fstream>
#include "string.h"

#ifdef _WINDOWS
#include "assert.h"
#endif

void SetError(const wchar_t *msg);
void ClearError();

CEgf2UvFile::CEgf2UvFile()
    : pEgfFile(nullptr), pUvDoc(nullptr)
{
}


CEgf2UvFile::~CEgf2UvFile()
{
    if (pEgfFile) delete pEgfFile;
}

bool CEgf2UvFile::Recognizer(const wchar_t *fileName)
{
    std::wstring wstr(fileName);
    if (wstr.substr(wstr.length() - 4, 4).compare(L".egf") == 0)
        return true;

    UvModel::UV_FILE *fp = UvModel::uv_wfopen_2mem(fileName, L"rb");

    if (fp)
    {
        bool res = RecognizerBuffer(fp->buffer, (int)fp->filesize);
        uv_fclose(fp, true);
        return res;
    }

    return false;
}

bool CEgf2UvFile::RecognizerBuffer(unsigned char *pData, int datalen)
{
    //tinyxml2::XMLDocument xmlDoc;
    //tinyxml2::XMLError err = xmlDoc.LoadFile(pData, datalen);
    //if (err != tinyxml2::XML_SUCCESS)
    //{
    //    SetError(L"Failed to parse xml contents!");
    //    return false;
    //}

    //tinyxml2::XMLElement* xmlXml2Uv = xmlDoc.FirstChildElement("Xml2Uv");
    //if (xmlXml2Uv) return true;

    return false;
}


CUvDoc* CEgf2UvFile::OpenDoc(const wchar_t *fileName)
{
    ClearError();

    std::ifstream ifs(fileName, std::ifstream::binary);
    if (!ifs.good())
    {
        SetError(L"Open file failed!");
        return nullptr;
    }

    pEgfFile = NEW CEgfFile;
    bool res = pEgfFile->Parse(ifs);
    ifs.close();
    if (!res) return nullptr;

    pUvDoc = NEW CUvDoc();
    if (pEgfFile->m_pChild)
        pUvDoc->nTotalPages = 1;
    else
        pUvDoc->nTotalPages = 0;

    // convert global tables
    ConvertDashPatterns(pEgfFile->m_pDashPattern, pUvDoc->linePatterns);
    ConvertEmbedFonts(pEgfFile->m_pEmbeddedFontList, pUvDoc->fontList);
    ConvertEmbedFonts(pEgfFile->m_pEmbeddedTrueTypeFonts, pUvDoc->fontList);


    return pUvDoc;
}

CUvPage* CEgf2UvFile::GetPage(int pageNo)
{
    ClearError();

    if (pEgfFile == nullptr || pEgfFile->m_pChild == nullptr)
        return nullptr;

    // convert CEgfObj's to CUv objects
    CUvPage *pUvPage = (CUvPage*)EgfPage2Uv(pEgfFile->m_pChild);
    if (pUvPage)
    {
        pUvPage->pageNo = pageNo;
        //pUvPage->bkColor = CUvColor(255, 255, 255, 255);
    }

    return pUvPage;

}

CUvShape* CEgf2UvFile::EgfPage2Uv(CEgfObj *pEgfObj)
{
    CUvPage *pUvPage = NEW CUvPage();

    if (EgfGroup2Uv(pEgfObj, pUvDoc, pUvPage) == nullptr)
    {
        delete pUvPage;
        SetError(L"Failed to convert Egf objects to Uv objects!");
        return nullptr;
    }

    pUvPage->mat.mat[2][0] = -pUvPage->left;
    pUvPage->mat.mat[2][1] = -pUvPage->top;

    return pUvPage;
}

CUvShape* CEgf2UvFile::EgfGroup2Uv(CEgfObj *pEgfObj, CUvShape *parent, CUvShape *pUvPage /*= nullptr*/)
{
    if (pEgfObj == nullptr) return nullptr;

    wchar_t *pEgfObjName = pEgfObj->GetMetaName();
    if (wcscmp(L"Group", pEgfObjName) != 0)
        return nullptr;

    if (IsVisible(pEgfObj) == false) return nullptr; // invisible

    CUvGroup *pDstShape = nullptr;
    if (pUvPage) pDstShape = (CUvGroup*)pUvPage;
    else pDstShape = NEW CUvGroup();

    pDstShape->parent = parent;

    // convert attrs
    ConvertMer((wchar_t*)L"Mer", pEgfObj, pDstShape);
    //CEgfAttr *pEgfAttr= nullptr;
    //pEgfAttr = pEgfObj->FindAttr((wchar_t*)L"BackDropColor");
    //if (pEgfAttr)
    //    pDstShape->bkColor = MakeColor(pEgfAttr->i);
    //else
    //    pDstShape->bkColor = CUvColor(255, 255, 255, 255);


    // convert children
    CUvShape *pChildShape;
    U32 numChildren = (U32)pEgfObj->children.size();
    bool bHasMask = hasMask(pEgfObj);
    for (U32 i = 0; i < numChildren; i++)
    {
        bool bFirstPgonIsMask = (i == 0 && bHasMask && wcscmp(L"Polygon", pEgfObj->children[i]->GetMetaName()) == 0);

        pChildShape = ConvertOneEgf2Uv(pEgfObj->children[i], pDstShape, bFirstPgonIsMask);
        if (pChildShape)
        {
            pDstShape->components.push_back(pChildShape);
            pChildShape->parent = pDstShape;

            //if (bFirstPgonIsMask) // convert to CUvPath
            //{
            //    CUvPath *pUvPath = NEW CUvPath();
            //    pUvPath->addPoly(((CUvPoly*)pChildShape)->points->points, true);
            //    pDstShape->pClipPath = pUvPath;
            //}
        }

    }

    pEgfObj->pUvObj = pDstShape;

    return pDstShape;
}

CUvShape* CEgf2UvFile::ConvertOneEgf2Uv(CEgfObj *pEgfObj, CUvShape *parent, bool pgonIsGroupMask)
{

    CUvShape *pDstShape = nullptr;
    wchar_t *pEgfObjName = pEgfObj->GetMetaName();
    if (wcscmp(L"Group", pEgfObjName) == 0)
        pDstShape = EgfGroup2Uv(pEgfObj, parent);
    else if (wcscmp(L"Polygon", pEgfObjName) == 0)
        pDstShape = EgfPgon2Uv(pEgfObj, parent, pgonIsGroupMask);
    else if (wcscmp(L"Line", pEgfObjName) == 0)
        pDstShape = EgfLine2Uv(pEgfObj, parent);
    else if (wcscmp(L"Ellipse", pEgfObjName) == 0)
        pDstShape = EgfEllipse2Uv(pEgfObj, parent);
    else if (wcscmp(L"Arc", pEgfObjName) == 0)
        pDstShape = EgfArc2Uv(pEgfObj, parent);
    else if (wcscmp(L"Path", pEgfObjName) == 0)
        pDstShape = EgfPath2Uv(pEgfObj, parent, false);
    else if (wcscmp(L"Text", pEgfObjName) == 0)
        pDstShape = EgfText2Uv(pEgfObj, parent);
    else if (wcscmp(L"Paste", pEgfObjName) == 0)
        pDstShape = EgfPaste2Uv(pEgfObj, parent);
    else if (wcscmp(L"MetaFile", pEgfObjName) == 0)
        pDstShape = EgfEmf2Uv(pEgfObj, parent);
    else if (wcscmp(L"Polyline", pEgfObjName) == 0)
        pDstShape = EgfPline2Uv(pEgfObj, parent);
    else if (wcscmp(L"SText", pEgfObjName) == 0)
        pDstShape = EgfSText2Uv(pEgfObj, parent);
    else if (wcscmp(L"EtxText", pEgfObjName) == 0)
        pDstShape = EgfText2Uv(pEgfObj, parent);
    else
        assert(0);

    pEgfObj->pUvObj = pDstShape;

    return pDstShape;
}

CUvShape* CEgf2UvFile::EgfSText2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr || !IsVisible(pEgfObj) || pEgfObj->children.size() != 1) return nullptr;

    wchar_t *pEgfObjName = pEgfObj->children[0]->GetMetaName();
    CEgfObj *pChildEgfObj = nullptr;

    if (wcscmp(L"EtxFmtPage", pEgfObjName) == 0
        || wcscmp(L"EtxPage", pEgfObjName) == 0
        || wcscmp(L"EtxShape", pEgfObjName) == 0
        || wcscmp(L"EtxPicture", pEgfObjName) == 0
        )
    {
        pChildEgfObj = pEgfObj->children[0]->children.size() == 1 ? pEgfObj->children[0]->children[0] : nullptr;
    }
    else
        pChildEgfObj = pEgfObj->children[0];

    return ConvertOneEgf2Uv(pChildEgfObj, parent, false);
}

CUvShape* CEgf2UvFile::EgfPline2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr) return nullptr;

    bool isMask = IsMask(pEgfObj);

    if (!IsVisible(pEgfObj)) return nullptr;

    CUvShape *pUvShapeRes = nullptr;
    CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();

    SetStrokeFillParmsFromStyles(pEgfObj, pStrokeFill, false);

    CUvRectF rect;
    ConvertMer((wchar_t*)L"Mer", pEgfObj, &rect);

    // points
    int *numPoints = GetAttrIntValue((wchar_t*)L"NumPoints", pEgfObj);
    CEgfPointArray *pointArray = GetAttrPointArray((wchar_t*)L"Points", pEgfObj);

    CUvPoly *pUvShape = NEW CUvPoly();
    pUvShapeRes = pUvShape;
    pUvShape->points = NEW CUvPointFs;
    bool *bClosed = GetAttrBoolValue((wchar_t*)L"Closed", pEgfObj);
    pUvShape->bPolygon = false;

    for (int i = 0; i < *numPoints; i++)
        pUvShape->points->points.push_back(CUvPointF(pointArray->points[i]->x / 4096, pointArray->points[i]->y / 4096));


    pUvShapeRes->pStrokeFill = pStrokeFill;

    return pUvShapeRes;
}

CUvShape* CEgf2UvFile::EgfPgon2Uv(CEgfObj *pEgfObj, CUvShape *parent, bool isGroupMask)
{
    if (pEgfObj == nullptr) return nullptr;
    
    bool isMask = IsMask(pEgfObj);
    
    if(!IsVisible(pEgfObj) && (!isMask || !isGroupMask)) return nullptr;

    CUvShape *pUvShapeRes = nullptr;
    CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();

    SetStrokeFillParmsFromStyles(pEgfObj, pStrokeFill, false);

    CUvRectF rect;
    ConvertMer((wchar_t*)L"Mer", pEgfObj, &rect);

    // points
    int *numSubPgons = GetAttrIntValue((wchar_t*)L"NumSubPgons", pEgfObj);
    short *subPgonPoints = GetAttrShortArray((wchar_t*)L"SubPgonNumPoints", pEgfObj);
    int *numPoints = GetAttrIntValue((wchar_t*)L"NumPoints", pEgfObj);
    CEgfPointArray *pointArray = GetAttrPointArray((wchar_t*)L"Points", pEgfObj);
    if (*numSubPgons > 1) // polypolygon: create path
    {
        CUvPath *pUvPath = NEW CUvPath();
        pUvShapeRes = pUvPath;
        int ptCount = 0;
        for (int i = 0; i < *numSubPgons; i++)
        {
            std::vector<CUvPointF> points;
            for(int j = 0; j < subPgonPoints[i]; j++)
            {
                points.push_back( CUvPointF( pointArray->points[ptCount]->x / 4096, pointArray->points[ptCount]->y / 4096 ) );
                ptCount++;
            }
            pUvPath->addPoly(points, true, true);
            points.clear();
        }
    }
    else
    {
        CUvPoly *pUvShape = NEW CUvPoly();
        pUvShapeRes = pUvShape;
        pUvShape->points = NEW CUvPointFs;
        bool *bClosed = GetAttrBoolValue((wchar_t*)L"Closed", pEgfObj);
        pUvShape->bPolygon = (bClosed!= nullptr && *bClosed);

        //int ptCount = 0;
        //CUvPointF *pPoints = ConvertPoints((wchar_t*)L"GradientPoints", pEgfObj, &ptCount);
        //if (pPoints && ptCount > 0)
        //{
        //    for (int i = 0; i < ptCount; i++)
        //        pUvShape->points->points.push_back(CUvPointF(pPoints[i].x / 4096, pPoints[i].y / 4096));
        //}
        //else
        for (int i = 0; i < *numPoints; i++)
            pUvShape->points->points.push_back(CUvPointF(pointArray->points[i]->x / 4096, pointArray->points[i]->y / 4096));
    }

    pUvShapeRes->pStrokeFill = pStrokeFill;

    if (isMask || isGroupMask) // add clip rect to parent group
    {
        if (parent == nullptr && parent->type != uvGroup_ShapeType) return pUvShapeRes;

        CUvGroup *pUvGroup = (CUvGroup*)parent;
        CUvPath *pUvPath;
        if (pUvShapeRes->type == uvPoly_ShapeType) // convert to CUvPath
        {
            pUvPath = NEW CUvPath();
            pUvPath->addPoly(((CUvPoly*)pUvShapeRes)->points->points, false);
            delete pUvShapeRes;
        }
        else
            pUvPath = (CUvPath*)pUvShapeRes;

        pUvGroup->pClipPath = pUvPath; // assign may not be correct, should be joined.
        return nullptr;
    }

    return pUvShapeRes;
}

CUvShape* CEgf2UvFile::EgfEllipse2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr) return nullptr;

    if (!IsVisible(pEgfObj)) return nullptr;

    CUvShape *pUvShapeRes = nullptr;
    CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();

    SetStrokeFillParmsFromStyles(pEgfObj, pStrokeFill, false);

    // points
    CUvPointF center, radius;
    ConvertPoint((wchar_t*)L"Center", pEgfObj, &center);
    ConvertPoint((wchar_t*)L"Radius", pEgfObj, &radius);
    radius.x = fabs(radius.x - center.x);
    radius.y = fabs(radius.y - center.y);

    // XRotation
    double xRotation = (double)ConvertAngle((wchar_t*)L"XRotation", pEgfObj);

    // angles
    int startAngle = ConvertAngle((wchar_t*)L"StartAngle", pEgfObj);
    int endAngle = ConvertAngle((wchar_t*)L"EndAngle", pEgfObj);
    if (((endAngle - startAngle) % 360) == 0) // oval
    {
        CUvOval *pUvOval = NEW CUvOval();
        pUvShapeRes = pUvOval;
        pUvOval->rect.Set(center.x - radius.x, center.x + radius.x, center.y - radius.y, center.y + radius.y);
        pUvOval->xRotation = -xRotation;
    }
    else // angle oval arc
    {
        CUvAngleArc *pUvOvalArc = NEW CUvAngleArc();
        pUvShapeRes = pUvOvalArc;
        pUvOvalArc->oval.Set(center.x - radius.x, center.x + radius.x, center.y - radius.y, center.y + radius.y);
        pUvOvalArc->startAngle = startAngle;
        pUvOvalArc->sweepAngle = (endAngle - startAngle) % 360;
        pUvOvalArc->xRotation = -xRotation;
    }

    pUvShapeRes->pStrokeFill = pStrokeFill;

    return pUvShapeRes;
}

CUvShape* CEgf2UvFile::EgfArc2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr) return nullptr;

    if (!IsVisible(pEgfObj)) return nullptr;

    CUvShape *pUvShapeRes = nullptr;
    CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();

    // stroke
    pStrokeFill->SetStrokeFill(uvStroke_StrokeFill);
    SetStrokeFillParmsFromStyles(pEgfObj, pStrokeFill, false);

    // points
    CUvPointF center, radius;
    ConvertPoint((wchar_t*)L"StartPoint", pEgfObj, &center);
    ConvertPoint((wchar_t*)L"EndPoint", pEgfObj, &radius);

    // angles
    int startAngle = -ConvertAngle((wchar_t*)L"StartAngle", pEgfObj);
    int endAngle = -ConvertAngle((wchar_t*)L"EndAngle", pEgfObj);
    if (((endAngle - startAngle) % 360) == 0) // oval
    {
        CUvOval *pUvOval = NEW CUvOval();
        pUvShapeRes = pUvOval;
        pUvOval->rect.Set(center.x - radius.x, center.x + radius.x, center.y - radius.y, center.y + radius.y);
    }
    else // angle oval arc
    {
        CUvAngleArc *pUvOvalArc = NEW CUvAngleArc();
        pUvShapeRes = pUvOvalArc;
        pUvOvalArc->oval.Set(center.x - radius.x, center.x + radius.x, center.y - radius.y, center.y + radius.y);
        pUvOvalArc->startAngle = startAngle;
        pUvOvalArc->sweepAngle = (endAngle - startAngle) % 360;
    }

    pUvShapeRes->pStrokeFill = pStrokeFill;

    return pUvShapeRes;
}

void CEgf2UvFile::SetStrokeFillParmsFromStyles(CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill, bool bTextObj)
{
    bool bGdiplus = true;
    int *gdiIndex = GetAttrIntValue((wchar_t*)L"GdiIndex", pEgfObj);
    int *gdiplusIndex = GetAttrIntValue((wchar_t*)L"GdiplusIndex", pEgfObj);

    CEgfObj *pGdiFillParms = (pEgfFile && gdiIndex && *gdiIndex > 0 && *gdiIndex <= (int)pEgfFile->m_pGdiFillParmsTable->children.size())? pEgfFile->m_pGdiFillParmsTable->children[*gdiIndex - 1] : nullptr;
    CEgfObj *pGdiplusFillParms = (pEgfFile && gdiplusIndex && *gdiplusIndex > 0 && *gdiplusIndex <= (int)pEgfFile->m_pGdiplusFillParmsTable->children.size())? pEgfFile->m_pGdiplusFillParmsTable->children[*gdiplusIndex - 1] : nullptr;

    pStrokeFill->SetStrokeFill(uvStroke_StrokeFill);

    CUvColor lineColor, fillColor;
    CEgfObj *pParmsObj = nullptr;
    if (pGdiFillParms && pGdiFillParms->attr.size() > 0)
        pParmsObj = pGdiFillParms;
    else
        pParmsObj = pEgfObj;

    {
        // line color
        int *pLineColor = GetAttrIntValue((wchar_t*)L"LineColor", pParmsObj);
        if(gdiplusIndex && *gdiplusIndex > 0) lineColor = MapStrokeFillColor(pLineColor);
        else if (pLineColor) lineColor = MapStrokeFillColor(pLineColor); ;// CUvColor::MakeColor(*pLineColor);
        // fill color
        int *pFillColor = GetAttrIntValue((wchar_t*)L"FillColor", pParmsObj);
        fillColor = MapStrokeFillColor(pFillColor);
        // line style
        ConvertLineStyle((wchar_t*)L"LineStyle", pParmsObj, pStrokeFill);
        // fill style
        ConvertFillStyle((wchar_t*)L"FillStyle", pParmsObj, pStrokeFill);
        // line join style
        ConvertJoinStyle((wchar_t*)L"JoinStyle", pParmsObj, pStrokeFill);
        // line width
        ConvertThickness(pParmsObj, pStrokeFill);

        // fill mode
        int *polyFillMode = GetAttrIntValue((wchar_t*)L"PolyFillMode", pParmsObj);
        if (polyFillMode)
            pStrokeFill->SetFillMode((UvFillMode)*polyFillMode);
    }

    lineColor.a = 255;
    fillColor.a = 255;
    if (pGdiplusFillParms && pGdiplusFillParms->attr.size() > 0)
        pParmsObj = pGdiplusFillParms;
    else
        pParmsObj = pEgfObj;

    {
        int *lineAlpha = GetAttrIntValue((wchar_t*)L"LineAlpha", pParmsObj);
        int *fillAlpha = GetAttrIntValue((wchar_t*)L"FillAlpha", pParmsObj);
        if (lineAlpha) lineColor.a = *lineAlpha;
        if (fillAlpha) fillColor.a = *fillAlpha;

        if (*pStrokeFill->GetStrokeFill() == uvStroke_StrokeFill && lineColor.a == 0 && fillColor.a > 0)
            lineColor.a = fillColor.a;

        // cap style
        if (ConvertCapStyle((wchar_t*)L"EndCapStyle", pParmsObj, pStrokeFill) == false)
        {
            ConvertCapStyle((wchar_t*)L"CapStyle", pParmsObj, pStrokeFill);
        }

        // miter limit
        float *miterLimit = GetAttrFloatValue((wchar_t*)L"MiterLimit", pParmsObj);
        if (miterLimit)
            pStrokeFill->SetMitLimit((int)*miterLimit);
    }

    pStrokeFill->SetStrokeColor(lineColor);
    if (bTextObj)
        pStrokeFill->AppendFillColor(lineColor);
    else
        pStrokeFill->AppendFillColor(fillColor);

    if (pStrokeFill->GetFillType() == uvLinearGradient_FillType || pStrokeFill->GetFillType() == uvRadialGradient_FillType)
        ConvertGradientFill(pEgfObj, pStrokeFill);
}

typedef unsigned int EGF_CURVESTYLE, *EGF_CURVESTYLE_PTR; /* fits in a 2 bit field */
#define EGF_CURVENONE       (EGF_CURVESTYLE)0 
#define EGF_CURVEBSPLINE    (EGF_CURVESTYLE)1  /* Bezier Spline */
#define EGF_CURVECSPLINE    (EGF_CURVESTYLE)2  /* Cardinal Spline */
#define EGF_CURVEUNASSIGNED (EGF_CURVESTYLE)3

int  MPL_NormalizeXAngle(int inXAngle)
{
    if (inXAngle < 0) {
        do {
            inXAngle += 360;
        } while (inXAngle < 0);
    }
    else if (inXAngle > 359) {
        do {
            inXAngle -= 360;
        } while (inXAngle > 359);
    } /* if */
    return inXAngle;
}

CUvShape* CEgf2UvFile::EgfPath2Uv(CEgfObj *pEgfObj, CUvShape *parent, bool bSubPath)
{
    if (pEgfObj == nullptr) return nullptr;

    if (!IsVisible(pEgfObj)) return nullptr;


    CUvPath *pUvPath = NEW CUvPath();
    std::vector<CUvPathOp*>* pOps = pUvPath->getOps();

    // convert attrs
    CUvRectF rect;
    ConvertMer((wchar_t*)L"Mer", pEgfObj, &rect);

    CUvShape *pUvShapeRes = nullptr;

    // stroke
    if (bSubPath)
        pUvPath->pStrokeFill = nullptr;
    else
    {
        CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();
        SetStrokeFillParmsFromStyles(pEgfObj, pStrokeFill, false);
        pUvPath->pStrokeFill = pStrokeFill;
    }

    // Center point, translation, rotation
    CUvPointF center, translation;
    ConvertPoint((wchar_t*)L"CenterPoint", pEgfObj, &center);
    ConvertPoint((wchar_t*)L"Translation", pEgfObj, &translation);
    int rotAngle = GetIntValue((wchar_t*)L"RotAngle", pEgfObj);
    if (rotAngle > 0)
    {
        CUvMatrix m, rot;
        m.setTranslation(-center.x, -center.y);
        rot.setRotation((float)(-rotAngle * DEG2RAD));
        pUvPath->mat = m * rot;
        m.setTranslation(center.x + translation.x, center.y + translation.y);
        pUvPath->mat = pUvPath->mat * m;
    }

    int gradientIndex = GetIntValue((wchar_t*)L"GradientIndex", pEgfObj);
    int gradientNumPoints = GetIntValue((wchar_t*)L"GradientNumPoints", pEgfObj);
    if (gradientIndex > 0 && gradientNumPoints > 0)
    {
        int count = 0;
        CUvPointF *pPoints = ConvertPoints((wchar_t*)L"GradientPoints", pEgfObj, &count);
        CUvPointFs uvPoints;
        for (int i = 0; i < count; i++)
            uvPoints.points.push_back(pPoints[i]);
        pUvPath->pStrokeFill->SetLinearGradientPoints(&uvPoints);
        delete[] pPoints;
    }

    bool *pClosed = GetAttrBoolValue((wchar_t*)L"ClosedFigure", pEgfObj);
    pUvPath->bClosed = (pClosed && *pClosed);
    bool bFirstPoint = true;

    // convert children
    U32 numChildren = (U32)pEgfObj->children.size();
    for (U32 i = 0; i < numChildren; i++)
    {
        bool supported = true;
        wchar_t *pMetaName = pEgfObj->children[i]->GetMetaName();
        pEgfObj->children[i]->pUvObj = nullptr;// pUvPath;
        if (wcscmp(pMetaName, L"Arc") == 0)
        {
            // points
            CUvPointF center, radius;
            ConvertPoint((wchar_t*)L"Center", pEgfObj->children[i], &center);
            ConvertPoint((wchar_t*)L"Radii", pEgfObj->children[i], &radius);

            CUvRectF oval;
            oval.Set(center.x - radius.x, center.x + radius.x, center.y - radius.y, center.y + radius.y);

            // angles
            int startAngle = ConvertAngle((wchar_t*)L"StartAngle", pEgfObj->children[i]);
            int endAngle = ConvertAngle((wchar_t*)L"EndAngle", pEgfObj->children[i]);
            bool bClosewise = GetBoolValue((wchar_t*)L"ClockWise", pEgfObj->children[i]);
            bool b360 = (abs(endAngle - startAngle) == 360);

            // test files
            // camera.svg, shapes-rect-BE-01.svg, path-curves-BE-04.svg
            bool bStartAngleNegative = (startAngle < 0);
            int sweepAngle = 0;
            startAngle = MPL_NormalizeXAngle(startAngle);
            endAngle = MPL_NormalizeXAngle(endAngle);
            //if (endAngle < startAngle) startAngle -= 360;
            if (endAngle < startAngle) endAngle += 360;
            if (bClosewise)
            {
                sweepAngle = abs(endAngle - startAngle);
                if (bStartAngleNegative) // ExperienceSuiteFeatureOverview.pptx page28, and path-curves-BE-04.svg
                {
                    if (endAngle <= 90)  startAngle = -endAngle;
                    else startAngle = 360 - endAngle;
                }
            }
            else
            {
                sweepAngle = -abs(endAngle - startAngle);
                startAngle = -startAngle;
            }
            if (b360) sweepAngle = 360;

            pUvPath->arcTo(oval, (float)(startAngle), (float)sweepAngle);
        }
        else if (wcscmp(pMetaName, L"Path") == 0)
        {
            CUvShape *pSubPath = EgfPath2Uv(pEgfObj->children[i], pUvPath, true);
            if (pSubPath)
            {
                pUvPath->addPath(*(CUvPath*)pSubPath);
                //delete pSubPath;
            }
        }
        else if (wcscmp(pMetaName, L"Line") == 0)
        {
            CUvPointF startPoint, endPoint;
            ConvertPoint((wchar_t*)L"StartPoint", pEgfObj->children[i], &startPoint);
            ConvertPoint((wchar_t*)L"EndPoint", pEgfObj->children[i], &endPoint);
            if(bFirstPoint) pUvPath->moveTo(startPoint);
            else pUvPath->lineTo(startPoint);
            pUvPath->lineTo(endPoint);
        }
        else if (wcscmp(pMetaName, L"Polyline") == 0)
        {
            int count = 0;
            CUvPointF *pPoints = ConvertPoints((wchar_t*)L"Points", pEgfObj->children[i], &count);
            if (pPoints)
            {
                pUvPath->addPoly(pPoints, count, !bFirstPoint);
                delete[] pPoints;
            }
        }
        else if (wcscmp(pMetaName, L"Polygon") == 0)
        {
            int count = 0;
            CUvPointF *pPoints = ConvertPoints((wchar_t*)L"Points", pEgfObj->children[i], &count);
            if (pPoints)
            {
                int curveStyle = GetIntValue((wchar_t*)L"CurveStyle", pEgfObj->children[i]);

                if (curveStyle == EGF_CURVEBSPLINE && count > 3)
                {
                    if(bFirstPoint)
                        pUvPath->moveTo(pPoints[0]);
                    else
                        pUvPath->lineTo(pPoints[0]);
                    pUvPath->cubicTo(pPoints[1], pPoints[2], pPoints[3]);
                }
                else
                {
                    pUvPath->addPoly(pPoints, count, !bFirstPoint, true);
                }
                
                delete[] pPoints;
            }
        }
        else if (wcscmp(pMetaName, L"Ellipse") == 0)
        {
            supported = false;
        }
        else if (wcscmp(pMetaName, L"Text") == 0)
        {
            supported = false;
        }
        else
        {
            supported = false;
        }

#ifdef _WINDOWS
        assert(supported);
#endif

        if (supported && pOps->size() > 0)
            pEgfObj->children[i]->pUvObj = (*pOps)[pOps->size() - 1]->pShape;

        bFirstPoint = false;
    }

    return pUvPath;
}

CUvShape* CEgf2UvFile::EgfLine2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr || IsVisible(pEgfObj) == false) return nullptr;

    CUvLine *pUvShape = NEW CUvLine();

    CEgfAttr *pEgfAttr = nullptr;

    ConvertPoint((wchar_t*)L"StartPoint", pEgfObj, &pUvShape->point[0]);
    ConvertPoint((wchar_t*)L"EndPoint", pEgfObj, &pUvShape->point[1]);

    pUvShape->pStrokeFill = NEW CUvStrokeFill();
    SetStrokeFillParmsFromStyles(pEgfObj, pUvShape->pStrokeFill, false);

    return pUvShape;
}

typedef unsigned int EGF_TYPEFACE, *EGF_TYPEFACE_PTR;
#define EGF_TYPENORMAL      (EGF_TYPEFACE)0x00    /* normal weight, typeface (def, no modifiers). */
#define EGF_TYPEBOLD        (EGF_TYPEFACE)0x01    /* bold weight. */
#define EGF_TYPEFINE        (EGF_TYPEFACE)0x02    /* fin weight. */
#define EGF_TYPEITALIC      (EGF_TYPEFACE)0x80    /* italic typeface. */
#define EGF_TYPEUNDERLN     (EGF_TYPEFACE)0x40    /* underlining ON. */
#define EGF_TYPESTRKOUT     (EGF_TYPEFACE)0x20    /* strikeout ON. */
#define EGF_TYPEMULTIPLE    (EGF_TYPEFACE)254
#define EGF_TYPEUNASSIGNED  (EGF_TYPEFACE)255

typedef unsigned int EGF_JUSTIFICATION, *EGF_JUSTIFICATION_PTR;  /* fits in a 7 bit field */
#define EGF_JUSTLEFT        (EGF_JUSTIFICATION)0   /* left */
#define EGF_JUSTCENTER      (EGF_JUSTIFICATION)1   /* center */
#define EGF_JUSTRIGHT       (EGF_JUSTIFICATION)2   /* right */
#define EGF_JUSTALIGNED     (EGF_JUSTIFICATION)3   /* aligned */
#define EGF_JUSTMIDDLEH     (EGF_JUSTIFICATION)4   /* middle */
#define EGF_JUSTFIT         (EGF_JUSTIFICATION)5   /* fit */
#define EGF_JUST_HMASK      (EGF_JUSTIFICATION)7   /* mask for horizontal portion. */ 
/* these or'd in to get left,bottom etc. */
#define EGF_JUSTTOP         (EGF_JUSTIFICATION)0x00   /* top */
#define EGF_JUSTBOTTOM      (EGF_JUSTIFICATION)0x08   /* bottom */
#define EGF_JUSTMIDDLEV     (EGF_JUSTIFICATION)0x10   /* middle */
#define EGF_JUSTBASELINE    (EGF_JUSTIFICATION)0x18   /* baseline */
#define EGF_JUST_VMASK      (EGF_JUSTIFICATION)0x18   /* mask for vertical portion */
#define EGF_JUSTMULTIPLE    (EGF_JUSTIFICATION)0x20
#define EGF_JUSTUNASSIGNED  (EGF_JUSTIFICATION)0x40   


CUvShape* CEgf2UvFile::EgfExtText2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr || IsVisible(pEgfObj) == false) return nullptr;

    CUvText *pUvText = NEW CUvText();

    pUvText->pStrokeFill = NEW CUvStrokeFill();
    SetStrokeFillParmsFromStyles(pEgfObj, pUvText->pStrokeFill, true);


    return nullptr;
}

CUvShape* CEgf2UvFile::EgfText2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr || IsVisible(pEgfObj) == false) return nullptr;

    CUvText *pUvText = NEW CUvText();

    pUvText->pStrokeFill = NEW CUvStrokeFill();
    SetStrokeFillParmsFromStyles(pEgfObj, pUvText->pStrokeFill, true);

    // start point
    CUvPointF dropPoint;
    ConvertPoint((wchar_t*)L"DropPoint", pEgfObj, &dropPoint);
    pUvText->x = dropPoint.x;
    pUvText->y = dropPoint.y;

    // Text length
    pUvText->textLen = GetIntValue((wchar_t*)L"TextLength", pEgfObj);
    int srcLen = pUvText->textLen;

    // charset
    int charset = GetIntValue((wchar_t*)L"Charset", pEgfObj);
    if(charset == CHARSET_PDF_CID)
    {
        pUvText->encoding = uvUTF16_TextEncoding; // CID
    }
    else if(charset == CHARSET_SHIFTJIS) // japanese: code page 932
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_JAPANESE;
    }
    else if(charset == CHARSET_HANGEUL) // korean: code page 949
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_HANGEUL;
    }
    else if(charset == CHARSET_GB2312) // chinese: code page 936
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_GB2312;
    }
    else if(charset == CHARSET_CHINESEBIG5) // traditional chinese: code page 950
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_CHINESEBIG5;
    }
    else if(charset == CHARSET_GREEK) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_GREEK;
    }
    else if(charset == CHARSET_TURKISH) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_TURKISH;
    }
    else if(charset == CHARSET_HEBREW) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_HEBREWANSI;
    }
    else if(charset == CHARSET_ARABIC) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_ARABICANSI;
    }
    else if(charset == CHARSET_BALTIC) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_BALTIC;
    }
    else if(charset == CHARSET_VIETNAMESE) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_VIETNAMESE;
    }
    else if(charset == CHARSET_JOHAB) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_JOHAB;
    }
    else if(charset == CHARSET_ANSI) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_WESTERN;
    }
    else if(charset == CHARSET_PDFEMBEDFONT) // 
    {
        pUvText->encoding = uvUTF16_TextEncoding;
        pUvText->codepage = CODEPAGE_WESTERN;
    }
    else
    {
        pUvText->encoding = uvUTF8_TextEncoding;
        pUvText->codepage = CODEPAGE_NONE;// CODEPAGE_WESTERN;
    }

    // search by glyph index
    //bool searchByGlyphIndex = GetBoolValue((wchar_t*)L"Charset", pEgfObj);
    //if (searchByGlyphIndex) pUvText->encoding = uvGlyphID_TextEncoding;

    // Text
    unsigned char *pString = (unsigned char*)GetStringValue((wchar_t*)L"Text", pEgfObj);
    if (pString)
    {
        //if (pUvText->encoding == uvUTF8_TextEncoding)
        //{
        //    CVector<unsigned char> src;
        //    UV_1ByteStringTo2BytesUtf8(pString, pUvText->textLen, &src);
        //    pUvText->textLen = src.size();
        //    if (pUvText->textLen > 0)
        //    {
        //        pUvText->text = NEW U8[pUvText->textLen + 2];
        //        for (int i = 0; i < pUvText->textLen; i++)
        //            pUvText->text[i] = src[i];
        //        pUvText->text[pUvText->textLen] = 0;
        //        pUvText->text[pUvText->textLen + 1] = 0;
        //    }
        //}
        //else
        {
            pUvText->textLen = srcLen;
            if (pUvText->textLen > 0)
            {
                pUvText->text = NEW U8[pUvText->textLen + 2];
                memcpy(pUvText->text, pString, pUvText->textLen);
            }
        }
        if(charset == CHARSET_PDF_CID)
        {
            for(int i = 0; i < pUvText->textLen; i+=2)
            {
                char c = pUvText->text[i];
                pUvText->text[i] = pUvText->text[i + 1];
                pUvText->text[i + 1] = c;
            }
        }

        if(pUvText->encoding == uvUTF16_TextEncoding && charset != CHARSET_PDF_CID)//&& skEncoding != SkTextEncoding::kGlyphID)
        {
            unsigned char *pText = new unsigned char[pUvText->textLen * 2 + 2];
            int text_len = pUvText->textLen;
            text_len = CharCodeMapping( pUvText, pText );
            if(text_len > 0)
            {
                pUvText->convertedText = pText;
                pUvText->convertedTextLen = text_len;
            }
            else
                delete[] pText;
        }

    }
        
    // font
    pString = (unsigned char*)GetStringValue((wchar_t*)L"Font", pEgfObj);
    if (pString)
    {
        CUvPage *pUvPage = (CUvPage*)getUvPage(parent);
        if (pUvPage)
            pUvText->pFontFace = pUvPage->fontList->findFace((const U8*)pString);
        if (pUvText->pFontFace == nullptr)
        {
            CUvDoc *pUvDoc = (CUvDoc*)getUvDoc(parent);
            if (pUvDoc)
                pUvText->pFontFace = pUvDoc->fontList->findFace((const U8*)pString);
        }

        //if(pUvText->pFontFace && strstr( (char*)pString, "MSTT31" ))
        //    pUvText->pFontFace = nullptr;
    }

    if(pUvText->pFontFace == nullptr && pString)
    {
        pUvText->pFontFace = NEW CUvFontFace();
        pUvText->pFontFace->setFamilyName((U8*)pString);
        pUvText->bReleaseFontFace = true;
    }

    // font size
    int heightUnits = GetIntValue((wchar_t*)L"HeightUnits", pEgfObj);
    float heightValue = GetFloatValue((wchar_t*)L"HeightValue", pEgfObj);
    //pUvText->fontSize = (float)(S32)(TransMeas2Meas(heightUnits, heightValue, IM_UNITS_POINTS) + 0.5f);
    pUvText->fontSize = (float)TransMeas2Meas(heightUnits, heightValue, IM_UNITS_PELS) ;

    // font width
    int widthUnits = GetIntValue((wchar_t*)L"WidthUnits", pEgfObj);
    float widthValue = GetFloatValue((wchar_t*)L"WidthValue", pEgfObj);
    float textScaledWidth = (float)TransMeas2Meas(widthUnits, widthValue, IM_UNITS_PELS);

    // font avg width
    int avgWidth = GetIntValue((wchar_t*)L"AvgWidth", pEgfObj);
    int fontHeight = GetIntValue((wchar_t*)L"FontHeight", pEgfObj);
    pUvText->scaleX = 1.0f * textScaledWidth * fontHeight / avgWidth / pUvText->fontSize;
    if (pUvText->scaleX == 0.0f) pUvText->scaleX = 1.0f;

    // styles
    if (pUvText->pFontFace)
    {
        int typeface = GetIntValue((wchar_t*)L"TypeFace", pEgfObj);
        pUvText->pFontFace->setItalic((typeface & EGF_TYPEITALIC) > 0);
        pUvText->pFontFace->setStrikeOut(typeface & EGF_TYPESTRKOUT ? true : false);
        pUvText->pFontFace->setUnderline(typeface & EGF_TYPEUNDERLN ? true : false);
    }

    // rotation
    float rotation = (float)((GetIntValue((wchar_t*)L"Rotation", pEgfObj) % 3600) / 10.0 / 180 * PI);
    pUvText->orientation = (float)((GetIntValue((wchar_t*)L"Orientation", pEgfObj) % 3600) / 10.0 / 180 * PI);
    CUvMatrix tmpMat;
    tmpMat.setRotation(rotation);
    pUvText->mat.setTranslation(-pUvText->x, -pUvText->y);
    pUvText->mat = pUvText->mat * tmpMat;
    tmpMat.set(1, 0, 0, 1, pUvText->x, pUvText->y);
    pUvText->mat = pUvText->mat * tmpMat;

    // text alignment
    int justification = GetIntValue((wchar_t*)L"Justification", pEgfObj);
    switch (justification & EGF_JUST_HMASK)
    {
    case EGF_JUSTCENTER:
    case EGF_JUSTMIDDLEH:
        pUvText->align = uvCenter_TextAlign;
        break;
    case EGF_JUSTRIGHT:
        pUvText->align = uvRight_TextAlign;
        break;
    case EGF_JUSTLEFT:
    case EGF_JUSTALIGNED:
    default:
        pUvText->align = uvLeft_TextAlign;
        break;
    }

    return pUvText;
}
