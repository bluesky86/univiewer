#include "stdafx.h"
#include "CEgf2UvFile.h"
#include "tinyxml2.h"
#include "Error.h"
#include "UvFile.h"
#include "UvmUtils.h"
#include "CUvPoint.h"
#include "CUvPath.h"
#include "CUvRefer.h"
#include "CUvImage.h"
#include "CUvText.h"
#include "CUvFont.h"
#include <string>
#include <vector>
#include "DebugNew.h"
#include <fstream>
#include "EgfObj.h"
#include "assert.h"

#define  MPL_MIRROR     0x04

#define  MPL_ROT0       0x00
#define  MPL_ROT90      0x01
#define  MPL_ROT180     0x02
#define  MPL_ROT270     0x03
#define  MPL_ROT0M      (MPL_MIRROR + MPL_ROT0)
#define  MPL_ROT90M     (MPL_MIRROR + MPL_ROT90)
#define  MPL_ROT180M    (MPL_MIRROR + MPL_ROT180)
#define  MPL_ROT270M    (MPL_MIRROR + MPL_ROT270)

int _RSL_IDF_map[33] = {
//          0   1   2   3   4   5   6   7   8   9
/* 0 */     0,  1,  1,  1,  1,  8,  8,  8,  24, 8,
/* 1 */     1,  1,  1,  8,  24, 24, 1,  24, 24, 1,
/* 2 */     0,  24, 1,  1,  1,  1,  0,  0,  0,  0,
/* 3 */     32, 32, 32,
};


CUvShape* CEgf2UvFile::EgfEmf2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr) return nullptr;

    if (!IsVisible(pEgfObj)) return nullptr;

    bool supported = true;

    //CUvShape *pUvShapeRes = nullptr;
    CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();

    SetStrokeFillParmsFromStyles(pEgfObj, pStrokeFill, false);

    CUvImage *pUvImage = NEW CUvImage();

    int type = GetIntValue((wchar_t*)L"Type", pEgfObj);
    if (type == 0) pUvImage->imageType = uvImageType_wmf;
    else pUvImage->imageType = uvImageType_emf;

    pUvImage->pStrokeFill = pStrokeFill;


    CUvRectF uvRectF;
    ConvertMer((wchar_t*)L"Frame", pEgfObj, &uvRectF);
    pUvImage->x = (int)uvRectF.left;
    pUvImage->y = (int)uvRectF.top;
    pUvImage->targetWidth = (int)uvRectF.Width();
    pUvImage->targetHeight = (int)uvRectF.Height();

    // data
    int size = GetIntValue((wchar_t*)L"Size", pEgfObj);
    if (size > 0)
    {
        CUvRasterStrip *pStrip = NEW CUvRasterStrip();

        pStrip->numBlocks = 1;

        CUvRasterStripBlock *pBlock = NEW CUvRasterStripBlock();
        pBlock->tileIndex = 0;
        pBlock->size = size;
        char *pData = GetStringValue((wchar_t*)L"Data", pEgfObj);
        pBlock->data = NEW unsigned char[pBlock->size];
        memcpy(pBlock->data, pData, pBlock->size);
        pStrip->pBlock = pBlock;
        pBlock->next = nullptr;

        pUvImage->data.push_back(pStrip);
        pUvImage->numStrips = 1;
    }

    return pUvImage;
}

CUvShape* CEgf2UvFile::EgfPaste2Uv(CEgfObj *pEgfObj, CUvShape *parent)
{
    if (pEgfObj == nullptr) return nullptr;

    if (!IsVisible(pEgfObj)) return nullptr;

    bool supported = true;

    //CUvShape *pUvShapeRes = nullptr;
    CUvStrokeFill *pStrokeFill = NEW CUvStrokeFill();

    SetStrokeFillParmsFromStyles(pEgfObj, pStrokeFill, false);

    CUvImage *pUvImage = NEW CUvImage();

    pUvImage->imageType = uvImageType_strip;
    pUvImage->pStrokeFill = pStrokeFill;

    bool isNegative = false;

    pUvImage->numBits = GetIntValue((wchar_t*)L"InNumbits", pEgfObj);
    pUvImage->numColors = GetIntValue((wchar_t*)L"NumColours", pEgfObj);
    if (pUvImage->numColors == 0)  pUvImage->numColors = 1 << pUvImage->numBits;

    RSL_RAST_DATA_PTR pRastData = GetRasterStruct((wchar_t*)L"RastStruct", pEgfObj);
    if (pRastData)
    {
        pUvImage->numStrips = pRastData->numstrips;

        switch (pRastData->dataType)
        {
        case 1: // RSL_IDF_NRLE
            pUvImage->encodingType = uvImageEncoding_Nibble;
            break;
        case 12: // RSL_IDF_G4M : Group 4 MSB
        case 16: // RSL_IDF_G4MT : tiled
            pUvImage->encodingType = uvImageEncoding_G4M;
            break;
        case 26: // RSL_IDF_RAW
            pUvImage->encodingType = uvImageEncoding_Raw;
            break;
        case 27: // RSL_IDF_RAWT: raw uncompressed tiled data
            pUvImage->encodingType = uvImageEncoding_RawTiled;
            break;
        case 13: // RSL_IDF_CC1: CC1 - 8 bits
            pUvImage->encodingType = uvImageEncoding_CC1;
            break;
        case 32: // RSL_IDF_CC1RGBA - 32 bits
            pUvImage->encodingType = uvImageEncoding_CC1RGBA;
            pUvImage->numStrips *= pRastData->numplanes;
            break;
        case 8: // RSL_IDF_24BP
            pUvImage->encodingType = uvImageEncoding_24BP;
            pUvImage->numStrips *= pRastData->numplanes;
            break;
        case 14: // RSL_IDF_CC1RGB
            pUvImage->encodingType = uvImageEncoding_CC1RGB;
            pUvImage->numStrips *= pRastData->numplanes;
            break;
        case 17: // RSL_IDF_JPEG
            pUvImage->encodingType = uvImageEncoding_JPEG;
            pUvImage->jpegParams = GetJpegStruct((wchar_t*)L"JpegStruct", pEgfObj);
            break;
        case 21: // RSL_IDF_SJPEG
            pUvImage->encodingType = uvImageEncoding_SJPEG;
            pUvImage->jpegParams = GetJpegStruct((wchar_t*)L"JpegStruct", pEgfObj);
            break;
        default:
            supported = false;
            break;
        }

        if (supported == false)
        {
            assert(0);
            //pUvImage->numStrips = 0;
            //delete pUvImage;
            return nullptr;
        }

        if (pUvImage->numBits == 0)
            pUvImage->numBits = _RSL_IDF_map[pRastData->dataType];
        //pUvImage->numBits = pRastData->numplanes * 8;// _RSL_IDF_map[pRastData->dataType];

        pUvImage->imageWidth = pRastData->imagewidth;
        pUvImage->imageHeight = pRastData->imagelength;

        pUvImage->tileWidth = pRastData->twidth;
        if(pRastData->imagelength < pRastData->tlength)
            pUvImage->tileHeight = pRastData->imagelength;
        else
            pUvImage->tileHeight = pRastData->tlength;
        pUvImage->numTiles = pRastData->dmNumTiles;
        pUvImage->numTilesPerWidth = pRastData->dmTilesWide;
        if (pRastData->dmNumTiles > 0 && pRastData->dmTilesWide > 0 && (pUvImage->imageWidth != pUvImage->tileWidth || pUvImage->imageHeight != pUvImage->tileHeight))
            pUvImage->bTiled = true;

        isNegative = pRastData->negative;

        if (pRastData->dmHeaderType == 0x2100) // Windows BMP
        {
            //pUvImage->origin = uvBottomLeft_Image;
            pUvImage->bFlipRGB = true;
        }
        //else
        {
            int rot = GetIntValue((wchar_t*)L"DataRot", pEgfObj);
            switch (rot)
            {
            case MPL_ROT0:
                pUvImage->origin = uvTopLeft_Image;
                break;
            case MPL_ROT90:
                pUvImage->origin = uvLeftBottom_Image;
                break;
            case MPL_ROT180:
                pUvImage->origin = uvBottomRight_Image;
                break;
            case MPL_ROT270:
                pUvImage->origin = uvRightTop_Image;
                break;
            case MPL_ROT0M:
                pUvImage->origin = uvTopRight_Image;
                break;
            case MPL_ROT90M:
                pUvImage->origin = uvRightBottom_Image;
                break;
            case MPL_ROT180M:
                pUvImage->origin = uvBottomLeft_Image;
                break;
            case MPL_ROT270M:
                pUvImage->origin = uvLeftTop_Image;
                break;
            }
        }

        //FILE *fp;
        //char buf[256];
        //sprintf_s( buf, 256, "C:\\Work\\Test\\tmp2\\a_24_24_%d_%d.dat", pUvImage->imageWidth, pUvImage->imageHeight );
        //fopen_s( &fp, buf, "wb" );
        for (int i = 0; i < pUvImage->numStrips && i < (int)pEgfObj->children.size(); i++)
        {
            CEgfObj *pEgfStripObj = pEgfObj->children[i];
            CUvRasterStrip *pStrip = NEW CUvRasterStrip();

            pStrip->width = GetIntValue((wchar_t*)L"StripWidth", pEgfStripObj);
            pStrip->numBlocks = GetIntValue((wchar_t*)L"NumBlocks", pEgfStripObj);

            CUvRasterStripBlock *pBlock = nullptr, *pPrevBlock = nullptr;
            int numBlocks = 0;
            for (int j = 0; j < (int)pEgfStripObj->children.size(); j++)
            {
                CEgfObj *pEgfBlockObj = pEgfStripObj->children[j];
                pBlock = NEW CUvRasterStripBlock();
                pBlock->tileIndex = GetIntValue((wchar_t*)L"TileIndex", pEgfBlockObj);
                pBlock->size = GetIntValue((wchar_t*)L"NumBytes", pEgfBlockObj);
                char *pData = GetStringValue((wchar_t*)L"Data", pEgfBlockObj);
                pBlock->data = NEW unsigned char[pBlock->size];
                memcpy(pBlock->data, pData, pBlock->size);
                //fwrite( pBlock->data, 1, pBlock->size, fp );
                if (pStrip->pBlock == nullptr) pStrip->pBlock = pBlock;
                else if(pPrevBlock) pPrevBlock->next = pBlock;
                pPrevBlock = pBlock;
                numBlocks++;
            }
            if (pStrip->numBlocks < numBlocks)
                pStrip->numBlocks = numBlocks;

            pUvImage->data.push_back(pStrip);
        }
        //fclose( fp );
    }

    // transparet type
    int transparentType = GetIntValue((wchar_t*)L"TransparentType", pEgfObj);
    int transparentColor = GetIntValue((wchar_t*)L"TransparentColor", pEgfObj);

    // palette
    int numAlpha = GetIntValue((wchar_t*)L"NumAlpha", pEgfObj);
    unsigned char *pAlpha = (unsigned char*)GetStringValue((wchar_t*)L"PalAlpha", pEgfObj);
    int numColorInPal = GetIntValue((wchar_t*)L"NumColorInPalette", pEgfObj);
    if (numColorInPal > 0)
    {
        pUvImage->palette = NEW CUvColor[numColorInPal];
        int *pOrigArray = GetAttrIntArray((wchar_t*)L"Palette", pEgfObj);
        for (int i = 0; pOrigArray && i < numColorInPal; i++)
        {
            pUvImage->palette[i] = CUvColor::MakeColor(pOrigArray[i]);
            if (pAlpha && i < numAlpha)
                pUvImage->palette[i].a = pAlpha[i];
            else if (transparentType == 4 && transparentColor == i)
                pUvImage->palette[i].a = 0;
            else
                pUvImage->palette[i].a = 255;
        }
        //if (!isNegative && numColorInPal == 2 && pUvImage->numBits == 1)
        //{
        //    CUvColor color = pUvImage->palette[0];
        //    pUvImage->palette[0] = pUvImage->palette[1];
        //    pUvImage->palette[1] = color;
        //}
    }

    // ImgEffectType
    int effectType = GetIntValue((wchar_t*)L"ImgEffectType", pEgfObj);
    if (effectType == 1 || effectType == 2) // map one color to another
    {
        CUvMap<CUvColor, CUvColor>::CUvMapEle *pMapEle = NEW CUvMap<CUvColor, CUvColor>::CUvMapEle;
        int v = GetIntValue((wchar_t*)L"ColorOne", pEgfObj);
        pMapEle->first = CUvColor::MakeColor(v);
        v = GetIntValue((wchar_t*)L"ColorTwo", pEgfObj);
        pMapEle->second = CUvColor::MakeColor(v);
        pUvImage->colorMap.push_back(pMapEle);
        if(effectType == 1) pUvImage->colorEffectType = uvImageColorEffectType_colorChange;
        else
            pUvImage->colorEffectType = uvImageColorEffectType_duoTone;
    }
    else if(effectType == 3)
        pUvImage->colorEffectType = uvImageColorEffectType_grayScale;

    // mer
    CUvRectF uvRectF;
    ConvertMer((wchar_t*)L"Frame", pEgfObj, &uvRectF);
    pUvImage->x = (int)uvRectF.left;
    pUvImage->y = (int)uvRectF.top;
    pUvImage->targetWidth = (int)uvRectF.Width();
    pUvImage->targetHeight = (int)uvRectF.Height();

    int userRot = GetIntValue((wchar_t*)L"UserRot", pEgfObj) / 10;
    if (userRot != 0)
    {
        CUvMatrix m, rot;
        m.setTranslation((float)-pUvImage->x, (float)-pUvImage->y);
        rot.setRotation((float)(userRot * DEG2RAD));
        pUvImage->mat = m * rot;
        m.setTranslation((float)pUvImage->x, (float)pUvImage->y);
        pUvImage->mat = pUvImage->mat * m;
    }


    return pUvImage;
}
