#include "stdafx.h"
#include "EgfApi.h"
#include "EgfObj.h"
#include "EgfFile.h"
#include "tinyxml2.h"
#include "DebugNew.h"
#include "UvmUtils.h"
#include <string>
#include "Egf2Uv.h"
#include "CEgf2UvFile.h"

//class CEgfSearchMetaList;
//class CEgfMetaList;
extern CEgfMetaList gEgfMetaList;  // egf meta array
extern std::map<int, int> *gMetaSeqToIndexMap;  // egf meta sequence to array index of gEgfMetaList
extern std::map<int, int> *gEgfIdToIndexMap;   // egf id in imagenation to index of gEgfMetaList
extern CEgfSearchMetaList gEgfSearchMetaList;

void SetError(const wchar_t *msg);
void ClearError();
//void ConvertDashPatterns(CEgfObj *pDashs, CUvLinePatterns &uvDashPatterns);


bool LoadNumId(CEgfEleMeta *pEleMeta, tinyxml2::XMLElement* xmlEgfObj)
{
    const char *sValue = xmlEgfObj->Attribute("num_id");
    if (sValue == NULL || strcmp(sValue, "no") == 0)
    {
        sValue = xmlEgfObj->Attribute("num");
        if (sValue == NULL)
        {
            SetError(L"Parse xml EgfObjList failed!");
            return false;
        }
        pEleMeta->num = std::stoi(sValue, 0, 10);
    }
    else
        pEleMeta->num_id = std::stoi(sValue, 0, 10);

    return true;
}

bool GetValueByType(tinyxml2::XMLElement* xmlEgfMetaEle, CEgfMeta *pMeta, CEgfEleMeta *pEleMeta, const char* typeStr)
{
    const char* sValue = xmlEgfMetaEle->Attribute(typeStr);
    if (sValue == NULL)
    {
        SetError(L"Parse xml EgfObjList failed!");
        return false;
    }
    if (strcmp(sValue, "int") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_int;
    else if (strcmp(sValue, "8bool") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_8bool;
    else if (strcmp(sValue, "byte") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_byte;
    else if (strcmp(sValue, "intBool") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_intBool;
    else if (strcmp(sValue, "color3") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_color3;
    else if (strcmp(sValue, "color4") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_color4;
    else if (strcmp(sValue, "point") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_point;
    else if (strcmp(sValue, "mer") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_mer;
    else if (strcmp(sValue, "LineStyle") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_lineStyle;
    else if (strcmp(sValue, "CapStyle") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_capStyle;
    else if (strcmp(sValue, "Operation") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_operation;
    else if (strcmp(sValue, "mer") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_mer;
    else if (strcmp(sValue, "FillStyle") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_fillStyle;
    else if (strcmp(sValue, "HatchStyle") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_hatchStyle;
    else if (strcmp(sValue, "ShadingStyle") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_shadingStyle;
    else if (strcmp(sValue, "JoinStyle") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_joinStyle;
    else if (strcmp(sValue, "CurveStyle") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_curveStyle;
    else if (strcmp(sValue, "EffectType") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_effectType;
    else if (strcmp(sValue, "float") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_float;
    else if (strcmp(sValue, "double") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_double;
    else if (strcmp(sValue, "matrix") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_matrix;
    else if (strcmp(sValue, "RasterType") == 0)
        pEleMeta->type = EGF_ELE_META_TYPE_rasterType;
    else if (strcmp(sValue, "RasterStruct") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_rasterStruct;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "JpegStruct") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_jpegStruct;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "points") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_points;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "intArray") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_intArray;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "shortArray") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_shortArray;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "floatArray") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_floatArray;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "color3Array") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_color3Array;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "color4Array") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_color3Array;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;
    }
    else if (strcmp(sValue, "string") == 0)
    {
        pEleMeta->type = EGF_ELE_META_TYPE_string;
        if (!LoadNumId(pEleMeta, xmlEgfMetaEle))
            return false;

        sValue = xmlEgfMetaEle->Attribute("displayHex");
        if (sValue != NULL && strcmp(sValue, "yes") == 0)
        {
            pEleMeta->displayHex = true;
            if (pMeta) pMeta->hasDisplayHex = true;
        }
        else
            pEleMeta->displayHex = false;

    }
    else
    {
        SetError(L"Element type is not correct!");
        return false;
    }
    return true;
}


EGF2UV_API bool _LoadXmlConfig(const char *filePath)
{
    if (filePath == nullptr)
    {
        SetError(L"no xml file is provided!");
        return false;
    }

    if (gMetaSeqToIndexMap == nullptr)
        gMetaSeqToIndexMap = NEW std::map<int, int>;
    else
        gMetaSeqToIndexMap->clear();

    if (gEgfIdToIndexMap == nullptr)
        gEgfIdToIndexMap = NEW std::map<int, int>;
    else
        gEgfIdToIndexMap->clear();

    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLError err = xmlDoc.LoadFile(filePath);
    if (err != tinyxml2::XML_SUCCESS)
    {
        SetError(L"Load config file failed!");
        return false;
    }

    tinyxml2::XMLElement* xmlSupportedEgfObj = xmlDoc.FirstChildElement()->FirstChildElement("GlobalArgs")->FirstChildElement("SupportedEgfObj");
    int totalEgfObjs = 0;
    int searchEgfObjs = 0;
    const char* sValue = xmlSupportedEgfObj->Attribute("TotalNumber");
    if (sValue != NULL) totalEgfObjs = std::stoi(sValue, 0, 10);
    sValue = xmlSupportedEgfObj->Attribute("SearchNumber");
    if (sValue != NULL) searchEgfObjs = std::stoi(sValue, 0, 10);
    sValue = xmlSupportedEgfObj->Attribute("stylesStartId");

    // SupportedEgfObj
    tinyxml2::XMLElement* xmlEgfObj = xmlSupportedEgfObj->FirstChildElement("EgfObj");
    while (xmlEgfObj)
    {
        CEgfMeta *pMeta = NEW CEgfMeta();
        sValue = xmlEgfObj->Attribute("sequence");
        if (sValue != NULL) pMeta->sequenceId = std::stoi(sValue, 0, 10);
        else pMeta->sequenceId = -1;
        pMeta->index = (int)gEgfMetaList.size();
        gMetaSeqToIndexMap->insert(std::pair<int, int>(pMeta->sequenceId, pMeta->index));

        sValue = xmlEgfObj->Attribute("kids");
        if (sValue != NULL)
        {
            if (strcmp(sValue, "no") == 0)
                pMeta->kidsMode = KIDS_MODE_NONE;
            else if (strcmp(sValue, "group") == 0)
                pMeta->kidsMode = KIDS_MODE_Group;
            else if (strcmp(sValue, "path") == 0)
                pMeta->kidsMode = KIDS_MODE_Path;
            else if (strcmp(sValue, "stext") == 0)
                pMeta->kidsMode = KIDS_MODE_SText;
            else if (strcmp(sValue, "ext") == 0)
                pMeta->kidsMode = KIDS_MODE_Ext;
            else if (strcmp(sValue, "styles") == 0)
                pMeta->kidsMode = KIDS_MODE_Styles;
        }
        else pMeta->kidsMode = KIDS_MODE_NONE;

        sValue = xmlEgfObj->Attribute("id");
        if (sValue != NULL)
        {
            std::string cs(sValue);
            size_t pos = cs.find(',');
            if (pos == std::string::npos)
            {
                pMeta->egfId = std::stoi(sValue, 0, 16);
                gEgfIdToIndexMap->insert(std::pair<int, int>(pMeta->egfId, pMeta->index));
            }
            else
            {
                std::string one = cs.substr(0, pos);
                pMeta->egfId = std::stoi(sValue, 0, 16);
                gEgfIdToIndexMap->insert(std::pair<int, int>(pMeta->egfId, pMeta->index));
                one = cs.substr(pos + 1, cs.length() - pos - 1);
                int idd = std::stoi(one.c_str(), 0, 16);
                gEgfIdToIndexMap->insert(std::pair<int, int>(idd, pMeta->index));
            }
        }
        else pMeta->egfId = 0;

        sValue = xmlEgfObj->Attribute("name");
        if (sValue != NULL) pMeta->name = UV_Char2Wchar(sValue, false);

        sValue = xmlEgfObj->Attribute("hasOffset");
        if (sValue != NULL && strcmp(sValue, "yes") == 0)
            pMeta->hasOffset = true;
        else
            pMeta->hasOffset = false;

        gEgfMetaList.push_back(pMeta);

        xmlEgfObj = xmlEgfObj->NextSiblingElement("EgfObj");
    }

    // Egf Object meta
    tinyxml2::XMLElement* xmlEgfObjList = xmlDoc.FirstChildElement()->FirstChildElement("EgfObjList");
    xmlEgfObj = xmlEgfObjList->FirstChildElement("EgfObj");
    while (xmlEgfObj)
    {
        sValue = xmlEgfObj->Attribute("sequence");
        if (sValue != NULL)
        {
            int seq = std::stoi(sValue, 0, 10);
            std::map<int, int>::iterator it;
            it = gMetaSeqToIndexMap->find(seq);
            if (it == gMetaSeqToIndexMap->end())
            {
                SetError(L"Parse xml EgfObjList failed!");
                return false;
            }

            CEgfMeta *pMeta = gEgfMetaList[it->second];

            tinyxml2::XMLElement* xmlEgfMetaEle = xmlEgfObj->FirstChildElement("Ele");
            while (xmlEgfMetaEle)
            {
                CEgfEleMeta *pEleMeta = NEW CEgfEleMeta();

                sValue = xmlEgfMetaEle->Attribute("id");
                if (sValue != NULL) pEleMeta->id = std::stoi(sValue, 0, 10);
                else pEleMeta->id = -1;

                sValue = xmlEgfMetaEle->Attribute("name");
                if (sValue != NULL) pEleMeta->name = UV_Char2Wchar(sValue, false);

                sValue = xmlEgfMetaEle->Attribute("displayOrder");
                if (sValue != NULL)
                {
                    int n = std::stoi(sValue, 0, 10);
                    std::map<int, int>::iterator it = pMeta->displayOrderMap.find(n);
                    if (it != pMeta->displayOrderMap.end())
                        pMeta->displayOrderMap.insert(std::pair<int, int>((int)pMeta->displayOrderMap.size(), pEleMeta->id));
                    else
                        pMeta->displayOrderMap.insert(std::pair<int, int>(n, pEleMeta->id));
                }

                // get value from type
                if (GetValueByType(xmlEgfMetaEle, pMeta, pEleMeta, "type") == false)
                    return false;

                sValue = xmlEgfMetaEle->Attribute("draw");
                if (sValue == NULL)
                    pEleMeta->drawType = DRAW_TYPE_None;
                else
                {
                    size_t pos;
                    std::string cs(sValue);
                    switch (pEleMeta->type)
                    {
                    case EGF_ELE_META_TYPE_point:
                        pos = cs.find(',');
                        if (pos == std::string::npos)
                            pEleMeta->drawType = DRAW_TYPE_point;
                        else
                        {
                            pEleMeta->drawType = DRAW_TYPE_line;
                            pEleMeta->drawArgs[0] = pEleMeta->id;
                            pEleMeta->drawArgs[1] = (short)std::stoi(cs.substr(pos + 1, cs.length() - pos - 1).c_str(), 0, 10);
                        }
                        break;
                    case EGF_ELE_META_TYPE_points:
                        pEleMeta->drawArgs[1] = -1;
                        pos = cs.find(',');
                        if (pos == std::string::npos)
                            pEleMeta->drawType = DRAW_TYPE_lines;
                        else
                        {
                            pEleMeta->drawType = DRAW_TYPE_Polylines;
                            pEleMeta->drawArgs[0] = (short)std::stoi(cs.substr(0, pos).c_str(), 0, 10);
                            pEleMeta->drawArgs[1] = (short)std::stoi(cs.substr(pos + 1, cs.length() - pos - 1).c_str(), 0, 10);
                        }
                        break;
                    case EGF_ELE_META_TYPE_mer:
                        pEleMeta->drawType = DRAW_TYPE_mer;
                        break;
                    default: // draw point on (0, 0)
                        pEleMeta->drawType = DRAW_TYPE_point;
                        break;
                    }
                }

                pMeta->elements.push_back(pEleMeta);

                xmlEgfMetaEle = xmlEgfMetaEle->NextSiblingElement("Ele");
            }
        }
        else
        {
            SetError(L"Parse xml EgfObjList failed!");
            return false;
        }


        xmlEgfObj = xmlEgfObj->NextSiblingElement("EgfObj");
    }

    return true;
}

EGF2UV_API bool _LoadXmlSearchConfig(const char *filePath)
{
    if (filePath == nullptr)
    {
        SetError(L"no xml file is provided!");
        return false;
    }

    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLError err = xmlDoc.LoadFile(filePath);
    if (err != tinyxml2::XML_SUCCESS)
    {
        SetError(L"Load search config file failed!");
        return false;
    }

    tinyxml2::XMLElement* xmlSearchArgs = xmlDoc.FirstChildElement()->FirstChildElement("SearchArgs");

    int start = 10001;
    const char* sValue = xmlSearchArgs->Attribute("start");
    if (sValue != NULL) start = std::stoi(sValue, 0, 10);

    tinyxml2::XMLElement* xmlSearchObj = xmlSearchArgs->FirstChildElement("SearchObj");
    while (xmlSearchObj)
    {
    //static int count = 0;
    //count++;

        CEgfSearchMeta *pMeta = NEW CEgfSearchMeta();

        int sss = sizeof(CEgfSearchMeta);

        sValue = xmlSearchObj->Attribute("sequence");
        if (sValue != NULL) pMeta->id = std::stoi(sValue, 0, 10);
        else
        {
            delete pMeta;
            SetError(L"Load search config file failed!");
            return false;
        }

        tinyxml2::XMLElement* xmlEle = xmlSearchObj->FirstChildElement("Ele");
        while (xmlEle)
        {
            CEgfSearchMetaEle *pMetaEle = NEW CEgfSearchMetaEle();
            sss = sizeof(CEgfSearchMetaEle);
            sValue = xmlEle->Attribute("id");
            pMetaEle->id = std::stoi(sValue, 0, 10);
            pMetaEle->ctrlId = start + pMeta->id * 200 + pMetaEle->id * 5;

            sValue = xmlEle->Attribute("name");
            if (sValue != NULL)
                pMetaEle->name = UV_Char2Wchar(sValue, false);

            pMeta->elements.push_back(pMetaEle);

            xmlEle = xmlEle->NextSiblingElement("Ele");
        }

        //if(count ==10)
        //    new char[3];
        gEgfSearchMetaList.push_back(pMeta);
        //if (count == 10)
        //    new char[4];

        xmlSearchObj = xmlSearchObj->NextSiblingElement("SearchObj");
    }

    return true;
}

EGF2UV_API CUvDoc* CreateUvDocFromEgfFile(const wchar_t *fileName)
{
    return OpenDoc(fileName);

}
EGF2UV_API CUvPage* CreateUvPageFromEgfObj(CUvDoc *pDoc, int pageNo)
{
    return GetPage(pDoc, pageNo);
}
EGF2UV_API void CloseUvDoc(CUvDoc *pDoc)
{
    CloseDoc(pDoc);
}
EGF2UV_API CEgfFile* GetEgfFile(CUvDoc *pDoc)
{
    return ((CEgf2UvFile*)pDoc->pLoaderInfo)->pEgfFile;
}

EGF2UV_API bool ReleaseGlobalVarialbes()
{
    for (int i = 0; i < (int)gEgfMetaList.size(); i++)
        delete gEgfMetaList[i];
    gEgfMetaList.clear();
    gMetaSeqToIndexMap->clear();
    delete gMetaSeqToIndexMap;

    for (int i = 0; i < (int)gEgfSearchMetaList.size(); i++)
        delete gEgfSearchMetaList[i];
    gEgfSearchMetaList.clear();

    gEgfIdToIndexMap->clear();
    delete gEgfIdToIndexMap;

    return true;
}

EGF2UV_API CEgfMetaList* GetEgfMetaList()
{
    return &gEgfMetaList;
}

EGF2UV_API void* GetMetaSeqToIndexMap()
{
    return gMetaSeqToIndexMap;
}

EGF2UV_API void* GetEgfIdToIndexMap()
{
    return gEgfIdToIndexMap;
}

EGF2UV_API CEgfSearchMetaList* GetEgfSearchMetaList()
{
    return &gEgfSearchMetaList;
}

