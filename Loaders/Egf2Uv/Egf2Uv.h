#pragma once
#include "CUvDoc.h"
#include "CUvPage.h"

#define EGF2UV_API __declspec(dllexport)

void SetError(const wchar_t *msg);

extern "C"
{
    EGF2UV_API bool LoadXmlConfig(const char *filePath);
    EGF2UV_API bool LoadXmlSearchConfig(const char *filePath);
    EGF2UV_API void CloseLoader(const char *filePath);


    EGF2UV_API bool Recognizer(const wchar_t *fileName);
    EGF2UV_API bool RecognizerBuffer(unsigned char *pData, int datalen);

    EGF2UV_API CUvDoc* OpenDoc(const wchar_t *fileName);
    EGF2UV_API CUvDoc* OpenDoc(const wchar_t *fileName);

    EGF2UV_API CUvPage* GetPage(CUvDoc *pDoc, int pageNo);

    EGF2UV_API void CloseDoc(CUvDoc *pDoc);

    EGF2UV_API wchar_t* GetLoaderLastError();

};

