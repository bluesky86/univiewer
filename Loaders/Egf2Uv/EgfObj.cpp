#include "stdafx.h"
#include "EgfObj.h"
#include <string>
#include "DebugNew.h"
#include "Map.h"

CEgfMetaList gEgfMetaList;
std::map<int, int> *gMetaSeqToIndexMap = nullptr;
std::map<int, int> *gEgfIdToIndexMap = nullptr;
CEgfSearchMetaList gEgfSearchMetaList;

void SetError(const wchar_t *msg);
void ClearError();

CEgfObj::CEgfObj()
    : mObjType(0), mObjDataType(0), mObjSize(0), hasOffset(false), mParent(0), metaIndex(0), mSequenceId(0), pUvObj(0)
{
}

CEgfObj::CEgfObj(int objType, int objDataType, int objSize)
    : mObjType(objType), mObjDataType(objDataType), mObjSize(objSize), hasOffset(false), mParent(0), mSequenceId(0), pUvObj(0)
{
    metaIndex = (*gEgfIdToIndexMap)[objType];
}

CEgfObj::~CEgfObj()
{
    for (unsigned int i = 0; i < attr.size(); i++)
        delete attr[i];

    attr.clear();

    for (unsigned int i = 0; i < children.size(); i++)
        delete children[i];

    children.clear();
}

int CEgfObj::M16_ToInt(char *data)
{
    char outdata[2];
    outdata[0] = *(data + 1);
    outdata[1] = *(data);

    return (int)(*(short*)outdata);
}

int CEgfObj::M32_ToInt(char *data)
{
    char outdata[4];
    outdata[0] = *(data + 3);
    outdata[1] = *(data + 2);
    outdata[2] = *(data + 1);
    outdata[3] = *(data);

    return *(int*)outdata;
}

int CEgfObj::Egf32_ToInt(char *data)
{
    return *(int*)data;
}

bool CEgfObj::ReadObjectFlags(std::ifstream &ifs, bool &visible, bool &mask)
{
    char buf;
    if (!ReadByte(ifs, buf)) return false;
    visible = buf > 0;
    if (!ReadByte(ifs, buf)) return false;
    mask = buf > 0;
    return true;
}

bool CEgfObj::Read16BitArray(std::ifstream &ifs, int count, short *value)
{
    char buf[2];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 2);
        if (ifs.gcount() != 2)
            return false;
        value[i] = *(short*)buf;
    }

    return true;
}

bool CEgfObj::Read32BitArray(std::ifstream &ifs, int count, int *value)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        value[i] = *(int*)buf;
    }

    return true;
}

bool CEgfObj::ReadFloatArray(std::ifstream &ifs, int count, float *value)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        value[i] = (*(int*)buf) / 10000.0f;
    }

    return true;

}

bool CEgfObj::ReadMer(std::ifstream &ifs, CEgfMer &mer)
{
    int value[4];
    if (!Read32BitArray(ifs, 4, value)) return false;
    mer.x1 = (float)value[0];
    mer.y1 = (float)value[1];
    mer.x2 = (float)value[2];
    mer.y2 = (float)value[3];

    return true;
}

bool CEgfObj::ReadPoints(std::ifstream &ifs, int count, CEgfPoint *pts)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].x = (float)*(int*)buf;
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].y = (float)*(int*)buf;
    }
    return true;
}

bool CEgfObj::ReadByte(std::ifstream &ifs, char &value)
{
    char buf[1];
    ifs.read(buf, 1);
    if (ifs.gcount() != 1)
        return false;
    value = (BYTE)buf[0];
    return true;
}

bool CEgfObj::ReadShort(std::ifstream &ifs, short &value)
{
    char buf[2];
    ifs.read(buf, 2);
    if (ifs.gcount() != 2)
        return false;
    value = *(short*)buf;
    return true;
}

bool CEgfObj::ReadInt(std::ifstream &ifs, int &value)
{
    char buf[4];
    ifs.read(buf, 4);
    if (ifs.gcount() != 4)
        return false;
    value = *(int*)buf;
    return true;
}

bool CEgfObj::ReadFloat(std::ifstream &ifs, float &value)
{
    char buf[4];
    ifs.read(buf, 4);
    if (ifs.gcount() != 4)
        return false;
    value = (*(int*)buf) / 10000.0f;;
    return true;
}

bool CEgfObj::ReadString(std::ifstream &ifs, int count, char *buf)
{
    ifs.read(buf, count);
    if (ifs.gcount() != count)
        return false;
    return true;
}

bool CEgfObj::LoadObjHdr(std::ifstream &ifs, int &objType, int &objDataType, int &objSize)
{
    // offset (100) - type
    if (!ReadInt(ifs, objType)) return false;

    // objDataType
    if (!ReadInt(ifs, objDataType)) return false;

    // Object size
    if (!ReadInt(ifs, objSize)) return false;

    return true;
}

bool CEgfObj::CheckCondition(CSearchEle *pCondition, void *pValue)
{
    if (pCondition == NULL) return false;

    switch (pCondition->valueType)
    {
    case EGFSVT_bool:
        if (pCondition->b == (*(bool*)pValue))
            return true;
        break;
    case EGFSVT_int:
        if (pCondition->i == (*(int*)pValue))
            return true;
        break;
    case EGFSVT_uint:
        if (pCondition->u == (*(unsigned int*)pValue))
            return true;
        break;
    case EGFSVT_float:
        if (pCondition->f == (*(float*)pValue))
            return true;
        break;
    case EGFSVT_string:
        if (std::wstring(pCondition->s).compare(std::wstring(((wchar_t*)pValue))) == 0)
            return true;
        break;
    default:
        return false;
    }


    return false;
}

bool CEgfObj::CheckColorCondition(CSearchEle *pCondition, char *pValue)
{
    if (pCondition->valueType != EGFSVT_uint) return false;

    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000) >> 16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF));
}

bool CEgfObj::CheckRGBAColorCondition(CSearchEle *pCondition, short *pValue)
{
    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000) >> 16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF)
        && (BYTE)pValue[3] == ((pCondition->u & 0xFF000000) >> 24));
}

CEgfObj* CEgfObj::CreateEgfObj(int objType, int objDataType, int objSize)
{
    int newObjType;

    switch (objDataType)
    {
    case SDT_Styles_GdiplusGradientParmsTable:
    case SDT_Styles_GdiplusStrokeParmsTable:
    case SDT_Styles_GdiplusArcParmsTable:
    case SDT_Styles_GdiplusFillParmsTable:
    case SDT_Styles_GdiFillParmsTable:
    case SDT_Styles_GdiStrokeParmsTable:
    case SDT_Styles_GdiEllipseParmsTable:
    case SDT_Styles_TextParmsTable:
    case SDT_Styles_EmbeddedFontList:
    case SDT_Styles_DashPatterns:
    case SDT_Styles_HatchPatterns:
    case SDT_Styles_RasterFillTable:
    case SDT_Styles_EmbeddedTrueTypeFontTable:
        newObjType = objDataType;
        break;
    default: // default to group object
        newObjType = objType;
        break;
    }

    CEgfObj *pObj = NEW CEgfObj(newObjType, objDataType, objSize);

    return pObj;
}

CEgfAttr* CEgfObj::FindAttr(wchar_t *name, EGF_ELE_META_TYPE type)
{
    if (name == NULL && type == EGF_ELE_META_TYPE_none)
        return NULL;

    CEgfMeta *pMeta = GetMeta();
    if (name)
    {
        if (type != EGF_ELE_META_TYPE_none)
        {
            for (unsigned int i = 0; i < pMeta->elements.size(); i++)
            {
                if (type == pMeta->elements[i]->type && wcscmp(name, pMeta->elements[i]->name) == 0)
                    return attr[i];
            }
        }
        else
        {
            for (unsigned int i = 0; i < pMeta->elements.size(); i++)
            {
                if (wcscmp(name, pMeta->elements[i]->name) == 0)
                    return attr[i];
            }
        }

    }
    else
    {
        for (unsigned int i = 0; i < pMeta->elements.size(); i++)
        {
            if (type == pMeta->elements[i]->type)
                return attr[i];
        }
    }


    return NULL;
}

CEgfMeta* CEgfObj::GetMeta()
{
    return gEgfMetaList.size() > 0 && metaIndex < (int)gEgfMetaList.size() ? gEgfMetaList[metaIndex] : nullptr;
}

std::map<int, int>* CEgfObj::GetAttrDisplayOrderMap()
{
    return &GetMeta()->displayOrderMap;
}

wchar_t* CEgfObj::GetAttrName(int column)
{
    return GetMeta()->elements[column]->name;
}

EGF_ELE_META_TYPE CEgfObj::GetAttrType(int column)
{
    return GetMeta()->elements[column]->type;
}

bool CEgfObj::GetDisplayHex(int column)
{
    return GetMeta()->elements[column]->displayHex;
}

wchar_t* CEgfObj::GetMetaName()
{
    return GetMeta()->name;
}

int CEgfObj::GetAttrNumOfEles(int column)
{
    CEgfMeta *pMeta = GetMeta();
    if (pMeta->elements[column]->num_id != -1)
        return attr[pMeta->elements[column]->num_id]->i;

    return pMeta->elements[column]->num;
}

KIDS_MODE CEgfObj::GetKidsMode()
{
    return GetMeta()->kidsMode;
}

bool CEgfObj::GetHasOffset()
{
    return GetMeta()->hasOffset;
}

void CEgfObj::PushChild(CEgfObj* pObj)
{
    if (pObj)
    {
        children.push_back(pObj);
        pObj->mParent = this;
    }
}

CEgfObj* CEgfObj::GetChild(int index)
{
    if ((int)children.size() > index && index >= 0)
        return children[index];
    else return nullptr;
}

bool CEgfObj::ParseArgs(std::ifstream &ifs)
{
    char buf[4];
    CEgfMeta *pMeta = GetMeta();
    for (unsigned int i = 0; i < pMeta->elements.size(); i++)
    {
        size_t offset = ifs.tellg();

        CEgfAttr *pAttr = NEW CEgfAttr();
        pAttr->type = pMeta->elements[i]->type;
        switch (pMeta->elements[i]->type)
        {
        case EGF_ELE_META_TYPE_8bool:
            ReadByte(ifs, buf[0]);
            pAttr->b = (buf[0] != 0);
            break;
        case EGF_ELE_META_TYPE_byte:
            ReadByte(ifs, buf[0]);
            pAttr->uc = (unsigned char)buf[0];
            break;
        case EGF_ELE_META_TYPE_intBool:
            ifs.read(buf, 4);
            pAttr->b = (*(int*)buf != 0);
            break;
        case EGF_ELE_META_TYPE_int:
        case EGF_ELE_META_TYPE_lineStyle:
        case EGF_ELE_META_TYPE_capStyle:
        case EGF_ELE_META_TYPE_operation:
        case EGF_ELE_META_TYPE_fillStyle:
        case EGF_ELE_META_TYPE_hatchStyle:
        case EGF_ELE_META_TYPE_shadingStyle:
        case EGF_ELE_META_TYPE_joinStyle:
        case EGF_ELE_META_TYPE_curveStyle:
        case EGF_ELE_META_TYPE_effectType:
        case EGF_ELE_META_TYPE_rasterType:
            ifs.read(buf, 4);
            pAttr->i = *(int*)buf;
            break;
        case EGF_ELE_META_TYPE_rasterStruct:
            {
                pAttr->pRastStruct = NEW RSL_RAST_DATA;
                int num = GetAttrNumOfEles(i);
                if(num > 0)
                    ifs.read((char*)pAttr->pRastStruct, num);
            }
            break;
        case EGF_ELE_META_TYPE_jpegStruct:
            {
                pAttr->pJpegStruct = NEW JFIF_PARM;
                int num = GetAttrNumOfEles(i);
                if(num > 0)
                    ifs.read((char*)pAttr->pJpegStruct, num);
            }
            break;
        case EGF_ELE_META_TYPE_float:
        case EGF_ELE_META_TYPE_double:
            ReadFloat(ifs, pAttr->f);
            break;
        case EGF_ELE_META_TYPE_matrix:
            pAttr->matrix = NEW float[6];
            ReadFloat(ifs, pAttr->matrix[0]);
            ReadFloat(ifs, pAttr->matrix[1]);
            ReadFloat(ifs, pAttr->matrix[2]);
            ReadFloat(ifs, pAttr->matrix[3]);
            ReadFloat(ifs, pAttr->matrix[4]);
            ReadFloat(ifs, pAttr->matrix[5]);
            break;
        case EGF_ELE_META_TYPE_color3:
            ifs.read((char*)&pAttr->i, 3);
            ((unsigned char*)&pAttr->i)[3] = 0xFF;
            break;
        case EGF_ELE_META_TYPE_color4:
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[0] = (unsigned char)*(short*)buf;
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[1] = (unsigned char)*(short*)buf;
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[2] = (unsigned char)*(short*)buf;
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[3] = (unsigned char)*(short*)buf;
            break;
        case EGF_ELE_META_TYPE_point:
            pAttr->point = NEW CEgfPoint;
            ReadPoints(ifs, 1, pAttr->point);
            break;
        case EGF_ELE_META_TYPE_points:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->points = NEW CEgfPointArray;
                    for (int j = 0; j < num; j++)
                    {
                        CEgfPoint *pPoint = NEW CEgfPoint;
                        ReadPoints(ifs, 1, pPoint);
                        pAttr->points->points.push_back(pPoint);
                    }
                }
            }
            break;
        case EGF_ELE_META_TYPE_mer:
            pAttr->mer = NEW CEgfMer;
            ReadMer(ifs, *pAttr->mer);
            break;
        case EGF_ELE_META_TYPE_intArray:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
					pAttr->ia = NEW int[num];
                    Read32BitArray(ifs, GetAttrNumOfEles(i), pAttr->ia);
                }
            }
            break;
		case EGF_ELE_META_TYPE_shortArray:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
					pAttr->sa = NEW short[num];
					Read16BitArray(ifs, GetAttrNumOfEles(i), pAttr->sa);
                }
            }
            break;
        case EGF_ELE_META_TYPE_floatArray:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->fa = NEW float[num];
                    for(int i = 0; i<num; i++)
                        ReadFloat(ifs, pAttr->fa[i]);
                }
            }
            break;
        case EGF_ELE_META_TYPE_color3Array:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->ia = NEW int[num];
                    for (int ia = 0; ia < num; ia++)
                    {
                        ifs.read((char*)&pAttr->ia[ia], 3);
                        ((unsigned char*)&pAttr->ia[ia])[3] = 0xFF;
                    }
                }
            }
            break;
        case EGF_ELE_META_TYPE_color4Array:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->ia = NEW int[num];
                    for (int ia = 0; ia < num; ia++)
                    {
                        ifs.read(buf, 2);
                        ((unsigned char*)&pAttr->ia[ia])[0] = (unsigned char)*(short*)buf;
                        ifs.read(buf, 2);
                        ((unsigned char*)&pAttr->ia[ia])[1] = (unsigned char)*(short*)buf;
                        ifs.read(buf, 2);
                        ((unsigned char*)&pAttr->ia[ia])[2] = (unsigned char)*(short*)buf;
                        ifs.read(buf, 2);
                        ((unsigned char*)&pAttr->ia[ia])[3] = (unsigned char)*(short*)buf;
                    }
                }
            }
            break;
        case EGF_ELE_META_TYPE_string:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->s = NEW char[num + 2];
                    ifs.read(pAttr->s, num);
                    pAttr->s[num] = pAttr->s[num + 1] = 0;
                }
            }
            break;
        case EGF_ELE_META_TYPE_none:
        default:
            SetError(L"Unknown data type when paring file");
#ifdef _DEBUG
            //NEW char[1]; // debug
#endif
            return false;
        }

        attr.push_back(pAttr);
    }

    return true;
}

bool CEgfObj::ParseChildren(std::ifstream &ifs, CEgfObj* pParent)
{
    int objType = 0, objDataType = 0, objSize = 0;
    int sequenceId = 0;
    bool childrenStart = false;

    // parse children
    while (true)
    {
        if (!LoadObjHdr(ifs, objType, objDataType, objSize))
            break;

        if (childrenStart == false && objDataType != SDT_OBJ_CHILDREN_START)
            return false;
        if (childrenStart == false && objDataType == SDT_OBJ_CHILDREN_START)
        {
            childrenStart = true;
            continue;
        }
        if (childrenStart == true && objDataType == SDT_OBJ_CHILDREN_END)
            break;

        CEgfObj *pObj = CreateEgfObj(objType, objDataType, objSize);
        if (!pObj->Parse(ifs, sequenceId++, objDataType))
        {
            PushChild(pObj);
            return false;
        }

        PushChild(pObj);
    }

    return true;
}

bool CEgfObj::Parse(std::ifstream &ifs, int sequenceId, int _objDataType)
{
    int objType = 0, objDataType = 0, objSize = 0;

    mSequenceId = sequenceId;

    // parse children
    switch (GetKidsMode())
    {
    case KIDS_MODE_Group:
        if (_objDataType != SDT_OBJ_ARGS)
        {
            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;
        }

        if (!ParseArgs(ifs))
            return false;

        return ParseChildren(ifs, this);

    case KIDS_MODE_Path:
        if (!ParseArgs(ifs))
            return false;

        return ParseChildren(ifs, this);

    case KIDS_MODE_SText:
        {
            if (!ParseArgs(ifs))
                return false;

            if (GetHasOffset())
            {
                mOffset.x = attr[attr.size() - 1]->point->x;
                mOffset.y = attr[attr.size() - 1]->point->y;
            }
            if (mObjSize == 50)
                return true;

            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;

            CEgfObj *pObj = CreateEgfObj(objType, objDataType, objSize);
            if (!pObj->Parse(ifs, 0, objDataType))
            {
                PushChild(pObj);
                return false;
            }

            PushChild(pObj);
            return true;
    }
    case KIDS_MODE_Ext:
        if (mObjSize > 0)
        {
            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;

            CEgfObj *pObj = CreateEgfObj(objType, objDataType, objSize);
            if (!pObj->Parse(ifs, 0, objDataType))
            {
                PushChild(pObj);
                return false;
            }

            PushChild(pObj);

            if (objSize < mObjSize - 12) // 12 is header size
            {
                if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                    return false;

                CEgfObj *pObj1 = CreateEgfObj(objType, objDataType, objSize);
                if (!pObj1->Parse(ifs, 1, objDataType))
                {
                    PushChild(pObj1);
                    return false;
                }

                PushChild(pObj1);
            }

            return true;
        }
        break;
    case KIDS_MODE_Styles:
        {
            int styleCount = 0;
            int index, valid;
            switch (_objDataType)
            {
            case SDT_Styles_GdiplusGradientParmsTable:
            case SDT_Styles_GdiplusStrokeParmsTable:
            case SDT_Styles_GdiplusArcParmsTable:
            case SDT_Styles_GdiplusFillParmsTable:
            case SDT_Styles_GdiFillParmsTable:
            case SDT_Styles_GdiStrokeParmsTable:
            case SDT_Styles_GdiEllipseParmsTable:
            case SDT_Styles_TextParmsTable:
            case SDT_Styles_EmbeddedFontList:
            case SDT_Styles_DashPatterns:
            case SDT_Styles_HatchPatterns:
            case SDT_Styles_RasterFillTable:
            case SDT_Styles_EmbeddedTrueTypeFontTable:
                index = (int)ifs.tellg();
                if (!ReadInt(ifs, styleCount)) break;
                for (int i = 0; i < styleCount; i++)
                {
                    if (!ReadInt(ifs, index)) break;
                    valid = 0;
                    CEgfObj *pObj = CreateEgfObj(SDT_OBJ_NONE, _objDataType, 0);
                    if(ReadInt( ifs, valid ) && valid)
                    {
                        pObj->ParseArgs( ifs );
                        PushChild( pObj );
                    }
                    else
                        delete pObj;
                }
                break;
            default:
                return ParseArgs(ifs);
            }
        }
        break;
    case KIDS_MODE_NONE:
    default:
        return ParseArgs(ifs);
    }

    return true;
}

void CEgfObj::Search(int objMetaId, CSearchCriteria *pPairs, CVector<CEgfObj*> *pList)
{
    bool met = true;

    CEgfMeta *pMeta = GetMeta();

    if (pMeta->index == objMetaId)
    {
        for (unsigned int i = 0; i < pPairs->pairs.size(); i++)
        {
            int column = pPairs->pairs[i]->argId;
            switch (pMeta->elements[column]->type)
            {
            case EGF_ELE_META_TYPE_8bool:
            case EGF_ELE_META_TYPE_intBool:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->b);
                break;
            case EGF_ELE_META_TYPE_int:
            case EGF_ELE_META_TYPE_lineStyle:
            case EGF_ELE_META_TYPE_capStyle:
            case EGF_ELE_META_TYPE_operation:
            case EGF_ELE_META_TYPE_fillStyle:
            case EGF_ELE_META_TYPE_hatchStyle:
            case EGF_ELE_META_TYPE_shadingStyle:
            case EGF_ELE_META_TYPE_joinStyle:
            case EGF_ELE_META_TYPE_curveStyle:
            case EGF_ELE_META_TYPE_effectType:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->i);
                break;
            case EGF_ELE_META_TYPE_color3:
                met = CheckColorCondition(pPairs->pairs[i], (char*)&attr[column]->i);
                break;
            case EGF_ELE_META_TYPE_color4:
                {
                    short value[4];
                    value[0] = (attr[column]->i >> 24);
                    value[1] = (attr[column]->i & 0xFF0000) >> 16;
                    value[2] = (attr[column]->i & 0xFF00) >> 8;
                    value[3] = (attr[column]->i & 0xFF);
                    met = CheckRGBAColorCondition(pPairs->pairs[i], value);
                }
                break;
            case EGF_ELE_META_TYPE_point:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->point->x);
                break;
            case EGF_ELE_META_TYPE_point + 1:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->point->y);
                break;
            case EGF_ELE_META_TYPE_mer:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->x1);
                break;
            case EGF_ELE_META_TYPE_mer + 1:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->y1);
                break;
            case EGF_ELE_META_TYPE_mer + 2:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->x2);
                break;
            case EGF_ELE_META_TYPE_mer + 3:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->y2);
                break;
            case EGF_ELE_META_TYPE_string:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->s);
                break;
            case EGF_ELE_META_TYPE_intArray:
			case EGF_ELE_META_TYPE_shortArray:
			default:
                break;
            }

            if (met == false)
                break;
        }

        if (met)
            pList->push_back(this);
    }

    // check children
    for (unsigned int i = 0; i < children.size(); i++)
        children[i]->Search(objMetaId, pPairs, pList);
}

bool IsVisible(CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr((wchar_t*)L"Visible");
    if (pEgfAttr && pEgfAttr->b == false) // invisible
        return false;

    return true;
}

bool hasMask(CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr((wchar_t*)L"HasMask");
    if (pEgfAttr && pEgfAttr->b == false) // no mask within group
        return false;

    return true;
}

bool IsMask(CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr((wchar_t*)L"Mask");
    if (pEgfAttr && pEgfAttr->b == false) // not mask
        return false;

    return true;
}

unsigned char* GetAttrByteValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return &pEgfAttr->uc;

    return nullptr;
}
char* GetAttrStringValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return pEgfAttr->s;

    return nullptr;
}
int* GetAttrIntValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return &pEgfAttr->i;

    return nullptr;
}
float* GetAttrFloatValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return &pEgfAttr->f;

    return nullptr;
}
bool* GetAttrBoolValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return &pEgfAttr->b;

    return nullptr;
}
int* GetAttrIntArray(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return pEgfAttr->ia;

    return nullptr;
}
short* GetAttrShortArray(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return pEgfAttr->sa;

    return nullptr;
}
float* GetAttrFloatArray(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return pEgfAttr->fa;

    return nullptr;
}
CEgfPoint* GetAttrPoint(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return pEgfAttr->point;

    return nullptr;
}
CEgfPointArray* GetAttrPointArray(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return pEgfAttr->points;

    return nullptr;
}
CEgfMer* GetAttrMer(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
        return pEgfAttr->mer;

    return nullptr;
}


#define MPL_ISCUSTOM(unit) ((unit & IM_UNITS_UNITMASK)==IM_UNITS_CUSTOM1 || (unit & IM_UNITS_UNITMASK)==IM_UNITS_CUSTOM2 || (unit & IM_UNITS_UNITMASK)==IM_UNITS_CUSTOM3)

double TransMeas2Meas(int inUnits, double inValue, int outUnits)
{
    if (inUnits == outUnits) return inValue;

    double dpi = 200.0, dpcm = 200.0 / 2.54;
    double scaleFactor = 1.0;

    /*  Check if the in or out units is a percent.
        If so then set the out value to zero since percent means relative to something and is not absolute
        as are the others.  */
    if (inUnits == IM_UNITS_PERCENT || outUnits == IM_UNITS_PERCENT)
        return 0.0;

    /* if the input measure is CUSTOM, map it to native units first. */
    if (MPL_ISCUSTOM(inUnits))
        return inValue;

    if (inUnits == IM_UNITS_DEG || inUnits == IM_UNITS_RAD) {
        /* frst map to degrees, then map to output units. */
        switch (inUnits) {
        case IM_UNITS_DEG:                                break;
        case IM_UNITS_RAD: scaleFactor *= RAD2DEG;        break;
        }/*switch*/
        switch (outUnits) {
        case IM_UNITS_DEG:                                break;
        case IM_UNITS_RAD: scaleFactor *= DEG2RAD;        break;
        default: SetError(L"Out units is not match in units (angle)");  break;
        }/*switch*/
    }
    /* special handling for PELS based measurements - these require a map pointer. */
    else if ((outUnits == IM_UNITS_PELS || inUnits == IM_UNITS_PELS) && !(MPL_ISCUSTOM(outUnits) || MPL_ISCUSTOM(inUnits)))
    {
        /* map input units to pels, then to output units. */
        switch (inUnits) {
        case IM_UNITS_DISPLAYPIXELS: break;
        case IM_UNITS_PELS:                              break;
        case IM_UNITS_MM: scaleFactor *= dpcm / 10.0;    break;
        case IM_UNITS_CM: scaleFactor *= dpcm;         break;
        case IM_UNITS_M: scaleFactor *= dpcm * 100.0;   break;
        case IM_UNITS_INCH: scaleFactor *= dpi;          break;
        case IM_UNITS_FT: scaleFactor *= dpi * 12.0;     break;
        case IM_UNITS_TWIPS: scaleFactor *= dpi / 1440.0;   break;
        case IM_UNITS_POINTS: scaleFactor *= dpi / 72.0;     break;
        default: SetError(L"In units is not supported");     break;
        }/*switch*/
        switch (outUnits) {
        case IM_UNITS_DISPLAYPIXELS:                        break;
        case IM_UNITS_PELS:                                 break;
        case IM_UNITS_M: scaleFactor *= 1.0 / dpcm / 100.0;  break;
        case IM_UNITS_CM: scaleFactor *= 1.0 / dpcm;        break;
        case IM_UNITS_MM: scaleFactor *= 10.0 / dpcm;       break;
        case IM_UNITS_INCH: scaleFactor *= 1.0 / dpi;         break;
        case IM_UNITS_FT: scaleFactor *= 1.0 / dpi / 12.0;    break;
        case IM_UNITS_TWIPS: scaleFactor *= 1440.0 / dpi;      break;
        case IM_UNITS_POINTS: scaleFactor *= 72.0 / dpi;        break;
        default: SetError(L"Out units is not supported");     break;
        }/*switch*/
    }
    else if (!(MPL_ISCUSTOM(outUnits) || MPL_ISCUSTOM(inUnits))) {
        /* map input units to mm, then to output units. */
        switch (inUnits) {
        case IM_UNITS_MM:                             break;
        case IM_UNITS_CM: scaleFactor *= 10.0;        break;
        case IM_UNITS_M: scaleFactor *= 1000.0;      break;
        case IM_UNITS_INCH: scaleFactor *= 25.4;        break;
        case IM_UNITS_FT: scaleFactor *= 25.4*12.0;   break;
        case IM_UNITS_TWIPS: scaleFactor *= 25.4 / 1440.0; break;
        case IM_UNITS_POINTS: scaleFactor *= 25.4 / 72.0;   break;
        default: SetError(L"In units is not supported");     break;
        }/*switch*/
        switch (outUnits) {
        case IM_UNITS_MM:                                  break;
        case IM_UNITS_CM: scaleFactor *= 1.0 / 10.0;         break;
        case IM_UNITS_M: scaleFactor *= 1.0 / 1000.0;       break;
        case IM_UNITS_INCH: scaleFactor *= 1.0 / 25.4;         break;
        case IM_UNITS_FT: scaleFactor *= 1.0 / 25.4 / 12.0;    break;
        case IM_UNITS_TWIPS: scaleFactor *= 1.0 / 25.4*1440.0;  break;
        case IM_UNITS_POINTS: scaleFactor *= 1.0 / 25.4*72.0;    break;
        default: SetError(L"Out units is not supported");     break;
        }/*switch*/
    }

    return inValue * scaleFactor;
}

