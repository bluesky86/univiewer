#pragma once
#define EGF2UV_API __declspec(dllexport)

#include "EgfObj.h"
#include "CUvDoc.h"
#include "CUvPage.h"
#include "EgfFile.h"


// helpers
EGF2UV_API bool _LoadXmlConfig(const char *filePath);
EGF2UV_API bool _LoadXmlSearchConfig(const char *filePath);
EGF2UV_API bool ReleaseGlobalVarialbes();

EGF2UV_API CUvDoc* CreateUvDocFromEgfFile(const wchar_t *fileName); 
EGF2UV_API CUvPage* CreateUvPageFromEgfObj(CUvDoc *pDoc, int pageNo);
EGF2UV_API CEgfFile* GetEgfFile(CUvDoc *pDoc);
EGF2UV_API void CloseUvDoc(CUvDoc *pDoc);

// EgfMetaList
EGF2UV_API CEgfMetaList* GetEgfMetaList();

// MetaSeqToIndexMap
EGF2UV_API void* GetMetaSeqToIndexMap();

// EgfIdToIndexMap
EGF2UV_API void* GetEgfIdToIndexMap();

// EgfSearchMetaList
EGF2UV_API CEgfSearchMetaList* GetEgfSearchMetaList();

