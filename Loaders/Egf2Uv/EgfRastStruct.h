#pragma once
#include "CUvImage.h"

typedef unsigned char U8;
typedef int BOOL;
typedef unsigned short U16;
typedef short S16;
typedef int S32;
typedef int IM_BOOL;
typedef int RSL_IDF;
typedef double DOUBLE;
typedef int INT;
typedef wchar_t WCHAR; 
typedef unsigned int U32;
typedef U32 *U32PTR;

#ifndef PATHLEN
#define PATHLEN        1024
#endif

#ifndef FILENAMELEN
#define FILENAMELEN    1024
#endif


typedef struct {
    char    filename[FILENAMELEN + 1];
    short   vRefNum;
    char    path[PATHLEN + 1];
    int     bShareWriteNeeded; // if we need the share write perm to open then this is true and the file is still being written to - so read must wait until done.
    IM_BOOL bDemandLoad;      // are we using demand loading?
    U16     codePage;
    IM_BOOL bWcharFlag;
    wchar_t pathFilename[PATHLEN + FILENAMELEN + 2];

}  IM_FILEID, *IM_FILEIDPTR, * * IM_FILEIDPTRPTR,  *IM_FILEID_PTR, * * IM_FILEID_PTRPTR;

/* BLOCK struct */
typedef struct _rsl_block {
    U8       *data;                    /* Pointer to actual data.           */
    S32        num;                     /* Number of bytes in data.          */
    S32        accumOffset;             /* Number of bytes before this block */
    struct _rsl_block *next;        /* Next block in strip.              */
} RSL_BLOCK, *RSL_BLOCKPTR;



/* SS - Start State struct */
typedef struct _rsl_ss {
    RSL_BLOCKPTR   pBlock;              /* Pointer to data block of this SS  */
    S32		      offset;              /* byte offset in block for SS       */
    S32            line;                /* Line number of this SS            */
    int            bitOffset;           /* bit offset within byte - for G4/G3*/
    struct _rsl_ss *next;           /* ptr to next SS, NULL for end.     */
    /* Optional data for SS */
    S32            numData;             /* number of additional data         */
    U8             data[1];             /* Additional data like TTLINE for G4*/
} RSL_SS, *RSL_SSPTR;



/* STRIP struct */
typedef struct _rsl_strip {
    RSL_BLOCKPTR   pBlock;              /* Pointer to first block in strip.  */
    RSL_SSPTR      pSS;                 /* Pointer to first start state      */
    S32            swidth;              /* strip width                       */
} RSL_STRIP, *RSL_STRIPPTR;


typedef struct _rsl_rast_data {
    int            numstrips;           /* Number of strips, 1 for monolithic*/
    int            numplanes;           /* Number of planes, 3 for 24bit     */
    BOOL           bStartState;         /* FALSE if you do not want to add Start States */

    S32            tlength;             /* tile length                       */
    S32            twidth;              /* tile width                        */
    S32				imagelength;			/* image length							 */
    S32				imagewidth;				/* image width								 */
    IM_BOOL        negative;            /* For scaling/preview - TRUE = neg  */
    RSL_IDF        dataType;            /* RSL dataType of this image        */

    DOUBLE         scaleFactor;         /* Prev image size / Base image size */
    int            image_class;         /* Class of image - Preview, Base    */
    // 23Feb05,BrianH: // Added as optimzation after each open this var is reset - FALSE is the default state.
    IM_BOOL        sample;              /* Data will be sampled on output - for colour images to save data */
    INT            loadDelete;          /* if Flag == 0 never delete, == 1 render only one time, == 2 delete rast */
                                        /* this is a specific flag only used for demand loading and PDF previews */
    /* Demand load variables                                                 */
    IM_BOOL        dmDemandLoad;        /* True - we are keeping the data on disk                   */
    IM_BOOL        dmMainWinLoaded;     /* True if the document has been Loaded in the Main Window  */
    WCHAR          dmFileName[PATHLEN + FILENAMELEN + 2];
    U32            dmFileOffset;        /* Offset from beginning of file to start of raster header  */
    S32            dmNumTiles;          /* Total number of tiles for tiled image                    */
    S32            dmTilesWide;         /* Number of tiles across the width of the image            */
    U32PTR         dmTileOffset;
    U32PTR         dmTileSize;

    /* Extra info needed on RSL_IDF type - can be made a union type          */
    JFIF_PARMPTR   pjpeg_parms;         /* Tables for JPEG data              */

    S32            dmLZ77encoding;      /* Type of LZ77 encoding ZLIB_ENCODING_... defined in trl.h */
    S32            dmLZ77extBits;       /* Number of extra bits in data      */
    S32            dmRawNumBits;        /* Demand load RAW data type         */
    S32            dmRawBytesPerLine;   /* Demand load RAW bytes in a line   */
    S32            dmLZWflags;          /* Demand load LZW data type flags   */
    S32            dmHeaderType;        /* HEL FMT type of image data        */
    /* End of extra stuff                                                    */

    IM_FILEID      fid;                 /* Needed for Unicode conversion <June 8, 2008 DraganM>*/
    RSL_STRIP      strip[3];            /* Array of strips[numstrips]        */

} RSL_RAST_DATA, FAR *RSL_RAST_DATA_PTR;
