#pragma once
#include <fstream>
#include <vector>
#include <map>
#include "Vector.h"

#include "EgfRastStruct.h"

#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class

#define EGF2UV_API __declspec(dllexport)

class CEgfMeta;
typedef CVector<CEgfMeta*> CEgfMetaList;

class CEgfSearchMeta;
//typedef std::vector<CEgfSearchMeta*> CEgfSearchMetaList;
typedef CVector<CEgfSearchMeta*> CEgfSearchMetaList;

typedef enum
{
    SDT_OBJ_NONE,   // 0
    SDT_OBJ_TYPE,   // 1
    SDT_OBJ_ARGS,   // 2
    SDT_OBJ_DATA,   // 3
    SDT_OBJ_CHILDREN_START, // 4
    SDT_OBJ_CHILDREN_END,   // 5
    // global type
    //SDT_SECTION_Offsets, // section offsets
    SDT_Styles_GdiplusGradientParmsTable    = 0x3000,
    SDT_Styles_GdiplusStrokeParmsTable      = 0x3001,
    SDT_Styles_GdiplusArcParmsTable         = 0x3002,
    SDT_Styles_GdiplusFillParmsTable        = 0x3003,
    SDT_Styles_GdiFillParmsTable            = 0x3004,
    SDT_Styles_GdiStrokeParmsTable          = 0x3005,
    SDT_Styles_GdiEllipseParmsTable         = 0x3006,
    SDT_Styles_TextParmsTable               = 0x3007,
    SDT_Styles_EmbeddedFontList             = 0x3008,
    SDT_Styles_DashPatterns                 = 0x3009,
    SDT_Styles_HatchPatterns                = 0x300A,
    SDT_Styles_RasterFillTable              = 0x300B,
    SDT_Styles_EmbeddedTrueTypeFontTable    = 0x300C,
} SIMON_DATA_TYPE;

typedef enum
{
    EGFSOP_LESS,
    EGFSOP_EQUAL,
    EGFSOP_GREATER
} EGF_SEACH_OP;

typedef enum
{
    EGFSVT_bool,
    EGFSVT_int,
    EGFSVT_uint,
    EGFSVT_float,
    EGFSVT_string
} EGF_SEACH_VALUE_TYPE;

#define     GLOBAL_ARGS_ID_mVisible         0
#define     GLOBAL_ARGS_ID_mMask            1

class EGF2UV_API CSearchEle
{
public:
    CSearchEle() : argId(0), op(EGFSOP_EQUAL), valueType(EGFSVT_string), s(0) {}
    ~CSearchEle() { if (valueType == EGFSVT_string && s) delete[] s; }

public:
    int argId;
    EGF_SEACH_OP op;
    EGF_SEACH_VALUE_TYPE valueType;
    union
    {
        bool b;
        int i;
        unsigned int u;
        float f;
        wchar_t *s;
    };
};

class EGF2UV_API CSearchCriteria
{
public:
    CSearchCriteria(){};
    ~CSearchCriteria()
    {
        CVector<CSearchEle*>::iterator it;
        for (it = pairs.begin(); it != pairs.end(); it++)
            delete *it;
        pairs.clear();
    }

public:
    CVector<CSearchEle*> pairs;

};

class EGF2UV_API CEgfPoint
{
public:
    CEgfPoint() :x(0), y(0){};
    CEgfPoint(float x_, float y_) :x(x_), y(y_){};

    float x, y;
};

class CEgfMer
{
public:
    CEgfMer() :x1(0), x2(0), y1(0), y2(0) {};

    float x1, x2, y1, y2;
};

typedef enum
{
    KIDS_MODE_NONE,
    KIDS_MODE_Group,
    KIDS_MODE_Path,
    KIDS_MODE_SText,
    KIDS_MODE_Ext,
    KIDS_MODE_Styles,
} KIDS_MODE;

typedef enum
{
    EGF_ELE_META_TYPE_none,
    EGF_ELE_META_TYPE_8bool,
    EGF_ELE_META_TYPE_intBool,
    EGF_ELE_META_TYPE_byte,
    EGF_ELE_META_TYPE_int,
    EGF_ELE_META_TYPE_color3,
    EGF_ELE_META_TYPE_color4,
    EGF_ELE_META_TYPE_point,
    EGF_ELE_META_TYPE_points = EGF_ELE_META_TYPE_point + 2,
    EGF_ELE_META_TYPE_mer,
    EGF_ELE_META_TYPE_intArray = EGF_ELE_META_TYPE_mer + 4,
	EGF_ELE_META_TYPE_shortArray,
    EGF_ELE_META_TYPE_string,
    EGF_ELE_META_TYPE_lineStyle,
    EGF_ELE_META_TYPE_capStyle,
    EGF_ELE_META_TYPE_operation,
    EGF_ELE_META_TYPE_fillStyle,
    EGF_ELE_META_TYPE_hatchStyle,
    EGF_ELE_META_TYPE_shadingStyle,
    EGF_ELE_META_TYPE_joinStyle,
    EGF_ELE_META_TYPE_curveStyle,
    EGF_ELE_META_TYPE_effectType,
    EGF_ELE_META_TYPE_float,
    EGF_ELE_META_TYPE_double,
    EGF_ELE_META_TYPE_floatArray,
    EGF_ELE_META_TYPE_matrix,
    EGF_ELE_META_TYPE_color3Array,
    EGF_ELE_META_TYPE_color4Array,
    EGF_ELE_META_TYPE_rasterType,
    EGF_ELE_META_TYPE_rasterStruct,
    EGF_ELE_META_TYPE_jpegStruct,
} EGF_ELE_META_TYPE;

typedef enum
{
    DRAW_TYPE_None,
    DRAW_TYPE_point,
    DRAW_TYPE_points,
    DRAW_TYPE_line,
    DRAW_TYPE_lines,
    DRAW_TYPE_Polylines,
    DRAW_TYPE_mer,
}DRAW_TYPE;

class EGF2UV_API CEgfEleMeta
{
public:
    CEgfEleMeta() : type(EGF_ELE_META_TYPE_none), subtype(EGF_ELE_META_TYPE_none), name(0), displayHex(0), drawType(DRAW_TYPE_None), num(-1), num_id(-1){};
    ~CEgfEleMeta()
    {
        if (name)
            delete[] name;

    };

public:
    int id;
    EGF_ELE_META_TYPE type;
    EGF_ELE_META_TYPE subtype;
    wchar_t *name;
    int num;
    int num_id;
    bool displayHex;
    DRAW_TYPE drawType;
    int drawArgs[2];
};

class EGF2UV_API CEgfMeta
{
public:
    CEgfMeta() :index(0), sequenceId(0), egfId(0), name(0), kidsMode(KIDS_MODE_NONE), hasOffset(0), hasDisplayHex(0){};
    ~CEgfMeta()
    {
        if (name)
            delete[] name;

        for (int i = 0; i < (int)elements.size(); i++)
            delete elements[i];
        elements.clear();

        displayOrderMap.clear();
    };

public:
    int index;
    int sequenceId;
    int egfId;
    wchar_t *name;
    KIDS_MODE kidsMode;
    bool hasOffset;
    bool hasDisplayHex;

    CVector<CEgfEleMeta*> elements;
    std::map<int, int> displayOrderMap;
};

class EGF2UV_API CEgfPointArray
{
public:
    CEgfPointArray(){};
    ~CEgfPointArray()
    {
        for (int i = 0; i < (int)points.size(); i++)
            delete points[i];
        points.clear();
    };

    CVector<CEgfPoint*> points;
};

class EGF2UV_API CEgfAttr
{
public:
    CEgfAttr() :type(EGF_ELE_META_TYPE_none), ia(nullptr) {};
    ~CEgfAttr()
    {
        switch (type)
        {
        case EGF_ELE_META_TYPE_intArray:
        case EGF_ELE_META_TYPE_color3Array:
        case EGF_ELE_META_TYPE_color4Array:
            delete[] ia;
            break;
		case EGF_ELE_META_TYPE_shortArray:
			delete[] sa;
			break;
		case EGF_ELE_META_TYPE_floatArray:
			delete[] fa;
			break;
        case EGF_ELE_META_TYPE_string:
            delete[] s;
            break;
        case EGF_ELE_META_TYPE_point:
            delete point;
            break;
        case EGF_ELE_META_TYPE_points:
            delete points;
            break;
        case EGF_ELE_META_TYPE_mer:
            delete mer;
            break;
        case EGF_ELE_META_TYPE_matrix:
            delete[] matrix;
            break;
        case EGF_ELE_META_TYPE_rasterStruct:
            delete pRastStruct;
            break;
        case EGF_ELE_META_TYPE_jpegStruct:
            delete pJpegStruct;
            break;
        }
    };

public:
    EGF_ELE_META_TYPE type;
    union
    {
        bool b;
        unsigned char uc;
        int i;
        float f;
        float *matrix;
        int *ia;
		short *sa;
        float *fa;
        char *s;
        CEgfPoint *point;
        CEgfPointArray *points;
        CEgfMer *mer;
        RSL_RAST_DATA_PTR pRastStruct;
        JFIF_PARMPTR pJpegStruct;
    };


};

class EGF2UV_API CEgfObj
{
public:
    CEgfObj();
    CEgfObj(int objType, int objDataType, int objSize);
    ~CEgfObj();

    virtual bool Parse(std::ifstream &ifs, int sequenceId, int _objDataType);
    virtual void Search(int objMetaId, CSearchCriteria *pPairs, CVector<CEgfObj*> *pList);

public:
    int metaIndex;
    bool hasOffset;

public:
    int mObjType;
    int mObjDataType;
    int mObjSize;

    int mSequenceId;

    CEgfObj *mParent;

    // internal use
    CEgfPoint mOffset;

    CVector<CEgfAttr*> attr;

    CVector<CEgfObj*> children;

    void *pUvObj;

public:

    CEgfAttr* FindAttr(wchar_t *name, EGF_ELE_META_TYPE type = EGF_ELE_META_TYPE_none);
    CEgfMeta* GetMeta();
    std::map<int, int>* GetAttrDisplayOrderMap();
    wchar_t* GetAttrName(int column);
    EGF_ELE_META_TYPE GetAttrType(int column);
    bool GetDisplayHex(int column);
    wchar_t* GetMetaName();
    int GetAttrNumOfEles(int column);
    KIDS_MODE GetKidsMode();
    bool GetHasOffset();

    void PushChild(CEgfObj* pObj);

    CEgfObj* GetChild(int index);

private:
    bool ParseArgs(std::ifstream &ifs);
    bool ParseChildren(std::ifstream &ifs, CEgfObj* pParent);

public:
    static CEgfObj* CreateEgfObj(int objType, int objDataType, int objSize);

    static int M16_ToInt(char *data);
    static int M32_ToInt(char *data);

    static int Egf32_ToInt(char *data);

    static bool ReadObjectFlags(std::ifstream &ifs, bool &visible, bool &mask);
    static bool Read16BitArray(std::ifstream &ifs, int count, short *value);
    static bool Read32BitArray(std::ifstream &ifs, int count, int *value);
    static bool ReadFloatArray(std::ifstream &ifs, int count, float *value);
    static bool ReadMer(std::ifstream &ifs, CEgfMer &mer);
    static bool ReadPoints(std::ifstream &ifs, int count, CEgfPoint *pts);

    static bool ReadByte(std::ifstream &ifs, char &value);
    static bool ReadShort(std::ifstream &ifs, short &value);
    static bool ReadInt(std::ifstream &ifs, int &value);
    static bool ReadFloat(std::ifstream &ifs, float &value);
    static bool ReadString(std::ifstream &ifs, int count, char *buf);
    static bool LoadObjHdr(std::ifstream &ifs, int &objType, int &objDataType, int &objSize);

    static bool CheckCondition(CSearchEle *pCondition, void *pValue);
    static bool CheckColorCondition(CSearchEle *pCondition, char *pValue);
    static bool CheckRGBAColorCondition(CSearchEle *pCondition, short *pValue);
};

class EGF2UV_API CEgfSearchMetaEle
{
public:
    CEgfSearchMetaEle()
        :id(0), ctrlId(0), name(0)
    {
    };
    ~CEgfSearchMetaEle()
    {
        if (name) delete[] name;
    }

public:
    int id;
    int ctrlId;
    wchar_t *name;
};

class EGF2UV_API CEgfSearchMeta
{
public:
    CEgfSearchMeta()
    {
    };
    ~CEgfSearchMeta()
    {
        for (int i = 0; i < (int)elements.size(); i++)
            delete elements[i];

        elements.clear();
    }

public:
    int id;

    CVector<CEgfSearchMetaEle*> elements;
};

bool IsVisible(CEgfObj *pEgfObj);
bool hasMask(CEgfObj *pEgfObj);
bool IsMask(CEgfObj *pEgfObj);
unsigned char* GetAttrByteValue(wchar_t* attrName, CEgfObj *pEgfObj);
char* GetAttrStringValue(wchar_t* attrName, CEgfObj *pEgfObj);
int* GetAttrIntValue(wchar_t* attrName, CEgfObj *pEgfObj);
float* GetAttrFloatValue(wchar_t* attrName, CEgfObj *pEgfObj);
bool* GetAttrBoolValue(wchar_t* attrName, CEgfObj *pEgfObj);
int* GetAttrIntArray(wchar_t* attrName, CEgfObj *pEgfObj);
short* GetAttrShortArray(wchar_t* attrName, CEgfObj *pEgfObj);
float* GetAttrFloatArray(wchar_t* attrName, CEgfObj *pEgfObj);
CEgfPoint* GetAttrPoint(wchar_t* attrName, CEgfObj *pEgfObj);
CEgfPointArray* GetAttrPointArray(wchar_t* attrName, CEgfObj *pEgfObj);
CEgfMer* GetAttrMer(wchar_t* attrName, CEgfObj *pEgfObj);

typedef unsigned IM_UNITS, FAR * IM_UNITS_PTR;
#define IM_UNITS_PELS         (IM_UNITS)0
#define IM_UNITS_INCH         (IM_UNITS)1
#define IM_UNITS_CM           (IM_UNITS)2
#define IM_UNITS_MM           (IM_UNITS)3
#define IM_UNITS_M            (IM_UNITS)4
#define IM_UNITS_FT           (IM_UNITS)5
#define IM_UNITS_POINTS       (IM_UNITS)6
#define IM_UNITS_TWIPS        (IM_UNITS)7
#define IM_UNITS_CUSTOM1      (IM_UNITS)8
#define IM_UNITS_CUSTOM2      (IM_UNITS)9
#define IM_UNITS_CUSTOM3      (IM_UNITS)0x0a
#define IM_UNITS_MEASSCALED   (IM_UNITS)0x0b
#define IM_UNITS_PERCENT      (IM_UNITS)0x0c
#define IM_UNITS_PROPORTIONAL (IM_UNITS)0x0d    /*  Default coordinate system used by the API.  */
#define IM_UNITS_DISPLAYPIXELS (IM_UNITS)0x0e   /* For display based dashing. <18May99,Harmeet> */

#define IM_UNITS_ANGULAR_FLAG (IM_UNITS)0x10
#define IM_UNITS_DEG          (IM_UNITS)0x10
#define IM_UNITS_RAD          (IM_UNITS)0x11

#define IM_UNITS_UNITMASK     (IM_UNITS)0x1F

#define IM_UNITS_SQUARED_FLAG (IM_UNITS)0x20  /* modifier for square linear units... */
#define IM_UNITS_CUBED_FLAG   (IM_UNITS)0x40  /* modifier for cubic linear units... */

#define IM_UNITS_YRES_FLAG    (IM_UNITS)0x80  /* flag to use y resolution (used with IM_UNITS_PELS) */

#define IM_UNITS_MULTIPLE     (IM_UNITS)254
#define IM_UNITS_UNASSIGNED   (IM_UNITS)255

#define PI        ((double)3.1415926534)
#define RAD2DEG   ((double)(180.0 / PI))
#define DEG2RAD   ((double)(PI / 180.0))

double TransMeas2Meas(int inUnits, double inValue, int outUnits);
