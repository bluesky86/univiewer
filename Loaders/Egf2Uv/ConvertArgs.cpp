#include "stdafx.h"
#include "CEgf2UvFile.h"
#include "tinyxml2.h"
#include "Error.h"
#include "UvFile.h"
#include "UvmUtils.h"
#include "CUvPoint.h"
#include "CUvPath.h"
#include "CUvRefer.h"
#include "CUvImage.h"
#include "CUvText.h"
#include "CUvFont.h"
#include <string>
#include <vector>
#include "DebugNew.h"
#include <fstream>
#include "assert.h"


void CEgf2UvFile::ConvertMer(wchar_t* attrName, CEgfObj *pEgfObj, CUvRectF *pUvRectF)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        pUvRectF->left = pEgfAttr->mer->x1 / 4096;
        pUvRectF->top = pEgfAttr->mer->y1 / 4096;
        pUvRectF->right = pEgfAttr->mer->x2 / 4096;
        pUvRectF->bottom = pEgfAttr->mer->y2 / 4096;
    }
}

void CEgf2UvFile::ConvertColor(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    if (pStrokeFill == nullptr) return;

    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        CUvColor c = CUvColor::MakeColor(pEgfAttr->i);
        c.a = 255;
        pStrokeFill->SetStrokeColor(c);
    }
}

void CEgf2UvFile::ConvertFillColor(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    if (pStrokeFill == nullptr) return;

    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        CUvColor c = MapStrokeFillColor(&pEgfAttr->i);
        //CUvColor c = CUvColor::MakeColor(pEgfAttr->i);
        c.a = 255;
        pStrokeFill->AppendFillColor(c);
    }
}

void CEgf2UvFile::ConvertPoint(wchar_t* attrName, CEgfObj *pEgfObj, CUvPointF *pUvPointF)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        pUvPointF->x = pEgfAttr->point->x / 4096;
        pUvPointF->y = pEgfAttr->point->y / 4096;
    }
}

CUvPointF* CEgf2UvFile::ConvertPoints(wchar_t* attrName, CEgfObj *pEgfObj, int *count)
{
    CEgfPointArray* attrPoints = GetAttrPointArray(attrName, pEgfObj);
    if (attrPoints && attrPoints->points.size() > 0)
    {
        U32 size = (U32)attrPoints->points.size();
        CUvPointF *pPoints = NEW CUvPointF[size];
        for (U32 i = 0; i < size; i++)
        {
            pPoints[i].x = attrPoints->points[i]->x / 4096;
            pPoints[i].y = attrPoints->points[i]->y / 4096;
        }
        if (count) *count = (int)size;
        return pPoints;
    }
    return nullptr;
}

void CEgf2UvFile::ConvertThickness(CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    int units = 0;
    float value = 0;
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr((wchar_t*)L"ThicknessUnits");
    if (pEgfAttr)
        units = pEgfAttr->i;
    pEgfAttr = pEgfObj->FindAttr((wchar_t*)L"ThicknessValue");
    if (pEgfAttr)
        value = pEgfAttr->f;

    pStrokeFill->SetStrokeWidth((int)TransMeas2Meas(units, value, IM_UNITS_PELS));
}

void CEgf2UvFile::ConvertLineStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    if (pStrokeFill == nullptr) return;

    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        switch (pEgfAttr->i)
        {
        case 0:     //EGF_LINESOLID
        case 246:   //EGF_SYSTEMSOLID
            pStrokeFill->SetLineStyle( uvSolid_DashStyle);
            break;
        case 1:     //EGF_LINEDASH
        case 247:   //EGF_SYSTEMDASH
            pStrokeFill->SetLineStyle(uvDash_DashStyle);
            break;
        case 2:     //EGF_LINEDOT
        case 248:   //EGF_SYSTEMDOT
            pStrokeFill->SetLineStyle(uvDot_DashStyle);
            break;
        case 3:     //EGF_LINEDASHDOT
        case 249:   //EGF_SYSTEMDASHDOT
            pStrokeFill->SetLineStyle(uvDashDot_DashStyle);
            break;
        case 4:     //EGF_LINEDASHDOTDOT
        case 250:   //EGF_SYSTEMDASHDOTDOT
            pStrokeFill->SetLineStyle(uvDashDotDot_DashStyle);
            break;
        case 251:   //EGF_LINEFILL
        case 252:   //EGF_LINEUNIMPLEMENT
        case 254:   //EGF_LINEMULTIPLE
        case 255:   //EGF_LINEUNASSIGNED
            break;
        case 253:   //EGF_LINENULL: line is invisible
            if (pStrokeFill->GetStrokeFill())
            {
                if (*pStrokeFill->GetStrokeFill() == uvStroke_StrokeFill)
                    pStrokeFill->SetStrokeFill(uvNone_StrokeFill);
                else if (*pStrokeFill->GetStrokeFill() == uvStrokeAndFill_StrokeFill)
                    pStrokeFill->SetStrokeFill(uvFill_StrokeFill);
            }
            break;
        default: // TODO: custome style (in EGF, from 5 to 245 are custom style
            //uvDocCustom_DashStyle = 100000,  // custom dash style shared in docment level
            //uvPageCustom_DashStyle = 200000, // custom dash style shared in page level
            if (pEgfAttr->i > 0 && pEgfAttr->i < (int)pEgfFile->m_pDashPattern->children.size())
            {
                pStrokeFill->SetLineStyle(uvDocCustom_DashStyle + pEgfAttr->i - 5);
            }
            else
                pStrokeFill->SetLineStyle(uvSolid_DashStyle);

            break;
        }
    }
}

bool CEgf2UvFile::ConvertCapStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    if (pStrokeFill == nullptr) return false;

    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        switch (pEgfAttr->i)
        {
        case 0:
            pStrokeFill->SetStrokeCap(uvRound_StrokeCap);
            break;
        case 1:
        case 11:
            pStrokeFill->SetStrokeCap(uvSquare_StrokeCap);
            break;
        case 2:
            //cs = _T("2-Multiple");
            pStrokeFill->SetStrokeCap(uvButt_StrokeCap);
            break;
        case 3:
            //cs = _T("3-Unassigned");
            pStrokeFill->SetStrokeCap(uvButt_StrokeCap);
        case 10:
            //cs = _T("10-Flat");
            //break;
        case 13:
            //cs = _T("13-Triangle");
            //break;
        case 16:
            //cs = _T("16-NoAnchor");
            //break;
        case 17:
            //cs = _T("17-SquareAnchor");
            //break;
        case 18:
            //cs = _T("18-RoundAnchor");
            //break;
        case 19:
            //cs = _T("19-DiamondAnchor");
            //break;
        case 20:
            //cs = _T("20-ArrowAnchor");
            //break;
        case 21:
            //cs = _T("21-CapCustom");
            //break;
        default:
            //cs.Format(L"%d", style);
            //break;
            pStrokeFill->SetStrokeCap(uvButt_StrokeCap);
            break;
        }

        return true;
    }

    return false;
}

void CEgf2UvFile::ConvertJoinStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    if (pStrokeFill == nullptr) return;

    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        switch (pEgfAttr->i)
        {
        case 0:
        case 12:
            pStrokeFill->SetStrokeJoin(uvRound_StrokeJoin);
            break;
        case 1:
        case 10:
            pStrokeFill->SetStrokeJoin(uvMiter_StrokeJoin);
            break;
        case 2:
            //cs = _T("2-Multiple");
            pStrokeFill->SetStrokeJoin(uvMiter_StrokeJoin);
            break;
        case 3:
            //cs = _T("3-Unassigned");
            pStrokeFill->SetStrokeJoin(uvMiter_StrokeJoin);
            break;
        case 4:
            //cs = _T("4-Diamond");
            pStrokeFill->SetStrokeJoin(uvMiter_StrokeJoin);
            break;
        case 11:
            pStrokeFill->SetStrokeJoin(uvBevel_StrokeJoin);
            break;
        case 13:
            //cs = _T("13-MiterClipped");
            pStrokeFill->SetStrokeJoin(uvMiter_StrokeJoin);
            break;
        default:
            //cs.Format(L"%d", style);
            pStrokeFill->SetStrokeJoin(uvMiter_StrokeJoin);
            break;
        }
    }
}

void CEgf2UvFile::ConvertFillStyle(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    if (pStrokeFill == nullptr) return;

    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr)
    {
        switch (pEgfAttr->i)
        {
        case 0: // Transparent
            if (pStrokeFill->GetStrokeFill())
                pStrokeFill->SetStrokeFill(uvNone_StrokeFill);
            break;
        case 1: //Opaque
        case 2: //Erase
        case 3: //Translucent
            if (pStrokeFill->GetStrokeFill())
            {
                if (*pStrokeFill->GetStrokeFill() == uvStroke_StrokeFill)
                {
                    pStrokeFill->SetStrokeFill(uvStrokeAndFill_StrokeFill);
                    pStrokeFill->SetFillType(uvColor_FillType);
                }
                else if (*pStrokeFill->GetStrokeFill() == uvNone_StrokeFill)
                {
                    pStrokeFill->SetStrokeFill(uvFill_StrokeFill);
                    pStrokeFill->SetFillType(uvColor_FillType);
                }
            }
            break;
        case 4: //Hatch
            assert(false);
            break;
        case 5: //Sketch
            assert(false);
            break;
        case 6: //Multiple
            assert(false);
            break;
        case 7: //Unassigned
            assert(false);
            break;
        case 8: //FillRaster
            assert(false);
            break;
        case 9: //LinearGradient
            if (pStrokeFill->GetStrokeFill() && *pStrokeFill->GetStrokeFill() == uvStroke_StrokeFill)
                pStrokeFill->SetStrokeFill(uvStrokeAndFill_StrokeFill);
            pStrokeFill->SetFillType(uvLinearGradient_FillType);
            break;
        case 10: //PathGradient
            if (pStrokeFill->GetStrokeFill() && *pStrokeFill->GetStrokeFill() == uvStroke_StrokeFill)
                pStrokeFill->SetStrokeFill(uvStrokeAndFill_StrokeFill);
            pStrokeFill->SetFillType(uvRadialGradient_FillType);
            break;
        case 11: //XOR
            assert(false);
            break;
        case 12: //Merge
            assert(false);
            break;
        case 13: //Mask
            assert(false);
            break;
        case 14: //NotXOR
            assert(false);
            break;
        case 15: //NotMerge
            assert(false);
            break;
        case 16: //NOP
            assert(false);
            break;
        case 17: //FILLNULL
            assert(false);
            break;
        case 18: //FillObj
            assert(false);
            break;
        case 19: //FillInvert
            assert(false);
            break;
        default:
            pStrokeFill->SetStrokeFill(uvStroke_StrokeFill);
            break;
        }
    }
}

void CEgf2UvFile::ConvertGradientFill(CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    int *gradientIndex = GetAttrIntValue((wchar_t*)L"GradientIndex", pEgfObj);
    if (pEgfFile && gradientIndex && *gradientIndex > 0 && *gradientIndex <= (int)pEgfFile->m_pGdiplusGradientParmsTable->children.size())
    {
        CEgfObj *pGradientParms = pEgfFile->m_pGdiplusGradientParmsTable->children[*gradientIndex - 1];
        if (pGradientParms == nullptr) return;

        int *pValue = GetAttrIntValue((wchar_t*)L"NoOfGradientColors", pGradientParms);
        int numColors = pValue ? *pValue : 0;
        if (numColors == 0) return;

        int *intArray = GetAttrIntArray((wchar_t*)L"GradientColours", pGradientParms);
        if (intArray == nullptr) return;
        CUvColor *pColors = NEW CUvColor[numColors];
        for (int i = 0; i < numColors; i++)
        {
            pColors[i] = MapStrokeFillColor(&intArray[i]);
            pColors[i].a = 0xFF;
            //pColors[i] = CUvColor::MakeColor(intArray[i]);
        }

        unsigned char *pString = (unsigned char*)GetAttrStringValue((wchar_t*)L"GradientAlpha", pGradientParms);
        if (pString)
        {
            for (int i = 0; i < numColors; i++)
                pColors[i].a = pString[i];
        }
        pStrokeFill->ClearFillColors();
        if (pStrokeFill->GetFillType() == uvLinearGradient_FillType)
        {
            for (int i = 0; i < numColors; i++)
                pStrokeFill->AppendFillColor(pColors[i]);
        }
        else
        {
            for (int i = numColors-1; i >= 0; i--)
                pStrokeFill->AppendFillColor(pColors[i]);
        }
        delete[] pColors;

        short *shortArray = GetAttrShortArray((wchar_t*)L"BlendPosition", pGradientParms);
        if (shortArray)
        {
            if (pStrokeFill->GetFillType() == uvLinearGradient_FillType)
            {
                CVector<float> colorPos;
                for (int i = 0; i < numColors; i++)
                    colorPos.push_back(shortArray[i] / 1000.0f);

                pStrokeFill->SetLinearGradientColorPos(&colorPos);
            }
            else
            {
                CVector<float> colorPos;
                for (int i = numColors - 1; i >= 0; i--)
                    colorPos.push_back(1.0f - shortArray[i] / 1000.0f);

                pStrokeFill->SetRadialGradientColorPos(&colorPos);

                CUvPointF center;
                int ptCount = 0;
                CUvPointF *pPoints = ConvertPoints((wchar_t*)L"GradientPoints", pEgfObj, &ptCount);
                if (pPoints && ptCount > 0) 
                    center = pPoints[0];
                if(pPoints) delete[] pPoints;
                pStrokeFill->SetRadialGradientCenter(center);

                float radius = .0f;
                CUvRectF rect;
                ConvertMer((wchar_t*)L"Mer", pEgfObj, &rect);
                radius = rect.Width()*2; // @@@@@@@@@@@@@@@@@@@@@@@@
                pStrokeFill->SetRadialGradientRadius(radius);
            }
        }
    }
}

void CEgf2UvFile::ConvertFillMode(wchar_t* attrName, CEgfObj *pEgfObj, CUvStrokeFill *pStrokeFill)
{
    if (pStrokeFill == nullptr) return;

    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr && pEgfAttr->i == 0)
        pStrokeFill->SetFillMode(uvEvenOdd_FillMode);
    else
        pStrokeFill->SetFillMode(uvWinding_FillMode);
}

int CEgf2UvFile::ConvertAngle(wchar_t* attrName, CEgfObj *pEgfObj)
{
    int *pAngle = GetAttrIntValue(attrName, pEgfObj);
    return pAngle ? *pAngle / 1000000 : 0;
}

void CEgf2UvFile::ConvertDashPatterns(CEgfObj *pDashs, CUvLinePatterns &uvDashPatterns)
{
    if (pDashs == nullptr) return;

    CEgfAttr *pEgfAttr;
    int numDashs = (int)pDashs->children.size();
    int count = 0;
    for (int i = 5; i < numDashs; i++)
    {
        int length = 0;
        float *patterns = nullptr;
        pEgfAttr = pDashs->children[i]->FindAttr((wchar_t*)L"PatternLength");
        if (pEgfAttr) length = pEgfAttr->uc;
        pEgfAttr = pDashs->children[i]->FindAttr((wchar_t*)L"Pattern");
        if (pEgfAttr) patterns = pEgfAttr->fa;

        CUvLinePattern *pPattern = NEW CUvLinePattern();
        if (patterns)
        {
            for (int j = 0; j < length; j++)
                pPattern->intervals.push_back(fabs(patterns[j]));// / 7.0f));
        }
        uvDashPatterns.patterns.insert(std::pair<int, CUvLinePattern*>(count++, pPattern));
    }
}

void CEgf2UvFile::ConvertEmbedFonts(CEgfObj *pObj, CUvFonts *pUvFonts)
{
    if (pObj == nullptr) return;

    int numFonts = (int)pObj->children.size();
    int count = 0;
    for (int i = 0; i < numFonts; i++)
    {
        char *fontName = GetStringValue((wchar_t*)L"Font", pObj->children[i]);
        int bufSize = GetIntValue((wchar_t*)L"FontDataSize", pObj->children[i]);
        char *bufData = GetStringValue((wchar_t*)L"FontData", pObj->children[i]);
        bool bSearchByGlyphId = GetBoolValue( (wchar_t*)L"ByGlyphIndex", pObj->children[i] );
        bool bWinCharset = GetBoolValue( (wchar_t*)L"WinCharset", pObj->children[i] );

        if (fontName)// && bufSize)
        {
            CUvFont *pUvFont = NEW CUvFont();
            pUvFont->setFamilyName((U8*)fontName);
            if (bufData && bufSize > 0)
            {
                U8* pData = NEW U8[bufSize];
                memcpy(pData, bufData, bufSize);
                pUvFont->setData(pData, bufSize);

                //FILE *fp;
                //fopen_s(&fp, "C:\\Workspaces\\fonts\\a.ttf", "wb");
                //fwrite(pData, bufSize, 1, fp);
                //fclose(fp);
            }

            CUvFontFace *pFace = NEW CUvFontFace();
            pFace->setFaceName((U8*)pUvFont->getFamilyName());
            pFace->setFamilyName(pUvFont->getFamilyName());
            pFace->setParent(pUvFont);
            pUvFont->appendFace(pFace);
            pUvFont->setSearchByGlyphId( bSearchByGlyphId );
            if(bWinCharset)
                pUvFont->setCharsetType( CHARSET_TYPE_windows );
            //else if(bufSize == 0)
            //    pUvFont->setCharsetType( CHARSET_TYPE_adobe );

            pUvFonts->append(pUvFont);
        }
    }
}

CUvColor CEgf2UvFile::MapStrokeFillColor(int *pColor)
{
    CUvColor color;
    if (pColor)
    {
        if ((*pColor & 0x00FFFFFF) == 0)
            color = CUvColor::MakeColor(0xFFFFFF);
        else if ((*pColor & 0x00FFFFFF) == 0xFFFFFF)
            color = CUvColor::MakeColor(0);
        else
            color = CUvColor::MakeColor(*pColor);
    }

    return color;
}

bool CEgf2UvFile::GetBoolValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    bool *pValue = GetAttrBoolValue(attrName, pEgfObj);
    if (pValue)
        return *pValue;
    return false;
}
unsigned char CEgf2UvFile::GetByteValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    unsigned char *pValue = GetAttrByteValue(attrName, pEgfObj);
    if (pValue)
        return *pValue;
    return 0;
}
int CEgf2UvFile::GetIntValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    int *pValue = GetAttrIntValue(attrName, pEgfObj);
    if (pValue)
        return *pValue;
    return 0;
}
float CEgf2UvFile::GetFloatValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName);
    if (pEgfAttr == nullptr || (pEgfAttr->type != EGF_ELE_META_TYPE_float && pEgfAttr->type != EGF_ELE_META_TYPE_double))
        return 0;
    return pEgfAttr->f;
}
char* CEgf2UvFile::GetStringValue(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName, EGF_ELE_META_TYPE_string);
    if (pEgfAttr == nullptr)
        return nullptr;
    return pEgfAttr->s;
}

RSL_RAST_DATA_PTR CEgf2UvFile::GetRasterStruct(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName, EGF_ELE_META_TYPE_rasterStruct);
    if (pEgfAttr == nullptr)
        return nullptr;
    return pEgfAttr->pRastStruct;
}

JFIF_PARMPTR CEgf2UvFile::GetJpegStruct(wchar_t* attrName, CEgfObj *pEgfObj)
{
    CEgfAttr *pEgfAttr = pEgfObj->FindAttr(attrName, EGF_ELE_META_TYPE_jpegStruct);
    if (pEgfAttr == nullptr)
        return nullptr;
    return pEgfAttr->pJpegStruct;
}
