#pragma once
#include "EgfObj.h"

#define EGF2UV_API __declspec(dllexport)

class EGF2UV_API CEgfFile
{
public:
    CEgfFile();
    ~CEgfFile();


public:

    CEgfObj *m_pChild;

    CEgfObj *m_pGdiplusGradientParmsTable;
    CEgfObj *m_pGdiplusStrokeParmsTable;
    CEgfObj *m_pGdiplusArcParmsTable;
    CEgfObj *m_pGdiplusFillParmsTable;
    CEgfObj *m_pGdiFillParmsTable;
    CEgfObj *m_pGdiStrokeParmsTable;
    CEgfObj *m_pGdiEllipseParmsTable;
    CEgfObj *m_pTextParmsTable;
    CEgfObj *m_pEmbeddedFontList;
    CEgfObj *m_pDashPattern;
    CEgfObj *m_pHatchPattern;
    CEgfObj *m_pRasterFillTables;
    CEgfObj *m_pEmbeddedTrueTypeFonts;

public:
    bool Parse(std::ifstream &ifs);
    void Clear();
    void Search(int objMetaId, CSearchCriteria *pPairs, CVector<CEgfObj*> *pList);

    CEgfObj* FindObjFromTree(CVector<int> *index);

private:
    void ConvertEgfDashPatterns(CEgfObj *pObj);
};

