#pragma once

#define UNIV_RENDER_WIN_API __declspec(dllexport)

UNIV_RENDER_WIN_API char* DrawEmfWin(int type, unsigned int size, unsigned char *data, int &memBmpFileSize);
