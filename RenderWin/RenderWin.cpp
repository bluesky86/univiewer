// RenderWin.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "wingdi.h"
#include "RenderWin.h"
#include "math.h"
#include "stdio.h"
#include "assert.h"

BOOL SetBackColorToWhite(HDC pDC)
{
    // Set brush to desired background color
    HBRUSH backBrush = (HBRUSH)(RGB(255, 255, 255));


    // Save old brush
    HBRUSH pOldBrush = (HBRUSH)::SelectObject(pDC, backBrush);

    RECT rect;
    ::GetClipBox(pDC, &rect);     // Erase the area needed

    //paint the given rectangle using the brush that is currently selected 
    //into the specified device context
    ::PatBlt(pDC, rect.left, rect.top, abs(rect.left - rect.right), abs(rect.top - rect.bottom), PATCOPY);

    //Select back the old brush
    ::SelectObject(pDC, pOldBrush);

    return TRUE;
}

HANDLE DDBToDIB(HBITMAP bitmap, DWORD dwCompression, HPALETTE pPal)
{
    BITMAP			bm;
    BITMAPINFOHEADER	bi;
    LPBITMAPINFOHEADER 	lpbi;
    DWORD			dwLen;
    HANDLE			hDIB;
    HANDLE			handle;
    HDC 			hDC;
    HPALETTE		hPal;


    // The function has no arg for bitfields
    if (dwCompression == BI_BITFIELDS)
        return NULL;

    // If a palette has not been supplied use defaul palette
    hPal = (HPALETTE)pPal;
    if (hPal == NULL)
        hPal = (HPALETTE) ::GetStockObject(DEFAULT_PALETTE);

    // Get bitmap information
    ::GetObject(bitmap, sizeof(bm), (LPSTR)&bm);

    // Initialize the bitmapinfoheader
    bi.biSize = sizeof(BITMAPINFOHEADER);
    bi.biWidth = bm.bmWidth;
    bi.biHeight = bm.bmHeight;
    bi.biPlanes = 1;
    bi.biBitCount = bm.bmPlanes * bm.bmBitsPixel;
    bi.biCompression = dwCompression;
    bi.biSizeImage = 0;
    bi.biXPelsPerMeter = 0;
    bi.biYPelsPerMeter = 0;
    bi.biClrUsed = 0;
    bi.biClrImportant = 0;

    // Compute the size of the  infoheader and the color table
    int nColors = (1 << bi.biBitCount);
    if (nColors > 256)
        nColors = 0;
    dwLen = bi.biSize + nColors * sizeof(RGBQUAD);

    // We need a device context to get the DIB from
    hDC = ::GetDC(NULL);
    hPal = SelectPalette(hDC, hPal, FALSE);
    RealizePalette(hDC);

    // Allocate enough memory to hold bitmapinfoheader and color table
    hDIB = GlobalAlloc(GMEM_FIXED, dwLen);

    if (!hDIB)
    {
        SelectPalette(hDC, hPal, FALSE);
        ::ReleaseDC(NULL, hDC);
        return NULL;
    }

    lpbi = (LPBITMAPINFOHEADER)hDIB;

    *lpbi = bi;

    // Call GetDIBits with a NULL lpBits param, so the device driver 
    // will calculate the biSizeImage field 
    GetDIBits(hDC, (HBITMAP)bitmap, 0L, (DWORD)bi.biHeight,
        (LPBYTE)NULL, (LPBITMAPINFO)lpbi, (DWORD)DIB_RGB_COLORS);

    bi = *lpbi;

    // If the driver did not fill in the biSizeImage field, then compute it
    // Each scan line of the image is aligned on a DWORD (32bit) boundary
    if (bi.biSizeImage == 0)
    {
        bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8)
            * bi.biHeight;

        // If a compression scheme is used the result may infact be larger
        // Increase the size to account for this.
        if (dwCompression != BI_RGB)
            bi.biSizeImage = (bi.biSizeImage * 3) / 2;
    }

    // Realloc the buffer so that it can hold all the bits
    dwLen += bi.biSizeImage;
    if (handle = GlobalReAlloc(hDIB, dwLen, GMEM_MOVEABLE))
    {
        hDIB = handle;
    }
    else
    {
        GlobalFree(hDIB);

        // Reselect the original palette
        SelectPalette(hDC, hPal, FALSE);
        ::ReleaseDC(NULL, hDC);
        return NULL;
    }

    // Get the bitmap bits
    lpbi = (LPBITMAPINFOHEADER)hDIB;

    // FINALLY get the DIB
    BOOL bGotBits = GetDIBits(hDC, bitmap,
        0L,				// Start scan line
        (DWORD)bi.biHeight,		// # of scan lines
        (LPBYTE)lpbi 			// address for bitmap bits
        + (bi.biSize + nColors * sizeof(RGBQUAD)),
        (LPBITMAPINFO)lpbi,		// address of bitmapinfo
        (DWORD)DIB_RGB_COLORS);		// Use RGB for color table

    if (!bGotBits)
    {
        GlobalFree(hDIB);

        SelectPalette(hDC, hPal, FALSE);
        ::ReleaseDC(NULL, hDC);
        return NULL;
    }

    SelectPalette(hDC, hPal, FALSE);
    ::ReleaseDC(NULL, hDC);
    return hDIB;
}

char* CreateBMPInMemory(HANDLE hDIB, int &memBmpFileSize)
{
    BITMAPFILEHEADER	hdr;
    LPBITMAPINFOHEADER	lpbi;

    if (!hDIB)
        return nullptr;

    lpbi = (LPBITMAPINFOHEADER)hDIB;

    int nColors = 0;
    if (lpbi->biBitCount <= 8)
        nColors = (1 << lpbi->biBitCount);

    int dibSize = (int)GlobalSize(hDIB);
    int bmpHeaserSize = (int)sizeof(BITMAPFILEHEADER);

    // Fill in the fields of the file header 
    hdr.bfType = ((WORD)('M' << 8) | 'B');	// is always "BM"
    hdr.bfSize = (DWORD)dibSize + sizeof(hdr);
    hdr.bfReserved1 = 0;
    hdr.bfReserved2 = 0;
    hdr.bfOffBits = (DWORD)(sizeof(hdr) + lpbi->biSize + nColors * sizeof(RGBQUAD));

    memBmpFileSize = bmpHeaserSize + dibSize;

    char *buffer = new char[memBmpFileSize];
    memcpy_s(buffer, bmpHeaserSize + dibSize, &hdr, bmpHeaserSize);
    memcpy_s(&buffer[bmpHeaserSize], dibSize, lpbi, dibSize);

    return buffer;
}

//UNIV_RENDER_WIN_API void DrawEmfWin(int type, unsigned int size, unsigned char *data)
UNIV_RENDER_WIN_API char* DrawEmfWin(int type, unsigned int size, unsigned char *data, int &memBmpFileSize)
{
    HENHMETAFILE hMetaFile = 0;

    //{
    //    FILE *fp;
    //    fopen_s(&fp, "F:\\Test\\tmp1\\b.emf", "wb");
    //    fwrite(data, 1, size, fp);
    //    fclose(fp);
    //}

    if (type == 0) // wmf: convert to emf
    {

    }
    else // emf
    {
        hMetaFile = SetEnhMetaFileBits(size, data);
    }

    if (hMetaFile == 0) return nullptr;

    ENHMETAHEADER emh;
    ZeroMemory(&emh, sizeof(ENHMETAHEADER));
    emh.nSize = sizeof(ENHMETAHEADER);
    if (GetEnhMetaFileHeader(hMetaFile, sizeof(ENHMETAHEADER), &emh) == 0)
    {
        DeleteEnhMetaFile(hMetaFile);
        return nullptr;
    }

    HDC dc = GetDC(0);
    HDC memDC = CreateCompatibleDC(dc);
    if (memDC == 0)
    {
        DeleteEnhMetaFile(hMetaFile);
        return nullptr;
    }

    // Get width and height of emf
    RECT rect;
    float    PixelsX, PixelsY, MMX, MMY;

    //float fAspectRatio;
    long lWidth, lHeight;

    // Get the characteristics of the output device.
    PixelsX = (float)GetDeviceCaps(dc, HORZRES);
    PixelsY = (float)GetDeviceCaps(dc, VERTRES);
    MMX = (float)GetDeviceCaps(dc, HORZSIZE);
    MMY = (float)GetDeviceCaps(dc, VERTSIZE);

    // Calculate the rect in which to draw the metafile based on the
    // intended size and the current output device resolution.
    // Remember that the intended size is given in 0.01 mm units, so
    // convert those to device units on the target device.
    rect.top = (int)((float)(emh.rclFrame.top) * PixelsY / (MMY*100.0f));
    rect.left = (int)((float)(emh.rclFrame.left) * PixelsX / (MMX*100.0f));
    rect.right = (int)((float)(emh.rclFrame.right) * PixelsX / (MMX*100.0f));
    rect.bottom = (int)((float)(emh.rclFrame.bottom) * PixelsY / (MMY*100.0f));

    //Calculate the Width and Height of the metafile
    lWidth = (long)((float)(abs(rect.left - rect.right)));
    lHeight = (long)((float)(abs(rect.top - rect.bottom)));

    //
    HBITMAP bmap = CreateCompatibleBitmap(dc, lWidth, lHeight);
    HGDIOBJ oldobj = SelectObject(memDC, bmap);
    SetBackColorToWhite(memDC);

    if (PlayEnhMetaFile(memDC, hMetaFile, &rect) == FALSE)
    {
        //DWORD dwRet = GetLastError();
        //DeleteDC(memDC);
        //DeleteEnhMetaFile(hMetaFile);
        //return nullptr;
    }

    HPALETTE pal = 0;
    if (GetDeviceCaps(dc, RASTERCAPS) & RC_PALETTE)
    {
        UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
        LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];
        pLP->palVersion = 0x300;

        pLP->palNumEntries =
            GetSystemPaletteEntries(dc, 0, 255, pLP->palPalEntry);

        // Create the palette
        pal = ::CreatePalette(pLP);

        delete[] pLP;
    }

    HANDLE hDIB = DDBToDIB(bmap, BI_RGB, pal);

    if (hDIB == NULL)
    {
        DeleteEnhMetaFile(hMetaFile);
        return nullptr;
    }

    ////////////////////////
    LPBITMAPINFOHEADER	lpbi = (LPBITMAPINFOHEADER)hDIB;

    //int globalSize = GlobalSize(hDIB);
    //int headerSize = sizeof(BITMAPINFOHEADER);

    ///////////////////////////////////////
    //// draw 
    char *bmpData = CreateBMPInMemory(hDIB, memBmpFileSize);

    GlobalFree(hDIB);

    SelectObject(memDC, oldobj);
    DeleteDC(memDC);

    DeleteEnhMetaFile(hMetaFile);

    return bmpData;
}
