#include "stdafx.h"
#include "CUvText.h"
#include "DebugNew.h"
#include "CUvRect.h"


CUvText::CUvText()
    :encoding(uvUTF8_TextEncoding), align(uvLeft_TextAlign), text(nullptr), textLen(0), codepage( CODEPAGE_WESTERN )
    , fontSize(9), x(0), y(0), pFontFace(nullptr), bReleaseFontFace(false), scaleX(1.0f), orientation(0.0f)
    , convertedTextLen(0), convertedText(nullptr)
{
    type = uvText_ShapeType;
}

CUvText::~CUvText()
{
    if(text)
        delete[] text;
    if(convertedText)
        delete[] convertedText;

    if (bReleaseFontFace && pFontFace)
        delete pFontFace;
}

CUvText* CUvText::Copy()
{
    CUvText *p = NEW CUvText();
    p->CopyFrom(this);

    return p;
}

void CUvText::CopyFrom(CUvText *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    encoding = p->encoding;
    align = p->align;
    textLen = p->textLen;
    fontSize = p->fontSize;
    x = p->x;
    y = p->y;
    mat = p->mat;
    scaleX = p->scaleX;
    orientation = p->orientation;

    text = NEW U8[textLen + 2];
    memcpy( text, p->text, textLen );
    text[textLen] = text[textLen + 1] = 0;
    if(convertedText)
    {
        convertedTextLen = p->convertedTextLen;
        convertedText = NEW U8[textLen + 2];
        memcpy( convertedText, p->convertedText, convertedTextLen );
        convertedText[convertedTextLen] = convertedText[convertedTextLen + 1] = 0;
    }

    bReleaseFontFace = p->bReleaseFontFace;
    if (bReleaseFontFace)
    {
        pFontFace = NEW CUvFontFace;
        pFontFace->copyFrom(p->pFontFace);
    }
}

void CUvText::GetBound(CUvRectF &rect)
{
    CUvPointF pt(x, y);
    rect.Set(pt);
    pt.x++; pt.y++;
    rect.Add(pt);
}



S32 fontSize;
CUvFontFace *pFontFace;
bool bReleaseFontFace;

