#pragma once
#include "CUvShape.h"
#include "CUvRect.h"
#include "Vector.h"
#include "CUvMap.h"
#include "CUvMatrix.h"

typedef enum _uv_ImageType
{
    uvImageType_file,
    uvImageType_buffer,
    uvImageType_strip,
    uvImageType_wmf,
    uvImageType_emf,
}UV_ImageType;

typedef enum _uv_ImageColorEffectType
{
    uvImageColorEffectType_none,
    uvImageColorEffectType_colorChange,
    uvImageColorEffectType_duoTone,
    uvImageColorEffectType_grayScale,
}UV_ImageColorEffectType;

typedef enum _uv_Image_Encoding
{
    uvImageEncoding_none,
    uvImageEncoding_Nibble,
    uvImageEncoding_G4M,
    uvImageEncoding_G4MT,
    uvImageEncoding_G3M,
    uvImageEncoding_G3M2D,
    uvImageEncoding_G3MT,
    uvImageEncoding_XG4,
    uvImageEncoding_Raw,
    uvImageEncoding_RawTiled,
    uvImageEncoding_CC1,
    uvImageEncoding_CC1RGB,
    uvImageEncoding_CC1RGBA,
    uvImageEncoding_JPEG,
    uvImageEncoding_SJPEG,
    uvImageEncoding_LZW,
    uvImageEncoding_24BP,
}UV_ImageEncoding;

typedef enum _uv_ImageEncodedOrigin
{
    uvTopLeft_Image = 1, // Default
    uvTopRight_Image = 2, // mirror (Reflected across y-axis)
    uvBottomRight_Image = 3, // rotation 180 = ( mirror + vertical mirror )
    uvBottomLeft_Image = 4, // mirror + rotation 180 = vertical mirror (Reflected across x-axis)
    uvLeftTop_Image = 5, // mirror + rotation -90 (Reflected across x-axis, Rotated 90 CCW)
    uvRightTop_Image = 6, // rotation -90 (Rotated 90 CW)
    uvRightBottom_Image = 7, // mirror + rotation 90 (Reflected across x-axis, Rotated 90 CW)
    uvLeftBottom_Image = 8, // rotation 90 (Rotated 90 CCW)
}UV_ImageEncodedOrigin;

#define  RGB2RGB_CCONVERSION        0
#define  YCBCR_CCONVERSION          1
#define  CMYK2RGB_CCONVERSION       2  /* < Mar 13, 2009 HocH > support CMYK color space JPEG files */

#define  ADOBE_JPEG  1
#define  NOADOBE_JPEG  0

typedef struct _tagJPEGhuffdecode {
    int            max_val;         /* number of values in the val array                      */
    U8             ncodes[16];      /* how many codes of [index] size exist                   */
    U16            val[400];        /* val >> 4 = run : val & 0xF = num bits for value        */
    S32            tbl[256];        /* Huffman decoding table                                 */
} JPGHUFFDECODE, *JPGHUFFDECODEPTR;


typedef struct _tagJPEGhead {
    U16   width;
    U16   height;
    U16   mcu_h1;
    U16   mcu_v1;
    U16   mcu_h2;
    U16   mcu_v2;
    U16   mcu_h3;
    U16   mcu_v3;
    U16   mcu_h4;
    U16   mcu_v4;
    U8    components;
    U8    colour_conversion;
    U8    adobe_jpeg;
    U8    padd;
    U16   tifV7height;
} JPGHEAD, *JPGHEADPTR;


typedef struct _tagJPEGscan {
    U8  components;
    U8  comp_select1;
    U8  dc_ac1;
    U8  comp_select2;
    U8  dc_ac2;
    U8  comp_select3;
    U8  dc_ac3;
    U8  comp_select4;
    U8  dc_ac4;
    U8  spect_start;
    U8  spect_end;
    U8  ah_al;
    U8  padd[2];
} JPGSCAN, *JPGSCANPTR;

typedef struct _tagJPEGdecodeparms {
    S16               dc_predict;
    U16               quantbl[80];
    U16               mcu_h;
    U16               mcu_v;
    JPGHUFFDECODE     huffdc;
    JPGHUFFDECODE     huffac;
    S32               quantbl2[80]; /* 29Oct97(Brian) Set to 80 for overruns - last 16 entries all the same */
} JPGPRMS, *JPGPRMSPTR;

typedef struct _tagJFIFparms {
    JPGSCAN        jpgscan;
    JPGHEAD        jpeghead;
    JPGHUFFDECODE  huffdc0;
    JPGHUFFDECODE  huffdc1;
    JPGHUFFDECODE  huffdc2; // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
    JPGHUFFDECODE  huffdc3; // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
    JPGHUFFDECODE  huffac0;
    JPGHUFFDECODE  huffac1;
    JPGHUFFDECODE  huffac2; // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
    JPGHUFFDECODE  huffac3; // <Sept.28, 2015 Simon> DCS-8084:Jpg specifications says up to 4 huffuman tables can be supported
    U16            dri;             /* Sequential DCT MCU count restart interval  0 - none      */
    U16            quant[4][64];
    int            bPreviewReq;     /* True if decompressing this JPEG as preview (1/8th image) */
    U8             quant_table1;    /* Quantization table selector for component 1              */
    U8             quant_table2;    /* These selectors which of the 4(max) quantize tables we use */
    U8             quant_table3;    /* In reality only 2 tables are used Y & CbCr table         */
    U8             quant_table4;    /* In reality only 2 tables are used Y & CbCr table         */
} JFIF_PARM, *JFIF_PARMPTR;


class CUvRasterStripBlock
{
public:
    CUvRasterStripBlock() : data(nullptr), size(0), next(nullptr), tileIndex(0){};
    ~CUvRasterStripBlock()
    {
        if (data) delete[] data;
        if (next) delete next;
    };

public:
    unsigned char           *data;                    /* Pointer to actual data.           */
    int                     size;                     /* Number of bytes in data.          */
    CUvRasterStripBlock     *next;                    /* Next block in strip.              */
    int                     tileIndex;                /* tile index in image               */
};
class CUvRasterStrip
{
public:
    CUvRasterStrip() :pBlock(nullptr), width(0), numBlocks(0), sizeRefLine(0), firstRefLine(nullptr){};
    ~CUvRasterStrip()
    {
        if (pBlock) delete pBlock;
        if (firstRefLine) delete[] firstRefLine;
    };

public:
    CUvRasterStripBlock   *pBlock;              /* Pointer to first block in strip.  */
    int                    width;              /* strip width                       */
    int                    numBlocks;
    int                    sizeRefLine;        // the byte number of first refer line
    char                   *firstRefLine;      // the first refer line for G4
};

class UNIV_API CUvImage : public CUvShape
{
public:
    CUvImage();
    virtual ~CUvImage();


    CUvShape* Copy();
    virtual void CopyFrom(CUvImage *p);
    virtual void GetBound(CUvRectF &rect);

    void* GetInfo();
    void SetInfo(void* in);

public:
    UV_ImageType imageType;
    UV_ImageEncoding encodingType;
    const char *href;
    size_t data_len;
    int numStrips;
    CVector<CUvRasterStrip*> data;
    CUvMatrix mat;

    int numBits;
    int numColors;
    CUvColor *palette;
    int imageWidth, imageHeight;
    int outNumBits;

    int x, y;
    int targetWidth, targetHeight;
    bool widthIsPercentage, heightIsPercentage;

    bool bTiled;
    int numTiles;            // total number of tiles
    int numTilesPerWidth;    // number of tiles per image width
    int tileWidth;           // the width of each tile
    int tileHeight;          // the tile height of each tile except last row of tile

    JFIF_PARMPTR jpegParams;

    UV_ImageEncodedOrigin origin;       // rotation and mirror
    bool bFlipRGB;

    UV_ImageColorEffectType colorEffectType;
    CUvMap<CUvColor, CUvColor> colorMap;

private:
    void *info;

};

