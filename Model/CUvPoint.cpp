#include "stdafx.h"
#include "CUvPoint.h"
#include "DebugNew.h"
#include "CUvRect.h"


////////////////////////////////////////////////////////////////////////////////
// float point
CUvPointF::CUvPointF()
    :x(0.0f), y(0.0f)
{
}

CUvPointF::CUvPointF(float _x, float _y)
    :x(_x), y(_y)
{
}

CUvPointF::~CUvPointF()
{
}

CUvPointF* CUvPointF::Copy()
{
    CUvPointF *p = NEW CUvPointF();

    p->CopyFrom(this);

    return p;
}

void CUvPointF::CopyFrom(CUvPointF *p)
{
    if (p == nullptr) return;

    x = p->x;
    y = p->y;
}

////////////////////////////////////////////////////////////////////////////////
// integer point
CUvPoint::CUvPoint()
    :x(0), y(0)
{
}

CUvPoint::CUvPoint(int _x, int _y)
    : x(_x), y(_y)
{
}

CUvPoint::~CUvPoint()
{
}

CUvPoint* CUvPoint::Copy()
{
    CUvPoint *p = NEW CUvPoint();

    p->CopyFrom(this);

    return p;
}

void CUvPoint::CopyFrom(CUvPoint *p)
{
    if (p == nullptr) return;

    x = p->x;
    y = p->y;
}

////////////////////////////////////////////////////////////////////////////////
// float points
CUvPointFs::CUvPointFs()
{
}


CUvPointFs::~CUvPointFs()
{
}

CUvPointFs* CUvPointFs::Copy()
{
    CUvPointFs *p = NEW CUvPointFs();
    p->CopyFrom(this);

    return p;
}

void CUvPointFs::CopyFrom(CUvPointFs *p)
{
    if (p == nullptr) return;

    for (size_t i = 0; i < p->points.size(); i++)
        points.push_back(p->points[i]);
}

////////////////////////////////////////////////////////////////////////////////
// integer points
CUvPoints::CUvPoints()
{
}


CUvPoints::~CUvPoints()
{
}

CUvPoints* CUvPoints::Copy()
{
    CUvPoints *p = NEW CUvPoints();
    p->CopyFrom(this);

    return p;
}

void CUvPoints::CopyFrom(CUvPoints *p)
{
    if (p == nullptr) return;

    for (size_t i = 0; i < p->points.size(); i++)
        points.push_back(p->points[i]);
}

////////////////////////////////////////////////////////////////////////////////
// float point shape
CUvPointFShape::CUvPointFShape()
{
    type = uvPointF_ShapeType;
}

CUvPointFShape::CUvPointFShape(float _x, float _y)
    : CUvPointF(_x, _y)
{
    type = uvPointF_ShapeType;
}

CUvPointFShape::~CUvPointFShape()
{
}

CUvShape* CUvPointFShape::Copy()
{
    CUvPointFShape *p = NEW CUvPointFShape();

    p->CopyFrom(this);

    return p;
}

void CUvPointFShape::CopyFrom(CUvPointFShape *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);
    CUvPointF::CopyFrom(p);
}

void CUvPointFShape::GetBound(CUvRectF &rect)
{
    CUvPointF pt(x,y);
    rect.Set(pt);
}



////////////////////////////////////////////////////////////////////////////////
// float points shape
CUvPointFsShape::CUvPointFsShape()
{
    type = uvPointFs_ShapeType;
}

CUvPointFsShape::~CUvPointFsShape()
{
}

CUvShape* CUvPointFsShape::Copy()
{
    CUvPointFsShape *p = NEW CUvPointFsShape();

    p->CopyFrom(this);

    return p;
}

void CUvPointFsShape::CopyFrom(CUvPointFsShape *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);
    CUvPointFs::CopyFrom(p);
}

void CUvPointFsShape::GetBound(CUvRectF &rect)
{
    for (size_t i = 0; i < points.size(); i++)
    {
        if (i == 0)
            rect.Set(points[i]);
        else
            rect.Add(points[i]);
    }
}

//////////////////////////////////////////////////////////////////
// line
CUvLine::CUvLine()
{
    type = uvLine_ShapeType;
}
CUvLine::~CUvLine()
{
}

CUvShape* CUvLine::Copy()
{
    CUvLine *p = NEW CUvLine();
    p->CopyFrom(this);

    return p;
}

void CUvLine::CopyFrom(CUvLine *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);
    point[0] = p->point[0];
    point[1] = p->point[1];
}

void CUvLine::GetBound(CUvRectF &rect)
{
    rect.Set(point[0]);
    rect.Add(point[1]);
}
