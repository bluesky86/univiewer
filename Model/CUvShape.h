#pragma once
#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class

#include "Model.h"
#include "CUvColor.h"
#include <vector>
#include <map>


class CUvMatrix;
class CUvStrokeFill;
class CUvRect;
class CUvRectF;

//
class UNIV_API CUvLinePattern
{
public:
    CUvLinePattern();
    ~CUvLinePattern();

public:
    std::vector<float> intervals;
};

class UNIV_API CUvLinePatterns
{
public:
    CUvLinePatterns();
    ~CUvLinePatterns();

    CUvLinePattern* Find(int id);
    void Clear();

public:
    std::map<int, CUvLinePattern*> patterns;
};

class UNIV_API CUvShape
{
public:
    CUvShape();
    virtual ~CUvShape();
    
    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvShape *p);
    virtual void GetBound(CUvRectF &rect);

public:
    UvShapeType type;

    CUvStrokeFill *pStrokeFill;

    CUvShape *parent;
};


