#include "stdafx.h"
#include "CUvShape.h"
#include "DebugNew.h"
#include "CUvStrokeFill.h"

//////////////////////////////////////////////////////////////////////////
CUvLinePattern::CUvLinePattern()
{

}
CUvLinePattern::~CUvLinePattern()
{
    //intervals.clear();
}

//////////////////////////////////////////////////////////////////////////
CUvLinePatterns::CUvLinePatterns()
{

}
CUvLinePatterns::~CUvLinePatterns()
{
    Clear();
}
CUvLinePattern* CUvLinePatterns::Find(int id)
{
    return patterns[id];
}
void CUvLinePatterns::Clear()
{
    std::map<int, CUvLinePattern*>::iterator it;
    for (it = patterns.begin(); it != patterns.end(); it++)
        delete it->second;
    patterns.clear();
}



//////////////////////////////////////////////////////////////////////////

CUvShape::CUvShape()
    :type(uvShape_ShapeType), pStrokeFill(nullptr), parent(nullptr)
{
}


CUvShape::~CUvShape()
{
    if (pStrokeFill) delete pStrokeFill;
}

CUvShape* CUvShape::Copy()
{
    CUvShape *p = NEW CUvShape();
    p->CopyFrom(this);

    return p;
}

void CUvShape::CopyFrom(CUvShape *p)
{
    if (p == nullptr) return;

    type = p->type;

    if (p->pStrokeFill)
    {
        pStrokeFill = NEW CUvStrokeFill();
        pStrokeFill->CopyFrom(p->pStrokeFill);
    }

    parent = p->parent;
}

void CUvShape::GetBound(CUvRectF &rect)
{

}
