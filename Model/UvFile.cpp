#include "stdafx.h"
#include "UvFile.h"
//#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <string.h>
#include "DebugNew.h"

namespace UvModel
{
    UNIV_API UV_FILE*    uv_createMemFile(size_t memBufSize)
    {
        UV_FILE * bsFile = NEW UV_FILE;
        memset(bsFile, 0, sizeof(UV_FILE));
        bsFile->type = UV_FILE_MEM;
        bsFile->buffer = NEW unsigned char[memBufSize];
        bsFile->pos = 0;
        bsFile->filesize = memBufSize;
        bsFile->fp = NULL;
        return bsFile;
    }

    UNIV_API UV_FILE*    uv_fopen(const char *filename, const char *mode, unsigned char *memBuf, size_t memBufSize)
    {
        UV_FILE * bsFile = NEW UV_FILE;
        memset(bsFile, 0, sizeof(UV_FILE));

        if (memBuf)
        {
            bsFile->type = UV_FILE_MEM;
            bsFile->buffer = memBuf;
            bsFile->pos = 0;
            bsFile->filesize = memBufSize;
            bsFile->fp = NULL;
        }
        else
        {
            bsFile->type = UV_FILE_FILE;
            fopen_s(&bsFile->fp, filename, mode);
            if (!bsFile->fp)
            {
                delete bsFile;
                return NULL;
            }

            fseek(bsFile->fp, 0, SEEK_END);
            bsFile->filesize = (size_t)ftell(bsFile->fp);
            fseek(bsFile->fp, 0, SEEK_SET);
            bsFile->pos = 0;
        }

        return bsFile;
    }

    // caller has the responsibility to release UV_FILE
    UNIV_API UV_FILE*    uv_wfopen(const wchar_t *filename, const wchar_t *mode, unsigned char *memBuf, size_t memBufSize)
    {
        UV_FILE * bsFile = NEW UV_FILE;
        memset(bsFile, 0, sizeof(UV_FILE));

        if (memBuf)
        {
            bsFile->type = UV_FILE_MEM;
            bsFile->buffer = memBuf;
            bsFile->pos = 0;
            bsFile->filesize = memBufSize;
            bsFile->fp = NULL;
        }
        else
        {
            bsFile->type = UV_FILE_FILE;
            _wfopen_s(&bsFile->fp, filename, mode);
            if (!bsFile->fp)
            {
                delete bsFile;
                return NULL;
            }
            fseek(bsFile->fp, 0, SEEK_END);
            bsFile->filesize = (size_t)ftell(bsFile->fp);
            fseek(bsFile->fp, 0, SEEK_SET);
            bsFile->pos = 0;
        }

        return bsFile;
    }

    UNIV_API UV_FILE*    uv_wfopen_2mem(const wchar_t *filename, const wchar_t *mode)
    {
        UV_FILE*    fp = uv_wfopen(filename, mode, NULL, NULL);
        if (fp == NULL) return NULL;
        unsigned char * buffer = NEW unsigned char[fp->filesize + 1];
        size_t bytes = uv_fread(buffer, 1, fp->filesize, fp);
        UV_FILE* resFile = uv_wfopen(NULL, NULL, buffer, bytes);
        uv_fclose(fp);
        return resFile;
    }

    UNIV_API int uv_wfopenf(UV_FILE *bsFile, const wchar_t *filename, const wchar_t *mode, unsigned char *memBuf, size_t memBufSize)
    {
        memset(bsFile, 0, sizeof(UV_FILE));

        if (memBuf)
        {
            bsFile->type = UV_FILE_MEM;
            bsFile->buffer = memBuf;
            bsFile->pos = 0;
            bsFile->filesize = memBufSize;
            bsFile->fp = NULL;
        }
        else
        {
            bsFile->type = UV_FILE_FILE;
            _wfopen_s(&bsFile->fp, filename, mode);

            fseek(bsFile->fp, 0, SEEK_END);
            bsFile->filesize = (size_t)ftell(bsFile->fp);
            fseek(bsFile->fp, 0, SEEK_SET);
            bsFile->pos = 0;
        }

        return 1;
    }

    UNIV_API int uv_isMemFile(UV_FILE* fp)
    {
        if (fp->type == UV_FILE_MEM) return 1;
        else return 0;
    }

    UNIV_API size_t uv_flength(UV_FILE *fp)
    {
        if(fp) return fp->filesize;
        else return 0;
    }

    UNIV_API char* uv_fgets(char *string, int n, UV_FILE* fp)
    {
        size_t i, count = 0;

        if (fp->type == UV_FILE_FILE)
        {
            fgets(string, n, fp->fp);
        }
        else
        {
            for (i = fp->pos; i < fp->filesize; i++)
            {
                if (fp->buffer[i] == 0 || fp->buffer[i] == '\n')
                {
                    count = i - fp->pos;
                    break;
                }
            }
            memcpy(string, &(fp->buffer[fp->pos]), count);
            string[count] = 0;

            fp->pos = +count;
        }

        return string;
    }

    UNIV_API size_t  uv_fread(void *buffer, size_t size, size_t count, UV_FILE *fp)
    {
        size_t readBytes = 0;
        size_t requestBytes = size * count;

        if (fp->type == UV_FILE_FILE)
        {
            readBytes = fread(buffer, size, count, fp->fp);
        }
        else
        {
            if (requestBytes <= fp->filesize - fp->pos)
                readBytes = requestBytes;
            else
                readBytes = fp->filesize - fp->pos;
            memcpy(buffer, fp->buffer + fp->pos, readBytes);
        }

        fp->pos += readBytes;

        return readBytes / size;
    }

    UNIV_API int uv_fprintf(UV_FILE* fp, const char *format, ...)
    {
        return 0;
    }

    UNIV_API int uv_fputc(int c, UV_FILE* fp)
    {
        if (fp->type == UV_FILE_FILE)
            fputc(c, fp->fp);
        else
            fp->buffer[fp->pos] = c;

        fp->pos++;

        return c;
    }

    UNIV_API int uv_fputs(const char *string, UV_FILE* fp)
    {
        if (fp->type == UV_FILE_FILE)
        {
            fputs(string, fp->fp);
        }
        else
        {
            strcpy_s((char*)&(fp->buffer[fp->pos]), strlen(string), string);
        }

        fp->pos += strlen(string);

        return 1;
    }

    UNIV_API size_t  uv_fwrite(const void *buffer, size_t size, size_t count, UV_FILE* fp)
    {
        size_t len = size*count;
        if (fp->type == UV_FILE_FILE)
        {
            fwrite(buffer, size, count, fp->fp);
        }
        else
        {
            const char* buf = (const char*)buffer;
            for (int i = 0; i < len; i++)
                fp->buffer[fp->pos + i] = buf[i];
        }

        fp->pos += len;

        return count;
    }

    UNIV_API int uv_fclose(UV_FILE* fp, bool dataToo)
    {
        if (fp)
        {
            if (fp->type == UV_FILE_FILE)
            {
                if (fp->fp) fclose(fp->fp);
                fp->fp = NULL;
                delete fp;
                fp = NULL;
            }
            else
            {
                if (fp->buffer && dataToo)
                {
                    delete[] fp->buffer;
                    fp->buffer = NULL;
                    delete fp;
                    fp = NULL;
                }
            }
        }

        return 0;
    }

    UNIV_API int uv_feof(UV_FILE* fp)
    {
        if (fp->pos >= fp->filesize)
            return 1;

        return 0;
    }

    UNIV_API int uv_ferror(UV_FILE* fp)
    {
        return 0;
    }


    UNIV_API int uv_fflush(UV_FILE* fp)
    {
        return 0;
    }

    UNIV_API int uv_fseek(UV_FILE* fp, int offset, int origin)
    {
        if (fp->type == UV_FILE_FILE)
        {
            fseek(fp->fp, offset, origin);
            fp->pos = ftell(fp->fp);
        }
        else
        {
            switch (origin)
            {
            case SEEK_CUR:
                fp->pos += offset;
                break;
            case SEEK_END:
                if (offset > 0) return 1;
                fp->pos = fp->filesize + offset;
                break;
            case SEEK_SET:
            default:
                if (offset < 0) return 1;
                fp->pos = offset;
                break;
            }
        }

        if (fp->pos < 0 || fp->pos > fp->filesize) return 1;

        return 0;
    }

    UNIV_API void uv_rewind(UV_FILE* fp)
    {
        if (fp->type == UV_FILE_FILE)
            rewind(fp->fp);

        fp->pos = 0;
    }

    UNIV_API int uv_ftell(UV_FILE* fp)
    {
        return (int)fp->pos;
    }

    UNIV_API int uv_setvbuf(UV_FILE* fp, char *buffer, int mode, size_t size)
    {
        return 0;
    }

    UNIV_API int uv_unlink(const char *filename)
    {
        return 0;
    }

    UNIV_API int uv_wunlink(const wchar_t *filename)
    {
        return _wunlink(filename);
    }

    UNIV_API int uv_access(const wchar_t *path, int mode)
    {
        return _waccess(path, mode);
    }

} // UvModel