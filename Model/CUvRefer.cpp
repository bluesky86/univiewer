#include "stdafx.h"
#include "CUvRefer.h"
#include "DebugNew.h"

//////////////////////////////////////////////////////////////////////////
CUvRefer::CUvRefer()
    :pRefer(nullptr)
{
    type = uvRefer_ShapeType;
}

CUvRefer::~CUvRefer()
{
}

CUvShape* CUvRefer::Copy()
{
    CUvRefer *p = NEW CUvRefer();
    p->CopyFrom(this);

    return p;
}

void CUvRefer::CopyFrom(CUvRefer *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    pRefer = p->pRefer;
    mat = p->mat;
}

void CUvRefer::GetBound(CUvRectF &rect)
{
    if (pRefer)
        pRefer->GetBound(rect);
}

//////////////////////////////////////////////////////////////////////////
CUvRefers::CUvRefers()
{

}

CUvRefers::~CUvRefers()
{
    Clear();
}

void CUvRefers::Clear()
{
    std::map<int, CUvShape*>::iterator it;
    for (it = uvShapes.begin(); it != uvShapes.end(); it++)
        delete it->second;
    uvShapes.clear();
}


