#pragma once
#include "Model.h"
#include <vector>
#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class

typedef enum
{
    CHARSET_TYPE_none,
    CHARSET_TYPE_windows,
    CHARSET_TYPE_adobe,
}CHARSET_TYPE;

class UNIV_API CUvFont;

class UNIV_API CUvFontFace
{
public:
    enum Weight {
        uvInvisible_Weight = 0,
        uvThin_Weight = 100,
        uvExtraLight_Weight = 200,
        uvLight_Weight = 300,
        uvNormal_Weight = 400,
        uvMedium_Weight = 500,
        uvSemiBold_Weight = 600,
        uvBold_Weight = 700,
        uvExtraBold_Weight = 800,
        uvBlack_Weight = 900,
        uvExtraBlack_Weight = 1000,
    };

    enum Width {
        uvUltraCondensed_Width = 1,
        uvExtraCondensed_Width = 2,
        uvCondensed_Width = 3,
        uvSemiCondensed_Width = 4,
        uvNormal_Width = 5,
        uvSemiExpanded_Width = 6,
        uvExpanded_Width = 7,
        uvExtraExpanded_Width = 8,
        uvUltraExpanded_Width = 9,
    };

    enum Slant {
        uvUpright_Slant,
        uvItalic_Slant,
        uvOblique_Slant,
    };

    CUvFontFace();
    ~CUvFontFace();

    const U8* getFaceName();
    void setFaceName(U8* _faceName);
    U8** getFaceNameAddr();
    const U8* getFamilyName();
    void setFamilyName(const U8* _familyName);

    bool isItalic();
    void setItalic(bool _i);

    bool isBold();
    void setBold(bool _b);

    bool isUnderline();
    void setUnderline(bool _b);

    bool isStrikeOut();
    void setStrikeOut(bool _b);

    CUvFont* getParent();
    void setParent(CUvFont*);

    void copyFrom(CUvFontFace *p);

private:
    U8* familyName; // UTF-8:
    U8* faceName; // UTF-8: nullptr means it is default font face which refers to parent (CUvFont)
    CUvFont* parent;
    bool bItalic;
    bool bBold;
    bool bUnderline;
    bool bStrikeOut;
};


class UNIV_API CUvFont
{
public:
    CUvFont();
    ~CUvFont();

    const U8* getFamilyName();
    void setFamilyName(const U8* _familyName);
    const U8* getHref();
    void setHref(const U8* _href);
    const U8* getData(size_t &len);
    void setData(const U8* _data, size_t len);

    bool isEmbeddedFont();

    size_t faceCount();
    void appendFace(CUvFontFace* font);
    void removeFace(S32 index);
    CUvFontFace* findFace(const U8* _faceName);
    void clearFace();
    std::vector<CUvFontFace*>* getFontFaces();

    void* getRenderFont();
    void setRenderFont( void *pFont );

    bool getSearchByGlyphId();
    void setSearchByGlyphId( bool v );

    CHARSET_TYPE getCharsetType();
    void setCharsetType( CHARSET_TYPE v );

private:
    const U8* familyName; // UTF-8
    const U8* href;
    const U8* data;
    size_t data_len;
    bool bSearchByGlyphId;
    CHARSET_TYPE charsetType;
    std::vector<CUvFontFace*> *fontFaces;

    void *pRenderFont;
    
};

class UNIV_API CUvFonts
{
public:
    CUvFonts();
    ~CUvFonts();

    size_t size();
    CUvFont* operator[](S32 index);
    void append(CUvFont* font);
    void remove(S32 index);
    CUvFont* find(const U8* familyName);
    CUvFontFace* findFace(const U8* faceName);
    void clear();
    std::vector<CUvFont*>* getFonts();

private:
    std::vector<CUvFont*> *fonts;
};

UNIV_API size_t ConvertHexFontToBinary(const char *src, char** dst);
UNIV_API size_t ConvertBinaryFontToHexText(const char *src, size_t src_len, char** dst);
UNIV_API void ConvertBinaryFontToHexTextFromFile(char *src);
