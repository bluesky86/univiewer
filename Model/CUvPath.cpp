#include "stdafx.h"
#include "CUvPath.h"
#include "DebugNew.h"
#include "CUvRect.h"

/////////////////////////////////////////////////////////////////////
CUvPathOp::CUvPathOp()
    :verb(uvNone_Verbs), pShape(nullptr), bCopy(false)
{

}

CUvPathOp::CUvPathOp(UvPathVerbs v, CUvShape *shape, bool copy, bool _bAddLineFromLastPoint)
    : verb(v), bCopy(copy), bAddLineFromLastPoint(_bAddLineFromLastPoint)
{
    if (copy)
    {
        pShape = shape->Copy();
    }
    else
        pShape = shape;
}

CUvPathOp::~CUvPathOp()
{
    //if (pShape && bCopy)
    if(pShape)
        delete pShape;
}

CUvPathOp* CUvPathOp::Copy()
{
    CUvPathOp *p = NEW CUvPathOp(verb, pShape, bAddLineFromLastPoint);
    p->matrix = matrix;
    return p;
}

/////////////////////////////////////////////////////////////////////
CUvQuad::CUvQuad()
    :points(nullptr)
{
    type = uvQuad_ShapeType;
}

CUvQuad::~CUvQuad()
{
    if (points)
        delete points;
}

CUvShape* CUvQuad::Copy()
{
    CUvQuad *p = NEW CUvQuad();
    p->CopyFrom(this);

    return p;
}

void CUvQuad::CopyFrom(CUvQuad *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    if(p->points)
        points = (CUvPointFs*)p->points->Copy();
}

void CUvQuad::GetBound(CUvRectF &rect)
{
    for (size_t i = 0; i < points->points.size(); i++)
    {
        if (i == 0) rect.Set(points->points[i]);
        else rect.Add(points->points[i]);
    }
}

/////////////////////////////////////////////////////////////////////
CUvCubic::CUvCubic()
    :points(nullptr)
{
    type = uvCubic_ShapeType;
}

CUvCubic::~CUvCubic()
{
    if (points)
        delete points;
}

CUvShape* CUvCubic::Copy()
{
    CUvCubic *p = NEW CUvCubic();
    p->CopyFrom(this);

    return p;
}

void CUvCubic::CopyFrom(CUvCubic *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    if (p->points)
        points = (CUvPointFs*)p->points->Copy();
}

void CUvCubic::GetBound(CUvRectF &rect)
{
    for (size_t i = 0; i < points->points.size(); i++)
    {
        if (i == 0) rect.Set(points->points[i]);
        else rect.Add(points->points[i]);
    }
}

/////////////////////////////////////////////////////////////////////
CUvPoly::CUvPoly()
    :points(nullptr), bPolygon(false)
{
    type = uvPoly_ShapeType;
}

CUvPoly::~CUvPoly()
{
    if (points)
        delete points;
}

CUvShape* CUvPoly::Copy()
{
    CUvPoly *p = NEW CUvPoly();
    p->CopyFrom(this);

    return p;
}

void CUvPoly::CopyFrom(CUvPoly *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    points = (CUvPointFs*)p->points->Copy();
    bPolygon = p->bPolygon;
}

void CUvPoly::GetBound(CUvRectF &rect)
{
    for (size_t i = 0; i < points->points.size(); i++)
    {
        if (i == 0) rect.Set(points->points[i]);
        else rect.Add(points->points[i]);
    }
}


/////////////////////////////////////////////////////////////////////
CUvAngleArc::CUvAngleArc()
    :startAngle(0.0), sweepAngle(0.0), xRotation(0.0)
{
    type = uvAngleArc_ShapeType;
}

CUvAngleArc::~CUvAngleArc()
{
}

CUvShape* CUvAngleArc::Copy()
{
    CUvAngleArc *p = NEW CUvAngleArc();
    p->CopyFrom(this);

    return p;
}

void CUvAngleArc::CopyFrom(CUvAngleArc *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    oval = p->oval;
    startAngle = p->startAngle;
    sweepAngle = p->sweepAngle;
    xRotation = p->xRotation;
}

void CUvAngleArc::GetBound(CUvRectF &rect)
{
    CUvPointF pt1(oval.left, oval.top);
    CUvPointF pt2(oval.top, oval.bottom);
    rect.Set(pt1);
    rect.Add(pt2);
}

/////////////////////////////////////////////////////////////////////
CUvTangentArc::CUvTangentArc()
    :radius(0.0f)
{
    type = uvTangentArc_ShapeType;
}

CUvTangentArc::~CUvTangentArc()
{
}

CUvShape* CUvTangentArc::Copy()
{
    CUvTangentArc *p = NEW CUvTangentArc();
    p->CopyFrom(this);

    return p;
}

void CUvTangentArc::CopyFrom(CUvTangentArc *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    points[0] = p->points[0];
    points[1] = p->points[1];
    points[2] = p->points[2];
    radius = p->radius;
}

void CUvTangentArc::GetBound(CUvRectF &rect)
{
    rect.Set(points[0]);
    rect.Add(points[1]);
    rect.Add(points[2]);
}

/////////////////////////////////////////////////////////////////////
CUvOval::CUvOval()
    :bClockwise(true), xRotation(0)
{
    type = uvOval_ShapeType;
}

CUvOval::~CUvOval()
{
}

CUvShape* CUvOval::Copy()
{
    CUvOval *p = NEW CUvOval();
    p->CopyFrom(this);

    return p;
}

void CUvOval::CopyFrom(CUvOval *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    rect = p->rect;
    bClockwise = p->bClockwise;
    xRotation = p->xRotation;
}

void CUvOval::GetBound(CUvRectF &rect1)
{
    CUvPointF pt1(rect.left, rect.top);
    CUvPointF pt2(rect.right, rect.bottom);
    rect1.Set(pt1);
    rect1.Add(pt2);
}


/////////////////////////////////////////////////////////////////////
CUvOvalArc::CUvOvalArc()
    :x(.0f), y(.0f), bLargeArc(false)
{
    type = uvOvalArc_ShapeType;
}

CUvOvalArc::~CUvOvalArc()
{
}

CUvShape* CUvOvalArc::Copy()
{
    CUvOvalArc *p = NEW CUvOvalArc();
    p->CopyFrom(this);

    return p;
}

void CUvOvalArc::CopyFrom(CUvOvalArc *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    rx = p->rx;
    ry = p->ry;
    x = p->x;
    y = p->y;
    xRotate = p->xRotate;
    bLargeArc = p->bLargeArc;
    bClockwise = p->bClockwise;
}

void CUvOvalArc::GetBound(CUvRectF &rect1)
{
    CUvPointF pt1(x - rx, y - ry);
    CUvPointF pt2(x + rx, y + ry);
    rect1.Set(pt1);
    rect1.Add(pt2);
}

/////////////////////////////////////////////////////////////////////
CUvCircle::CUvCircle()
    :radius(.0f), bClockwise(true)
{
    type = uvCircle_ShapeType;
}

CUvCircle::~CUvCircle()
{
}

CUvShape* CUvCircle::Copy()
{
    CUvCircle *p = NEW CUvCircle();
    p->CopyFrom(this);

    return p;
}

void CUvCircle::CopyFrom(CUvCircle *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    center = p->center;
    radius = p->radius;
    bClockwise = p->bClockwise;
}

void CUvCircle::GetBound(CUvRectF &rect1)
{
    CUvPointF pt1(center.x - radius, center.y - radius);
    CUvPointF pt2(center.x + radius, center.y + radius);
    rect1.Set(pt1);
    rect1.Add(pt2);
}

/////////////////////////////////////////////////////////////////////
CUvCircleArc::CUvCircleArc()
    :startAngle(.0f), sweepAngle(.0f), useCenter(true)
{
    type = uvCircleArc_ShapeType;
}

CUvCircleArc::~CUvCircleArc()
{
}

CUvShape* CUvCircleArc::Copy()
{
    CUvCircleArc *p = NEW CUvCircleArc();
    p->CopyFrom(this);

    return p;
}

void CUvCircleArc::CopyFrom(CUvCircleArc *p)
{
    if (p == nullptr) return;

    CUvRectFShape::CopyFrom(p);

    startAngle = p->startAngle;
    sweepAngle = p->sweepAngle;
    useCenter = p->useCenter;
}

void CUvCircleArc::GetBound(CUvRectF &rect)
{
    CUvRectFShape::GetBound(rect);
}

/////////////////////////////////////////////////////////////////////
CUvPath::CUvPath()
    :fillMode(uvWinding_FillMode), bClosed(false)
{
    type = uvPath_ShapeType;

    ops = nullptr;
    ops = new std::vector<CUvPathOp*>;
}


CUvPath::~CUvPath()
{
    reset();
}

int CUvPath::getVerbCount() const
{
    return (int)ops->size();
}

std::vector<CUvPathOp*>* CUvPath::getOps()
{
    return ops;
}

CUvShape* CUvPath::Copy()
{
    CUvPath *p = NEW CUvPath();
    p->CopyFrom(this);

    return p;
}

void CUvPath::CopyFrom(CUvPath *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    fillMode = p->fillMode;

    if (p->ops)
    {
        for (size_t i = 0; i < p->ops->size(); i++)
            ops->push_back((*p->ops)[i]->Copy());
    }
}

void CUvPath::GetBound(CUvRectF &rect)
{
    if (ops)
    {
        std::vector<CUvPointF> *pts = nullptr;
        for (size_t i = 0; i < ops->size(); i++)
        {
            switch ((*ops)[i]->verb)
            {
            case uvMoveTo_Verbs:
                if (i == 0)
                    rect.Set(*(((CUvPointF*)(*ops)[i]->pShape)));
                else
                    rect.Add(*(((CUvPointF*)(*ops)[i]->pShape)));
                break;
            case uvLineTo_Verbs:
                if (i == 0)
                    rect.Set(*(((CUvPointF*)(*ops)[i]->pShape)));
                else
                    rect.Add(*(((CUvPointF*)(*ops)[i]->pShape)));
                break;
            case uvQuadTo_Verbs:
                pts = &((CUvQuad*)(*ops)[i]->pShape)->points->points;
                if (i == 0)
                {
                    if(pts->size() > 1) rect.Set((*pts)[1]);
                }
                else
                {
                    if (pts->size() > 1) rect.Add((*pts)[1]);
                    if(pts->size() > 2) rect.Add((*pts)[2]);
                }
                break;
            case uvCubicTo_Verbs:
                pts = &((CUvCubic*)(*ops)[i]->pShape)->points->points;
                if (i == 0)
                {
                    if (pts->size() > 1) rect.Set((*pts)[1]);
                }
                else
                {
                    if (pts->size() > 1) rect.Add((*pts)[1]);
                    if (pts->size() > 2) rect.Add((*pts)[2]);
                    if (pts->size() > 3) rect.Add((*pts)[3]);
                }
                break;
            case uvArcTo_Verbs:
                switch ((*ops)[i]->pShape->type)
                {
                    case uvAngleArc_ShapeType:
                    {
                        CUvAngleArc *pArc = (CUvAngleArc*)(*ops)[i]->pShape;
                        CUvPointF pt1(pArc->oval.left, pArc->oval.top);
                        CUvPointF pt2(pArc->oval.right, pArc->oval.bottom);
                        if (i == 0) rect.Set(pt1);
                        else rect.Add(pt1);
                        rect.Add(pt2);
                    }
                    break;
                    case uvTangentArc_ShapeType:
                    {
                        CUvTangentArc *pArc = (CUvTangentArc*)(*ops)[i]->pShape;
                        CUvPointF pt1(pArc->points[1].x - pArc->radius, pArc->points[1].y - pArc->radius);
                        CUvPointF pt2(pArc->points[1].x + pArc->radius, pArc->points[1].y + pArc->radius);
                        if (i == 0) rect.Set(pt1);
                        else rect.Add(pt1);
                    }
                    break;
                    case uvOvalArc_ShapeType:
                    {
                        CUvOvalArc *pArc = (CUvOvalArc*)(*ops)[i]->pShape;
                        CUvPointF pt1(pArc->x - pArc->rx, pArc->y - pArc->ry);
                        CUvPointF pt2(pArc->x + pArc->rx, pArc->y + pArc->ry);
                        if (i == 0) rect.Set(pt1);
                        else rect.Add(pt1);
                    }
                    break;
                }
                break;
            case uvAddPoly_Verbs:
            {
                CUvPoly *pPoly = (CUvPoly*)(*ops)[i]->pShape;
                if (pPoly->points && pPoly->points->points.size() > 1)
                {
                    for (size_t j = 0; j < pPoly->points->points.size(); j++)
                    {
                        if (i == 0 && j == 0)
                            rect.Set(pPoly->points->points[j]);
                        else 
                            rect.Add(pPoly->points->points[j]);
                    }
                }
            }
            break;
            case uvAddRectCW_Verbs:
            {
                CUvRectF *p = (CUvRectF*)(*ops)[i]->pShape;
                CUvPointF pt1(p->left, p->top);
                CUvPointF pt2(p->right, p->bottom);
                if (i == 0) rect.Set(pt1);
                else rect.Add(pt1);
                rect.Add(pt2);
            }
            break;
            case uvAddRectCCW_Verbs:
            {
                CUvRectF *p = (CUvRectF*)(*ops)[i]->pShape;
                CUvPointF pt1(p->left, p->top);
                CUvPointF pt2(p->right, p->bottom);
                if (i == 0) rect.Set(pt1);
                else rect.Add(pt1);
                rect.Add(pt2);
            }
            break;
            case uvAddOvalCW_Verbs:
            {
                CUvOval *p = (CUvOval*)(*ops)[i]->pShape;
                CUvPointF pt1(p->rect.left, p->rect.top);
                CUvPointF pt2(p->rect.right, p->rect.bottom);
                if (i == 0) rect.Set(pt1);
                else rect.Add(pt1);
                rect.Add(pt2);
            }
            break;
            case uvAddOvalCCW_Verbs:
            {
                CUvOval *p = (CUvOval*)(*ops)[i]->pShape;
                CUvPointF pt1(p->rect.left, p->rect.top);
                CUvPointF pt2(p->rect.right, p->rect.bottom);
                if (i == 0) rect.Set(pt1);
                else rect.Add(pt1);
                rect.Add(pt2);
            }
            break;
            case uvAddCircleCW_Verbs:
            {
                CUvCircle *p = (CUvCircle*)(*ops)[i]->pShape;
                CUvPointF pt1(p->center.x + p->radius, p->center.y + p->radius);
                CUvPointF pt2(p->center.x - p->radius, p->center.y - p->radius);
                if (i == 0) rect.Set(pt1);
                else rect.Add(pt1);
                rect.Add(pt2);
            }
            break;
            case uvAddCircleCCW_Verbs:
            {
                CUvCircle *p = (CUvCircle*)(*ops)[i]->pShape;
                CUvPointF pt1(p->center.x + p->radius, p->center.y + p->radius);
                CUvPointF pt2(p->center.x - p->radius, p->center.y - p->radius);
                if (i == 0) rect.Set(pt1);
                else rect.Add(pt1);
                rect.Add(pt2);
            }

            break;
            case uvAddArc_Verbs:
            {
                CUvAngleArc *pArc = (CUvAngleArc*)(*ops)[i]->pShape;
                CUvPointF pt1(pArc->oval.left, pArc->oval.top);
                CUvPointF pt2(pArc->oval.right, pArc->oval.bottom);
                if (i == 0) rect.Set(pt1);
                else rect.Add(pt1);
                rect.Add(pt2);
            }
            break;
            case uvAddPath_Verbs:
            {
                CUvPath *p = (CUvPath*)(*ops)[i]->pShape;
                CUvRectF rc;
                p->GetBound(rc);
                CUvPointF pt1(rc.left, rc.top);
                CUvPointF pt2(rc.right, rc.bottom);
                CUvPointF pt = pt1 * (*ops)[i]->matrix;
                if (i == 0)
                    rect.Set(pt);
                else
                    rect.Add(pt);
                pt = pt2 * (*ops)[i]->matrix;
                rect.Add(pt);
            }
            break;
            }
        }
    }
}

void CUvPath::moveTo(CUvPointF &point)
{
    CUvPathOp *pOp = NEW CUvPathOp(uvMoveTo_Verbs, &CUvPointFShape(point.x, point.y), true);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::moveTo(float x, float y)
{
    CUvPathOp *pOp = NEW CUvPathOp(uvMoveTo_Verbs, &CUvPointFShape(x, y), true);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}

void CUvPath::lineTo(CUvPointF &point)
{
    CUvPathOp *pOp = NEW CUvPathOp(uvLineTo_Verbs, &CUvPointFShape(point.x, point.y), true);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::lineTo(float x, float y)
{
    CUvPathOp *pOp = NEW CUvPathOp(uvLineTo_Verbs, &CUvPointFShape(x, y), true);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}

void CUvPath::quadTo(float x1, float y1, float x2, float y2)
{
    CUvQuad *p = NEW CUvQuad();
    p->points = NEW CUvPointFs();
    p->points->points.push_back(CUvPointF(.0f, .0f));
    p->points->points.push_back(CUvPointF(x1, y1));
    p->points->points.push_back(CUvPointF(x2, y2));

    CUvPathOp *pOp = NEW CUvPathOp(uvQuadTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::quadTo(CUvPointF &point1, CUvPointF &point2)
{
    CUvQuad *p = NEW CUvQuad();
    p->points = NEW CUvPointFs();
    p->points->points.push_back(CUvPointF(.0f, .0f));
    p->points->points.push_back(point1);
    p->points->points.push_back(point2);

    CUvPathOp *pOp = NEW CUvPathOp(uvQuadTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::cubicTo(float x1, float y1, float x2, float y2, float x3, float y3)
{
    CUvCubic *p = NEW CUvCubic();
    p->points = NEW CUvPointFs();
    p->points->points.push_back(CUvPointF(.0f, .0f));
    p->points->points.push_back(CUvPointF(x1, y1));
    p->points->points.push_back(CUvPointF(x2, y2));
    p->points->points.push_back(CUvPointF(x3, y3));

    CUvPathOp *pOp = NEW CUvPathOp(uvCubicTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::cubicTo(CUvPointF &point1, CUvPointF &point2, CUvPointF &point3)
{
    CUvCubic *p = NEW CUvCubic();
    p->points = NEW CUvPointFs();
    p->points->points.push_back(CUvPointF(.0f, .0f));
    p->points->points.push_back(point1);
    p->points->points.push_back(point2);
    p->points->points.push_back(point3);

    CUvPathOp *pOp = NEW CUvPathOp(uvCubicTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::arcTo(CUvRectF &oval, float startAngle, float sweepAngle)
{
    CUvAngleArc *p = NEW CUvAngleArc();
    p->oval = oval;
    p->startAngle = startAngle;
    p->sweepAngle = sweepAngle;

    CUvPathOp *pOp = NEW CUvPathOp(uvArcTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::arcTo(float x1, float y1, float x2, float y2, float radius)
{
    CUvTangentArc *p = NEW CUvTangentArc();
    p->points[0] = CUvPointF(.0f, .0f);
    p->points[1] = CUvPointF(x1, y1);
    p->points[2] = CUvPointF(x2, y2);

    CUvPathOp *pOp = NEW CUvPathOp(uvArcTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
// 
void CUvPath::arcTo(CUvPointF &point1, CUvPointF &point2, float radius)
{
    CUvTangentArc *p = NEW CUvTangentArc();
    p->points[0] = CUvPointF(.0f, .0f);
    p->points[1] = point1;
    p->points[2] = point2;
    p->radius = radius;

    CUvPathOp *pOp = NEW CUvPathOp(uvArcTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}

void CUvPath::ovalArcTo(float rx, float ry, double xRotate, float endX, float endY, bool largeArc, bool clockwise)
{
    CUvOvalArc *p = NEW CUvOvalArc();
    p->rx = rx;
    p->ry = ry;
    p->xRotate = xRotate;
    p->x = endX;
    p->y = endY;
    p->bLargeArc = largeArc;
    p->bClockwise = clockwise;

    CUvPathOp *pOp = NEW CUvPathOp(uvArcTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}

void CUvPath::addPoly(const CUvPointF pts[], int count, bool bNewStart, bool bPolygon)
{
    CUvPoly *pShape = NEW CUvPoly();
    pShape->points = NEW CUvPointFs();
    pShape->bPolygon = bPolygon;

    for (int i = 0; i < count; i++)
        pShape->points->points.push_back(pts[i]);

    //CUvPathOp *pOp = NEW CUvPathOp(bPolygon ? uvAddPolygon_Verbs : uvAddPolyline_Verbs, pShape, false);
    CUvPathOp *pOp = NEW CUvPathOp(uvAddPoly_Verbs, pShape, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
    pOp->bAddLineFromLastPoint = !bNewStart;
}
void CUvPath::addPoly(std::vector<CUvPointF> &points, bool bNewStart, bool bPolygon)
{
    CUvPoly *pShape = NEW CUvPoly();
    pShape->points = NEW CUvPointFs();
    pShape->bPolygon = bPolygon;

    for (int i = 0; i < points.size(); i++)
        pShape->points->points.push_back(points[i]);

    //CUvPathOp *pOp = NEW CUvPathOp(bPolygon ? uvAddPolygon_Verbs : uvAddPolyline_Verbs, pShape, false);
    CUvPathOp *pOp = NEW CUvPathOp(uvAddPoly_Verbs, pShape, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
    pOp->bAddLineFromLastPoint = !bNewStart;
}
void CUvPath::addArc(const CUvRectF &oval, double startAngle, double sweepAngle)
{
    CUvAngleArc *p = NEW CUvAngleArc();
    p->oval = oval;
    p->startAngle = startAngle;
    p->sweepAngle = sweepAngle;

    CUvPathOp *pOp = NEW CUvPathOp(uvArcTo_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::addCircle(const CUvPointF &center, float radius, bool clockwise)
{
    CUvCircle *p = NEW CUvCircle();
    p->center = center;
    p->radius = radius;
    p->bClockwise = clockwise;

    CUvPathOp *pOp = NEW CUvPathOp(clockwise ? uvAddCircleCW_Verbs : uvAddCircleCCW_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::addRect(const CUvRectF &rect, bool clockwise)
{
    CUvRectFShape *p = NEW CUvRectFShape();
    ((CUvRectF*)p)->CopyFrom((CUvRectF*)&rect);

    CUvPathOp *pOp = NEW CUvPathOp(clockwise ? uvAddRectCW_Verbs : uvAddRectCCW_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::addOval(const CUvRectF &rect, bool clockwise)
{
    CUvOval *p = NEW CUvOval();
    p->rect = rect;
    p->bClockwise = clockwise;

    CUvPathOp *pOp = NEW CUvPathOp(clockwise ? uvAddOvalCW_Verbs : uvAddOvalCCW_Verbs, p, false);
    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::addPath(const CUvPath& src, float dx, float dy, bool bCopy)
{
    CUvPath *p;
    if (bCopy)
    {
        p = NEW CUvPath();
        p->CopyFrom((CUvPath*)&src);
    }
    else p = (CUvPath*)&src;

    CUvPathOp *pOp = NEW CUvPathOp(uvAddPath_Verbs, p, false);
    pOp->matrix.mat[0][2] = dx;
    pOp->matrix.mat[1][2] = dy;

    ops->push_back(pOp);
    pOp->pShape->parent = this;
}
void CUvPath::addPath(const CUvPath& src, CUvMatrix& mat, bool bCopy)
{
    CUvPath *p;
    if (bCopy)
    {
        p = NEW CUvPath();
        p->CopyFrom((CUvPath*)&src);
    }
    else p = (CUvPath*)&src;

    CUvPathOp *pOp = NEW CUvPathOp(uvAddPath_Verbs, p, false);
    pOp->matrix = mat;

    ops->push_back(pOp);
    pOp->pShape->parent = this;
}

void CUvPath::reset()
{
    if (ops)
    {
        for (size_t i = 0; i < ops->size(); i++)
            delete (*ops)[i];

        ops->clear();
        delete ops;
    }
}
