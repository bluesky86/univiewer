#pragma once
#include "Model.h"

class UNIV_API CUvColor
{
public:
    CUvColor();
    CUvColor(const char *hexstr);
    CUvColor(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a = 255);
    ~CUvColor();

    bool operator == (CUvColor& color);
    bool operator != (CUvColor& color);

    static CUvColor MakeColor(unsigned int i);
    unsigned int ToRgbaInt();  // byte sequence: RGBA
    unsigned int ToArgbInt(); // byte sequence: ARGB

public:
    unsigned char r, g, b, a;
};


