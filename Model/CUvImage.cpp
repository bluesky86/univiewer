#include "stdafx.h"
#include "CUvImage.h"
#include "DebugNew.h"



CUvImage::CUvImage()
    : href(nullptr), data_len(0), x(0), y(0), targetWidth(0), targetHeight(0), widthIsPercentage(false), heightIsPercentage(false), info(nullptr)
    , imageType(uvImageType_file), numStrips(0), numBits(0), numColors(0), palette(nullptr), imageWidth(0), imageHeight(0), encodingType(uvImageEncoding_none)
    , outNumBits(-1), bTiled(false), numTiles(0), numTilesPerWidth(0), tileWidth(0), tileHeight(0), jpegParams(nullptr), origin(uvTopLeft_Image), bFlipRGB(false)
    , colorEffectType(uvImageColorEffectType_none)

{
    type = uvImage_ShapeType;
}

CUvImage::~CUvImage()
{
    for(int i = 0; i< numStrips; i++)
        if (data[i]) delete data[i];
    data.clear();

    if (palette) delete[] palette;
}

CUvShape* CUvImage::Copy()
{
    CUvImage *p = NEW CUvImage();
    p->CopyFrom(this);

    return p;
}

void CUvImage::CopyFrom(CUvImage *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);

    href = p->href;

    numStrips = p->numStrips;
    int strips = (int)p->data.size();
    if (strips > 0 && p->data_len > 0)
    {
        // strips
        for (int i = 0; i < strips; i++)
        {
            CUvRasterStrip *pStrip = NEW CUvRasterStrip();
            pStrip->width = p->data[i]->width;
            CUvRasterStripBlock *pNewBlock = nullptr, *pPrevBlock = nullptr;
            CUvRasterStripBlock *pBlock = p->data[i]->pBlock;
            // blocks
            while (pBlock)
            {
                pNewBlock = NEW CUvRasterStripBlock();
                if (pStrip->pBlock == nullptr) pStrip->pBlock = pNewBlock;

                memcpy(pNewBlock, pBlock, pBlock->size);

                pNewBlock->size = pBlock->size;

                if (pPrevBlock != nullptr)
                    pPrevBlock->next = pNewBlock;

                pPrevBlock = pNewBlock;

                pBlock = pBlock->next;
            }
        }

        data_len = p->data_len;
    }

    if (p->palette && p->numColors > 0)
    {
        palette = NEW CUvColor[p->numColors];
        memcpy(palette, p->palette, p->numColors * sizeof(CUvColor));
    }

    imageType = p->imageType;
    encodingType = p->encodingType;

    imageWidth = p->imageWidth;
    imageHeight = p->imageHeight;

    x = p->x;
    y = p->y;
    targetWidth = p->targetWidth;
    targetHeight = p->targetHeight;
    widthIsPercentage = p->widthIsPercentage;
    heightIsPercentage = p->heightIsPercentage;

    outNumBits = p->outNumBits;

    bTiled = p->bTiled;
    numTiles = p->numTiles;
    numTilesPerWidth = p->numTiles;
    tileWidth = p->tileWidth;
    tileHeight = p->tileHeight;

    jpegParams = p->jpegParams;

    origin = p->origin;
    bFlipRGB = p->bFlipRGB;
}

void* CUvImage::GetInfo()
{
    return info;
}
void CUvImage::SetInfo(void* in)
{
    info = in;
}

void CUvImage::GetBound(CUvRectF &rect)
{
    rect.left = (float)x;
    rect.top = (float)y;
    rect.right = rect.left + targetWidth;
    rect.bottom = rect.top + targetHeight;
}
