#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>

#define UNIV_API __declspec(dllexport)

#define FILEPATH_MAX_LENGTH     1024

typedef int S32;
typedef unsigned int U32;
typedef short S16;
typedef unsigned short U16;
typedef char S8;
typedef unsigned char U8;

typedef enum _UvStrokeCap
{
    uvButt_StrokeCap = 0,
    uvRound_StrokeCap,
    uvSquare_StrokeCap,
} UvStrokeCap;

typedef enum _UvStrokeJoin
{
    uvMiter_StrokeJoin = 0,
    uvRound_StrokeJoin,
    uvBevel_StrokeJoin,
} UvStrokeJoin;

typedef enum _UvStrokeFill
{
    uvFill_StrokeFill = 0,       // fill inside
    uvStroke_StrokeFill,
    uvStrokeAndFill_StrokeFill,
    //uvFromParent_StrokeFill, // inherit from parent
    //uvFromRefer_StrokeFill, // using reference for CUvRefer object
    uvNone_StrokeFill,
} UvStrokeFill;

typedef enum _UvDashStyle
{
    uvSolid_DashStyle = 0,
    uvDash_DashStyle = -1, // { 5, 3 }
    uvDot_DashStyle = -2,  // { 1, 1 }
    uvDashDot_DashStyle = -3,   // { 3, 1, 1, 1 }
    uvDashDotDot_DashStyle = -4,  // { 3, 1, 1, 1, 1, 1 }
    //uvNone_DashStyle = -5,
    uvDocCustom_DashStyle = 100000,  // custom dash style shared in docment level
    uvPageCustom_DashStyle = 200000, // custom dash style shared in page level
                                     // uvCustom_DashStyle = 5, // all other values are the costom dash style, and the value is the number of elements
} UvDashStyle;

typedef enum _UvTextEncoding
{
    uvUTF8_TextEncoding,
    uvUTF16_TextEncoding,
    uvUTF32_TextEncoding,
    uvGlyphID_TextEncoding,
} UvTextEncoding;

typedef enum _UvTextAlign
{
    uvLeft_TextAlign,
    uvCenter_TextAlign,
    uvRight_TextAlign,
} UvTextAlign;

typedef enum _uvShapeType
{
    uvShape_ShapeType,
    uvPoint_ShapeType,
    uvPointF_ShapeType,
    uvPoints_ShapeType,
    uvPointFs_ShapeType,
    uvLine_ShapeType,
    uvRect_ShapeType,
    uvRectF_ShapeType,
    uvPath_ShapeType,
    uvQuad_ShapeType,
    uvCubic_ShapeType,
    uvAngleArc_ShapeType,
    uvTangentArc_ShapeType,
    uvOvalArc_ShapeType,
    uvCircle_ShapeType,
    uvCircleArc_ShapeType,
    uvOval_ShapeType,
    uvPoly_ShapeType,
    uvRefer_ShapeType,
    uvGroup_ShapeType,
    uvPage_ShapeType,
    uvDoc_ShapeType,

    uvImage_ShapeType,
    uvText_ShapeType,

} UvShapeType;

typedef enum _uvPathVerbs
{
    uvNone_Verbs,
    uvMoveTo_Verbs,
    uvLineTo_Verbs,
    uvQuadTo_Verbs,
    uvCubicTo_Verbs,
    uvArcTo_Verbs,
    uvConicTo_Verbs,
    //uvAngleArcTo_Verbs,
    //uvTangentArcTo_Verbs,
    //uvOvalArcTo_Verbs,
    uvClose_Verbs,
    uvAddRectCW_Verbs, // move to top-left of rect, draw clock-wise
    uvAddRectCCW_Verbs, // move to top-left of rect, draw counter-clock-wise
    uvAddOvalCW_Verbs, // move to (right, centerY) of rect, draw clock-wise
    uvAddOvalCCW_Verbs, // move to (right, centerY) of rect, draw counter-clock-wise
    uvAddCircleCW_Verbs, // move to (x + radius, y), draw clock-wise
    uvAddCircleCCW_Verbs, // move to (x + radius, y), draw counter-clock-wise
    uvAddArc_Verbs, // move to the point of start angle
    uvAddPoly_Verbs,
    //uvAddPolyline_Verbs,
    //uvAddPolygon_Verbs,
    uvAddPath_Verbs,
} UvPathVerbs;

typedef enum
{
    UV_ERROR_NONE = 0,
    UV_ERROR_LOADER_CONFIG_FAILED,
    UV_ERROR_CHAR_CONVERT_FAILED,
    UV_ERROR_LOAD_FILE_FAILED,
    UV_ERROR_LOADER_CONFIG_EXT_NOT_DEFINED,
    UV_ERROR_LOADER_API_LOAD_FAILED,
    UV_ERROR_FAILED_CREATE_DEVICE_CONTEXT,
    UV_ERROR_NO_LOADER_CAN_USED_FOR_THIS_FILE,
    UV_ERROR_API_OPENDOC_DIDNOT_LOADED,
    UV_ERROR_FAIL_TO_CONVERT_FILE,
    UV_ERROR_CONVERT_API_NOT_FOUND,

    UV_ERROR_LOADER_DEFINED,
    UV_ERROR_BEFORE_ERROR_LIST_LOADED,

    UV_ERROR_LOAD_FONT_CONFIG_FAILED,
}UV_ERROR_CODE_ENUM;

class CUvText;

UNIV_API bool uvModelInit( const wchar_t* dllDir );
UNIV_API void uvModelClose();
// CID Code mapping
UNIV_API int CharCodeMapping( CUvText *pUvText, unsigned char *dest );
UNIV_API int CharCodeMapping( CUvText *pUvText, unsigned char *dest );
UNIV_API int CharCodeMapping( CUvText *pUvText, unsigned char *dest );
UNIV_API unsigned int GetCodepageSignature( int codepage );
UNIV_API const char* GetCodepageBcp47( int codepage );

