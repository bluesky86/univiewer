#pragma once
#include "Model.h"
#include "CUvShape.h"
#include "CUvPoint.h"

class UNIV_API CUvRect
{
public:
    CUvRect();
    ~CUvRect();

    CUvRect* Copy();
    void CopyFrom(CUvRect *p);

    void Set(CUvPoint &pt);
    void Set(CUvPoint pt[], int count);
    void Set(int _left, int _right, int _top, int _bottom);
    void Add(CUvPoint &pt);
    int Width();
    int Height();

public:
    int left, right, top, bottom;
};

//class UNIV_API CUvRectShape : public CUvShape, public CUvRect
//{
//public:
//    CUvRectShape();
//    ~CUvRectShape();
//
//    virtual CUvShape* Copy();
//    virtual void CopyFrom(CUvRectShape *p);
//
//};

class UNIV_API CUvRectF
{
public:
    CUvRectF();
    ~CUvRectF();

    CUvRectF* Copy();
    void CopyFrom(CUvRectF *p);

    void Set(CUvPointF &pt);
    void Set(CUvPointF pt[], int count);
    void Set(float _left, float _right, float _top, float _bottom);
    void Add(CUvPointF &pt);
    void AddX(float v);
    void AddY(float v);
    void AddXY(float x, float y);
    float Width();
    float Height();

public:
    float left, right, top, bottom;
};

class UNIV_API CUvRectFShape : public CUvShape, public CUvRectF
{
public:
    CUvRectFShape();
    ~CUvRectFShape();

    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvRectFShape *p);
    virtual void GetBound(CUvRectF &rect);

};

