#pragma once

#include "Model.h"
#include "DebugNew.h"
#include "Vector.h"


template <typename S, typename D>
class UNIV_API CUvMap
{
public:
    class CUvMapEle
    {
    public:
        CUvMapEle() {};
        ~CUvMapEle() {};

    public:
        S first;
        D second;
    };

    typedef CUvMapEle* iterator;

public:

    CUvMap();
    ~CUvMap();
    CUvMap(const CUvMap<S, D> &m);
    void CopyFrom(const CUvMap<S,D> *pMap);

    U32 capacity() const;
    U32 size() const;
    bool empty() const;
    iterator begin();
    iterator end();
    iterator front();
    iterator back();
    //void push_back(const CUvMapEle *value);
    void push_back(iterator value);
    void pop_back();
    iterator find(S v);
    S32 findIndex(S v);
    void pop(S v);

    void reserve(U32 capacity);
    void resize(U32 size);

    void makeReverse();

    iterator operator[](U32 index);
    CUvMap<S, D>& operator = (const CUvMap<S, D> & v);
    void clear();

private:
    CVector<CUvMapEle*> elems;
};

template <typename S, typename D>
CUvMap<S, D>::CUvMap()
{
}

template <typename S, typename D>
CUvMap<S, D>::~CUvMap()
{
    clear();
}

template <typename S, typename D>
void CUvMap<S, D>::clear()
{
    for (U32 i = 0; i < elems.size(); i++)
        delete elems[i];
    elems.clear();
}

template <typename S, typename D>
CUvMap<S, D>::CUvMap(const CUvMap<S, D> &m)
{
    elems = m.elems;
}

template <typename S, typename D>
void CUvMap<S, D>::CopyFrom(const CUvMap<S, D> *pMap)
{
    if (pMap)
    {
        elems.resize(pMap->capacity());
        for (U32 i = 0; i < pMap->size(); i++)
        {
            CUvMapEle *pEle = NEW CUvMapEle();
            pEle->first = (*pMap)[i]->first;
            pEle->second = (*pMap)[i]->second;
            elems.push_back(pEle);
        }
    }
}

template <typename S, typename D>
unsigned int CUvMap<S, D>::capacity()const
{
    return elems.capacity();
}

template <typename S, typename D>
bool CUvMap<S, D>::empty() const
{
    return elems.empty();
}

template <typename S, typename D>
U32 CUvMap<S, D>::size()const
{
    return elems.size();
}

template <typename S, typename D>
typename CUvMap<S, D>::iterator CUvMap<S, D>::begin()
{
    return elems.begin();
}

template <typename S, typename D>
typename CUvMap<S, D>::iterator CUvMap<S, D>::end()
{
    return elems.end();
}

template <typename S, typename D>
typename CUvMap<S, D>::iterator CUvMap<S, D>::front()
{
    return elems.front();
}

template <typename S, typename D>
typename CUvMap<S, D>::iterator CUvMap<S, D>::back()
{
    return elems.back();
}

template <typename S, typename D>
void CUvMap<S, D>::push_back(CUvMap<S, D>::iterator v)
{
    elems.push_back(v);
}

template <typename S, typename D>
void CUvMap<S, D>::pop_back()
{
    if (elems.back())
    {
        delete elems.back();
        elems.pop_back();
    }
}

template <typename S, typename D>
typename CUvMap<S, D>::iterator CUvMap<S, D>::find(S v)
{
    S32 index = findIndex(v);
    if (index == -1) return nullptr;
    return elems[index];
}

template <typename S, typename D>
S32 CUvMap<S, D>::findIndex(S v)
{
    for (U32 i = 0; i < elems.size(); i++)
    {
        if (elems[i]->first == v)
            return (S32)i;
    }
    return -1;
}

template <typename S, typename D>
void CUvMap<S, D>::pop(S v)
{
    S32 index = findIndex(v);
    if (index == -1) return;

    elems.pop(index);
}

template <typename S, typename D>
void CUvMap<S, D>::reserve(U32 capacity)
{
    elems.reserve(capacity);
}

template <typename S, typename D>
void CUvMap<S, D>::resize(U32 size)
{
    elems.resize();
}

template <typename S, typename D>
void CUvMap<S, D>::makeReverse()
{
    elems.makeReverse();
}

template <typename S, typename D>
typename CUvMap<S, D>::iterator CUvMap<S, D>::operator[](U32 index)
{
    return elems[index];
}

template <typename S, typename D>
CUvMap<S, D>& CUvMap<S, D>::operator = (const CUvMap<S, D> & v)
{
    elems = v.elems;

    return *this;
}
