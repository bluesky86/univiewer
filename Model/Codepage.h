#pragma once
#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class
#include "Model.h"
#include <map>
#include <string>

typedef enum
{
    CODEPAGE_NONE                           =       0,       // IBM037,      IBM EBCDIC US-Canada 
    CODEPAGE_EBCDIC_US                      =       37,      // ,            Windows Symbol
    CODEPAGE_SYMBOL                         =       42,
    CODEPAGE_OEM_US                         =       437,      // IBM437,      OEM United States 
    CODEPAGE_EBCDIC_INTERNATIONAL           =       500,      // IBM500,      IBM EBCDIC International 
    CODEPAGE_ARABIC_ASMO                    =       708,      // ASMO-708,    Arabic (ASMO 708) 
    CODEPAGE_ARABIC_BCON                    =       709,      // ,            Arabic (ASMO-449+, BCON V4) 
    CODEPAGE_ARABIC_TRANSPARENT             =       710,      // ,            Arabic - Transparent Arabic 
    CODEPAGE_DOS_ARABIC_TRANSPARENT         =       720,      // DOS-720,     Arabic (Transparent ASMO); Arabic (DOS) 
    CODEPAGE_DOS_GREEK                      =       737,      // ibm737,      OEM Greek (formerly 437G); Greek (DOS) 
    CODEPAGE_DOS_BALTIC                     =       775,      // ibm775,      OEM Baltic; Baltic (DOS) 
    CODEPAGE_DOS_WESTERN_EUROPEAN           =       850,      // ibm850,      OEM Multilingual Latin 1; Western European (DOS) 
    CODEPAGE_DOS_CENTRAL_EUROPEAN           =       852,      // ibm852,      OEM Latin 2; Central European (DOS) 
    CODEPAGE_DOS_RUSSIAN                    =       855,      // IBM855,      OEM Cyrillic (primarily Russian) 
    CODEPAGE_DOS_TURKISH                    =       857,      // ibm857,      OEM Turkish; Turkish (DOS) 
    CODEPAGE_DOS_WESTERN_EUROPEAN_EURO      =       858,      // IBM00858,    OEM Multilingual Latin 1 + Euro symbol 
    CODEPAGE_DOS_PORTUGUESE                 =       860,      // IBM860,      OEM Portuguese; Portuguese (DOS) 
    CODEPAGE_DOS_ICELANDIC                  =       861,      // ibm861,      OEM Icelandic; Icelandic (DOS) 
    CODEPAGE_DOS_HEBREW                     =       862,      // DOS-862,     OEM Hebrew; Hebrew (DOS) 
    CODEPAGE_DOS_FRENCH_CANADIAN            =       863,      // IBM863,      OEM French Canadian; French Canadian (DOS) 
    CODEPAGE_DOS_ARABIC                     =       864,      // IBM864,      OEM Arabic; Arabic (864) 
    CODEPAGE_DOS_NORDIC                     =       865,      // IBM865,      OEM Nordic; Nordic (DOS) 
    CODEPAGE_DOS_CYRILLIC                   =       866,      // cp866,       OEM Russian; Cyrillic (DOS) 
    CODEPAGE_DOS_GREEK_MODERN               =       869,      // ibm869,      OEM Modern Greek; Greek, Modern (DOS) 
    CODEPAGE_EBCDIC_MULTILINGUAL            =       870,      // IBM870,      IBM EBCDIC Multilingual/ROECE (Latin 2); IBM EBCDIC Multilingual Latin 2 
    CODEPAGE_THAI                           =       874,      // windows-874, ANSI/OEM Thai (same as 28605, ISO 8859-15); Thai (Windows) 
    CODEPAGE_EBCDIC_GREEK_MODERN            =       875,      // cp875,       IBM EBCDIC Greek Modern 
    CODEPAGE_JAPANESE                       =       932,      // shift_jis,   ANSI/OEM Japanese; Japanese (Shift-JIS) 
    CODEPAGE_GB2312                         =       936,      // gb2312,      ANSI/OEM Simplified Chinese (PRC, Singapore); Chinese Simplified (GB2312) 
    CODEPAGE_HANGEUL                        =       949,      // ks_c_5601-1987, ANSI/OEM Korean (Unified Hangul Code) 
    CODEPAGE_CHINESEBIG5                    =       950,      // big5,        ANSI/OEM Traditional Chinese (Taiwan; Hong Kong SAR, PRC); Chinese Traditional (Big5) 
    CODEPAGE_EBCDIC_TURKISH_EURO            =       1026,      // IBM1026,     IBM EBCDIC Turkish (Latin 5) ???20905 + Euro symbol???
    CODEPAGE_EBCDIC_LATIN_1                 =       1047,      // IBM01047,    IBM EBCDIC Latin 1/Open System 
    CODEPAGE_EBCDIC_US_CANADA_EURO          =       1140,      // IBM01140,    IBM EBCDIC US-Canada (037 + Euro symbol); IBM EBCDIC (US-Canada-Euro) 
    CODEPAGE_EBCDIC_GERMANY_EURO            =       1141,      // IBM01141,    IBM EBCDIC Germany (20273 + Euro symbol); IBM EBCDIC (Germany-Euro) 
    CODEPAGE_EBCDIC_DENMARK_NORWAY_EURO     =       1142,      // IBM01142,    IBM EBCDIC Denmark-Norway (20277 + Euro symbol); IBM EBCDIC (Denmark-Norway-Euro) 
    CODEPAGE_EBCDIC_FINLAND_SWEDEN_EURO     =       1143,      // IBM01143,    IBM EBCDIC Finland-Sweden (20278 + Euro symbol); IBM EBCDIC (Finland-Sweden-Euro) 
    CODEPAGE_EBCDIC_ITALY_EURO              =       1144,      // IBM01144,    IBM EBCDIC Italy (20280 + Euro symbol); IBM EBCDIC (Italy-Euro) 
    CODEPAGE_EBCDIC_SPAIN_EURO              =       1145,      // IBM01145,    IBM EBCDIC Latin America-Spain (20284 + Euro symbol); IBM EBCDIC (Spain-Euro) 
    CODEPAGE_EBCDIC_UNITED_KINGDOM_EURO     =       1146,      // IBM01146,    IBM EBCDIC United Kingdom (20285 + Euro symbol); IBM EBCDIC (UK-Euro) 
    CODEPAGE_EBCDIC_FRANCE_EURO             =       1147,      // IBM01147,    IBM EBCDIC France (20297 + Euro symbol); IBM EBCDIC (France-Euro) 
    CODEPAGE_EBCDIC_INTERNATIONAL_EURO      =       1148,      // IBM01148,    IBM EBCDIC International (500 + Euro symbol); IBM EBCDIC (International-Euro) 
    CODEPAGE_EBCDIC_ICELANDIC_EURO          =       1149,      // IBM01149,    IBM EBCDIC Icelandic (20871 + Euro symbol); IBM EBCDIC (Icelandic-Euro) 
    CODEPAGE_UNICODE_UTF16_LE               =       1200,      // utf-16,      Unicode UTF-16, little endian byte order (BMP of ISO 10646); available only to managed applications 
    CODEPAGE_UNICODE_UTF16_BE               =       1201,      // unicodeFFFE,    Unicode UTF-16, big endian byte order; available only to managed applications 
    CODEPAGE_WINDOWS31EASTERNEUROPEAN       =       1250,     // windows-1250,   ANSI Central European; Central European (Windows) 
    CODEPAGE_WINDOWS31SOVIETUNION           =       1251,     // windows-1251,   ANSI Cyrillic; Cyrillic (Windows) 
    CODEPAGE_WESTERN                        =       1252,     // windows-1252,   ANSI Latin 1; Western European (Windows) 
    CODEPAGE_GREEK                          =       1253,     // windows-1253,   ANSI Greek; Greek (Windows) 
    CODEPAGE_TURKISH                        =       1254,     // windows-1254,   ANSI Turkish; Turkish (Windows) 
    CODEPAGE_HEBREWANSI                     =       1255,     // windows-1255,   ANSI Hebrew; Hebrew (Windows) 
    CODEPAGE_ARABICANSI                     =       1256,     // windows-1256,   ANSI Arabic; Arabic (Windows) 
    CODEPAGE_BALTIC                         =       1257,     // windows-1257,   ANSI Baltic; Baltic (Windows) 
    CODEPAGE_VIETNAMESE                     =       1258,     // windows-1258,   ANSI/OEM Vietnamese; Vietnamese (Windows) 
    CODEPAGE_JOHAB                          =       1361,      // Johab,          Korean (Johab) 
    CODEPAGE_MAC_WESTERN_EUROPEAN           =      10000,      // macintosh,      MAC Roman; Western European (Mac) 
    CODEPAGE_MAC_JAPANESE                   =      10001,      // x-mac-japanese, Japanese (Mac) 
    CODEPAGE_MAC_CHINESE_TRADITIONAL        =      10002,      // x-mac-chinesetrad, MAC Traditional Chinese (Big5); Chinese Traditional (Mac) 
    CODEPAGE_MAC_KOREAN                     =      10003,      // x-mac-korean,   Korean (Mac) 
    CODEPAGE_MAC_ARABIC                     =      10004,      // x-mac-arabic,   Arabic (Mac) 
    CODEPAGE_MAC_HEBREW                     =      10005,      // x-mac-hebrew,   Hebrew (Mac) 
    CODEPAGE_MAC_GREEK                      =      10006,      // x-mac-greek,    Greek (Mac) 
    CODEPAGE_MAC_CYRILLIC                   =      10007,      // x-mac-cyrillic, Cyrillic (Mac) 
    CODEPAGE_MAC_CHINESE_SIMPLIFIED         =      10008,      // x-mac-chinesesimp, MAC Simplified Chinese (GB 2312); Chinese Simplified (Mac) 
    CODEPAGE_MAC_ROMANIAN                   =      10010,      // x-mac-romanian, Romanian (Mac) 
    CODEPAGE_MAC_UKRAINIAN                  =      10017,      // x-mac-ukrainian, Ukrainian (Mac) 
    CODEPAGE_MAC_THAI                       =      10021,      // x-mac-thai,     Thai (Mac) 
    CODEPAGE_MAC_CENTRAL_EUROPEAN           =      10029,      // x-mac-ce,       MAC Latin 2; Central European (Mac) 
    CODEPAGE_MAC_ICELANDIC                  =      10079,      // x-mac-icelandic, Icelandic (Mac) 
    CODEPAGE_MAC_TURKISH                    =      10081,      // x-mac-turkish,  Turkish (Mac) 
    CODEPAGE_MAC_CROATIAN                   =      10082,      // x-mac-croatian, Croatian (Mac) 
    CODEPAGE_UNICODE_UTF32_LE               =      12000,      // utf-32,         Unicode UTF-32, little endian byte order; available only to managed applications 
    CODEPAGE_UNICODE_UTF32_BE               =      12001,      // utf-32BE,       Unicode UTF-32, big endian byte order; available only to managed applications 
    CODEPAGE_CNS_CHINESE_TRADITIONAL        =      20000,      // x-Chinese_CNS,  CNS Taiwan; Chinese Traditional (CNS) 
    CODEPAGE_TCA_TAIWAN                     =      20001,      // x-cp20001,      TCA Taiwan 
    CODEPAGE_ETEN_CHINESE_TRADITIONAL       =      20002,      // x_Chinese-Eten, Eten Taiwan; Chinese Traditional (Eten) 
    CODEPAGE_IBM5550_TAIWAN                 =      20003,      // x-cp20003,      IBM5550 Taiwan 
    CODEPAGE_TELETEXT_TAIWAN                =      20004,      // x-cp20004,      TeleText Taiwan 
    CODEPAGE_WANG_TAIWAN                    =      20005,      // x-cp20005,      Wang Taiwan 
    CODEPAGE_IA5_WESTERN_EUROPEAN           =      20105,      // x-IA5,          IA5 (IRV International Alphabet No. 5, 7-bit); Western European (IA5) 
    CODEPAGE_IA5_GERMAN                     =      20106,      // x-IA5-German,   IA5 German (7-bit) 
    CODEPAGE_IA5_SWEDISH                    =      20107,      // x-IA5-Swedish,  IA5 Swedish (7-bit) 
    CODEPAGE_IA5_NORWEGIAN                  =      20108,      // x-IA5-Norwegian, IA5 Norwegian (7-bit) 
    CODEPAGE_ASCII                          =      20127,      // us-ascii,       US-ASCII (7-bit) 
    CODEPAGE_T61                            =      20261,      // x-cp20261,      T.61 
    CODEPAGE_ISO_6937                       =      20269,      // x-cp20269,      ISO 6937 Non-Spacing Accent 
    CODEPAGE_EBCDIC_GERMANY                 =      20273,      // IBM273,         IBM EBCDIC Germany 
    CODEPAGE_EBCDIC_DENMARK_NORWAY          =      20277,      // IBM277,         IBM EBCDIC Denmark-Norway 
    CODEPAGE_EBCDIC_FINLAND_SWEDEN          =      20278,      // IBM278,         IBM EBCDIC Finland-Sweden 
    CODEPAGE_EBCDIC_ITALY                   =      20280,      // IBM280,         IBM EBCDIC Italy 
    CODEPAGE_EBCDIC_SPAIN                   =      20284,      // IBM284,         IBM EBCDIC Latin America-Spain 
    CODEPAGE_EBCDIC_UNITED_KINGDOM          =      20285,      // IBM285,         IBM EBCDIC United Kingdom 
    CODEPAGE_EBCDIC_JAPANESE_KATAKANA_EXTENDED =   20290,      // IBM290,         IBM EBCDIC Japanese Katakana Extended   ???differences to 50930???
    CODEPAGE_EBCDIC_FRANCE                  =      20297,      // IBM297,         IBM EBCDIC France 
    CODEPAGE_EBCDIC_ARABIC                  =      20420,      // IBM420,         IBM EBCDIC Arabic 
    CODEPAGE_EBCDIC_GREEK                   =      20423,      // IBM423,         IBM EBCDIC Greek 
    CODEPAGE_EBCDIC_HEBREW                  =      20424,      // IBM424,         IBM EBCDIC Hebrew 
    CODEPAGE_EBCDIC_KOREAN_EXTENDED         =      20833,      // x-EBCDIC-KoreanExtended, IBM EBCDIC Korean Extended ???differences to 50933???
    CODEPAGE_EBCDIC_THAI                    =      20838,      // IBM-Thai,       IBM EBCDIC Thai 
    CODEPAGE_KOI8_R                         =      20866,      // koi8-r,         Russian (KOI8-R); Cyrillic (KOI8-R) 
    CODEPAGE_EBCDIC_ICELANDIC               =      20871,      // IBM871,         IBM EBCDIC Icelandic 
    CODEPAGE_EBCDIC_CYRILLIC_RUSSIAN        =      20880,      // IBM880,         IBM EBCDIC Cyrillic Russian 
    CODEPAGE_EBCDIC_TURKISH                 =      20905,      // IBM905,         IBM EBCDIC Turkish 
    CODEPAGE_EBCDIC_LATIN_1_EURO            =      20924,      // IBM00924,       IBM EBCDIC Latin 1/Open System (1047 + Euro symbol) 
    CODEPAGE_EUC_JP                         =      20932,      // EUC-JP,         Japanese (JIS 0208-1990 and 0121-1990) 
    CODEPAGE_GB2312_80                      =      20936,      // x-cp20936,      Simplified Chinese (GB2312); Chinese Simplified (GB2312-80) 
    CODEPAGE_KOREAN_WANSUNG                 =      20949,      // x-cp20949,      Korean Wansung 
    CODEPAGE_EBCDIC_CYRILLIC_SERBIAN_BULGARIAN =   21025,      // cp1025,         IBM EBCDIC Cyrillic Serbian-Bulgarian 
    CODEPAGE_21027                          =      21027,      // (deprecated) 
    CODEPAGE_KOI8_U                         =      21866,      // koi8-u,         Ukrainian (KOI8-U); Cyrillic (KOI8-U) 
    CODEPAGE_ISO_8859_1_WESTERN_EUROPEAN    =      28591,      // iso-8859-1,     ISO 8859-1 Latin 1; Western European (ISO) 
    CODEPAGE_ISO_8859_2_CENTRAL_EUROPEAN    =      28592,      // iso-8859-2,     ISO 8859-2 Central European; Central European (ISO) 
    CODEPAGE_ISO_8859_3_LATIN_3             =      28593,      // iso-8859-3,     ISO 8859-3 Latin 3 
    CODEPAGE_ISO_8859_4_BALTIC              =      28594,      // iso-8859-4,     ISO 8859-4 Baltic 
    CODEPAGE_ISO_8859_5_CYRILLIC            =      28595,      // iso-8859-5,     ISO 8859-5 Cyrillic 
    CODEPAGE_ISO_8859_6_ARABIC              =      28596,      // iso-8859-6,     ISO 8859-6 Arabic 
    CODEPAGE_ISO_8859_7_GREEK               =      28597,      // iso-8859-7,     ISO 8859-7 Greek 
    CODEPAGE_ISO_8859_8_HEBREW_VISUAL       =      28598,      // iso-8859-8,     ISO 8859-8 Hebrew; Hebrew (ISO-Visual) 
    CODEPAGE_ISO_8859_9_TURKISH             =      28599,      // iso-8859-9,     ISO 8859-9 Turkish 
    CODEPAGE_ISO_8859_10_NORDIC             =      28600,      // iso-8859-10,    ISO 8859-10 Nordic*** 
    CODEPAGE_ISO_8859_13_ESTONIAN           =      28603,      // iso-8859-13,    ISO 8859-13 Estonian 
    CODEPAGE_ISO_8859_14_CELTIC             =      28604,      // iso-8859-14,    ISO 8859-14 Celtic*** 
    CODEPAGE_ISO_8859_15_LATIN_9            =      28605,      // iso-8859-15,    ISO 8859-15 Latin 9 
    CODEPAGE_ISO_8859_16_LATIN_10           =      28606,      // iso-8859-16,    ISO 8859-16 Latin 10*** 
    CODEPAGE_EUROPA_3                       =      29001,      // x-Europa,       Europa 3 
    CODEPAGE_ISO_8859_8_HEBREW_LOGICAL      =      38598,      // iso-8859-8-i,   ISO 8859-8 Hebrew; Hebrew (ISO-Logical) 
    CODEPAGE_ISO_2022_JAPANESE              =      50220,      // iso-2022-jp,    ISO 2022 Japanese with no halfwidth Katakana; Japanese (JIS) 
    CODEPAGE_ISO_2022_JAPANESE_HALFWIDTH_KATAKANA= 50221,      // csISO2022JP,    ISO 2022 Japanese with halfwidth Katakana; Japanese (JIS-Allow 1 byte Kana) 
    CODEPAGE_ISO_2022_JAPANESE_JIS_X_0201_1989 =   50222,      // iso-2022-jp, ISO 2022 Japanese JIS X 0201-1989; Japanese (JIS-Allow 1 byte Kana - SO/SI) 
    CODEPAGE_ISO_2022_KOREAN                =      50225,      // iso-2022-kr,    ISO 2022 Korean 
    CODEPAGE_ISO_2022_CHINESE_SIMPLIFIED    =      50227,      // x-cp50227,      ISO 2022 Simplified Chinese; Chinese Simplified (ISO 2022) 
    CODEPAGE_ISO_2022_TRADITIONAL_CHINESE   =      50229,      // ,               ISO 2022 Traditional Chinese 
    CODEPAGE_EBCDIC_JAPANESE__KATAKANA__EXTENDED = 50930,      // ,               EBCDIC Japanese (Katakana) Extended  ???differences to 20290???
    CODEPAGE_EBCDIC_US_CANADA_AND_JAPANESE  =      50931,      // ,               EBCDIC US-Canada and Japanese 
    CODEPAGE_EBCDIC_KOREAN_EXTENDED_AND_KOREAN =   50933,      // ,               EBCDIC Korean Extended and Korean   ???differences to 20833???
    CODEPAGE_EBCDIC_SIMPLIFIED_CHINESE_EXTENDED =  50935,      // ,               EBCDIC Simplified Chinese Extended and Simplified Chinese 
    CODEPAGE_EBCDIC_SIMPLIFIED_CHINESE      =      50936,      // ,               EBCDIC Simplified Chinese 
    CODEPAGE_EBCDIC_TRADITIONAL_CHINESE     =      50937,      // ,               EBCDIC US-Canada and Traditional Chinese 
    CODEPAGE_EBCDIC_JAPANESE_LATIN_EXTENDED =      50939,      // ,               EBCDIC Japanese (Latin) Extended and Japanese 
    CODEPAGE_EUC_JAPANESE                   =      51932,      // euc-jp,         EUC Japanese 
    CODEPAGE_EUC_CHINESE_SIMPLIFIED         =      51936,      // EUC-CN,         EUC Simplified Chinese; Chinese Simplified (EUC) 
    CODEPAGE_EUC_KOREAN                     =      51949,      // euc-kr,         EUC Korean 
    CODEPAGE_EUC_CHINESE_TRADITIONAL        =      51950,      // ,               EUC Traditional Chinese 
    CODEPAGE_HZ_CHINESE_SIMPLIFIED          =      52936,      // hz-gb-2312,     HZ-GB2312 Simplified Chinese; Chinese Simplified (HZ) 
    CODEPAGE_GB18030                        =      54936,      // GB18030,        Windows XP and later: GB18030 Simplified Chinese (4 byte); Chinese Simplified (GB18030) 
    CODEPAGE_ISCII_DEVANAGARI               =      57002,      // x-iscii-de,     ISCII Devanagari 
    CODEPAGE_ISCII_BENGALI                  =      57003,      // x-iscii-be,     ISCII Bengali 
    CODEPAGE_ISCII_TAMIL                    =      57004,      // x-iscii-ta,     ISCII Tamil 
    CODEPAGE_ISCII_TELUGU                   =      57005,      // x-iscii-te,     ISCII Telugu 
    CODEPAGE_ISCII_ASSAMESE                 =      57006,      // x-iscii-as,     ISCII Assamese 
    CODEPAGE_ISCII_ORIYA                    =      57007,      // x-iscii-or,     ISCII Oriya 
    CODEPAGE_ISCII_KANNADA                  =      57008,      // x-iscii-ka,     ISCII Kannada 
    CODEPAGE_ISCII_MALAYALAM                =      57009,      // x-iscii-ma,     ISCII Malayalam 
    CODEPAGE_ISCII_GUJARATI                 =      57010,      // x-iscii-gu,     ISCII Gujarati 
    CODEPAGE_ISCII_PUNJABI                  =      57011,      // x-iscii-pa,     ISCII Punjabi 
    CODEPAGE_UNICODE_UTF7                   =      65000,      // utf-7,          Unicode (UTF-7) 
    CODEPAGE_UNICODE_UTF8                   =      65001,      // utf-8,          Unicode (UTF-8) 

}UV_CODEPAGE;

#define CODEPAGE_IS_FAR_EAST( c )          ( c == CODEPAGE_JAPANESE || c == CODEPAGE_GB2312 || c == CODEPAGE_CHINESEBIG5 || c == CODEPAGE_HANGEUL || c == CODEPAGE_JOHAB )

typedef enum
{
    CHARSET_ANSI              =  0,
    CHARSET_DEFAULT           =  1,
    CHARSET_SYMBOL            =  2,
    CHARSET_DECMULTINATIONAL  =  30,  /* imgntn custom */
    CHARSET_ACADBIG5          =  31,  /* imgntn custom */
    CHARSET_MACEXPERT         =  32,  /* imgntn custom */
    CHARSET_PDFDOC            =  33,  /* imgntn custom */
    CHARSET_AUTOCAD           =  36,  /* imgntn custom */
    CHARSET_ACADSHIFTJIS      =  37,  /* imgntn custom */
    CHARSET_MACROMAN          =  38,  /* imgntn custom */
    CHARSET_UNICODE           =  39,  /* imgntn custom */
    CHARSET_PDFEMBEDFONT      =  40,  /* for pdfembedfont (PMR 34676)Weixing*/
    CHARSET_GLYPHIDX          =  41,  /* imgntn custom */ 
    CHARSET_PDF_CID           = 42,  /* imgntn custom */ 
    CHARSET_MAC               = 77,
    CHARSET_SHIFTJIS_UCS2     = 127,  /* support UniJIS-UCS2 for Japanese font */
    CHARSET_SHIFTJIS          = 128, // 0x80
    CHARSET_HANGEUL           = 129, // 0x81
    CHARSET_JOHAB             = 130, // 0x82
    CHARSET_GB2312            = 134, // 0x86
    CHARSET_CHINESEBIG5       = 136, // 0x88
    CHARSET_GREEK             = 161, // 0xA1
    CHARSET_TURKISH           = 162, // 0xA2
    CHARSET_VIETNAMESE        = 163, // 0xA3
    CHARSET_HEBREW            = 177, // 0xB1
    CHARSET_ARABIC            = 178, // 0xB2
    CHARSET_BALTIC            = 186, // 0xBA
    CHARSET_RUSSIAN           = 204, // 0xCC
    CHARSET_THAI              = 222, // 0xDE
    CHARSET_EASTEUROPE        = 238, // 0xEE
    CHARSET_OEM               = 255, // 0xFF
}UV_CHARSET;

#define CHARSET_IS_SYSTEM_PAN_EUROPE( c )   ( c == CHARSET_ANSI || c == CHARSET_EASTEUROPE || c == CHARSET_RUSSIAN || c == CHARSET_BALTIC || c == CHARSET_TURKISH || c == CHARSET_GREEK )
#define CHARSET_IS_SYSTEM_NON_WESTERN_PAN_EUROPE( c )   ( c == CHARSET_EASTEUROPE || c == CHARSET_RUSSIAN || c == CHARSET_BALTIC || c == CHARSET_TURKISH || c == CHARSET_GREEK )
#define CHARSET_IS_FAR_EAST( c )   ( c == CHARSET_SHIFTJIS || c == CHARSET_SHIFTJIS_UCS2 || c == CHARSET_GB2312 || c == CHARSET_CHINESEBIG5 || c == CHARSET_HANGEUL || c == CHARSET_JOHAB )
#define CHARSET_IS_ACAD_FAR_EAST( c )   ( c == CHARSET_ACADSHIFTJIS || c == CHARSET_ACADBIG5 )
#define CHARSET_IS_DEFAULT( c )    ( c == CHARSET_DEFAULT || c == CHARSET_DECMULTINATIONAL || c == CHARSET_ACADBIG5 || c == CHARSET_MACEXPERT || c == CHARSET_PDFDOC || c == CHARSET_AUTOCAD || c == CHARSET_ACADSHIFTJIS || c == CHARSET_MACROMAN || c == CHARSET_UNICODE || c == CHARSET_PDFEMBEDFONT || c == CHARSET_MAC )
#define CHARSET_IS_BIDI( c )       ( c == CHARSET_HEBREW || c == CHARSET_ARABIC )

/*
https://www.unicode.org/Public/MAPPINGS/VENDORS/MICSFT/
https://www.techonthenet.com/js/language_tags.php
*/

class UNIV_API CCodepage
{
public:
    CCodepage();
    ~CCodepage();

    const char* getBcp47( int codepage );
    unsigned int getSignature( int codepage );

public:
    void setExePath(const wchar_t *_path);
    int codepageToUtf16( int codepage, const unsigned char *src, int len, unsigned char *dest );

private:
    int getCodepageIndex( int codepage );
    void loadCodepageMap( int index, int codepage );
      
private:
    std::wstring exePath;
    std::map<int, int> codepageIndexMap;
    std::map<unsigned int, unsigned int> ** maps;
    unsigned int **signature;
};


