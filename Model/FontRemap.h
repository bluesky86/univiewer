#pragma once
#include "Model.h"

class UNIV_API CUvFontRemap
{
public:
    CUvFontRemap();
    ~CUvFontRemap();

    float scaleX;
    float scaleY;
    wchar_t *origName;
    wchar_t *remapName;
};

UNIV_API bool UV_LoadFontConfig(const wchar_t* dllDir, const wchar_t *configFile);

UNIV_API void UV_InitDefaultFonts();
UNIV_API void UV_ClearDefaultFonts();

UNIV_API CUvFontRemap* UV_RemapFont(const wchar_t* fontName);
UNIV_API CUvFontRemap* UV_RemapFont(const char* fontName);
