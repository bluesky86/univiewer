#include "stdafx.h"
#include "CUvColor.h"
#include "string.h"
#include "DebugNew.h"

CUvColor::CUvColor()
    :r(0), g(0), b(0), a(255)
{
}

CUvColor::CUvColor(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a /*= 255*/)
    :r(_r), g(_g), b(_b), a(_a)
{
}

unsigned char GetValueFromHex(unsigned char c1, unsigned char c2)
{
    if (c1 >= 'a') c1 = c1 - 'a' + 10;
    else if (c1 >= 'A') c1 = c1 - 'A' + 10;
    else c1 = c1 - '0';
    if (c2 >= 'a') c2 = c2 - 'a' + 10;
    else if (c2 >= 'A') c2 = c2 - 'A' + 10;
    else c2 = c2 - '0';

    return c1 * 16 + c2;
}
CUvColor::CUvColor(const char *hexstr)
{
    if (hexstr == nullptr || strlen(hexstr) < 6) return;

    r = GetValueFromHex(hexstr[0], hexstr[1]);
    g = GetValueFromHex(hexstr[2], hexstr[3]);
    b = GetValueFromHex(hexstr[4], hexstr[5]);
    if (strlen(hexstr) >= 8)
        a = GetValueFromHex(hexstr[6], hexstr[7]);
    else
        a = 255;
}

CUvColor::~CUvColor()
{
}

bool CUvColor::operator == (CUvColor& color)
{
    return !((*this) != color);
}

bool CUvColor::operator != (CUvColor& color)
{
    if (r != color.r || b != color.b || g != color.g || a != color.a)
        return true;
    else
        return false;
}

CUvColor CUvColor::MakeColor(unsigned int i)
{
    unsigned char* pI = (unsigned char*)&i;
    return CUvColor(pI[0], pI[1], pI[2], pI[3]);
}

unsigned int CUvColor::ToRgbaInt()
{
    return (r << 24) | (g << 16) | (b << 8) | a;
}

unsigned int CUvColor::ToArgbInt()
{
    return (a << 24) | (r << 16) | (g << 8) | b;
}
