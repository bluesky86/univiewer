#include "stdafx.h"
#include "Error.h"
#include "tinyxml2.h"
#include "UvmUtils.h"
#include <vector>
#include "DebugNew.h"
#include "FontRemap.h"

CUvFontRemap::CUvFontRemap()
    :scaleX(1.0f), scaleY(1.0f), origName(nullptr), remapName(nullptr)
{

}
CUvFontRemap::~CUvFontRemap()
{
    if (origName) delete[] origName;
    if (remapName) delete[] remapName;
}

typedef std::vector< CUvFontRemap*> CUvFontRemapList;
CUvFontRemapList *gFontRemapList = nullptr;

UNIV_API bool UV_LoadFontConfig(const wchar_t* dllDir, const wchar_t *configFile)
{
    char filePath[FILEPATH_MAX_LENGTH];
    size_t len;
    len = UV_Wchar2Char(filePath, FILEPATH_MAX_LENGTH, dllDir);
    if (len > 0)
        len = UV_Wchar2Char(&filePath[len - 1], FILEPATH_MAX_LENGTH - len, configFile);
    if (len == 0)
    {
        UV_SetError(UV_ERROR_LOAD_FONT_CONFIG_FAILED, L"Failed to convert between char and Wchar");
        return false;
    }

    UV_InitDefaultFonts();

    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLError err = xmlDoc.LoadFile(filePath);
    if (err != tinyxml2::XML_SUCCESS)
    {
        UV_SetError(UV_ERROR_LOAD_FONT_CONFIG_FAILED, L"Failed to load font config");
        return false;
    }

    tinyxml2::XMLElement* xmlRemapListElement = xmlDoc.FirstChildElement()->FirstChildElement("FontRemapList");
    tinyxml2::XMLElement* xmlRemapEntry = xmlRemapListElement->FirstChildElement("RemapEntry");
    while (xmlRemapEntry)
    {
        const float scale_x = xmlRemapEntry->FloatAttribute("scale_x");
        const float scale_y = xmlRemapEntry->FloatAttribute("scale_y");
        const char *xmlFontName = xmlRemapEntry->Attribute("orig_font_name");
        const char *xmlRemapName = xmlRemapEntry->Attribute("remap_font_name");

        CUvFontRemap *fontRemap = NEW CUvFontRemap();
        fontRemap->origName = UV_Char2Wchar(xmlFontName);
        fontRemap->remapName = UV_Char2Wchar(xmlRemapName);
        fontRemap->scaleX = scale_x;
        fontRemap->scaleY = scale_y;
        gFontRemapList->push_back(fontRemap);

        xmlRemapEntry = xmlRemapEntry->NextSiblingElement("RemapEntry");
    }

    return true;
}

UNIV_API void UV_InitDefaultFonts()
{
    if (gFontRemapList == nullptr)
        gFontRemapList = NEW CUvFontRemapList;
}
UNIV_API void UV_ClearDefaultFonts()
{
    if (gFontRemapList)
    {
        for (size_t i = 0; i < gFontRemapList->size(); i++)
            if((*gFontRemapList)[i]) delete (*gFontRemapList)[i];

        delete gFontRemapList;
    }
}

UNIV_API CUvFontRemap* UV_RemapFont(const wchar_t* fontName)
{
    if (gFontRemapList == nullptr) return nullptr;

    for (size_t i = 0; i < gFontRemapList->size(); i++)
    {
        if (wcscmp((*gFontRemapList)[i]->origName, fontName) == 0)
            return (*gFontRemapList)[i];
    }

    return nullptr;
}

UNIV_API CUvFontRemap* UV_RemapFont(const char* fontName)
{
    if (gFontRemapList == nullptr) return nullptr;

    char origName[256];
    for (size_t i = 0; i < gFontRemapList->size(); i++)
    {
        UV_Wchar2Char(origName, 256, (*gFontRemapList)[i]->origName);
        if (strcmp(origName, fontName) == 0)
            return (*gFontRemapList)[i];
    }

    return nullptr;
}