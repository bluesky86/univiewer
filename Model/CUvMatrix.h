#pragma once
#include "Model.h"
#include "CUvPoint.h"

class UNIV_API CUvMatrix
{
public:
    CUvMatrix();
    CUvMatrix(float m00, float m01, float m10, float m11, float m20, float m21);
    ~CUvMatrix();
    CUvMatrix & operator = (const CUvMatrix &rhs);

    void set(float m00, float m01, float m10, float m11, float m20, float m21);

    void setRotation(float radAngle); // RAD
    void setTranslation(float dx, float dy);

    bool isIdentity();

public:
    float mat[3][3];
    //float m11, m12, m13; // scaleX, skewY, persp0 (nput x-axis values perspective factor to store)
    //float m21, m22, m23; // skewX, scaleY, persp1 (input y-axis values perspective factor to store)
    //float m31, m32, m33; // transX, transY, persp2 (perspective scale factor to store)
};

UNIV_API CUvPointF operator * (const CUvPointF &p, const CUvMatrix &m);
UNIV_API CUvMatrix operator * (const CUvMatrix &a, const CUvMatrix &b);
UNIV_API CUvMatrix operator * (float s, const CUvMatrix &matrix);

