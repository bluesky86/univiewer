#pragma once

#include "CUvDoc.h"
#include "CUvPage.h"

// file type
#define FILETYPE_PNG            1
#define FILETYPE_JPG            2
#define FILETYPE_TIF            3
#define FILETYPE_GIF            4

// color type
#define FILE_COLORTYPE_256              0
#define FILE_COLORTYPE_BILEVLE          1
#define FILE_COLORTYPE_GRAYSCALE        2
#define FILE_COLORTYPE_TRUECOLOR        3
#define FILE_COMPOUND_FILE              4

// load xml config file of certain loader, name is "LoadXmlConfig"
// filePath is in UTF8 format
// return false if error
typedef bool(__stdcall *loader_LoadXmlConfig)(const char *filePath);
// close loader to release resource allocated by loader. 
// name is "CloseLoader".
typedef void(__stdcall *loader_CloseLoader)();

// Loader returns true if this file can be parsed by this loader, otherwise false
// loader should check the file content to make sure it can be recognized.
// the function name in loader must be "Recognizer"
// "loader-config.xml" will list the file extensions each loader supported. Render will
// load the selected file with the loader registered in that xml. If it fails to load it,
// Render will loop each loader by calling this function, if any loader return true,
// "LoadDoc" of that loader will be called to load the file. And also it is possible, the 
// file don't have extension at all
typedef bool (__stdcall *loader_Recognizer)(const wchar_t *fileName);
// recognizer from buffer. Name must be "RecognizerBuffer"
typedef bool (__stdcall *loader_RecognizerBuffer)(unsigned char *pData, int datalen);

// Loader returns parsed contents in a structure (void* doc)
// the function name in loader must be "OpenDoc". return total pages
typedef CUvDoc* (__stdcall *loader_OpenDoc)(const wchar_t *fileName);
// load from buffer. Name must be "OpenDocFromBuffer"
typedef CUvDoc* (__stdcall *loader_OpenDocFromBuffer)(unsigned char *pData, int datalen);

// the function name in loader must be "GetPage"
typedef CUvPage* (__stdcall *loader_GetPage)(CUvDoc *pDoc, int pageNo);

// the function name in loader must be "CloseDoc"
typedef void (__stdcall *loader_CloseDoc)(CUvDoc *pDoc);

// If the function "LoadDoc" above return NULL, Render will call below function to retrieve
// error string. The function name must be "GetLoaderLastError"
// Loader has the responsibility to release the Wchar string, so it is better to define error
// string as a global variable and released when DLL is unloaded only.
typedef wchar_t* (__stdcall *loader_GetLastError)();
