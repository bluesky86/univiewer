#include "stdafx.h"
#include "CUvPage.h"
#include "DebugNew.h"



CUvPage::CUvPage()
    :pageNo(-1), pRenderFonts(nullptr)
{
    type = uvPage_ShapeType;
    fontList = NEW CUvFonts;
}


CUvPage::~CUvPage()
{
    if (fontList)
    {
        delete fontList;
    }
}

CUvShape* CUvPage::Copy()
{
    CUvPage *p = NEW CUvPage();
    p->CopyFrom(this);

    return p;
}

void CUvPage::CopyFrom(CUvPage *p)
{
    if (p == nullptr) return;

    CUvGroup::CopyFrom(p);

    pageNo = p->pageNo;
    bkColor = p->bkColor;
}

void CUvPage::GetBound(CUvRectF &rect)
{
    rect.CopyFrom(this);
}


CUvShape* getUvPage(CUvShape *pShape)
{
    while (pShape && pShape->type != uvPage_ShapeType)
        pShape = pShape->parent;

    if (pShape && pShape->type == uvPage_ShapeType) return pShape;
    else return nullptr;
}


