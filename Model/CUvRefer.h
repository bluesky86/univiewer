#pragma once
#include "Model.h"
#include "CUvShape.h"
#include "CUvPoint.h"
#include "CUvMatrix.h"
#include "CUvRect.h"

class UNIV_API CUvRefer : public CUvShape
{
public:
    CUvRefer();
    ~CUvRefer();

    CUvShape* Copy();
    virtual void CopyFrom(CUvRefer *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvShape * pRefer;
    CUvMatrix mat;
};

class UNIV_API CUvRefers
{
public:
    CUvRefers();
    ~CUvRefers();
    void Clear();

public:
    std::map<int, CUvShape*> uvShapes;
};



