#pragma once
#include "Model.h"

UNIV_API bool LoadErrorConfig(const wchar_t* dllDir, const wchar_t *configFile);
UNIV_API UV_ERROR_CODE_ENUM UV_GetLastError();
UNIV_API const wchar_t* UV_GetLastErrorMsg();
UNIV_API void UV_SetError(UV_ERROR_CODE_ENUM err, const wchar_t* msg = NULL);
UNIV_API void UV_SetError(UV_ERROR_CODE_ENUM err, const char* msg);

UNIV_API void UV_InitError();
UNIV_API void UV_ClearError();

