#pragma once
#include "CUvRect.h"
#include "CUvMatrix.h"
#include "CUvPath.h"
#include <vector>
#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class

class UNIV_API CUvGroup : public CUvRectFShape
{
public:
    CUvGroup();
    virtual ~CUvGroup();

    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvGroup *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvMatrix mat;
    std::vector<CUvShape*> components;

    CUvPath *pClipPath;

    CUvColor bkColor;
};

