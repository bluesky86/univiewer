#include "stdafx.h"
#include "UvmUtils.h"
#include <stdlib.h>
#include <string.h>
#include "DebugNew.h"

UNIV_API wchar_t* UV_Char2Wchar(const char* src, bool bFreeSrc)
{
    size_t len = strlen(src);
    mbstowcs_s(&len, NULL, 0, src, len + 1);
    wchar_t *pWchar = NEW wchar_t[len + 1];
    mbstowcs_s(&len, pWchar, len + 1, src, len + 1);
    if (bFreeSrc) delete[] src;
    return pWchar;
}

UNIV_API size_t UV_Char2Wchar(wchar_t* dest, size_t bufferLen, const char* src)
{
    size_t len = strlen(src);
    mbstowcs_s(&len, NULL, 0, src, len + 1);
    if (bufferLen < len) return 0;
    mbstowcs_s(&len, dest, len + 1, src, len + 1);
    return len;
}

UNIV_API char* UV_Wchar2Char(const wchar_t* src, bool bFreeSrc)
{
    size_t len = 0;
    wcstombs_s(&len, NULL, 0, src, len + 1);
    char *pChar = NEW char[len + 1];
    wcstombs_s(&len, pChar, len + 1, src, len + 1);
    if (bFreeSrc) delete[] src;
    return pChar;
}

UNIV_API size_t UV_Wchar2Char(char* dest, size_t bufferLen, const wchar_t* src)
{
    size_t len = 0;
    wcstombs_s(&len, NULL, 0, src, len + 1);
    if (bufferLen < len) return 0;
    wcstombs_s(&len, dest, len + 1, src, len + 1);
    return len;
}

UNIV_API size_t reverseCharPos(wchar_t *string, wchar_t ch, int startPos)
{
    size_t len = wcslen(string);
    if (startPos > len - 1)
        startPos = (int)(len - 1);
    if (startPos <= 0)
        startPos = (int)(len - 1);

    for (; startPos >= 0; startPos--)
    {
        if (string[startPos] == ch)
            break;
    }
    return startPos; // -1 if not found
}

UNIV_API size_t replaceChar(wchar_t *string, wchar_t ch, wchar_t newCh, int startPos)
{
    size_t count = 0;

    int len = (int)wcslen(string);
    for (int i = startPos; i < len; i++)
    {
        if (string[i] == ch)
        {
            string[i] = newCh;
            count++;
        }
    }

    return count;
}

UNIV_API void UV_AllocCopyStr(const char* src, const char** _target)
{
    if (_target == nullptr || src == nullptr) return;

    size_t len = strlen(src);

    if (*_target) delete[](*_target);

    *_target = NEW char[len + 2];

    memcpy((void*)(*_target), src, len);
    (*(char**)_target)[len] = 0;
    (*(char**)_target)[len + 1] = 0;
}

UNIV_API int UV_Utf8_strlen(const char* str)
{
    int len = (int)strlen(str);
    return UV_Utf8_strlen(str, len);
}

UNIV_API int UV_Utf8_strlen(const char* str, int len)
{
    int c, i, ix, q;
    for (q = 0, i = 0, ix = len; i < ix; i++, q++)
    {
        c = (unsigned char)str[i];
        if (c >= 0 && c <= 127) i += 0;
        else if ((c & 0xE0) == 0xC0) i += 1;
        else if ((c & 0xF0) == 0xE0) i += 2;
        else if ((c & 0xF8) == 0xF0) i += 3;
        //else if (($c & 0xFC) == 0xF8) i+=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
        //else if (($c & 0xFE) == 0xFC) i+=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
        else
            return 0;//invalid utf8
    }
    return q;
}

UNIV_API int UV_Utf8_first_char( const char* str, int len )
{
    int v = (unsigned char)str[0];
    if(v >= 0 && v <= 127) return v;
    else if(( v & 0xE0 ) == 0xC0) return (v << 8) | (unsigned char)str[1];
    else if(( v & 0xF0 ) == 0xE0) return ( v << 16 ) | (((unsigned char)str[1]) << 8) | (unsigned char)str[2];
    else if(( v & 0xF8 ) == 0xF0) return ( v << 24 ) | ( ( (unsigned char)str[1] ) << 16 ) | ( ( (unsigned char)str[2] ) << 8 ) | (unsigned char)str[3];
    else
        return 0;//invalid utf8
}


// 0x91 will be converted into 0xc2 0x91
UNIV_API int UV_1ByteStringTo2BytesUtf8(const unsigned char* str, int len, CVector<unsigned char> *dest)
{
    if (dest == nullptr) return 0;

    dest->clear();
    for (int i = 0; i < len; i++)
    {
        unsigned char ch = str[i];
        if (str[i] > 0x7F)
        {
            if (str[i] <= 0xBF)
                dest->push_back(0xC2);
            else
            {
                dest->push_back(0xC3);
                ch -= 0x40;
            }
        }
        dest->push_back(ch);
    }
    return (int)dest->size();
}


