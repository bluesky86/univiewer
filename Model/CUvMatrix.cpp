#include "stdafx.h"
#include "CUvMatrix.h"



CUvMatrix::CUvMatrix()
{
    mat[0][0] = mat[1][1] = mat[2][2] = 1.0f;
    mat[0][1] = mat[0][2] = 0.0f;
    mat[1][0] = mat[1][2] = 0.0f;
    mat[2][0] = mat[2][1] = 0.0f;
}

CUvMatrix::CUvMatrix(float m00, float m01, float m10, float m11, float m20, float m21)
{
    set(m00, m01, m10, m11, m20, m21);
}

CUvMatrix::~CUvMatrix()
{
}

void CUvMatrix::set(float m00, float m01, float m10, float m11, float m20, float m21)
{
    mat[0][0] = m00;
    mat[1][1] = m11;
    mat[2][2] = 1.0f;
    mat[0][1] = m01;
    mat[0][2] = 0.0f;
    mat[1][0] = m10;
    mat[1][2] = 0.0f;
    mat[2][0] = m20;
    mat[2][1] = m21;
}

void CUvMatrix::setRotation(float radAngle)
{
    float cosValue = cos(radAngle);
    float sinValue = sin(radAngle);
    set(cosValue, -sinValue, sinValue, cosValue, 0, 0);
}

void CUvMatrix::setTranslation(float dx, float dy)
{
    mat[2][0] = dx;
    mat[2][1] = dy;
}

CUvMatrix &CUvMatrix::operator = (const CUvMatrix &rhs)
{
    if (this != &rhs)
    {
        memcpy(mat, rhs.mat, sizeof(mat));
    }

    //SetDirty();

    return(*this);
}

bool CUvMatrix::isIdentity()
{
    if (mat[0][0] == 1.0f && mat[1][1] == 1.0f && mat[2][2] == 1.0f
        && mat[0][1] == 0.0f && mat[0][2] == 0.0f && mat[1][0] == 0.0f
        && mat[1][2] == 0.0f && mat[2][0] == 1.0f && mat[2][1] == 0.0f)
        return true;
    else
        return false;
}

/////////////////////////////////////////////////////////////////////////////////
CUvPointF operator * (const CUvPointF &p, const CUvMatrix &m)
{
    return(CUvPointF(
        (float)(p.x * m.mat[0][0] + p.y * m.mat[1][0] + m.mat[2][0]),
        (float)(p.x * m.mat[0][1] + p.y * m.mat[1][1] + m.mat[2][1]))
        );
}

CUvMatrix operator * (const CUvMatrix &a, const CUvMatrix &b)
{
    CUvMatrix product;

    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            product.mat[i][j] = a.mat[i][0] * b.mat[0][j];

            for (k = 1; k < 3; k++)
            {
                product.mat[i][j] += a.mat[i][k] * b.mat[k][j];
            }
        }
    }

    return(product);
}

CUvMatrix operator * (float s, const CUvMatrix &a)
{
    CUvMatrix tmp;

    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            tmp.mat[i][j] = s * a.mat[i][j];
        }
    }

    return(tmp);
}

