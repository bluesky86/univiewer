#include "stdafx.h"
#include "CUvDoc.h"
#include "DebugNew.h"



CUvDoc::CUvDoc()
    :nTotalPages(0), pLoaderInfo(nullptr), pRenderFonts(nullptr)
{
    type = uvDoc_ShapeType;
    fontList = NEW CUvFonts;
}


CUvDoc::~CUvDoc()
{
    if (fontList)
    {
        delete fontList;
    }
}

void CUvDoc::GetBound(CUvRectF &rect)
{
}

CUvShape* getUvDoc(CUvShape *pShape)
{
    while (pShape && pShape->type != uvDoc_ShapeType)
        pShape = pShape->parent;

    if (pShape && pShape->type == uvDoc_ShapeType) return pShape;
    else return nullptr;
}

