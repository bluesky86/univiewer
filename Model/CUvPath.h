#pragma once
#include "Model.h"
#include "CUvShape.h"
#include "CUvPoint.h"
#include "CUvMatrix.h"
#include "CUvRect.h"
#include "CUvStrokeFill.h"
#include <vector>


class UNIV_API CUvQuad : public CUvShape
{
public:
    CUvQuad();
    ~CUvQuad();

    CUvShape* Copy();
    virtual void CopyFrom(CUvQuad *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvPointFs *points; // three points
};

class UNIV_API CUvCubic : public CUvShape
{
public:
    CUvCubic();
    ~CUvCubic();

    CUvShape* Copy();
    virtual void CopyFrom(CUvCubic *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvPointFs * points; // four points
};

class UNIV_API CUvAngleArc : public CUvShape
{
public:
    CUvAngleArc();
    ~CUvAngleArc();

    CUvShape* Copy();
    virtual void CopyFrom(CUvAngleArc *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvRectF oval; 
    double startAngle;
    double sweepAngle;
    double xRotation;
};

class UNIV_API CUvTangentArc : public CUvShape
{
public:
    CUvTangentArc();
    ~CUvTangentArc();

    CUvShape* Copy();
    virtual void CopyFrom(CUvTangentArc *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvPointF points[3];
    float radius;
};

class UNIV_API CUvOval : public CUvShape
{
public:
    CUvOval();
    ~CUvOval();

    CUvShape* Copy();
    virtual void CopyFrom(CUvOval *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvRectF rect;
    bool bClockwise;
    double xRotation; // rotate at oval center
};

class UNIV_API CUvOvalArc : public CUvShape  // only used as a component of CUvPath
{
public:
    CUvOvalArc();
    ~CUvOvalArc();

    CUvShape* Copy();
    virtual void CopyFrom(CUvOvalArc *p);
    virtual void GetBound(CUvRectF &rect);

public:
    float rx, ry;
    double xRotate;
    bool bClockwise;

    float x, y; // end point
    bool bLargeArc;
};

class UNIV_API CUvCircle : public CUvShape
{
public:
    CUvCircle();
    ~CUvCircle();

    CUvShape* Copy();
    virtual void CopyFrom(CUvCircle *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvPointF center;
    float radius;
    bool bClockwise;
};

class UNIV_API CUvCircleArc : public CUvRectFShape
{
public:
    CUvCircleArc();
    ~CUvCircleArc();

    CUvShape* Copy();
    virtual void CopyFrom(CUvCircleArc *p);
    virtual void GetBound(CUvRectF &rect);

public:
    double startAngle;
    double sweepAngle;
    bool useCenter;     // true: pie, false: chord
};

class UNIV_API CUvPoly : public CUvShape
{
public:
    CUvPoly();
    ~CUvPoly();

    CUvShape* Copy();
    virtual void CopyFrom(CUvPoly *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvPointFs * points;
    bool bPolygon;
};

class CUvPathOp
{
public:
    CUvPathOp();
    CUvPathOp(UvPathVerbs v, CUvShape *shape, bool copy = true, bool _bAddLineFromLastPoint = false);
    ~CUvPathOp();

    CUvPathOp* Copy();

public:
    UvPathVerbs verb;
    CUvShape *pShape;
    bool bAddLineFromLastPoint;

    CUvMatrix matrix;

private:
    bool bCopy; // if it is true, delete pShape when deconstruction
};


class UNIV_API CUvPath : public CUvShape
{
public:
    CUvPath();
    virtual ~CUvPath();

    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvPath *p);
    virtual void GetBound(CUvRectF &rect);

    int getVerbCount() const;
    std::vector<CUvPathOp*>* getOps();

    void moveTo(CUvPointF &point);
    void moveTo(float x, float y);

    void lineTo(CUvPointF &point);
    void lineTo(float x, float y);

    void quadTo(float x1, float y1, float x2, float y2);
    void quadTo(CUvPointF &point1, CUvPointF &point2);

    void cubicTo(float x1, float y1, float x2, float y2, float x3, float y3);
    void cubicTo(CUvPointF &point1, CUvPointF &point2, CUvPointF &point3);

    void arcTo(CUvRectF &oval, float startAngle, float sweepAngle);
    void arcTo(float x1, float y1, float x2, float y2, float radius);
    void arcTo(CUvPointF &point1, CUvPointF &point2, float radius);

    void ovalArcTo(float rx, float ry, double xRotate, float endX, float endY, bool largeArc, bool clockwise);

    void addPoly(const CUvPointF pts[], int count, bool bNewStart, bool bPolygon = false);
    void addPoly(std::vector<CUvPointF> &points, bool bNewStart, bool bPolygon = false);
    void addArc(const CUvRectF &oval, double startAngle, double sweepAngle);
    void addCircle(const CUvPointF &center, float radius, bool clockwise);
    void addRect(const CUvRectF &rect, bool clockwise);
    void addOval(const CUvRectF &rect, bool clockwise);

    void addPath(const CUvPath& src, float dx = .0f, float dy = .0f, bool bCopy = false);
    void addPath(const CUvPath& src, CUvMatrix& mat, bool bCopy = false);

    void reset();

public:
    UvFillMode fillMode;
    bool bClosed;

    CUvMatrix mat;

private:
    std::vector<CUvPathOp*> *ops;
};

