#pragma once
//#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class
#include "Model.h"
#include "CUvShape.h"
#include "CUvGroup.h"
#include "CUvRefer.h"
#include "CUvFont.h"

class UNIV_API CUvPage : public CUvGroup
{
public:
    CUvPage();
    ~CUvPage();

    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvPage *p);
    virtual void GetBound(CUvRectF &rect);

public:
    int pageNo;
    
    // font list
    CUvFonts *fontList;

    // line pattern
    CUvLinePatterns linePatterns;

    // reference objects
    CUvRefers refers;

    // render objects
    void *pRenderFonts;
};

UNIV_API CUvShape* getUvPage(CUvShape *pShape);


