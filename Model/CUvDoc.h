#pragma once
//#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class
#include "Model.h"
#include "CUvShape.h"
#include "CUvRefer.h"
#include "CUvFont.h"

class UNIV_API CUvDoc : public CUvShape
{
public:
    CUvDoc();
    virtual ~CUvDoc();
    virtual void GetBound(CUvRectF &rect);

public:
    int nTotalPages;

    // line pattern
    CUvLinePatterns linePatterns;
    // reference objects
    CUvRefers refers;

    // font list
    CUvFonts *fontList;

public:
    void *pLoaderInfo;  // used and maintained by loader/parser
    void *pRenderFonts;
};

UNIV_API CUvShape* getUvDoc(CUvShape *pShape);
