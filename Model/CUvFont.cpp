#include "stdafx.h"
#include "CUvFont.h"
#include "UvmUtils.h"
#include "DebugNew.h"


/////////////////////////////////////////////
CUvFontFace::CUvFontFace()
    :familyName(0), faceName(nullptr), parent(nullptr), bItalic(false), bBold(false)
    , bUnderline(false), bStrikeOut(false)
{

}
CUvFontFace::~CUvFontFace()
{
    if (faceName) delete[] faceName;
}

const U8* CUvFontFace::getFaceName()
{
    return faceName;
}
void CUvFontFace::setFaceName(U8* _faceName)
{
    UV_AllocCopyStr((const char*)_faceName, (const char**)&faceName);
}

U8** CUvFontFace::getFaceNameAddr()
{
    return &faceName;
}

const U8* CUvFontFace::getFamilyName()
{
    if(familyName == nullptr && parent)
        familyName = (U8*)parent->getFamilyName();
    return familyName;
}
void CUvFontFace::setFamilyName(const U8* _fontName)
{
    familyName = (U8*)_fontName;
}

bool CUvFontFace::isItalic()
{
    return bItalic;
}
void CUvFontFace::setItalic(bool _i)
{
    bItalic = _i;
}

bool CUvFontFace::isBold()
{
    return bBold;
}
void CUvFontFace::setBold(bool _b)
{
    bBold = _b;
}

bool CUvFontFace::isUnderline()
{
    return bUnderline;
}
void CUvFontFace::setUnderline(bool _b)
{
    bUnderline = _b;
}

bool CUvFontFace::isStrikeOut()
{
    return bStrikeOut;
}
void CUvFontFace::setStrikeOut(bool _b)
{
    bStrikeOut = _b;
}

CUvFont* CUvFontFace::getParent()
{
    return parent;
}
void CUvFontFace::setParent(CUvFont* p)
{
    parent = p;
}

void CUvFontFace::copyFrom(CUvFontFace *p)
{
    if (p == nullptr) return;

    familyName = p->familyName;
    parent = p->parent;
    bItalic = p->bItalic;
    bBold = p->bBold;
    bUnderline = p->bUnderline;
    bStrikeOut = p->bStrikeOut;

    if (p->faceName)
        setFaceName(p->faceName);
}

/////////////////////////////////////////////
CUvFont::CUvFont()
    :familyName(nullptr), href(nullptr), data(nullptr), data_len(0), pRenderFont(nullptr), bSearchByGlyphId(false), charsetType( CHARSET_TYPE_none )
{
    fontFaces = NEW std::vector<CUvFontFace*>;
}


CUvFont::~CUvFont()
{
    if (familyName)
        delete[] familyName;
    if (href)
        delete[] href;
    if (data)
        delete[] data;

    if (fontFaces)
    {
        clearFace();
        delete fontFaces;
    }
}

const U8* CUvFont::getFamilyName()
{
    return familyName;
}

void CUvFont::setFamilyName(const U8* _fontName)
{
    UV_AllocCopyStr((const char*)_fontName, (const char**)&familyName);
}

const U8* CUvFont::getHref()
{
    return href;
}
void CUvFont::setHref(const U8* _href)
{
    UV_AllocCopyStr((const char*)_href, (const char**)&href);
}

const U8* CUvFont::getData(size_t &len)
{
    len = data_len;
    return data;
}

void CUvFont::setData(const U8* _data, size_t len)
{
    //UV_AllocCopyStr((const char*)_data, (const char**)&data);
    data = _data;
    data_len = len;
}

bool CUvFont::isEmbeddedFont()
{
    if(data_len > 0 && data) return true;
    if(href) return true;
    return false;
}

size_t CUvFont::faceCount()
{
    return fontFaces->size();
}
void CUvFont::appendFace(CUvFontFace* fontFace)
{
    if (fontFace == nullptr || findFace(fontFace->getFaceName()))
        return;

    fontFaces->push_back(fontFace);
}
void CUvFont::removeFace(S32 index)
{
    if (index < 0 || index >= (S32)fontFaces->size()) return;

    delete (*fontFaces)[index];

    for (size_t i = index; i < fontFaces->size(); i++)
        (*fontFaces)[i] = (*fontFaces)[i + 1];

    fontFaces->pop_back();
}
CUvFontFace* CUvFont::findFace(const U8* _faceName)
{
    if (_faceName == nullptr) return nullptr;

    for (size_t i = 0; i < fontFaces->size(); i++)
    {
        const U8* pFaceName = (*fontFaces)[(U32)i]->getFaceName();
        if (strcmp((const char*)pFaceName, (const char*)_faceName) == 0)
            return (*fontFaces)[(U32)i];

        if(pFaceName && strcmp((char*)pFaceName, "Default") == 0 && strcmp((char*)getFamilyName(), (char*)_faceName) == 0)
            return (*fontFaces)[(U32)i];
    }

    return nullptr;
}
void CUvFont::clearFace()
{
    for (size_t i = 0; i < faceCount(); i++)
        delete (*fontFaces)[(U32)i];
    fontFaces->clear();
}

std::vector<CUvFontFace*>* CUvFont::getFontFaces()
{
    return fontFaces;
}

void* CUvFont::getRenderFont()
{
    return pRenderFont;
}

void CUvFont::setRenderFont( void *pFont )
{
    pRenderFont = pFont;
}

bool CUvFont::getSearchByGlyphId()
{
    return bSearchByGlyphId;
}

void CUvFont::setSearchByGlyphId( bool v )
{
    bSearchByGlyphId = v;
}

CHARSET_TYPE CUvFont::getCharsetType()
{
    return charsetType;
}

void CUvFont::setCharsetType( CHARSET_TYPE v )
{
    charsetType = v;
}

/////////////////////////////////////////////
CUvFonts::CUvFonts()
{
    fonts = NEW std::vector<CUvFont*>;
}


CUvFonts::~CUvFonts()
{
    clear();

    if (fonts)
        delete fonts;
}

size_t CUvFonts::size()
{
    return fonts->size();
}

CUvFont* CUvFonts::operator[](S32 index)
{
    if (index < 0 || index > (S32)fonts->size()) return nullptr;

    return (*fonts)[index];
}

void CUvFonts::clear()
{
    for (size_t i = 0; i < size(); i++)
        delete (*fonts)[(U32)i];
    fonts->clear();
}

std::vector<CUvFont*>* CUvFonts::getFonts()
{
    return fonts;
}

CUvFont* CUvFonts::find(const U8* familyName)
{
    for (size_t i = 0; i < size(); i++)
    {
        if (strcmp((const char*)(*fonts)[(U32)i]->getFamilyName(), (const char*)familyName) == 0)
            return (*fonts)[(U32)i];
    }

    return nullptr;
}
CUvFontFace* CUvFonts::findFace(const U8* faceName)
{
    for (size_t i = 0; i < size(); i++)
    {
        CUvFontFace *pFace = (*fonts)[(U32)i]->findFace(faceName);
        if (pFace)
            return pFace;
    }

    return nullptr;
}
void CUvFonts::append(CUvFont* font)
{
    if (font == nullptr || find(font->getFamilyName()))
        return;

    fonts->push_back(font);
}

void CUvFonts::remove(S32 index)
{
    if (index < 0 || index >= (S32)fonts->size()) return;

    delete (*fonts)[index];

    for (size_t i = index; i < fonts->size(); i++)
        (*fonts)[i] = (*fonts)[i + 1];

    fonts->pop_back();
}

UNIV_API size_t ConvertHexFontToBinary(const char *src, char** dst)
{
    int pos = 0, start_pos;
    size_t len = strlen(src);

    bool found = false;
    for (pos = 0; pos < len - 3; pos++)
    {
        if (src[pos] == 'F' && src[pos + 1] == 'F' && src[pos + 2] == 'F'&& src[pos + 3] == 'E')
        {
            found = true;
            break;
        }
    }

    if (!found) return 0;

    start_pos = pos;

    found = false;
    for (pos = (int)len - 4; pos >= start_pos + 4; pos--)
    {
        if (src[pos] == 'F' && src[pos + 1] == 'F' && src[pos + 2] == 'F'&& src[pos + 3] == 'E')
        {
            found = true;
            break;
        }
    }
    if (!found) return 0;

    len = pos - start_pos - 4;

    size_t dst_len = len / 2;
    char *pData = NEW char[dst_len];
    *dst = pData;

    size_t j = 0;
    unsigned ch[2];
    for (size_t i = start_pos + 4; i < pos; i += 2)
    {
        if (src[i] <= '9') ch[0] = src[i] - '0';
        else ch[0] = src[i] - 'A' + 10;
        if (src[i + 1] <= '9') ch[1] = src[i + 1] - '0';
        else ch[1] = src[i + 1] - 'A' + 10;
        pData[j++] = ch[0] * 16 + ch[1];
    }

    return dst_len;
}

UNIV_API size_t ConvertBinaryFontToHexText(const char *src, size_t src_len, char** dst)
{
    if (src == nullptr || dst == nullptr) return 0;

    size_t dst_len = src_len * 2 + 8;
    char *pData = NEW char[dst_len];
    *dst = pData;

    size_t pos = 0;
    pData[pos++] = 'F';
    pData[pos++] = 'F';
    pData[pos++] = 'F';
    pData[pos++] = 'E';

    size_t src_pos = 0;

    unsigned char c, ce;
    char ch;
    for (size_t i = 0; i < src_len; i++)
    {
        c = (unsigned char)src[i];
        ce = (c >> 4);
        if (ce <= 9)
            ch = ce + '0';
        else
            ch = 'A' + (ce - 10);
        pData[pos++] = ch;

        ce = (c & 0x0F);
        if (ce <= 9)
            ch = ce + '0';
        else
            ch = 'A' + (ce - 10);
        pData[pos++] = ch;
    }

    pData[pos++] = 'F';
    pData[pos++] = 'F';
    pData[pos++] = 'F';
    pData[pos++] = 'E';

    return dst_len;
}

UNIV_API void ConvertBinaryFontToHexTextFromFile(char *src)
{
    FILE *fp_out, *fp_in;
    char buf[1024];

    std::string srcStr = src;
    std::string targetPath;
    size_t pos = srcStr.find_last_of('\\');
    if (pos == std::string::npos)
        targetPath = "";
    targetPath = srcStr.substr(0, pos + 1);
    std::string targetName = srcStr.substr(pos + 1, srcStr.length() - pos - 1);

    sprintf_s(buf, 1024, "%s%s.txt", targetPath.c_str(), targetName.c_str());
    fopen_s(&fp_out, buf, "w");

    fopen_s(&fp_in, src, "rb");
    fseek(fp_in, 0, SEEK_END);
    size_t flen = ftell(fp_in);
    fseek(fp_in, 0, SEEK_SET);

    char *pSrcData = NEW char[flen];
    size_t bytesRead = fread(pSrcData, 1, flen, fp_in);
    fclose(fp_in);

    if (bytesRead != flen)
    {
        delete[] pSrcData;
        fclose(fp_out);
        return;
    }

    char *pDstData = nullptr;
    size_t dst_len = ConvertBinaryFontToHexText(pSrcData, flen, &pDstData);
    if (dst_len > 0 && pDstData != nullptr)
        fwrite(pDstData, 1, dst_len, fp_out);
    
    delete[] pSrcData;
    if (pDstData)
        delete[] pDstData;
    fclose(fp_out);

    /*
    fwrite("FFFE", 1, 4, fp_out);

    unsigned char c, ce;
    char ch;
    while (fread(&c, 1, 1, fp_in) == 1)
    {
        ce = (c >> 4);
        if (ce <= 9)
            ch = ce + '0';
        else
            ch = 'A' + (ce - 10);
        fwrite(&ch, 1, 1, fp_out);

        ce = (c & 0x0F);
        if (ce <= 9)
            ch = ce + '0';
        else
            ch = 'A' + (ce - 10);
        fwrite(&ch, 1, 1, fp_out);
    }

    fwrite("FFFE", 1, 4, fp_out);

    fclose(fp_in);
    fclose(fp_out);
    */
}

