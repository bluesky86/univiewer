#pragma once
#pragma warning(disable : 4251) //needs to have dll-interface to be used by clients of class
#include "Model.h"
#include "CUvShape.h"
#include <vector>

class CUvRectF;

////////////////////////////////////////////////////////////////////////////////
// float point
class UNIV_API CUvPointF
{
public:
    CUvPointF();
    CUvPointF(float _x, float _y);
    ~CUvPointF();

    CUvPointF* Copy();
    void CopyFrom(CUvPointF *p);

public:
    float x, y;
};

////////////////////////////////////////////////////////////////////////////////
// integer point
class UNIV_API CUvPoint
{
public:
    CUvPoint();
    CUvPoint(int _x, int _y);
    ~CUvPoint();

    virtual CUvPoint* Copy();
    virtual void CopyFrom(CUvPoint *p);

public:
    int x, y;
};

////////////////////////////////////////////////////////////////////////////////
// float points
class UNIV_API CUvPointFs
{
public:
    CUvPointFs();
    ~CUvPointFs();

    CUvPointFs* Copy();
    void CopyFrom(CUvPointFs *p);

public:
    std::vector<CUvPointF> points;
};

////////////////////////////////////////////////////////////////////////////////
// integer points
class UNIV_API CUvPoints
{
public:
    CUvPoints();
    ~CUvPoints();

    virtual CUvPoints* Copy();
    virtual void CopyFrom(CUvPoints *p);

public:
    std::vector<CUvPoint> points;
};

////////////////////////////////////////////////////////////////////////////////
// float point shape
class UNIV_API CUvPointFShape : public CUvShape, public CUvPointF
{
public:
    CUvPointFShape();
    CUvPointFShape(float _x, float _y);
    ~CUvPointFShape();

    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvPointFShape *p);
    virtual void GetBound(CUvRectF &rect);

};

////////////////////////////////////////////////////////////////////////////////
// float points
class UNIV_API CUvPointFsShape : public CUvShape, public CUvPointFs
{
public:
    CUvPointFsShape();
    ~CUvPointFsShape();

    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvPointFsShape *p);
    virtual void GetBound(CUvRectF &rect);

};

////////////////////////////////////////////////////////////////////////////////
// line
class UNIV_API CUvLine : public CUvShape
{
public:
    CUvLine();
    ~CUvLine();

    virtual CUvShape* Copy();
    virtual void CopyFrom(CUvLine *p);
    virtual void GetBound(CUvRectF &rect);

public:
    CUvPointF point[2];
};
