#include "stdafx.h"
#include "Error.h"
#include "tinyxml2.h"
#include "UvmUtils.h"
#include <map>
#include "DebugNew.h"
#include "Codepage.h"

#define ERROR_MSG_MAX_LENGTH       511

typedef std::map<int, wchar_t*> UV_ERROR_MAP;

class CUvError
{
public:
    CUvError() { };
    ~CUvError()
    {
        Clear();
    }

    void Clear()
    {
        for (int i = 0; i < errMap.size(); i++)
            delete[]errMap[i];

        errMap.clear();
    };

public:
    UV_ERROR_MAP errMap;
};


CUvError *gErrorObj = nullptr;
static UV_ERROR_CODE_ENUM gUvError = UV_ERROR_NONE;
static wchar_t gUvErrorMsg[ERROR_MSG_MAX_LENGTH + 1];


UNIV_API UV_ERROR_CODE_ENUM UV_GetLastError()
{
    return gUvError;
}

UNIV_API const wchar_t* UV_GetLastErrorMsg()
{
    return gUvErrorMsg;
}

UNIV_API void UV_SetError(UV_ERROR_CODE_ENUM err, const wchar_t* msg)
{
    gUvError = err;
    if (err == UV_ERROR_NONE)
        memset(gUvErrorMsg, 0, sizeof(wchar_t) * ERROR_MSG_MAX_LENGTH);
    else
    {
        UV_InitError();

        wchar_t *pMainMsg = gErrorObj->errMap[err];
        if (!pMainMsg && !msg)
        {
            wcscpy_s(gUvErrorMsg, L"No message available!");
        }
        else
        {
			size_t len = 0;
            if (pMainMsg)
            {
				len = wcslen(pMainMsg);
                wcscpy_s(gUvErrorMsg, ERROR_MSG_MAX_LENGTH, pMainMsg);
            }
            if (msg)
            {
                wcscpy_s(&gUvErrorMsg[len], ERROR_MSG_MAX_LENGTH, msg);
            }
        }
    }
}

UNIV_API void UV_SetError(UV_ERROR_CODE_ENUM err, const char* msg)
{
    wchar_t *pWchar = NULL;
    if (msg)
    {
        pWchar = UV_Char2Wchar(msg);
    }

    UV_SetError(err, pWchar);

    if (pWchar) delete[] pWchar;
}

UNIV_API bool LoadErrorConfig(const wchar_t* dllDir, const wchar_t *configFile)
{
    char filePath[FILEPATH_MAX_LENGTH];
    size_t len;
    len = UV_Wchar2Char(filePath, FILEPATH_MAX_LENGTH, dllDir);
    if (len > 0)
        len = UV_Wchar2Char(&filePath[len - 1], FILEPATH_MAX_LENGTH - len, configFile);
    if (len == 0)
    {
        UV_SetError(UV_ERROR_BEFORE_ERROR_LIST_LOADED, L"Failed to convert between char and Wchar");
        return false;
    }

    UV_InitError();

    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLError err = xmlDoc.LoadFile(filePath);
    if (err != tinyxml2::XML_SUCCESS)
    {
        UV_SetError(UV_ERROR_BEFORE_ERROR_LIST_LOADED, L"Failed to load error list file: error-config.xml");
        return false;
    }

    tinyxml2::XMLElement* xmlErrorListElement = xmlDoc.FirstChildElement()->FirstChildElement("ErrorList");
    tinyxml2::XMLElement* xmlErrorEntry = xmlErrorListElement->FirstChildElement("ErrorEntry");
    while (xmlErrorEntry)
    {
        const int id = xmlErrorEntry->IntAttribute("id");
        const char *xmlMsg = xmlErrorEntry->Attribute("msg");

        wchar_t *pWchar = UV_Char2Wchar(xmlMsg);
        gErrorObj->errMap.insert(std::pair<int, wchar_t*>(id, pWchar));

        xmlErrorEntry = xmlErrorEntry->NextSiblingElement("ErrorEntry");
    }

    return true;
}

UNIV_API void UV_InitError()
{
    if (gErrorObj == nullptr)
        gErrorObj = NEW CUvError;
}
UNIV_API void UV_ClearError()
{
    if (gErrorObj)
        delete gErrorObj ;
    gErrorObj = nullptr;
}
