#pragma once
#include "Model.h"
#include <fcntl.h>

#include <stdio.h>

#define FILENAME_SIZE      FILENAME_MAX
#define WFILENAME_SIZE     FILENAME_MAX
#define FILEBLOCK_SIZE     10240 // 10k
#define MAX_FILEBLOCK_SIZE 1048576 // 1meg

#define UV_FILE_FILE        0
#define UV_FILE_MEM         1

namespace UvModel
{

    typedef struct UV_FILE
    {
        int                 type;  // memory file or file
        size_t              pos;  // current pos
        size_t              filesize;      // file or mem size
        wchar_t             filename[FILENAME_SIZE];
        unsigned char       *buffer;
        FILE                *fp;
    } UV_FILE;

    UNIV_API UV_FILE*    uv_fopen(const char *filename, const char *mode, unsigned char *memBuf, size_t memBufSize);
    UNIV_API UV_FILE*    uv_wfopen(const wchar_t *filename, const wchar_t *mode, unsigned char *memBuf, size_t memBufSize);
    UNIV_API UV_FILE*    uv_wfopen_2mem(const wchar_t *filename, const wchar_t *mode);
    UNIV_API int         uv_wfopenf(UV_FILE *bsFile, const wchar_t *filename, const wchar_t *mode, unsigned char *memBuf, size_t memBufSize);
    UNIV_API UV_FILE*    uv_createMemFile(size_t memBufSize);

    UNIV_API int         uv_isMemFile(UV_FILE* fp);

    UNIV_API size_t      uv_flength(UV_FILE *fp);
    UNIV_API char*       uv_fgets(char *string, int n, UV_FILE* fp); //fgets
    UNIV_API size_t      uv_fread(void *buffer, size_t size, size_t count, UV_FILE *fp);

    UNIV_API int         uv_fprintf(UV_FILE* fp, const char *format, ...);
    UNIV_API int         uv_fputc(int c, UV_FILE* fp);
    UNIV_API int         uv_fputs(const char *string, UV_FILE* fp);
    UNIV_API size_t      uv_fwrite(const void *buffer, size_t size, size_t count, UV_FILE* fp);

    UNIV_API int         uv_fclose(UV_FILE* fp, bool dataToo = true);
    UNIV_API int         uv_feof(UV_FILE* fp);
    UNIV_API int         uv_ferror(UV_FILE* fp);
    UNIV_API int         uv_fflush(UV_FILE* fp);
    UNIV_API int         uv_fseek(UV_FILE* fp, int offset, int origin);
    UNIV_API void        uv_rewind(UV_FILE* fp);
    UNIV_API int         uv_ftell(UV_FILE* fp);
    UNIV_API int         uv_setvbuf(UV_FILE* fp, char *buffer, int mode, size_t size);
    UNIV_API int         uv_unlink(const char *filename);
    UNIV_API int         uv_wunlink(const wchar_t *filename);
    UNIV_API int         uv_access(const wchar_t *path, int mode);

}; // RenderDoc