#pragma once
#include "CUvShape.h"
#include "Model.h"
#include "CUvFont.h"
#include "CUvMatrix.h"
#include "Codepage.h"

class UNIV_API CUvText : public CUvShape
{
public:
    CUvText();
    virtual ~CUvText();

    CUvText* Copy();
    void CopyFrom(CUvText *p);
    virtual void GetBound(CUvRectF &rect);

    UV_CODEPAGE codepage;
    UvTextEncoding encoding;
    UvTextAlign align;
    U8 *text;
    S32 textLen;
    U8 *convertedText;
    S32 convertedTextLen;
    float x, y;
    CUvMatrix mat;
    float scaleX;
    float orientation;

    float fontSize;
    CUvFontFace *pFontFace;
    bool bReleaseFontFace;
};

