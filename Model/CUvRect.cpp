#include "stdafx.h"
#include "CUvRect.h"
#include "DebugNew.h"


////////////////////////////////////////////////////
CUvRect::CUvRect()
    :left(0), right(0), top(0), bottom(0)
{
}


CUvRect::~CUvRect()
{
}

CUvRect* CUvRect::Copy()
{
    CUvRect *p = NEW CUvRect();
    p->CopyFrom(this);

    return p;
}

void CUvRect::CopyFrom(CUvRect *p)
{
    if (p == nullptr) return;

    left = p->left;
    right = p->right;
    top = p->top;
    bottom = p->bottom;
}

void CUvRect::Set(CUvPoint &pt)
{
    left = right = pt.x;
    top = bottom = pt.y;
}

void CUvRect::Set(CUvPoint pt[], int count)
{
    if (count <= 0) return;

    Set(pt[0]);
    for (int i = 1; i < count; i++)
        Add(pt[i]);
}

void CUvRect::Set(int _left, int _right, int _top, int _bottom)
{
    left = _left;
    right = _right;
    top = _top;
    bottom = _bottom;
}

void CUvRect::Add(CUvPoint &pt)
{
    if (pt.x < left)
    {
        left = pt.x;
    }
    else if (pt.x > right)
    {
        right = pt.x;
    }
    if (pt.y < top)
    {
        top = pt.y;
    }
    else if (pt.y > bottom)
    {
        bottom = pt.y;
    }
}
int CUvRect::Width()
{
    return right - left + 1;
}
int CUvRect::Height()
{
    return bottom - top + 1;
}

//////////////////////////////////////////////////////
//CUvRectShape::CUvRectShape()
//{
//    type = uvRect_ShapeType;
//}
//
//
//CUvRectShape::~CUvRectShape()
//{
//}
//
//CUvShape* CUvRectShape::Copy()
//{
//    CUvRectShape *p = NEW CUvRectShape();
//    p->CopyFrom(this);
//
//    return p;
//}
//
//void CUvRectShape::CopyFrom(CUvRectShape *p)
//{
//    if (p == nullptr) return;
//
//    CUvShape::CopyFrom(p);
//    CUvRect::CopyFrom(p);
//}

/////////////////////////////////////////////////////////////////
CUvRectF::CUvRectF()
    :left(0), right(0), top(0), bottom(0)
{
}


CUvRectF::~CUvRectF()
{
}

CUvRectF* CUvRectF::Copy()
{
    CUvRectF *p = NEW CUvRectF();
    p->CopyFrom(this);

    return p;
}

void CUvRectF::CopyFrom(CUvRectF *p)
{
    if (p == nullptr) return;

    left = p->left;
    right = p->right;
    top = p->top;
    bottom = p->bottom;
}

void CUvRectF::Set(CUvPointF &pt)
{
    left = right = pt.x;
    top = bottom = pt.y;
}

void CUvRectF::Set(CUvPointF pt[], int count)
{
    if (count <= 0) return;

    Set(pt[0]);
    for (int i = 1; i < count; i++)
        Add(pt[i]);
}

void CUvRectF::Set(float _left, float _right, float _top, float _bottom)
{
    left = _left;
    right = _right;
    top = _top;
    bottom = _bottom;
}

void CUvRectF::Add(CUvPointF &pt)
{
    if (pt.x < left)
    {
        left = pt.x;
    }
    else if (pt.x > right)
    {
        right = pt.x;
    }
    if (pt.y < top)
    {
        top = pt.y;
    }
    else if (pt.y > bottom)
    {
        bottom = pt.y;
    }
}
void CUvRectF::AddX(float v)
{
    left += v;
    right += v;
}
void CUvRectF::AddY(float v)
{
    top += v;
    bottom += v;
}
void CUvRectF::AddXY(float x, float y)
{
    AddX(x);
    AddY(y);
}

float CUvRectF::Width()
{
    return right - left;
}
float CUvRectF::Height()
{
    return bottom - top;
}

////////////////////////////////////////////////////
CUvRectFShape::CUvRectFShape()
{
    type = uvRectF_ShapeType;
}


CUvRectFShape::~CUvRectFShape()
{
}

CUvShape* CUvRectFShape::Copy()
{
    CUvRectFShape *p = NEW CUvRectFShape();
    p->CopyFrom(this);

    return p;
}

void CUvRectFShape::CopyFrom(CUvRectFShape *p)
{
    if (p == nullptr) return;

    CUvShape::CopyFrom(p);
    CUvRectF::CopyFrom(p);
}

void CUvRectFShape::GetBound(CUvRectF &rect)
{
    rect.Set(left, right, top, bottom);
}


