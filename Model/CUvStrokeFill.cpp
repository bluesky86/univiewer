#include "stdafx.h"
#include "CUvStrokeFill.h"
#include "UvmUtils.h"
#include "CUvImage.h"
#include "DebugNew.h"

////////////////////////////////////////////////////////////////////
class CUvColorFill
{
public:
    CUvColorFill();
    virtual ~CUvColorFill();

    virtual void CopyFrom(CUvColorFill *src);
    CVector<CUvColor>* GetColors();
    UvFillType GetFillType();

protected:
    UvFillType type;

private:
    CVector<CUvColor> colors;
};

CUvColorFill::CUvColorFill()
    :type(uvColor_FillType)
{
}
CUvColorFill::~CUvColorFill()
{
}

void CUvColorFill::CopyFrom(CUvColorFill *src)
{
    colors.CopyFrom(&src->colors);
    type = src->type;
}

UvFillType CUvColorFill::GetFillType()
{
    return type;
};

CVector<CUvColor>* CUvColorFill::GetColors()
{
    return &colors;
}

//////////////////////////////////////////////////////////////
class CUvTileFill : public CUvColorFill
{
public:
    CUvTileFill();
    ~CUvTileFill();

    virtual void CopyFrom(CUvTileFill *src);
    UvTileMode GetHoriTileMode();
    void SetHoriTileMode(UvTileMode v);
    UvTileMode GetVertTileMode();
    void SetVertTileMode(UvTileMode v);

private:
    UvTileMode horiTileMode;
    UvTileMode vertTileMode;

};

CUvTileFill::CUvTileFill()
    :horiTileMode(uvNone_TileMode), vertTileMode(uvNone_TileMode)
{

}
CUvTileFill::~CUvTileFill()
{

}
void CUvTileFill::CopyFrom(CUvTileFill *src)
{
    CUvColorFill::CopyFrom(src);

    SetHoriTileMode(src->GetHoriTileMode());
    SetVertTileMode(src->GetVertTileMode());
}

UvTileMode CUvTileFill::GetHoriTileMode()
{
    return horiTileMode;
}
void CUvTileFill::SetHoriTileMode(UvTileMode v)
{
    horiTileMode = v;
}
UvTileMode CUvTileFill::GetVertTileMode()
{
    return vertTileMode;
}
void CUvTileFill::SetVertTileMode(UvTileMode v)
{
    vertTileMode = v;
}

///////////////////////////////////////////////////////////////////
class CUvLinearGradientFill : public CUvTileFill
{
public:
    CUvLinearGradientFill();
    virtual ~CUvLinearGradientFill();

    virtual void CopyFrom(CUvLinearGradientFill *src);

    CUvPointFs* GetPoints();
    void SetPoints(CUvPointFs *src);
    void SetPoints(CUvPointF &pt1, CUvPointF &pt2);
    CVector<float>* GetColorPos();
    void SetColorPos(CVector<float>* src);

private:
    CUvPointFs points;
    CVector<float> *colorPos;
};

CUvLinearGradientFill::CUvLinearGradientFill()
    : colorPos(nullptr)
{
    type = uvLinearGradient_FillType;
}
CUvLinearGradientFill::~CUvLinearGradientFill()
{
    if (colorPos) delete colorPos;
}

void CUvLinearGradientFill::CopyFrom(CUvLinearGradientFill *src)
{
    CUvTileFill::CopyFrom(src);

    points.CopyFrom(src->GetPoints());
    if (src->GetColorPos())
    {
        colorPos = NEW CVector<float>;
        colorPos->CopyFrom(src->colorPos);
    }
}

CUvPointFs* CUvLinearGradientFill::GetPoints()
{
    return &points;
}
void CUvLinearGradientFill::SetPoints(CUvPointFs* src)
{
    if (src == nullptr) return;

    points.points.clear();

    for (U32 i = 0; i < src->points.size(); i++)
        points.points.push_back(src->points[i]);
}
void CUvLinearGradientFill::SetPoints(CUvPointF &pt1, CUvPointF &pt2)
{
    points.points.clear();
    points.points.push_back(pt1);
    points.points.push_back(pt2);
}
CVector<float>* CUvLinearGradientFill::GetColorPos()
{
    return colorPos;
}
void CUvLinearGradientFill::SetColorPos(CVector<float>* src)
{
    if (src == nullptr || src->size() == 0) return;

    if (colorPos == nullptr)
        colorPos = NEW CVector<float>;
    for (U32 i = 0; i < src->size(); i++)
        colorPos->push_back((*src)[i]);
}



////////////////////////////////////////////////////////////////////
class CUvRadialGradientFill : public CUvTileFill
{
public:
    CUvRadialGradientFill();
    virtual ~CUvRadialGradientFill();

    virtual void CopyFrom(CUvRadialGradientFill *src);

    CUvPointF* GetCenter();
    void SetCenter(CUvPointF v);
    float* GetRadius();
    void SetRadius(float v);
    CVector<float>* GetColorPos();
    void SetColorPos(CVector<float>* src);

private:
    CUvPointF *center;
    float *radius;
    CVector<float> *colorPos;
};

CUvRadialGradientFill::CUvRadialGradientFill()
    :center(nullptr), radius(nullptr), colorPos(nullptr)
{
    type = uvRadialGradient_FillType;
}
CUvRadialGradientFill::~CUvRadialGradientFill()
{
    if (center) delete center;
    if (radius) delete radius;
    if (colorPos) delete colorPos;
}

void CUvRadialGradientFill::CopyFrom(CUvRadialGradientFill *src)
{
    CUvTileFill::CopyFrom(src);

    if (src->GetCenter())
        SetCenter(*src->GetCenter());
    if (src->GetRadius())
        SetRadius(*src->GetRadius());
    if (src->GetColorPos())
    {
        colorPos = NEW CVector<float>;
        colorPos->CopyFrom(src->colorPos);
    }
}

CUvPointF* CUvRadialGradientFill::GetCenter()
{
    return center;
}
void CUvRadialGradientFill::SetCenter(CUvPointF v)
{
    if (center == nullptr)
        center = NEW CUvPointF();
    center->x = v.x;
    center->y = v.y;
}
float* CUvRadialGradientFill::GetRadius()
{
    return radius;
}
void CUvRadialGradientFill::SetRadius(float v)
{
    if (radius == nullptr)
        radius = NEW float;
    *radius = v;
}

CVector<float>* CUvRadialGradientFill::GetColorPos()
{
    return colorPos;
}
void CUvRadialGradientFill::SetColorPos(CVector<float>* src)
{
    if (src == nullptr || src->size() == 0) return;

    if (colorPos == nullptr)
        colorPos = NEW CVector<float>;
    for (U32 i = 0; i < src->size(); i++)
        colorPos->push_back((*src)[i]);
}

///////////////////////////////// raster fill ////////////////////////////
class CUvObjectFill : public CUvTileFill
{
public:
    CUvObjectFill();
    ~CUvObjectFill();

    virtual void CopyFrom(CUvObjectFill *src);
    CUvTileFill* Copy();

    CUvShape* GetObj();
    void SetObj(CUvShape *shp, bool _isRef);

    //const char* GetHref();
    //void SetHref(const char *ref);
    //int* GetImageWidth();
    //void SetImageWidth(int v);
    //int* GetImageHeight();
    //void SetImageHeight(int v);
    int* GetTargetWidth();
    void SetTargetWidth(int v);
    int* GetTargetHeight();
    void SetTargetHeight(int v);

private:
    //const char *href;
    //int *imageWidth, *imageHeight;

    CUvShape *pShape;
    bool isRef;

    int *targetWidth;
    int *targetHeight;
};

CUvObjectFill::CUvObjectFill()
    : pShape(nullptr), isRef(false), targetWidth(0), targetHeight(0)

{
    type = uvObject_FillType;
}

CUvObjectFill::~CUvObjectFill()
{
    if (targetWidth)
        delete targetWidth;

    if (targetHeight)
        delete targetHeight;

    if (pShape && !isRef)
        delete pShape;
}

CUvTileFill* CUvObjectFill::Copy()
{
    CUvObjectFill *p = NEW CUvObjectFill();
    p->CopyFrom(this);

    return p;
}

void CUvObjectFill::CopyFrom(CUvObjectFill *p)
{
    if (p == nullptr) return;

    CUvTileFill::CopyFrom(p);

    if (p->pShape)
        pShape->CopyFrom(p->pShape);

    //UV_AllocCopyStr((const char*)p->href, (const char**)&href);

    //if (p->imageWidth)
    //{
    //    imageWidth = NEW int;
    //    *imageWidth = *p->imageWidth;
    //}

    //if (p->imageHeight)
    //{
    //    imageHeight = NEW int;
    //    *imageHeight = *p->imageHeight;
    //}

    if (p->targetWidth)
    {
        targetWidth = NEW int;
        *targetWidth = *p->targetWidth;
    }

    if (p->targetHeight)
    {
        targetHeight = NEW int;
        *targetHeight = *p->targetHeight;
    }
}

CUvShape* CUvObjectFill::GetObj()
{
    return pShape;
}
void CUvObjectFill::SetObj(CUvShape *shp, bool _isRef)
{
    if (shp == nullptr) return;

    if (pShape)
        delete pShape;
    
    pShape = shp;
    isRef = _isRef;
}

//const char* CUvObjectFill::GetHref()
//{
//    if (pImage == nullptr) return nullptr;
//    return pImage->href;
//}
//void CUvObjectFill::SetHref(const char *ref)
//{
//    UV_AllocCopyStr((const char*)ref, (const char**)&href);
//}
//int* CUvObjectFill::GetImageWidth()
//{
//    return imageWidth;
//}
//void CUvObjectFill::SetImageWidth(int v)
//{
//    if (imageWidth == nullptr)
//        imageWidth = NEW int;
//    *imageWidth = v;
//}
//int* CUvObjectFill::GetImageHeight()
//{
//    return imageHeight;
//}
//void CUvObjectFill::SetImageHeight(int v)
//{
//    if (imageHeight == nullptr)
//        imageHeight = NEW int;
//    *imageHeight = v;
//}
int* CUvObjectFill::GetTargetWidth()
{
    return targetWidth;
}
void CUvObjectFill::SetTargetWidth(int v)
{
    if (targetWidth == nullptr)
        targetWidth = NEW int;
    *targetWidth = v;
}
int* CUvObjectFill::GetTargetHeight()
{
    return targetHeight;
}
void CUvObjectFill::SetTargetHeight(int v)
{
    if (targetHeight == nullptr)
        targetHeight = NEW int;
    *targetHeight = v;
}


//////////////////////////////////////////////////
class CUvFill
{
public:
    CUvFill();
    ~CUvFill();

    void CopyFrom(CUvFill *src);

    CVector<CUvColor>* GetColorArray();
    UvFillType GetFillType();  // return value rather than pointer: fill type is readonly. The change of fill type must be done by SetFillType
    void SetFillType(UvFillType v);
    UvFillMode GetFillMode();
    void SetFillMode(UvFillMode v);
    CVector<CUvColor>* GetColors();
    UvTileMode GetHoriTileMode();
    bool SetHoriTileMode(UvTileMode v);
    UvTileMode GetVertTileMode();
    bool SetVertTileMode(UvTileMode v);

    CUvPointFs* GetLinearGradientPoints();
    bool SetLinearGradientPoints(CUvPointFs *src);
    bool SetLinearGradientPoints(CUvPointF &pt1, CUvPointF &pt2);
    CVector<float>* GetLinearGradientColorPos();
    bool SetLinearGradientColorPos(CVector<float>* src);

    CUvPointF* GetRadialGradientCenter();
    bool SetRadialGradientCenter(CUvPointF &point);
    float* GetRadialGradientRadius();
    bool SetRadialGradientRadius(float v);
    CVector<float>* GetRadialGradientColorPos();
    bool SetRadialGradientColorPos(CVector<float>* src);

    CUvShape* GetFillObj();
    bool SetFillObj(CUvShape *shp, bool isRef);
    int* GetTargetFillObjWidth();
    bool SetTargetFillObjWidth(int v);
    int* GetTargetFillObjHeight();
    bool SetTargetFillObjHeight(int v);


private:
    void InitFillArgs(UvFillType type);

private:
    UvFillMode fillMode;
    CUvColorFill *pFillInfo;
};



////////////////////////////////////////////////////////////////////
CUvFill::CUvFill()
    :fillMode(uvNone_FillMode), pFillInfo(0)
{

}
CUvFill::~CUvFill()
{
    if (pFillInfo)
        delete pFillInfo;
}

void CUvFill::CopyFrom(CUvFill *src)
{
    if (src)
    {
        fillMode = src->fillMode;
        pFillInfo->CopyFrom(src->pFillInfo);
    }
}

void CUvFill::InitFillArgs(UvFillType type)
{
    if (pFillInfo)
    {
        if (pFillInfo->GetFillType() != type)
        {
            delete pFillInfo;
            pFillInfo = nullptr;
        }
        else return;
    }

    switch (type)
    {
    case uvColor_FillType:
        pFillInfo = NEW CUvColorFill();
        break;
    case uvLinearGradient_FillType:
        pFillInfo = NEW CUvLinearGradientFill;
        break;
    case uvRadialGradient_FillType:
        pFillInfo = NEW CUvRadialGradientFill();
        break;
    case uvRaster_FillType:
    case uvObject_FillType:
        pFillInfo = NEW CUvObjectFill();
        break;
    default:
        pFillInfo = NEW CUvColorFill();
    }
}

CVector<CUvColor>* CUvFill::GetColorArray()
{
    if (!pFillInfo || (pFillInfo->GetFillType() != uvColor_FillType && pFillInfo->GetFillType() != uvLinearGradient_FillType && pFillInfo->GetFillType() != uvRadialGradient_FillType))
        return nullptr;

    return pFillInfo->GetColors();
}

UvFillType CUvFill::GetFillType()
{
    if (pFillInfo == nullptr) return uvNone_FillType;

    return pFillInfo->GetFillType();
}
void CUvFill::SetFillType(UvFillType v)
{
    InitFillArgs(v);
}
UvFillMode CUvFill::GetFillMode()
{
    return fillMode;
}
void CUvFill::SetFillMode(UvFillMode v)
{
    fillMode = v;
}
CVector<CUvColor>* CUvFill::GetColors()
{
    if (pFillInfo == nullptr) return nullptr;
    return pFillInfo->GetColors();
}
UvTileMode CUvFill::GetHoriTileMode()
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() == uvNone_FillType || pFillInfo->GetFillType() == uvColor_FillType) return uvNone_TileMode;
    return ((CUvTileFill*)pFillInfo)->GetHoriTileMode();
}
bool CUvFill::SetHoriTileMode(UvTileMode v)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() == uvNone_FillType || pFillInfo->GetFillType() == uvColor_FillType) return false;
    ((CUvTileFill*)pFillInfo)->SetHoriTileMode(v);
    return true;
}
UvTileMode CUvFill::GetVertTileMode()
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() == uvNone_FillType || pFillInfo->GetFillType() == uvColor_FillType) return uvNone_TileMode;
    return ((CUvTileFill*)pFillInfo)->GetVertTileMode();
}
bool CUvFill::SetVertTileMode(UvTileMode v)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() == uvNone_FillType || pFillInfo->GetFillType() == uvColor_FillType) return false;
    ((CUvTileFill*)pFillInfo)->SetVertTileMode(v);
    return true;
}

CUvPointFs* CUvFill::GetLinearGradientPoints()
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvLinearGradient_FillType) return nullptr;
    return ((CUvLinearGradientFill*)pFillInfo)->GetPoints();
}
bool CUvFill::SetLinearGradientPoints(CUvPointFs *src)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvLinearGradient_FillType) return false;
    ((CUvLinearGradientFill*)pFillInfo)->SetPoints(src);
    return true;
}
bool CUvFill::SetLinearGradientPoints(CUvPointF &pt1, CUvPointF &pt2)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvLinearGradient_FillType) return false;
    ((CUvLinearGradientFill*)pFillInfo)->SetPoints(pt1, pt2);
    return true;
}
CVector<float>* CUvFill::GetLinearGradientColorPos()
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvLinearGradient_FillType) return nullptr;
    return ((CUvLinearGradientFill*)pFillInfo)->GetColorPos();
}
bool CUvFill::SetLinearGradientColorPos(CVector<float>* src)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvLinearGradient_FillType) return false;
    ((CUvLinearGradientFill*)pFillInfo)->SetColorPos(src);
    return true;
}
CUvPointF* CUvFill::GetRadialGradientCenter()
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvRadialGradient_FillType) return nullptr;
    return ((CUvRadialGradientFill*)pFillInfo)->GetCenter();
}
bool CUvFill::SetRadialGradientCenter(CUvPointF &point)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvRadialGradient_FillType) return false;
    ((CUvRadialGradientFill*)pFillInfo)->SetCenter(point);
    return true;
}
float* CUvFill::GetRadialGradientRadius()
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvRadialGradient_FillType) return nullptr;
    return ((CUvRadialGradientFill*)pFillInfo)->GetRadius();
}
bool CUvFill::SetRadialGradientRadius(float v)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvRadialGradient_FillType) return false;
    ((CUvRadialGradientFill*)pFillInfo)->SetRadius(v);
    return true;
}
CVector<float>* CUvFill::GetRadialGradientColorPos()
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvRadialGradient_FillType) return nullptr;
    return ((CUvRadialGradientFill*)pFillInfo)->GetColorPos();
}
bool CUvFill::SetRadialGradientColorPos(CVector<float>* src)
{
    if (pFillInfo == nullptr || pFillInfo->GetFillType() != uvRadialGradient_FillType) return false;
    ((CUvRadialGradientFill*)pFillInfo)->SetColorPos(src);
    return true;
}
CUvShape* CUvFill::GetFillObj()
{
    if (pFillInfo == nullptr || (pFillInfo->GetFillType() != uvRaster_FillType && pFillInfo->GetFillType() != uvObject_FillType)) return nullptr;
    return ((CUvObjectFill*)pFillInfo)->GetObj();
}
bool CUvFill::SetFillObj(CUvShape *shp, bool isRef)
{
    if (pFillInfo == nullptr || (pFillInfo->GetFillType() != uvRaster_FillType && pFillInfo->GetFillType() != uvObject_FillType)) return false;
    ((CUvObjectFill*)pFillInfo)->SetObj(shp, isRef);
    return true;
}
int* CUvFill::GetTargetFillObjWidth()
{
    if (pFillInfo == nullptr || (pFillInfo->GetFillType() != uvRaster_FillType && pFillInfo->GetFillType() != uvObject_FillType)) return nullptr;
    return ((CUvObjectFill*)pFillInfo)->GetTargetWidth();
}
bool CUvFill::SetTargetFillObjWidth(int v)
{
    if (pFillInfo == nullptr || (pFillInfo->GetFillType() != uvRaster_FillType && pFillInfo->GetFillType() != uvObject_FillType)) return false;
    ((CUvObjectFill*)pFillInfo)->SetTargetWidth(v);
    return true;
}
int* CUvFill::GetTargetFillObjHeight()
{
    if (pFillInfo == nullptr || (pFillInfo->GetFillType() != uvRaster_FillType && pFillInfo->GetFillType() != uvObject_FillType)) return nullptr;
    return ((CUvObjectFill*)pFillInfo)->GetTargetHeight();
}
bool CUvFill::SetTargetFillObjHeight(int v)
{
    if (pFillInfo == nullptr || (pFillInfo->GetFillType() != uvRaster_FillType && pFillInfo->GetFillType() != uvObject_FillType)) return false;
    ((CUvObjectFill*)pFillInfo)->SetTargetHeight(v);
    return true;
}

////////////////////////////////////////////////////////////////////
// check if that attribute is provided
typedef enum
{
    strokeFill_StrokeFillMask = 0x0001,
    color_StrokeFillMask = 0x0002,
    strokeWidth_StrokeFillMask = 0x0004,
    mitLimit_StrokeFillMask = 0x0008,
    strokeCap_StrokeFillMask = 0x0010,
    strokeJoin_StrokeFillMask = 0x0020,
    lineStyle_StrokeFillMask = 0x0040,
    //fillMode_StrokeFillMask = 0x0080,
    //fillType_StrokeFillMask = 0x0100,
    //fillColor_StrokeFillMask = 0x0200,
} _UvStrokeFillMask;

CUvStrokeFill::CUvStrokeFill(CUvColor &_color, UvStrokeFill _strokeFill /*= uvStoke_StrokeFill*/,
    int _strokeWidth /*= 0*/,
    int _mitLimit /*= 4*/,
    UvStrokeCap _strokeCap /*= uvButt_StrokeCap*/,
    UvStrokeJoin _strokeJoin /*= uvMiter_StrokeJoin*/,
    UvDashStyle _lineStyle /*= uvSolid_DashStyle*/,
    float* _intervals /*= nullptr*/)
    : color(_color), strokeFill(_strokeFill), strokeWidth(_strokeWidth), mitLimit(_mitLimit), strokeCap(_strokeCap), strokeJoin(_strokeJoin), lineStyle(_lineStyle),
    pFillArgs(nullptr), intervals(_intervals), attrMask(0)
{
    attrMask |= strokeFill_StrokeFillMask;
    attrMask |= color_StrokeFillMask;
    attrMask |= strokeWidth_StrokeFillMask;
    attrMask |= mitLimit_StrokeFillMask;
    attrMask |= strokeCap_StrokeFillMask;
    attrMask |= strokeJoin_StrokeFillMask;
    attrMask |= lineStyle_StrokeFillMask;
}

CUvStrokeFill::CUvStrokeFill()
    : strokeFill(uvNone_StrokeFill), strokeWidth(0), mitLimit(4), strokeCap(uvButt_StrokeCap), strokeJoin(uvMiter_StrokeJoin), lineStyle(uvSolid_DashStyle),
    pFillArgs(nullptr), intervals(nullptr), attrMask(0)
{
}

CUvStrokeFill::~CUvStrokeFill()
{
    if (intervals)
        delete[] intervals;

    if (pFillArgs)
        delete (CUvFill*)pFillArgs;
}
void CUvStrokeFill::CopyFrom(CUvStrokeFill *src)
{
    if (src == nullptr) return;

    strokeFill = src->strokeFill;
    color = src->color;
    strokeWidth = src->strokeWidth;
    mitLimit = src->mitLimit;
    strokeCap = src->strokeCap;
    strokeJoin = src->strokeJoin;
    lineStyle = src->lineStyle;
    attrMask = src->attrMask;
    if (src->intervals && src->lineStyle > 0)
    {
        intervals = NEW float[src->lineStyle];
        memcpy(intervals, src->intervals, sizeof(float) * src->lineStyle);
    }

    if (src->pFillArgs)
    {
        pFillArgs = NEW CUvFill();
        ((CUvFill*)pFillArgs)->CopyFrom((CUvFill*)src->pFillArgs);
    }
}

UvStrokeFill* CUvStrokeFill::GetStrokeFill()
{
    if(attrMask & strokeFill_StrokeFillMask)
        return &strokeFill;
    return nullptr;
}
void CUvStrokeFill::SetStrokeFill(UvStrokeFill sf)
{
    strokeFill = sf;
    attrMask |= strokeFill_StrokeFillMask;
}
CUvColor* CUvStrokeFill::GetStrokeColor()
{
    if(attrMask & color_StrokeFillMask)
        return &color;
    return nullptr;
}
void CUvStrokeFill::SetStrokeColor(CUvColor &c)
{
    color = c;
    attrMask |= color_StrokeFillMask;
}
int* CUvStrokeFill::GetStrokeWidth()
{
    if (attrMask & strokeWidth_StrokeFillMask)
        return &strokeWidth;
    return nullptr;
}
void CUvStrokeFill::SetStrokeWidth(int w)
{
    strokeWidth = w;
    attrMask |= strokeWidth_StrokeFillMask;
}
int* CUvStrokeFill::GetMitLimit()
{
    if (attrMask & mitLimit_StrokeFillMask)
        return &mitLimit;
    return nullptr;
}
void CUvStrokeFill::SetMitLimit(int m)
{
    mitLimit = m;
    attrMask |= mitLimit_StrokeFillMask;
}
UvStrokeCap* CUvStrokeFill::GetStrokeCap()
{
    if (attrMask & strokeCap_StrokeFillMask)
        return &strokeCap;
    return nullptr;
}
void CUvStrokeFill::SetStrokeCap(UvStrokeCap v)
{
    strokeCap = v;
    attrMask |= strokeCap_StrokeFillMask;
}
UvStrokeJoin* CUvStrokeFill::GetStrokeJoin()
{
    if (attrMask & strokeJoin_StrokeFillMask)
        return &strokeJoin;
    return nullptr;
}
void CUvStrokeFill::SetStrokeJoin(UvStrokeJoin v)
{
    strokeJoin = v;
    attrMask |= strokeJoin_StrokeFillMask;
}
int* CUvStrokeFill::GetLineStyle()
{
    if (attrMask & lineStyle_StrokeFillMask)
        return &lineStyle;
    return nullptr;
}
void CUvStrokeFill::SetLineStyle(int v)
{
    lineStyle = v;
    attrMask |= lineStyle_StrokeFillMask;
}
float* CUvStrokeFill::GetDashIntervals()
{
    return intervals;
}
void CUvStrokeFill::SetDashIntervals(float* v)
{
    if (intervals) delete[] intervals;
    intervals = v;
}

UvFillMode CUvStrokeFill::GetFillMode()
{
    if (pFillArgs == nullptr) return uvNone_FillMode;
    return ((CUvFill*)pFillArgs)->GetFillMode();
}

void CUvStrokeFill::SetFillMode(UvFillMode v)
{
    if (pFillArgs == nullptr)
        pFillArgs = NEW CUvFill();
    ((CUvFill*)pFillArgs)->SetFillMode(v);
}
UvFillType CUvStrokeFill::GetFillType()
{
    if (pFillArgs == nullptr) return uvNone_FillType;
    return ((CUvFill*)pFillArgs)->GetFillType();
}
void CUvStrokeFill::SetFillType(UvFillType v)
{
    if (pFillArgs == nullptr)
        pFillArgs = NEW CUvFill();
    ((CUvFill*)pFillArgs)->SetFillType(v);
}
int CUvStrokeFill::GetNumOfFillColors()
{
    if (pFillArgs == nullptr) return 0;
    CVector<CUvColor>* colors = ((CUvFill*)pFillArgs)->GetColors();
    if (colors == nullptr) return 0;
    return (int)colors->size();
}
CUvColor* CUvStrokeFill::GetFillColor(int index)
{
    if (pFillArgs == nullptr) return nullptr;
    CVector<CUvColor>* colors = ((CUvFill*)pFillArgs)->GetColors();
    if (colors == nullptr || index < 0 || index >= (int)colors->size()) return nullptr;
    return &(*colors)[index];
}
int CUvStrokeFill::AppendFillColor(CUvColor& c)
{
    if (pFillArgs == nullptr) return -1;
    CVector<CUvColor>* colors = ((CUvFill*)pFillArgs)->GetColors();
    if (colors == nullptr) return -1;
    colors->push_back(c);

    return (int)colors->size();
}
void CUvStrokeFill::RemoveFillColor(int index)
{
    if (pFillArgs == nullptr) return;
    CVector<CUvColor>* colors = ((CUvFill*)pFillArgs)->GetColors();
    if (colors == nullptr || index < 0 || index >= (int)colors->size()) return;
    colors->pop(index);
}
void CUvStrokeFill::ClearFillColors()
{
    if (pFillArgs == nullptr) return;
    CVector<CUvColor>* colors = ((CUvFill*)pFillArgs)->GetColors();
    if (colors == nullptr) return;
    colors->clear();
}
void CUvStrokeFill::CopyFillColors(CVector<CUvColor> &colors)
{
    if (pFillArgs == nullptr) return;
    CVector<CUvColor>* pColors = ((CUvFill*)pFillArgs)->GetColors();
    if (pColors == nullptr) return;
    pColors->CopyFrom(&colors);
}
UvTileMode CUvStrokeFill::GetHoriTileMode()
{
    if (pFillArgs == nullptr) return uvNone_TileMode;
    return ((CUvFill*)pFillArgs)->GetHoriTileMode();
}
bool CUvStrokeFill::SetHoriTileMode(UvTileMode v)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetHoriTileMode(v);
}
UvTileMode CUvStrokeFill::GetVertTileMode()
{
    if (pFillArgs == nullptr) return uvNone_TileMode;
    return ((CUvFill*)pFillArgs)->GetVertTileMode();
}
bool CUvStrokeFill::SetVertTileMode(UvTileMode v)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetVertTileMode(v);
}
CUvPointFs* CUvStrokeFill::GetLinearGradientPoints()
{
    if (pFillArgs == nullptr) return nullptr;
    return ((CUvFill*)pFillArgs)->GetLinearGradientPoints();
}
bool CUvStrokeFill::SetLinearGradientPoints(CUvPointFs *src)
{
    if (pFillArgs == nullptr) return false;
    return ((CUvFill*)pFillArgs)->SetLinearGradientPoints(src);
}
bool CUvStrokeFill::SetLinearGradientPoints(CUvPointF &pt1, CUvPointF &pt2)
{
    if (pFillArgs == nullptr) return false;
    return ((CUvFill*)pFillArgs)->SetLinearGradientPoints(pt1, pt2);
}
CUvPointF* CUvStrokeFill::GetRadialGradientCenter()
{
    if (pFillArgs == nullptr) return nullptr;
    return ((CUvFill*)pFillArgs)->GetRadialGradientCenter();
}
bool CUvStrokeFill::SetRadialGradientCenter(CUvPointF &point)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetRadialGradientCenter(point);
}
float* CUvStrokeFill::GetRadialGradientRadius()
{
    if (pFillArgs == nullptr) return nullptr;
    return ((CUvFill*)pFillArgs)->GetRadialGradientRadius();
}
bool CUvStrokeFill::SetRadialGradientRadius(float v)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetRadialGradientRadius(v);
}

CVector<float>* CUvStrokeFill::GetRadialGradientColorPos()
{
    if (pFillArgs == nullptr) return nullptr;
    return ((CUvFill*)pFillArgs)->GetRadialGradientColorPos();
}
bool CUvStrokeFill::SetRadialGradientColorPos(CVector<float>* src)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetRadialGradientColorPos(src);
}
CVector<float>* CUvStrokeFill::GetLinearGradientColorPos()
{
    if (pFillArgs == nullptr) return nullptr;
    return ((CUvFill*)pFillArgs)->GetLinearGradientColorPos();
}
bool CUvStrokeFill::SetLinearGradientColorPos(CVector<float>* src)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetLinearGradientColorPos(src);
}
CUvShape* CUvStrokeFill::GetFillObj()
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->GetFillObj();
}
bool CUvStrokeFill::SetFillObj(CUvShape *shp, bool isRef)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetFillObj(shp, isRef);
}

int* CUvStrokeFill::GetTargetFillObjWidth()
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->GetTargetFillObjWidth();
}
bool CUvStrokeFill::SetTargetFillObjWidth(int v)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetTargetFillObjWidth(v);
}
int* CUvStrokeFill::GetTargetFillObjHeight()
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->GetTargetFillObjHeight();
}
bool CUvStrokeFill::SetTargetFillObjHeight(int v)
{
    if (pFillArgs == nullptr) return false;

    return ((CUvFill*)pFillArgs)->SetTargetFillObjHeight(v);
}
