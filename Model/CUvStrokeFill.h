#pragma once
#include "Model.h"
#include "CUvColor.h"
#include "Vector.h"
#include "CUvPoint.h"

typedef enum _uvFillMode
{
    uvWinding_FillMode,
    uvEvenOdd_FillMode,
    uvInverseWinding_FillMode,
    uvInverseEvenOdd_FillMode,
    uvNone_FillMode,
} UvFillMode;

typedef enum _uvFillType
{
    uvNone_FillType,
    uvColor_FillType,
    uvLinearGradient_FillType,
    uvRadialGradient_FillType,
    uvRaster_FillType,
    uvObject_FillType,
} UvFillType;

typedef enum _uvTileMode {
    /** replicate the edge color if the shader draws outside of its
     *  original bounds
     */
    uvClamp_TileMode,

    /** repeat the shader's image horizontally and vertically */
    uvRepeat_TileMode,

    /** repeat the shader's image horizontally and vertically, alternating
     *  mirror images so that adjacent images always seam
     */
    uvMirror_TileMode,

    /**
     *  Only draw within the original domain, return transparent-black everywhere else.
     *  EXPERIMENTAL -- DO NOT USE YET
     */
    uvDecal_TileMode,

    uvNone_TileMode,  // not set yet
} UvTileMode;



class UNIV_API CUvStrokeFill
{
public:
    CUvStrokeFill(CUvColor &_color, UvStrokeFill _strokeFill = uvStroke_StrokeFill, int _strokeWidth = 0, int _mitLimit = 4,
        UvStrokeCap _strokeCap = uvButt_StrokeCap, UvStrokeJoin _strokeJoin = uvMiter_StrokeJoin, 
        UvDashStyle _lineStyle = uvSolid_DashStyle, float* _intervals = nullptr);
    CUvStrokeFill();
    ~CUvStrokeFill();

    void CopyFrom(CUvStrokeFill *src);

    //////////////////////////////////
    UvStrokeFill* GetStrokeFill();
    void SetStrokeFill(UvStrokeFill sf);

    /////////////////////////////////////////////////////////////////////////////////////////
    // Stroke
    /////////////////////////////////////////////////////////////////////////////////////////

    CUvColor* GetStrokeColor();
    void SetStrokeColor(CUvColor &c);
    int* GetStrokeWidth();
    void SetStrokeWidth(int w);
    int* GetMitLimit();
    void SetMitLimit(int m);
    UvStrokeCap* GetStrokeCap();
    void SetStrokeCap(UvStrokeCap v);
    UvStrokeJoin* GetStrokeJoin();
    void SetStrokeJoin(UvStrokeJoin v);
    int* GetLineStyle();
    void SetLineStyle(int v);
    float* GetDashIntervals();
    void SetDashIntervals(float* v);

    /////////////////////////////////////////////////////////////////////////////////////////
    // Fill
    /////////////////////////////////////////////////////////////////////////////////////////

    UvFillMode GetFillMode();
    void SetFillMode(UvFillMode v);
    UvFillType GetFillType();
    void SetFillType(UvFillType v);
    UvTileMode GetHoriTileMode();
    bool SetHoriTileMode(UvTileMode v); // return false if the fill type wrong
    UvTileMode GetVertTileMode();
    bool SetVertTileMode(UvTileMode v); // return false if the fill type wrong
    ////// fill colors   /////////
    int GetNumOfFillColors();
    CUvColor* GetFillColor(int index);
    int AppendFillColor(CUvColor& c); // return size of colors, -1 if append failed (e.g. need to set fill type first)
    void RemoveFillColor(int index);
    void ClearFillColors();
    void CopyFillColors(CVector<CUvColor> &colors);
    ////// Linear Gradient   /////////
    CUvPointFs* GetLinearGradientPoints();
    bool SetLinearGradientPoints(CUvPointFs *src);
    bool SetLinearGradientPoints(CUvPointF &pt1, CUvPointF &pt2);
    CVector<float>* GetLinearGradientColorPos();
    bool SetLinearGradientColorPos(CVector<float>* src); // color positions must have same number of Fill colors
    ////// Radial Gradient   /////////
    CUvPointF* GetRadialGradientCenter();
    bool SetRadialGradientCenter(CUvPointF &point);
    float* GetRadialGradientRadius();
    bool SetRadialGradientRadius(float v);
    CVector<float>* GetRadialGradientColorPos();
    bool SetRadialGradientColorPos(CVector<float>* src); // color positions must have same number of Fill colors
    ////// Raster or object  /////////
    CUvShape* GetFillObj();
    bool SetFillObj(CUvShape *shp, bool isRef);
    int* GetTargetFillObjWidth();
    bool SetTargetFillObjWidth(int v);
    int* GetTargetFillObjHeight();
    bool SetTargetFillObjHeight(int v);


private:
    UvStrokeFill strokeFill; // storke or fill type

    // Stroke
    CUvColor color;
    int strokeWidth;
    int mitLimit;
    UvStrokeCap strokeCap;
    UvStrokeJoin strokeJoin;
    int lineStyle;
    float *intervals;

    // Fill
    void *pFillArgs;

private:
    unsigned short attrMask;
};

