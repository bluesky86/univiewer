#include "stdafx.h"
#include "Model.h"
#include "Codepage.h"
#include "CUvText.h"

CCodepage *gCodePages = nullptr;

UNIV_API bool uvModelInit( const wchar_t* dllDir )
{
    if(!gCodePages)
    {
        gCodePages = new CCodepage();
        gCodePages->setExePath( dllDir );
    }

    return true;
}

UNIV_API void uvModelClose()
{
    if(gCodePages)
        delete gCodePages;
    gCodePages = nullptr;
}

UNIV_API unsigned int GetCodepageSignature( int codepage )
{
    if(gCodePages)
        return gCodePages->getSignature( codepage );
    return 0x41;
}

UNIV_API const char* GetCodepageBcp47( int codepage )
{
    if(gCodePages)
        return gCodePages->getBcp47( codepage );
    return nullptr;
}

UNIV_API int CharCodeMapping( CUvText *pUvText, unsigned char *dest )
{
    int text_len = 0;
    //CUvFont *pUvFontParent = pUvText->pFontFace->getParent();
    //if(pUvFontParent)// && pUvFontParent->getCharsetType() != CHARSET_TYPE_none)
    //{
    //    CIDFONT_ENCODING baseEncodingStyle = (CIDFONT_ENCODING)pUvFontParent->getBaseEncodingStyle();
    //    if(baseEncodingStyle != 0)
    //    {
    //        //text_len = CMAP_textToCodepageByEncoding( baseEncodingStyle, (unsigned char*)pUvText->text, pUvText->textLen, (unsigned char *)dest, pUvText->textLen * 2 + 2 );
    //    }
    //    //if(pUvFontParent->getCharsetType() == CHARSET_TYPE_windows)
    //    //{
    //    //    if(gCodePages)
    //    //        text_len = gCodePages->codepageToUtf16( pUvText->codepage, (unsigned char*)pUvText->text, pUvText->textLen, dest );
    //    //}
    //    //else
    //    {
    //    }
    //    //else if(pUvText->codepage == CODEPAGE_GB2312 || pUvText->codepage == CODEPAGE_JAPANESE || pUvText->codepage == CODEPAGE_HANGEUL || pUvText->codepage == CODEPAGE_CHINESEBIG5) // adobe CJK CMAP
    //    //{
    //    //    //
    //    //}
    //}

    if(text_len == 0 && gCodePages)
        text_len = gCodePages->codepageToUtf16( pUvText->codepage, (unsigned char*)pUvText->text, pUvText->textLen, dest );

    if(text_len == 0)
    {
        for(int i = 0; i < pUvText->textLen; i++)
            dest[i] = pUvText->text[i];
        text_len = pUvText->textLen;
    }

    return text_len;

}
