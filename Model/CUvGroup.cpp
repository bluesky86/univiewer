#include "stdafx.h"
#include "CUvGroup.h"
#include "DebugNew.h"


CUvGroup::CUvGroup()
    :pClipPath(nullptr), bkColor(255, 255, 255, 255)
{
    type = uvGroup_ShapeType;
}


CUvGroup::~CUvGroup()
{
    for (size_t i = 0; i < components.size(); i++)
        delete components[i];
    components.clear();

    if (pClipPath)
        delete pClipPath;
}

CUvShape* CUvGroup::Copy()
{
    CUvGroup *p = NEW CUvGroup();
    p->CopyFrom(this);

    return p;
}

void CUvGroup::GetBound(CUvRectF &rect)
{
    rect.CopyFrom(this);
}

void CUvGroup::CopyFrom(CUvGroup *p)
{
    if (p == nullptr) return;

    CUvRectF::CopyFrom(p);

    mat = p->mat;
    pClipPath = p->pClipPath;

    for (size_t i = 0; i < p->components.size(); i++)
    {
        components.push_back(p->components[i]->Copy());
    }
}
