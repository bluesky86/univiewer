#pragma once
#include "Model.h"
#include "Vector.h"

UNIV_API wchar_t* UV_Char2Wchar(const char* src, bool bFreeSrc = false);
UNIV_API size_t UV_Char2Wchar(wchar_t* dest, size_t bufferLen, const char* src);
UNIV_API char* UV_Wchar2Char(const wchar_t* src, bool bFreeSrc = false);
UNIV_API size_t UV_Wchar2Char(char* dest, size_t bufferLen, const wchar_t* src);

UNIV_API size_t reverseCharPos(wchar_t *string, wchar_t ch, int startPos = -1);
UNIV_API size_t replaceChar(wchar_t *string, wchar_t ch, wchar_t newCh, int startPos = 0);

UNIV_API void UV_AllocCopyStr(const char* src, const char** _target);


UNIV_API int UV_Utf8_strlen(const char* str);
UNIV_API int UV_Utf8_strlen(const char* str, int len);
UNIV_API int UV_Utf8_first_char( const char* str, int len );

UNIV_API int UV_1ByteStringTo2BytesUtf8(const unsigned char* str, int len, CVector<unsigned char> *dest);
