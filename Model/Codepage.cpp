#include "stdafx.h"
#include "Codepage.h"
#include <fstream>
#include "assert.h"

const char *bcp47[] = {
    "th",       // 874: Thai
    "jp-JP",    // 932: Japanese
    "zh-CN",    // 936: China
    "ko",       // 949: Korea
    "zh-HK",    // 950: Big5
    "en-GB",    // 1250: Centeral Europ
    "ru",       // 1251: Russian
    "en-US",    // 1252: Western
    "el-GR",    // 1253: Greek
    "tr",       // 1254: Turkish
    "he",       // 1255: Hebrew
    "ar",       // 1256: Arabic
    "sk",       // 1257: Slovak
    "vi",       // 1258: Vietnam
};

const char* CCodepage::getBcp47( int codepage )
{
    int index = getCodepageIndex( codepage );
    if(index != -1)
        return bcp47[index];

    return nullptr;
}

/************************************************************
Member	    Value   Description
Baltic	    186     Baltic character set.
Mac	        77	    Characters used by Macintosh.
Russian	    204	    Russian character set.
EastEurope	238	    Eastern European character set.
Thai	    222	    Thai character set.
Vietnamese	163	    Vietnamese character set.
Turkish	    162	    Turkish character set.
Greek	    161	    Greek character set.
Arabic	    178	    Arabic character set.
Hebrew	    177	    Hebrew character set.
Johab	    130	    Korean character set.
Oem	        255	    Extended ASCII character set used with disk operating system( DOS ) and some Microsoft Windows fonts.
ChineseBig5	136	    Chinese character set used mostly in Hong Kong SAR and Taiwan.
GB2312	    134	    Chinese character set used in mainland China.
Hangul	    129	    Korean character set.
Hangeul	    129	    Another common spelling of the Korean character set.
ShiftJIS	128	    Japanese character set.
Symbol	    2	    Symbol character set.
Default	    1	    System default character set.
Ansi	    0	    ASCII character set.
************************************************************/

CCodepage::CCodepage()
{
    int index = 0;
    codepageIndexMap.insert( std::pair<int, int>( 874, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 932, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 936, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 949, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 950, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1250, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1251, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1252, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1253, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1254, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1255, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1256, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1257, index++ ) );
    codepageIndexMap.insert( std::pair<int, int>( 1258, index++ ) );

    maps = new std::map<unsigned int, unsigned int>*[codepageIndexMap.size()];
    for(int i = 0; i < codepageIndexMap.size(); i++)
        maps[i] = nullptr;

    exePath = L"";

    signature = new unsigned int*[codepageIndexMap.size()];
    memset( signature, 0, sizeof( unsigned int* ) * codepageIndexMap.size() );
}
CCodepage::~CCodepage()
{
    if(maps)
    {
        for(size_t i = 0; i < codepageIndexMap.size(); i++)
        {
            if(maps[i])
            {
                delete maps[i];
            }
            if(signature[i])
                delete signature[i];
        }

        delete maps;
        delete signature;
    }

    codepageIndexMap.clear();
}

void CCodepage::setExePath( const wchar_t *_path )
{
    exePath = _path;
    if(exePath[exePath.length() - 1] == L'\\')
        exePath += L"codepage\\";
    else
        exePath += L"\\codepage\\";
}

int CCodepage::getCodepageIndex( int codepage )
{
    std::map<int, int>::iterator it = codepageIndexMap.find( codepage );
    if(it == codepageIndexMap.end())
        return -1;
    return it->second;
}

int CCodepage::codepageToUtf16( int codepage, const unsigned char *src, int len, unsigned char *dest )
{
    if(!src || len == 0 || !dest) return 0;
    
    int dest_len = 0;

    int codepageIndex = getCodepageIndex( codepage );
    if(codepageIndex != -1)
    {
        if(maps[codepageIndex] == nullptr)
            loadCodepageMap( codepageIndex, codepage );
    }
    if(codepageIndex == -1 || maps[codepageIndex] == nullptr)
    {
        for(int i = 0; i < len; i++)
            dest[i] = src[i];
        dest_len = len;
    }
    else
    {
        unsigned short iv;
        unsigned short v = 0;
        unsigned char* cv;
        int count = 0;
        std::map<unsigned int, unsigned int>::iterator it;
        for(int i = 0; i < len; i++)
        {
            iv = src[i];
            it = maps[codepageIndex]->find(iv);
            if(it != maps[codepageIndex]->end())
                v = it->second;
            else
            {
                i++;
                iv <<= 8;
                iv += src[i];
                it = maps[codepageIndex]->find( iv );
                if(it == maps[codepageIndex]->end())
                {
                    //assert( 0 );
                    v = 0x20;
                }
                else
                    v = it->second;
            }
            cv = (unsigned char*)&v;
            dest[count++] = cv[0];
            dest[count++] = cv[1];
        }
        dest_len = count;
    }

    return dest_len;
}

unsigned int convertToInt(std::string &src)
{
    bool bHex = ( src[0] == '0' && src[1] == 'x' );

    if(bHex)
    {
        unsigned int v = 0;
        for(int i = 2; i < src.length(); i++)
        {
            unsigned int c;
            if(src[i] >= '0' && src[i] <= '9')
                c = src[i] - '0';
            else if(src[i] >= 'a' && src[i] <= 'f')
                c = src[i] - 'a' + 10;
            else
                c = src[i] - 'A' + 10;
            v <<= 4;
            v += c;
        }
        return v;
    }
    else
        return (unsigned int)atoi( src.c_str() );
}

void CCodepage::loadCodepageMap( int index, int codepage )
{
    std::wstring filePath = exePath + L"cp";
    filePath += std::to_wstring( codepage ) + L".txt";

    std::ifstream ifs;
    ifs.open( filePath.c_str(), std::ifstream::in );
    if(!ifs.is_open()) return;

    maps[index] = new std::map<unsigned int, unsigned int>;
    signature[index] = new unsigned int;
    *signature[index] = 0;

    char buffer[512];

    std::string value[2];
    unsigned int ivalue[2];
    while(!ifs.eof())
    {
        ifs.getline( buffer, 512 );
        std::streamsize len = ifs.gcount();
        if(len == 0) continue;
        int order = 0;
        bool bSignature = false;
        value[0] = ""; value[1] = "";
        for(int i = 0; i < len && order < 2; i++)
        {
            char c = buffer[i];
            switch(c)
            {
            case '#':
                i = (int)len;
                continue;
            case '@':
                bSignature = true;
                break;
            case ' ':
            case '\t':
                order++;
                break;
            default:
                value[order] += c;
                break;
            }
        }
        if(value[0] == "") continue;
        ivalue[0] = convertToInt( value[0] );
        if(bSignature)
        {
            *signature[index] = ivalue[0];
        }
        else
        {
            if(value[1] == "") continue;
            ivalue[1] = convertToInt( value[1] );
            maps[index]->insert( std::pair<unsigned int, unsigned int>( ivalue[0], ivalue[1] ) );
        }
    }
}

unsigned int CCodepage::getSignature( int codepage )
{
    int codepageIndex = getCodepageIndex( codepage );
    if(codepageIndex != -1)
    {
        if(maps[codepageIndex] == nullptr)
            loadCodepageMap( codepageIndex, codepage );
    }
    if(codepageIndex == -1 || maps[codepageIndex] == nullptr)
        return 0x41;

    return *signature[codepageIndex];
}


