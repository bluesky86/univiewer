// ImscoreDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EgfViewer.h"
#include "ImscoreDlg.h"
#include "afxdialogex.h"


// CImscoreDlg dialog

IMPLEMENT_DYNAMIC(CImscoreDlg, CDialogEx)

CImscoreDlg::CImscoreDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CImscoreDlg::IDD, pParent)
{
    m_csSource = "";
    m_csTarget = "";
    m_nStartPage = 1;
    m_nEndPage = 1;
    m_bAllPages = false;
}

CImscoreDlg::~CImscoreDlg()
{
}

void CImscoreDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_MFCEDITBROWSE_SOURCE_FILE, m_browseSource);
    DDX_Control(pDX, IDC_MFCEDITBROWSE_TARGET_DIR, m_browseTarget);
    DDX_Control(pDX, IDC_EDIT_START_PAGE, m_editStartPage);
    DDX_Control(pDX, IDC_EDIT_END_PAGE, m_editEndPage);
    DDX_Control(pDX, IDC_CHECK_CONVERT_ALL_PAGES, m_chkAllPages);
}


BEGIN_MESSAGE_MAP(CImscoreDlg, CDialogEx)
    ON_BN_CLICKED(IDOK, &CImscoreDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDC_CHECK_CONVERT_ALL_PAGES, &CImscoreDlg::OnBnClickedCheckConvertAllPages)
END_MESSAGE_MAP()


// CImscoreDlg message handlers


BOOL CImscoreDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // TODO:  Add extra initialization here
    m_browseSource.EnableFileBrowseButton();
    m_browseSource.SetWindowTextW(m_csSource);
    
    m_browseTarget.EnableFolderBrowseButton();
    //if (m_csTarget.IsEmpty()) m_csTarget = "c:\\";
    m_browseTarget.SetWindowTextW(m_csTarget);

    wchar_t buf[64];
    wsprintf(buf, _T("%d"), m_nStartPage);
    m_editStartPage.SetWindowTextW(buf);
    wsprintf(buf, _T("%d"), m_nEndPage);
    m_editEndPage.SetWindowTextW(buf);

    if(m_bAllPages) m_chkAllPages.SetCheck(BST_CHECKED);
    else m_chkAllPages.SetCheck(BST_UNCHECKED);
    m_editStartPage.EnableWindow(!m_bAllPages);
    m_editEndPage.EnableWindow(!m_bAllPages);

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}


void CImscoreDlg::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    m_browseSource.GetWindowTextW(m_csSource);
    m_browseTarget.GetWindowTextW(m_csTarget);

    CString cs;
    m_editStartPage.GetWindowTextW(cs);
    m_nStartPage = _wtoi(cs.GetBuffer());
    m_editEndPage.GetWindowTextW(cs);
    m_nEndPage = _wtoi(cs.GetBuffer());

    m_bAllPages = m_chkAllPages.GetCheck() == BST_CHECKED;
    if (m_bAllPages)
    {
        m_nStartPage = 1;
        m_nEndPage = 0;
    }
    else if (m_nStartPage > m_nEndPage)
    {
        AfxMessageBox(_T("Start page number cannot be larger than end page number"));
        return;
    }

    CDialogEx::OnOK();
}


void CImscoreDlg::OnBnClickedCheckConvertAllPages()
{
    m_bAllPages = (m_chkAllPages.GetCheck() == BST_CHECKED);
    if (m_bAllPages)
    {
        m_editStartPage.SetWindowTextW(L"1");
        m_editEndPage.SetWindowTextW(L"0");
    }

    m_editStartPage.EnableWindow(!m_bAllPages);
    m_editEndPage.EnableWindow(!m_bAllPages);
}
