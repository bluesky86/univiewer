#include "stdafx.h"
#include "EgfPath.h"
#include "Global.h"


CEgfPath::CEgfPath()
    :m_pGradientPoints(0)
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfPath::CEgfPath(int objType, int objDataType, int objSize)
    :m_pGradientPoints(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}


CEgfPath::~CEgfPath()
{
    if (m_pGradientPoints)
        delete[] m_pGradientPoints;
}

bool CEgfPath::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mMer
    ReadMer(ifs, mMer);

    // mCenterPoint
    ReadPoints(ifs, 1, &mCenterPoint);

    // mTranslation
    ReadPoints(ifs, 1, &mTranslation);

    // mFlipH
    ifs.read(buf, 4);
    mFlipH = Egf32_ToInt(buf);

    // mFlipV
    ifs.read(buf, 4);
    mFlipV = Egf32_ToInt(buf);

    // mRotAngle
    ifs.read(buf, 4);
    mRotAngle = Egf32_ToInt(buf);

    // mShaddingStyle
    ifs.read(buf, 4);
    mShaddingStyle = Egf32_ToInt(buf);

    // Gradient index
    ifs.read(buf, 4);
    mGradientIndex = Egf32_ToInt(buf);

    // Gradient index 2
    ifs.read(buf, 4);
    mGradientIndex2 = Egf32_ToInt(buf);

    // mGradientNumPoints
    ifs.read(buf, 4);
    mGradientNumPoints = Egf32_ToInt(buf);

    // gradient points
    if (mGradientNumPoints > 0)
    {
        m_pGradientPoints = new CEgfPoint[mGradientNumPoints];
        ReadPoints(ifs, mGradientNumPoints, m_pGradientPoints);
    }

    // mGdiPlusFlatternFigure
    ifs.read(buf, 4);
    mGdiPlusFlatternFigure = Egf32_ToInt(buf);

    // mGdiPlusFlatternFactor
    ifs.read(buf, 4);
    mGdiPlusFlatternFactor = Egf32_ToInt(buf);

    // Closed Figure flag
    ifs.read(buf, 4);
    mClosedFigure = (*(int*)buf > 0);

    // Connect Figure flag
    ifs.read(buf, 4);
    mConnectFigure = (*(int*)buf > 0);

    // parse children
    return ParseChildren(ifs, this);
}

void CEgfPath::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    CheckCriteria(objType, pPairs, pList);

    // search children
    for (int i = 0; i < mChildren.size(); i++)
        mChildren[i]->Search(objType, pPairs, pList);
}

void CEgfPath::CheckCriteria(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_PATH_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case PATH_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case PATH_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case PATH_ARGS_ID_mCenterPoint_x:
            met = CheckCondition(pPairs->pairs[i], &mCenterPoint.x);
            break;
        case PATH_ARGS_ID_mCenterPoint_y:
            met = CheckCondition(pPairs->pairs[i], &mCenterPoint.y);
            break;
        case PATH_ARGS_ID_mTranslation_x:
            met = CheckCondition(pPairs->pairs[i], &mTranslation.y);
            break;
        case PATH_ARGS_ID_mTranslation_y:
            met = CheckCondition(pPairs->pairs[i], &mTranslation.y);
            break;
        case PATH_ARGS_ID_mFlipH:
            met = CheckCondition(pPairs->pairs[i], &mFlipH);
            break;
        case PATH_ARGS_ID_mFlipV:
            met = CheckCondition(pPairs->pairs[i], &mFlipV);
            break;
        case PATH_ARGS_ID_mRotAngle:
            met = CheckCondition(pPairs->pairs[i], &mRotAngle);
            break;
        case PATH_ARGS_ID_mShaddingStyle:
            met = CheckCondition(pPairs->pairs[i], &mShaddingStyle);
            break;
        case PATH_ARGS_ID_mGradientIndex:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex);
            break;
        case PATH_ARGS_ID_mGradientIndex2:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex2);
            break;
        case PATH_ARGS_ID_mGradientNumPoints:
            met = CheckCondition(pPairs->pairs[i], &mGradientNumPoints);
            break;
        case PATH_ARGS_ID_mGdiPlusFlatternFigure:
            met = CheckCondition(pPairs->pairs[i], &mGdiPlusFlatternFigure);
            break;
        case PATH_ARGS_ID_mGdiPlusFlatternFactor:
            met = CheckCondition(pPairs->pairs[i], &mGdiPlusFlatternFactor);
            break;
        case PATH_ARGS_ID_mClosedFigure:
            met = CheckCondition(pPairs->pairs[i], &mClosedFigure);
            break;
        case PATH_ARGS_ID_mConnectFigure:
            met = CheckCondition(pPairs->pairs[i], &mConnectFigure);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}