#pragma once
#include "afxdockablepane.h"
#include "ViewList.h"
#include <vector>
#include "EgfObj.h"


class CSearchResult : public CDockablePane
{
public:
    CSearchResult();
    virtual ~CSearchResult();

    void AdjustLayout();

    void ShowList(CVector<CEgfObj*> *objList);

public:


protected:
    CViewList m_wndListView;
    CImageList m_ListViewImages;
    UINT m_nCurrSort;

    // Overrides
public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg void OnPaint();
    afx_msg void OnSetFocus(CWnd* pOldWnd);

    DECLARE_MESSAGE_MAP()
};

