#include "stdafx.h"
#include "EtxText.h"
#include "Global.h"


CEtxText::CEtxText()
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;

    m_pText = NULL;
}

CEtxText::CEtxText(int objType, int objDataType, int objSize)
//:mVisible(0), mMask(0), mHasMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;

    m_pText = NULL;
}


CEtxText::~CEtxText()
{
    if (m_pText)
        delete[] m_pText;
}

bool CEtxText::Parse(std::ifstream &ifs)
{
    char buf[4];
    int objType = 0, objDataType = 0, objSize = 0;

    // mIsUnicode
    ifs.read(buf, 1);
    mIsUnicode = buf[0]>0?true:false;

    // mTextLength
    ifs.read(buf, 4);
    mTextLength = Egf32_ToInt(buf);

    // m_pText
    if (mTextLength > 0)
    {
        m_pText = new char[mTextLength + 2];
        ifs.read(m_pText, mTextLength);
        m_pText[mTextLength] = 0;
        m_pText[mTextLength + 1] = 0;
    }

    // mFont
    ifs.read(mFontName, 81);

    // mFillColor
    ifs.read(mFillColor, 3);

    // mOutline
    ifs.read(buf, 1);
    mOutline = buf[0]>0 ? true : false;

    // mLineStyle
    ifs.read(buf, 4);
    mLineStyle = Egf32_ToInt(buf);

    // mFillStyle
    ifs.read(buf, 4);
    mFillStyle = Egf32_ToInt(buf);

    // mHatchStyle
    ifs.read(buf, 4);
    mHatchStyle = Egf32_ToInt(buf);

    return true;
}

void CEtxText::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != OBJTYPE_TEXT || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case ETXTEXT_ARGS_ID_mIsUnicode:
            met = CheckCondition(pPairs->pairs[i], &mIsUnicode);
            break;
        case ETXTEXT_ARGS_ID_mTextLength:
            met = CheckCondition(pPairs->pairs[i], &mTextLength);
            break;
        case ETXTEXT_ARGS_ID_m_pText:
            met = CheckColorCondition(pPairs->pairs[i], m_pText);
            break;
        case ETXTEXT_ARGS_mFontName:
            met = CheckCondition(pPairs->pairs[i], &mFontName);
            break;
        case ETXTEXT_ARGS_ID_mFillColor:
            met = CheckCondition(pPairs->pairs[i], &mFillColor);
            break;
        case ETXTEXT_ARGS_ID_mOutline:
            met = CheckCondition(pPairs->pairs[i], &mOutline);
            break;
        case ETXTEXT_ARGS_ID_mLineStyle:
            met = CheckCondition(pPairs->pairs[i], &mLineStyle);
            break;
        case ETXTEXT_ARGS_ID_mFillStyle:
            met = CheckCondition(pPairs->pairs[i], &mFillStyle);
            break;
        case ETXTEXT_ARGS_ID_mHatchStyle:
            met = CheckCondition(pPairs->pairs[i], &mHatchStyle);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}


