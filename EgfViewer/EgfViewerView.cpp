// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

// EgfViewerView.cpp : implementation of the CEgfViewerView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "EgfViewer.h"
#endif

#include "EgfViewerDoc.h"
#include "EgfViewerView.h"
#include "CSurface.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//using namespace Gdiplus;

// CEgfViewerView

IMPLEMENT_DYNCREATE(CEgfViewerView, CScrollView)

BEGIN_MESSAGE_MAP(CEgfViewerView, CScrollView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CEgfViewerView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
    ON_WM_SIZE()
END_MESSAGE_MAP()

// CEgfViewerView construction/destruction

CEgfViewerView::CEgfViewerView()
{
	// TODO: add construction code here
    m_nClientWidth = 1;
    m_nClientHeight = 1;
    m_nPaintWidth = 1;
    m_nPaintHeight = 1;
    m_fScaleX = 1.0f;
    m_fScaleY = 1.0f;
    m_nLeftMargin = VIEW_MARGIN;
    m_nTopMargin = VIEW_MARGIN;

    m_pImage = nullptr;
    m_pLayerImage = nullptr;
}

CEgfViewerView::~CEgfViewerView()
{
}

BOOL CEgfViewerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}

// CEgfViewerView drawing

void CEgfViewerView::OnDraw(CDC* pDC)
{
	CEgfViewerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
    Gdiplus::Graphics g(pDC->m_hDC);

    CRect rect;
    GetClientRect(&rect);
    pDC->FillRect(&rect, &CBrush(RGB(171, 171, 171)));

    if (m_pImage)
    {
        BITMAPINFO bmpInfo;
        ZeroMemory(&bmpInfo, sizeof(BITMAPINFO));
        bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        bmpInfo.bmiHeader.biWidth = m_pImage->width;
        bmpInfo.bmiHeader.biHeight = -m_pImage->height; // negative means top-down bitmap. Skia draws top-down.
        bmpInfo.bmiHeader.biPlanes = 1;
        bmpInfo.bmiHeader.biBitCount = m_pImage->bytePerPixel * 8;
        bmpInfo.bmiHeader.biCompression = BI_RGB;
        void* pixels = m_pImage->bits;

        StretchDIBits(pDC->m_hDC, (int)m_nLeftMargin, (int)m_nTopMargin, (int)m_nPaintWidth, (int)m_nPaintHeight, 0, 0, m_pImage->width, m_pImage->height, pixels, &bmpInfo, DIB_RGB_COLORS, SRCCOPY);
    }
    if (m_pLayerImage)
    {
        BITMAPINFO bmpInfo;
        ZeroMemory(&bmpInfo, sizeof(BITMAPINFO));
        bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        bmpInfo.bmiHeader.biWidth = m_pLayerImage->width;
        bmpInfo.bmiHeader.biHeight = -m_pLayerImage->height; // negative means top-down bitmap. Skia draws top-down.
        bmpInfo.bmiHeader.biPlanes = 1;
        bmpInfo.bmiHeader.biBitCount = m_pLayerImage->bytePerPixel * 8;
        bmpInfo.bmiHeader.biCompression = BI_RGB;
        void* pixels = m_pLayerImage->bits;


        //StretchDIBits(pDC->m_hDC, (int)m_nLeftMargin, (int)m_nTopMargin, (int)m_nPaintWidth, (int)m_nPaintHeight, 0, 0, m_pLayerImage->width, m_pLayerImage->height, pixels, &bmpInfo, DIB_RGB_COLORS, SRCCOPY);
        BLENDFUNCTION blend = { AC_SRC_OVER, 0, 127, AC_SRC_ALPHA };

        HBITMAP hBitmap = CreateCompatibleBitmap(pDC->m_hDC, m_pLayerImage->width, m_pLayerImage->height);
        HDC tempHdc = CreateCompatibleDC(pDC->m_hDC);
        HGDIOBJ hOldBitmap = SelectObject(tempHdc, hBitmap);

        StretchDIBits(tempHdc, 0, 0, (int)m_nPaintWidth, (int)m_nPaintHeight, 0, 0, m_pLayerImage->width, m_pLayerImage->height, pixels, &bmpInfo, DIB_RGB_COLORS, SRCCOPY);

        AlphaBlend(pDC->m_hDC, (int)m_nLeftMargin, (int)m_nTopMargin, (int)m_nPaintWidth, (int)m_nPaintHeight, tempHdc, 0, 0, m_pLayerImage->width, m_pLayerImage->height, blend);
 
        SelectObject(tempHdc, hOldBitmap);

        DeleteDC(tempHdc);
        
    }
}

void CEgfViewerView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);

}


// CEgfViewerView printing


void CEgfViewerView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CEgfViewerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CEgfViewerView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CEgfViewerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CEgfViewerView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CEgfViewerView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CEgfViewerView diagnostics

#ifdef _DEBUG
void CEgfViewerView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CEgfViewerView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CEgfViewerDoc* CEgfViewerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEgfViewerDoc)));
	return (CEgfViewerDoc*)m_pDocument;
}
#endif //_DEBUG


// CEgfViewerView message handlers


void CEgfViewerView::OnSize(UINT nType, int cx, int cy)
{
    CScrollView::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here

    CEgfViewerDoc* pDoc = GetDocument();
    ASSERT_VALID(pDoc);
    if (!pDoc) return;

    CRect rc;
    GetClientRect(&rc);
    float fClientWidth = (float)rc.Width();
    float fClientHeight = (float)rc.Height();

    if (fClientWidth == m_nClientWidth && fClientHeight == m_nClientHeight) return;

    m_nClientWidth = fClientWidth;
    m_nClientHeight = fClientHeight;

    if (fClientWidth > 0 && fClientHeight > 0)
    {
        if (pDoc->nPageNo != -1)
        {
            CalculateRatio();

            pDoc->GetPage(pDoc->nPageNo);
            Invalidate();
        }
    }
}

void CEgfViewerView::CalculateRatio()
{
    CEgfViewerDoc* pDoc = GetDocument();

    float xyRatio = pDoc->m_nWidth / pDoc->m_nHeight;
    float clientRatio = m_nClientWidth / m_nClientHeight;
    float pageRatio = pDoc->m_nWidth / pDoc->m_nHeight;

    if (clientRatio > pageRatio) // client rect has bigger width
    {
        // use Y
        m_nPaintHeight = m_nClientHeight - VIEW_MARGIN * 2;
        m_nPaintWidth = m_nPaintHeight * xyRatio;
    }
    else
    {
        // use X
        m_nPaintWidth = m_nClientWidth - VIEW_MARGIN * 2;
        m_nPaintHeight = m_nPaintWidth / xyRatio;
    }

    m_fScaleX = m_nPaintWidth / pDoc->m_nWidth;
    m_fScaleY = m_nPaintHeight / pDoc->m_nHeight;
    m_nLeftMargin = (m_nClientWidth - m_nPaintWidth) / 2;
    m_nTopMargin = (m_nClientHeight - m_nPaintHeight) / 2;

}

void CEgfViewerView::ShowEgfObject(CEgfObj *pObj)
{
    CEgfViewerDoc* pDoc = GetDocument();
    pDoc->m_pObj = pObj;

    if (pObj && pObj->pUvObj)
    {
        CUvMatrix mat;
        mat = m_fScaleX * mat;

        CUvSurface sfWin;

        if (m_pLayerImage) delete m_pLayerImage;
        m_pLayerImage = sfWin.DrawTransparentLayer(pDoc->m_pUvPage, (CUvShape*)pObj->pUvObj, mat, false, true);

        Invalidate();
    }
    else
    {
        if (m_pLayerImage) delete m_pLayerImage;
        m_pLayerImage = nullptr;
    }

}

