// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

// EgfViewerDoc.cpp : implementation of the CEgfViewerDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "EgfViewer.h"
#endif

#include "EgfViewerDoc.h"
#include "EgfViewerView.h"
#include "CSurface.h"
#include "MainFrm.h"

#include <propkey.h>

#include <fstream>

#include "EgfApi.h"

#include "CSurface.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

// CEgfViewerDoc

IMPLEMENT_DYNCREATE(CEgfViewerDoc, CDocument)

BEGIN_MESSAGE_MAP(CEgfViewerDoc, CDocument)
END_MESSAGE_MAP()


// CEgfViewerDoc construction/destruction

CEgfViewerDoc::CEgfViewerDoc()
    :m_pUvDoc(0), m_pUvPage(0), m_pEgfFile1(0)
{
	// TODO: add one-time construction code here
    m_pObj = NULL;
    m_nWidth = 1;
    m_nHeight = 1;
}

CEgfViewerDoc::~CEgfViewerDoc()
{
    //if (m_pEgfFile)
    //    delete m_pEgfFile;

    if (m_pUvDoc)
    {
        if(m_pUvDoc->pRenderFonts)
            FreeRenderFonts( m_pUvDoc->pRenderFonts );
        CloseUvDoc(m_pUvDoc);
        delete m_pUvDoc;
    }
    if(m_pUvPage)
    {
        if(m_pUvPage->pRenderFonts)
            FreeRenderFonts( m_pUvPage->pRenderFonts );
        delete m_pUvPage;
    }
}

BOOL CEgfViewerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CEgfViewerDoc serialization

void CEgfViewerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CEgfViewerDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CEgfViewerDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CEgfViewerDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CEgfViewerDoc diagnostics

#ifdef _DEBUG
void CEgfViewerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CEgfViewerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CEgfViewerDoc commands
BOOL CEgfViewerDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
    if (!CDocument::OnOpenDocument(lpszPathName))
        return FALSE;

    // TODO:  Add your specialized creation code here
    LoadFile(lpszPathName);

    return TRUE;
}

CEgfViewerView* CEgfViewerDoc::GetView()
{
    POSITION pos = GetFirstViewPosition();
    CEgfViewerView *pView = (CEgfViewerView*)(GetNextView(pos));
    return pView;
}

bool CEgfViewerDoc::GetPage(int pageNo)
{
    if (m_pUvDoc == nullptr) return false;

    if (m_pUvPage == nullptr)
    {
        m_pUvPage = CreateUvPageFromEgfObj(m_pUvDoc, pageNo);
        if (m_pUvPage == nullptr)
        {
            AfxMessageBox(_T("Failed to create Uv Page object!"));
            return false;
        }
        m_pUvPage->parent = m_pUvDoc;
        m_pEgfFile1 = (CEgfFile*)GetEgfFile(m_pUvDoc);

        MakeRenderFonts( m_pUvPage->fontList, &m_pUvPage->pRenderFonts );
    }

    // get page width and height
    if (m_pEgfFile1 && m_pEgfFile1->m_pChild)
    {
        CEgfAttr *pMerAttr = m_pEgfFile1->m_pChild->FindAttr(_T("Mer"), EGF_ELE_META_TYPE_mer);

        if (pMerAttr == NULL || wcscmp(_T("Group"), m_pEgfFile1->m_pChild->GetMetaName()) != 0)
        {
            AfxMessageBox(_T("First object must be an EGF Group object!"));
            return false;
        }

        //m_nWidth = pMerAttr->mer->x2 - pMerAttr->mer->x1;
        //m_nHeight = pMerAttr->mer->y2 - pMerAttr->mer->y1;
        //m_Orign.x = pMerAttr->mer->x1;
        //m_Orign.y = pMerAttr->mer->y1;

        m_nWidth = m_pUvPage->Width() + 0.5f;
        m_nHeight = m_pUvPage->Height() + 0.5f;
        m_nOrigX = 0;
        m_nOrigY = 0;

        CEgfViewerView *pView = GetView();
        pView->CalculateRatio();

        CUvMatrix mat;
        mat = pView->m_fScaleX * mat;

        CUvSurface sfWin;

        if (pView->m_pImage) delete pView->m_pImage;
        pView->m_pImage = sfWin.DrawPage(m_pUvPage, mat, !((CMainFrame*)AfxGetMainWnd())->m_bApplyClip);
        if (pView->m_pLayerImage)
        {
            delete pView->m_pLayerImage;
            pView->m_pLayerImage = nullptr;
        }

        nPageNo = pageNo;

        // send signore to main frame
        AfxGetMainWnd()->PostMessage(WM_LOAD_FILE_DONE, 0, (LPARAM)m_pEgfFile1);
    }

    return true;
}

void CEgfViewerDoc::LoadFile(LPCTSTR fileName)
{
    if (m_pUvDoc)
    {
        if(m_pUvDoc->pRenderFonts)
            FreeRenderFonts( m_pUvDoc->pRenderFonts );
        CloseUvDoc(m_pUvDoc);
        delete m_pUvDoc;
        m_pUvDoc = nullptr;
    }
    if (m_pUvPage)
    {
        if(m_pUvPage->pRenderFonts)
            FreeRenderFonts( m_pUvPage->pRenderFonts );
        delete m_pUvPage;
        m_pUvPage = nullptr;
    }

    m_pUvDoc = CreateUvDocFromEgfFile(fileName);

    MakeRenderFonts( m_pUvDoc->fontList, &m_pUvDoc->pRenderFonts );

    GetPage(0);

}

