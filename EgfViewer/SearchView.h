#pragma once
#include "afxdockablepane.h"
#include "SearchDlg.h"

class CSearchToolBar : public CMFCToolBar
{
    virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
    {
        CMFCToolBar::OnUpdateCmdUI((CFrameWnd*)GetOwner(), bDisableIfNoHndler);
    }

    virtual BOOL AllowShowOnList() const { return FALSE; }
};

class CSearchView : public CDockablePane
{
public:
    CSearchView();
    virtual ~CSearchView();

    void AdjustLayout();

protected:
    CSearchToolBar m_wndToolBar;
    CSearchDlg m_dlgSearch;

    // Overrides
public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSetFocus(CWnd* pOldWnd);
    afx_msg void OnPaint();

    DECLARE_MESSAGE_MAP()

};

