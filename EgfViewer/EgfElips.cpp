#include "stdafx.h"
#include "EgfElips.h"
#include "Global.h"


CEgfElips::CEgfElips() : m_pGradientPoints(0), mVisible(0), mMask(0)

{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfElips::CEgfElips(int objType, int objDataType, int objSize)
    : m_pGradientPoints(0), mVisible(0), mMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}

CEgfElips::~CEgfElips()
{
    if (m_pGradientPoints)
        delete[] m_pGradientPoints;
}

bool CEgfElips::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mThinknessUnits
    ifs.read(buf, 4);
    mThinknessUnits = Egf32_ToInt(buf);
    // mThinknessValue
    ifs.read(buf, 4);
    mThinknessValue = Egf32_ToInt(buf);

    // mLineColor
    ifs.read(mLineColor, 3);

    // mFillColor
    ifs.read(mFillColor, 3);

    // mLineStyle
    ifs.read(buf, 4);
    mLineStyle = Egf32_ToInt(buf);

    // mFillStyle
    ifs.read(buf, 4);
    mFillStyle = Egf32_ToInt(buf);

    // mHatchStyle
    ifs.read(buf, 4);
    mHatchStyle = Egf32_ToInt(buf);

    // mXRrotation
    ifs.read(buf, 4);
    mXRotation = Egf32_ToInt(buf);

    // mShadingStyle
    ifs.read(buf, 4);
    mShadingStyle = Egf32_ToInt(buf);

    // mPenNum
    ifs.read(buf, 4);
    mPenNum = Egf32_ToInt(buf);

    // mStartPoint
    ReadPoints(ifs, 1, &mStartPoint);

    // mEndPoint
    ReadPoints(ifs, 1, &mEndPoint);

    // start angle
    ifs.read(buf, 4);
    mStartAngle = (float)Egf32_ToInt(buf);
    // end angle
    ifs.read(buf, 4);
    mEndAndgle = (float)Egf32_ToInt(buf);

    // mCombineMode
    ifs.read(buf, 4);
    mCombineMode = Egf32_ToInt(buf);

    // Gradient index
    ifs.read(buf, 4);
    mGradientIndex = Egf32_ToInt(buf);

    // Gradient index 2
    ifs.read(buf, 4);
    mGradientIndex2 = Egf32_ToInt(buf);

    // mGradientNumPoints
    ifs.read(buf, 4);
    mGradientNumPoints = Egf32_ToInt(buf);

    // gradient points
    if (mGradientNumPoints > 0)
    {
        m_pGradientPoints = new CEgfPoint[mGradientNumPoints];
        ReadPoints(ifs, mGradientNumPoints, m_pGradientPoints);
    }

    return true;
}

void CEgfElips::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_ELIPS_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case ELIPS_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case ELIPS_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case ELIPS_ARGS_ID_mThinknessUnits:
            met = CheckCondition(pPairs->pairs[i], &mThinknessUnits);
            break;
        case ELIPS_ARGS_ID_mThinknessValue:
            met = CheckCondition(pPairs->pairs[i], &mThinknessValue);
            break;
        case ELIPS_ARGS_ID_mLineColor:
            met = CheckColorCondition(pPairs->pairs[i], mLineColor);
            break;
        case ELIPS_ARGS_ID_mFillColor:
            met = CheckColorCondition(pPairs->pairs[i], mFillColor);
            break;
        case ELIPS_ARGS_ID_mLineStyle:
            met = CheckCondition(pPairs->pairs[i], &mLineStyle);
            break;
        case ELIPS_ARGS_ID_mFillStyle:
            met = CheckCondition(pPairs->pairs[i], &mFillStyle);
            break;
        case ELIPS_ARGS_ID_mHatchStyle:
            met = CheckCondition(pPairs->pairs[i], &mHatchStyle);
            break;
        case ELIPS_ARGS_ID_mXRotation:
            met = CheckCondition(pPairs->pairs[i], &mXRotation);
            break;
        case ELIPS_ARGS_ID_mShadingStyle:
            met = CheckCondition(pPairs->pairs[i], &mShadingStyle);
            break;
        case ELIPS_ARGS_ID_mStartPointX:
            met = CheckCondition(pPairs->pairs[i], &mStartPoint.x);
            break;
        case ELIPS_ARGS_ID_mStartPointY:
            met = CheckCondition(pPairs->pairs[i], &mStartPoint.y);
            break;
        case ELIPS_ARGS_ID_mEndPointX:
            met = CheckCondition(pPairs->pairs[i], &mEndPoint.y);
            break;
        case ELIPS_ARGS_ID_mEndPointY:
            met = CheckCondition(pPairs->pairs[i], &mEndPoint.y);
            break;
        case ELIPS_ARGS_ID_mStartAngle:
            met = CheckCondition(pPairs->pairs[i], &mStartAngle);
            break;
        case ELIPS_ARGS_ID_mEndAngle:
            met = CheckCondition(pPairs->pairs[i], &mEndAndgle);
            break;
        case ELIPS_ARGS_ID_mCombineMode:
            met = CheckCondition(pPairs->pairs[i], &mCombineMode);
            break;
        case ELIPS_ARGS_ID_mGradientIndex:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex);
            break;
        case ELIPS_ARGS_ID_mGradientIndex2:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex2);
            break;
        case ELIPS_ARGS_ID_mGradientNumPoints:
            met = CheckCondition(pPairs->pairs[i], &mGradientNumPoints);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}