#pragma once

#define     WM_LOAD_FILE_DONE                       (WM_USER + 1)
#define     WM_SELECTED_OBJECT_CHANGED              (WM_USER + 2)
#define     WM_SEARCH_EGF_OBJ                       (WM_USER + 3)
#define     WM_SEARCHED_OBJ_SELECTED                (WM_USER + 4)

#define     ID_USER_DEFINED                         10001
#define     ID_USER_DEFINED_GROUP                   (ID_USER_DEFINED + EGF_GROUP_TAG * 100)
#define     ID_USER_DEFINED_LINE                    (ID_USER_DEFINED + EGF_LINE_TAG * 100)
#define     ID_USER_DEFINED_ARC                     (ID_USER_DEFINED + EGF_ARC_TAG * 100)
#define     ID_USER_DEFINED_ELIPS                   (ID_USER_DEFINED + EGF_ELIPS_TAG * 100)
#define     ID_USER_DEFINED_ARROW                   (ID_USER_DEFINED + EGF_ARROW_TAG * 100)
#define     ID_USER_DEFINED_PLINE                   (ID_USER_DEFINED + EGF_PLINE_TAG * 100)
#define     ID_USER_DEFINED_PGON                    (ID_USER_DEFINED + EGF_PGON_TAG * 100)
#define     ID_USER_DEFINED_PASTE                   (ID_USER_DEFINED + EGF_PASTE_TAG * 100)
#define     ID_USER_DEFINED_TEXT                    (ID_USER_DEFINED + EGF_TEXT_TAG * 100)
#define     ID_USER_DEFINED_ANNO                    (ID_USER_DEFINED + EGF_ANNO_TAG * 100)
#define     ID_USER_DEFINED_PATH                    (ID_USER_DEFINED + EGF_PATH_TAG * 100)
#define     ID_USER_DEFINED_STEXT                   (ID_USER_DEFINED + EGF_STEXT_TAG * 100)
#define     ID_USER_DEFINED_ETXTEXT                 (ID_USER_DEFINED + OBJTYPE_TEXT * 100)

//typedef enum
//{
//    GROUP_ARGS_ID_mVisible,
//    GROUP_ARGS_ID_mMask,
//    GROUP_ARGS_ID_mHasMask,
//    GROUP_ARGS_ID_mIsPasteObj,
//    GROUP_ARGS_ID_mMaskGroup,
//    GROUP_ARGS_ID_mMaskObject,
//    GROUP_ARGS_ID_mSourceObject,
//    GROUP_ARGS_ID_mRefObject,
//    GROUP_ARGS_ID_mTransparentGroup,
//    GROUP_ARGS_ID_mBackDropColor,
//    GROUP_ARGS_ID_mMer_x1 = GROUP_ARGS_ID_mBackDropColor + 3,
//    GROUP_ARGS_ID_mMer_x2,
//    GROUP_ARGS_ID_mMer_y1,
//    GROUP_ARGS_ID_mMer_y2,
//    GROUP_ARGS_ID_mTilingStep
//}GROUP_ARGS_ID;
//
//typedef enum
//{
//    LINE_ARGS_ID_mVisible,
//    LINE_ARGS_ID_mMask,
//    LINE_ARGS_ID_mThinknessUnits,
//    LINE_ARGS_ID_mThinknessValue,
//    LINE_ARGS_ID_mColor,
//    LINE_ARGS_ID_mLineStyle = LINE_ARGS_ID_mColor + 3,
//    LINE_ARGS_ID_mCapStyle,
//    LINE_ARGS_ID_mOperation,
//    LINE_ARGS_ID_mStartPointX,
//    LINE_ARGS_ID_mStartPointY,
//    LINE_ARGS_ID_mEndPointX,
//    LINE_ARGS_ID_mEndPointY,
//    LINE_ARGS_ID_mGradientIndex,
//    LINE_ARGS_ID_mGradientNumPoints,
//
//}LINE_ARGS_ID;
//
//typedef enum
//{
//    ARC_ARGS_ID_mVisible,
//    ARC_ARGS_ID_mMask,
//    ARC_ARGS_ID_mThinknessUnits,
//    ARC_ARGS_ID_mThinknessValue,
//    ARC_ARGS_ID_mColor,
//    ARC_ARGS_ID_mLineStyle = ARC_ARGS_ID_mColor + 3,
//    ARC_ARGS_ID_mCapStyle,
//    ARC_ARGS_ID_mOperation,
//    ARC_ARGS_ID_mStartPointX,
//    ARC_ARGS_ID_mStartPointY,
//    ARC_ARGS_ID_mEndPointX,
//    ARC_ARGS_ID_mEndPointY,
//    ARC_ARGS_ID_mStartAngle,
//    ARC_ARGS_ID_mEndAngle,
//    ARC_ARGS_ID_mGradientIndex,
//    ARC_ARGS_ID_mGradientNumPoints,
//
//}ARC_ARGS_ID;
//
//typedef enum
//{
//    ELIPS_ARGS_ID_mVisible,
//    ELIPS_ARGS_ID_mMask,
//    ELIPS_ARGS_ID_mThinknessUnits,
//    ELIPS_ARGS_ID_mThinknessValue,
//    ELIPS_ARGS_ID_mLineColor,
//    ELIPS_ARGS_ID_mFillColor = ELIPS_ARGS_ID_mLineColor + 3,
//    ELIPS_ARGS_ID_mLineStyle = ELIPS_ARGS_ID_mFillColor + 3,
//    ELIPS_ARGS_ID_mFillStyle,
//    ELIPS_ARGS_ID_mHatchStyle,
//    ELIPS_ARGS_ID_mXRotation,
//    ELIPS_ARGS_ID_mShadingStyle,
//    ELIPS_ARGS_ID_mStartPointX,
//    ELIPS_ARGS_ID_mStartPointY,
//    ELIPS_ARGS_ID_mEndPointX,
//    ELIPS_ARGS_ID_mEndPointY,
//    ELIPS_ARGS_ID_mStartAngle,
//    ELIPS_ARGS_ID_mEndAngle,
//    ELIPS_ARGS_ID_mCombineMode,
//    ELIPS_ARGS_ID_mGradientIndex,
//    ELIPS_ARGS_ID_mGradientIndex2,
//    ELIPS_ARGS_ID_mGradientNumPoints,
//
//}ELIPS_ARGS_ID;
//
//typedef enum
//{
//    ARROW_ARGS_ID_mVisible,
//    ARROW_ARGS_ID_mMask,
//    ARROW_ARGS_ID_mThinknessUnits,
//    ARROW_ARGS_ID_mThinknessValue,
//    ARROW_ARGS_ID_mArrowHeadSizeUnits,
//    ARROW_ARGS_ID_mArrowHeadSizeValue,
//    ARROW_ARGS_ID_mColor,
//    ARROW_ARGS_ID_mLineStyle = ARROW_ARGS_ID_mColor + 3,
//    ARROW_ARGS_ID_mOperation,
//    ARROW_ARGS_ID_mCapStyle,
//    ARROW_ARGS_ID_mArrowsProp,
//    ARROW_ARGS_ID_mArrowsSolid,
//    ARROW_ARGS_ID_mArrowsDualHeaded,
//    ARROW_ARGS_ID_mArrowsStyleStart,
//    ARROW_ARGS_ID_mArrowsStyleEnd,
//
//}ARROW_ARGS_ID;
//
//typedef enum
//{
//    POLYLINE_ARGS_ID_mVisible,
//    POLYLINE_ARGS_ID_mMask,
//    POLYLINE_ARGS_ID_mThinknessUnits,
//    POLYLINE_ARGS_ID_mThinknessValue,
//    POLYLINE_ARGS_ID_mArrowHeadSizeUnits,
//    POLYLINE_ARGS_ID_mArrowHeadSizeValue,
//    POLYLINE_ARGS_ID_mColor,
//    POLYLINE_ARGS_ID_mLineStyle = POLYLINE_ARGS_ID_mColor + 3,
//    POLYLINE_ARGS_ID_mOperation,
//    POLYLINE_ARGS_ID_mCapStyle,
//    POLYLINE_ARGS_ID_mJoinStyle,
//    POLYLINE_ARGS_ID_mArrowsProp,
//    POLYLINE_ARGS_ID_mArrowsSolid,
//    POLYLINE_ARGS_ID_mArrowsDualHeaded,
//    POLYLINE_ARGS_ID_mArrowsStyleStart,
//    POLYLINE_ARGS_ID_mArrowsStyleEnd,
//    POLYLINE_ARGS_ID_mMer_x1,
//    POLYLINE_ARGS_ID_mMer_y1,
//    POLYLINE_ARGS_ID_mMer_x2,
//    POLYLINE_ARGS_ID_mMer_y2,
//    POLYLINE_ARGS_ID_mNumPoints,
//    POLYLINE_ARGS_ID_mGradientIndex,
//    POLYLINE_ARGS_ID_mGradientIndex2,
//    POLYLINE_ARGS_ID_mGradientNumPoints,
//
//}POLYLINE_ARGS_ID;
//
//typedef enum
//{
//    POLYGON_ARGS_ID_mVisible,
//    POLYGON_ARGS_ID_mMask,
//    POLYGON_ARGS_ID_mThinknessUnits,
//    POLYGON_ARGS_ID_mThinknessValue,
//    POLYGON_ARGS_ID_mLineColor,
//    POLYGON_ARGS_ID_mFillColor = POLYGON_ARGS_ID_mLineColor + 3,
//    POLYGON_ARGS_ID_mLineStyle = POLYGON_ARGS_ID_mFillColor + 3,
//    POLYGON_ARGS_ID_mFillStyle,
//    POLYGON_ARGS_ID_mJoinStyle,
//    POLYGON_ARGS_ID_mPolyfillMode,
//    POLYGON_ARGS_ID_mClosed,
//    POLYGON_ARGS_ID_mCurveStyle,
//    POLYGON_ARGS_ID_mHatchStyle,
//    POLYGON_ARGS_ID_mhRasterFillIndex,
//    POLYGON_ARGS_ID_mRasterFillStyle,
//    POLYGON_ARGS_ID_mShadingStyle,
//    POLYGON_ARGS_ID_mFillObjType,
//    POLYGON_ARGS_ID_mMer_x1,
//    POLYGON_ARGS_ID_mMer_y1,
//    POLYGON_ARGS_ID_mMer_x2,
//    POLYGON_ARGS_ID_mMer_y2,
//    POLYGON_ARGS_ID_mNumSubPgons,
//    POLYGON_ARGS_ID_mNumPoints,
//    POLYGON_ARGS_ID_mGradientIndex,
//    POLYGON_ARGS_ID_mGradientIndex2,
//    POLYGON_ARGS_ID_mGradientNumPoints,
//
//}POLYGON_ARGS_ID;
//
//typedef enum
//{
//    PASTE_ARGS_ID_mVisible,
//    PASTE_ARGS_ID_mMask,
//    PASTE_ARGS_ID_mColor,
//    PASTE_ARGS_ID_mBkColor = PASTE_ARGS_ID_mColor + 3,
//    PASTE_ARGS_ID_mOperation = PASTE_ARGS_ID_mBkColor + 3,
//    PASTE_ARGS_ID_mBkOperation,
//    PASTE_ARGS_ID_mUserRot,
//    PASTE_ARGS_ID_mImgEffectType,
//    PASTE_ARGS_ID_mColorOne,
//    PASTE_ARGS_ID_mColorTwo = PASTE_ARGS_ID_mColorOne + 4,
//    PASTE_ARGS_ID_mImageMask = PASTE_ARGS_ID_mColorTwo + 4,
//    PASTE_ARGS_ID_mAddColor,
//    PASTE_ARGS_ID_mMaskType = PASTE_ARGS_ID_mAddColor + 3,
//    PASTE_ARGS_ID_mMaskFlags,
//    PASTE_ARGS_ID_mBasePasteObjType,
//    PASTE_ARGS_ID_mFrame_x1,
//    PASTE_ARGS_ID_mFrame_y1,
//    PASTE_ARGS_ID_mFrame_x2,
//    PASTE_ARGS_ID_mFrame_y2,
//    PASTE_ARGS_ID_mDataRot,
//    PASTE_ARGS_ID_mInNumbits,
//    PASTE_ARGS_ID_mInExtBits,
//    PASTE_ARGS_ID_mOutNumBits,
//    PASTE_ARGS_ID_mTransparentType,
//    PASTE_ARGS_ID_mTransparentColor,
//    PASTE_ARGS_ID_mNumcolours,
//    PASTE_ARGS_ID_mNumAlpha,
//    PASTE_ARGS_ID_mStrLen,
//
//}PASTE_ARGS_ID;
//
//typedef enum
//{
//    TEXT_ARGS_ID_mVisible,
//    TEXT_ARGS_ID_mMask,
//    TEXT_ARGS_ID_mColor,
//    TEXT_ARGS_ID_mTypeFace = TEXT_ARGS_ID_mColor + 3,
//    TEXT_ARGS_ID_mFont,
//    TEXT_ARGS_ID_mTextWeightUnits,
//    TEXT_ARGS_ID_mTextWeightValue,
//    TEXT_ARGS_ID_mHeightUnits,
//    TEXT_ARGS_ID_mHeightValue,
//    TEXT_ARGS_ID_mWidthUnits,
//    TEXT_ARGS_ID_mWidthValue,
//    TEXT_ARGS_ID_mRotation,
//    TEXT_ARGS_ID_mJustification,
//    TEXT_ARGS_ID_mMirror,
//    TEXT_ARGS_ID_mFillStyle,
//    TEXT_ARGS_ID_mLineStyle,
//    TEXT_ARGS_ID_mHatchStyle,
//    TEXT_ARGS_ID_mFillColor,
//    TEXT_ARGS_ID_mOrientation = TEXT_ARGS_ID_mFillColor + 3,
//    TEXT_ARGS_ID_mCharset,
//    TEXT_ARGS_ID_mMer_x1,
//    TEXT_ARGS_ID_mMer_y1,
//    TEXT_ARGS_ID_mMer_x2,
//    TEXT_ARGS_ID_mMer_y2,
//    TEXT_ARGS_ID_mDropPoint_x,
//    TEXT_ARGS_ID_mDropPoint_y,
//    TEXT_ARGS_ID_mTextLength,
//    TEXT_ARGS_ID_m_pText,
//
//}TEXT_ARGS_ID;
//
//typedef enum
//{
//    ANNOT_ARGS_ID_mVisible,
//    ANNOT_ARGS_ID_mMask,
//    ANNOT_ARGS_ID_mColor,
//    ANNOT_ARGS_ID_mTypeFace = ANNOT_ARGS_ID_mColor + 3,
//    ANNOT_ARGS_ID_mFont,
//    ANNOT_ARGS_ID_mTextWeightUnits,
//    ANNOT_ARGS_ID_mTextWeightValue,
//    ANNOT_ARGS_ID_mHeightUnits,
//    ANNOT_ARGS_ID_mHeightValue,
//    ANNOT_ARGS_ID_mWidthUnits,
//    ANNOT_ARGS_ID_mWidthValue,
//    ANNOT_ARGS_ID_mRotation,
//    ANNOT_ARGS_ID_mJustification,
//    ANNOT_ARGS_ID_mMirror,
//    ANNOT_ARGS_ID_mFillStyle,
//    ANNOT_ARGS_ID_mLineStyle,
//    ANNOT_ARGS_ID_mHatchStyle,
//    ANNOT_ARGS_ID_mFillColor,
//    ANNOT_ARGS_ID_mOrientation = ANNOT_ARGS_ID_mFillColor + 3,
//    ANNOT_ARGS_ID_mCharset,
//    ANNOT_ARGS_ID_mMer_x1,
//    ANNOT_ARGS_ID_mMer_y1,
//    ANNOT_ARGS_ID_mMer_x2,
//    ANNOT_ARGS_ID_mMer_y2,
//    ANNOT_ARGS_ID_mDropPoint_x,
//    ANNOT_ARGS_ID_mDropPoint_y,
//    ANNOT_ARGS_ID_mStrLen,
//    ANNOT_ARGS_ID_m_pStr,
//
//}ANNOT_ARGS_ID;
//
//typedef enum
//{
//    PATH_ARGS_ID_mVisible,
//    PATH_ARGS_ID_mMask,
//    PATH_ARGS_ID_mCenterPoint_x,
//    PATH_ARGS_ID_mCenterPoint_y,
//    PATH_ARGS_ID_mTranslation_x,
//    PATH_ARGS_ID_mTranslation_y,
//    PATH_ARGS_ID_mFlipH,
//    PATH_ARGS_ID_mFlipV,
//    PATH_ARGS_ID_mRotAngle,
//    PATH_ARGS_ID_mShaddingStyle,
//    PATH_ARGS_ID_mGradientIndex,
//    PATH_ARGS_ID_mGradientIndex2,
//    PATH_ARGS_ID_mGradientNumPoints,
//    PATH_ARGS_ID_mGdiPlusFlatternFigure,
//    PATH_ARGS_ID_mGdiPlusFlatternFactor,
//    PATH_ARGS_ID_mClosedFigure,
//    PATH_ARGS_ID_mConnectFigure,
//} PATH_ARGS_ID;
//
////typedef enum
////{
////    EMF_ARGS_ID_mVisible,
////    EMF_ARGS_ID_mMask,
////    EMF_ARGS_ID_mSize,
////    EMF_ARGS_ID_mFrame,
////    EMF_ARGS_ID_mType,
////    EMF_ARGS_ID_mMappingMode,
////    EMF_ARGS_ID_mUserRot,
////    EMF_ARGS_ID_mXExt,
////    EMF_ARGS_ID_mYExt,
////} EMF_ARGS_ID;
//
//typedef enum
//{
//    STEXT_ARGS_ID_mVisible,
//    STEXT_ARGS_ID_mMask,
//    STEXT_ARGS_ID_mAbsPos,
//    STEXT_ARGS_ID_mScaleX,
//    STEXT_ARGS_ID_mScaleX1,
//    STEXT_ARGS_ID_mScaleY,
//    STEXT_ARGS_ID_mRotation,
//    STEXT_ARGS_ID_mPgnType,
//    STEXT_ARGS_ID_mPoint_x,
//    STEXT_ARGS_ID_mPoint_y,
//} STEXT_ARGS_ID;
//
//typedef enum
//{
//    ETXTEXT_ARGS_ID_mIsUnicode,
//    ETXTEXT_ARGS_ID_mTextLength,
//    ETXTEXT_ARGS_ID_m_pText,
//    ETXTEXT_ARGS_mFontName,
//    ETXTEXT_ARGS_ID_mFillColor,
//    ETXTEXT_ARGS_ID_mOutline = ETXTEXT_ARGS_ID_mFillColor + 3,
//    ETXTEXT_ARGS_ID_mLineStyle,
//    ETXTEXT_ARGS_ID_mFillStyle,
//    ETXTEXT_ARGS_ID_mHatchStyle,
//} ETXTEXT_ARGS_ID;
//



