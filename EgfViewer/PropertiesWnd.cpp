// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

#include "stdafx.h"

#include "PropertiesWnd.h"
#include "Resource.h"
#include "MainFrm.h"
#include "EgfViewer.h"

#include "HexDlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////
class CItemData
{
public:
    CItemData() :pAttr(nullptr), index(0), subIndex(0){};
    ~CItemData() {};

    CEgfAttr *pAttr;
    int index;
    int subIndex;
};

/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar

CPropertiesWnd::CPropertiesWnd()
{
	m_nComboHeight = 0;
    m_pEgfObj = 0;
    mXWorkSpaceFactor = 4096;
}

CPropertiesWnd::~CPropertiesWnd()
{
    Clear();
}

BEGIN_MESSAGE_MAP(CPropertiesWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_EXPAND_ALL, OnExpandAllProperties)
	ON_UPDATE_COMMAND_UI(ID_EXPAND_ALL, OnUpdateExpandAllProperties)
	ON_COMMAND(ID_SORTPROPERTIES, OnSortProperties)
	ON_UPDATE_COMMAND_UI(ID_SORTPROPERTIES, OnUpdateSortProperties)
	ON_COMMAND(ID_PROPERTIES1, OnProperties1)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES1, OnUpdateProperties1)
	ON_COMMAND(ID_PROPERTIES2, OnProperties2)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES2, OnUpdateProperties2)
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()
    ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, OnPropertyChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar message handlers

void CPropertiesWnd::AdjustLayout()
{
	if (GetSafeHwnd () == NULL || (AfxGetMainWnd() != NULL && AfxGetMainWnd()->IsIconic()))
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndObjectCombo.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), m_nComboHeight, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top + m_nComboHeight, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndPropList.SetWindowPos(NULL, rectClient.left, rectClient.top + m_nComboHeight + cyTlb, rectClient.Width(), rectClient.Height() -(m_nComboHeight+cyTlb), SWP_NOACTIVATE | SWP_NOZORDER);
}

int CPropertiesWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create combo:
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_BORDER | CBS_SORT | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (!m_wndObjectCombo.Create(dwViewStyle, rectDummy, this, 1))
	{
		TRACE0("Failed to create Properties Combo \n");
		return -1;      // fail to create
	}

	m_wndObjectCombo.AddString(_T("Application"));
	m_wndObjectCombo.AddString(_T("Properties Window"));
	m_wndObjectCombo.SetCurSel(0);

	CRect rectCombo;
	m_wndObjectCombo.GetClientRect (&rectCombo);

	m_nComboHeight = rectCombo.Height();

	if (!m_wndPropList.Create(WS_VISIBLE | WS_CHILD, rectDummy, this, 2))
	{
		TRACE0("Failed to create Properties Grid \n");
		return -1;      // fail to create
	}

	InitPropList();

	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_PROPERTIES);
	m_wndToolBar.LoadToolBar(IDR_PROPERTIES, 0, 0, TRUE /* Is locked */);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_PROPERTIES_HC : IDR_PROPERTIES, 0, 0, TRUE /* Locked */);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	AdjustLayout();
	return 0;
}

void CPropertiesWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CPropertiesWnd::OnExpandAllProperties()
{
	m_wndPropList.ExpandAll();
}

void CPropertiesWnd::OnUpdateExpandAllProperties(CCmdUI* /* pCmdUI */)
{
}

void CPropertiesWnd::OnSortProperties()
{
	m_wndPropList.SetAlphabeticMode(!m_wndPropList.IsAlphabeticMode());
}

void CPropertiesWnd::OnUpdateSortProperties(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_wndPropList.IsAlphabeticMode());
}

void CPropertiesWnd::OnProperties1()
{
    if (m_pEgfObj)
    {
        CEgfMeta *pMeta = m_pEgfObj->GetMeta();
        for (unsigned int i = 0; i < pMeta->elements.size(); i++)
        {
            if (pMeta->elements[i]->type == EGF_ELE_META_TYPE_string && pMeta->elements[i]->displayHex == true)
            {
                CHexDlg dlg;
                dlg.mStrLen = m_pEgfObj->GetAttrNumOfEles(i);
                dlg.m_pStr = m_pEgfObj->attr[i]->s;
                if (dlg.mStrLen > 0)
                    dlg.DoModal();

                break;
            }
        }
    }
}

void CPropertiesWnd::OnUpdateProperties1(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(m_pEgfObj && m_pEgfObj->GetMeta()->hasDisplayHex);
}

void CPropertiesWnd::OnProperties2()
{
    if (mXWorkSpaceFactor == 1)
        mXWorkSpaceFactor = 4096;
    else
        mXWorkSpaceFactor = 1;

    FillProperties(m_pEgfObj);
}

void CPropertiesWnd::OnUpdateProperties2(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(mXWorkSpaceFactor == 4096);
}

void CPropertiesWnd::RemoveItemData(CMFCPropertyGridProperty* pItem)
{
    CItemData *pData = pItem ? (CItemData*)pItem->GetData() : nullptr;
    if (pData) delete pData;

    int count = pItem->GetSubItemsCount();
    for (int i = 0; i < count; i++)
    {
        CMFCPropertyGridProperty *pSubItem = pItem->GetSubItem(i);
        if (pSubItem)
        {
            if (pSubItem->GetSubItemsCount() == 0)
            {
                pData = (CItemData*)pSubItem->GetData();
                if (pData) delete pData;
            }
            else
                RemoveItemData(pSubItem);
        }
    }
}

void CPropertiesWnd::Clear()
{
    int count = m_wndPropList.GetPropertyCount();
    for(int i = 0; i<count; i++)
        RemoveItemData(m_wndPropList.GetProperty(i));

    m_wndPropList.RemoveAll();
}

void CPropertiesWnd::InitPropList()
{
	SetPropListFont();

	m_wndPropList.EnableHeaderCtrl(FALSE);
	m_wndPropList.EnableDescriptionArea();
	m_wndPropList.SetVSDotNetLook();
	m_wndPropList.MarkModifiedProperties();

    return;

	CMFCPropertyGridProperty* pGroup1 = new CMFCPropertyGridProperty(_T("Appearance"));

	pGroup1->AddSubItem(new CMFCPropertyGridProperty(_T("3D Look"), (_variant_t) false, _T("Specifies the window's font will be non-bold and controls will have a 3D border")));

	CMFCPropertyGridProperty* pProp = new CMFCPropertyGridProperty(_T("Border"), _T("Dialog Frame"), _T("One of: None, Thin, Resizable, or Dialog Frame"));
	pProp->AddOption(_T("None"));
	pProp->AddOption(_T("Thin"));
	pProp->AddOption(_T("Resizable"));
	pProp->AddOption(_T("Dialog Frame"));
	pProp->AllowEdit(FALSE);

	pGroup1->AddSubItem(pProp);
	pGroup1->AddSubItem(new CMFCPropertyGridProperty(_T("Caption"), (_variant_t) _T("About"), _T("Specifies the text that will be displayed in the window's title bar")));

	m_wndPropList.AddProperty(pGroup1);

	CMFCPropertyGridProperty* pSize = new CMFCPropertyGridProperty(_T("Window Size"), 0, TRUE);

	pProp = new CMFCPropertyGridProperty(_T("Height"), (_variant_t) 250l, _T("Specifies the window's height"));
	pProp->EnableSpinControl(TRUE, 50, 300);
	pSize->AddSubItem(pProp);

	pProp = new CMFCPropertyGridProperty( _T("Width"), (_variant_t) 150l, _T("Specifies the window's width"));
	pProp->EnableSpinControl(TRUE, 50, 200);
	pSize->AddSubItem(pProp);

	m_wndPropList.AddProperty(pSize);

	CMFCPropertyGridProperty* pGroup2 = new CMFCPropertyGridProperty(_T("Font"));

	LOGFONT lf;
	CFont* font = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	font->GetLogFont(&lf);

	lstrcpy(lf.lfFaceName, _T("Arial"));

	pGroup2->AddSubItem(new CMFCPropertyGridFontProperty(_T("Font"), lf, CF_EFFECTS | CF_SCREENFONTS, _T("Specifies the default font for the window")));
	pGroup2->AddSubItem(new CMFCPropertyGridProperty(_T("Use System Font"), (_variant_t) true, _T("Specifies that the window uses MS Shell Dlg font")));

	m_wndPropList.AddProperty(pGroup2);

	CMFCPropertyGridProperty* pGroup3 = new CMFCPropertyGridProperty(_T("Misc"));
	pProp = new CMFCPropertyGridProperty(_T("(Name)"), _T("Application"));
	pProp->Enable(FALSE);
	pGroup3->AddSubItem(pProp);

	CMFCPropertyGridColorProperty* pColorProp = new CMFCPropertyGridColorProperty(_T("Window Color"), RGB(210, 192, 254), NULL, _T("Specifies the default window color"));
	pColorProp->EnableOtherButton(_T("Other..."));
	pColorProp->EnableAutomaticButton(_T("Default"), ::GetSysColor(COLOR_3DFACE));
	pGroup3->AddSubItem(pColorProp);

	static const TCHAR szFilter[] = _T("Icon Files(*.ico)|*.ico|All Files(*.*)|*.*||");
	pGroup3->AddSubItem(new CMFCPropertyGridFileProperty(_T("Icon"), TRUE, _T(""), _T("ico"), 0, szFilter, _T("Specifies the window icon")));

	pGroup3->AddSubItem(new CMFCPropertyGridFileProperty(_T("Folder"), _T("c:\\")));

	m_wndPropList.AddProperty(pGroup3);

	CMFCPropertyGridProperty* pGroup4 = new CMFCPropertyGridProperty(_T("Hierarchy"));

	CMFCPropertyGridProperty* pGroup41 = new CMFCPropertyGridProperty(_T("First sub-level"));
	pGroup4->AddSubItem(pGroup41);

	CMFCPropertyGridProperty* pGroup411 = new CMFCPropertyGridProperty(_T("Second sub-level"));
	pGroup41->AddSubItem(pGroup411);

	pGroup411->AddSubItem(new CMFCPropertyGridProperty(_T("Item 1"), (_variant_t) _T("Value 1"), _T("This is a description")));
	pGroup411->AddSubItem(new CMFCPropertyGridProperty(_T("Item 2"), (_variant_t) _T("Value 2"), _T("This is a description")));
	pGroup411->AddSubItem(new CMFCPropertyGridProperty(_T("Item 3"), (_variant_t) _T("Value 3"), _T("This is a description")));

	pGroup4->Expand(FALSE);
	m_wndPropList.AddProperty(pGroup4);
}

void CPropertiesWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_wndPropList.SetFocus();
}

void CPropertiesWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetPropListFont();
}

void CPropertiesWnd::SetPropListFont()
{
	::DeleteObject(m_fntPropList.Detach());

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;

	m_fntPropList.CreateFontIndirect(&lf);

	m_wndPropList.SetFont(&m_fntPropList);
	m_wndObjectCombo.SetFont(&m_fntPropList);
}

CString GetLineStyle(int style)
{
    CString cs;

    switch (style)
    {
    case 0:
    case 246:
        cs = _T("0-Solid");
        break;
    case 1:
    case 247:
        cs = _T("1-Dash");
        break;
    case 2:
    case 248:
        cs = _T("2-Dot");
        break;
    case 3:
    case 249:
        cs = _T("3-DashDot");
        break;
    case 4:
    case 250:
        cs = _T("4-DashDotDot");
        break;
    default:
        cs.Format(L"%d", style);
        break;
    }

    return cs;
}

CString GetCapStyle(int style)
{
    CString cs;

    switch (style)
    {
    case 0:
        cs = _T("0-Round");
        break;
    case 1:
    case 11:
        cs.Format(_T("%d-Square"), style);
        break;
    case 2:
        cs = _T("2-Multiple");
        break;
    case 3:
        cs = _T("3-Unassigned");
        break;
    case 10:
        cs = _T("10-Flat");
        break;
    case 13:
        cs = _T("13-Triangle");
        break;
    case 16:
        cs = _T("16-NoAnchor");
        break;
    case 17:
        cs = _T("17-SquareAnchor");
        break;
    case 18:
        cs = _T("18-RoundAnchor");
        break;
    case 19:
        cs = _T("19-DiamondAnchor");
        break;
    case 20:
        cs = _T("20-ArrowAnchor");
        break;
    case 21:
        cs = _T("21-CapCustom");
        break;
    default:
        cs.Format(L"%d", style);
        break;
    }

    return cs;
}

CString GetOperation(int op)
{
    CString cs;

    switch (op)
    {
    case 0:
        cs = _T("0-Opaque");
        break;
    case 1:
        cs = _T("1-Translucent");
        break;
    case 2:
        cs = _T("2-Sketch");
        break;
    case 3:
        cs = _T("3-XOR Pen");
        break;
    case 4:
        cs = _T("4-Merge Pen");
        break;
    case 5:
        cs = _T("5-Mask Pen");
        break;
    case 6:
        cs = _T("6-Multiple");
        break;
    case 7:
        cs = _T("7-Unassigned");
        break;
    case 8:
        cs = _T("8-NotXorPen");
        break;
    case 9:
        cs = _T("9-Transparent");
        break;
    case 11:
        cs = _T("11-NOP");
        break;
    case 12:
        cs = _T("12-AlphaBlendMask");
        break;
    case 13:
        cs = _T("13-AlphaBlend");
        break;
    case 14:
        cs = _T("14-PreAlphaBlend");
        break;
    case 15:
        cs = _T("15-ImageMask Mask");
        break;
    case 16:
        cs = _T("16-ImageMask Image");
        break;
    default:
        cs.Format(L"%d", op);
        break;
    }

    return cs;
}

CString GetFillStyle(int style)
{
    CString cs;

    switch (style)
    {
    case 0:
        cs = _T("0-Transparent");
        break;
    case 1:
        cs = _T("1-Opaque");
        break;
    case 2:
        cs = _T("2-Erase");
        break;
    case 3:
        cs = _T("3-Translucent");
        break;
    case 4:
        cs = _T("4-Hatch");
        break;
    case 5:
        cs = _T("5-Sketch");
        break;
    case 6:
        cs = _T("6-Multiple");
        break;
    case 7:
        cs = _T("7-Unassigned");
        break;
    case 8:
        cs = _T("8-FillRaster");
        break;
    case 9:
        cs = _T("9-LinearGradient");
        break;
    case 10:
        cs = _T("10-PathGradient");
        break;
    case 11:
        cs = _T("11-XOR");
        break;
    case 12:
        cs = _T("12-Merge");
        break;
    case 13:
        cs = _T("13-Mask");
        break;
    case 14:
        cs = _T("14-NotXOR");
        break;
    case 15:
        cs = _T("15-NotMerge");
        break;
    case 16:
        cs = _T("16-NOP");
        break;
    case 17:
        cs = _T("17-FILLNULL");
        break;
    case 18:
        cs = _T("18-FillObj");
        break;
    case 19:
        cs = _T("19-FillInvert");
        break;
    default:
        cs.Format(L"%d", style);
        break;
    }

    return cs;
}

CString GetJoinStyle(int style)
{
    CString cs;

    switch (style)
    {
    case 0:
    case 12:
        cs.Format(_T("%d-Round"), style);
        break;
    case 1:
    case 10:
        cs.Format(_T("%d-Miter"), style);
        break;
    case 2:
        cs = _T("2-Multiple");
        break;
    case 3:
        cs = _T("3-Unassigned");
        break;
    case 4:
        cs = _T("4-Diamond");
        break;
    case 11:
        cs = _T("11-Bevel");
        break;
    case 13:
        cs = _T("13-MiterClipped");
        break;
    default:
        cs.Format(L"%d", style);
        break;
    }

    return cs;
}

CString GetCurveStyle(int style)
{
    CString cs;

    switch (style)
    {
    case 0:
        cs = _T("0-None");
        break;
    case 1:
        cs = _T("1-Bezier Spline");
        break;
    case 2:
        cs = _T("2-Cardinal Spline");
        break;
    case 3:
        cs = _T("3-Unassigned");
        break;
    default:
        cs.Format(L"%d", style);
        break;
    }

    return cs;
}

CString GetHatchStyle(int style)
{
    CString cs;

    cs.Format(L"%d", style);

    return cs;
}

CString GetShadingStyle(int style)
{
    CString cs;

    cs.Format(L"%d", style);

    return cs;
}

CString GetEffectType(int style)
{
    CString cs;

    switch (style)
    {
    case 0:
        cs = _T("0-None");
        break;
    case 1:
        cs = _T("1-ColorChange");
        break;
    case 2:
        cs = _T("2-DuoTone");
        break;
    case 3:
        cs = _T("3-GrayScale");
        break;
    default:
        cs.Format(L"%d", style);
        break;
    }

    return cs;
}

CString GetRasterType(int style)
{
    CString cs;

    switch (style)
    {
    case 0: // RSL_IDF_NONE
        cs = _T("0-None");
        break;
    case 1: // RSL_IDF_NRLE
        cs = _T("1-NRLE");
        break;
    case 2: // RSL_IDF_G3MT
        cs = _T("2-G3MT");
        break;
    case 3: // RSL_IDF_G3M
        cs = _T("3-G3M");
        break;
    case 4: // RSL_IDF_TBL
        cs = _T("4-TBL");
        break;
    case 5: // RSL_IDF_8BIT
        cs = _T("5-8BIT");
        break;
    case 6: // RSL_IDF_PBITS
        cs = _T("6-PBITS");
        break;
    case 7: // RSL_IDF_4BIT
        cs = _T("7-4BIT");
        break;
    case 8: // RSL_IDF_24BP
        cs = _T("8-24BP");
        break;
    case 9: // RSL_IDF_BPBIT
        cs = _T("9-BPBIT");
        break;
    case 10: // RSL_IDF_G3MTIFF
        cs = _T("10-G3MTIFF");
        break;
    case 11: // RSL_IDF_G3M2D
        cs = _T("11-G3M2D");
        break;
    case 12: // RSL_IDF_G4M
        cs = _T("12-G4M");
        break;
    case 13: // RSL_IDF_CC1
        cs = _T("13-CC1");
        break;
    case 14: // RSL_IDF_CC1RGB
        cs = _T("14-CC1RGB");
        break;
    case 15: // RSL_IDF_24RGB
        cs = _T("15-24RGB");
        break;
    case 16: // RSL_IDF_G4MT
        cs = _T("16-G4MT");
        break;
    case 17: // RSL_IDF_JPEG
        cs = _T("17-JPEG");
        break;
    case 18: // RSL_IDF_24CBP
        cs = _T("18-24CBP");
        break;
    case 19: // RSL_IDF_XG4
        cs = _T("19-XG4");
        break;
    case 20: // RSL_IDF_FILE
        cs = _T("20-FILE");
        break;
    case 21: // RSL_IDF_SJPEG
        cs = _T("21-SJPEG");
        break;
    case 22: // RSL_IDF_G3LTIFF
        cs = _T("22-G3LTIFF");
        break;
    case 23: // RSL_IDF_G3L2D
        cs = _T("23-G3L2D");
        break;
    case 24: // RSL_IDF_G4L
        cs = _T("24-G4L");
        break;
    case 25: // RSL_IDF_G3L
        cs = _T("25-G3L");
        break;
    case 26: // RSL_IDF_RAW
        cs = _T("26-RAW");
        break;
    case 27: // RSL_IDF_RAWT
        cs = _T("27-RAWT");
        break;
    case 28: // RSL_IDF_LZ77
        cs = _T("28-LZ77");
        break;
    case 29: // RSL_IDF_LZW
        cs = _T("29-LZW");
        break;
    case 30: // RSL_IDF_32RGB
        cs = _T("30-32RGB");
        break;
    case 31: // RSL_IDF_32BP
        cs = _T("31-32BP");
        break;
    case 32: // RSL_IDF_CC1RGBA
        cs = _T("32-CC1RGBA");
        break;
    default:
        cs.Format(L"%d", style);
        break;
    }

    return cs;
}

void CPropertiesWnd::InsertPoint(CMFCPropertyGridProperty *pParent, CString name, CEgfPoint &pt, CEgfAttr *pAttr, int index)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CMFCPropertyGridProperty *pEle = new CMFCPropertyGridProperty(_T("x"), (_variant_t)(pt.x / mXWorkSpaceFactor), _T("X axis of point"));
    InsertItemData(pAttr, pEle, index, 0);
    pGroupProp->AddSubItem(pEle);
    pEle = new CMFCPropertyGridProperty(_T("y"), (_variant_t)(pt.y / mXWorkSpaceFactor), _T("Y axis of point"));
    InsertItemData(pAttr, pEle, index, 1);
    pGroupProp->AddSubItem(pEle);

    pParent->AddSubItem(pGroupProp);
}

void CPropertiesWnd::InsertPoints(CMFCPropertyGridProperty *pParent, CString name, int numPts, CEgfPoint *pts, CEgfAttr *pAttr)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CString cs;
    for (int i = 0; i < numPts; i++)
    {
        cs.Format(L"point %d", i);
        InsertPoint(pGroupProp, cs, pts[i], pAttr, i);
    }
    pParent->AddSubItem(pGroupProp);
}

void CPropertiesWnd::InsertPoints(CMFCPropertyGridProperty *pParent, CString name, CVector<CEgfPoint*> &pts, CEgfAttr *pAttr)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CString cs;
    for (unsigned int i = 0; i < pts.size(); i++)
    {
        cs.Format(L"point %d", i);
        InsertPoint(pGroupProp, cs, *pts[i], pAttr, i);
    }
    pParent->AddSubItem(pGroupProp);
}

void CPropertiesWnd::InsertIntArray(CMFCPropertyGridProperty *pParent, CString name, int count, int *value, CEgfAttr *pAttr)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CString cs;
    CMFCPropertyGridProperty *pEle;
    for (int i = 0; i < count; i++)
    {
        cs.Format(L"%d", i);
        pEle = new CMFCPropertyGridProperty(cs, (_variant_t)value[i], _T(""));
        InsertItemData(pAttr, pEle, i, 0);
        pGroupProp->AddSubItem(pEle);
    }
    pParent->AddSubItem(pGroupProp);
}

void CPropertiesWnd::InsertFloatArray(CMFCPropertyGridProperty *pParent, CString name, int count, float *value, CEgfAttr *pAttr)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CString cs;
    CMFCPropertyGridProperty *pEle;
    for (int i = 0; i < count; i++)
    {
        cs.Format(L"%d", i);
        pEle = new CMFCPropertyGridProperty(cs, (_variant_t)value[i], _T(""));
        InsertItemData(pAttr, pEle, i, 0);
        pGroupProp->AddSubItem(pEle);
    }
    pParent->AddSubItem(pGroupProp);
}

void CPropertiesWnd::InsertColor3Array(CMFCPropertyGridProperty *pParent, CString name, int count, int *value, CEgfAttr *pAttr)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CString cs;
    for (int i = 0; i < count; i++)
    { 
        cs.Format(L"%d", i);
        int cValue = value[i];

        CMFCPropertyGridColorProperty *pColor = new CMFCPropertyGridColorProperty(cs,
            RGB(((unsigned char*)&cValue)[0], ((unsigned char*)&cValue)[1], ((unsigned char*)&cValue)[2]),
            NULL);// , pObj->GetAttrName(i));
        InsertItemData(pAttr, pColor, i, 0);
        pGroupProp->AddSubItem(pColor);
    }
    pParent->AddSubItem(pGroupProp);
}
void CPropertiesWnd::InsertShortArray(CMFCPropertyGridProperty *pParent, CString name, int count, short *value, CEgfAttr *pAttr)
{
	CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
	CString cs;
	CMFCPropertyGridProperty *pEle;
	for (int i = 0; i < count; i++)
	{
		cs.Format(L"%d", i);
		pEle = new CMFCPropertyGridProperty(cs, (_variant_t)value[i], _T(""));
        InsertItemData(pAttr, pEle, i, 0);
        pGroupProp->AddSubItem(pEle);
	}
	pParent->AddSubItem(pGroupProp);
}
void CPropertiesWnd::InsertMatrix(CMFCPropertyGridProperty *pParent, CString name, float *value, CEgfAttr *pAttr)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CString cs;
    CMFCPropertyGridProperty *pEle;
    for (int i = 0; i < 6; i++)
    {
        cs.Format(L"%d", i);
        pEle = new CMFCPropertyGridProperty(cs, (_variant_t)value[i], _T(""));
        InsertItemData(pAttr, pEle, 0, i);
        pGroupProp->AddSubItem(pEle);
    }
    pParent->AddSubItem(pGroupProp);
}

void CPropertiesWnd::InsertMer(CMFCPropertyGridProperty *pParent, CString name, CEgfMer &mer, CEgfAttr *pAttr)
{
    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(name);
    CMFCPropertyGridProperty *pEle = new CMFCPropertyGridProperty(_T("x1"), (_variant_t)(mer.x1 / mXWorkSpaceFactor), _T(""));
    InsertItemData(pAttr, pEle, 0, 0);
    pGroupProp->AddSubItem(pEle);
    pEle = new CMFCPropertyGridProperty(_T("y1"), (_variant_t)(mer.y1 / mXWorkSpaceFactor), _T(""));
    InsertItemData(pAttr, pEle, 0, 1);
    pGroupProp->AddSubItem(pEle);
    pEle = new CMFCPropertyGridProperty(_T("x2"), (_variant_t)(mer.x2 / mXWorkSpaceFactor), _T(""));
    InsertItemData(pAttr, pEle, 0, 2);
    pGroupProp->AddSubItem(pEle);
    pEle = new CMFCPropertyGridProperty(_T("y2"), (_variant_t)(mer.y2 / mXWorkSpaceFactor), _T(""));
    InsertItemData(pAttr, pEle, 0, 3);
    pGroupProp->AddSubItem(pEle);
    pParent->AddSubItem(pGroupProp);
}

void CPropertiesWnd::InsertUnitValue(CMFCPropertyGridProperty *pParent, CString name, int units, int value)
{
    CMFCPropertyGridProperty *pSubGroupProp = new CMFCPropertyGridProperty(name);
    CMFCPropertyGridProperty *pEle = new CMFCPropertyGridProperty(_T("Units"), (_variant_t)units, _T("Units"));
    pSubGroupProp->AddSubItem(pEle);
    pEle = new CMFCPropertyGridProperty(_T("Value"), (_variant_t)value, _T(""));
    pSubGroupProp->AddSubItem(pEle);
    pParent->AddSubItem(pSubGroupProp);
}

void CPropertiesWnd::InsertItemData(CEgfAttr *pAttr, CMFCPropertyGridProperty *pEle, int index, int subIndex)
{
    if (!pAttr || !pEle || pAttr->type == EGF_ELE_META_TYPE_none) return;

    CItemData *pItemData = new CItemData();
    pItemData->pAttr = pAttr;
    pItemData->index = index;
    pItemData->subIndex = subIndex;
    pEle->SetData((DWORD_PTR)pItemData);
}

void CPropertiesWnd::FillProperties(CEgfObj *pObj)
{
    Clear();

    m_pEgfObj = pObj;

    if (pObj == NULL || pObj->attr.size() == 0) return;

    int comboSize = m_wndObjectCombo.GetCount();
    for (int i = comboSize - 1; i >= 0; i--)
        m_wndObjectCombo.DeleteString(i);


    CMFCPropertyGridProperty *pGroupProp = new CMFCPropertyGridProperty(pObj->GetMetaName());
    CMFCPropertyGridProperty *pEle = NULL;
    CMFCPropertyGridColorProperty *pColor = NULL;

    std::map<int, int>* pDisplayMap = pObj->GetAttrDisplayOrderMap();
    int numAttr = (int)pDisplayMap->size();
    for (int j = 0; j < numAttr; j++)
    {
        int i = (*pDisplayMap)[j];
        int type = pObj->GetAttrType(i);
        switch (type)
        {
        case EGF_ELE_META_TYPE_8bool:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)pObj->attr[i]->b, pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_byte:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)pObj->attr[i]->uc, pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_intBool:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)(pObj->attr[i]->i > 0), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_int:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)pObj->attr[i]->i, pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_float:
        case EGF_ELE_META_TYPE_double:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)pObj->attr[i]->f, pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_color3:
            pColor = new CMFCPropertyGridColorProperty(pObj->GetAttrName(i),
                RGB(((unsigned char*)&pObj->attr[i]->i)[0], ((unsigned char*)&pObj->attr[i]->i)[1], ((unsigned char*)&pObj->attr[i]->i)[2]),
                NULL, pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pColor, 0, 0);
            pGroupProp->AddSubItem(pColor);
            break;
        case EGF_ELE_META_TYPE_color3Array:
        case EGF_ELE_META_TYPE_color4Array:
            {
                int count = pObj->GetAttrNumOfEles(i);
                if (count > 0)
                {
                    InsertColor3Array(pGroupProp, pObj->GetAttrName(i), pObj->GetAttrNumOfEles(i), pObj->attr[i]->ia, pObj->attr[i]);
                }
            }
            break;
        case EGF_ELE_META_TYPE_color4:
            {            
                CMFCPropertyGridProperty *pSubGroupProp = new CMFCPropertyGridProperty(pObj->GetAttrName(i));
                pEle = new CMFCPropertyGridProperty(_T("R"), (_variant_t)((unsigned int)(((unsigned char*)&pObj->attr[i]->i)[0])), _T("Red"));
                InsertItemData(pObj->attr[i], pEle, 0, 0);
                pSubGroupProp->AddSubItem(pEle);
                pEle = new CMFCPropertyGridProperty(_T("G"), (_variant_t)((unsigned int)(((unsigned char*)&pObj->attr[i]->i)[1])), _T("Green"));
                InsertItemData(pObj->attr[i], pEle, 0, 1);
                pSubGroupProp->AddSubItem(pEle);
                pEle = new CMFCPropertyGridProperty(_T("B"), (_variant_t)((unsigned int)(((unsigned char*)&pObj->attr[i]->i)[2])), _T("Blue"));
                InsertItemData(pObj->attr[i], pEle, 0, 2);
                pSubGroupProp->AddSubItem(pEle);
                pEle = new CMFCPropertyGridProperty(_T("A"), (_variant_t)((unsigned int)(((unsigned char*)&pObj->attr[i]->i)[3])), _T("Alpha"));
                InsertItemData(pObj->attr[i], pEle, 0, 3);
                pSubGroupProp->AddSubItem(pEle);
                pGroupProp->AddSubItem(pSubGroupProp);
            }
            break;
        case EGF_ELE_META_TYPE_point:
            InsertPoint(pGroupProp, pObj->GetAttrName(i), *pObj->attr[i]->point, pObj->attr[i], 0);
            break;
        case EGF_ELE_META_TYPE_points:
            if (pObj->attr[i]->points != NULL)
                InsertPoints(pGroupProp, pObj->GetAttrName(i), pObj->attr[i]->points->points, pObj->attr[i]);
            break;
        case EGF_ELE_META_TYPE_mer:
            InsertMer(pGroupProp, pObj->GetAttrName(i), *pObj->attr[i]->mer, pObj->attr[i]);
            break;
        case EGF_ELE_META_TYPE_matrix:
            InsertMatrix(pGroupProp, pObj->GetAttrName(i), pObj->attr[i]->matrix, pObj->attr[i]);
            break;
        case EGF_ELE_META_TYPE_intArray:
            if (pObj->GetAttrNumOfEles(i) > 0)
                InsertIntArray(pGroupProp, pObj->GetAttrName(i), pObj->GetAttrNumOfEles(i), pObj->attr[i]->ia, pObj->attr[i]);
            break;
        case EGF_ELE_META_TYPE_floatArray:
            if (pObj->GetAttrNumOfEles(i) > 0)
                InsertFloatArray(pGroupProp, pObj->GetAttrName(i), pObj->GetAttrNumOfEles(i), pObj->attr[i]->fa, pObj->attr[i]);
            break;
		case EGF_ELE_META_TYPE_shortArray:
            if (pObj->GetAttrNumOfEles(i) > 0)
				InsertShortArray(pGroupProp, pObj->GetAttrName(i), pObj->GetAttrNumOfEles(i), pObj->attr[i]->sa, pObj->attr[i]);
            break;
        case EGF_ELE_META_TYPE_string:
            {
                int len = pObj->GetAttrNumOfEles(i);
                if (len > 0)
                {
                    CString cs(pObj->attr[i]->s);
                    pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)cs, pObj->GetAttrName(i));
                    InsertItemData(pObj->attr[i], pEle, 0, 0);
                    pGroupProp->AddSubItem(pEle);


                    // convert into hex string
                    if (pObj->GetDisplayHex(i))
                    {
                        CString subs("");
                        for (int k = 0; k < len; k++)
                        {
                            cs.Format(_T("%02X "), (unsigned char)pObj->attr[i]->s[k]);
                            subs += cs;
                        }
                        pEle = new CMFCPropertyGridProperty(_T("Hex"), (_variant_t)subs, _T("Hex string"));
                        InsertItemData(pObj->attr[i], pEle, 0, 0);
                        pGroupProp->AddSubItem(pEle);
                    }
                }
            }
            break;
        case EGF_ELE_META_TYPE_lineStyle:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetLineStyle(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_capStyle:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetCapStyle(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_operation:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetOperation(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_fillStyle:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetFillStyle(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_hatchStyle:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetHatchStyle(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_shadingStyle:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetShadingStyle(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_joinStyle:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetJoinStyle(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_curveStyle:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetCurveStyle(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_effectType:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetEffectType(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_rasterType:
            pEle = new CMFCPropertyGridProperty(pObj->GetAttrName(i), (_variant_t)GetRasterType(pObj->attr[i]->i), pObj->GetAttrName(i));
            InsertItemData(pObj->attr[i], pEle, 0, 0);
            pGroupProp->AddSubItem(pEle);
            break;
        case EGF_ELE_META_TYPE_rasterStruct:
            break;
        case EGF_ELE_META_TYPE_jpegStruct:
            break;

        case EGF_ELE_META_TYPE_none:
        default:
            AfxMessageBox(_T("PropertiesWnd cannot display an unknown object type!"));
            break;
        }
    }

    m_wndPropList.AddProperty(pGroupProp);

}

LRESULT CPropertiesWnd::OnPropertyChanged( __in WPARAM wparam, __in LPARAM lparam)
{
    CMFCPropertyGridProperty * pProperty = (CMFCPropertyGridProperty *)lparam;
    CItemData *pItemData = (CItemData*)pProperty->GetData();

    if (pItemData->pAttr == nullptr) return 0;

    COleVariant varValue;

    switch (pItemData->pAttr->type)
    {
    case EGF_ELE_META_TYPE_8bool:
        pItemData->pAttr->b = pProperty->GetValue().boolVal;
        break;
    case EGF_ELE_META_TYPE_byte:
        pItemData->pAttr->uc = pProperty->GetValue().bVal;
        break;
    case EGF_ELE_META_TYPE_intBool:
        pItemData->pAttr->i = pProperty->GetValue().intVal;
        break;
    case EGF_ELE_META_TYPE_int:
        pItemData->pAttr->i = pProperty->GetValue().intVal;
        break;
    case EGF_ELE_META_TYPE_float:
    case EGF_ELE_META_TYPE_double:
        pItemData->pAttr->f = pProperty->GetValue().fltVal;
        break;
    case EGF_ELE_META_TYPE_color3:
        {
            CMFCPropertyGridColorProperty *pColor = (CMFCPropertyGridColorProperty*)pProperty;
            COLORREF color = pColor->GetColor();
            pItemData->pAttr->i = (int)color;
        }
        break;
    case EGF_ELE_META_TYPE_color3Array:
    case EGF_ELE_META_TYPE_color4Array:
        {
            CMFCPropertyGridColorProperty *pColor = (CMFCPropertyGridColorProperty*)pProperty;
            COLORREF color = pColor->GetColor();
            pItemData->pAttr->ia[pItemData->index] = (int)color;
        }
        break;
    case EGF_ELE_META_TYPE_color4:
        {
            unsigned char byteValue = (unsigned char)pProperty->GetValue().uintVal;
            unsigned int value = (byteValue << (( 3 - pItemData->subIndex) * 8));
            pItemData->pAttr->i = value;
        }
    break;
    case EGF_ELE_META_TYPE_point:
        if (pItemData->subIndex == 0) pItemData->pAttr->point->x = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
        else pItemData->pAttr->point->y = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
        break;
    case EGF_ELE_META_TYPE_points:
        if(pItemData->subIndex == 0) pItemData->pAttr->points->points[pItemData->index]->x = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
        else pItemData->pAttr->points->points[pItemData->index]->y = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
        break;
    case EGF_ELE_META_TYPE_mer:
        switch (pItemData->subIndex)
        {
        case 0:
            pItemData->pAttr->mer->x1 = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
            break;
        case 1:
            pItemData->pAttr->mer->y1 = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
            break;
        case 2:
            pItemData->pAttr->mer->x2 = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
            break;
        case 3:
            pItemData->pAttr->mer->y2 = pProperty->GetValue().fltVal * mXWorkSpaceFactor;
            break;
        }
        break;
    case EGF_ELE_META_TYPE_matrix:
        pItemData->pAttr->matrix[pItemData->subIndex] = pProperty->GetValue().fltVal;
        break;
    case EGF_ELE_META_TYPE_intArray:
        pItemData->pAttr->ia[pItemData->index] = pProperty->GetValue().intVal;
        break;
    case EGF_ELE_META_TYPE_floatArray:
        pItemData->pAttr->fa[pItemData->index] = pProperty->GetValue().fltVal;
        break;
    case EGF_ELE_META_TYPE_shortArray:
        pItemData->pAttr->sa[pItemData->index] = pProperty->GetValue().iVal;
        break;
    case EGF_ELE_META_TYPE_string:
        if (pItemData->pAttr->s) delete[] pItemData->pAttr->s;
        pItemData->pAttr->s = _com_util::ConvertBSTRToString(pProperty->GetValue().bstrVal);
        break;
    case EGF_ELE_META_TYPE_lineStyle:
    case EGF_ELE_META_TYPE_capStyle:
    case EGF_ELE_META_TYPE_operation:
    case EGF_ELE_META_TYPE_fillStyle:
    case EGF_ELE_META_TYPE_hatchStyle:
    case EGF_ELE_META_TYPE_shadingStyle:
    case EGF_ELE_META_TYPE_joinStyle:
    case EGF_ELE_META_TYPE_curveStyle:
    case EGF_ELE_META_TYPE_effectType:
    case EGF_ELE_META_TYPE_rasterType:
        {
            std::string s(_com_util::ConvertBSTRToString(pProperty->GetValue().bstrVal));
            size_t pos = s.find('-');
            if (pos != std::string::npos)
            {
                s = s.substr(0, pos);
                pItemData->pAttr->i = atoi(s.c_str());
            }
            else pItemData->pAttr->i = atoi(s.c_str());
        }
        break;

    case EGF_ELE_META_TYPE_none:
    default:
        break;
    }

    ((CMainFrame*)AfxGetMainWnd())->ReconvertEgf2Uv(m_pEgfObj);

    return 0;
}