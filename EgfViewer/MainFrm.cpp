// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "EgfViewer.h"
#include "EgfViewerDoc.h"
#include "EgfViewerView.h"
#include <vector>
#include <xstring>

#include "MainFrm.h"
#include "Codepage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)
	ON_WM_CREATE()
	ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
	ON_COMMAND(ID_FILE_PRINT, &CMainFrame::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CMainFrame::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMainFrame::OnFilePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, &CMainFrame::OnUpdateFilePrintPreview)
    ON_COMMAND(ID_VIEW_CLASSVIEW, &CMainFrame::OnViewClassView)
    ON_UPDATE_COMMAND_UI(ID_VIEW_CLASSVIEW, &CMainFrame::OnUpdateViewClassView)
    ON_COMMAND(ID_VIEW_FILEVIEW, &CMainFrame::OnViewFileView)
    ON_UPDATE_COMMAND_UI(ID_VIEW_FILEVIEW, &CMainFrame::OnUpdateViewFileView)
    ON_COMMAND(ID_VIEW_GLOBALVIEW, &CMainFrame::OnViewGlobalView)
    ON_UPDATE_COMMAND_UI(ID_VIEW_GLOBALVIEW, &CMainFrame::OnUpdateViewGlobalView)
    ON_COMMAND(ID_VIEW_PROPERTIESWND, &CMainFrame::OnViewPropertiesWindow)
    ON_UPDATE_COMMAND_UI(ID_VIEW_PROPERTIESWND, &CMainFrame::OnUpdateViewPropertiesWindow)
    ON_COMMAND(IDD_DLG_SEARCH, &CMainFrame::OnViewSearchView)
    ON_UPDATE_COMMAND_UI(IDD_DLG_SEARCH, &CMainFrame::OnUpdateViewSearchView)
    ON_COMMAND(IDD_DLG_SEARCH_RESULT, &CMainFrame::OnViewSearchResult)
    ON_UPDATE_COMMAND_UI(IDD_DLG_SEARCH_RESULT, &CMainFrame::OnUpdateViewSearchResult)
    ON_COMMAND(ID_FILE_IMSCORE_TEST, &CMainFrame::OnFileImscoreTest)
    ON_UPDATE_COMMAND_UI(ID_FILE_IMSCORE_TEST, &CMainFrame::OnUpdateFileImscoreTest)
    ON_COMMAND(ID_CHECK_HOME_APPLY_CLIP, &CMainFrame::OnCheckHomeApplyClip)
    ON_UPDATE_COMMAND_UI(ID_CHECK_HOME_APPLY_CLIP, &CMainFrame::OnUpdateCheckHomeApplyClip)
END_MESSAGE_MAP()

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_VS_2008);
    
    m_pEgfFile = NULL;
    m_bApplyClip = true;

    wchar_t buffer[MAX_PATH];
    GetModuleFileName(NULL, buffer, MAX_PATH);
    CString cs = buffer;
    int pos = cs.ReverseFind('\\');
    if (pos != -1)
        m_csExePath = cs.Left(pos + 1);
    else
        m_csExePath = _T("");
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	BOOL bNameValid;

	m_wndRibbonBar.Create(this);
	m_wndRibbonBar.LoadFromResource(IDR_RIBBON);

    InitializeRibbon();

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	CString strTitlePane1;
	CString strTitlePane2;
	bNameValid = strTitlePane1.LoadString(IDS_STATUS_PANE1);
	ASSERT(bNameValid);
	bNameValid = strTitlePane2.LoadString(IDS_STATUS_PANE2);
	ASSERT(bNameValid);
	m_wndStatusBar.AddElement(new CMFCRibbonStatusBarPane(ID_STATUSBAR_PANE1, strTitlePane1, TRUE), strTitlePane1);
	m_wndStatusBar.AddExtendedElement(new CMFCRibbonStatusBarPane(ID_STATUSBAR_PANE2, strTitlePane2, TRUE), strTitlePane2);

	// enable Visual Studio 2005 style docking window behavior
	CDockingManager::SetDockingMode(DT_SMART);
	// enable Visual Studio 2005 style docking window auto-hide behavior
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	// Load menu item image (not placed on any standard toolbars):
	CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);

	// create docking windows
	if (!CreateDockingWindows())
	{
		TRACE0("Failed to create docking windows\n");
		return -1;
	}

	m_wndFileView.EnableDocking(CBRS_ALIGN_ANY);
	m_wndClassView.EnableDocking(CBRS_ALIGN_ANY);
    m_wndGlobalView.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndFileView);
	CDockablePane* pTabbedBar = NULL;
	m_wndClassView.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);
    m_wndGlobalView.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);
	m_wndProperties.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndProperties);

    m_wndSearchView.EnableDocking(CBRS_ALIGN_ANY);
    m_wndSearchResult.EnableDocking(CBRS_ALIGN_ANY);
    DockPane(&m_wndSearchView);
    CDockablePane* pTabbedBar1;
    m_wndSearchResult.AttachToTabWnd(&m_wndSearchView, DM_SHOW, TRUE, &pTabbedBar1);
    //DockPane(&m_wndSearchResult);

	// set the visual manager and style based on persisted value
	OnApplicationLook(theApp.m_nAppLook);


	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWndEx::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

BOOL CMainFrame::CreateDockingWindows()
{
	BOOL bNameValid;

	// Create class view
	CString strClassView = L"Page Egfs";
	//bNameValid = strClassView.LoadString(IDS_CLASS_VIEW);
	//ASSERT(bNameValid);
	if (!m_wndClassView.Create(strClassView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_CLASSVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Class View window\n");
		return FALSE; // failed to create
	}

	// Create file view
	CString strFileView;
	bNameValid = strFileView.LoadString(IDS_FILE_VIEW);
	ASSERT(bNameValid);
	if (!m_wndFileView.Create(strFileView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_FILEVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT| CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create File View window\n");
		return FALSE; // failed to create
	}

    CString strGlobalView = L"Global Parameters";
    //bNameValid = strGlobalView.LoadString(IDS_CLASS_VIEW);
    //ASSERT(bNameValid);
    if (!m_wndGlobalView.Create(strGlobalView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_GLOBALVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
    {
        TRACE0("Failed to create Global View window\n");
        return FALSE; // failed to create
    }

	// Create properties window
	CString strPropertiesWnd;
	bNameValid = strPropertiesWnd.LoadString(IDS_PROPERTIES_WND);
	ASSERT(bNameValid);
	if (!m_wndProperties.Create(strPropertiesWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_PROPERTIESWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Properties window\n");
		return FALSE; // failed to create
	}

	//// Create search view
	CString strSearchView;
    bNameValid = strSearchView.LoadString(IDS_SEARCH_VIEW);
	ASSERT(bNameValid);
    if (!m_wndSearchView.Create(strSearchView, this, CRect(0, 0, 200, 200), TRUE, IDD_DLG_SEARCH, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Search View window\n");
		return FALSE; // failed to create
	}

	//// Create search result view
	CString strSearchResultView = L"Search Result";
 //   bNameValid = strSearchView.LoadString(IDS_SEARCH_VIEW);
	//ASSERT(bNameValid);
    if (!m_wndSearchResult.Create(strSearchResultView, this, CRect(0, 0, 200, 200), TRUE, IDD_DLG_SEARCH_RESULT, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Search View window\n");
		return FALSE; // failed to create
	}

	SetDockingWindowIcons(theApp.m_bHiColorIcons);
	return TRUE;
}

void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
	HICON hFileViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndFileView.SetIcon(hFileViewIcon, FALSE);

	HICON hClassViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndClassView.SetIcon(hClassViewIcon, FALSE);

	HICON hGlobalViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_GLOBAL_VIEW_HC : IDI_GLOBAL_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndGlobalView.SetIcon(hGlobalViewIcon, FALSE);

	HICON hPropertiesBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

	//HICON hSearchViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndSearchView.SetIcon(hClassViewIcon, FALSE);

}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnApplicationLook(UINT id)
{
	CWaitCursor wait;

	theApp.m_nAppLook = id;

	switch (theApp.m_nAppLook)
	{
	case ID_VIEW_APPLOOK_WIN_2000:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_OFF_XP:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_WIN_XP:
		CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_OFF_2003:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_VS_2005:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_VS_2008:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_WINDOWS_7:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(TRUE);
		break;

	default:
		switch (theApp.m_nAppLook)
		{
		case ID_VIEW_APPLOOK_OFF_2007_BLUE:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_BLACK:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_SILVER:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_AQUA:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
			break;
		}

		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
	}

	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

	theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}


void CMainFrame::OnFilePrint()
{
	if (IsPrintPreview())
	{
		PostMessage(WM_COMMAND, AFX_ID_PREVIEW_PRINT);
	}
}

void CMainFrame::OnFilePrintPreview()
{
	if (IsPrintPreview())
	{
		PostMessage(WM_COMMAND, AFX_ID_PREVIEW_CLOSE);  // force Print Preview mode closed
	}
}

void CMainFrame::OnUpdateFilePrintPreview(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(IsPrintPreview());
}

bool CMainFrame::InitializeRibbon()
{

    return true;
}

void CMainFrame::OnViewClassView()
{
    // Show or activate the pane, depending on current state.  The
    // pane can only be closed via the [x] button on the pane frame.
    if (m_wndClassView.IsVisible())
        m_wndClassView.ShowPane(FALSE, FALSE, TRUE);
    else
    {
        m_wndClassView.ShowPane(TRUE, FALSE, TRUE);
        m_wndClassView.SetFocus();
    }
}

void CMainFrame::OnUpdateViewClassView(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_wndClassView.IsVisible());
}

void CMainFrame::OnViewGlobalView()
{
    // Show or activate the pane, depending on current state.  The
    // pane can only be closed via the [x] button on the pane frame.
    if (m_wndGlobalView.IsVisible())
        m_wndGlobalView.ShowPane(FALSE, FALSE, TRUE);
    else
    {
        m_wndGlobalView.ShowPane(TRUE, FALSE, TRUE);
        m_wndGlobalView.SetFocus();
    }
}

void CMainFrame::OnUpdateViewGlobalView(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_wndGlobalView.IsVisible());
}

void CMainFrame::OnViewFileView()
{
    // Show or activate the pane, depending on current state.  The
    // pane can only be closed via the [x] button on the pane frame.
    if (m_wndFileView.IsVisible())
        m_wndFileView.ShowPane(FALSE, FALSE, TRUE);
    else
    {
        m_wndFileView.ShowPane(TRUE, FALSE, TRUE);
        m_wndFileView.SetFocus();
    }
}

void CMainFrame::OnUpdateViewFileView(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_wndFileView.IsVisible());
}

void CMainFrame::OnViewSearchView()
{
    // Show or activate the pane, depending on current state.  The
    // pane can only be closed via the [x] button on the pane frame.
    if (m_wndSearchView.IsVisible())
        m_wndSearchView.ShowPane(FALSE, FALSE, TRUE);
    else
    {
        m_wndSearchView.ShowPane(TRUE, FALSE, TRUE);
        m_wndSearchView.SetFocus();
    }
}

void CMainFrame::OnUpdateViewSearchView(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_wndSearchView.IsVisible());
}

void CMainFrame::OnViewSearchResult()
{
    // Show or activate the pane, depending on current state.  The
    // pane can only be closed via the [x] button on the pane frame.
    if (m_wndSearchResult.IsVisible())
        m_wndSearchResult.ShowPane(FALSE, FALSE, TRUE);
    else
    {
        m_wndSearchResult.ShowPane(TRUE, FALSE, TRUE);
        m_wndSearchResult.SetFocus();
    }
}

void CMainFrame::OnUpdateViewSearchResult(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_wndSearchResult.IsVisible());
}

void CMainFrame::OnViewPropertiesWindow()
{
    // Show or activate the pane, depending on current state.  The
    // pane can only be closed via the [x] button on the pane frame.
    if (m_wndProperties.IsVisible())
        m_wndProperties.ShowPane(FALSE, FALSE, TRUE);
    else
    {
        m_wndProperties.ShowPane(TRUE, FALSE, TRUE);
        m_wndProperties.SetFocus();
    }
}

void CMainFrame::OnUpdateViewPropertiesWindow(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck(m_wndProperties.IsVisible());
}


LRESULT CMainFrame::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    // TODO: Add your specialized code here and/or call the base class
    switch (message)
    {
        case WM_LOAD_FILE_DONE:
        {
            m_wndProperties.FillProperties(nullptr);
            m_wndFileView.FillFileView(nullptr);

            m_pEgfFile = (CEgfFile*)lParam;
            ((CEgfViewerView*)GetActiveView())->CalculateRatio();
            m_wndGlobalView.FillView(m_pEgfFile);
            m_wndClassView.FillClassView(m_pEgfFile->m_pChild);
        }
        break;
        case WM_SELECTED_OBJECT_CHANGED:
        {
            CEgfObj *pObj = (CEgfObj*)lParam;
            m_wndProperties.FillProperties(pObj);
            if (wParam == 0) // notification comes from CClassView, fill the CFileView
                m_wndFileView.FillFileView(pObj);

            ///////////////
            ((CEgfViewerView*)GetActiveView())->ShowEgfObject(pObj);
        }
        break;
        case WM_SEARCH_EGF_OBJ:
        {
            CSearchCriteria *pPairs = (CSearchCriteria*)lParam;
            int objMetaId = (int)wParam;
            CVector<CEgfObj*> objList;
            if (m_pEgfFile)
                m_pEgfFile->Search(objMetaId, pPairs, &objList);

            m_wndSearchResult.ShowList(&objList);

            delete pPairs;
            objList.clear();
        }
        break;
        case WM_SEARCHED_OBJ_SELECTED:
        {
            CEgfObj *pObj = (CEgfObj*)lParam;
            m_wndClassView.SelectObject(pObj);
        }
        break;
    }

    return CFrameWndEx::DefWindowProc(message, wParam, lParam);
}

void CMainFrame::OnFileImscoreTest()
{
    if (m_imscoreDlg.DoModal() == IDOK)
    {
        CString source = m_imscoreDlg.m_csSource;
        CString target = m_imscoreDlg.m_csTarget;
        if (source.IsEmpty() || target.IsEmpty())
        {
            AfxMessageBox(_T("Source File or target directory is empty!"));
            return;
        }

        // execute imscore_test
        wchar_t buf[1024];
        if (m_imscoreDlg.m_nStartPage == m_imscoreDlg.m_nEndPage)
            wsprintf(buf, _T("%simagenation\\imscore_test.exe -s\"%s\" -t\"%s\" -f7,1 -p%d"), m_csExePath, source.GetBuffer(), target.GetBuffer(), m_imscoreDlg.m_nStartPage);
        else if (m_imscoreDlg.m_nStartPage == 1 && m_imscoreDlg.m_nEndPage == 0)
            wsprintf(buf, _T("%simagenation\\imscore_test.exe -s\"%s\" -t\"%s\" -f7,1 "), m_csExePath, source.GetBuffer(), target.GetBuffer());
        else
            wsprintf(buf, _T("%simagenation\\imscore_test.exe -s\"%s\" -t\"%s\" -f7,1 -p%d-%d"), m_csExePath, source.GetBuffer(), target.GetBuffer(), m_imscoreDlg.m_nStartPage, m_imscoreDlg.m_nEndPage);

        char buf_target[2048];
        size_t res;
        wcstombs_s(&res, buf_target, 2048, buf, wcslen(buf));
        int result = system(buf_target);
        if (result == 0)
            AfxMessageBox(_T("Execution of imscore_test finished!"));
    }
}


void CMainFrame::OnUpdateFileImscoreTest(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(TRUE);
}

void CMainFrame::RefreshView()
{
    CEgfViewerView *pView = ((CEgfViewerView*)GetActiveView());
    CEgfViewerDoc *pDoc = pView->GetDocument();
    pDoc->GetPage(pDoc->nPageNo);
    pView->Invalidate();
}

void CMainFrame::ReconvertEgf2Uv(CEgfObj *pEgfObj)
{
    CEgfViewerView *pView = ((CEgfViewerView*)GetActiveView());
    CEgfViewerDoc *pDoc = pView->GetDocument();

    m_wndClassView.SetSelectObject(pEgfObj);

    if (pDoc->m_pUvPage) delete pDoc->m_pUvPage;
    pDoc->m_pUvPage = nullptr;
    pDoc->GetPage(pDoc->nPageNo);
    pView->Invalidate();
}

void CMainFrame::OnCheckHomeApplyClip()
{
    m_bApplyClip = !m_bApplyClip;
    RefreshView();
}


void CMainFrame::OnUpdateCheckHomeApplyClip(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_bApplyClip);
}
