#include "stdafx.h"
#include "EgfText.h"
#include "Global.h"


CEgfText::CEgfText() : m_pText(0), mVisible(0), mMask(0)
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfText::CEgfText(int objType, int objDataType, int objSize)
    : m_pText(0), mVisible(0), mMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}

CEgfText::~CEgfText()
{
    if (m_pText)
        delete[] m_pText;
}

bool CEgfText::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mColor
    ifs.read(mColor, 3);

    // mTypeFace
    ifs.read(buf, 4);
    mTypeFace = Egf32_ToInt(buf);

    // mFont
    ifs.read(mFont, 81);

    // mTextWeightUnits
    ifs.read(buf, 4);
    mTextWeightUnits = Egf32_ToInt(buf);
    // mTextWeightValue
    ifs.read(buf, 4);
    mTextWeightValue = Egf32_ToInt(buf);

    // mHeightUnits
    ifs.read(buf, 4);
    mHeightUnits = Egf32_ToInt(buf);
    // mHeightValue
    ifs.read(buf, 4);
    mHeightValue = Egf32_ToInt(buf);

    // mWidthUnits
    ifs.read(buf, 4);
    mWidthUnits = Egf32_ToInt(buf);
    // mWidthValue
    ifs.read(buf, 4);
    mWidthValue = Egf32_ToInt(buf);

    // mRotation
    ifs.read(buf, 4);
    mRotation = Egf32_ToInt(buf);

    // mJustification
    ifs.read(buf, 4);
    mJustification = Egf32_ToInt(buf);

    // mMirror
    ifs.read(buf, 4);
    mMirror = Egf32_ToInt(buf);

    // mFillStyle
    ifs.read(buf, 4);
    mFillStyle = Egf32_ToInt(buf);

    // mLineStyle
    ifs.read(buf, 4);
    mLineStyle = Egf32_ToInt(buf);

    // mHatchStyle
    ifs.read(buf, 4);
    mHatchStyle = Egf32_ToInt(buf);

    // mFillColor
    ifs.read(mFillColor, 3);

    // mOrientation
    ifs.read(buf, 4);
    mOrientation = Egf32_ToInt(buf);

    // mCharset
    ifs.read(buf, 4);
    mCharset = Egf32_ToInt(buf);

    // mPenNum
    ifs.read(buf, 4);
    mPenNum = Egf32_ToInt(buf);

    // mer
    ReadMer(ifs, mMer);

    // mDropPoint
    ReadPoints(ifs, 1, &mDropPoint);

    // mTextLength
    ifs.read(buf, 4);
    mTextLength = Egf32_ToInt(buf);

    // m_pText
    if (mTextLength > 0)
    {
        m_pText = new char[mTextLength + 2];
        ifs.read(m_pText, mTextLength);
        m_pText[mTextLength] = 0;
        m_pText[mTextLength + 1] = 0;
    }

    return true;
}

void CEgfText::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_TEXT_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case TEXT_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case TEXT_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case TEXT_ARGS_ID_mColor:
            met = CheckColorCondition(pPairs->pairs[i], mColor);
            break;
        case TEXT_ARGS_ID_mTypeFace:
            met = CheckCondition(pPairs->pairs[i], &mTypeFace);
            break;
        case TEXT_ARGS_ID_mFont:
            met = CheckCondition(pPairs->pairs[i], &mFont);
            break;
        case TEXT_ARGS_ID_mTextWeightUnits:
            met = CheckCondition(pPairs->pairs[i], &mTextWeightUnits);
            break;
        case TEXT_ARGS_ID_mTextWeightValue:
            met = CheckCondition(pPairs->pairs[i], &mTextWeightValue);
            break;
        case TEXT_ARGS_ID_mHeightUnits:
            met = CheckCondition(pPairs->pairs[i], &mHeightUnits);
            break;
        case TEXT_ARGS_ID_mHeightValue:
            met = CheckCondition(pPairs->pairs[i], &mHeightValue);
            break;
        case TEXT_ARGS_ID_mWidthUnits:
            met = CheckCondition(pPairs->pairs[i], &mWidthUnits);
            break;
        case TEXT_ARGS_ID_mWidthValue:
            met = CheckCondition(pPairs->pairs[i], &mWidthValue);
            break;
        case TEXT_ARGS_ID_mRotation:
            met = CheckCondition(pPairs->pairs[i], &mRotation);
            break;
        case TEXT_ARGS_ID_mJustification:
            met = CheckCondition(pPairs->pairs[i], &mJustification);
            break;
        case TEXT_ARGS_ID_mMirror:
            met = CheckCondition(pPairs->pairs[i], &mMirror);
            break;
        case TEXT_ARGS_ID_mFillStyle:
            met = CheckCondition(pPairs->pairs[i], &mFillStyle);
            break;
        case TEXT_ARGS_ID_mLineStyle:
            met = CheckCondition(pPairs->pairs[i], &mLineStyle);
            break;
        case TEXT_ARGS_ID_mHatchStyle:
            met = CheckCondition(pPairs->pairs[i], &mHatchStyle);
            break;
        case TEXT_ARGS_ID_mFillColor:
            met = CheckColorCondition(pPairs->pairs[i], mFillColor);
            break;
        case TEXT_ARGS_ID_mOrientation:
            met = CheckCondition(pPairs->pairs[i], &mOrientation);
            break;
        case TEXT_ARGS_ID_mCharset:
            met = CheckCondition(pPairs->pairs[i], &mCharset);
            break;
        case TEXT_ARGS_ID_mMer_x1:
            met = CheckCondition(pPairs->pairs[i], &mMer.x1);
            break;
        case TEXT_ARGS_ID_mMer_y1:
            met = CheckCondition(pPairs->pairs[i], &mMer.y1);
            break;
        case TEXT_ARGS_ID_mMer_x2:
            met = CheckCondition(pPairs->pairs[i], &mMer.x2);
            break;
        case TEXT_ARGS_ID_mMer_y2:
            met = CheckCondition(pPairs->pairs[i], &mMer.y2);
            break;
        case TEXT_ARGS_ID_mDropPoint_x:
            met = CheckCondition(pPairs->pairs[i], &mDropPoint.x);
            break;
        case TEXT_ARGS_ID_mDropPoint_y:
            met = CheckCondition(pPairs->pairs[i], &mDropPoint.y);
            break;
        case TEXT_ARGS_ID_mTextLength:
            met = CheckCondition(pPairs->pairs[i], &mTextLength);
            break;
        case TEXT_ARGS_ID_m_pText:
            met = CheckCondition(pPairs->pairs[i], m_pText);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}