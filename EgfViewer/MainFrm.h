// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

// MainFrm.h : interface of the CMainFrame class
//

#pragma once
#include "FileView.h"
#include "ClassView.h"
#include "GlobalView.h"
#include "PropertiesWnd.h"
#include "SearchView.h"
#include "SearchResult.h"
#include "Global.h"
#include "EgfFile.h"
#include "EgfObj.h"
#include "ImscoreDlg.h"

class CMainFrame : public CFrameWndEx
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
    CEgfFile *m_pEgfFile;
    bool m_bApplyClip;

// Operations
public:
    bool InitializeRibbon();
    void ReconvertEgf2Uv(CEgfObj *pEgfObj);
    void RefreshView();

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CMFCRibbonBar     m_wndRibbonBar;
	CMFCRibbonApplicationButton m_MainButton;
	CMFCToolBarImages m_PanelImages;
	CMFCRibbonStatusBar  m_wndStatusBar;
	CFileView         m_wndFileView;
    CClassView        m_wndClassView;
    CGlobalView        m_wndGlobalView;
    CPropertiesWnd    m_wndProperties;
    CSearchView       m_wndSearchView;
    CSearchResult     m_wndSearchResult;

    CImscoreDlg       m_imscoreDlg;
    CString           m_csExePath;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnApplicationLook(UINT id);
	afx_msg void OnUpdateApplicationLook(CCmdUI* pCmdUI);
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
    afx_msg void OnViewClassView();
    afx_msg void OnUpdateViewClassView(CCmdUI* pCmdUI);
    afx_msg void OnViewGlobalView();
    afx_msg void OnUpdateViewGlobalView(CCmdUI* pCmdUI);
    afx_msg void OnViewFileView();
    afx_msg void OnUpdateViewFileView(CCmdUI* pCmdUI);
    afx_msg void OnViewPropertiesWindow();
    afx_msg void OnUpdateViewPropertiesWindow(CCmdUI* pCmdUI);
    afx_msg void OnViewSearchView();
    afx_msg void OnUpdateViewSearchView(CCmdUI* pCmdUI);
    afx_msg void OnViewSearchResult();
    afx_msg void OnUpdateViewSearchResult(CCmdUI* pCmdUI);
    DECLARE_MESSAGE_MAP()

	BOOL CreateDockingWindows();
	void SetDockingWindowIcons(BOOL bHiColorIcons);
public:

    virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
    afx_msg void OnFileImscoreTest();
    afx_msg void OnUpdateFileImscoreTest(CCmdUI *pCmdUI);
    afx_msg void OnCheckHomeApplyClip();
    afx_msg void OnUpdateCheckHomeApplyClip(CCmdUI *pCmdUI);
};


