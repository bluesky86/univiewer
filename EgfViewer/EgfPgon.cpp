#include "stdafx.h"
#include "EgfPgon.h"
#include "Global.h"


CEgfPgon::CEgfPgon() : m_pGradientPoints(0), mVisible(0), mMask(0), m_pSubPgonNumPoints(0), m_pPoints(0)
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfPgon::CEgfPgon(int objType, int objDataType, int objSize)
    : m_pGradientPoints(0), mVisible(0), mMask(0), m_pSubPgonNumPoints(0), m_pPoints(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}

CEgfPgon::~CEgfPgon()
{
    if (m_pGradientPoints)
        delete[] m_pGradientPoints;

    if (m_pSubPgonNumPoints)
        delete[] m_pSubPgonNumPoints;

    if (m_pPoints)
        delete[] m_pPoints;
}

bool CEgfPgon::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mThinknessUnits
    ifs.read(buf, 4);
    mThinknessUnits = Egf32_ToInt(buf);
    // mThinknessValue
    ifs.read(buf, 4);
    mThinknessValue = Egf32_ToInt(buf);

    // mLineColor
    ifs.read(mLineColor, 3);

    // mFillColor
    ifs.read(mFillColor, 3);

    // mLineStyle
    ifs.read(buf, 4);
    mLineStyle = Egf32_ToInt(buf);

    // mFillStyle
    ifs.read(buf, 4);
    mFillStyle = Egf32_ToInt(buf);

    // mJoinStyle
    ifs.read(buf, 4);
    mJoinStyle = Egf32_ToInt(buf);

    // mPolyfillMode
    ifs.read(buf, 4);
    mPolyfillMode = Egf32_ToInt(buf);

    // mClosed
    ifs.read(buf, 4);
    mClosed = Egf32_ToInt(buf);

    // mCurveStyle
    ifs.read(buf, 4);
    mCurveStyle = Egf32_ToInt(buf);

    // mHatchStyle
    ifs.read(buf, 4);
    mHatchStyle = Egf32_ToInt(buf);

    // mhRasterFillIndex
    ifs.read(buf, 4);
    mhRasterFillIndex = Egf32_ToInt(buf);

    // mRasterFillStyle
    ifs.read(buf, 4);
    mRasterFillStyle = Egf32_ToInt(buf);

    // mPenNum
    ifs.read(buf, 4);
    mPenNum = Egf32_ToInt(buf);

    // mShadingStyle
    ifs.read(buf, 4);
    mShadingStyle = Egf32_ToInt(buf);

    // fillObjType
    ifs.read(buf, 4);
    mFillObjType = Egf32_ToInt(buf);

    // mMer
    ReadMer(ifs, mMer);

    // mNumSubPgons
    ifs.read(buf, 4);
    mNumSubPgons = Egf32_ToInt(buf);

    // m_pSubPgonNumPoints
    if (mNumSubPgons > 0)
    {
        m_pSubPgonNumPoints = new int[mNumSubPgons];
        Read32BitArray(ifs, mNumSubPgons, m_pSubPgonNumPoints);
    }

    // mNumPoints
    ifs.read(buf, 4);
    mNumPoints = Egf32_ToInt(buf);

    // points
    if (mNumPoints > 0)
    {
        m_pPoints = new CEgfPoint[mNumPoints];
        ReadPoints(ifs, mNumPoints, m_pPoints);
    }

    // mGradientIndex
    ifs.read(buf, 4);
    mGradientIndex = Egf32_ToInt(buf);

    // mGradientIndex2
    ifs.read(buf, 4);
    mGradientIndex2 = Egf32_ToInt(buf);

    // mGradientNumPoints
    ifs.read(buf, 4);
    mGradientNumPoints = Egf32_ToInt(buf);

    // gradient points
    if (mGradientNumPoints > 0)
    {
        m_pGradientPoints = new CEgfPoint[mGradientNumPoints];
        ReadPoints(ifs, mGradientNumPoints, m_pGradientPoints);
    }


    return true;
}

void CEgfPgon::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_PGON_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case POLYGON_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case POLYGON_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case POLYGON_ARGS_ID_mThinknessUnits:
            met = CheckCondition(pPairs->pairs[i], &mThinknessUnits);
            break;
        case POLYGON_ARGS_ID_mThinknessValue:
            met = CheckCondition(pPairs->pairs[i], &mThinknessValue);
            break;
        case POLYGON_ARGS_ID_mLineColor:
            met = CheckColorCondition(pPairs->pairs[i], mLineColor);
            break;
        case POLYGON_ARGS_ID_mFillColor:
            met = CheckColorCondition(pPairs->pairs[i], mFillColor);
            break;
        case POLYGON_ARGS_ID_mLineStyle:
            met = CheckCondition(pPairs->pairs[i], &mLineStyle);
            break;
        case POLYGON_ARGS_ID_mFillStyle:
            met = CheckCondition(pPairs->pairs[i], &mFillStyle);
            break;
        case POLYGON_ARGS_ID_mJoinStyle:
            met = CheckCondition(pPairs->pairs[i], &mJoinStyle);
            break;
        case POLYGON_ARGS_ID_mPolyfillMode:
            met = CheckCondition(pPairs->pairs[i], &mPolyfillMode);
            break;
        case POLYGON_ARGS_ID_mClosed:
            met = CheckCondition(pPairs->pairs[i], &mClosed);
            break;
        case POLYGON_ARGS_ID_mCurveStyle:
            met = CheckCondition(pPairs->pairs[i], &mCurveStyle);
            break;
        case POLYGON_ARGS_ID_mHatchStyle:
            met = CheckCondition(pPairs->pairs[i], &mHatchStyle);
            break;
        case POLYGON_ARGS_ID_mhRasterFillIndex:
            met = CheckCondition(pPairs->pairs[i], &mhRasterFillIndex);
            break;
        case POLYGON_ARGS_ID_mRasterFillStyle:
            met = CheckCondition(pPairs->pairs[i], &mRasterFillStyle);
            break;
        case POLYGON_ARGS_ID_mShadingStyle:
            met = CheckCondition(pPairs->pairs[i], &mShadingStyle);
            break;
        case POLYGON_ARGS_ID_mFillObjType:
            met = CheckCondition(pPairs->pairs[i], &mFillObjType);
            break;
        case POLYGON_ARGS_ID_mMer_x1:
            met = CheckCondition(pPairs->pairs[i], &mMer.x1);
            break;
        case POLYGON_ARGS_ID_mMer_y1:
            met = CheckCondition(pPairs->pairs[i], &mMer.y1);
            break;
        case POLYGON_ARGS_ID_mMer_x2:
            met = CheckCondition(pPairs->pairs[i], &mMer.x2);
            break;
        case POLYGON_ARGS_ID_mMer_y2:
            met = CheckCondition(pPairs->pairs[i], &mMer.y2);
            break;
        case POLYGON_ARGS_ID_mNumSubPgons:
            met = CheckCondition(pPairs->pairs[i], &mNumSubPgons);
            break;
        case POLYGON_ARGS_ID_mNumPoints:
            met = CheckCondition(pPairs->pairs[i], &mNumPoints);
            break;
        case POLYGON_ARGS_ID_mGradientIndex:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex);
            break;
        case POLYGON_ARGS_ID_mGradientIndex2:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex2);
            break;
        case POLYGON_ARGS_ID_mGradientNumPoints:
            met = CheckCondition(pPairs->pairs[i], &mGradientNumPoints);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}
