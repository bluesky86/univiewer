#include "stdafx.h"
#include "EgfObject.h"

CEgfMetaList gEgfMetaList;
CEgfSearchMetaList gEgfSearchMetaList;
std::map<int, int> gEgfIdToIndexMap;

CEgfMeta::CEgfMeta()
    :id(0), egfId(0), kidsMode(KIDS_MODE_NONE), name(0), hasOffset(0), hasDisplayHex(0)
{
}

CEgfMeta::~CEgfMeta()
{
    if (name)
        delete[] name;

    for (int i = 0; i < elements.size(); i++)
        delete elements[i];

    elements.clear();

    displayOrderMap.clear();
}

CEgfSearchMeta::CEgfSearchMeta()
    :id(0)
{
}

CEgfSearchMeta::~CEgfSearchMeta()
{
    for (int i = 0; i < elements.size(); i++)
        delete elements[i];

    elements.clear();
}

CEgfObjAttr::CEgfObjAttr()
    :type(EGF_ELE_META_TYPE_none)
{
}

CEgfObjAttr::~CEgfObjAttr()
{
    switch (type)
    {
    case EGF_ELE_META_TYPE_intArray:
        delete[] ia;
        break;
    case EGF_ELE_META_TYPE_string:
        delete[] s;
        break;
    case EGF_ELE_META_TYPE_point:
        delete point;
        break;
    case EGF_ELE_META_TYPE_points:
        delete points;
        break;
    case EGF_ELE_META_TYPE_mer:
        delete mer;
        break;
    }
}

CEgfObj::CEgfObj()
    :metaId(-1), mParent(0)
{
}

CEgfObj::CEgfObj(int id)
    : metaId(id), mParent(0)
{
}


CEgfObj::~CEgfObj()
{
    for (int i = 0; i < attr.size(); i++)
        delete attr[i];

    attr.clear();

    for (int i = 0; i < children.size(); i++)
        delete children[i];

    children.clear();

}

int CEgfObj::M16_ToInt(char *data)
{
    char outdata[2];
    outdata[0] = *(data + 1);
    outdata[1] = *(data);

    return (int)(*(short*)outdata);
}

int CEgfObj::M32_ToInt(char *data)
{
    char outdata[4];
    outdata[0] = *(data + 3);
    outdata[1] = *(data + 2);
    outdata[2] = *(data + 1);
    outdata[3] = *(data);

    return *(int*)outdata;
}

int CEgfObj::Egf32_ToInt(char *data)
{
    return *(int*)data;
}

bool CEgfObj::ReadObjectFlags(std::ifstream &ifs, bool &visible, bool &mask)
{
    char buf;
    if (!ReadByte(ifs, buf)) return false;
    visible = buf > 0;
    if (!ReadByte(ifs, buf)) return false;
    mask = buf > 0;
    return true;
}

bool CEgfObj::Read16BitArray(std::ifstream &ifs, int count, short *value)
{
    char buf[2];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 2);
        if (ifs.gcount() != 2)
            return false;
        value[i] = *(short*)buf;
    }

    return true;
}

bool CEgfObj::Read32BitArray(std::ifstream &ifs, int count, int *value)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        value[i] = *(int*)buf;
    }

    return true;
}

bool CEgfObj::ReadMer(std::ifstream &ifs, CEgfMer &mer)
{
    int value[4];
    if (!Read32BitArray(ifs, 4, value)) return false;
    mer.x1 = (float)value[0];
    mer.y1 = (float)value[1];
    mer.x2 = (float)value[2];
    mer.y2 = (float)value[3];

    return true;
}

bool CEgfObj::ReadPoints(std::ifstream &ifs, int count, CEgfPoint *pts)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].x = (float)*(int*)buf;
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].y = (float)*(int*)buf;
    }
    return true;
}

bool CEgfObj::ReadByte(std::ifstream &ifs, char &value)
{
    char buf[1];
    ifs.read(buf, 1);
    if (ifs.gcount() != 1)
        return false;
    value = (BYTE)buf[0];
    return true;
}

bool CEgfObj::ReadShort(std::ifstream &ifs, short &value)
{
    char buf[2];
    ifs.read(buf, 2);
    if (ifs.gcount() != 2)
        return false;
    value = *(short*)buf;
    return true;
}

bool CEgfObj::ReadInt(std::ifstream &ifs, int &value)
{
    char buf[4];
    ifs.read(buf, 4);
    if (ifs.gcount() != 4)
        return false;
    value = *(int*)buf;
    return true;
}

bool CEgfObj::ReadString(std::ifstream &ifs, int count, char *buf)
{
    ifs.read(buf, count);
    if (ifs.gcount() != count)
        return false;
    return true;
}

bool CEgfObj::LoadObjHdr(std::ifstream &ifs, int &objType, int &objDataType, int &objSize)
{
    // offset (100) - type
    if (!ReadInt(ifs, objType)) return false;

    // objDataType
    if (!ReadInt(ifs, objDataType)) return false;

    // Object size
    if (!ReadInt(ifs, objSize)) return false;

    return true;
}

wchar_t* CEgfObj::Get_EgfObjName(int objType)
{
    switch (objType)
    {
    case EGF_GROUP_TAG:
        return gEGF_Object_Name[0];
    case EGF_LINE_TAG:
        return gEGF_Object_Name[1];
    case EGF_ARC_TAG:
        return gEGF_Object_Name[2];
    case EGF_ELIPS_TAG:
        return gEGF_Object_Name[3];
    case EGF_ARROW_TAG:
        return gEGF_Object_Name[4];
    case EGF_PLINE_TAG:
        return gEGF_Object_Name[5];
    case EGF_PGON_TAG:
    case EGF_BOX_TAG:
        return gEGF_Object_Name[6];
    case EGF_PASTE_TAG:
        return gEGF_Object_Name[7];
    case EGF_TEXT_TAG:
        return gEGF_Object_Name[8];
    case EGF_ANNO_TAG:
        return gEGF_Object_Name[9];
    case EGF_PATH_TAG:
        return gEGF_Object_Name[10];
    case EGF_STEXT_TAG:
        return gEGF_Object_Name[11];
    case OBJTYPE_TEXT:
        return gEGF_Object_Name[12];
    case OBJTYPE_FMTPAGE:
        return gEGF_Object_Name[13];
    case OBJTYPE_SHAPEOBJ:
        return gEGF_Object_Name[14];
    case OBJTYPE_ANNOTATION:
        return gEGF_Object_Name[15];
    case OBJTYPE_BOOKMARK:
        return gEGF_Object_Name[16];
    case OBJTYPE_BLOCK:
        return gEGF_Object_Name[17];
    case OBJTYPE_SPCCHAR:
        return gEGF_Object_Name[18];
    case OBJTYPE_TAB:
        return gEGF_Object_Name[19];
    case OBJTYPE_PAGE:
        return gEGF_Object_Name[20];
    case OBJTYPE_BACKGROUND:
        return gEGF_Object_Name[21];
    case OBJTYPE_PGBLOCK:
        return gEGF_Object_Name[22];
    case OBJTYPE_PICTURE:
        return gEGF_Object_Name[23];
    case OBJTYPE_ROW:
        return gEGF_Object_Name[24];
    case OBJTYPE_DRAWOBJ:
        return gEGF_Object_Name[25];
    case OBJTYPE_FORMFIELD:
        return gEGF_Object_Name[26];
    case OBJTYPE_FRAME:
        return gEGF_Object_Name[27];
    case OBJTYPE_FOOTNOTE:
        return gEGF_Object_Name[28];
    default:
    {
        assert(false);
    }
    break;
    }

    return NULL;
}

wchar_t* CEgfObj::Get_EgfObjName(EGF_OBJ_TYPE *objType)
{
    return Get_EgfObjName((int)*objType);
}

bool CEgfObj::CheckCondition(CSearchEle *pCondition, void *pValue)
{
    if (pCondition == NULL) return false;

    switch (pCondition->valueType)
    {
    case EGFSVT_bool:
        if (pCondition->b == (*(bool*)pValue))
            return true;
        break;
    case EGFSVT_int:
        if (pCondition->i == (*(int*)pValue))
            return true;
        break;
    case EGFSVT_uint:
        if (pCondition->u == (*(unsigned int*)pValue))
            return true;
        break;
    case EGFSVT_float:
        if (pCondition->f == (*(float*)pValue))
            return true;
        break;
    case EGFSVT_string:
        if (CString(pCondition->s) == CString((*(wchar_t*)pValue)))
            return true;
        break;
    default:
        return false;
    }


    return false;
}

bool CEgfObj::CheckColorCondition(CSearchEle *pCondition, char *pValue)
{
    if (pCondition->valueType != EGFSVT_uint) return false;

    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000) >> 16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF));
}

bool CEgfObj::CheckRGBAColorCondition(CSearchEle *pCondition, short *pValue)
{
    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000) >> 16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF)
        && (BYTE)pValue[3] == ((pCondition->u & 0xFF000000) >> 24));
}

CEgfObj* CEgfObj::CreateEgfObj(int &objType, int &objDataType, int &objSize)
{
    CEgfObj *pObj = new CEgfObj(gEgfIdToIndexMap[objType]);
    pObj->mObjType = objType;
    pObj->mObjDataType = objDataType;
    pObj->mObjSize = objSize;

    return pObj;
}

int CEgfObj::GetAttrNumOfEles(int column)
{
    CEgfEleMeta *pMeta = gEgfMetaList[metaId]->elements[column];
    int num = 0;
    if (pMeta->num_id != -1)
        num = attr[pMeta->num_id]->i;
    else
        num = pMeta->num;

    return num;
}

std::map<int, int>* CEgfObj::GetAttrDisplayOrderMap()
{
    return &gEgfMetaList[metaId]->displayOrderMap;
}

bool CEgfObj::ParseAttrs(std::ifstream &ifs)
{
    char buf[4];

    CEgfEleMeta * pMeta = NULL;
    for (int i = 0; i < gEgfMetaList[metaId]->elements.size(); i++)
    {
        pMeta = gEgfMetaList[metaId]->elements[i];

        CEgfObjAttr *pAttr = new CEgfObjAttr();

        attr.push_back(pAttr);

        pAttr->type = pMeta->type;

        switch (pMeta->type)
        {
        case EGF_ELE_META_TYPE_int:
        case EGF_ELE_META_TYPE_lineStyle:
        case EGF_ELE_META_TYPE_capStyle:
        case EGF_ELE_META_TYPE_operation:
        case EGF_ELE_META_TYPE_fillStyle:
        case EGF_ELE_META_TYPE_hatchStyle:
        case EGF_ELE_META_TYPE_shadingStyle:
        case EGF_ELE_META_TYPE_joinStyle:
        case EGF_ELE_META_TYPE_curveStyle:
        case EGF_ELE_META_TYPE_effectTyle:
            ifs.read(buf, 4);
            pAttr->i = Egf32_ToInt(buf);
            //pAttr->type = pMeta->type;// EGF_ELE_META_TYPE_int;
            break;
        case EGF_ELE_META_TYPE_8bool:
            ifs.read(buf, 1);
            pAttr->b = (buf[0] > 0);
            //pAttr->type = EGF_ELE_META_TYPE_8bool;
            break;
        case EGF_ELE_META_TYPE_intBool:
            ifs.read(buf, 4);
            pAttr->b = (*(int*)buf > 0);
            //pAttr->type = EGF_ELE_META_TYPE_8bool;
            break;
        case EGF_ELE_META_TYPE_color3:
            ifs.read((char*)&pAttr->i, 3);
            //pAttr->type = EGF_ELE_META_TYPE_color3;
            break;
        case EGF_ELE_META_TYPE_point:
            pAttr->point = new CEgfPoint();
            ReadPoints(ifs, 1, pAttr->point);
            //pAttr->type = EGF_ELE_META_TYPE_point;
            break;
        case EGF_ELE_META_TYPE_points:
            {
                int num = GetAttrNumOfEles(i);

                pAttr->points = NULL;
                if (num > 0)
                {
                    pAttr->points = new CEgfPointArray();
                    for (int j = 0; j < num; j++)
                    {
                        CEgfPoint *pPoint = new CEgfPoint();
                        ReadPoints(ifs, 1, pPoint);
                        pAttr->points->points.push_back(pPoint);
                    }
                }
                //pAttr->type = EGF_ELE_META_TYPE_points;
            }
            break;
        case EGF_ELE_META_TYPE_mer:
            pAttr->mer = new CEgfMer();
            //pAttr->type = EGF_ELE_META_TYPE_mer;
            ReadMer(ifs, *pAttr->mer);
            break;
        case EGF_ELE_META_TYPE_intArray:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->ia = new int[num];
                    Read32BitArray(ifs, num, pAttr->ia);
                }
                //pAttr->type = EGF_ELE_META_TYPE_intArray;
            }
            break;
        case EGF_ELE_META_TYPE_string:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->s = new char[num + 2];
                    ifs.read(pAttr->s, num);
                    pAttr->s[num] = pAttr->s[num + 1] = 0;
                }
                //pAttr->type = EGF_ELE_META_TYPE_string;
            }
            break;
        default:
            AfxMessageBox(L"Incorrect element meta type!");
            break;
        }
    }

    return true;
}

bool CEgfObj::ParseChildren(std::ifstream &ifs, CEgfObj* pParent)
{
    int objType = 0, objDataType = 0, objSize = 0;
    int sequenceId = 0;
    bool childrenStart = false;

    // parse children
    while (true)
    {
        if (!LoadObjHdr(ifs, objType, objDataType, objSize))
            break;

        if (childrenStart == false && objDataType != SDT_OBJ_CHILDREN_START)
            return false;
        if (childrenStart == false && objDataType == SDT_OBJ_CHILDREN_START)
        {
            childrenStart = true;
            continue;
        }
        if (childrenStart == true && objDataType == SDT_OBJ_CHILDREN_END)
            break;

        CEgfObj *pObj = CEgfObj::CreateEgfObj(objType, objDataType, objSize);

        if (!pObj->Parse(ifs, sequenceId++))
        {
            delete pObj;
            return false;
        }

        pParent->children.push_back(pObj);
        pObj->mParent = pParent;
    }

    return true;
}


bool CEgfObj::Parse(std::ifstream &ifs, int sequenceId)
{
    mSequenceId = sequenceId;

    switch (gEgfMetaList[metaId]->kidsMode)
    {
    case KIDS_MODE_Group:
        {
            int objType = 0, objDataType = 0, objSize = 0;
            if (!LoadObjHdr(ifs, objType, objDataType, objSize) || objDataType != SDT_OBJ_ARGS)
                return false;

            if (!ParseAttrs(ifs))
                return false;

            // parse children
            return ParseChildren(ifs, this);
        }
        break;
    case KIDS_MODE_Path:
        if (!ParseAttrs(ifs))
            return false;
        // parse children
        return ParseChildren(ifs, this);
    case KIDS_MODE_SText:
        {
            if (!ParseAttrs(ifs))
                return false;
            if (gEgfMetaList[metaId]->hasOffset)
            {
                if (attr[attr.size() - 1]->type == EGF_ELE_META_TYPE_point)
                {
                    mOffset.x = attr[attr.size() - 1]->point->x;
                    mOffset.y = attr[attr.size() - 1]->point->y;

                    if (mObjSize == 50)
                        return true;
                }

            }
            int objType = 0, objDataType = 0, objSize = 0;
            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;

            CEgfObj *pObj = CEgfObj::CreateEgfObj(objType, objDataType, objSize);

            if (!pObj->Parse(ifs, 0))
            {
                delete pObj;
                return false;
            }

            children.push_back(pObj);
            pObj->mParent = this;

            return true;
    }
    case KIDS_MODE_Ext:
        if (mObjSize > 0)
        {
            int objType = 0, objDataType = 0, objSize = 0;
            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;

            CEgfObj *pObj = CEgfObj::CreateEgfObj(objType, objDataType, objSize);

            if (!pObj->Parse(ifs, 0))
            {
                delete pObj;
                return false;
            }

            children.push_back(pObj);
            pObj->mParent = this;

            if (objSize < mObjSize - 12) // 12 is header size
            {
                if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                    return false;

                CEgfObj *pObj1 = CEgfObj::CreateEgfObj(objType, objDataType, objSize);

                if (!pObj1->Parse(ifs, 1))
                {
                    delete pObj1;
                    return false;
                }

                children.push_back(pObj1);
                pObj1->mParent = this;
            }

            return true;
        }
        break;
    default:
        return ParseAttrs(ifs);
    }

    return true;
}

CEgfMeta* CEgfObj::GetMeta()
{
    return gEgfMetaList[metaId];
}

wchar_t* CEgfObj::GetMetaName()
{
    return gEgfMetaList[metaId]->name;
}

wchar_t* CEgfObj::GetAttrName(int column)
{
    return gEgfMetaList[metaId]->elements[column]->name;
}

EGF_ELE_META_TYPE CEgfObj::GetAttrType(int column)
{
    return gEgfMetaList[metaId]->elements[column]->type;
}

bool CEgfObj::GetDisplayHex(int column)
{
    return gEgfMetaList[metaId]->elements[column]->displayHex;
}

CEgfObjAttr* CEgfObj::FindAttr(wchar_t *name /*= NULL*/, EGF_ELE_META_TYPE type /*= EGF_ELE_META_TYPE_none*/)
{
    if (name == NULL && type == EGF_ELE_META_TYPE_none)
        return NULL;

    if (type == EGF_ELE_META_TYPE_none)
    {
        for (int i = 0; i < gEgfMetaList[metaId]->elements.size(); i++)
        {
            if (wcscmp(name, gEgfMetaList[metaId]->elements[i]->name) == 0)
                return attr[i];
        }
    }
    else
    {
        if (name == NULL)
        {
            for (int i = 0; i < gEgfMetaList[metaId]->elements.size(); i++)
            {
                if ( gEgfMetaList[metaId]->elements[i]->type == type)
                    return attr[i];
            }
        }
        else
        {
            for (int i = 0; i < gEgfMetaList[metaId]->elements.size(); i++)
            {
                if (wcscmp(name, gEgfMetaList[metaId]->elements[i]->name) == 0 && gEgfMetaList[metaId]->elements[i]->type == type)
                    return attr[i];
            }
        }
    }

    return NULL;
}

void CEgfObj::Search(int objMetaId, CSearchCriteria *pPairs, std::vector<CEgfObj*> *pList)
{
    if ( pPairs->pairs.size() == 0) return;

    CEgfMeta *pMeta = gEgfMetaList[objMetaId];

    if (objMetaId == metaId)
    {
        bool met = true;

        for (int i = 0; i < pPairs->pairs.size(); i++)
        {
            switch (attr[pPairs->pairs[i]->argId]->type)
            {
            case EGF_ELE_META_TYPE_int:
            case EGF_ELE_META_TYPE_lineStyle:
            case EGF_ELE_META_TYPE_capStyle:
            case EGF_ELE_META_TYPE_operation:
            case EGF_ELE_META_TYPE_fillStyle:
            case EGF_ELE_META_TYPE_hatchStyle:
            case EGF_ELE_META_TYPE_shadingStyle:
            case EGF_ELE_META_TYPE_joinStyle:
            case EGF_ELE_META_TYPE_curveStyle:
            case EGF_ELE_META_TYPE_effectTyle:
            case EGF_ELE_META_TYPE_effectType:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->i);
                break;
            case EGF_ELE_META_TYPE_8bool:
            case EGF_ELE_META_TYPE_intBool:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->b);
                break;
            case EGF_ELE_META_TYPE_color3:
                met = CheckColorCondition(pPairs->pairs[i], &((char*)&attr[pPairs->pairs[i]->argId]->i)[1]);
                break;
            case EGF_ELE_META_TYPE_color4:
            {
                short rgba[4];
                rgba[0] = attr[pPairs->pairs[i]->argId]->i & 0xFF;
                rgba[1] = (attr[pPairs->pairs[i]->argId]->i & 0xFF00) >> 8;
                rgba[2] = (attr[pPairs->pairs[i]->argId]->i & 0xFF00) >> 16;
                rgba[3] = (attr[pPairs->pairs[i]->argId]->i & 0xFF00) >> 24;
                met = CheckRGBAColorCondition(pPairs->pairs[i], rgba);
            }
            break;
            case EGF_ELE_META_TYPE_point:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->point->x);
                break;
            case EGF_ELE_META_TYPE_point + 1:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->point->y);
                break;
            case EGF_ELE_META_TYPE_mer:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->mer->x1);
                break;
            case EGF_ELE_META_TYPE_mer + 1:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->mer->y1);
                break;
            case EGF_ELE_META_TYPE_mer + 2:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->mer->x2);
                break;
            case EGF_ELE_META_TYPE_mer + 3:
                met = CheckCondition(pPairs->pairs[i], &attr[pPairs->pairs[i]->argId]->mer->y2);
                break;
            case EGF_ELE_META_TYPE_string:
                met = CheckCondition(pPairs->pairs[i], attr[pPairs->pairs[i]->argId]->s);
                break;
            case EGF_ELE_META_TYPE_intArray:
            case EGF_ELE_META_TYPE_points:
            case EGF_ELE_META_TYPE_none:
            default:
                break;
            }

            if (met == false)
                break;
        }

        if (met)
            pList->push_back(this);
    }

    // check children
    for (int i = 0; i < children.size(); i++)
        children[i]->Search(objMetaId, pPairs, pList);
}

CEgfFileNew::CEgfFileNew()
    :m_pChild(0)
{

}

CEgfFileNew::~CEgfFileNew()
{
    Clear();
}

void CEgfFileNew::Clear()
{
    if (m_pChild)
    {
        delete m_pChild;
        m_pChild = NULL;
    }
}

bool CEgfFileNew::Parse(std::ifstream &ifs)
{
    /////////////////////////////////////////////////////////////
    //  load object
    /////////////////////////////////////////////////////////////
    int objType = 0, objDataType = 0, objSize = 0;
    if (CEgfObj::LoadObjHdr(ifs, objType, objDataType, objSize))
    {
        //if (objType != EGF_GROUP_TAG && objDataType != SDT_OBJ_TYPE)
        //{
        //    AfxMessageBox(L"File is not correct!");
        //    return false;
        //}

        Clear();

        m_pChild = CEgfObj::CreateEgfObj(objType, objDataType, objSize);
        if (m_pChild->Parse(ifs, 0))
        {
        }
        else
        {

        }
    }
    else
    {
        AfxMessageBox(L"Failed to load file!");
        return false;
    }


    return true;
}

void CEgfFileNew::Search(int objMetaId, CSearchCriteria *pPairs, std::vector<CEgfObj*> *pList)
{
    if (m_pChild)
        m_pChild->Search(objMetaId, pPairs, pList);
}

////////////////////////////////////////////////////////////////////////////////////
int gEGF_Object_Type[] =
{
    EGF_GROUP_TAG, EGF_LINE_TAG, EGF_ARC_TAG, EGF_ELIPS_TAG, EGF_ARROW_TAG,
    EGF_PLINE_TAG, EGF_PGON_TAG, EGF_PASTE_TAG, EGF_TEXT_TAG, EGF_ANNO_TAG,
    EGF_PATH_TAG, EGF_STEXT_TAG, OBJTYPE_TEXT, 
    OBJTYPE_FMTPAGE, OBJTYPE_SHAPEOBJ, OBJTYPE_ANNOTATION, OBJTYPE_BOOKMARK, OBJTYPE_BLOCK, 
    OBJTYPE_SPCCHAR, OBJTYPE_TAB, OBJTYPE_PAGE, OBJTYPE_BACKGROUND, OBJTYPE_PGBLOCK,
    OBJTYPE_PICTURE, OBJTYPE_ROW, OBJTYPE_DRAWOBJ, OBJTYPE_FORMFIELD, OBJTYPE_FRAME,
    OBJTYPE_FOOTNOTE
};

wchar_t gEGF_Object_Name[gSupportedAllObj][16] =
{
    L"Group",       L"Line",        L"Arc",         L"Ellipse",         L"Arrow", 
    L"Polyline",    L"Polygon",     L"Paste",       L"Text",            L"Annotation",
    L"Path",        L"SText",       L"EtxText",
    L"EtxFmtPage",  L"EtxShape",    L"EtxAnnotation", L"EtxBookmark",   L"EtxBlock", 
    L"EtxSpcChar",  L"EtxTab",      L"EtxPage",       L"EtxBackground", L"EtxPgBlock",
    L"EtxPicture",  L"EtxRow",      L"EtxDraw",       L"EtxFormField",  L"ExtFrame", 
    L"EtxFootnote"
};


CEgfObject::CEgfObject()
    : mParent(0)
{
}


CEgfObject::~CEgfObject()
{
}

int CEgfObject::M16_ToInt(char *data)
{
    char outdata[2];
    outdata[0] = *(data + 1);
    outdata[1] = *(data);

    return (int)(*(short*)outdata);
}

int CEgfObject::M32_ToInt(char *data)
{
    char outdata[4];
    outdata[0] = *(data + 3);
    outdata[1] = *(data + 2);
    outdata[2] = *(data + 1);
    outdata[3] = *(data);

    return *(int*)outdata;
}

int CEgfObject::Egf32_ToInt(char *data)
{
    return *(int*)data;
}

bool CEgfObject::ReadObjectFlags(std::ifstream &ifs, bool &visible, bool &mask)
{
    char buf;
    if (!ReadByte(ifs, buf)) return false;
    visible = buf > 0;
    if (!ReadByte(ifs, buf)) return false;
    mask = buf > 0;
    return true;
}

bool CEgfObject::Read16BitArray(std::ifstream &ifs, int count, short *value)
{
    char buf[2];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 2);
        if (ifs.gcount() != 2)
            return false;
        value[i] = *(short*)buf;
    }

    return true;
}

bool CEgfObject::Read32BitArray(std::ifstream &ifs, int count, int *value)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        value[i] = *(int*)buf;
    }

    return true;
}

bool CEgfObject::ReadMer(std::ifstream &ifs, CEgfMer &mer)
{
    int value[4];
    if (!Read32BitArray(ifs, 4, value)) return false;
    mer.x1 = (float)value[0];
    mer.y1 = (float)value[1];
    mer.x2 = (float)value[2];
    mer.y2 = (float)value[3];

    return true;
}

bool CEgfObject::ReadPoints(std::ifstream &ifs, int count, CEgfPoint *pts)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].x = (float)*(int*)buf;
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].y = (float)*(int*)buf;
    }
    return true;
}

bool CEgfObject::ReadByte(std::ifstream &ifs, char &value)
{
    char buf[1];
    ifs.read(buf, 1);
    if (ifs.gcount() != 1)
        return false;
    value = (BYTE)buf[0];
    return true;
}

bool CEgfObject::ReadShort(std::ifstream &ifs, short &value)
{
    char buf[2];
    ifs.read(buf, 2);
    if (ifs.gcount() != 2)
        return false;
    value = *(short*)buf;
    return true;
}

bool CEgfObject::ReadInt(std::ifstream &ifs, int &value)
{
    char buf[4];
    ifs.read(buf, 4);
    if (ifs.gcount() != 4)
        return false;
    value = *(int*)buf;
    return true;
}

bool CEgfObject::ReadString(std::ifstream &ifs, int count, char *buf)
{
    ifs.read(buf, count);
    if (ifs.gcount() != count)
        return false;
    return true;
}

bool CEgfObject::LoadObjHdr(std::ifstream &ifs, int &objType, int &objDataType, int &objSize)
{
    // offset (100) - type
    if (!ReadInt(ifs, objType)) return false;

    // objDataType
    if (!ReadInt(ifs, objDataType)) return false;

    // Object size
    if (!ReadInt(ifs, objSize)) return false;

    return true;
}

wchar_t* CEgfObject::Get_EgfObjName(int objType)
{
    switch (objType)
    {
    case EGF_GROUP_TAG:
        return gEGF_Object_Name[0];
    case EGF_LINE_TAG:
        return gEGF_Object_Name[1];
    case EGF_ARC_TAG:
        return gEGF_Object_Name[2];
    case EGF_ELIPS_TAG:
        return gEGF_Object_Name[3];
    case EGF_ARROW_TAG:
        return gEGF_Object_Name[4];
    case EGF_PLINE_TAG:
        return gEGF_Object_Name[5];
    case EGF_PGON_TAG:
    case EGF_BOX_TAG:
        return gEGF_Object_Name[6];
    case EGF_PASTE_TAG:
        return gEGF_Object_Name[7];
    case EGF_TEXT_TAG:
        return gEGF_Object_Name[8];
    case EGF_ANNO_TAG:
        return gEGF_Object_Name[9];
    case EGF_PATH_TAG:
        return gEGF_Object_Name[10];
    case EGF_STEXT_TAG:
        return gEGF_Object_Name[11];
    case OBJTYPE_TEXT:
        return gEGF_Object_Name[12];
    case OBJTYPE_FMTPAGE:
        return gEGF_Object_Name[13];
    case OBJTYPE_SHAPEOBJ:
        return gEGF_Object_Name[14];
    case OBJTYPE_ANNOTATION:
        return gEGF_Object_Name[15];
    case OBJTYPE_BOOKMARK:
        return gEGF_Object_Name[16];
    case OBJTYPE_BLOCK:
        return gEGF_Object_Name[17];
    case OBJTYPE_SPCCHAR:
        return gEGF_Object_Name[18];
    case OBJTYPE_TAB:
        return gEGF_Object_Name[19];
    case OBJTYPE_PAGE:
        return gEGF_Object_Name[20];
    case OBJTYPE_BACKGROUND:
        return gEGF_Object_Name[21];
    case OBJTYPE_PGBLOCK:
        return gEGF_Object_Name[22];
    case OBJTYPE_PICTURE:
        return gEGF_Object_Name[23];
    case OBJTYPE_ROW:
        return gEGF_Object_Name[24];
    case OBJTYPE_DRAWOBJ:
        return gEGF_Object_Name[25];
    case OBJTYPE_FORMFIELD:
        return gEGF_Object_Name[26];
    case OBJTYPE_FRAME:
        return gEGF_Object_Name[27];
    case OBJTYPE_FOOTNOTE:
        return gEGF_Object_Name[28];
    default:
        {
            assert(false);
        }
        break;
    }

    return NULL;
}

wchar_t* CEgfObject::Get_EgfObjName(EGF_OBJ_TYPE *objType)
{
    return Get_EgfObjName((int)*objType);
}

bool CEgfObject::CheckCondition(CSearchEle *pCondition, void *pValue)
{
    if (pCondition == NULL) return false;

    switch (pCondition->valueType)
    {
    case EGFSVT_bool:
        if (pCondition->b == (*(bool*)pValue))
            return true;
        break;
    case EGFSVT_int:
        if (pCondition->i == (*(int*)pValue))
            return true;
        break;
    case EGFSVT_uint:
        if (pCondition->u == (*(unsigned int*)pValue))
            return true;
        break;
    case EGFSVT_float:
        if (pCondition->f == (*(float*)pValue))
            return true;
        break;
    case EGFSVT_string:
        if (CString(pCondition->s) == CString((*(wchar_t*)pValue)))
            return true;
        break;
    default:
        return false;
    }


    return false;
}

bool CEgfObject::CheckColorCondition(CSearchEle *pCondition, char *pValue)
{
    if (pCondition->valueType != EGFSVT_uint) return false;

    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000)>>16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF));
}

bool CEgfObject::CheckRGBAColorCondition(CSearchEle *pCondition, short *pValue)
{
    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000) >> 16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF)
        && (BYTE)pValue[4] == ((pCondition->u & 0xFF000000) >> 24));
}
