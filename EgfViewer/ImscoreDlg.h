#pragma once
#include "afxeditbrowsectrl.h"
#include "afxwin.h"


// CImscoreDlg dialog

class CImscoreDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CImscoreDlg)

public:
	CImscoreDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CImscoreDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_IMSCORE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    CMFCEditBrowseCtrl m_browseSource;
    CMFCEditBrowseCtrl m_browseTarget;
    CString m_csSource;
    CString m_csTarget;
    virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedOk();
    CEdit m_editStartPage;
    CEdit m_editEndPage;
    int m_nStartPage;
    int m_nEndPage;
    CButton m_chkAllPages;
    bool m_bAllPages;
    afx_msg void OnBnClickedCheckConvertAllPages();
};
