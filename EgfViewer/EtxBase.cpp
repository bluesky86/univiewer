#include "stdafx.h"
#include "EtxBase.h"


CEtxBase::CEtxBase()
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEtxBase::CEtxBase(int objType, int objDataType, int objSize)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}



CEtxBase::~CEtxBase()
{
}

bool CEtxBase::Parse(std::ifstream &ifs)
{
    if (mObjSize > 0)
    {
        int objType = 0, objDataType = 0, objSize = 0;
        if (!LoadObjHdr(ifs, objType, objDataType, objSize))
            return false;

        ParseChild(ifs, this, objType, objDataType, objSize, 0);

        if (objSize < mObjSize - 12) // 12 is header size
        {
            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;

            ParseChild(ifs, this, objType, objDataType, objSize, 1);
        }
    }

    return true;
}

void CEtxBase::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    // search children
    for (int i = 0; i < mChildren.size(); i++)
        mChildren[i]->Search(objType, pPairs, pList);
}