// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

// EgfViewerView.h : interface of the CEgfViewerView class
//

#pragma once

#include "Gdiplus.h"

#define VIEW_MARGIN         5

class CUvSkImage;

class CEgfViewerView : public CScrollView
{
protected: // create from serialization only
	CEgfViewerView();
	DECLARE_DYNCREATE(CEgfViewerView)

// Attributes
public:
	CEgfViewerDoc* GetDocument() const;
    float m_nClientWidth;
    float m_nClientHeight;
    float m_nPaintWidth;
    float m_nPaintHeight;
    float m_fScaleX;
    float m_fScaleY;

    float m_nLeftMargin;
    float m_nTopMargin;

    CUvSkImage *m_pImage;
    CUvSkImage *m_pLayerImage;

// Operations
public:
    void ShowEgfObject(CEgfObj *pObj);
    void CalculateRatio();
    void DrawEfgObj(Gdiplus::Graphics &g);

    //void DrawPoint(Gdiplus::Graphics &g, Gdiplus::Brush &brush, CEgfPoint &pt);
    //void DrawPoints(Gdiplus::Graphics &g, Gdiplus::Brush &brush, int count, CEgfPoint *pts);
    //void DrawPoints(Gdiplus::Graphics &g, Gdiplus::Brush &brush, CVector<CEgfPoint*> *pts);
    //void DrawLines(Gdiplus::Graphics &g, Gdiplus::Pen &pen, int count, CEgfPoint *pts);
    //void DrawLines(Gdiplus::Graphics &g, Gdiplus::Pen &pen, CVector<CEgfPoint*> *pts);

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CEgfViewerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnSize(UINT nType, int cx, int cy);
};

#ifndef _DEBUG  // debug version in EgfViewerView.cpp
inline CEgfViewerDoc* CEgfViewerView::GetDocument() const
   { return reinterpret_cast<CEgfViewerDoc*>(m_pDocument); }
#endif

