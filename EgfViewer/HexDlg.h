#pragma once
#include "afxwin.h"


// CHexDlg dialog

class CHexDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CHexDlg)

public:
	CHexDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHexDlg();

public:
    int mStrLen;
    char *m_pStr;
    void SetHexString(int len, char *pStr);

// Dialog Data
	enum { IDD = IDD_DILG_HEX_WND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    CEdit m_editHex;
    virtual BOOL OnInitDialog();
};
