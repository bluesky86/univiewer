#include "stdafx.h"
#include "MyTabCtrl.h"
//#include "Global.h"


CMyTabCtrl::CMyTabCtrl()
{

    m_nNumberOfPages = 0;
    m_tabCurrent = 0;

}

CMyTabCtrl::~CMyTabCtrl()
{
    for (int i = 0; i < m_tabPages.size(); i++)
        delete m_tabPages[i];
    m_tabPages.clear();
}

int CMyTabCtrl::CreateTab(int index, wchar_t *tabName, int DlgId)
{
    CTabBase *pTabBase = new CTabBase();

    int pos = InsertItem((int)m_tabPages.size(), tabName);
    pTabBase->Create(DlgId, this);
    m_tabPages.push_back(pTabBase);

    pTabBase->ShowWindow(SW_HIDE);

    return pos;

}

void CMyTabCtrl::SetRectangle()
{
    CRect tabRect, itemRect, itemZeroRect;
    int nX, nY, nXc, nYc;

    GetClientRect(&tabRect);
    GetItemRect(0, &itemZeroRect);
    GetItemRect(m_tabCurrent, &itemRect);

    nX = itemZeroRect.left;
    nY = itemRect.bottom + 1;
    nXc = tabRect.right - itemZeroRect.left - 1;
    nYc = tabRect.bottom - nY - 1;

    for (int nCount = 0; nCount < m_tabPages.size(); nCount++){
        m_tabPages[nCount]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);
    }
    m_tabPages[m_tabCurrent]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
}

BEGIN_MESSAGE_MAP(CMyTabCtrl, CTabCtrl)
    //{{AFX_MSG_MAP(CMyTabCtrl)
    ON_WM_LBUTTONDOWN()
    //}}AFX_MSG_MAP
    ON_WM_SIZE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyTabCtrl message handlers

void CMyTabCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
    CTabCtrl::OnLButtonDown(nFlags, point);

    if (m_tabCurrent != GetCurFocus()){
        m_tabPages[m_tabCurrent]->ShowWindow(SW_HIDE);
        m_tabCurrent = GetCurFocus();
        m_tabPages[m_tabCurrent]->ShowWindow(SW_SHOW);
        m_tabPages[m_tabCurrent]->SetFocus();
    }
}

void CMyTabCtrl::ShowTab(int index)
{
    for (int i = 0; i < m_tabPages.size(); i++)
    {
        if (i == index)
            m_tabPages[i]->ShowWindow(SW_SHOW);
        else
            m_tabPages[i]->ShowWindow(SW_HIDE);
    }
}



void CMyTabCtrl::OnSize(UINT nType, int cx, int cy)
{
    CTabCtrl::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here
    SetRectangle();
}
