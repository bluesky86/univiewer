#include "stdafx.h"
#include "EgfPline.h"
#include "Global.h"


CEgfPline::CEgfPline() : m_pGradientPoints(0), mVisible(0), mMask(0), m_pPoints(0)
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfPline::CEgfPline(int objType, int objDataType, int objSize)
    : m_pGradientPoints(0), mVisible(0), mMask(0), m_pPoints(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}

CEgfPline::~CEgfPline()
{
    if (m_pGradientPoints)
        delete[] m_pGradientPoints;

    if (m_pPoints)
        delete[] m_pPoints;
}

bool CEgfPline::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mThinknessUnits
    ifs.read(buf, 4);
    mThinknessUnits = Egf32_ToInt(buf);
    // mThinknessValue
    ifs.read(buf, 4);
    mThinknessValue = Egf32_ToInt(buf);

    // mArrowHeadSizeUnits
    ifs.read(buf, 4);
    mArrowHeadSizeUnits = Egf32_ToInt(buf);
    // mArrowHeadSizeValue
    ifs.read(buf, 4);
    mArrowHeadSizeValue = Egf32_ToInt(buf);

    // mColor
    ifs.read(mColor, 3);

    // mLineStyle
    ifs.read(buf, 4);
    mLineStyle = Egf32_ToInt(buf);

    // mOperation
    ifs.read(buf, 4);
    mOperation = Egf32_ToInt(buf);

    // mCapStyle
    ifs.read(buf, 4);
    mCapStyle = Egf32_ToInt(buf);

    // mJoinStyle
    ifs.read(buf, 4);
    mJoinStyle = Egf32_ToInt(buf);

    // mPenNum
    ifs.read(buf, 4);
    mPenNum = Egf32_ToInt(buf);

    // mArrowsProp
    ifs.read(buf, 4);
    mArrowsProp = Egf32_ToInt(buf);

    // mArrowsSolid
    ifs.read(buf, 4);
    mArrowsSolid = Egf32_ToInt(buf);

    // mArrowsDualHeaded
    ifs.read(buf, 4);
    mArrowsDualHeaded = Egf32_ToInt(buf);

    // mArrowsStyleStart
    ifs.read(buf, 4);
    mArrowsStyleStart = Egf32_ToInt(buf);

    // mArrowsStyleEnd
    ifs.read(buf, 4);
    mArrowsStyleEnd = Egf32_ToInt(buf);

    // mer
    ReadMer(ifs, mMer);

    // number of points
    ifs.read(buf, 4);
    mNumPoints = Egf32_ToInt(buf);

    // points
    if (mNumPoints > 0)
    {
        m_pPoints = new CEgfPoint[mNumPoints];
        ReadPoints(ifs, mNumPoints, m_pPoints);
    }

    // Gradient index
    ifs.read(buf, 4);
    mGradientIndex = Egf32_ToInt(buf);

    // mGradientNumPoints
    ifs.read(buf, 4);
    mGradientNumPoints = Egf32_ToInt(buf);

    // gradient points
    if (mGradientNumPoints > 0)
    {
        m_pGradientPoints = new CEgfPoint[mGradientNumPoints];
        ReadPoints(ifs, mGradientNumPoints, m_pGradientPoints);
    }

    // arrow points
    ReadPoints(ifs, 4, m_pArrowPoints);

    return true;
}

void CEgfPline::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_PLINE_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case POLYLINE_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case POLYLINE_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case POLYLINE_ARGS_ID_mThinknessUnits:
            met = CheckCondition(pPairs->pairs[i], &mThinknessUnits);
            break;
        case POLYLINE_ARGS_ID_mThinknessValue:
            met = CheckCondition(pPairs->pairs[i], &mThinknessValue);
            break;
        case POLYLINE_ARGS_ID_mArrowHeadSizeUnits:
            met = CheckCondition(pPairs->pairs[i], &mArrowHeadSizeUnits);
            break;
        case POLYLINE_ARGS_ID_mArrowHeadSizeValue:
            met = CheckCondition(pPairs->pairs[i], &mArrowHeadSizeValue);
            break;
        case POLYLINE_ARGS_ID_mColor:
            met = CheckColorCondition(pPairs->pairs[i], mColor);
            break;
        case POLYLINE_ARGS_ID_mLineStyle:
            met = CheckCondition(pPairs->pairs[i], &mLineStyle);
            break;
        case POLYLINE_ARGS_ID_mOperation:
            met = CheckCondition(pPairs->pairs[i], &mOperation);
            break;
        case POLYLINE_ARGS_ID_mCapStyle:
            met = CheckCondition(pPairs->pairs[i], &mCapStyle);
            break;
        case POLYLINE_ARGS_ID_mJoinStyle:
            met = CheckCondition(pPairs->pairs[i], &mJoinStyle);
            break;
        case POLYLINE_ARGS_ID_mArrowsProp:
            met = CheckCondition(pPairs->pairs[i], &mArrowsProp);
            break;
        case POLYLINE_ARGS_ID_mArrowsSolid:
            met = CheckCondition(pPairs->pairs[i], &mArrowsSolid);
            break;
        case POLYLINE_ARGS_ID_mArrowsDualHeaded:
            met = CheckCondition(pPairs->pairs[i], &mArrowsDualHeaded);
            break;
        case POLYLINE_ARGS_ID_mArrowsStyleStart:
            met = CheckCondition(pPairs->pairs[i], &mArrowsStyleStart);
            break;
        case POLYLINE_ARGS_ID_mArrowsStyleEnd:
            met = CheckCondition(pPairs->pairs[i], &mArrowsStyleEnd);
            break;
        case POLYLINE_ARGS_ID_mMer_x1:
            met = CheckCondition(pPairs->pairs[i], &mMer.x1);
            break;
        case POLYLINE_ARGS_ID_mMer_y1:
            met = CheckCondition(pPairs->pairs[i], &mMer.y1);
            break;
        case POLYLINE_ARGS_ID_mMer_x2:
            met = CheckCondition(pPairs->pairs[i], &mMer.x2);
            break;
        case POLYLINE_ARGS_ID_mMer_y2:
            met = CheckCondition(pPairs->pairs[i], &mMer.y2);
            break;
        case POLYLINE_ARGS_ID_mNumPoints:
            met = CheckCondition(pPairs->pairs[i], &mNumPoints);
            break;
        case POLYLINE_ARGS_ID_mGradientIndex:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex);
            break;
        case POLYLINE_ARGS_ID_mGradientNumPoints:
            met = CheckCondition(pPairs->pairs[i], &mGradientNumPoints);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}
