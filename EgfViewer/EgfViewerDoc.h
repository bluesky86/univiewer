// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

// EgfViewerDoc.h : interface of the CEgfViewerDoc class
//


#pragma once

#include "Global.h"
#include "EgfObj.h"
#include "EgfFile.h"
#include "CUvDoc.h"
#include "CUvPage.h"

class CEgfViewerView;

class CEgfViewerDoc : public CDocument
{
protected: // create from serialization only
	CEgfViewerDoc();
	DECLARE_DYNCREATE(CEgfViewerDoc)

// Attributes
public:
    CEgfFile *m_pEgfFile1;
    CEgfObj *m_pObj;

    CUvDoc *m_pUvDoc;
    CUvPage *m_pUvPage;
    int nPageNo;

    float m_nWidth;
    float m_nHeight;
    float m_nOrigX;
    float m_nOrigY;

// Operations
public:
    CEgfViewerView* GetView();

    void LoadFile(LPCTSTR fileName);
    bool GetPage(int pageNo);

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CEgfViewerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
    virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
};
