// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

#include "stdafx.h"
#include "MainFrm.h"
#include "GlobalView.h"
#include "Resource.h"
#include "EgfViewer.h"
#include "EgfObj.h"
#include "EgfApi.h"


class CGlobalViewMenuButton : public CMFCToolBarMenuButton
{
	friend class CGlobalView;

	DECLARE_SERIAL(CGlobalViewMenuButton)

public:
	CGlobalViewMenuButton(HMENU hMenu = NULL) : CMFCToolBarMenuButton((UINT)-1, hMenu, -1)
	{
	}

	virtual void OnDraw(CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages, BOOL bHorz = TRUE,
		BOOL bCustomizeMode = FALSE, BOOL bHighlight = FALSE, BOOL bDrawBorder = TRUE, BOOL bGrayDisabledButtons = TRUE)
	{
		pImages = CMFCToolBar::GetImages();

		CAfxDrawState ds;
		pImages->PrepareDrawImage(ds);

		CMFCToolBarMenuButton::OnDraw(pDC, rect, pImages, bHorz, bCustomizeMode, bHighlight, bDrawBorder, bGrayDisabledButtons);

		pImages->EndDrawImage(ds);
	}
};

IMPLEMENT_SERIAL(CGlobalViewMenuButton, CMFCToolBarMenuButton, 1)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGlobalView::CGlobalView()
{
	m_nCurrSort = ID_SORTING_GROUPBYTYPE;
}

CGlobalView::~CGlobalView()
{
}

BEGIN_MESSAGE_MAP(CGlobalView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_CLASS_ADD_MEMBER_FUNCTION, OnClassAddMemberFunction)
	ON_COMMAND(ID_CLASS_ADD_MEMBER_VARIABLE, OnClassAddMemberVariable)
	ON_COMMAND(ID_CLASS_DEFINITION, OnClassDefinition)
	ON_COMMAND(ID_CLASS_PROPERTIES, OnClassProperties)
	ON_COMMAND(ID_NEW_FOLDER, OnNewFolder)
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_COMMAND_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnSort)
	ON_UPDATE_COMMAND_UI_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnUpdateSort)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGlobalView message handlers

int CGlobalView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create views:
    const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | TVS_SHOWSELALWAYS;

	if (!m_wndGlobalView.Create(dwViewStyle, rectDummy, this, 2))
	{
		TRACE0("Failed to create Class View\n");
		return -1;      // fail to create
	}

	// Load images:
	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_SORT);
	m_wndToolBar.LoadToolBar(IDR_SORT, 0, 0, TRUE /* Is locked */);

	OnChangeVisualStyle();

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

	m_wndToolBar.SetOwner(this);

	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	CMenu menuSort;
	menuSort.LoadMenu(IDR_POPUP_SORT);

	m_wndToolBar.ReplaceButton(ID_SORT_MENU, CGlobalViewMenuButton(menuSort.GetSubMenu(0)->GetSafeHmenu()));

	CGlobalViewMenuButton* pButton =  DYNAMIC_DOWNCAST(CGlobalViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButton != NULL)
	{
		pButton->m_bText = FALSE;
		pButton->m_bImage = TRUE;
		pButton->SetImage(GetCmdMgr()->GetCmdImage(m_nCurrSort));
		pButton->SetMessageWnd(this);
	}

	// Fill in some static tree view data (dummy code, nothing magic here)
    m_wndGlobalView.m_nId = 2;
    FillView(NULL);

	return 0;
}

void CGlobalView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CGlobalView::Clear()
{
    m_wndGlobalView.DeleteAllItems();
}

void CGlobalView::FillView(CEgfFile *pEgfFile)
{
    Clear();

    if (pEgfFile == NULL) return;

    HTREEITEM hRoot = m_wndGlobalView.InsertItem(_T("Objects"), 0, 0);
    m_wndGlobalView.SetItemData(hRoot, (DWORD_PTR)0);
    m_wndGlobalView.SetItemState(hRoot, TVIS_BOLD, TVIS_BOLD);

    int sequence = 0;
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pGdiFillParmsTable, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pGdiplusFillParmsTable, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pGdiStrokeParmsTable, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pDashPattern, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pEmbeddedFontList, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pEmbeddedTrueTypeFonts, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pGdiEllipseParmsTable, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pGdiplusArcParmsTable, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pGdiplusGradientParmsTable, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pGdiplusStrokeParmsTable, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pHatchPattern, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pRasterFillTables, sequence++);
    FillOneObject(m_wndGlobalView, hRoot, pEgfFile->m_pTextParmsTable, sequence++);

    m_wndGlobalView.Expand(hRoot, TVE_EXPAND);
}

void CGlobalView::FillOneObject(CViewTree &m_wndView, HTREEITEM hParent, CEgfObj *pObj, int sequence)
{
    if (pObj == NULL) return;

    HTREEITEM hItem = 0;
    CString sequenceId;

    switch ((*GetEgfMetaList())[pObj->metaIndex]->kidsMode)
    {
    case KIDS_MODE_Styles:
        {
            sequenceId.Format(_T(" - %d"), 0);
            hItem = m_wndView.InsertItem(pObj->GetMetaName() + sequenceId, 0, 0, hParent);

            for (unsigned int i = 0; i < pObj->children.size(); i++)
                FillOneParms(m_wndView, hItem, pObj->children[i], i + 1);
            m_wndView.Expand(hItem, TVE_EXPAND);
        }
        break;
    default: 
        break;
    }

    if (hItem != 0)
        m_wndView.SetItemData(hItem, (DWORD_PTR)pObj);

}

void CGlobalView::FillOneParms(CViewTree &m_wndView, HTREEITEM hParent, CEgfObj *pObj, int sequence)
{
    if (pObj == NULL) return;

    HTREEITEM hItem = 0;
    CString sequenceId;
    sequenceId.Format(_T("Index - %d"), sequence);
    hItem = m_wndView.InsertItem(sequenceId, 0, 0, hParent);

    if (hItem != 0)
        m_wndView.SetItemData(hItem, (DWORD_PTR)pObj);
}

void CGlobalView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndGlobalView;
	ASSERT_VALID(pWndTree);

	if (pWnd != pWndTree)
	{
		CDockablePane::OnContextMenu(pWnd, point);
		return;
	}

	if (point != CPoint(-1, -1))
	{
		// Select clicked item:
		CPoint ptTree = point;
		pWndTree->ScreenToClient(&ptTree);

		UINT flags = 0;
		HTREEITEM hTreeItem = pWndTree->HitTest(ptTree, &flags);
		if (hTreeItem != NULL)
		{
			pWndTree->SelectItem(hTreeItem);
		}
	}

	pWndTree->SetFocus();
	CMenu menu;
	menu.LoadMenu(IDR_POPUP_SORT);

	CMenu* pSumMenu = menu.GetSubMenu(0);

	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}
}

void CGlobalView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
    m_wndGlobalView.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

BOOL CGlobalView::PreTranslateMessage(MSG* pMsg)
{
	return CDockablePane::PreTranslateMessage(pMsg);
}

void CGlobalView::OnSort(UINT id)
{
	if (m_nCurrSort == id)
	{
		return;
	}

	m_nCurrSort = id;

	CGlobalViewMenuButton* pButton =  DYNAMIC_DOWNCAST(CGlobalViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButton != NULL)
	{
		pButton->SetImage(GetCmdMgr()->GetCmdImage(id));
		m_wndToolBar.Invalidate();
		m_wndToolBar.UpdateWindow();
	}
}

void CGlobalView::OnUpdateSort(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(pCmdUI->m_nID == m_nCurrSort);
}

void CGlobalView::OnClassAddMemberFunction()
{
	AfxMessageBox(_T("Add member function..."));
}

void CGlobalView::OnClassAddMemberVariable()
{
	// TODO: Add your command handler code here
}

void CGlobalView::OnClassDefinition()
{
	// TODO: Add your command handler code here
}

void CGlobalView::OnClassProperties()
{
	// TODO: Add your command handler code here
}

void CGlobalView::OnNewFolder()
{
    AfxMessageBox(L"Folder...");
}

void CGlobalView::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rectTree;
    m_wndGlobalView.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

void CGlobalView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);

    m_wndGlobalView.SetFocus();
}

void CGlobalView::OnChangeVisualStyle()
{
    m_GlobalViewImages.DeleteImageList();

	UINT uiBmpId = theApp.m_bHiColorIcons ? IDB_CLASS_VIEW_24 : IDB_CLASS_VIEW;

	CBitmap bmp;
	if (!bmp.LoadBitmap(uiBmpId))
	{
		TRACE(_T("Can't load bitmap: %x\n"), uiBmpId);
		ASSERT(FALSE);
		return;
	}

	BITMAP bmpObj;
	bmp.GetBitmap(&bmpObj);

	UINT nFlags = ILC_MASK;

	nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;

	m_GlobalViewImages.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
	m_GlobalViewImages.Add(&bmp, RGB(255, 0, 0));

    m_wndGlobalView.SetImageList(&m_GlobalViewImages, TVSIL_NORMAL);

	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_SORT_24 : IDR_SORT, 0, 0, TRUE /* Locked */);
}

void CGlobalView::SelectObject(CEgfObj *pObj)
{
    HTREEITEM hItem = FindItem((DWORD_PTR)pObj, m_wndGlobalView);

    if (hItem == 0)
        AfxMessageBox(_T("Cannot find the object!"));
    else
        m_wndGlobalView.SelectItem(hItem);
}

// name - the name of the item that is searched for
// tree - a reference to the tree control
// hRoot - the handle to the item where the search begins
HTREEITEM CGlobalView::FindItem(const DWORD_PTR pObj, CTreeCtrl& tree, HTREEITEM hRoot)
{
    // check whether the current item is the searched one
    DWORD_PTR data = tree.GetItemData(hRoot);
    if (data == pObj)
        return hRoot;

    // get a handle to the first child item
    HTREEITEM hSub = tree.GetChildItem(hRoot);
    // iterate as long a new item is found
    while (hSub)
    {
        // check the children of the current item
        HTREEITEM hFound = FindItem(pObj, tree, hSub);
        if (hFound)
            return hFound;

        // get the next sibling of the current item
        hSub = tree.GetNextSiblingItem(hSub);
    }

    // return NULL if nothing was found
    return NULL;
}

HTREEITEM CGlobalView::FindItem(const DWORD_PTR pObj, CTreeCtrl& tree)
{
    HTREEITEM root = tree.GetRootItem();
    while (root != NULL)
    {
        HTREEITEM hFound = FindItem(pObj, tree, root);
        if (hFound)
            return hFound;

        root = tree.GetNextSiblingItem(root);
    }

    return NULL;
}
