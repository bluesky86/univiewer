#include "stdafx.h"
#include "EgfSText.h"
#include "Global.h"

CEgfSText::CEgfSText()
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfSText::CEgfSText(int objType, int objDataType, int objSize)
    //:mVisible(0), mMask(0), mHasMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}


CEgfSText::~CEgfSText()
{
}

bool CEgfSText::Parse(std::ifstream &ifs)
{
    char buf[4];
    int objType = 0, objDataType = 0, objSize = 0;

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mMer
    ReadMer(ifs, mMer);

    // mAbsPos
    ifs.read(buf, 4);
    mAbsPos = Egf32_ToInt(buf);

    // mScaleX
    ifs.read(buf, 4);
    mScaleX = Egf32_ToInt(buf);

    // mScaleX1
    ifs.read(buf, 4);
    mScaleX1 = Egf32_ToInt(buf);

    // mScaleY
    ifs.read(buf, 4);
    mScaleY = Egf32_ToInt(buf);

    // mRotation
    ifs.read(buf, 4);
    mRotation = Egf32_ToInt(buf);

    // mPgnType
    ifs.read(buf, 4);
    mPgnType = Egf32_ToInt(buf);

    // point
    ReadPoints(ifs, 1, &mPoint);
    mOffset.x = mPoint.x;
    mOffset.y = mPoint.y;

    if (mObjSize == 50)
        return true;

    if (!LoadObjHdr(ifs, objType, objDataType, objSize))
        return false;

    return ParseChild(ifs, this, objType, objDataType, objSize, 0);
}

void CEgfSText::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    CheckCriteria(objType, pPairs, pList);

    // search children
    for (int i = 0; i < mChildren.size(); i++)
        mChildren[i]->Search(objType, pPairs, pList);
}

void CEgfSText::CheckCriteria(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_STEXT_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case STEXT_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case STEXT_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case STEXT_ARGS_ID_mScaleX:
            met = CheckCondition(pPairs->pairs[i], &mScaleX);
            break;
        case STEXT_ARGS_ID_mScaleX1:
            met = CheckCondition(pPairs->pairs[i], &mScaleX1);
            break;
        case STEXT_ARGS_ID_mScaleY:
            met = CheckCondition(pPairs->pairs[i], &mScaleY);
            break;
        case STEXT_ARGS_ID_mRotation:
            met = CheckCondition(pPairs->pairs[i], &mRotation);
            break;
        case STEXT_ARGS_ID_mPgnType:
            met = CheckCondition(pPairs->pairs[i], &mPgnType);
            break;
        case STEXT_ARGS_ID_mPoint_x:
            met = CheckCondition(pPairs->pairs[i], &mPoint.x);
            break;
        case STEXT_ARGS_ID_mPoint_y:
            met = CheckCondition(pPairs->pairs[i], &mPoint.y);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}