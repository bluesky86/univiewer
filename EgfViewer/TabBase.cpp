#include "stdafx.h"
#include "TabBase.h"
#include "afxdialogex.h"
//#include "Global.h"
#include "EgfApi.h"
#include "DebugNew.h"


IMPLEMENT_DYNAMIC(CTabBase, CDialog)

CTabBase::CTabBase(CWnd* pParent /*=NULL*/)
: CDialog(CTabBase::IDD, pParent)
{
    xPos = CTRL_LEFT_MARGIN;
    yPos = CTRL_TOP_MARGIN;
    m_nSearchMetaId = -1;
}

CTabBase::CTabBase(int idd, CWnd* pParent)
    : CDialog(idd, pParent)
{
    xPos = CTRL_LEFT_MARGIN;
    yPos = CTRL_TOP_MARGIN;
    yPos = CTRL_TOP_MARGIN;
}

CTabBase::~CTabBase()
{
    Clear();

    if (NULL == m_nonCleartypeFont.m_hObject)
    {
        // destroy font
        m_nonCleartypeFont.DeleteObject();
    }
}

void CTabBase::Clear()
{
    for (int i = 0; i < m_pStatic.size(); i++)
        delete m_pStatic[i];
    m_pStatic.clear();

    for (int i = 0; i < m_pComboBox.size(); i++)
        delete m_pComboBox[i];
    m_pComboBox.clear();

    for (int i = 0; i < m_pEdit.size(); i++)
        delete m_pEdit[i];
    m_pEdit.clear();

    for (int i = 0; i < m_pBtn.size(); i++)
        delete m_pBtn[i];
    m_pBtn.clear();

    xPos = CTRL_LEFT_MARGIN;
    yPos = CTRL_TOP_MARGIN;

    gControlIndex = 0;
}

void CTabBase::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_STATIC_BASE, m_staticBase);
}

BEGIN_MESSAGE_MAP(CTabBase, CDialog)
    ON_WM_VSCROLL()
    ON_WM_SIZE()
    ON_WM_SIZE()
    ON_WM_MOUSEWHEEL()
    ON_WM_CREATE()
END_MESSAGE_MAP()

////////////////////////////

CStatic* CTabBase::CreateStatic(int x, int y, int width, int height, CString &title)
{
    CStatic *pStatic = new CStatic;

    RECT rc;
    rc.left = x; rc.right = x + width;
    rc.top = y; rc.bottom = y + height;
    pStatic->Create(title + ": ", WS_CHILD | WS_VISIBLE, rc, this);
 
    pStatic->SetFont(&m_nonCleartypeFont, FALSE);

    m_pStatic.push_back(pStatic);

    return pStatic;
}

CComboBox* CTabBase::CreateComboBox(int id, int x, int y, int width, int height)
{
    CComboBox *pCb = new CComboBox;

    RECT rc;
    rc.left = x; rc.right = x + width;
    rc.top = y; rc.bottom = y + height;
    pCb->Create(WS_CHILD | WS_VISIBLE | WS_TABSTOP | CBS_DROPDOWN, rc, this, id);

    pCb->SetFont(&m_nonCleartypeFont, FALSE);

    m_pComboBox.push_back(pCb);

    return pCb;
}

CEdit* CTabBase::CreateEdit(int id, int x, int y, int width, int height)
{
    CEdit *pEdit = new CEdit;

    RECT rc;
    rc.left = x; rc.right = x + width;
    rc.top = y; rc.bottom = y + height;
    pEdit->Create(WS_CHILD | WS_VISIBLE | WS_TABSTOP | CBS_DROPDOWN, rc, this, id);

    pEdit->SetFont(&m_nonCleartypeFont, FALSE);

    m_pEdit.push_back(pEdit);

    return pEdit;
}

CStatic* CTabBase::CreateStatic(int width, int height, CString &title)
{
    CStatic *p = CreateStatic(xPos, yPos, width, height, title);
    xPos += (width + CTRL_HORI_SPACE);
    return p;
}

CComboBox* CTabBase::CreateComboBox(int id, int width, int height)
{
    CComboBox *p = CreateComboBox(id, xPos, yPos, width, height);
    xPos = CTRL_LEFT_MARGIN;
    yPos += (height + CTRL_VERT_SPACE);

    return p;
}

//CComboBox* CTabBase::CreateComboBox(int id, int width, int height, CString &name)
//{
//    CreateStatic(width, CTRL_DEFAULT_HEIGHT, name);
//    CreateComboBox()
//}
//
CEdit* CTabBase::CreateEdit(int id, int width, int height)
{
    CEdit *p = CreateEdit(id, xPos, yPos, width, height);
    xPos = CTRL_LEFT_MARGIN;
    yPos += (height + CTRL_VERT_SPACE);
    return p;
}

bool CTabBase::CreateMer(int idx1, int idy1, int idx2, int idy2, CString &name)
{
    CButton * pGroup = new CButton();

    CRect rc(xPos, yPos, xPos + 100 + CTRL_HORI_SPACE * 2, yPos + (CTRL_DEFAULT_HEIGHT + CTRL_VERT_SPACE) * 5);
    pGroup->Create(name, WS_CHILD | WS_VISIBLE | BS_GROUPBOX, rc, this, gControlIndex++);
    pGroup->SetFont(&m_nonCleartypeFont, FALSE);
    m_pBtn.push_back(pGroup);

    yPos += 20;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("x1")));
    xPos += 30;
    CreateEdit(idx1, 80, CTRL_DEFAULT_HEIGHT);
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("y1")));
    xPos += 30;
    CreateEdit(idy1, 80, CTRL_DEFAULT_HEIGHT);
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("x2")));
    xPos += 30;
    CreateEdit(idx2, 80, CTRL_DEFAULT_HEIGHT);
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("y2")));
    xPos += 30;
    CreateEdit(idy2, 80, CTRL_DEFAULT_HEIGHT);

    return true;
}

bool CTabBase::CreatePoint(int idx, int idy, CString &name)
{
    CButton * pGroup = new CButton();

    CRect rc(xPos, yPos, xPos + 100 + CTRL_HORI_SPACE * 2, yPos + (CTRL_DEFAULT_HEIGHT + CTRL_VERT_SPACE) * 3);
    pGroup->Create(name, WS_CHILD | WS_VISIBLE | BS_GROUPBOX, rc, this, gControlIndex++);
    pGroup->SetFont(&m_nonCleartypeFont, FALSE);
    m_pBtn.push_back(pGroup);

    yPos += 20;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("x")));
    xPos += 30;
    CreateEdit(idx, 80, CTRL_DEFAULT_HEIGHT);
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("y")));
    xPos += 30;
    CreateEdit(idy, 80, CTRL_DEFAULT_HEIGHT);

    yPos += CTRL_DEFAULT_HEIGHT;

    return true;
}

bool CTabBase::CreateColor(int idr, int idg, int idb, CString &name)
{
    CButton * pGroup = new CButton();

    CRect rc(xPos, yPos, xPos + 180 + CTRL_HORI_SPACE * 2, yPos + (CTRL_DEFAULT_HEIGHT + CTRL_VERT_SPACE) * 2);
    pGroup->Create(name, WS_CHILD | WS_VISIBLE | BS_GROUPBOX, rc, this, gControlIndex++);
    pGroup->SetFont(&m_nonCleartypeFont, FALSE);
    m_pBtn.push_back(pGroup);

    yPos += 20;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("R")));
    xPos += 30;
    CreateEdit(idr, xPos, yPos, 30, CTRL_DEFAULT_HEIGHT);

    xPos += 30;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("G")));
    xPos += 30;
    CreateEdit(idg, xPos, yPos, 30, CTRL_DEFAULT_HEIGHT);

    xPos += 30;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("B")));
    xPos += 30;
    CreateEdit(idb, xPos, yPos, 30, CTRL_DEFAULT_HEIGHT);

    xPos = CTRL_LEFT_MARGIN;
    yPos += (CTRL_DEFAULT_HEIGHT + CTRL_VERT_SPACE * 2);

    return true;
}

bool CTabBase::CreateColor(int idr, int idg, int idb, int ida, CString &name)
{
    CButton * pGroup = new CButton();

    CRect rc(xPos, yPos, xPos + 210 + CTRL_HORI_SPACE * 3, yPos + (CTRL_DEFAULT_HEIGHT + CTRL_VERT_SPACE) * 2);
    pGroup->Create(name, WS_CHILD | WS_VISIBLE | BS_GROUPBOX, rc, this, gControlIndex++);
    pGroup->SetFont(&m_nonCleartypeFont, FALSE);
    m_pBtn.push_back(pGroup);

    yPos += 20;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("R")));
    xPos += 30;
    CreateEdit(idr, xPos, yPos, 30, CTRL_DEFAULT_HEIGHT);

    xPos += 30;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("G")));
    xPos += 30;
    CreateEdit(idg, xPos, yPos, 30, CTRL_DEFAULT_HEIGHT);

    xPos += 30;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("B")));
    xPos += 30;
    CreateEdit(idb, xPos, yPos, 30, CTRL_DEFAULT_HEIGHT);

    xPos += 30;
    CreateStatic(xPos + 10, yPos, 20, CTRL_DEFAULT_HEIGHT, CString(_T("A")));
    xPos += 30;
    CreateEdit(ida, xPos, yPos, 30, CTRL_DEFAULT_HEIGHT);

    xPos = CTRL_LEFT_MARGIN;
    yPos += (CTRL_DEFAULT_HEIGHT + CTRL_VERT_SPACE * 2);

    return true;
}

void CTabBase::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    int nDelta;
    //int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;
    int nMaxPos = yPos - m_nCurHeight;

    switch (nSBCode)
    {
    case SB_LINEDOWN:
        if (m_nScrollPos >= nMaxPos)
            return;

        nDelta = min(max(nMaxPos / 20, 5), nMaxPos - m_nScrollPos);
        break;

    case SB_LINEUP:
        if (m_nScrollPos <= 0)
            return;
        nDelta = -min(max(nMaxPos / 20, 5), m_nScrollPos);
        break;
    case SB_PAGEDOWN:
        if (m_nScrollPos >= nMaxPos)
            return;
        nDelta = min(max(nMaxPos / 10, 5), nMaxPos - m_nScrollPos);
        break;
    case SB_THUMBTRACK:
    case SB_THUMBPOSITION:
        nDelta = (int)nPos - m_nScrollPos;
        break;

    case SB_PAGEUP:
        if (m_nScrollPos <= 0)
            return;
        nDelta = -min(max(nMaxPos / 10, 5), m_nScrollPos);
        break;

    default:
        return;
    }
    m_nScrollPos += nDelta;
    SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
    ScrollWindow(0, -nDelta);


    CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}


BOOL CTabBase::OnInitDialog()
{
    CDialog::OnInitDialog();

    // TODO:  Add extra initialization here
    // save the original size
    //GetWindowRect(m_rcOriginalRect);
    GetClientRect(m_rcOriginalRect);
    // initial scroll position
    m_nScrollPos = 0;

    //// font
    if (NULL == m_nonCleartypeFont.m_hObject)
    {
        CFont* pOriginalFont = m_staticBase.GetFont(); // use template font as... template!

        // pull information from original font  
        LOGFONT logfont;
        pOriginalFont->GetLogFont(&logfont);

        // create our font based on adjusted information
        m_nonCleartypeFont.CreateFontIndirect(&logfont);
    }


    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CTabBase::ResetScrollBar(int pos)
{
    m_nScrollPos = pos;
    SetScrollRange(SB_VERT, 0, yPos);
    SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
}

void CTabBase::OnSize(UINT nType, int cx, int cy)
{
    CDialog::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here
    //m_nCurHeight = cy;

    //SCROLLINFO si;
    //si.cbSize = sizeof(SCROLLINFO);
    //si.fMask = SIF_ALL;
    //si.nMin = 0;
    //si.nMax = m_rcOriginalRect.Height();
    //si.nPage = cy;
    //si.nPos = 0;
    //SetScrollInfo(SB_VERT, &si, TRUE);
}


BOOL CTabBase::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
    // TODO: Add your message handler code here and/or call default
    //int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;
    int nMaxPos = yPos - m_nCurHeight;

    if (zDelta<0)
    {
        if (m_nScrollPos < nMaxPos)
        {
            zDelta = min(max(nMaxPos / 20, 5), nMaxPos - m_nScrollPos);

            m_nScrollPos += zDelta;
            SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
            ScrollWindow(0, -zDelta);
        }
    }
    else
    {
        if (m_nScrollPos > 0)
        {
            zDelta = -min(max(nMaxPos / 20, 5), m_nScrollPos);

            m_nScrollPos += zDelta;
            SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
            ScrollWindow(0, -zDelta);
        }
    }

    return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

int CTabBase::BuildCondition(CSearchCriteria *pPairs)
{
    CEgfMeta *pMeta = (*GetEgfMetaList())[(*GetEgfSearchMetaList())[m_nSearchMetaId]->id];
    CEgfSearchMeta *pSearchMetaEle = (*GetEgfSearchMetaList())[m_nSearchMetaId];
    
    for (unsigned int i = 0; i < pSearchMetaEle->elements.size(); i++)
    {
        switch (pMeta->elements[pSearchMetaEle->elements[i]->id]->type)
        {
        case EGF_ELE_META_TYPE_int:
        case EGF_ELE_META_TYPE_lineStyle:
        case EGF_ELE_META_TYPE_capStyle:
        case EGF_ELE_META_TYPE_operation:
        case EGF_ELE_META_TYPE_fillStyle:
        case EGF_ELE_META_TYPE_hatchStyle:
        case EGF_ELE_META_TYPE_shadingStyle:
        case EGF_ELE_META_TYPE_joinStyle:
        case EGF_ELE_META_TYPE_curveStyle:
        case EGF_ELE_META_TYPE_effectType:
            CreateIntEle(pPairs, pSearchMetaEle->elements[i]->id, EGFSOP_EQUAL, (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId));
            break;
        case EGF_ELE_META_TYPE_8bool:
        case EGF_ELE_META_TYPE_intBool:
            CreateBoolEle(pPairs, pSearchMetaEle->elements[i]->id, (CComboBox*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId));
            break;
        case EGF_ELE_META_TYPE_color3:
            CreateColorEle(pPairs, pSearchMetaEle->elements[i]->id,
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId),
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 1),
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 2));
            break;
        case EGF_ELE_META_TYPE_color4:
            CreateColorEle(pPairs, pSearchMetaEle->elements[i]->id,
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId),
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 1),
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 2),
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 3));
            break;
        case EGF_ELE_META_TYPE_point:
            CreatePointEle(pPairs, pSearchMetaEle->elements[i]->id, pSearchMetaEle->elements[i]->id + 1, (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId), (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 1));
            break;
        case EGF_ELE_META_TYPE_mer:
            CreateMerEle(pPairs, pSearchMetaEle->elements[i]->id, pSearchMetaEle->elements[i]->id + 1, pSearchMetaEle->elements[i]->id + 2, pSearchMetaEle->elements[i]->id+3,
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId), (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 1),
                (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 2), (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId + 3));
            break;
        case EGF_ELE_META_TYPE_string:
            CreateStringEle(pPairs, pSearchMetaEle->elements[i]->id, (CEdit*)GetDlgItem(pSearchMetaEle->elements[i]->ctrlId));
            break;
        case EGF_ELE_META_TYPE_intArray:
		case EGF_ELE_META_TYPE_shortArray:
        case EGF_ELE_META_TYPE_points:
        case EGF_ELE_META_TYPE_none:
        default:
            break;
        }

    }

    return (*GetEgfSearchMetaList())[m_nSearchMetaId]->id;

}


