#pragma once
#include "EgfGroup.h"

class CEtxShape : public CEgfGroup
{
public:
    CEtxShape();
    CEtxShape(int objType, int objDataType, int objSize);
    virtual ~CEtxShape();

public:
    bool Parse(std::ifstream &ifs);
    virtual void Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList);
};

