#include "stdafx.h"
#include "SearchView.h"

class CSearchViewMenuButton : public CMFCToolBarMenuButton
{
    friend class CSearchView;

    DECLARE_SERIAL(CSearchViewMenuButton)

public:
    CSearchViewMenuButton(HMENU hMenu = NULL) : CMFCToolBarMenuButton((UINT)-1, hMenu, -1)
    {
    }

    virtual void OnDraw(CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages, BOOL bHorz = TRUE,
        BOOL bCustomizeMode = FALSE, BOOL bHighlight = FALSE, BOOL bDrawBorder = TRUE, BOOL bGrayDisabledButtons = TRUE)
    {
        pImages = CMFCToolBar::GetImages();

        CAfxDrawState ds;
        pImages->PrepareDrawImage(ds);

        CMFCToolBarMenuButton::OnDraw(pDC, rect, pImages, bHorz, bCustomizeMode, bHighlight, bDrawBorder, bGrayDisabledButtons);

        pImages->EndDrawImage(ds);
    }
};

IMPLEMENT_SERIAL(CSearchViewMenuButton, CMFCToolBarMenuButton, 1)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CSearchView::CSearchView()
{
}


CSearchView::~CSearchView()
{
}

BEGIN_MESSAGE_MAP(CSearchView, CDockablePane)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_CONTEXTMENU()
    ON_WM_PAINT()
    ON_WM_SETFOCUS()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClassView message handlers

int CSearchView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;

    CRect rectDummy;
    rectDummy.SetRectEmpty();

    // Create views:
    if (!m_dlgSearch.Create(IDD_DLG_SEARCH, this))
    {
        TRACE0("Failed to create Class View\n");
        return -1;      // fail to create
    }

    // Load images:
    m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_SORT);
    m_wndToolBar.LoadToolBar(IDR_SORT, 0, 0, TRUE /* Is locked */);

    //OnChangeVisualStyle();

    m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
    m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

    m_wndToolBar.SetOwner(this);

    // All commands will be routed via this control , not via the parent frame:
    m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

    CMenu menuSort;
    menuSort.LoadMenu(IDR_POPUP_SORT);

    m_wndToolBar.ReplaceButton(ID_SORT_MENU, CSearchViewMenuButton(menuSort.GetSubMenu(0)->GetSafeHmenu()));

    CSearchViewMenuButton* pButton = DYNAMIC_DOWNCAST(CSearchViewMenuButton, m_wndToolBar.GetButton(0));

    // Fill in some static tree view data (dummy code, nothing magic here)


    return 0;
}

void CSearchView::OnSize(UINT nType, int cx, int cy)
{
    CDockablePane::OnSize(nType, cx, cy);
    AdjustLayout();
}

BOOL CSearchView::PreTranslateMessage(MSG* pMsg)
{
    return CDockablePane::PreTranslateMessage(pMsg);
}

void CSearchView::AdjustLayout()
{
    if (GetSafeHwnd() == NULL)
    {
        return;
    }

    CRect rectClient;
    GetClientRect(rectClient);

    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
    m_dlgSearch.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CSearchView::OnSetFocus(CWnd* pOldWnd)
{
    CDockablePane::OnSetFocus(pOldWnd);

    m_dlgSearch.SetFocus();
}

void CSearchView::OnPaint()
{
    CPaintDC dc(this); // device context for painting

    CRect rectTree;
    m_dlgSearch.GetWindowRect(rectTree);
    ScreenToClient(rectTree);

    rectTree.InflateRect(1, 1);
    dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

