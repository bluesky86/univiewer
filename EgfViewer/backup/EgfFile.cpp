#include "stdafx.h"
#include "EgfFile.h"


CEgfFile::CEgfFile()
    :m_pChild(0)
{
}


CEgfFile::~CEgfFile()
{
    Clear();
}

void CEgfFile::Clear()
{
    if (m_pChild)
    {
        delete m_pChild;
        m_pChild = NULL;
    }
}


bool CEgfFile::Parse(std::ifstream &ifs)
{
    /////////////////////////////////////////////////////////////
    //  load object
    /////////////////////////////////////////////////////////////
    int objType = 0, objDataType = 0, objSize = 0;

    if (CEgfObj::LoadObjHdr(ifs, objType, objDataType, objSize))
    {
        Clear();

        m_pChild = CEgfObj::CreateEgfObj(objType, objDataType, objSize);
        if (m_pChild->Parse(ifs, 0))
        {
        }
        else
        {
            AfxMessageBox(_T("Parsing object failed!"));
        }
    }
    else
    {
        AfxMessageBox(L"Failed to load file!");
        return false;
    }


    return true;
}

void CEgfFile::Search(int objMetaId, CSearchCriteria *pPairs, std::vector<CEgfObj*> *pList)
{
    if (m_pChild)
        m_pChild->Search(objMetaId, pPairs, pList);
}