#pragma once
#include <fstream>
#include <vector>
#include <map>

class CEgfMeta;
typedef std::vector<CEgfMeta*> CEgfMetaList;
extern CEgfMetaList gEgfMetaList;
extern std::map<int, int> gEgfIdToIndexMap;

class CEgfSearchMeta;
typedef std::vector<CEgfSearchMeta*> CEgfSearchMetaList;
extern CEgfSearchMetaList gEgfSearchMetaList;

typedef enum
{
    SDT_OBJ_TYPE,
    SDT_OBJ_ARGS,
    SDT_OBJ_DATA,
    SDT_OBJ_CHILDREN_START,
    SDT_OBJ_CHILDREN_END
} SIMON_DATA_TYPE;

typedef enum
{
    EGFSOP_LESS,
    EGFSOP_EQUAL,
    EGFSOP_GREATER
} EGF_SEACH_OP;

typedef enum
{
    EGFSVT_bool,
    EGFSVT_int,
    EGFSVT_uint,
    EGFSVT_float,
    EGFSVT_string
} EGF_SEACH_VALUE_TYPE;

#define     GLOBAL_ARGS_ID_mVisible         0
#define     GLOBAL_ARGS_ID_mMask            1

class CSearchEle
{
public:
    CSearchEle() : argId(0), op(EGFSOP_EQUAL), valueType(EGFSVT_string), s(0) {}
    ~CSearchEle() { if (valueType == EGFSVT_string && s) delete[] s; }

public:
    int argId;
    EGF_SEACH_OP op;
    EGF_SEACH_VALUE_TYPE valueType;
    union
    {
        bool b;
        int i;
        unsigned int u;
        float f;
        wchar_t *s;
    };
};

class CSearchCriteria
{
public:
    CSearchCriteria(){};
    ~CSearchCriteria()
    {
        std::vector<CSearchEle*>::iterator it;
        for (it = pairs.begin(); it != pairs.end(); it++)
            delete *it;
        pairs.clear();
    }

public:
    std::vector<CSearchEle*> pairs;

};

class CEgfPoint
{
public:
    CEgfPoint() :x(0), y(0){};
    CEgfPoint(float x_, float y_) :x(x_), y(y_){};

    float x, y;
};

class CEgfMer
{
public:
    CEgfMer() :x1(0), x2(0), y1(0), y2(0) {};

    float x1, x2, y1, y2;
};

typedef enum
{
    KIDS_MODE_NONE,
    KIDS_MODE_Group,
    KIDS_MODE_Path,
    KIDS_MODE_SText,
    KIDS_MODE_Ext,
} KIDS_MODE;

typedef enum
{
    EGF_ELE_META_TYPE_none,
    EGF_ELE_META_TYPE_8bool,
    EGF_ELE_META_TYPE_intBool,
    EGF_ELE_META_TYPE_int,
    EGF_ELE_META_TYPE_color3,
    EGF_ELE_META_TYPE_color4,
    EGF_ELE_META_TYPE_point,
    EGF_ELE_META_TYPE_points = EGF_ELE_META_TYPE_point + 2,
    EGF_ELE_META_TYPE_mer,
    EGF_ELE_META_TYPE_intArray = EGF_ELE_META_TYPE_mer + 4,
	EGF_ELE_META_TYPE_shortArray,
    EGF_ELE_META_TYPE_string,
    EGF_ELE_META_TYPE_lineStyle,
    EGF_ELE_META_TYPE_capStyle,
    EGF_ELE_META_TYPE_operation,
    EGF_ELE_META_TYPE_fillStyle,
    EGF_ELE_META_TYPE_hatchStyle,
    EGF_ELE_META_TYPE_shadingStyle,
    EGF_ELE_META_TYPE_joinStyle,
    EGF_ELE_META_TYPE_curveStyle,
    EGF_ELE_META_TYPE_effectType,
} EGF_ELE_META_TYPE;

typedef enum
{
    DRAW_TYPE_None,
    DRAW_TYPE_point,
    DRAW_TYPE_points,
    DRAW_TYPE_line,
    DRAW_TYPE_lines,
    DRAW_TYPE_Polylines,
    DRAW_TYPE_mer,
}DRAW_TYPE;

class CEgfEleMeta
{
public:
    CEgfEleMeta() : type(EGF_ELE_META_TYPE_none), name(0), displayHex(0), drawType(DRAW_TYPE_None), num(-1), num_id(-1){};
    ~CEgfEleMeta()
    {
        if (name)
            delete[] name;

    };

public:
    int id;
    EGF_ELE_META_TYPE type;
    wchar_t *name;
    int num;
    int num_id;
    bool displayHex;
    DRAW_TYPE drawType;
    int drawArgs[2];
};

class CEgfMeta
{
public:
    CEgfMeta() :id(0), egfId(0), name(0), kidsMode(KIDS_MODE_NONE), hasOffset(0), hasDisplayHex(0){};
    ~CEgfMeta()
    {
        if (name)
            delete[] name;

        for (int i = 0; i < elements.size(); i++)
            delete elements[i];
        elements.clear();

        displayOrderMap.clear();
    };

public:
    int id;
    int egfId;
    wchar_t *name;
    KIDS_MODE kidsMode;
    bool hasOffset;
    bool hasDisplayHex;

    std::vector<CEgfEleMeta*> elements;
    std::map<int, int> displayOrderMap;
};

class CEgfPointArray
{
public:
    CEgfPointArray(){};
    ~CEgfPointArray()
    {
        for (int i = 0; i < points.size(); i++)
            delete points[i];
        points.clear();
    };

    std::vector<CEgfPoint*> points;
};

class CEgfAttr
{
public:
    CEgfAttr() :type(EGF_ELE_META_TYPE_none) {};
    ~CEgfAttr()
    {
        switch (type)
        {
        case EGF_ELE_META_TYPE_intArray:
            delete[] ia;
            break;
		case EGF_ELE_META_TYPE_shortArray:
			delete[] sa;
			break;
        case EGF_ELE_META_TYPE_string:
            delete[] s;
            break;
        case EGF_ELE_META_TYPE_point:
            delete point;
            break;
        case EGF_ELE_META_TYPE_points:
            delete points;
            break;
        case EGF_ELE_META_TYPE_mer:
            delete mer;
            break;
        }
    };

public:
    EGF_ELE_META_TYPE type;
    union
    {
        bool b;
        int i;
        int *ia;
		short *sa;
        char *s;
        CEgfPoint *point;
        CEgfPointArray *points;
        CEgfMer *mer;
    };


};

class CEgfObj
{
public:
    CEgfObj();
    CEgfObj(int objType, int objDataType, int objSize);
    ~CEgfObj();

    virtual bool Parse(std::ifstream &ifs, int sequenceId);
    virtual void Search(int objMetaId, CSearchCriteria *pPairs, std::vector<CEgfObj*> *pList);

public:
    int metaId;
    bool hasOffset;

public:
    int mObjType;
    int mObjDataType;
    int mObjSize;

    int mSequenceId;

    CEgfObj *mParent;

    // internal use
    CEgfPoint mOffset;

    std::vector<CEgfAttr*> attr;

    std::vector<CEgfObj*> children;

public:

    CEgfAttr* FindAttr(wchar_t *name, EGF_ELE_META_TYPE type);
    CEgfMeta* GetMeta() { return gEgfMetaList[metaId]; };
    std::map<int, int>* GetAttrDisplayOrderMap();
    wchar_t* GetAttrName(int column);
    EGF_ELE_META_TYPE GetAttrType(int column);
    bool GetDisplayHex(int column);
    wchar_t* GetMetaName();
    int GetAttrNumOfEles(int column);
    KIDS_MODE GetKidsMode();
    bool GetHasOffset();

    void PushChild(CEgfObj* pObj);

private:
    bool ParseArgs(std::ifstream &ifs);
    bool ParseChildren(std::ifstream &ifs, CEgfObj* pParent);

public:
    static CEgfObj* CreateEgfObj(int objType, int objDataType, int objSize);

    static int M16_ToInt(char *data);
    static int M32_ToInt(char *data);

    static int Egf32_ToInt(char *data);

    static bool ReadObjectFlags(std::ifstream &ifs, bool &visible, bool &mask);
    static bool Read16BitArray(std::ifstream &ifs, int count, short *value);
    static bool Read32BitArray(std::ifstream &ifs, int count, int *value);
    static bool ReadMer(std::ifstream &ifs, CEgfMer &mer);
    static bool ReadPoints(std::ifstream &ifs, int count, CEgfPoint *pts);

    static bool ReadByte(std::ifstream &ifs, char &value);
    static bool ReadShort(std::ifstream &ifs, short &value);
    static bool ReadInt(std::ifstream &ifs, int &value);
    static bool ReadString(std::ifstream &ifs, int count, char *buf);
    static bool LoadObjHdr(std::ifstream &ifs, int &objType, int &objDataType, int &objSize);

    static bool CheckCondition(CSearchEle *pCondition, void *pValue);
    static bool CheckColorCondition(CSearchEle *pCondition, char *pValue);
    static bool CheckRGBAColorCondition(CSearchEle *pCondition, short *pValue);
};

class CEgfSearchMetaEle
{
public:
    CEgfSearchMetaEle()
        :id(0), ctrlId(0), name(0)
    {
    };
    ~CEgfSearchMetaEle()
    {
        if (name) delete[] name;
    }

public:
    int id;
    int ctrlId;
    wchar_t *name;
};

class CEgfSearchMeta
{
public:
    CEgfSearchMeta()
    {
    };
    ~CEgfSearchMeta()
    {
        for (int i = 0; i < elements.size(); i++)
            delete elements[i];

        elements.clear();
    }

public:
    int id;

    std::vector<CEgfSearchMetaEle*> elements;
};