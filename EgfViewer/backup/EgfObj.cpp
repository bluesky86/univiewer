#include "stdafx.h"
#include "EgfObj.h"

CEgfMetaList gEgfMetaList;
std::map<int, int> gEgfIdToIndexMap;
CEgfSearchMetaList gEgfSearchMetaList;

CEgfObj::CEgfObj()
    : mObjType(0), mObjDataType(0), mObjSize(0), hasOffset(false), mParent(0), metaId(0), mSequenceId(0)
{
}

CEgfObj::CEgfObj(int objType, int objDataType, int objSize)
    : mObjType(objType), mObjDataType(objDataType), mObjSize(objSize), hasOffset(false), mParent(0), mSequenceId(0)
{
    metaId = gEgfIdToIndexMap[objType];
}

CEgfObj::~CEgfObj()
{
    for (int i = 0; i < attr.size(); i++)
        delete attr[i];

    attr.clear();

    for (int i = 0; i < children.size(); i++)
        delete children[i];

    children.clear();
}

int CEgfObj::M16_ToInt(char *data)
{
    char outdata[2];
    outdata[0] = *(data + 1);
    outdata[1] = *(data);

    return (int)(*(short*)outdata);
}

int CEgfObj::M32_ToInt(char *data)
{
    char outdata[4];
    outdata[0] = *(data + 3);
    outdata[1] = *(data + 2);
    outdata[2] = *(data + 1);
    outdata[3] = *(data);

    return *(int*)outdata;
}

int CEgfObj::Egf32_ToInt(char *data)
{
    return *(int*)data;
}

bool CEgfObj::ReadObjectFlags(std::ifstream &ifs, bool &visible, bool &mask)
{
    char buf;
    if (!ReadByte(ifs, buf)) return false;
    visible = buf > 0;
    if (!ReadByte(ifs, buf)) return false;
    mask = buf > 0;
    return true;
}

bool CEgfObj::Read16BitArray(std::ifstream &ifs, int count, short *value)
{
    char buf[2];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 2);
        if (ifs.gcount() != 2)
            return false;
        value[i] = *(short*)buf;
    }

    return true;
}

bool CEgfObj::Read32BitArray(std::ifstream &ifs, int count, int *value)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        value[i] = *(int*)buf;
    }

    return true;
}

bool CEgfObj::ReadMer(std::ifstream &ifs, CEgfMer &mer)
{
    int value[4];
    if (!Read32BitArray(ifs, 4, value)) return false;
    mer.x1 = (float)value[0];
    mer.y1 = (float)value[1];
    mer.x2 = (float)value[2];
    mer.y2 = (float)value[3];

    return true;
}

bool CEgfObj::ReadPoints(std::ifstream &ifs, int count, CEgfPoint *pts)
{
    char buf[4];

    for (int i = 0; i < count; i++)
    {
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].x = (float)*(int*)buf;
        ifs.read(buf, 4);
        if (ifs.gcount() != 4)
            return false;
        pts[i].y = (float)*(int*)buf;
    }
    return true;
}

bool CEgfObj::ReadByte(std::ifstream &ifs, char &value)
{
    char buf[1];
    ifs.read(buf, 1);
    if (ifs.gcount() != 1)
        return false;
    value = (BYTE)buf[0];
    return true;
}

bool CEgfObj::ReadShort(std::ifstream &ifs, short &value)
{
    char buf[2];
    ifs.read(buf, 2);
    if (ifs.gcount() != 2)
        return false;
    value = *(short*)buf;
    return true;
}

bool CEgfObj::ReadInt(std::ifstream &ifs, int &value)
{
    char buf[4];
    ifs.read(buf, 4);
    if (ifs.gcount() != 4)
        return false;
    value = *(int*)buf;
    return true;
}

bool CEgfObj::ReadString(std::ifstream &ifs, int count, char *buf)
{
    ifs.read(buf, count);
    if (ifs.gcount() != count)
        return false;
    return true;
}

bool CEgfObj::LoadObjHdr(std::ifstream &ifs, int &objType, int &objDataType, int &objSize)
{
    // offset (100) - type
    if (!ReadInt(ifs, objType)) return false;

    // objDataType
    if (!ReadInt(ifs, objDataType)) return false;

    // Object size
    if (!ReadInt(ifs, objSize)) return false;

    return true;
}

bool CEgfObj::CheckCondition(CSearchEle *pCondition, void *pValue)
{
    if (pCondition == NULL) return false;

    switch (pCondition->valueType)
    {
    case EGFSVT_bool:
        if (pCondition->b == (*(bool*)pValue))
            return true;
        break;
    case EGFSVT_int:
        if (pCondition->i == (*(int*)pValue))
            return true;
        break;
    case EGFSVT_uint:
        if (pCondition->u == (*(unsigned int*)pValue))
            return true;
        break;
    case EGFSVT_float:
        if (pCondition->f == (*(float*)pValue))
            return true;
        break;
    case EGFSVT_string:
        if (CString(pCondition->s) == CString((*(wchar_t*)pValue)))
            return true;
        break;
    default:
        return false;
    }


    return false;
}

bool CEgfObj::CheckColorCondition(CSearchEle *pCondition, char *pValue)
{
    if (pCondition->valueType != EGFSVT_uint) return false;

    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000) >> 16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF));
}

bool CEgfObj::CheckRGBAColorCondition(CSearchEle *pCondition, short *pValue)
{
    return ((BYTE)pValue[0] == ((pCondition->u & 0xFF0000) >> 16)
        && (BYTE)pValue[1] == ((pCondition->u & 0xFF00) >> 8)
        && (BYTE)pValue[2] == (pCondition->u & 0xFF)
        && (BYTE)pValue[3] == ((pCondition->u & 0xFF000000) >> 24));
}

CEgfObj* CEgfObj::CreateEgfObj(int objType, int objDataType, int objSize)
{
    CEgfObj *pObj = new CEgfObj(objType, objDataType, objSize);

    return pObj;
}

CEgfAttr* CEgfObj::FindAttr(wchar_t *name, EGF_ELE_META_TYPE type)
{
    if (name == NULL && type == EGF_ELE_META_TYPE_none)
        return NULL;

    CEgfMeta *pMeta = GetMeta();
    if (name)
    {
        if (type != EGF_ELE_META_TYPE_none)
        {
            for (int i = 0; i < pMeta->elements.size(); i++)
            {
                if (type == pMeta->elements[i]->type && wcscmp(name, pMeta->elements[i]->name) == 0)
                    return attr[i];
            }
        }
        else
        {
            for (int i = 0; i < pMeta->elements.size(); i++)
            {
                if (wcscmp(name, pMeta->elements[i]->name) == 0)
                    return attr[i];
            }
        }

    }
    else
    {
        for (int i = 0; i < pMeta->elements.size(); i++)
        {
            if (type == pMeta->elements[i]->type)
                return attr[i];
        }
    }


    return NULL;
}

std::map<int, int>* CEgfObj::GetAttrDisplayOrderMap()
{
    return &GetMeta()->displayOrderMap;
}

wchar_t* CEgfObj::GetAttrName(int column)
{
    return GetMeta()->elements[column]->name;
}

EGF_ELE_META_TYPE CEgfObj::GetAttrType(int column)
{
    return GetMeta()->elements[column]->type;
}

bool CEgfObj::GetDisplayHex(int column)
{
    return GetMeta()->elements[column]->displayHex;
}

wchar_t* CEgfObj::GetMetaName()
{
    return GetMeta()->name;
}

int CEgfObj::GetAttrNumOfEles(int column)
{
    CEgfMeta *pMeta = GetMeta();
    if (pMeta->elements[column]->num_id != -1)
        return attr[pMeta->elements[column]->num_id]->i;

    return pMeta->elements[column]->num;
}

KIDS_MODE CEgfObj::GetKidsMode()
{
    return GetMeta()->kidsMode;
}

bool CEgfObj::GetHasOffset()
{
    return GetMeta()->hasOffset;
}

void CEgfObj::PushChild(CEgfObj* pObj)
{
    if (pObj)
    {
        children.push_back(pObj);
        pObj->mParent = this;
    }
}

bool CEgfObj::ParseArgs(std::ifstream &ifs)
{
    char buf[4];
    CEgfMeta *pMeta = GetMeta();
    for (int i = 0; i < pMeta->elements.size(); i++)
    {
        CEgfAttr *pAttr = new CEgfAttr();
        pAttr->type = pMeta->elements[i]->type;
        switch (pMeta->elements[i]->type)
        {
        case EGF_ELE_META_TYPE_8bool:
            ReadByte(ifs, buf[0]);
            pAttr->b = (buf[0] != 0);
            break;
        case EGF_ELE_META_TYPE_intBool:
            ifs.read(buf, 4);
            pAttr->b = (*(int*)buf != 0);
            break;
        case EGF_ELE_META_TYPE_int:
        case EGF_ELE_META_TYPE_lineStyle:
        case EGF_ELE_META_TYPE_capStyle:
        case EGF_ELE_META_TYPE_operation:
        case EGF_ELE_META_TYPE_fillStyle:
        case EGF_ELE_META_TYPE_hatchStyle:
        case EGF_ELE_META_TYPE_shadingStyle:
        case EGF_ELE_META_TYPE_joinStyle:
        case EGF_ELE_META_TYPE_curveStyle:
        case EGF_ELE_META_TYPE_effectType:
            ifs.read(buf, 4);
            pAttr->i = *(int*)buf;
            break;
        case EGF_ELE_META_TYPE_color3:
            ifs.read((char*)&pAttr->i, 3);
            break;
        case EGF_ELE_META_TYPE_color4:
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[0] = (unsigned char)*(short*)buf;
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[1] = (unsigned char)*(short*)buf;
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[2] = (unsigned char)*(short*)buf;
            ifs.read(buf, 2);
            ((unsigned char*)&pAttr->i)[3] = (unsigned char)*(short*)buf;
            break;
        case EGF_ELE_META_TYPE_point:
            pAttr->point = new CEgfPoint;
            ReadPoints(ifs, 1, pAttr->point);
            break;
        case EGF_ELE_META_TYPE_points:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->points = new CEgfPointArray;
                    for (int j = 0; j < num; j++)
                    {
                        CEgfPoint *pPoint = new CEgfPoint;
                        ReadPoints(ifs, 1, pPoint);
                        pAttr->points->points.push_back(pPoint);
                    }
                }
            }
            break;
        case EGF_ELE_META_TYPE_mer:
            pAttr->mer = new CEgfMer;
            ReadMer(ifs, *pAttr->mer);
            break;
        case EGF_ELE_META_TYPE_intArray:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
					pAttr->ia = new int[num];
                    Read32BitArray(ifs, GetAttrNumOfEles(i), pAttr->ia);
                }
            }
            break;
		case EGF_ELE_META_TYPE_shortArray:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
					pAttr->sa = new short[num];
					Read16BitArray(ifs, GetAttrNumOfEles(i), pAttr->sa);
                }
            }
            break;
        case EGF_ELE_META_TYPE_string:
            {
                int num = GetAttrNumOfEles(i);
                if (num > 0)
                {
                    pAttr->s = new char[num + 2];
                    ifs.read(pAttr->s, num);
                    pAttr->s[num] = pAttr->s[num + 1] = 0;
                }
            }
            break;
        case EGF_ELE_META_TYPE_none:
        default:
            AfxMessageBox(_T("Unknown data type when paring file"));
            return false;
        }

        attr.push_back(pAttr);
    }

    return true;
}

bool CEgfObj::ParseChildren(std::ifstream &ifs, CEgfObj* pParent)
{
    int objType = 0, objDataType = 0, objSize = 0;
    int sequenceId = 0;
    bool childrenStart = false;

    // parse children
    while (true)
    {
        if (!LoadObjHdr(ifs, objType, objDataType, objSize))
            break;

        if (childrenStart == false && objDataType != SDT_OBJ_CHILDREN_START)
            return false;
        if (childrenStart == false && objDataType == SDT_OBJ_CHILDREN_START)
        {
            childrenStart = true;
            continue;
        }
        if (childrenStart == true && objDataType == SDT_OBJ_CHILDREN_END)
            break;

        CEgfObj *pObj = CreateEgfObj(objType, objDataType, objSize);
        if (!pObj->Parse(ifs, sequenceId++))
        {
            PushChild(pObj);
            return false;
        }

        PushChild(pObj);
    }

    return true;
}

bool CEgfObj::Parse(std::ifstream &ifs, int sequenceId)
{
    int objType = 0, objDataType = 0, objSize = 0;

    mSequenceId = sequenceId;

    // parse children
    switch (GetKidsMode())
    {
    case KIDS_MODE_Group:
        if (!LoadObjHdr(ifs, objType, objDataType, objSize))
            return false;

        if (!ParseArgs(ifs))
            return false;

        return ParseChildren(ifs, this);

    case KIDS_MODE_Path:
        if (!ParseArgs(ifs))
            return false;

        return ParseChildren(ifs, this);

    case KIDS_MODE_SText:
        {
            if (!ParseArgs(ifs))
                return false;

            if (GetHasOffset())
            {
                mOffset.x = attr[attr.size() - 1]->point->x;
                mOffset.y = attr[attr.size() - 1]->point->y;
            }
            if (mObjSize == 50)
                return true;

            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;

            CEgfObj *pObj = CreateEgfObj(objType, objDataType, objSize);
            if (!pObj->Parse(ifs, 0))
            {
                PushChild(pObj);
                return false;
            }

            PushChild(pObj);
            return true;
    }
    case KIDS_MODE_Ext:
        if (mObjSize > 0)
        {
            if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                return false;

            CEgfObj *pObj = CreateEgfObj(objType, objDataType, objSize);
            if (!pObj->Parse(ifs, 0))
            {
                PushChild(pObj);
                return false;
            }

            PushChild(pObj);

            if (objSize < mObjSize - 12) // 12 is header size
            {
                if (!LoadObjHdr(ifs, objType, objDataType, objSize))
                    return false;

                CEgfObj *pObj1 = CreateEgfObj(objType, objDataType, objSize);
                if (!pObj1->Parse(ifs, 1))
                {
                    PushChild(pObj1);
                    return false;
                }

                PushChild(pObj1);
            }

            return true;
        }
        break;
    case KIDS_MODE_NONE:
    default:
        return ParseArgs(ifs);
    }

    return true;
}

void CEgfObj::Search(int objMetaId, CSearchCriteria *pPairs, std::vector<CEgfObj*> *pList)
{
    bool met = true;

    CEgfMeta *pMeta = GetMeta();

    if (pMeta->id == objMetaId)
    {
        for (int i = 0; i < pPairs->pairs.size(); i++)
        {
            int column = pPairs->pairs[i]->argId;
            switch (pMeta->elements[column]->type)
            {
            case EGF_ELE_META_TYPE_8bool:
            case EGF_ELE_META_TYPE_intBool:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->b);
                break;
            case EGF_ELE_META_TYPE_int:
            case EGF_ELE_META_TYPE_lineStyle:
            case EGF_ELE_META_TYPE_capStyle:
            case EGF_ELE_META_TYPE_operation:
            case EGF_ELE_META_TYPE_fillStyle:
            case EGF_ELE_META_TYPE_hatchStyle:
            case EGF_ELE_META_TYPE_shadingStyle:
            case EGF_ELE_META_TYPE_joinStyle:
            case EGF_ELE_META_TYPE_curveStyle:
            case EGF_ELE_META_TYPE_effectType:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->i);
                break;
            case EGF_ELE_META_TYPE_color3:
                met = CheckColorCondition(pPairs->pairs[i], (char*)&attr[column]->i);
                break;
            case EGF_ELE_META_TYPE_color4:
                {
                    short value[4];
                    value[0] = (attr[column]->i >> 24);
                    value[1] = (attr[column]->i & 0xFF0000) >> 16;
                    value[2] = (attr[column]->i & 0xFF00) >> 8;
                    value[3] = (attr[column]->i & 0xFF);
                    met = CheckRGBAColorCondition(pPairs->pairs[i], value);
                }
                break;
            case EGF_ELE_META_TYPE_point:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->point->x);
                break;
            case EGF_ELE_META_TYPE_point + 1:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->point->y);
                break;
            case EGF_ELE_META_TYPE_mer:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->x1);
                break;
            case EGF_ELE_META_TYPE_mer + 1:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->y1);
                break;
            case EGF_ELE_META_TYPE_mer + 2:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->x2);
                break;
            case EGF_ELE_META_TYPE_mer + 3:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->mer->y2);
                break;
            case EGF_ELE_META_TYPE_string:
                met = CheckCondition(pPairs->pairs[i], &attr[column]->s);
                break;
            case EGF_ELE_META_TYPE_intArray:
			case EGF_ELE_META_TYPE_shortArray:
			default:
                break;
            }

            if (met == false)
                break;
        }

        if (met)
            pList->push_back(this);
    }

    // check children
    for (int i = 0; i < children.size(); i++)
        children[i]->Search(objMetaId, pPairs, pList);
}
