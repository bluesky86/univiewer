#pragma once
#include "EgfObj.h"

class CEgfFile
{
public:
    CEgfFile();
    ~CEgfFile();

    bool Parse(std::ifstream &ifs);

    void Search(int objMetaId, CSearchCriteria *pPairs, std::vector<CEgfObj*> *pList);

public:
    CEgfObj *m_pChild;

private:
    void Clear();
};

