#pragma once
#include "afxcmn.h"
#include "TabBase.h"
#include "EgfObj.h"
#include <vector>

class CMyTabCtrl : public CTabCtrl
{
    // Construction
public:
    CMyTabCtrl();
    std::vector<CTabBase*> m_tabPages;
    int m_tabCurrent;
    int m_nNumberOfPages;

    // Attributes
public:

    // Operations
public:
    void SetRectangle();

    int CreateTab(int index, wchar_t *tabName, int DlgId);
    void ShowTab(int index);

    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMyTabCtrl)
    //}}AFX_VIRTUAL

    // Implementation
public:
    virtual ~CMyTabCtrl();

    // Generated message map functions
protected:
    //{{AFX_MSG(CMyTabCtrl)
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    //}}AFX_MSG

    DECLARE_MESSAGE_MAP()
public:

    afx_msg void OnSize(UINT nType, int cx, int cy);
};

