#pragma once
#include "EgfObj.h"
#include "resource.h"
#include <Vector>
#include "afxwin.h"
#include "DebugNew.h"

#define CTRL_LEFT_MARGIN           3
#define CTRL_TOP_MARGIN            5
#define CTRL_HORI_SPACE            15
#define CTRL_VERT_SPACE            10
#define CTRL_DEFAULT_HEIGHT        17

static int gControlIndex = 20000;

class CTabBase : public CDialog
{
    DECLARE_DYNAMIC(CTabBase)

public:
    CTabBase(CWnd* pParent = NULL);
    CTabBase(int idd, CWnd* pParent);
    virtual ~CTabBase();

    virtual void DoDataExchange(CDataExchange* pDX);

    virtual int BuildCondition( CSearchCriteria *pPairs);

    static void CreateBoolEle(CSearchCriteria *pPairs, int id, CComboBox *cb)
    {
        if (cb == 0 || cb->GetCurSel() == 0) // N/A
            return;

        CSearchEle *pEle = NEW CSearchEle();

        pEle->argId = id;
        pEle->op = EGFSOP_EQUAL;
        pEle->valueType = EGFSVT_bool;
        if (cb->GetCurSel() == 1) // true
            pEle->b = true;
        else
            pEle->b = false;

        pPairs->pairs.push_back(pEle);
    }

    static void CreateIntEle(CSearchCriteria *pPairs, int id, EGF_SEACH_OP op, CEdit *edit)
    {
        if (edit == NULL) return;

        CString csValue;
        edit->GetWindowTextW(csValue);

        if (csValue.IsEmpty())
            return;

        CSearchEle *pEle = NEW CSearchEle();

        pEle->argId = id;
        pEle->op = op;
        pEle->valueType = EGFSVT_int;

        pEle->i = _wtoi(csValue);

        pPairs->pairs.push_back(pEle);
    }

    static void CreateUIntEle(CSearchCriteria *pPairs, int id, EGF_SEACH_OP op, CEdit *edit)
    {
        if (edit == NULL) return;

        CString csValue;
        edit->GetWindowTextW(csValue);

        if (csValue.IsEmpty())
            return;

        CSearchEle *pEle = NEW CSearchEle();

        pEle->argId = id;
        pEle->op = op;
        pEle->valueType = EGFSVT_uint;

        pEle->u = _wtoi(csValue);

        pPairs->pairs.push_back(pEle);
    }

    static void CreateFloatEle(CSearchCriteria *pPairs, int id, EGF_SEACH_OP op, CEdit *edit)
    {
        if (edit == NULL) return;

        CString csValue;
        edit->GetWindowTextW(csValue);

        if (csValue.IsEmpty())
            return;

        CSearchEle *pEle = NEW CSearchEle();

        pEle->argId = id;
        pEle->op = op;
        pEle->valueType = EGFSVT_float;

        pEle->f = (float)_wtof(csValue);

        pPairs->pairs.push_back(pEle);
    }

    static void CreateStringEle(CSearchCriteria *pPairs, int id, CEdit *edit)
    {
        if (edit == NULL) return;

        CString csValue;
        edit->GetWindowTextW(csValue);

        if (csValue.IsEmpty())
            return;

        CSearchEle *pEle = NEW CSearchEle();

        pEle->argId = id;
        pEle->op = EGFSOP_EQUAL;
        pEle->valueType = EGFSVT_string;

        pEle->s = NEW wchar_t[csValue.GetLength() + sizeof(wchar_t)];
        StrCpyW(pEle->s, csValue);

        pPairs->pairs.push_back(pEle);
    }

    static void CreateColorEle(CSearchCriteria *pPairs, int id, CEdit *editR, CEdit *editG, CEdit *editB)
    {
        if (editR == NULL || editG == NULL || editB == NULL ) return;

        CString csR, csG, csB;
        editR->GetWindowTextW(csR);
        editG->GetWindowTextW(csG);
        editB->GetWindowTextW(csB);

        if (csR.IsEmpty() && csG.IsEmpty() && csB.IsEmpty())
            return;

        int r, g, b, color = 0;

        if (csR.IsEmpty())
            r = 0;
        else
            r = _wtoi(csR);

        if (csG.IsEmpty())
            g = 0;
        else
            g = _wtoi(csG);

        if (csB.IsEmpty())
            b = 0;
        else
            b = _wtoi(csB);

        color |= (r << 16);
        color |= (g << 8);
        color |= b;

        CSearchEle *pEle = NEW CSearchEle();

        pEle->argId = id;
        pEle->op = EGFSOP_EQUAL;
        pEle->valueType = EGFSVT_uint;
        pEle->u = color;

        pPairs->pairs.push_back(pEle);
    }

    static void CreateColorEle(CSearchCriteria *pPairs, int id, CEdit *editR, CEdit *editG, CEdit *editB, CEdit *editA)
    {
        if (editR == NULL || editG == NULL || editB == NULL || editA == NULL) return;

        CString csR, csG, csB, csA;
        editR->GetWindowTextW(csR);
        editG->GetWindowTextW(csG);
        editB->GetWindowTextW(csB);
        editA->GetWindowTextW(csA);

        if (csR.IsEmpty() && csG.IsEmpty() && csB.IsEmpty() && csA.IsEmpty())
            return;

        int r, g, b, a, color = 0;

        if (csR.IsEmpty())
            r = 0;
        else
            r = _wtoi(csR);

        if (csG.IsEmpty())
            g = 0;
        else
            g = _wtoi(csG);

        if (csB.IsEmpty())
            b = 0;
        else
            b = _wtoi(csB);

        if (csA.IsEmpty())
            a = 0;
        else
            a = _wtoi(csA);

        color |= (a << 24);
        color |= (r << 16);
        color |= (g << 8);
        color |= b;

        CSearchEle *pEle = NEW CSearchEle();

        pEle->argId = id;
        pEle->op = EGFSOP_EQUAL;
        pEle->valueType = EGFSVT_uint;
        pEle->u = color;

        pPairs->pairs.push_back(pEle);
    }

    static void CreateMerEle(CSearchCriteria *pPairs, int idx1, int idy1, int idx2, int idy2, CEdit *editX1, CEdit *editY1, CEdit *editX2, CEdit *editY2)
    {
        if (editX1 == NULL || editY1 == NULL || editX2 == NULL || editY2 == NULL) return;

        CString csX1, csY1, csX2, csY2;
        editX1->GetWindowTextW(csX1);
        editY1->GetWindowTextW(csY1);
        editX2->GetWindowTextW(csX2);
        editY2->GetWindowTextW(csY2);

        if (csX1.IsEmpty() && csY1.IsEmpty() && csX2.IsEmpty() && csY2.IsEmpty())
            return;

        if (!csX1.IsEmpty())
        {
            CSearchEle *pEle = NEW CSearchEle();
            pEle->argId = idx1;
            pEle->op = EGFSOP_EQUAL;
            pEle->valueType = EGFSVT_float;
            pEle->f = (float)_wtof(csX1);
            pPairs->pairs.push_back(pEle);
        }
        if (!csX2.IsEmpty())
        {
            CSearchEle *pEle = NEW CSearchEle();
            pEle->argId = idx2;
            pEle->op = EGFSOP_EQUAL;
            pEle->valueType = EGFSVT_float;
            pEle->f = (float)_wtof(csX2);
            pPairs->pairs.push_back(pEle);
        }
        if (!csY1.IsEmpty())
        {
            CSearchEle *pEle = NEW CSearchEle();
            pEle->argId = idy1;
            pEle->op = EGFSOP_EQUAL;
            pEle->valueType = EGFSVT_float;
            pEle->f = (float)_wtof(csY1);
            pPairs->pairs.push_back(pEle);
        }
        if (!csY2.IsEmpty())
        {
            CSearchEle *pEle = NEW CSearchEle();
            pEle->argId = idy2;
            pEle->op = EGFSOP_EQUAL;
            pEle->valueType = EGFSVT_float;
            pEle->f = (float)_wtof(csY2);
            pPairs->pairs.push_back(pEle);
        }
    }
    
    static void CreatePointEle(CSearchCriteria *pPairs, int idx, int idy, CEdit *editX, CEdit *editY)
    {
        if (editX == NULL || editY == NULL) return;

        CString csX, csY;
        editX->GetWindowTextW(csX);
        editY->GetWindowTextW(csY);

        if (csX.IsEmpty() && csY.IsEmpty())
            return;

        if (!csX.IsEmpty())
        {
            CSearchEle *pEle = NEW CSearchEle();
            pEle->argId = idx;
            pEle->op = EGFSOP_EQUAL;
            pEle->valueType = EGFSVT_float;
            pEle->f = (float)_wtof(csX);
            pPairs->pairs.push_back(pEle);
        }
        if (!csY.IsEmpty())
        {
            CSearchEle *pEle = NEW CSearchEle();
            pEle->argId = idy;
            pEle->op = EGFSOP_EQUAL;
            pEle->valueType = EGFSVT_float;
            pEle->f = (float)_wtof(csY);
            pPairs->pairs.push_back(pEle);
        }
    }

    void Clear();

    CStatic* CreateStatic(int width, int height, CString &title);
    CComboBox* CreateComboBox(int id, int width, int height);
    //CComboBox* CreateComboBox(int id, int width, int height, CString &name);
    CEdit* CreateEdit(int id, int width, int height);
    bool CreateMer(int idx1, int idy1, int idx2, int idy2, CString &name);
    bool CreatePoint(int idx, int idy, CString &name);
    bool CreateColor(int idr, int idg, int idb, CString &name);
    bool CreateColor(int idr, int idg, int idb, int ida, CString &name);

    CStatic* CreateStatic(int x, int y, int width, int height, CString &title);
    CComboBox* CreateComboBox(int id, int x, int y, int width, int height);
    CEdit* CreateEdit(int id, int x, int y, int width, int height);

    void ResetScrollBar(int pos = 0);

public:
    enum { IDD = IDD_DLG_TAB_BASE };

    // dialog size as you see in the resource view (original size)
    CRect	m_rcOriginalRect;
    // actual scroll position
    int		m_nScrollPos;
    // actual dialog height
    int		m_nCurHeight;

private:
    CFont m_nonCleartypeFont;

public:
    int m_nSearchMetaId;
    std::vector<CStatic*> m_pStatic;
    std::vector<CComboBox*> m_pComboBox;
    std::vector<CEdit*> m_pEdit;
    std::vector<CButton*> m_pBtn;
    int xPos, yPos;

    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

    DECLARE_MESSAGE_MAP()
    virtual BOOL OnInitDialog();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
private:
    CStatic m_staticBase;
};