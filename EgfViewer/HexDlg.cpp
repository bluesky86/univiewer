// HexDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EgfViewer.h"
#include "HexDlg.h"
#include "afxdialogex.h"


// CHexDlg dialog

IMPLEMENT_DYNAMIC(CHexDlg, CDialogEx)

CHexDlg::CHexDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHexDlg::IDD, pParent)
{
    mStrLen = 0;
    m_pStr = NULL;

}

CHexDlg::~CHexDlg()
{
}

void CHexDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT_HEX_STRING, m_editHex);
}


BEGIN_MESSAGE_MAP(CHexDlg, CDialogEx)
END_MESSAGE_MAP()


// CHexDlg message handlers
void CHexDlg::SetHexString(int len, char *pStr)
{
    // convert into hex string
    CString cs;// (L"      ");
    CString subs(L"");
    int count = 0;
    subs = L"    \t0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F";
    subs += L"\r\n-----\t------------------------------------------------";
    for (int i = 0; i < len; i++)
    {
        if (/*i != 0 &&*/ (i % 16) == 0)
            cs.Format(_T("\r\n%04Xh\t%02X "), count++*16, (unsigned char)pStr[i]);
        else
            cs.Format(_T("%02X "), (unsigned char)pStr[i]);
        subs += cs;
    }
    m_editHex.SetWindowTextW(subs);
}


BOOL CHexDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // TODO:  Add extra initialization here
    if (mStrLen > 0)
        SetHexString(mStrLen, m_pStr);

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}
