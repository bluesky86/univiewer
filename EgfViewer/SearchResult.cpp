#include "stdafx.h"
#include "SearchResult.h"


CSearchResult::CSearchResult()
{
}


CSearchResult::~CSearchResult()
{
}

BEGIN_MESSAGE_MAP(CSearchResult, CDockablePane)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_CONTEXTMENU()
    ON_WM_PAINT()
    ON_WM_SETFOCUS()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchResult message handlers

int CSearchResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;

    CRect rectDummy;
    rectDummy.SetRectEmpty();

    // Create views:
    const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_VSCROLL | LVS_REPORT 
        | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;

    if (!m_wndListView.Create(dwViewStyle, rectDummy, this, 2))
    {
        TRACE0("Failed to create Class View\n");
        return -1;      // fail to create
    }

    const DWORD dwViewExStyle = LVS_EX_SIMPLESELECT | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_SINGLESEL;
    m_wndListView.SetExtendedStyle(dwViewExStyle);

    // Fill in some static tree view data (dummy code, nothing magic here)
    m_wndListView.InsertColumn(0, _T("ID"), LVCFMT_LEFT, 70);
    m_wndListView.InsertColumn(1, _T("Path"), LVCFMT_LEFT, 128);
    m_wndListView.InsertColumn(2, _T("Type"), LVCFMT_LEFT, 80);

    //m_wndListView.RemoveSortColumn(0);

    return 0;
}

void CSearchResult::OnSize(UINT nType, int cx, int cy)
{
    CDockablePane::OnSize(nType, cx, cy);
    AdjustLayout();
}

void CSearchResult::OnContextMenu(CWnd* pWnd, CPoint point)
{
}

void CSearchResult::OnPaint()
{
    CPaintDC dc(this); // device context for painting

    CRect rectTree;
    m_wndListView.GetWindowRect(rectTree);
    ScreenToClient(rectTree);

    rectTree.InflateRect(1, 1);
    dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

BOOL CSearchResult::PreTranslateMessage(MSG* pMsg)
{
    return CDockablePane::PreTranslateMessage(pMsg);
}

void CSearchResult::AdjustLayout()
{
    if (GetSafeHwnd() == NULL)
    {
        return;
    }

    CRect rectClient;
    GetClientRect(rectClient);

    int cyTlb = 0;// m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndListView.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CSearchResult::OnSetFocus(CWnd* pOldWnd)
{
    CDockablePane::OnSetFocus(pOldWnd);

    m_wndListView.SetFocus();
}

void CSearchResult::ShowList(CVector<CEgfObj*> *objList)
{
    ShowPane(TRUE, FALSE, TRUE);

    m_wndListView.DeleteAllItems();

    for (unsigned int i = 0; i < objList->size(); i++)
    {
        CEgfObj *pObj = (*objList)[i];
        CString id;
        CString path = _T("");
        do
        {
            if (path.IsEmpty())
                id.Format(_T("%d"), pObj->mSequenceId);
            else
                id.Format(_T("%d-"), pObj->mSequenceId);
            path = id + path;
            pObj = pObj->mParent;

        } while (pObj);

        id.Format(_T("%d"), i);
        m_wndListView.InsertItem(i, id);
        m_wndListView.SetItemText(i, 1, path);
        m_wndListView.SetItemText(i, 2, (*objList)[i]->GetMetaName());
        m_wndListView.SetItemData(i, (DWORD_PTR)(*objList)[i]);
    }
}

