// SearchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EgfViewer.h"
#include "SearchDlg.h"
#include "afxdialogex.h"
#include "Global.h"
#include "EgfObj.h"
#include "TabBase.h"
#include "EgfApi.h"

// CSearchDlg dialog

IMPLEMENT_DYNAMIC(CSearchDlg, CDialogEx)

CSearchDlg::CSearchDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSearchDlg::IDD, pParent)
{
    initialized = false;
}

CSearchDlg::~CSearchDlg()
{
}

void CSearchDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_TAB_OPTIONS, m_tabCtrl);
    DDX_Control(pDX, IDC_COMBO_SEARCH_VISIBILITY, m_cbVisibility);
    DDX_Control(pDX, IDC_COMBO_SEARCH_OBJ_IS_MASK, m_cbObjFlagsMask);
    DDX_Control(pDX, IDC_STATIC_SEARCH_DLG_VISIBILITY, m_staticVisibility);
}


BEGIN_MESSAGE_MAP(CSearchDlg, CDialogEx)
    ON_WM_SIZE()
    ON_BN_CLICKED(ID_SERACH_BUTTON, &CSearchDlg::OnBnClickedSerachButton)
END_MESSAGE_MAP()


// CSearchDlg message handlers


BOOL CSearchDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    CEgfSearchMetaList *pEgfSearchMetaList = GetEgfSearchMetaList();

    if (GetEgfMetaList()->size() == 0) return true;

    for (unsigned int i = 0; i < pEgfSearchMetaList->size(); i++)
    {
        CEgfMeta *pMeta = (*GetEgfMetaList())[(*pEgfSearchMetaList)[i]->id];
        int index = m_tabCtrl.CreateTab(i, pMeta->name, IDD_DLG_TAB_BASE);
        m_tabCtrl.m_tabPages[index]->m_nSearchMetaId = i;

        //m_tabCtrl.m_tabPages[index]->ResetScrollBar();
        //continue;

        for (unsigned int j = 0; j < (*pEgfSearchMetaList)[i]->elements.size(); j++)
        {
            switch (pMeta->elements[(*pEgfSearchMetaList)[i]->elements[j]->id]->type)
            {
            case EGF_ELE_META_TYPE_int:
            case EGF_ELE_META_TYPE_lineStyle:
            case EGF_ELE_META_TYPE_capStyle:
            case EGF_ELE_META_TYPE_operation:
            case EGF_ELE_META_TYPE_fillStyle:
            case EGF_ELE_META_TYPE_hatchStyle:
            case EGF_ELE_META_TYPE_shadingStyle:
            case EGF_ELE_META_TYPE_joinStyle:
            case EGF_ELE_META_TYPE_curveStyle:
            case EGF_ELE_META_TYPE_effectType:
                m_tabCtrl.m_tabPages[index]->CreateStatic(120, CTRL_DEFAULT_HEIGHT, CString((*pEgfSearchMetaList)[i]->elements[j]->name));
                m_tabCtrl.m_tabPages[index]->CreateEdit((*pEgfSearchMetaList)[i]->elements[j]->ctrlId, 80, CTRL_DEFAULT_HEIGHT);
                break;
            case EGF_ELE_META_TYPE_8bool:
            case EGF_ELE_META_TYPE_intBool:
                m_tabCtrl.m_tabPages[index]->CreateStatic(120, CTRL_DEFAULT_HEIGHT, CString((*pEgfSearchMetaList)[i]->elements[j]->name));
                InitBooleanComboBox(*m_tabCtrl.m_tabPages[index]->CreateComboBox((*pEgfSearchMetaList)[i]->elements[j]->ctrlId, 80, CTRL_DEFAULT_HEIGHT));
                break;
            case EGF_ELE_META_TYPE_color3:
                m_tabCtrl.m_tabPages[index]->CreateColor((*pEgfSearchMetaList)[i]->elements[j]->ctrlId, (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 1, (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 2,
                    CString((*pEgfSearchMetaList)[i]->elements[j]->name));
                break;
            case EGF_ELE_META_TYPE_color4:
                m_tabCtrl.m_tabPages[index]->CreateColor((*pEgfSearchMetaList)[i]->elements[j]->ctrlId, (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 1,
                    (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 2, (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 3,
                    CString((*pEgfSearchMetaList)[i]->elements[j]->name));
                break;
            case EGF_ELE_META_TYPE_point:
                m_tabCtrl.m_tabPages[index]->CreatePoint((*pEgfSearchMetaList)[i]->elements[j]->ctrlId, (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 1, CString((*pEgfSearchMetaList)[i]->elements[j]->name));
                break;
            case EGF_ELE_META_TYPE_mer:
                m_tabCtrl.m_tabPages[index]->CreateMer((*pEgfSearchMetaList)[i]->elements[j]->ctrlId, (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 1,
                    (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 2, (*pEgfSearchMetaList)[i]->elements[j]->ctrlId + 3, CString((*pEgfSearchMetaList)[i]->elements[j]->name));
                break;
            case EGF_ELE_META_TYPE_string:
                m_tabCtrl.m_tabPages[index]->CreateStatic(50, CTRL_DEFAULT_HEIGHT, CString((*pEgfSearchMetaList)[i]->elements[j]->name));
                m_tabCtrl.m_tabPages[index]->CreateEdit((*pEgfSearchMetaList)[i]->elements[j]->ctrlId, 188, CTRL_DEFAULT_HEIGHT);
                break;
            case EGF_ELE_META_TYPE_intArray:
			case EGF_ELE_META_TYPE_shortArray:
            case EGF_ELE_META_TYPE_points:
            case EGF_ELE_META_TYPE_none:
            default:
                break;
            }
        }

        m_tabCtrl.m_tabPages[index]->ResetScrollBar();
    }

    m_tabCtrl.SetCurSel(0);
    m_tabCtrl.SetRectangle();
    m_tabCtrl.ShowTab(0);


    // object flags
    InitBooleanComboBox(m_cbVisibility);
    InitBooleanComboBox(m_cbObjFlagsMask);

    // initialized
    initialized = true;

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}


void CSearchDlg::InitBooleanComboBox(CComboBox &cb)
{
    cb.InsertString(0, L"NA");
    cb.InsertString(1, L"true");
    cb.InsertString(2, L"false");
    cb.SetCurSel(0);
}


void CSearchDlg::OnSize(UINT nType, int cx, int cy)
{
    CDialogEx::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here
    if (initialized)
    {
        CRect rectClient;
        GetClientRect(rectClient);

        CRect rectVisibility;
        m_staticVisibility.GetWindowRect(&rectVisibility);
        ScreenToClient(&rectVisibility);
        m_tabCtrl.SetWindowPos(NULL, rectVisibility.left - 5, rectVisibility.bottom + 7, 
            rectClient.Width() - rectVisibility.left - 1, rectClient.Height() - rectVisibility.bottom - 10,
            SWP_NOACTIVATE | SWP_NOZORDER);
    }
}


void CSearchDlg::OnBnClickedSerachButton()
{
    // Build the searching criteria
    CSearchCriteria *pPairs = NEW CSearchCriteria();
    CTabBase::CreateBoolEle(pPairs, GLOBAL_ARGS_ID_mVisible, &m_cbVisibility);
    CTabBase::CreateBoolEle(pPairs, GLOBAL_ARGS_ID_mMask, &m_cbObjFlagsMask);

    CTabBase* pTabBase = (CTabBase*)m_tabCtrl.m_tabPages[m_tabCtrl.m_tabCurrent];
    int objMetaId = pTabBase->BuildCondition(pPairs);

    // send the criteria to MainFrame
    AfxGetMainWnd()->PostMessageW(WM_SEARCH_EGF_OBJ, objMetaId, (LPARAM)pPairs);
}
