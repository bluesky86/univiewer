#include "stdafx.h"
#include "EgfArrow.h"
#include "Global.h"


CEgfArrow::CEgfArrow() : mVisible(0), mMask(0)
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfArrow::CEgfArrow(int objType, int objDataType, int objSize)
    : mVisible(0), mMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}

CEgfArrow::~CEgfArrow()
{
}

bool CEgfArrow::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mThinknessUnits
    ifs.read(buf, 4);
    mThinknessUnits = Egf32_ToInt(buf);
    // mThinknessValue
    ifs.read(buf, 4);
    mThinknessValue = Egf32_ToInt(buf);

    // mArrowHeadSizeUnits
    ifs.read(buf, 4);
    mArrowHeadSizeUnits = Egf32_ToInt(buf);
    // mArrowHeadSizeValue
    ifs.read(buf, 4);
    mArrowHeadSizeValue = Egf32_ToInt(buf);

    // mColor
    ifs.read(mColor, 3);

    // mLineStyle
    ifs.read(buf, 4);
    mLineStyle = Egf32_ToInt(buf);

    // mOperation
    ifs.read(buf, 4);
    mOperation = Egf32_ToInt(buf);

    // mCapStyle
    ifs.read(buf, 4);
    mCapStyle = Egf32_ToInt(buf);

    // mArrowsProp
    ifs.read(buf, 4);
    mArrowsProp = Egf32_ToInt(buf);

    // mArrowsSolid
    ifs.read(buf, 4);
    mArrowsSolid = Egf32_ToInt(buf);

    // mArrowsDualHeaded
    ifs.read(buf, 4);
    mArrowsDualHeaded = Egf32_ToInt(buf);

    // mPenNum
    ifs.read(buf, 4);
    mPenNum = Egf32_ToInt(buf);

    // mArrowsStyleStart
    ifs.read(buf, 4);
    mArrowsStyleStart = Egf32_ToInt(buf);

    // mArrowsStyleEnd
    ifs.read(buf, 4);
    mArrowsStyleEnd = Egf32_ToInt(buf);

    // points
    ReadPoints(ifs, 5, m_pPoints);

    return true;
}

void CEgfArrow::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_ARROW_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case ARROW_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case ARROW_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case ARROW_ARGS_ID_mThinknessUnits:
            met = CheckCondition(pPairs->pairs[i], &mThinknessUnits);
            break;
        case ARROW_ARGS_ID_mThinknessValue:
            met = CheckCondition(pPairs->pairs[i], &mThinknessValue);
            break;
        case ARROW_ARGS_ID_mArrowHeadSizeUnits:
            met = CheckCondition(pPairs->pairs[i], &mArrowHeadSizeUnits);
            break;
        case ARROW_ARGS_ID_mArrowHeadSizeValue:
            met = CheckCondition(pPairs->pairs[i], &mArrowHeadSizeValue);
            break;
        case ARROW_ARGS_ID_mColor:
            met = CheckColorCondition(pPairs->pairs[i], mColor);
            break;
        case ARROW_ARGS_ID_mLineStyle:
            met = CheckCondition(pPairs->pairs[i], &mLineStyle);
            break;
        case ARROW_ARGS_ID_mCapStyle:
            met = CheckCondition(pPairs->pairs[i], &mCapStyle);
            break;
        case ARROW_ARGS_ID_mOperation:
            met = CheckCondition(pPairs->pairs[i], &mOperation);
            break;
        case ARROW_ARGS_ID_mArrowsProp:
            met = CheckCondition(pPairs->pairs[i], &mArrowsProp);
            break;
        case ARROW_ARGS_ID_mArrowsSolid:
            met = CheckCondition(pPairs->pairs[i], &mArrowsSolid);
            break;
        case ARROW_ARGS_ID_mArrowsDualHeaded:
            met = CheckCondition(pPairs->pairs[i], &mArrowsDualHeaded);
            break;
        case ARROW_ARGS_ID_mArrowsStyleStart:
            met = CheckCondition(pPairs->pairs[i], &mArrowsStyleStart);
            break;
        case ARROW_ARGS_ID_mArrowsStyleEnd:
            met = CheckCondition(pPairs->pairs[i], &mArrowsStyleEnd);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}
