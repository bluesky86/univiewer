#pragma once
#include "MyTabCtrl.h"
#include "resource.h"
#include "afxwin.h"

// CSearchDlg dialog

class CSearchDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSearchDlg)

public:
	CSearchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSearchDlg();

    static void InitBooleanComboBox(CComboBox &cb);

// Dialog Data
	enum { IDD = IDD_DLG_SEARCH };

private:
    bool initialized;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    CMyTabCtrl m_tabCtrl;
    virtual BOOL OnInitDialog();
    CComboBox m_cbVisibility;
    CComboBox m_cbObjFlagsMask;
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnBnClickedSerachButton();
    CStatic m_staticVisibility;
};
