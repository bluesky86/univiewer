#include "stdafx.h"
#include "EgfPaste.h"
#include "Global.h"


CEgfPaste::CEgfPaste() : m_pStr(0), mVisible(0), mMask(0)
{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfPaste::CEgfPaste(int objType, int objDataType, int objSize)
    : m_pStr(0), mVisible(0), mMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}

CEgfPaste::~CEgfPaste()
{
    if (m_pStr)
        delete[] m_pStr;
}

bool CEgfPaste::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mColor
    ifs.read(mColor, 3);

    // mBkColor
    ifs.read(mBkColor, 3);

    // mOperation
    ifs.read(buf, 4);
    mOperation = Egf32_ToInt(buf);

    // mBkOperation
    ifs.read(buf, 4);
    mBkOperation = Egf32_ToInt(buf);

    // mUserRot
    ifs.read(buf, 4);
    mUserRot = Egf32_ToInt(buf);

    // mImgEffectType
    ifs.read(buf, 4);
    mImgEffectType = Egf32_ToInt(buf);

    // mColorOne
    ifs.read((char*)mColorOne, 8);
    //Read16BitArray(ifs, 4, mColorOne);

    // mColorTwo
    ifs.read((char*)mColorTwo, 8);
    //Read16BitArray(ifs, 4, mColorTwo);

    // mImageMask
    ifs.read(buf, 4);
    mImageMask = Egf32_ToInt(buf);

    // mAddColor
    ifs.read(mAddColor, 3);

    // mMaskType
    ifs.read(buf, 4);
    mMaskType = Egf32_ToInt(buf);

    // mMaskFlags
    ifs.read(buf, 4);
    mMaskFlags = Egf32_ToInt(buf);

    // mBasePasteObjType
    ifs.read(buf, 4);
    mBasePasteObjType = Egf32_ToInt(buf);

    // frame
    ReadMer(ifs, mFrame);

    // mDataRot
    ifs.read(buf, 4);
    mDataRot = Egf32_ToInt(buf);

    // mInNumbits
    ifs.read(buf, 4);
    mInNumbits = Egf32_ToInt(buf);

    // mInExtBits
    ifs.read(buf, 4);
    mInExtBits = Egf32_ToInt(buf);

    // mOutNumBits
    ifs.read(buf, 4);
    mOutNumBits = Egf32_ToInt(buf);

    // mTransparentType
    ifs.read(buf, 4);
    mTransparentType = Egf32_ToInt(buf);

    // mTransparentColor
    ifs.read(buf, 4);
    mTransparentColor = Egf32_ToInt(buf);

    // mNumcolours
    ifs.read(buf, 4);
    mNumcolours = Egf32_ToInt(buf);

    // mNumAlpha
    ifs.read(buf, 4);
    mNumAlpha = Egf32_ToInt(buf);

    return true;
}

bool CEgfPaste::ParseData(std::ifstream &ifs, int len)
{
    mStrLen = len;
    if (mStrLen > 0)
    {
        m_pStr = new char[mStrLen + 2];
        ifs.read(m_pStr, mStrLen);
    }

    return true;
}

void CEgfPaste::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_PASTE_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case PASTE_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case PASTE_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case PASTE_ARGS_ID_mColor:
            met = CheckColorCondition(pPairs->pairs[i], mColor);
            break;
        case PASTE_ARGS_ID_mBkColor:
            met = CheckColorCondition(pPairs->pairs[i], mBkColor);
            break;
        case PASTE_ARGS_ID_mOperation:
            met = CheckCondition(pPairs->pairs[i], &mOperation);
            break;
        case PASTE_ARGS_ID_mBkOperation:
            met = CheckCondition(pPairs->pairs[i], &mBkOperation);
            break;
        case PASTE_ARGS_ID_mUserRot:
            met = CheckCondition(pPairs->pairs[i], &mUserRot);
            break;
        case PASTE_ARGS_ID_mImgEffectType:
            met = CheckCondition(pPairs->pairs[i], &mImgEffectType);
            break;
        case PASTE_ARGS_ID_mColorOne:
            met = CheckRGBAColorCondition(pPairs->pairs[i], mColorOne);
            break;
        case PASTE_ARGS_ID_mColorTwo:
            met = CheckRGBAColorCondition(pPairs->pairs[i], mColorTwo);
            break;
        case PASTE_ARGS_ID_mImageMask:
            met = CheckCondition(pPairs->pairs[i], &mImageMask);
            break;
        case PASTE_ARGS_ID_mAddColor:
            met = CheckColorCondition(pPairs->pairs[i], mAddColor);
            break;
        case PASTE_ARGS_ID_mMaskType:
            met = CheckCondition(pPairs->pairs[i], &mMaskType);
            break;
        case PASTE_ARGS_ID_mMaskFlags:
            met = CheckCondition(pPairs->pairs[i], &mMaskFlags);
            break;
        case PASTE_ARGS_ID_mBasePasteObjType:
            met = CheckCondition(pPairs->pairs[i], &mBasePasteObjType);
            break;
        case PASTE_ARGS_ID_mFrame_x1:
            met = CheckCondition(pPairs->pairs[i], &mFrame.x1);
            break;
        case PASTE_ARGS_ID_mFrame_y1:
            met = CheckCondition(pPairs->pairs[i], &mFrame.y1);
            break;
        case PASTE_ARGS_ID_mFrame_x2:
            met = CheckCondition(pPairs->pairs[i], &mFrame.x2);
            break;
        case PASTE_ARGS_ID_mFrame_y2:
            met = CheckCondition(pPairs->pairs[i], &mFrame.y2);
            break;
        case PASTE_ARGS_ID_mDataRot:
            met = CheckCondition(pPairs->pairs[i], &mDataRot);
            break;
        case PASTE_ARGS_ID_mInNumbits:
            met = CheckCondition(pPairs->pairs[i], &mInNumbits);
            break;
        case PASTE_ARGS_ID_mInExtBits:
            met = CheckCondition(pPairs->pairs[i], &mInExtBits);
            break;
        case PASTE_ARGS_ID_mOutNumBits:
            met = CheckCondition(pPairs->pairs[i], &mOutNumBits);
            break;
        case PASTE_ARGS_ID_mTransparentType:
            met = CheckCondition(pPairs->pairs[i], &mTransparentType);
            break;
        case PASTE_ARGS_ID_mTransparentColor:
            met = CheckCondition(pPairs->pairs[i], &mTransparentColor);
            break;
        case PASTE_ARGS_ID_mNumcolours:
            met = CheckCondition(pPairs->pairs[i], &mNumcolours);
            break;
        case PASTE_ARGS_ID_mNumAlpha:
            met = CheckCondition(pPairs->pairs[i], &mNumAlpha);
            break;
        case PASTE_ARGS_ID_mStrLen:
            met = CheckCondition(pPairs->pairs[i], &mStrLen);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}
