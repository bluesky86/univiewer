#include "stdafx.h"
#include "EgfGroup.h"
#include "Global.h"

#include "EgfArc.h"
#include "EgfLine.h"
#include "EgfElips.h"
#include "EgfArrow.h"
#include "EgfPline.h"
#include "EgfPgon.h"
#include "EgfText.h"
#include "EgfPath.h"
#include "EgfAnnot.h"
#include "EgfPaste.h"
#include "EgfSText.h"
#include "EtxBase.h"
#include "EtxText.h"

CEgfGroup::CEgfGroup() : mVisible(0), mMask(0), mHasMask(0)

{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfGroup::CEgfGroup(int objType, int objDataType, int objSize)
    :mVisible(0), mMask(0), mHasMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}


CEgfGroup::~CEgfGroup()
{
    for (unsigned int i = 0; i < mChildren.size(); i++)
        delete mChildren[i];
    mChildren.clear();
}

void CEgfGroup::Append(CEgfObject* pObj)
{
    if (pObj)
    {
        mChildren.push_back(pObj);
        pObj->mParent = this;
        //pObj->mOffset.x += mOffset.x;
        //pObj->mOffset.y += mOffset.y;
    }
}

bool CEgfGroup::ParseArgs(std::ifstream &ifs)
{
    char buf[4];

    int objType = 0, objDataType = 0, objSize = 0;
    if (!LoadObjHdr(ifs, objType, objDataType, objSize) || objDataType != SDT_OBJ_ARGS)
        return false;

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // group flags
    ReadByte(ifs, buf[0]);
    mHasMask = ((buf[0])>0);
    ReadByte(ifs, buf[0]);
    mIsPasteObj = ((buf[0])>0);
    ReadByte(ifs, buf[0]);
    mMaskGroup = ((buf[0])>0);
    ReadByte(ifs, buf[0]);
    mMaskObject = ((buf[0])>0);
    ReadByte(ifs, buf[0]);
    mSourceObject = ((buf[0])>0);
    ReadByte(ifs, buf[0]);
    mRefObject = ((buf[0])>0);
    ReadByte(ifs, buf[0]);
    mTransparentGroup = ((buf[0])>0);

    // mTilingStep
    ifs.read(buf, 4);
    mTilingStep.x = (float)Egf32_ToInt(buf);
    ifs.read(buf, 4);
    mTilingStep.y = (float)Egf32_ToInt(buf);

    // mBackDropColor
    ifs.read(buf, 4);
    mBackDropColor = Egf32_ToInt(buf);

    // mMer
    ReadMer(ifs, mMer);


    return true;
}

bool CEgfGroup::ParseChild(std::ifstream &ifs, CEgfGroup* pParent, int objType, int objDataType, int objSize, int sequenceId)
{
    switch (objType)
    {
    case EGF_GROUP_TAG:
    {
        CEgfGroup *pChild = new CEgfGroup(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_LINE_TAG:
    {
        CEgfLine *pChild = new CEgfLine(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_ARC_TAG:
    {
        CEgfArc *pChild = new CEgfArc(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_ELIPS_TAG:
    {
        CEgfElips *pChild = new CEgfElips(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_ARROW_TAG:
    {
        CEgfArrow *pChild = new CEgfArrow(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_PLINE_TAG:
    {
        CEgfPline *pChild = new CEgfPline(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_PGON_TAG:
    case EGF_BOX_TAG:
    {
        CEgfPgon *pChild = new CEgfPgon(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_PASTE_TAG:
    {
        CEgfPaste *pChild = new CEgfPaste(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_TEXT_TAG:
    {
        CEgfText *pChild = new CEgfText(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_ANNO_TAG:
    {
        CEgfAnnot *pChild = new CEgfAnnot(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_PATH_TAG:
    {
        CEgfPath *pChild = new CEgfPath(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case EGF_STEXT_TAG:
    {
        CEgfSText *pChild = new CEgfSText(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case OBJTYPE_FMTPAGE:
    case OBJTYPE_SHAPEOBJ:
    case OBJTYPE_ANNOTATION:
    case OBJTYPE_BOOKMARK:
    case OBJTYPE_BLOCK:
    case OBJTYPE_SPCCHAR:
    case OBJTYPE_TAB:
    case OBJTYPE_PAGE:
    case OBJTYPE_BACKGROUND:
    case OBJTYPE_PGBLOCK:
    case OBJTYPE_ROW:
    case OBJTYPE_DRAWOBJ:
    case OBJTYPE_FORMFIELD:
    case OBJTYPE_FRAME:
    case OBJTYPE_FOOTNOTE:
    case OBJTYPE_PICTURE:
    {
        CEtxBase *pChild = new CEtxBase(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case OBJTYPE_TEXT:
    {
        CEtxText *pChild = new CEtxText(objType, objDataType, objSize);
        pChild->mSequenceId = sequenceId;
        if (pChild->Parse(ifs))
        {
            if (pParent) pParent->Append(pChild);
        }
        else
            return false;
    }
    break;
    case ETX_OBJ_END:
        AfxMessageBox(L"Empty Etx object!");
        break;
    default:
    {
        wchar_t msg[256];
        wsprintf(msg, L"Unknown object type: %d, object data type: %f, size: %d", objType, objDataType, objSize);
        AfxMessageBox(msg);
        return false;
    }
    }

    return true;
}

bool CEgfGroup::ParseChildren(std::ifstream &ifs, CEgfGroup* pParent)
{
    int objType = 0, objDataType = 0, objSize = 0;
    int sequenceId = 0;
    bool childrenStart = false;

    // parse children
    while (true)
    {
        if (!LoadObjHdr(ifs, objType, objDataType, objSize))
            break;

        if (childrenStart == false && objDataType != SDT_OBJ_CHILDREN_START)
            return false;
        if (childrenStart == false && objDataType == SDT_OBJ_CHILDREN_START)
        {
            childrenStart = true;
            continue;
        }
        if (childrenStart == true && objDataType == SDT_OBJ_CHILDREN_END)
            break;

        if (!ParseChild(ifs, pParent, objType, objDataType, objSize, sequenceId++)) return false;
    }

    return true;
}

bool CEgfGroup::Parse(std::ifstream &ifs)
{
    // parse group args
    if (!ParseArgs(ifs)) return false;

    // parse children
    return ParseChildren(ifs, this);
}

void CEgfGroup::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    CheckCriteria(objType, pPairs, pList);

    // search children
    for (int i = 0; i < mChildren.size(); i++)
        mChildren[i]->Search(objType, pPairs, pList);
}

void CEgfGroup::CheckCriteria(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_GROUP_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case GROUP_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case GROUP_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case GROUP_ARGS_ID_mHasMask:
            met = CheckCondition(pPairs->pairs[i], &mHasMask);
            break;
        case GROUP_ARGS_ID_mIsPasteObj:
            met = CheckCondition(pPairs->pairs[i], &mIsPasteObj);
            break;
        case GROUP_ARGS_ID_mMaskGroup:
            met = CheckCondition(pPairs->pairs[i], &mMaskGroup);
            break;
        case GROUP_ARGS_ID_mMaskObject:
            met = CheckCondition(pPairs->pairs[i], &mMaskObject);
            break;
        case GROUP_ARGS_ID_mSourceObject:
            met = CheckCondition(pPairs->pairs[i], &mSourceObject);
            break;
        case GROUP_ARGS_ID_mRefObject:
            met = CheckCondition(pPairs->pairs[i], &mRefObject);
            break;
        case GROUP_ARGS_ID_mTransparentGroup:
            met = CheckCondition(pPairs->pairs[i], &mTransparentGroup);
            break;
        case GROUP_ARGS_ID_mBackDropColor:
            met = CheckCondition(pPairs->pairs[i], &mBackDropColor);
            break;
        case GROUP_ARGS_ID_mMer_x1:
            met = CheckCondition(pPairs->pairs[i], &mMer.x1);
            break;
        case GROUP_ARGS_ID_mMer_x2:
            met = CheckCondition(pPairs->pairs[i], &mMer.x2);
            break;
        case GROUP_ARGS_ID_mMer_y1:
            met = CheckCondition(pPairs->pairs[i], &mMer.y1);
            break;
        case GROUP_ARGS_ID_mMer_y2:
            met = CheckCondition(pPairs->pairs[i], &mMer.y2);
            break;
        case GROUP_ARGS_ID_mTilingStep:
            met = (mTilingStep.x != 0.0f || mTilingStep.y != 0.0f);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}
