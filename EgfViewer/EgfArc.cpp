#include "stdafx.h"
#include "EgfArc.h"
#include "Global.h"


CEgfArc::CEgfArc() : m_pGradientPoints(0), mVisible(0), mMask(0)

{
    mObjType = 0;
    mObjDataType = 0;
    mObjSize = 0;
}

CEgfArc::CEgfArc(int objType, int objDataType, int objSize)
    : m_pGradientPoints(0), mVisible(0), mMask(0)
{
    mObjType = objType;
    mObjDataType = objDataType;
    mObjSize = objSize;
}

CEgfArc::~CEgfArc()
{
    if (m_pGradientPoints)
        delete[] m_pGradientPoints;
}

bool CEgfArc::Parse(std::ifstream &ifs)
{
    char buf[4];

    // object flags
    ReadObjectFlags(ifs, mVisible, mMask);

    // mThinknessUnits
    ifs.read(buf, 4);
    mThinknessUnits = Egf32_ToInt(buf);
    // mThinknessValue
    ifs.read(buf, 4);
    mThinknessValue = Egf32_ToInt(buf);

    // mColor
    ifs.read(mColor, 3);

    // mLineStyle
    ifs.read(buf, 4);
    mLineStyle = Egf32_ToInt(buf);

    // mCapStyle
    ifs.read(buf, 4);
    mCapStyle = Egf32_ToInt(buf);

    // mPenNum
    ifs.read(buf, 4);
    mPenNum = Egf32_ToInt(buf);

    // mOperation
    ifs.read(buf, 4);
    mOperation = Egf32_ToInt(buf);

    // mStartPoint
    ReadPoints(ifs, 1, &mStartPoint);

    // mEndPoint
    ReadPoints(ifs, 1, &mEndPoint);

    // start angle
    ifs.read(buf, 4);
    mStartAngle = (float)Egf32_ToInt(buf);
    // end angle
    ifs.read(buf, 4);
    mEndAndgle = (float)Egf32_ToInt(buf);

    // Gradient index
    ifs.read(buf, 4);
    mGradientIndex = Egf32_ToInt(buf);

    // mGradientNumPoints
    ifs.read(buf, 4);
    mGradientNumPoints = Egf32_ToInt(buf);

    // gradient points
    if (mGradientNumPoints > 0)
    {
        m_pGradientPoints = new CEgfPoint[mGradientNumPoints];
        ReadPoints(ifs, mGradientNumPoints, m_pGradientPoints);
    }

    return true;
}

void CEgfArc::Search(int objType, CSearchCriteria *pPairs, std::vector<CEgfObject*> *pList)
{
    if (objType != EGF_ARC_TAG || pPairs->pairs.size() == 0) return;

    bool met = true;

    for (int i = 0; i < pPairs->pairs.size(); i++)
    {
        switch (pPairs->pairs[i]->argId)
        {
        case ARC_ARGS_ID_mVisible:
            met = CheckCondition(pPairs->pairs[i], &mVisible);
            break;
        case ARC_ARGS_ID_mMask:
            met = CheckCondition(pPairs->pairs[i], &mMask);
            break;
        case ARC_ARGS_ID_mThinknessUnits:
            met = CheckCondition(pPairs->pairs[i], &mThinknessUnits);
            break;
        case ARC_ARGS_ID_mThinknessValue:
            met = CheckCondition(pPairs->pairs[i], &mThinknessValue);
            break;
        case ARC_ARGS_ID_mColor:
            met = CheckColorCondition(pPairs->pairs[i], mColor);
            break;
        case ARC_ARGS_ID_mLineStyle:
            met = CheckCondition(pPairs->pairs[i], &mLineStyle);
            break;
        case ARC_ARGS_ID_mCapStyle:
            met = CheckCondition(pPairs->pairs[i], &mCapStyle);
            break;
        case ARC_ARGS_ID_mOperation:
            met = CheckCondition(pPairs->pairs[i], &mOperation);
            break;
        case ARC_ARGS_ID_mStartPointX:
            met = CheckCondition(pPairs->pairs[i], &mStartPoint.x);
            break;
        case ARC_ARGS_ID_mStartPointY:
            met = CheckCondition(pPairs->pairs[i], &mStartPoint.y);
            break;
        case ARC_ARGS_ID_mEndPointX:
            met = CheckCondition(pPairs->pairs[i], &mEndPoint.y);
            break;
        case ARC_ARGS_ID_mEndPointY:
            met = CheckCondition(pPairs->pairs[i], &mEndPoint.y);
            break;
        case ARC_ARGS_ID_mStartAngle:
            met = CheckCondition(pPairs->pairs[i], &mStartAngle);
            break;
        case ARC_ARGS_ID_mEndAngle:
            met = CheckCondition(pPairs->pairs[i], &mEndAndgle);
            break;
        case ARC_ARGS_ID_mGradientIndex:
            met = CheckCondition(pPairs->pairs[i], &mGradientIndex);
            break;
        case ARC_ARGS_ID_mGradientNumPoints:
            met = CheckCondition(pPairs->pairs[i], &mGradientNumPoints);
            break;
        }

        if (met == false)
            break;
    }

    if (met)
        pList->push_back(this);
}