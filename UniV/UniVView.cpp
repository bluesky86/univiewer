
// UniVView.cpp : implementation of the CUniVView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "UniV.h"
#endif

#include "UniVDoc.h"
#include "UniVView.h"
#include "CSurface.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define VIEW_MARGIN         5

// CUniVView

IMPLEMENT_DYNCREATE(CUniVView, CScrollView)

BEGIN_MESSAGE_MAP(CUniVView, CScrollView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CUniVView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
    ON_WM_SIZE()
END_MESSAGE_MAP()

// CUniVView construction/destruction

CUniVView::CUniVView()
{
	// TODO: add construction code here
	m_pImage = nullptr;

    m_nClientWidth = 1;
    m_nClientHeight = 1;
    m_nPaintWidth = 1;
    m_nPaintHeight = 1;
    m_fScaleX = 1.0f;
    m_fScaleY = 1.0f;
    m_nLeftMargin = VIEW_MARGIN;
    m_nTopMargin = VIEW_MARGIN;

}

CUniVView::~CUniVView()
{
	if (m_pImage)
		delete m_pImage;
}

BOOL CUniVView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}

// CUniVView drawing

void CUniVView::OnDraw(CDC* pDC)
{
	CUniVDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
	CRect rect;
	GetClientRect(&rect);
	pDC->FillRect(&rect, &CBrush(RGB(171, 171, 171)));

	if (m_pImage)
	{
        //CUvSurface surface;
        //surface.width = m_pImage->width;
        //surface.height = m_pImage->height;
        //surface.colorType = m_pImage->colorType;

		BITMAPINFO bmpInfo;
		ZeroMemory(&bmpInfo, sizeof(BITMAPINFO));
		bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bmpInfo.bmiHeader.biWidth = m_pImage->width;
		bmpInfo.bmiHeader.biHeight = -m_pImage->height; // negative means top-down bitmap. Skia draws top-down.
		bmpInfo.bmiHeader.biPlanes = 1;
		bmpInfo.bmiHeader.biBitCount = m_pImage->bytePerPixel * 8;
		bmpInfo.bmiHeader.biCompression = BI_RGB;
		void* pixels = m_pImage->bits;

        //StretchDIBits(pDC->m_hDC, pDoc->m_nOrigX, pDoc->m_nOrigY, m_pImage->width, m_pImage->height, 0, 0, m_pImage->width, m_pImage->height, pixels, &bmpInfo, DIB_RGB_COLORS, SRCCOPY);
        StretchDIBits(pDC->m_hDC, (int)m_nLeftMargin, (int)m_nTopMargin, (int)m_nPaintWidth, (int)m_nPaintHeight, 0, 0, m_pImage->width, m_pImage->height, pixels, &bmpInfo, DIB_RGB_COLORS, SRCCOPY);
    }
}

void CUniVView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
}


// CUniVView printing


void CUniVView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CUniVView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CUniVView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CUniVView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CUniVView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CUniVView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CUniVView diagnostics

#ifdef _DEBUG
void CUniVView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CUniVView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CUniVDoc* CUniVView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUniVDoc)));
	return (CUniVDoc*)m_pDocument;
}
#endif //_DEBUG


// CUniVView message handlers


void CUniVView::OnSize(UINT nType, int cx, int cy)
{
    CScrollView::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here
    CUniVDoc* pDoc = GetDocument();
    ASSERT_VALID(pDoc);
    if (!pDoc)
        return;

    CRect rc;
    GetClientRect(&rc);
    int nClientWidth = rc.Width();
    int nClientHeight = rc.Height();

    if (nClientWidth == m_nClientWidth && nClientHeight == m_nClientHeight) return;

    m_nClientWidth = nClientWidth;
    m_nClientHeight = nClientHeight;

    if (nClientWidth > 0 && nClientHeight > 0 )
    {
        if (pDoc->nPageNo != -1)
        {
            CalculateRatio();

            pDoc->GetPage(pDoc->nPageNo);
            Invalidate();
        }
    }
}

void CUniVView::CalculateRatio()
{
    CUniVDoc* pDoc = GetDocument();

    float xyRatio = pDoc->m_nWidth / pDoc->m_nHeight;
    float clientRatio = (float)m_nClientWidth / m_nClientHeight;
    float pageRatio = pDoc->m_nWidth / pDoc->m_nHeight;

    if (clientRatio > pageRatio) // client rect has bigger width
    {
        // use Y
        m_nPaintHeight = (float)m_nClientHeight - VIEW_MARGIN * 2;
        m_nPaintWidth = m_nPaintHeight * xyRatio;
    }
    else
    {
        // use X
        m_nPaintWidth = (float)m_nClientWidth - VIEW_MARGIN * 2;
        m_nPaintHeight = m_nPaintWidth / xyRatio;
    }

    m_fScaleX = m_nPaintWidth / pDoc->m_nWidth;
    m_fScaleY = m_nPaintHeight / pDoc->m_nHeight;
    m_nLeftMargin = (m_nClientWidth - m_nPaintWidth) / 2;
    m_nTopMargin = (m_nClientHeight - m_nPaintHeight) / 2;

}
