
// UniVDoc.h : interface of the CUniVDoc class
//


#pragma once

class CUniVView;

class CUniVDoc : public CDocument
{
protected: // create from serialization only
	CUniVDoc();
	DECLARE_DYNCREATE(CUniVDoc)

// Attributes
public:
	void *loaderContext;
    int nPageNo;

    float m_nWidth;
    float m_nHeight;
    float m_nOrigX;
    float m_nOrigY;

// Operations
public:
	CUniVView * CUniVDoc::GetView();
	BOOL OpenNewFile(LPCTSTR lpszPathName);
    BOOL GetPage(int pageNo);

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CUniVDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
    virtual void OnCloseDocument();
};
