
// UniVView.h : interface of the CUniVView class
//

#pragma once

class CUniVDoc;
class CUvSkImage;

class CUniVView : public CScrollView
{
protected: // create from serialization only
	CUniVView();
	DECLARE_DYNCREATE(CUniVView)

// Attributes
public:
	CUniVDoc* GetDocument() const;

	CUvSkImage *m_pImage;

    int m_nClientWidth;
    int m_nClientHeight;
    float m_nPaintWidth;
    float m_nPaintHeight;
    float m_fScaleX;
    float m_fScaleY;

    float m_nLeftMargin;
    float m_nTopMargin;

    // Operations
public:
    void CalculateRatio();

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CUniVView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnSize(UINT nType, int cx, int cy);
};

#ifndef _DEBUG  // debug version in UniVView.cpp
inline CUniVDoc* CUniVView::GetDocument() const
   { return reinterpret_cast<CUniVDoc*>(m_pDocument); }
#endif

