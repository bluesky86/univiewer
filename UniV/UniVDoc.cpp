
// UniVDoc.cpp : implementation of the CUniVDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "UniV.h"
#endif

#include "UniVDoc.h"
#include "UniVView.h"
//#include "MainFrm.h"
//#include "CLoaderContext.h"
#include "CUvMatrix.h"
#include "CSurface.h"
#include "LoaderControlApi.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CUniVDoc

IMPLEMENT_DYNCREATE(CUniVDoc, CDocument)

BEGIN_MESSAGE_MAP(CUniVDoc, CDocument)
END_MESSAGE_MAP()


// CUniVDoc construction/destruction

CUniVDoc::CUniVDoc()
{
	// TODO: add one-time construction code here
	loaderContext = nullptr;
    nPageNo = -1;
    m_nWidth = 1;
    m_nHeight = 1;
    m_nOrigX = 0;
    m_nOrigY = 0;
}

CUniVDoc::~CUniVDoc()
{
}

BOOL CUniVDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CUniVDoc serialization

void CUniVDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CUniVDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CUniVDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CUniVDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CUniVDoc diagnostics

#ifdef _DEBUG
void CUniVDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUniVDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CUniVDoc commands
CUniVView* CUniVDoc::GetView()
{
	POSITION pos = GetFirstViewPosition();
	CUniVView *pView = (CUniVView*)(GetNextView(pos));
	return pView;
}


BOOL CUniVDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	// TODO:  Add your specialized creation code here
	if(OpenNewFile(lpszPathName) == FALSE) return FALSE;

	return TRUE;
}

BOOL CUniVDoc::GetPage(int pageNo)
{
    if (loaderContext == nullptr) return FALSE;

    HLoaderContext hLoaderContext = (HLoaderContext)loaderContext;

    // load first page
    CUvPage* pUvPage = nullptr;
    if ((pUvPage = LoaderControl_LoadPage(hLoaderContext, 0, pageNo)) != nullptr)
    {
        CUniVView *pView = GetView();

        //CUvPage* pUvPage = LoaderControl_GetCurrentPage(hLoaderContext);
        //if (pUvPage == nullptr) return TRUE;

        m_nWidth = pUvPage->Width() + 0.5f;
        m_nHeight = pUvPage->Height() + 0.5f;
        m_nOrigX = 0;
        m_nOrigY = 0;

        pView->CalculateRatio();

        CUvMatrix mat;
        mat = pView->m_fScaleX * mat;

        CUvSurface sfWin;

        if (pView->m_pImage) delete pView->m_pImage;
        pView->m_pImage = sfWin.DrawPage(pUvPage, mat, false);

        nPageNo = pageNo;

        return TRUE;
    }

    return FALSE;
}

BOOL CUniVDoc::OpenNewFile(LPCTSTR lpszPathName)
{
    HLoaderContext hLoaderContext = LoaderControl_OpenContext(lpszPathName);

	loaderContext = hLoaderContext;

    if (GetPage(0) == FALSE) return FALSE;

    //GetView()->Invalidate();

    return TRUE;
}




void CUniVDoc::OnCloseDocument()
{
    // TODO: Add your specialized code here and/or call the base class
    LoaderControl_CloseContext(loaderContext);

    CDocument::OnCloseDocument();
}
