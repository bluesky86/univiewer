#include "stdafx.h"
#include "Canvas.h"
#include "SkCodec.h"
#include "CUvImage.h"
#include "CUvGroup.h"
#include "DebugNew.h"
#include "Codec.h"
#include "Compress.h"

#ifdef WIN32
#include "RenderWin.h"
#endif

extern SkMatrix gTotalCanvasMat;
extern SkCanvas *gCurrentCanvas;

class CUvSkImage;

extern CUvSkImage * GetImageFromSurface(sk_sp<SkSurface> skSurface);
extern CUvSkImage * GetImageFromCanvas(SkCanvas *skCanvas);
extern bool uvGetBound(CUvShape *pShape, CUvRectF &rect, SkCanvas *canvas);

//U8* convertTo8Bits(U8 &bitCount, CUvColor *palette, S32 &numCorlor, S32 width, S32 height, S32 &byteWidth, U8 *pBits, int newPads, bool bFreeOldBits);
//U8* convertTo24Bits(U8 &srcBitCount, CUvColor *palette, S32 width, S32 height, S32 &byteWidth, U8 *pBits, int newPads, bool bFlipRGB, bool bFreeOldBits);
//U8* convertTo32Bits(U8 &srcBitCount, CUvColor *palette, S32 width, S32 height, S32 &byteWidth, U8 *pBits, int newPads, bool bFlipRGB, bool bFreeOldBits);


bool decode_file_withColorType(const char* filename, SkBitmap* bitmap,
    SkColorType colorType = kN32_SkColorType,
    bool requireUnpremul = false)
{
    sk_sp<SkData> data(SkData::MakeFromFileName(filename));
    std::unique_ptr<SkCodec> codec = SkCodec::MakeFromData(data);
    if (!codec) {
        return false;
    }

    SkImageInfo info = codec->getInfo().makeColorType(colorType);
    if (requireUnpremul && kPremul_SkAlphaType == info.alphaType()) {
        info = info.makeAlphaType(kUnpremul_SkAlphaType);
    }

    if (!bitmap->tryAllocPixels(info)) {
        return false;
    }

    return SkCodec::kSuccess == codec->getPixels(info, bitmap->getPixels(), bitmap->rowBytes());
}

int decode_file(sk_sp<SkData> data, SkBitmap* bitmap)
{
    std::unique_ptr<SkCodec> codec = SkCodec::MakeFromData(data);
    if (!codec)
        return 0;

    SkImageInfo info = codec->getInfo();

    if (!bitmap->tryAllocPixels(info)) {
        return 0;
    }

    if (SkCodec::kSuccess == codec->getPixels(info, bitmap->getPixels(), bitmap->rowBytes()))
        return (int)codec->getOrigin();
    else
        return 0;
}

int decode_file(const char* buf, size_t buf_len, SkBitmap* bitmap)
{
    sk_sp<SkData> data = SkData::MakeWithoutCopy(buf, buf_len);

    return decode_file(data, bitmap);
}

int decode_file(const char* filename, SkBitmap* bitmap)
{
    sk_sp<SkData> data(SkData::MakeFromFileName(filename));
    return decode_file(data, bitmap);
}

bool decode_image_dimensions(sk_sp<SkData> data, int &width, int &height)
{
    std::unique_ptr<SkCodec> codec = SkCodec::MakeFromData(data);
    if (!codec)
        return false;

    SkImageInfo info = codec->getInfo();
    width = info.width();
    height = info.height();

    return true;
}

bool decode_image_dimensions(const char* filename, int &width, int &height)
{
    sk_sp<SkData> data(SkData::MakeFromFileName(filename));
    return decode_image_dimensions(data, width, height);
}

bool decode_image_dimensions(const char* buf, size_t buf_len, int &width, int &height)
{
    sk_sp<SkData> data(SkData::MakeFromMalloc(buf, buf_len));
    return decode_image_dimensions(data, width, height);
}

sk_sp<SkImage> decode_file(const char filename[])
{
    sk_sp<SkData> data(SkData::MakeFromFileName(filename));
    return data ? SkImage::MakeFromEncoded(data) : nullptr;
}

const SkImageInfo* GetImageInfo(CUvImage *pUvImage)
{
    if (pUvImage == nullptr) return nullptr;

    //void *pInfo = pUvImage->GetInfo();
    //if (pInfo) return (SkImageInfo*)pInfo;

    //sk_sp<SkData> data;
    //if (pUvImage->data && pUvImage->data_len > 0)
    //    data = SkData::MakeFromMalloc(pUvImage->data, pUvImage->data_len);
    //else
    //    data = SkData::MakeFromFileName(pUvImage->href);
    //std::unique_ptr<SkCodec> codec = SkCodec::MakeFromData(data);
    //if (!codec) return nullptr;

    //pUvImage->SetInfo((void*)&codec->getInfo());

    //return (SkImageInfo*)pUvImage->GetInfo();

    return nullptr;
}

///////////////////////////////////////////////////     
// Clockwise 90 (CW-90)
//                  |
//   ------+--+--+--+---+---+---+---+---+--------> x
//         +--+--+--+---+---+---+---+---+
//         +--+--+--+---+---+---+---+---+
//         +--+--+--+---+---+---+---+---+
//         +--+--+--+
//         +--+--+--+
//                  |
//                  |
//                  V
//                  y
//
void RotateCW90(SkBitmap *bitmap)
{
    SkImageInfo srcInfo = bitmap->info();
    SkImageInfo info = SkImageInfo::Make(srcInfo.height(), srcInfo.width(), srcInfo.colorType(), srcInfo.alphaType(), srcInfo.refColorSpace());

    SkBitmap newBitmap;

    if (!newBitmap.tryAllocPixels(info)) {
        return;
    }

    int bytesPerPixel = bitmap->bytesPerPixel();
    int srcRowBytes = (int)bitmap->rowBytes();
    int sh = bitmap->height();
    int sw = bitmap->width();

    int targetRowBytes = (int)newBitmap.rowBytes();
    int th = newBitmap.height();
    int tw = newBitmap.width();

    int sRow, sCol, tRow, tCol;
    unsigned char *pSrcData = (unsigned char*)bitmap->getPixels();
    unsigned char *pTargetData = (unsigned char*)newBitmap.getPixels();

    // tx = sh - sy - 1;
    // ty = sx
    for (int sy = 0; sy < sh; sy++)
    {
        sRow = sy * srcRowBytes;
        tCol = (sh - sy - 1) * bytesPerPixel;
        for (int sx = 0; sx < sw; sx++)
        {
            sCol = sRow + sx * bytesPerPixel;
            tRow = sx * targetRowBytes + tCol;
            memcpy(&pTargetData[tRow], &pSrcData[sCol], bytesPerPixel);
        }
    }

    bitmap->swap(newBitmap);
}
void RotateCCW90(SkBitmap *bitmap)
{
    SkImageInfo srcInfo = bitmap->info();
    SkImageInfo info = SkImageInfo::Make(srcInfo.height(), srcInfo.width(), srcInfo.colorType(), srcInfo.alphaType(), srcInfo.refColorSpace());

    SkBitmap newBitmap;

    if (!newBitmap.tryAllocPixels(info)) {
        return;
    }

    int bytesPerPixel = bitmap->bytesPerPixel();
    int srcRowBytes = (int)bitmap->rowBytes();
    int sh = bitmap->height();
    int sw = bitmap->width();

    int targetRowBytes = (int)newBitmap.rowBytes();
    int th = newBitmap.height();
    int tw = newBitmap.width();

    int sRow, sCol, tRow, tCol;
    unsigned char *pSrcData = (unsigned char*)bitmap->getPixels();
    unsigned char *pTargetData = (unsigned char*)newBitmap.getPixels();

    // tx = sy;
    // ty = sw - sx - 1
    for (int sy = 0; sy < sh; sy++)
    {
        sRow = sy * srcRowBytes;
        tCol = sy * bytesPerPixel;
        for (int sx = 0; sx < sw; sx++)
        {
            sCol = sRow + sx * bytesPerPixel;
            tRow = (sw - sx - 1) * targetRowBytes + tCol;
            memcpy(&pTargetData[tRow], &pSrcData[sCol], bytesPerPixel);
        }
    }

    bitmap->swap(newBitmap);
}

void FlipH(SkBitmap *bitmap)
{
    int bytesPerPixel = bitmap->bytesPerPixel();
    int bytesPerRow = (int)bitmap->rowBytes();
    int row, sCol, tCol;
    int h = bitmap->height();
    int w = bitmap->width();
    int halfw = w / 2;
    unsigned char buf[256];
    unsigned char *pData = (unsigned char*)bitmap->getPixels();

    for (int i = 0; i < h; i++)
    {
        row = i * bytesPerRow;
        for (int j = 0; j < halfw; j++)
        {
            sCol = row + j * bytesPerPixel;
            tCol = row + (w - j - 1) * bytesPerPixel;
            memcpy(buf, &pData[tCol], bytesPerPixel);
            memcpy(&pData[tCol], &pData[sCol], bytesPerPixel);
            memcpy(&pData[sCol], buf, bytesPerPixel);
        }
    }
}
void FlipV(SkBitmap *bitmap)
{
    int bytesPerPixel = bitmap->bytesPerPixel();
    int bytesPerRow = (int)bitmap->rowBytes();
    int sRow, sCol;
    int tRow, tCol;
    int h = bitmap->height();
    int w = bitmap->width();
    int halfh = h / 2;
    int halfw = w / 2;
    unsigned char buf[256];
    unsigned char *pData = (unsigned char*)bitmap->getPixels();

    for (int i = 0; i < halfh; i++)
    {
        sRow = i * bytesPerRow;
        tRow = (h - i - 1) * bytesPerRow;
        for (int j = 0; j < w; j++)
        {
            sCol = sRow + j * bytesPerPixel;
            tCol = tRow + j * bytesPerPixel;
            memcpy(buf, &pData[tCol], bytesPerPixel);
            memcpy(&pData[tCol], &pData[sCol], bytesPerPixel);
            memcpy(&pData[sCol], buf, bytesPerPixel);
        }
    }

}

bool ApplyImageOrigin(int orig, SkCanvas* canvas, CUvImage *pUvImage, SkBitmap &bitmap, int width, int height)
{
    SkMatrix skMatrix;

    switch (orig)
    {
    case kTopLeft_SkEncodedOrigin:  // 1: Default: rotation 0
        break;
    case kTopRight_SkEncodedOrigin: // 2: mirror (Reflected across y-axis)
        // solution 1:
        canvas->concat(SkMatrix::Translate((SkScalar)(pUvImage->x + width), (SkScalar)(pUvImage->y)));
        canvas->concat(SkMatrix::MakeAll(-1, 0, 0, 0, 1, 0, 0, 0, 1));
        canvas->concat(SkMatrix::Translate((SkScalar)(-pUvImage->x), (SkScalar)(-pUvImage->y)));

        // solution 2:
        //FlipH(&bitmap);
        break;
    case kBottomLeft_SkEncodedOrigin: // 4: mirror + rotation 180 = vertical mirror (Reflected across x-axis)
        // solution 1:
        canvas->concat(SkMatrix::Translate((SkScalar)(pUvImage->x), (SkScalar)(pUvImage->y + height)));
        canvas->concat(SkMatrix::MakeAll(1, 0, 0, 0, -1, 0, 0, 0, 1));
        canvas->concat(SkMatrix::Translate((SkScalar)(-pUvImage->x), (SkScalar)(-pUvImage->y)));

        // solution 2:
        //FlipV(&bitmap);
        break;
    case kBottomRight_SkEncodedOrigin: // 3: rotation 180 = ( mirror + vertical mirror )
        // solution 1:
        canvas->concat(SkMatrix::Translate((SkScalar)(pUvImage->x + width), (SkScalar)(pUvImage->y + height)));
        canvas->concat(SkMatrix::MakeAll(-1, 0, 0, 0, -1, 0, 0, 0, 1));
        canvas->concat(SkMatrix::Translate((SkScalar)(-pUvImage->x), (SkScalar)(-pUvImage->y)));

        // solution 2:
        ////FlipH(&bitmap);
        ////FlipV(&bitmap);
        break;
    case kLeftTop_SkEncodedOrigin: // 5: mirror + rotation -90 (Reflected across x-axis, Rotated 90 CCW)
        // solution 1:
        // second step: rotate 90 CCW
        canvas->concat(SkMatrix::Translate((SkScalar)(pUvImage->x), (SkScalar)(pUvImage->y + width)));
        skMatrix.setRotate(-90);
        canvas->concat(skMatrix);
        // first step: mirror
        canvas->concat(SkMatrix::Translate((SkScalar)(width), (SkScalar)0));
        canvas->concat(SkMatrix::MakeAll(-1, 0, 0, 0, 1, 0, 0, 0, 1));
        canvas->concat(SkMatrix::Translate((SkScalar)(-pUvImage->x), (SkScalar)(-pUvImage->y)));

        // solution 2:
        //FlipH(&bitmap);
        //RotateCCW90(&bitmap);
        //targetRect.set((SkScalar)pUvImage->x, (SkScalar)pUvImage->y, (SkScalar)pUvImage->x + targetHeight - 1, (SkScalar)pUvImage->y + targetWidth - 1);
        break;
    case kRightTop_SkEncodedOrigin: // 6: rotation -90 (Rotated 90 CW)
        // solution 1:
        canvas->concat(SkMatrix::Translate((SkScalar)(pUvImage->x + height), (SkScalar)(pUvImage->y)));
        skMatrix.setRotate(90);
        canvas->concat(skMatrix);
        canvas->concat(SkMatrix::Translate((SkScalar)(-pUvImage->x), (SkScalar)(-pUvImage->y)));

        // solution 2:
        //RotateCW90(&bitmap);
        //targetRect.set((SkScalar)pUvImage->x, (SkScalar)pUvImage->y, (SkScalar)pUvImage->x + targetHeight - 1, (SkScalar)pUvImage->y + targetWidth - 1);
        break;
    case kRightBottom_SkEncodedOrigin: // 7: mirror + rotation 90 (Reflected across x-axis, Rotated 90 CW)
        // solution 1:
        // second step: rotate 90 CW
        canvas->concat(SkMatrix::Translate((SkScalar)(pUvImage->x + height), (SkScalar)(pUvImage->y)));
        skMatrix.setRotate(90);
        canvas->concat(skMatrix);
        // first step: mirror
        canvas->concat(SkMatrix::Translate((SkScalar)(width), (SkScalar)0));
        canvas->concat(SkMatrix::MakeAll(-1, 0, 0, 0, 1, 0, 0, 0, 1));
        canvas->concat(SkMatrix::Translate((SkScalar)(-pUvImage->x), (SkScalar)(-pUvImage->y)));

        // solution 2:
        //FlipH(&bitmap);
        //RotateCW90(&bitmap);
        //targetRect.set((SkScalar)pUvImage->x, (SkScalar)pUvImage->y, (SkScalar)pUvImage->x + targetHeight - 1, (SkScalar)pUvImage->y + targetWidth - 1);
        break;
    case kLeftBottom_SkEncodedOrigin: // 8: rotation 90 (Rotated 90 CCW)
        // solution 1:
        canvas->concat(SkMatrix::Translate((SkScalar)(pUvImage->x), (SkScalar)(pUvImage->y + width)));
        skMatrix.setRotate(-90);
        canvas->concat(skMatrix);
        canvas->concat(SkMatrix::Translate((SkScalar)(-pUvImage->x), (SkScalar)(-pUvImage->y)));

        // solution 2:
        //RotateCCW90(&bitmap);
        //targetRect.set((SkScalar)pUvImage->x, (SkScalar)pUvImage->y, (SkScalar)pUvImage->x + targetHeight - 1, (SkScalar)pUvImage->y + targetWidth - 1);
        break;
    default:
        return false;
    }

    return true;
}

void MapBitmapColor(CUvImage *pUvImage, int outNumBits, int bytesPerRow, unsigned char *pOut)
{
    int num = pUvImage->colorMap.size();
    if (num <= 0) return;

    int bytes = outNumBits / 8;;
    switch (outNumBits)
    {
    case 24:
    case 32:
        for (int h = 0; h < pUvImage->imageHeight; h++)
        {
            int i;
            unsigned char *pData = (unsigned char*)&pOut[h * bytesPerRow];
            for (int w = 0; w < pUvImage->imageWidth; w++)
            {
                for (i = 0; i < num; i++)
                {
                    if (pData[0] == pUvImage->colorMap[i]->first.r && pData[1] == pUvImage->colorMap[i]->first.g && pData[2] == pUvImage->colorMap[i]->first.b)
                    {
                        pData[0] = pUvImage->colorMap[i]->second.r;
                        pData[1] = pUvImage->colorMap[i]->second.g;
                        pData[2] = pUvImage->colorMap[i]->second.b;
                        if(outNumBits == 32) pData[3] = pUvImage->colorMap[i]->second.a;
                        break;
                    }
                }
                pData += bytes;
            }
        }
        break;
    case 8:
        if (pUvImage->numColors > 0 && pUvImage->palette)
        {
            for (int i = 0; i < pUvImage->numColors; i++)
            {
                for (int j = 0; j < num; j++)
                {
                    if (pUvImage->palette[i] == pUvImage->colorMap[j]->first)
                    {
                        pUvImage->palette[i] = pUvImage->colorMap[j]->second;
                        break;
                    }
                }
            }
        }
        break;
    }
}

float gImageColorMatrix[2][5] =  {
        {0.3f,      0.59f,      0.11f,      0.0f,   0.0f     },
        {0.2126f,   0.7152f,    0.0722f,    0.0f,   0.0f     },
    };

void ApplyColorMatrix(CUvImage *pUvImage, int outNumBits, int bytesPerRow, unsigned char *pOut)
{
    if (pUvImage->colorEffectType != uvImageColorEffectType_duoTone && pUvImage->colorEffectType != uvImageColorEffectType_grayScale)
        return;
    if (pUvImage->colorEffectType == uvImageColorEffectType_duoTone && pUvImage->colorMap.size() <= 0)
        return;

    int bytes = outNumBits / 8;;

    unsigned char r1 = 0, g1 = 0, b1 = 0;
    float r = 1.0f, g = 1.0f, b = 1.0f;
    int index = 0;

    if (pUvImage->colorEffectType == uvImageColorEffectType_duoTone)
    {
        r1 = pUvImage->colorMap[0]->first.r;
        g1 = pUvImage->colorMap[0]->first.g;
        b1 = pUvImage->colorMap[0]->first.b;

        r = (pUvImage->colorMap[0]->second.r - pUvImage->colorMap[0]->first.r) / 255.0f;
        g = (pUvImage->colorMap[0]->second.g - pUvImage->colorMap[0]->first.g) / 255.0f;
        b = (pUvImage->colorMap[0]->second.b - pUvImage->colorMap[0]->first.b) / 255.0f;

        index = 1;
    }

    float v;
    switch (outNumBits)
    {
    case 24:
    case 32:
        for (int h = 0; h < pUvImage->imageHeight; h++)
        {
            unsigned char *pData = (unsigned char*)&pOut[h * bytesPerRow];
            for (int w = 0; w < pUvImage->imageWidth; w++)
            {
                v = gImageColorMatrix[index][0] * pData[0] + gImageColorMatrix[index][1] * pData[1] + gImageColorMatrix[index][2] * pData[2];
                pData[0] = (unsigned char)(v * r + r1);
                pData[1] = (unsigned char)(v * g + g1);
                pData[2] = (unsigned char)(v * b + b1);

                pData += bytes;
            }
        }
        break;
    case 8:
        if (pUvImage->numColors > 0 && pUvImage->palette)
        {
            for (int i = 0; i < pUvImage->numColors; i++)
            {
                v = gImageColorMatrix[index][0] * pUvImage->palette[i].r + gImageColorMatrix[index][1] * pUvImage->palette[i].g + gImageColorMatrix[index][2] * pUvImage->palette[i].b;
                pUvImage->palette[i].r = (unsigned char)(v * r + r1);
                pUvImage->palette[i].g = (unsigned char)(v * g + g1);
                pUvImage->palette[i].b = (unsigned char)(v * b + b1);
            }
        }
        break;
    }
}

bool _uvDrawImageFile(SkCanvas* canvas, CUvImage *pUvImage, char *memFile, int memFileSize, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    SkPaint paint;

    SkBitmap bitmap;

    int orig = 0;

    if (memFile)
    {
        orig = decode_file(memFile, memFileSize, &bitmap);
    }
    else if(pUvImage->type == uvImageType_file)
        orig = decode_file(pUvImage->href, &bitmap);
    else
    {
        CUvRasterStripBlock *pBlock = pUvImage->data[0]->pBlock;
        orig = decode_file((char*)pBlock->data, (size_t)pBlock->size, &bitmap);
    }

    SkImageInfo info = bitmap.info();

    pUvImage->imageWidth = info.width();
    pUvImage->imageHeight = info.height();

    int targetWidth, targetHeight;

    if (pUvImage->widthIsPercentage) targetWidth = (int)(pUvImage->targetWidth / 100.0f * info.width() + 0.5f);
    else targetWidth = pUvImage->targetWidth;

    if (pUvImage->heightIsPercentage) targetHeight = (int)(pUvImage->targetHeight / 100.0f * info.height() + 0.5f);
    else targetHeight = pUvImage->targetHeight;

    //SaveCanvas(canvas, nullptr);

    //#define DEBUG_UV_IMAGE
#ifdef DEBUG_UV_IMAGE
    {
        //static int count = 0;
        //count++;
        //if ((count % 3) == 1)
        //    orig = kTopLeft_SkEncodedOrigin;
        //else if ((count % 3) == 2)
        //    orig = kLeftTop_SkEncodedOrigin;
        //else
        //    orig = kRightTop_SkEncodedOrigin;

        orig = kRightTop_SkEncodedOrigin;
    }

#endif

    if (ApplyImageOrigin(orig, canvas, pUvImage, bitmap, targetWidth, targetHeight) == false)
        return false;

    SkRect targetRect;
    targetRect.setLTRB((SkScalar)pUvImage->x, (SkScalar)pUvImage->y, (SkScalar)pUvImage->x + targetWidth, (SkScalar)pUvImage->y + targetHeight);

    //canvas->drawRect(targetRect, paint);

    canvas->drawBitmapRect(bitmap, bitmap.bounds(), targetRect, &paint);

    if (bDrawPoint)
    {
        uvDrawRectOutline(canvas, &targetRect);

        CUvRectF rectF;
        rectF.Set(targetRect.fLeft, targetRect.fRight, targetRect.fTop, targetRect.fBottom);
        uvDrawRectPointHandle(canvas, &rectF);
    }

    //RestoreCanvas(canvas, nullptr);

    //// slower than SkBitmap
    //sk_sp<SkImage> pImage = decode_file(pUvImage->href);

    //if (pImage)
    //{
    //    canvas->drawImageRect(pImage, pImage->bounds(), targetRect, &paint);
    //}

    return true;
}

bool _uvDrawEmf(SkCanvas* canvas, CUvImage *pUvImage, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    char *memBmpFile = nullptr;
    int memBmpFileSize = 0;
    CUvRasterStripBlock *pBlock = pUvImage->data[0]->pBlock;
    //orig = decode_file((char*)pBlock->data, (size_t)pBlock->size, &bitmap);
#ifdef WIN32
    //UNIV_RENDER_WIN_API char* DrawEmfWin(int type, unsigned int size, unsigned char *data, int &numBits, int &width, int &height, int &bytesPerRow)
    memBmpFile = DrawEmfWin((pUvImage->type == uvImageType_wmf) ? 0 : 1, pBlock->size, pBlock->data, memBmpFileSize);

#else
    // linux and Unix

#endif

    bool res = false;

    if (memBmpFile)
    {
#ifdef _DEBUG
        //static int aaa = 0;
        //char buf[128];
        //sprintf_s(buf, 128, "F:\\Test\\tmp1\\a%d.bmp", aaa);
        //FILE *fp;
        //fopen_s(&fp, buf, "wb");
        //fwrite(memBmpFile, 1, memBmpFileSize, fp);
        //fclose(fp);
#endif

        res = _uvDrawImageFile(canvas, pUvImage, memBmpFile, memBmpFileSize, pStrokeFillArgs, bDrawPoint);
        delete[] memBmpFile;
    }

    return res;
}

void* _uvSetupDecoding(CUvImage *pUvImage, int *outNumBits, int *out_size)
{
    //int out_bytesPerRow;
    switch (pUvImage->encodingType)
    {
    //case uvImageEncoding_G3:
    //    break;
    case uvImageEncoding_G4M:
        return SetupG3G4Decoding(pUvImage, outNumBits, out_size);
    case uvImageEncoding_Raw:
    case uvImageEncoding_RawTiled:
        return SetupRawDecoding(pUvImage, outNumBits, out_size);
    case uvImageEncoding_CC1:
    case uvImageEncoding_CC1RGB:
    case uvImageEncoding_CC1RGBA:
        return SetupCC1Decoding(pUvImage, outNumBits, out_size);
    case uvImageEncoding_LZW:
        return SetupLzwDecoding(pUvImage, outNumBits, out_size);
    case uvImageEncoding_Nibble:
        return SetupNibbleDecoding(pUvImage, outNumBits, out_size);
    case uvImageEncoding_JPEG:
    case uvImageEncoding_SJPEG:
        return SetupJpegDecoding(pUvImage, outNumBits, out_size);
    case uvImageEncoding_24BP:
        return SetupBPDecoding( pUvImage, outNumBits, out_size );
    default:
        return nullptr;
    }
    return nullptr;
}
char* _uvDecode(CUvImage *pUvImage, void *handle, char *in_data, int in_size, char *out_buffer, int *out_size)
{
    switch (pUvImage->encodingType)
    {
    //case uvImageEncoding_G3:
    //    break;
    case uvImageEncoding_G4M:
        return DecodeG3G4(handle, in_size, in_data, 0, nullptr, out_buffer, out_size);
    case uvImageEncoding_Raw:
    case uvImageEncoding_RawTiled:
        return DecodeRaw(handle, in_size, in_data, 0, nullptr, out_buffer, out_size);
    case uvImageEncoding_CC1:
    case uvImageEncoding_CC1RGB:
    case uvImageEncoding_CC1RGBA:
        return DecodeCC1(handle, in_size, in_data, 0, nullptr, out_buffer, out_size);
    case uvImageEncoding_LZW:
        return DecodeLzw(handle, in_size, in_data, 0, nullptr, out_buffer, out_size);
    case uvImageEncoding_Nibble:
        return DecodeNibble(handle, in_size, in_data, 0, nullptr, out_buffer, out_size);
    case uvImageEncoding_JPEG:
    case uvImageEncoding_SJPEG:
        return DecodeJpeg(handle, in_size, in_data, 0, nullptr, out_buffer, out_size);
    case uvImageEncoding_24BP:
        return DecodeBP( handle, in_size, in_data, 0, nullptr, out_buffer, out_size );
    default:
        return nullptr;
    }
    return nullptr;
}
void _uvCloseDecode(CUvImage *pUvImage, void *handle)
{
    switch (pUvImage->encodingType)
    {
    //case uvImageEncoding_G3:
    //    break;
    case uvImageEncoding_G4M:
        CleanupG3G4Decoding(handle);
        break;
    case uvImageEncoding_Raw:
    case uvImageEncoding_RawTiled:
        CleanupRawDecoding(handle);
        break;
    case uvImageEncoding_CC1:
    case uvImageEncoding_CC1RGB:
    case uvImageEncoding_CC1RGBA:
        CleanupCC1Decoding(handle);
        break;
    case uvImageEncoding_LZW:
        CleanupLzwDecoding(handle);
        break;
    case uvImageEncoding_Nibble:
        CleanupNibbleDecoding(handle);
        break;
    case uvImageEncoding_JPEG:
    case uvImageEncoding_SJPEG:
        CleanupJpegDecoding(handle);
    case uvImageEncoding_24BP:
        return CleanupBPDecoding( handle );
        break;
    default:
        break;
    }
}

bool _uvDrawImageStrip(SkCanvas* canvas, CUvImage *pUvImage, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    SkPaint paint;

    SkBitmap bitmap;

    //int orig = decode_file(pUvImage->href, &bitmap);
    int targetWidth, targetHeight;

    if (pUvImage->widthIsPercentage) targetWidth = (int)(pUvImage->targetWidth / 100.0f * pUvImage->imageWidth + 0.5f);
    else targetWidth = pUvImage->targetWidth;

    if (pUvImage->heightIsPercentage) targetHeight = (int)(pUvImage->targetHeight / 100.0f * pUvImage->imageHeight + 0.5f);
    else targetHeight = pUvImage->targetHeight;

    int outNumBytes = 0;
    int outNumBits = pUvImage->numBits;

    // setup decoding environment
    void *handle = _uvSetupDecoding(pUvImage, &outNumBits, &outNumBytes);


    int bytesPerRow = outNumBytes / pUvImage->imageHeight;
    int bytesPerPixel = bytesPerRow / pUvImage->imageWidth;
    char *pOut = NEW char[outNumBytes + 4 * bytesPerRow];
    char *pTmpOut = NEW char[outNumBytes + 4 * bytesPerRow];
    memset(pOut, 255, outNumBytes);

    int total_out_pos;
 
    int numStrips = pUvImage->data.size(); // bit planes
        
    for(int i = 0; i < numStrips; i++)
    {
        total_out_pos = i;

        CUvRasterStripBlock *pBlock = pUvImage->data[(U32)i]->pBlock;
        while(pBlock)
        {
            int _out_size = 0;

            _uvDecode( pUvImage, handle, (char*)pBlock->data, pBlock->size, pTmpOut, &_out_size );
            if(pUvImage->bTiled)
            {
                int tileRow = pBlock->tileIndex / pUvImage->numTilesPerWidth;
                int tileCol = pBlock->tileIndex % pUvImage->numTilesPerWidth;
                //int bytesPerTileRow = _out_size / pUvImage->tileHeight;
                int bytesPerTileRow = bytesPerPixel * pUvImage->tileWidth;
                int numScanline = _out_size / bytesPerTileRow;
                int scanLineYStartPos = tileRow * pUvImage->tileHeight;
                int scanLineXStartPos = scanLineYStartPos * bytesPerRow + tileCol * bytesPerTileRow + i;
                if(numStrips == 1)
                {
                    for(int j = 0; j < numScanline; j++)
                    {
                        memcpy( &pOut[scanLineXStartPos], &pTmpOut[j * bytesPerTileRow], bytesPerTileRow );
                        scanLineXStartPos += bytesPerRow;
                    }
                }
                else
                {
                    for(int j = 0; j < numScanline; j++)
                    {
                        for(int k = 0; k < bytesPerTileRow; k++)
                            pOut[scanLineXStartPos + k * bytesPerPixel] = pTmpOut[j * bytesPerTileRow + k];
                        scanLineXStartPos += bytesPerRow;
                    }
                }
            }
            else
            {
                if(numStrips == 1)
                {
                    if(pUvImage->encodingType != uvImageEncoding_JPEG
                        && pUvImage->numColors > 0 && pUvImage->palette && outNumBits != pUvImage->numBits)
                    {
                        int step = outNumBits / 8;
                        unsigned char *pTmpOut1 = (unsigned char *)pTmpOut;
                        for(int j = 0; j < _out_size; j++)
                        {
                            int pos = total_out_pos + j * step;
                            pOut[pos++] = pUvImage->palette[pTmpOut1[j]].r;
                            pOut[pos++] = pUvImage->palette[pTmpOut1[j]].g;
                            pOut[pos++] = pUvImage->palette[pTmpOut1[j]].b;
                            pOut[pos++] = pUvImage->palette[pTmpOut1[j]].a;
                        }
                        total_out_pos += _out_size * step;
                    }
                    else
                    {
                        int bytes = _out_size;
                        if(total_out_pos + _out_size >= outNumBytes)
                            bytes = outNumBytes - total_out_pos;
                        memcpy( &pOut[total_out_pos], pTmpOut, bytes );
                        total_out_pos += bytes;
                    }
                }
                else
                {
                    int width = 0, height = 0;
                    int offset = height * bytesPerRow + i;
                    for(int j = 0; j < _out_size && offset < outNumBytes; j++, width++)
                    {
                        if(width == pUvImage->imageWidth)
                        {
                            width = 0;
                            height++;
                            offset = height * bytesPerRow + i;
                        }
                        pOut[offset] = pTmpOut[j];
                        offset += bytesPerPixel;
                    }
                    //total_out_pos += _out_size * bytesPerPixel;
                    //for(int j = 0; j < _out_size && total_out_pos + j * bytesPerPixel < outNumBytes; j++)
                    //    pOut[total_out_pos + j * bytesPerPixel] = pTmpOut[j];
                    //total_out_pos += _out_size * bytesPerPixel;
                }
            }
            pBlock = pBlock->next;
        }
    }

    FILE *fp;
    fopen_s(&fp, "C:\\Work\\Test\\tmp2\\image_8_8_478_123.dat", "wb" );
    fwrite( pOut, 1, outNumBytes, fp );
    fclose( fp );

    // close decode environment
    _uvCloseDecode(pUvImage, handle);

    //////////////////////////////////////////////////
    // draw image
    //////////////////////////////////////////////////
    SkImageInfo info;

    switch (outNumBits)
    {
    case 24:
        info = SkImageInfo::Make(pUvImage->imageWidth, pUvImage->imageHeight, kRGB_888x_SkColorType, kUnpremul_SkAlphaType);
        break;
    case 32:
        info = SkImageInfo::Make(pUvImage->imageWidth, pUvImage->imageHeight, kRGBA_8888_SkColorType, kUnpremul_SkAlphaType);
        break;
    case 8:
    default:
        info = SkImageInfo::Make(pUvImage->imageWidth, pUvImage->imageHeight, kGray_8_SkColorType, kOpaque_SkAlphaType);
        break;
    }

    bytesPerRow = outNumBytes / pUvImage->imageHeight;

    if (pUvImage->colorEffectType == uvImageColorEffectType_colorChange)
        MapBitmapColor(pUvImage, outNumBits, bytesPerRow, (unsigned char*)pOut);
    else if (pUvImage->colorEffectType == uvImageColorEffectType_duoTone || pUvImage->colorEffectType == uvImageColorEffectType_grayScale)
        ApplyColorMatrix(pUvImage, outNumBits, bytesPerRow, (unsigned char*)pOut);

    bitmap.setInfo(info, bytesPerRow);
    bitmap.setPixels((void*)pOut);
    //{
    //    FILE *fp;
    //    char buf[256];
    //    sprintf_s(buf, 256, "F:\\Test\\tmp\\image_%d_%d_%d_%d.dat", outNumBits, outNumBits, pUvImage->imageWidth, pUvImage->imageHeight);
    //    fopen_s(&fp, buf, "wb");
    //    fwrite(pOut, 1, outNumBytes, fp);
    //    fclose(fp);
    //}

    //SaveCanvas(canvas, nullptr);

    if (ApplyImageOrigin(pUvImage->origin, canvas, pUvImage, bitmap, targetWidth, targetHeight) == false)
        return false;

    SkRect targetRect;
    targetRect.setLTRB((SkScalar)pUvImage->x, (SkScalar)pUvImage->y, (SkScalar)pUvImage->x + targetWidth, (SkScalar)pUvImage->y + targetHeight);

    canvas->drawBitmapRect(bitmap, bitmap.bounds(), targetRect, &paint);

    if (bDrawPoint)
    {
        uvDrawRectOutline(canvas, &targetRect);

        CUvRectF rectF;
        rectF.Set(targetRect.fLeft, targetRect.fRight, targetRect.fTop, targetRect.fBottom);
        uvDrawRectPointHandle(canvas, &rectF);
    }

    //RestoreCanvas(canvas, nullptr);


    return true;
}

bool uvDrawImage(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvImage *pUvImage = (CUvImage*)pShape;

    bool res = true;

    SaveCanvas(canvas, &pUvImage->mat);

    switch (pUvImage->imageType)
    {
    case uvImageType_file:
    case uvImageType_buffer:
        res = _uvDrawImageFile(canvas, pUvImage, nullptr, 0, pStrokeFillArgs, bDrawPoint);
        break;
    case uvImageType_wmf:
    case uvImageType_emf:
        res = _uvDrawEmf(canvas, pUvImage, pStrokeFillArgs, bDrawPoint);
        break;
    case uvImageType_strip:
    default:
        res = _uvDrawImageStrip(canvas, pUvImage, pStrokeFillArgs, bDrawPoint);
        break;
    }

    RestoreCanvas(canvas, &pUvImage->mat);

    return res;
}

SkBitmap* GenerateImageFromUvObject(CUvShape *pShape, int targetWidth, int targetHeight, SkMatrix &matrix)
{
    if (pShape == nullptr) return nullptr;

    sk_sp<SkSurface> skSurface;
    SkCanvas *canvas = nullptr;

    targetWidth = (int) (targetWidth * matrix.getScaleX() + 0.5f);
    targetHeight = (int)(targetHeight * matrix.getScaleY() + 0.5f);

    /*if (pShape->type == uvImage_ShapeType) // raster fill
    {
        CUvImage *pUvImage = (CUvImage*)pShape;

        SkBitmap srcBitmap;

        int orig = decode_file(pUvImage->href, &srcBitmap);

        SkImageInfo info = srcBitmap.info();

        skSurface = SkSurface::MakeRasterN32Premul(targetWidth, targetHeight);
        canvas = skSurface->getCanvas();

        if (ApplyImageOrigin(orig, canvas, pUvImage, srcBitmap, info.width(), info.height()) == false)
            return nullptr;

        SkRect targetRect;
        targetRect.set((SkScalar)0, (SkScalar)0, (SkScalar)targetWidth, (SkScalar)targetHeight);

        SkPaint paint;

        SkIRect rc = srcBitmap.bounds();
        canvas->drawBitmapRect(srcBitmap, rc, targetRect, &paint);
    }
    else // object fill*/
    {
        // save environment
        SkMatrix skGlobalCanvasMat = gTotalCanvasMat;
        SkCanvas *bkCanvas = gCurrentCanvas;

        skSurface = SkSurface::MakeRasterN32Premul(targetWidth, targetHeight);
        canvas = skSurface->getCanvas();

        CUvRectF uvRect;
        //pShape->GetBound(uvRect);
        uvGetBound(pShape, uvRect, nullptr);

        float scaleX = 1.0f *targetWidth / uvRect.Width();
        float scaleY = 1.0f *targetHeight / uvRect.Height();

        //canvas->concat(SkMatrix::MakeScale(scaleX * matrix.getScaleX(), scaleY * matrix.getScaleY()));
        canvas->concat(SkMatrix::Scale(scaleX , scaleY));
        canvas->concat(SkMatrix::Translate(-uvRect.left, -uvRect.top));


        uvDrawShape(canvas, pShape, nullptr, false, false);

        // restore environment
        gTotalCanvasMat = skGlobalCanvasMat;
        gCurrentCanvas = bkCanvas;
    }

    // get image
    SkBitmap *bitmap = NEW SkBitmap;
    bitmap->allocPixels(SkImageInfo::MakeN32Premul(targetWidth, targetHeight));
    if (canvas->readPixels(*bitmap, 0, 0) == false)
        return nullptr;

    //SkRect skRect;
    //skRect.set((SkScalar)20, (SkScalar)20, (SkScalar)(20 + targetWidth), (SkScalar)(20 + targetHeight));
    //SkPaint paint;
    //gCurrentCanvas->drawRect(skRect, paint);
    //gCurrentCanvas->drawBitmap(*bitmap, 20, 20);

    return bitmap;
}

