#include "stdafx.h"
#include "Compress.h"

bool uvLzwDecompress(char* src, int size, char* result, int res_size);
bool uvLz77Decompress(char* src, int size, char* result, int res_size);

bool uvDecompress(int rasterType, char* data, int size, char* result, int res_size)
{
    switch (rasterType)
    {
    case UV_RRT_LZW:
        return uvLzwDecompress(data, size, result, res_size);
    case UV_RRT_LZ77:
        return uvLz77Decompress(data, size, result, res_size);
    case UV_RRT_NONE:
    default:
        return false;
    }

    return true;
}