#include "stdafx.h"
#include "Canvas.h"
#include "SkPath.h"
#include "SkDashPathEffect.h"
#include "SkFontStyle.h"
#include "SkTypeface.h"
#include "SkCodec.h"
#include "SkData.h"

#include "CUvGroup.h"
#include "CUvPath.h"
#include "CUvDoc.h"
#include "CUvPage.h"
#include "CUvImage.h"
#include "DebugNew.h"
#include "UvmUtils.h"
#include <string>
#include "SkShader.h"
#include "SkGradientShader.h"

SkMatrix gTotalCanvasMat;
CUvPage *gCurrentPage = nullptr;
SkCanvas *gCurrentCanvas = nullptr;

bool g_bHandleDrawInited = false;
SkPaint gOutlinePaint;
SkPaint gHandleStrokePaint;
SkPaint gHandleFillPaint;

class CUvSkImage;

bool uvDrawShape(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint, bool bNotApplyClip)
{
    gCurrentCanvas = canvas;

    switch (pShape->type)
    {
    case uvGroup_ShapeType:
        uvDrawGroup(canvas, pShape, pStrokeFillArgs, bDrawPoint, bNotApplyClip);
        break;
    case uvLine_ShapeType:
        uvDrawLine(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvPointFs_ShapeType:
        uvDrawPoints(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvPoly_ShapeType:
        uvDrawPoly(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvRectF_ShapeType:
        uvDrawRect(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvOval_ShapeType:
        uvDrawOval(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvAngleArc_ShapeType:
        uvDrawOvalArc(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvCircle_ShapeType:
        uvDrawCircle(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvCircleArc_ShapeType:
        uvDrawCircleArc(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvPath_ShapeType:
        uvDrawPath(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvRefer_ShapeType:
        uvDrawRefer(canvas, pShape, pStrokeFillArgs, bDrawPoint, bNotApplyClip);
        break;
    case uvImage_ShapeType:
        uvDrawImage(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    case uvText_ShapeType:
        uvDrawText(canvas, pShape, pStrokeFillArgs, bDrawPoint);
        break;
    }

    return true;
}

bool uvDrawGroup(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint, bool bNotApplyClip)
{
    if (pShape == nullptr) return true;

    CUvGroup *pGroup = (CUvGroup*)pShape;

    // save canvas matrix and clip
    SaveCanvas(canvas, &pGroup->mat);
    //canvas->save();

    //// apply new matrix
    //SkMatrix skMatrix;
    //skMatrix.setAll(pGroup->mat.mat[0][0], pGroup->mat.mat[1][0], pGroup->mat.mat[2][0],
    //                pGroup->mat.mat[0][1], pGroup->mat.mat[1][1], pGroup->mat.mat[2][1],
    //                pGroup->mat.mat[0][2], pGroup->mat.mat[1][2], pGroup->mat.mat[2][2]);
    //canvas->concat(skMatrix);

    // apply new clip
    if (!bNotApplyClip && pGroup->left != pGroup->right && pGroup->top != pGroup->bottom)
    {
        SkRect rect;
        rect.setLTRB((SkScalar)pGroup->left, (SkScalar)pGroup->top, (SkScalar)pGroup->right, (SkScalar)pGroup->bottom);
        canvas->clipRect(rect, SkClipOp::kIntersect, true);
    }
    if (!bNotApplyClip && pGroup->pClipPath && pGroup->pClipPath->type == uvPath_ShapeType)
    {
        SkPath *skClipPath = uvCreateSkPath(pGroup->pClipPath, nullptr, nullptr);
        if (skClipPath)
        {
            canvas->clipPath(*skClipPath, SkClipOp::kIntersect, true);
            delete skClipPath;
        }
    }

    //// save total canvas matrix
    //gTotalCanvasMat = canvas->getTotalMatrix();

    // draw background
    //canvas->clear(SkColorSetARGB(pGroup->bkColor.a, pGroup->bkColor.r, pGroup->bkColor.g, pGroup->bkColor.b));

    // draw children
    if (bDrawPoint)
    {
        uvDrawRectPointHandle(canvas, pGroup);
    }
    else
    {
        for (size_t i = 0; i < pGroup->components.size(); i++)
        {
            uvDrawShape(canvas, pGroup->components[i], pStrokeFillArgs, bDrawPoint, bNotApplyClip);
        }
    }

    // restore canvas matrix and clip
    RestoreCanvas(canvas, &pGroup->mat);
    //canvas->restore();
    //gTotalCanvasMat = canvas->getTotalMatrix();

    return true;
}

bool uvDrawLine(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvLine *pLine = (CUvLine*)pShape;

    SkPaint paint;
    SetPaint(paint, pStrokeFillArgs? pStrokeFillArgs : pShape->pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), nullptr);

    if (bDrawPoint)
    {
        uvDrawLinePointHandle(canvas, pLine->point[0], pLine->point[1]);
    }
    else
        canvas->drawLine((SkScalar)pLine->point[0].x, (SkScalar)pLine->point[0].y, (SkScalar)pLine->point[1].x, (SkScalar)pLine->point[1].y, paint);

    return true;
}

bool uvDrawPoints(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvPointFsShape *pPointFs = (CUvPointFsShape*)pShape;
    if (pPointFs->points.size() == 0) return true;

    SkPaint paint;
    SetPaint(paint, pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), nullptr);

    SkPoint *points = NEW SkPoint[pPointFs->points.size()];

    for (size_t i = 0; i < pPointFs->points.size(); i++)
    {
        if (bDrawPoint)
            uvDrawPointHandle(canvas, pPointFs->points[i]);
        else
            points[i].set(pPointFs->points[i].x, pPointFs->points[i].y);
    }

    if (!bDrawPoint)
        canvas->drawPoints(SkCanvas::kPoints_PointMode, pPointFs->points.size(), points, paint);

    delete[] points;

    return true;
}

bool uvDrawPoly(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvPoly *pPoly = (CUvPoly*)pShape;
    CUvPointFs *pPointFs = pPoly->points;
    if (pPointFs->points.size() == 0) return true;

    size_t size = pPointFs->points.size();

    SkPoint *points = NEW SkPoint[size + 1];

    for (size_t i = 0; i < size; i++)
    {
        if (bDrawPoint)
            uvDrawPointHandle(canvas, pPointFs->points[i]);
        else
            points[i].set(pPointFs->points[i].x, pPointFs->points[i].y);
    }

    if (!bDrawPoint)
    {
        SkPath skPath;
        skPath.addPoly( points, (int)size, false );// pPoly->bPolygon);
        SkRect bound = skPath.getBounds();
        CUvRectF uvBound;
        uvBound.Set(bound.fLeft, bound.fRight, bound.fTop, bound.fBottom);

        CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
        UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
        if (pDrawType)
        {
            if (*pDrawType == uvStrokeAndFill_StrokeFill || *pDrawType == uvFill_StrokeFill)
            {
                UvFillMode fillMode = pStrokeFill->GetFillMode();
                if (fillMode != uvNone_FillMode) skPath.setFillType((SkPathFillType)fillMode);

                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), &uvBound);
                canvas->drawPath(skPath, paint);
            }
            if (*pDrawType == uvStroke_StrokeFill || *pDrawType == uvStrokeAndFill_StrokeFill)
            {
                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), &uvBound);
                canvas->drawPath(skPath, paint);
            }
        }
    }

    delete[] points;

    return true;
}

bool uvDrawRect(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvRectFShape *pRectFShape = (CUvRectFShape*)pShape;

    SkRect rect;
    rect.setLTRB((SkScalar)pRectFShape->left, (SkScalar)pRectFShape->top, (SkScalar)pRectFShape->right, (SkScalar)pRectFShape->bottom);

    CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
    UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
    if (pDrawType)
    {
        if (bDrawPoint)
        {
            uvDrawRectPointHandle(canvas, pRectFShape);
        }
        else
        {
            if (*pDrawType == uvStrokeAndFill_StrokeFill || *pDrawType == uvFill_StrokeFill)
            {
                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), pRectFShape);
                canvas->drawRect(rect, paint);
            }
            if (*pDrawType == uvStroke_StrokeFill || *pDrawType == uvStrokeAndFill_StrokeFill)
            {
                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), pRectFShape);
                canvas->drawRect(rect, paint);
            }
        }
    }

    return true;
}

bool uvDrawOval(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvOval *pOval = (CUvOval*)pShape;

    SkRect rect;
    rect.setLTRB((SkScalar)pOval->rect.left, (SkScalar)pOval->rect.top, (SkScalar)pOval->rect.right, (SkScalar)pOval->rect.bottom);

    CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
    UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
    if (pDrawType)
    {
        SaveCanvas(canvas, nullptr);
        canvas->rotate((SkScalar)pOval->xRotation, (SkScalar)(pOval->rect.left + pOval->rect.right) / 2, (SkScalar)(pOval->rect.top + pOval->rect.bottom) / 2);

        if (bDrawPoint)
        {
            uvDrawRectPointHandle(canvas, &pOval->rect);
        }
        else
        {
            if (*pDrawType == uvStrokeAndFill_StrokeFill || *pDrawType == uvFill_StrokeFill)
            {
                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), &pOval->rect);
                canvas->drawOval(rect, paint);
            }
            if (*pDrawType == uvStroke_StrokeFill || *pDrawType == uvStrokeAndFill_StrokeFill)
            {
                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), &pOval->rect);
                canvas->drawOval(rect, paint);
            }
        }
        RestoreCanvas(canvas, nullptr);
    }

    return true;
}

bool uvDrawOvalArc(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvAngleArc *pArc = (CUvAngleArc*)pShape;

    SkRect rect;
    rect.setLTRB((SkScalar)pArc->oval.left, (SkScalar)pArc->oval.top, (SkScalar)pArc->oval.right, (SkScalar)pArc->oval.bottom);

    SkPaint paint;

    CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
    UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
    if (pDrawType)
        SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), &pArc->oval);

    SaveCanvas(canvas, nullptr);
    canvas->rotate((SkScalar)pArc->xRotation, (SkScalar)(pArc->oval.left + pArc->oval.right) / 2, (SkScalar)(pArc->oval.top + pArc->oval.bottom) / 2);

    if (bDrawPoint)
    {
        uvDrawRectPointHandle(canvas, &pArc->oval);
    }
    else
        canvas->drawArc(rect, (SkScalar)pArc->startAngle, (SkScalar)pArc->sweepAngle, false, paint);
    RestoreCanvas(canvas, nullptr);

    return true;
}

bool uvDrawCircle(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvCircle *pCircle = (CUvCircle*)pShape;

    CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
    UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
    if (pDrawType)
    {
        CUvPointF center(pCircle->center.x, pCircle->center.y);
        CUvRectF rect;
        rect.Set(CUvPointF(pCircle->center.x - pCircle->radius, pCircle->center.y - pCircle->radius));
        rect.Add(CUvPointF(pCircle->center.x - pCircle->radius, pCircle->center.y + pCircle->radius));
        rect.Add(CUvPointF(pCircle->center.x + pCircle->radius, pCircle->center.y + pCircle->radius));
        rect.Add(CUvPointF(pCircle->center.x + pCircle->radius, pCircle->center.y - pCircle->radius));

        if (bDrawPoint)
        {
            uvDrawPointHandle(canvas, center);
            uvDrawRectPointHandle(canvas, &rect);
        }
        else
        {
            if (*pDrawType == uvStrokeAndFill_StrokeFill || *pDrawType == uvFill_StrokeFill)
            {
                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), &rect);
                canvas->drawCircle(pCircle->center.x, pCircle->center.y, pCircle->radius, paint);
            }
            if (*pDrawType == uvStroke_StrokeFill || *pDrawType == uvStrokeAndFill_StrokeFill)
            {
                SkPaint paint;
                SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), &rect);
                canvas->drawCircle(pCircle->center.x, pCircle->center.y, pCircle->radius, paint);
            }
        }
    }

    return true;
}

bool uvDrawCircleArc(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return true;

    CUvCircleArc *pArc = (CUvCircleArc*)pShape;

    SkRect rect;
    rect.setLTRB((SkScalar)pArc->left, (SkScalar)pArc->top, (SkScalar)pArc->right, (SkScalar)pArc->bottom);

    SkPaint paint;

    CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
    UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
    if (pDrawType)
        SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), pArc);

    if (bDrawPoint)
    {
        CUvPointF center((pArc->left + pArc->right) / 2, (pArc->top + pArc->bottom) / 2);
        CUvRectF rect;
        uvDrawPointHandle(canvas, center);
        uvDrawRectPointHandle(canvas, pArc);
    }
    else
        canvas->drawArc(rect, (SkScalar)pArc->startAngle, (SkScalar)pArc->sweepAngle, pArc->useCenter, paint);

    return true;
}

bool uvDrawRefer(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint, bool bNotApplyClip)
{
    if (pShape == nullptr) return false;

    CUvRefer *pRefer = (CUvRefer*)pShape;

    if (pRefer->pRefer == nullptr) return false;
    
    SaveCanvas(canvas, &pRefer->mat);
    //bool hasMatrix = !pRefer->mat.isIdentity();

    //// save canvas matrix and clip
    //if (hasMatrix)
    //{
    //    canvas->save();

    //    // apply new matrix
    //    SkMatrix skMatrix;
    //    skMatrix.setAll(pRefer->mat.mat[0][0], pRefer->mat.mat[1][0], pRefer->mat.mat[2][0],
    //        pRefer->mat.mat[0][1], pRefer->mat.mat[1][1], pRefer->mat.mat[2][1],
    //        pRefer->mat.mat[0][2], pRefer->mat.mat[1][2], pRefer->mat.mat[2][2]);
    //    canvas->concat(skMatrix);
    //}

    //// save total canvas matrix
    //gTotalCanvasMat = canvas->getTotalMatrix();

    uvDrawShape(canvas, pRefer->pRefer, pShape->pStrokeFill, bDrawPoint, bNotApplyClip);

    // restore canvas matrix and clip
    RestoreCanvas(canvas, &pRefer->mat);
    //if (hasMatrix)
    //    canvas->restore();

    //// restore total canvas matrix 
    //gTotalCanvasMat = canvas->getTotalMatrix();

    return true;
}

