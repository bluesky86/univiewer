#include "stdafx.h"
#include "Canvas.h"
#include "SkPath.h"
#include "SkDashPathEffect.h"
#include "SkPathTypes.h"

#include "CUvGroup.h"
#include "CUvPath.h"
#include "CUvDoc.h"
#include "CUvPage.h"
#include "CUvImage.h"
#include "DebugNew.h"
#include "UvmUtils.h"
#include <string>

SkPath* uvCreateSkPath(CUvPath *pPath, CUvShape *pShape, int *pointCount)
{
    SkPath *skPath = NEW SkPath();

    std::vector<CUvPathOp*> *pOps = pPath->getOps();

    bool found = false;
    int startCount = 0;

    for (size_t i = 0; found == false && i < pOps->size(); i++)
    {
        // if pShape is not empty, return points from pShape to draw outline points rather than to draw skPath
        if (pShape == (*pOps)[i]->pShape)
        {
            found = true;
            startCount = skPath->countPoints();
        }

        switch ((*pOps)[i]->verb)
        {
        case uvMoveTo_Verbs:
            skPath->moveTo((SkScalar)((CUvPointFShape*)(*pOps)[i]->pShape)->x, (SkScalar)((CUvPointFShape*)(*pOps)[i]->pShape)->y);
            break;
        case uvLineTo_Verbs:
            skPath->lineTo((SkScalar)((CUvPointFShape*)(*pOps)[i]->pShape)->x, (SkScalar)((CUvPointFShape*)(*pOps)[i]->pShape)->y);
            break;
        case uvQuadTo_Verbs:
            skPath->quadTo((SkScalar)((CUvQuad*)(*pOps)[i]->pShape)->points->points[1].x, (SkScalar)((CUvQuad*)(*pOps)[i]->pShape)->points->points[1].y,
                (SkScalar)((CUvQuad*)(*pOps)[i]->pShape)->points->points[2].x, (SkScalar)((CUvQuad*)(*pOps)[i]->pShape)->points->points[2].y);
            break;
        case uvCubicTo_Verbs:
            skPath->cubicTo((SkScalar)((CUvCubic*)(*pOps)[i]->pShape)->points->points[1].x, (SkScalar)((CUvCubic*)(*pOps)[i]->pShape)->points->points[1].y,
                (SkScalar)((CUvCubic*)(*pOps)[i]->pShape)->points->points[2].x, (SkScalar)((CUvCubic*)(*pOps)[i]->pShape)->points->points[2].y,
                (SkScalar)((CUvCubic*)(*pOps)[i]->pShape)->points->points[3].x, (SkScalar)((CUvCubic*)(*pOps)[i]->pShape)->points->points[3].y
            );
            break;
        case uvArcTo_Verbs:
            switch ((*pOps)[i]->pShape->type)
            {
            case uvAngleArc_ShapeType:
            {
                CUvAngleArc *pArc = (CUvAngleArc*)(*pOps)[i]->pShape;
                SkRect rect;
                rect.setLTRB((SkScalar)pArc->oval.left, (SkScalar)pArc->oval.top, (SkScalar)pArc->oval.right, (SkScalar)pArc->oval.bottom);
                skPath->arcTo(rect, (SkScalar)pArc->startAngle, (SkScalar)pArc->sweepAngle, false);
            }
            break;
            case uvTangentArc_ShapeType:
            {
                CUvTangentArc *pArc = (CUvTangentArc*)(*pOps)[i]->pShape;
                skPath->arcTo((SkScalar)pArc->points[1].x, (SkScalar)pArc->points[1].y, (SkScalar)pArc->points[2].x, (SkScalar)pArc->points[2].y, (SkScalar)pArc->radius);
            }
            break;
            case uvOvalArc_ShapeType:
            {
                CUvOvalArc *pArc = (CUvOvalArc*)(*pOps)[i]->pShape;
                skPath->arcTo((SkScalar)pArc->rx, (SkScalar)pArc->ry, (SkScalar)pArc->xRotate, pArc->bLargeArc ? SkPath::kLarge_ArcSize : SkPath::kSmall_ArcSize,
                    pArc->bClockwise ? SkPathDirection::kCW : SkPathDirection::kCCW, (SkScalar)pArc->x, (SkScalar)pArc->y);
            }
            break;
            }
            break;
        case uvAddPoly_Verbs:
        {
            CUvPoly *pPoly = (CUvPoly*)(*pOps)[i]->pShape;
            if (pPoly->points && pPoly->points->points.size() > 1)
            {
                if ((*pOps)[i]->bAddLineFromLastPoint)
                {
                    for (size_t i = 0; i < pPoly->points->points.size(); i++)
                        skPath->lineTo(pPoly->points->points[i].x, pPoly->points->points[i].y);
                }
                else
                {
                    SkPoint *pts = NEW SkPoint[pPoly->points->points.size()];
                    for (size_t i = 0; i < pPoly->points->points.size(); i++)
                        pts[i].set((SkScalar)pPoly->points->points[i].x, (SkScalar)pPoly->points->points[i].y);
                    skPath->addPoly(pts, (int)pPoly->points->points.size(), pPoly->bPolygon);
                }
            }
        }
        break;
        case uvAddRectCW_Verbs:
        {
            CUvRectF *p = (CUvRectF*)(*pOps)[i]->pShape;
            SkRect rect;
            rect.setLTRB((SkScalar)p->left, (SkScalar)p->top, (SkScalar)p->right, (SkScalar)p->bottom);
            skPath->addRect(rect);// , kCW Direction);
        }
        break;
        case uvAddRectCCW_Verbs:
        {
            CUvRectF *p = (CUvRectF*)(*pOps)[i]->pShape;
            SkRect rect;
            rect.setLTRB((SkScalar)p->left, (SkScalar)p->top, (SkScalar)p->right, (SkScalar)p->bottom);
            skPath->addRect(rect, SkPathDirection::kCCW);
        }
        break;
        case uvAddOvalCW_Verbs:
        {
            CUvOval *p = (CUvOval*)(*pOps)[i]->pShape;
            SkRect rect;
            rect.setLTRB((SkScalar)p->rect.left, (SkScalar)p->rect.top, (SkScalar)p->rect.right, (SkScalar)p->rect.bottom);
            skPath->addOval(rect, SkPathDirection::kCW);
        }
        break;
        case uvAddOvalCCW_Verbs:
        {
            CUvOval *p = (CUvOval*)(*pOps)[i]->pShape;
            SkRect rect;
            rect.setLTRB((SkScalar)p->rect.left, (SkScalar)p->rect.top, (SkScalar)p->rect.right, (SkScalar)p->rect.bottom);
            skPath->addOval(rect, SkPathDirection::kCCW);
        }
        break;
        case uvAddCircleCW_Verbs:
        {
            CUvCircle *p = (CUvCircle*)(*pOps)[i]->pShape;
            skPath->addCircle((SkScalar)p->center.x, (SkScalar)p->center.y, (SkScalar)p->radius, SkPathDirection::kCW);
        }
        break;
        case uvAddCircleCCW_Verbs:
        {
            CUvCircle *p = (CUvCircle*)(*pOps)[i]->pShape;
            skPath->addCircle((SkScalar)p->center.x, (SkScalar)p->center.y, (SkScalar)p->radius, SkPathDirection::kCCW);
        }
        break;
        case uvAddArc_Verbs:
        {
            CUvAngleArc *pArc = (CUvAngleArc*)(*pOps)[i]->pShape;
            SkRect rect;
            rect.setLTRB((SkScalar)pArc->oval.left, (SkScalar)pArc->oval.top, (SkScalar)pArc->oval.right, (SkScalar)pArc->oval.bottom);
            skPath->arcTo(rect, (SkScalar)pArc->startAngle, (SkScalar)pArc->sweepAngle, false);
        }
        break;
        case uvAddPath_Verbs:
        {
            CUvPath *p = (CUvPath*)(*pOps)[i]->pShape;
            SkPath* childSkPath = uvCreateSkPath(p, nullptr, nullptr);
            SkMatrix skMatrix;
            skMatrix.setAll((*pOps)[i]->matrix.mat[0][0], (*pOps)[i]->matrix.mat[1][0], (*pOps)[i]->matrix.mat[2][0],
                (*pOps)[i]->matrix.mat[0][1], (*pOps)[i]->matrix.mat[1][1], (*pOps)[i]->matrix.mat[2][1],
                (*pOps)[i]->matrix.mat[0][2], (*pOps)[i]->matrix.mat[1][2], (*pOps)[i]->matrix.mat[2][2]);
            skPath->addPath(*childSkPath, skMatrix);
            //delete skPath;
        }
        break;
        }
    }

    if (found) // return points from sub-element
    {
        int count = skPath->countPoints() - startCount;
        if (pointCount)
            *pointCount = count;

        CUvPointF *points = NEW CUvPointF[count];
        for (int i = 0; i < count; i++)
        {
            SkPoint skPoint = skPath->getPoint(startCount + i);
            points[i].x = skPoint.fX;
            points[i].y = skPoint.fY;
        }
        //delete skPath;
        return (SkPath*)points;
    }
    else if (pOps->size() > 0 && pPath->bClosed)
        skPath->close();

    return skPath;
}

bool uvDrawPath(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return false;

    SkPath *skPath = nullptr;
    skPath = uvCreateSkPath((CUvPath*)pShape, nullptr, nullptr);

    if (skPath == nullptr) return false;

    SaveCanvas(canvas, &((CUvPath*)pShape)->mat);

    if (bDrawPoint)
    {
        int ptCount = skPath->countPoints();
        if (ptCount == 0) return true;

        SkPoint *skPoints = NEW SkPoint[ptCount];
        ptCount = skPath->getPoints(skPoints, ptCount);
        CUvPointF *points = NEW CUvPointF[ptCount];
        for (int i = 0; i < ptCount; i++)
        {
            points[i].x = skPoints[i].fX;
            points[i].y = skPoints[i].fY;
        }
        delete skPoints;

        uvDrawPointsHandle(canvas, points, ptCount);
        //delete points;
    }
    else
    {
        SkPaint paint;

        CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
        UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
        if (pDrawType)
        {
            SkRect skRect = skPath->getBounds();
            CUvRectF uvRect;
            uvRect.Set(skRect.fLeft, skRect.fRight, skRect.fTop, skRect.fBottom);

            if (*pDrawType == uvStrokeAndFill_StrokeFill || *pDrawType == uvFill_StrokeFill)
            {
                UvFillMode fillMode = pStrokeFill->GetFillMode();
                if (fillMode != uvNone_FillMode) skPath->setFillType((SkPathFillType)fillMode);

                SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), &uvRect);
                canvas->drawPath(*skPath, paint);
            }
            if (*pDrawType == uvStroke_StrokeFill || *pDrawType == uvStrokeAndFill_StrokeFill)
            {
                SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), &uvRect);
                canvas->drawPath(*skPath, paint);
            }
        }

    }

    RestoreCanvas(canvas, &((CUvPath*)pShape)->mat);

    delete skPath;

    return true;
}

void uvDrawPathElementPointsHandle(SkCanvas* canvas, CUvPath *pPath, CUvShape *pShape)
{
    if (pShape == nullptr || pPath == nullptr) return;

    int count = 0;
    CUvPointF *points = (CUvPointF*) uvCreateSkPath(pPath, pShape, &count);

    if (points == nullptr) return;

    uvDrawPointsHandle(canvas, points, count);
    //delete points;
}

