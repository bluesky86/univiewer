#pragma once
//#include <string>

typedef enum _Uv_Raster_Raw_Type
{
    UV_RRT_NONE,
    UV_RRT_LZW,
    UV_RRT_LZ77,
} UV_RENDER_RASTER_TYPE;

bool uvDecompress(int rasterType, char* data, int size, char* result, int res_size);