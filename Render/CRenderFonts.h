#pragma once
#include <vector>
#include <string>
#include "SkTypeface.h"
#include "SkFontMgr.h"

class CRenderFont
{
public:
    CRenderFont();
    CRenderFont( std::string fname );
    ~CRenderFont();

    std::string fontname;

    sk_sp<SkTypeface> typeface;
};

class CRenderFonts
{
public:
    CRenderFonts();
    ~CRenderFonts();

    void clear();

    CRenderFont* find( std::string fontName );
    CRenderFont* insert( std::string fontName, const char *fontData, int fontDataLen );
    CRenderFont* insert( std::string fontName, std::string href );

public:
    std::vector<CRenderFont*> fonts;

private:
    sk_sp<SkFontMgr> freetypeFontMgr;
};

