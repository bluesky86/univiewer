#include "stdafx.h"
#include "Canvas.h"
#include "SkPath.h"
#include "SkDashPathEffect.h"
#include "SkFontStyle.h"
#include "SkTypeface.h"

#include "CUvGroup.h"
#include "CUvPath.h"
#include "CUvDoc.h"
#include "CUvPage.h"
#include "CUvImage.h"
#include "DebugNew.h"
#include "UvmUtils.h"
#include <string>
#include "SkShader.h"
#include "SkGradientShader.h"


SkScalar _uvDashPattern[2] = { 5, 3 };
SkScalar _uvDotPattern[2] = { 1, 1 };
SkScalar _uvDashDotPattern[4] = { 3, 1, 1, 1 };
SkScalar _uvDashDotDotPattern[6] = { 3, 1, 1, 1, 1, 1 };

void SetPaint(SkPaint &paint, CUvStrokeFill *pStrokeFill, UvStrokeFill op, double scale, CUvRectF *bound)
{
    //CUvStrokeFill *pStrokeFill = GetStrokeFill(pShape);

    if (pStrokeFill == nullptr) return;

    paint.setAntiAlias(true);
    if (op == uvStroke_StrokeFill || op == uvStrokeAndFill_StrokeFill)
    {
        if (op == uvStroke_StrokeFill)
            paint.setStyle(SkPaint::kStroke_Style);
        else
            paint.setStyle(SkPaint::kStrokeAndFill_Style);

        CUvColor *pColor = pStrokeFill->GetStrokeColor();

        if (pColor == nullptr)
            paint.setColor(SkColorSetARGB(255, 0, 0, 0));
        else
            paint.setColor(SkColorSetARGB(pColor->a, pColor->r, pColor->g, pColor->b));
    }
    else if (op == uvFill_StrokeFill)
    {
        paint.setStyle(SkPaint::kFill_Style);
        UvFillType fillType = pStrokeFill->GetFillType();

        CUvColor *pColor = nullptr;
        int numColors = 0;
        switch (fillType)
        {
        case uvColor_FillType:
            pColor = pStrokeFill->GetFillColor(0);
            if (pColor)
                paint.setColor(SkColorSetARGB(pColor->a, pColor->r, pColor->g, pColor->b));
            else if ((pColor = pStrokeFill->GetStrokeColor()) != nullptr)
                paint.setColor(SkColorSetARGB(pColor->a, pColor->r, pColor->g, pColor->b));
            else
                paint.setColor(SkColorSetARGB(255, 0, 0, 0));
            break;
        case uvLinearGradient_FillType:
            numColors = pStrokeFill->GetNumOfFillColors();
            if (numColors > 0)
            {
                SkPoint pts[2];
                CUvPointFs *pPoints = pStrokeFill->GetLinearGradientPoints();
                if (pPoints && pPoints->points.size() > 1) // gradient points available
                {
                    pts[0] = { pPoints->points[0].x, pPoints->points[0].y };
                    pts[1] = { pPoints->points[1].x, pPoints->points[1].y };
                }
                else if(bound)// gradient points are boundling box
                {
                    pts[0] = { bound->left, bound->top };
                    pts[1] = { bound->right, bound->bottom };
                }

                SkColor *colors = NEW SkColor[numColors];
                for (int i = 0; i < numColors; i++)
                {
                    pColor = pStrokeFill->GetFillColor(i);
                    colors[i] = SkColor(pColor->ToArgbInt());
                }
                SkScalar *colorPos = nullptr;
                CVector<float> *pUvColorPos = pStrokeFill->GetLinearGradientColorPos();
                if (pUvColorPos)
                {
                    colorPos = NEW SkScalar[pUvColorPos->size()];
                    for (U32 i = 0; i < pUvColorPos->size(); i++)
                        colorPos[i] = (*pUvColorPos)[i];
                }

                //SkPoint pts[2] = { { pPoints->points[0].x, pPoints->points[0].y }, {pPoints->points[1].x, pPoints->points[1].y } };
                UvTileMode uvTileMode = pStrokeFill->GetHoriTileMode();
                SkTileMode tileMode = (uvTileMode > 0 && uvTileMode < uvNone_TileMode) ? (SkTileMode)uvTileMode : SkTileMode::kClamp;

                paint.setShader(SkGradientShader::MakeLinear(pts, colors, colorPos, numColors, tileMode));

                delete[] colors;
                if (colorPos) delete[] colorPos;
            }
            break;
        case uvRadialGradient_FillType:
            numColors = pStrokeFill->GetNumOfFillColors();
            if (numColors > 0)
            {
                CUvPointF *pCenter = pStrokeFill->GetRadialGradientCenter();
                float * pRadius = pStrokeFill->GetRadialGradientRadius();
                if (numColors > 0 && pCenter && pRadius)
                {
                    SkColor *colors = NEW SkColor[numColors];
                    for (int i = 0; i < numColors; i++)
                    {
                        pColor = pStrokeFill->GetFillColor(i);
                        colors[i] = SkColor(pColor->ToArgbInt());
                    }
                    SkScalar *colorPos = nullptr;
                    CVector<float> *pUvColorPos = pStrokeFill->GetRadialGradientColorPos();
                    if (pUvColorPos)
                    {
                        colorPos = new SkScalar[pUvColorPos->size()];
                        for (U32 i = 0; i < pUvColorPos->size(); i++)
                            colorPos[i] = (*pUvColorPos)[i];
                    }

                    SkPoint center = { pCenter->x, pCenter->y };
                    UvTileMode uvTileMode = pStrokeFill->GetHoriTileMode();
                    SkTileMode tileMode = (uvTileMode > 0 && uvTileMode < uvNone_TileMode) ? (SkTileMode)uvTileMode : SkTileMode::kClamp;
                    paint.setShader(SkGradientShader::MakeRadial(center, *pRadius, colors, colorPos, numColors, tileMode));

                    delete[] colors;
                    if (colorPos) delete[] colorPos;
                }
            }
            break;
        case uvRaster_FillType:
        case uvObject_FillType:
            if (pStrokeFill->GetFillObj())
            {
                SkBitmap *bitmap = GenerateImageFromUvObject(pStrokeFill->GetFillObj(), *pStrokeFill->GetTargetFillObjWidth(), *pStrokeFill->GetTargetFillObjHeight(), gTotalCanvasMat);
                if (bitmap)
                {
                    paint.setShader( bitmap->makeShader( ( pStrokeFill->GetHoriTileMode() == uvNone_TileMode ? SkTileMode::kRepeat : (SkTileMode)pStrokeFill->GetHoriTileMode() )
                                                        , ( pStrokeFill->GetVertTileMode() == uvNone_TileMode ? SkTileMode::kRepeat : (SkTileMode)pStrokeFill->GetVertTileMode() )
                                                        , &SkMatrix::Scale( 1 / gTotalCanvasMat.getScaleX(), 1 / gTotalCanvasMat.getScaleY() ) ));
                    //delete bitmap;
                }
            }
            break;
        case uvNone_FillType:
            paint.setColor(SkColorSetARGB(255, 0, 0, 0));
            break;
        }
    }

    int *pValue, value;

    // strokeWidth
    pValue = pStrokeFill->GetStrokeWidth();
    value = pValue ? *pValue : 0;
    if (scale * value < 1.0)
        paint.setStrokeWidth((SkScalar)0);
    else
        paint.setStrokeWidth((SkScalar)value);
    //paint.setStrokeWidth((SkScalar)0); // @@@@@@@@@@@@@@@@@@@@@@@@@

    // mitLimit
    pValue = pStrokeFill->GetMitLimit();
    if (pValue) paint.setStrokeMiter((SkScalar)*pValue);
    else paint.setStrokeMiter((SkScalar)3);

    // strokeCap
    UvStrokeCap *pStrokeCap, strokeCap;
    pStrokeCap = pStrokeFill->GetStrokeCap();
    strokeCap = pStrokeCap ? *pStrokeCap : uvButt_StrokeCap;
    paint.setStrokeCap((SkPaint::Cap)strokeCap);

    // strokeJoin
    UvStrokeJoin *pStrokeJoin, strokeJion;
    pStrokeJoin = pStrokeFill->GetStrokeJoin();
    strokeJion = pStrokeJoin ? *pStrokeJoin : uvMiter_StrokeJoin;
    paint.setStrokeJoin((SkPaint::Join)strokeJion);


    //paint.setDither(true);

    SetPaintStrokeStyle(paint, pStrokeFill, scale);
}

void SetPaintStrokeStyle(SkPaint &paint, CUvStrokeFill *pStrokeFill, double scale/*, CUvShape *parent*/)
{
    int *pLineStyle = pStrokeFill->GetLineStyle();
    if (pLineStyle == nullptr) return;

    if (*pLineStyle != uvSolid_DashStyle)
    {
        SkScalar *pInterval = nullptr;
        int count = 0;
        SkScalar sum = 0;

        // strokeWidth
        int strokeWidth = pStrokeFill->GetStrokeWidth() ? *pStrokeFill->GetStrokeWidth() : 1;

        switch (*pLineStyle)
        {
        case uvDash_DashStyle:
            pInterval = NEW SkScalar[2];
            pInterval[0] = _uvDashPattern[0] * (SkScalar)strokeWidth;
            pInterval[1] = _uvDashPattern[1] * (SkScalar)strokeWidth;
            count = 2;
            sum = pInterval[0] + pInterval[1];
            break;
        case uvDot_DashStyle:
            pInterval = NEW SkScalar[2];
            pInterval[0] = pInterval[1] = (SkScalar)strokeWidth * _uvDotPattern[0];
            count = 2;
            sum = pInterval[0] + pInterval[1];
            break;
        case uvDashDot_DashStyle:
            pInterval = NEW SkScalar[4];
            pInterval[0] = _uvDashDotPattern[0] * (SkScalar)strokeWidth;
            pInterval[1] = _uvDashDotPattern[1] * (SkScalar)strokeWidth;
            pInterval[2] = _uvDashDotPattern[2] * (SkScalar)strokeWidth;
            pInterval[3] = _uvDashDotPattern[3] * (SkScalar)strokeWidth;
            count = 4;
            sum = (SkScalar)pInterval[0] + (SkScalar)pInterval[1] + (SkScalar)pInterval[2] + (SkScalar)pInterval[3];
            break;
        case uvDashDotDot_DashStyle:
            count = 6;
            pInterval = NEW SkScalar[6];
            pInterval[0] = _uvDashDotDotPattern[0] * (SkScalar)strokeWidth;
            pInterval[1] = _uvDashDotDotPattern[1] * (SkScalar)strokeWidth;
            pInterval[2] = _uvDashDotDotPattern[2] * (SkScalar)strokeWidth;
            pInterval[3] = _uvDashDotDotPattern[3] * (SkScalar)strokeWidth;
            pInterval[4] = _uvDashDotDotPattern[4] * (SkScalar)strokeWidth;
            pInterval[5] = _uvDashDotDotPattern[5] * (SkScalar)strokeWidth;
            for (int i = 0; i < count; i++) sum += pInterval[i];
            break;
        default: // custom
            if (*pLineStyle >= uvDocCustom_DashStyle) // doc or page level shared style
            {
                int id;
                CUvLinePatterns *pPatterns = nullptr;
                if (*pLineStyle >= uvPageCustom_DashStyle)
                {
                    id = *pLineStyle - uvPageCustom_DashStyle;
                    CUvPage* pUvPage = (CUvPage*)gCurrentPage;
                    if (pUvPage) pPatterns = &pUvPage->linePatterns;
                }
                else
                {
                    id = *pLineStyle - uvDocCustom_DashStyle;
                    CUvDoc* pUvDoc = (CUvDoc*)getUvDoc(gCurrentPage);
                    if (pUvDoc) pPatterns = &pUvDoc->linePatterns;
                }

                if (pPatterns)
                {
                    CUvLinePattern *pPattern = pPatterns->patterns[id];
                    if (pPattern)
                    {
                        count = (int)pPattern->intervals.size();
                        pInterval = NEW SkScalar[count];
                        for (int i = 0; i < count; i++)
                        {
                            pInterval[i] = (SkScalar)pPattern->intervals[i];// *strokeWidth;
                            sum += pInterval[i];
                        }
                    }
                }
            }
            else if (*pLineStyle > 0)
            {
                count = *pLineStyle;
                pInterval = NEW SkScalar[count];
                float *srcInterval = pStrokeFill->GetDashIntervals();
                for (int i = 0; i < count; i++)
                {
                    pInterval[i] = (SkScalar)srcInterval[i] * strokeWidth;
                    sum += pInterval[i];
                }
            }
            break;
        }

        if (pInterval)
        {
            paint.setPathEffect(SkDashPathEffect::Make(pInterval, count, sum));
            delete[] pInterval;

            // force dash "dot" to use default stroke cap
            paint.setStrokeCap(SkPaint::kButt_Cap);
        }

    }
}

