#pragma once
#include "SkSurface.h"
#include "SkCanvas.h"
#include "SkPath.h"

#include "CUvShape.h"
#include "CUvMatrix.h"
#include "CUvText.h"
#include "CUvPath.h"
#include "CUvPage.h"


extern SkMatrix gTotalCanvasMat;
extern CUvPage *gCurrentPage;
extern SkCanvas *gCurrentCanvas;

extern bool g_bHandleDrawInited;
extern SkPaint gOutlinePaint;
extern SkPaint gHandleStrokePaint;
extern SkPaint gHandleFillPaint;

SkPath* uvCreateSkPath(CUvPath *pPath, CUvShape *pShape, int *pointCount);
SkBitmap* GenerateImageFromUvObject(CUvShape *pShape, int targetWidth, int targetHeight, SkMatrix &matrix);

void SetPaint(SkPaint &paint, CUvStrokeFill *pStrokeFill, UvStrokeFill op, double scale, CUvRectF *bound);
void SetPaintStrokeStyle(SkPaint &paint, CUvStrokeFill *pStrokeFill, double scale/*, CUvShape *parent*/);
void SetTextStyle(SkPaint &paint, CUvText *pUvText);

void SetTextFont( SkFont &font, CUvText *pUvText );


void FlipH(SkBitmap *bitmap);
void FlipV(SkBitmap *bitmap);
void RotateCW90(SkBitmap *bitmap);
void RotateCCW90(SkBitmap *bitmap);

void SaveCanvas(SkCanvas* canvas, CUvMatrix *pMat);
void RestoreCanvas(SkCanvas* canvas, CUvMatrix *pMat);

void uvDrawPointHandle(SkCanvas* canvas, CUvPointF point);
void uvDrawPointsHandle(SkCanvas* canvas, CUvPointF *pts, int count);
void uvDrawLinePointHandle(SkCanvas* canvas, CUvPointF &pt1, CUvPointF &pt2);
void uvDrawRectPointHandle(SkCanvas* canvas, CUvRectF *rect);
void uvDrawPathElementPointsHandle(SkCanvas* canvas, CUvPath *pPath, CUvShape *pShape);
void uvDrawRectOutline(SkCanvas* canvas, SkRect *skRect);
void uvDrawRectOutline(SkCanvas* canvas, CUvRectF *rect);


bool uvDrawShape(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint, bool bNotApplyClip);

bool uvDrawGroup(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint, bool bNotApplyClip);
bool uvDrawLine(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawPoints(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawPoly(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawRect(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawOval(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawOvalArc(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawCircle(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawCircleArc(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawPath(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawRefer(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint, bool bNotApplyClip);
bool uvDrawImage(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawText(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
bool uvDrawText2(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint);
