#include "stdafx.h"
#include "CRenderFonts.h"
#include "SkFontMgr_empty.h"
#include "SkData.h"


///////////////////////////////////////////////////////////////
//  CRenderFont
///////////////////////////////////////////////////////////////
CRenderFont::CRenderFont()
    :typeface(nullptr)
{

}
CRenderFont::CRenderFont( std::string fname )
{
    fontname = fname;
}

CRenderFont::~CRenderFont()
{
    //if(typeface)
    //    typeface->unref();

}


///////////////////////////////////////////////////////////////
//  CRenderFonts
///////////////////////////////////////////////////////////////
CRenderFonts::CRenderFonts()
{
    freetypeFontMgr = SkFontMgr_New_Custom_Empty();
}

CRenderFonts::~CRenderFonts()
{
    clear();
}

void CRenderFonts::clear()
{
    for(size_t i = 0; i < fonts.size(); i++)
    {
        if(fonts[i]) delete fonts[i];
    }
    fonts.clear();

    freetypeFontMgr.reset();
}

CRenderFont* CRenderFonts::find( std::string fontName )
{
    for(size_t i = 0; i < fonts.size(); i++)
    {
        if(fonts[i] && fonts[i]->fontname == fontName)
            return fonts[i];
    }
    return nullptr;
}

CRenderFont* CRenderFonts::insert( std::string fontName, const char *fontData, int fontDataLen )
{
    CRenderFont *pFont = find( fontName );
    if(pFont) return pFont;

    pFont = new CRenderFont( fontName );

    sk_sp<SkData> skData = SkData::MakeWithoutCopy( fontData, fontDataLen );
    //sk_sp<SkData> skData = SkData::MakeFromMalloc( fontData, fontDataLen );
    pFont->typeface = freetypeFontMgr->makeFromData( skData );

    if(pFont->typeface)
    {
        fonts.push_back( pFont );
        return pFont;
    }

    delete pFont;

    return nullptr;
}

CRenderFont* CRenderFonts::insert( std::string fontName, std::string href )
{
    CRenderFont *pFont = find( fontName );
    if(pFont) return pFont;

    pFont = new CRenderFont( fontName );

    pFont->typeface = SkTypeface::MakeFromFile( href.c_str() );

    if(pFont->typeface)
    {
        fonts.push_back( pFont );
        return pFont;
    }

    delete pFont;

    return nullptr;
}
