#include "stdafx.h"
#include "Canvas.h"

#include "CUvGroup.h"
#include "CUvPath.h"
#include "CUvDoc.h"
#include "CUvPage.h"
#include "CUvImage.h"
#include "DebugNew.h"
#include "UvmUtils.h"
#include "CUvRect.h"
#include <string>
#include "Canvas.h"

extern bool decode_image_dimensions(const char* filename, int &width, int &height);

bool uvGetBound(CUvShape *pShape, CUvRectF &rect, SkCanvas *canvas)
{
    if (pShape == nullptr) return false;

    int w = 0;
    int h = 0;

    switch (pShape->type)
    {
    case uvImage_ShapeType:
        if (!decode_image_dimensions(((CUvImage*)pShape)->href, w, h))
            return false;
        rect.Set((float)((CUvImage*)pShape)->x, (float)((CUvImage*)pShape)->x + w, (float)((CUvImage*)pShape)->y, (float)((CUvImage*)pShape)->y + h);
        break;
    case uvText_ShapeType:
        break;
    default:
        pShape->GetBound(rect);
        break;
    }

    return true;
}