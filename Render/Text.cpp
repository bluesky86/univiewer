#include "stdafx.h"
#include "Canvas.h"
#include "SkPath.h"
#include "SkDashPathEffect.h"
#include "SkFontStyle.h"
#include "SkTypeface.h"
#include "SkMatrix.h"
#include "SkFontMgr.h"
#include "SkFont.h"
#include <SkFontMetrics.h>
#include "SkFontMgr_empty.h"
#include "SkTextBlob.h"

#include "DebugNew.h"
#include "UvmUtils.h"
#include <string>
#include "FontRemap.h"
#include "CRenderFonts.h"

#include "Codepage.h"

int CharCodeMapping( CUvText *pUvText, unsigned char *dest );


SkTextEncoding GetEncoding( CUvText *pUvText )
{
    if(!pUvText) return SkTextEncoding::kUTF8;

    if(pUvText->pFontFace)
    {
        CUvFont *pUvFontParent = pUvText->pFontFace->getParent();
        if(pUvFontParent && pUvFontParent->getSearchByGlyphId())
        {
            return SkTextEncoding::kGlyphID;
        }
    }

    if(pUvText->encoding == uvUTF16_TextEncoding)
        return SkTextEncoding::kUTF16;
    else if(pUvText->encoding == uvUTF32_TextEncoding)
        return SkTextEncoding::kUTF32;
    else if(pUvText->encoding == uvGlyphID_TextEncoding)
        return SkTextEncoding::kGlyphID;
    else
        return SkTextEncoding::kUTF8;
}

void SetTextFont( SkFont &font, CUvText *pUvText )
{
    if(!pUvText) return;

    font.setSize( (SkScalar)pUvText->fontSize );
    font.setScaleX( (SkScalar)pUvText->scaleX );

    if(pUvText->pFontFace)
    {
        font.setEmbolden( pUvText->pFontFace->isBold() );
        if(pUvText->pFontFace->isItalic())
            font.setSkewX( -0.25f );

        sk_sp<SkTypeface> typeface;

        CUvFont *pUvFontParent = pUvText->pFontFace->getParent();
        if(pUvFontParent && pUvFontParent->isEmbeddedFont()) // embeded font
        {
            if(pUvFontParent->getRenderFont())
            {
                typeface = ( (CRenderFont*)pUvFontParent->getRenderFont() )->typeface;

                //SkUnichar unichar = pUvText->convertedText[0];
                //SkGlyphID glyphId = typeface->unicharToGlyph( unichar );
                //if(glyphId == 0)
                //    glyphId = unichar;
                //else
                //    pUvText->convertedText[0] = glyphId;
            }
            else
            {
                char *pUrl = (char*)pUvFontParent->getHref();
                //ConvertBinaryFontToHexTextFromFile(pUrl);
                if(pUrl)
                    typeface = SkTypeface::MakeFromFile( pUrl );
            }
        }

        if(typeface == nullptr) // using system default font or mapped font
        {
            sk_sp<SkFontMgr> fontMgr;
            SkTypeface *pTypeface = fontMgr->RefDefault()->matchFamilyStyle( (const char*)pUvText->pFontFace->getFamilyName(), SkFontStyle::Normal() );
            //pTypeface = fontMgr->RefDefault()->matchFamilyStyle( "MS PGothic", SkFontStyle::Normal() );

            if(pTypeface == nullptr)
            {
                wchar_t pWchar[256];
                UV_Char2Wchar( pWchar, 256, (char*)pUvText->pFontFace->getFamilyName() );

                CUvFontRemap *pRemapFont = UV_RemapFont( pWchar );
                if(pRemapFont)
                {
                    char sRemapName[256];
                    UV_Wchar2Char( sRemapName, 256, pRemapFont->remapName );
                    pTypeface = fontMgr->RefDefault()->matchFamilyStyle( sRemapName, SkFontStyle::Normal() );
                    if(pTypeface)
                    {
                        font.setSize( (SkScalar)pUvText->fontSize * pRemapFont->scaleY );
                        font.setScaleX( (SkScalar)pRemapFont->scaleX * pUvText->scaleX );
                    }
                }
            }

            if(pTypeface)
                typeface = sk_sp<SkTypeface>( pTypeface );
            else
            {
                int32_t value = 0;
                const char *bcp47 = nullptr;
                if(pUvText->encoding == uvUTF16_TextEncoding)
                {
                    value = GetCodepageSignature( pUvText->codepage );
                    bcp47 = GetCodepageBcp47( pUvText->codepage );
                }
                else if(pUvText->encoding == uvUTF32_TextEncoding)
                    value = ( pUvText->text[0] << 24 ) | ( pUvText->text[1] << 16 ) | ( pUvText->text[2] << 8 ) | pUvText->text[3];
                else if(pUvText->encoding == uvUTF8_TextEncoding)
                    value = UV_Utf8_first_char( (char*)pUvText->text, pUvText->textLen );

                //typeface = sk_sp<SkTypeface>(fontMgr->RefDefault()->matchFamilyStyleCharacter((const char*)pUvText->pFontFace->getFamilyName(), SkFontStyle(),
                //    nullptr, 0, (SkUnichar)value ) );
                SkTypeface *tf = fontMgr->RefDefault()->matchFamilyStyleCharacter( (const char*)pUvText->pFontFace->getFamilyName(), SkFontStyle(), bcp47?&bcp47:nullptr, bcp47?1:0, (SkUnichar)value );
                typeface = sk_sp<SkTypeface>( tf );

                //typeface = sk_sp<SkTypeface>(fontMgr->RefDefault()->matchFamilyStyle("Times New Roman", SkFontStyle::Normal()) );


                // default font is "Segoe UI" pronounced as "SEE-go" in Windows
                if(!typeface)
                    typeface = SkTypeface::MakeFromName( (const char*)pUvText->pFontFace->getFamilyName(), SkFontStyle::Normal() );
            }
        }

        if(typeface)
            font.setTypeface( typeface );
    }
}

void SetTextStyle(SkPaint &paint, CUvText *pUvText)
{
    if (pUvText)
    {
        /*paint.setTextSize((SkScalar)pUvText->fontSize);
        paint.setTextScaleX((SkScalar)pUvText->scaleX);
        paint.setTextEncoding((SkPaint::TextEncoding)pUvText->encoding);
        paint.setTextAlign((SkPaint::Align)pUvText->align);

        if(pUvText->encoding == uvUTF16_TextEncoding)
            paint.setTextEncoding(SkPaint::kUTF16_TextEncoding);
        else if (pUvText->encoding == uvUTF32_TextEncoding)
            paint.setTextEncoding(SkPaint::kUTF32_TextEncoding);
        else if (pUvText->encoding == uvGlyphID_TextEncoding)
            paint.setTextEncoding(SkPaint::kGlyphID_TextEncoding);
        else
            paint.setTextEncoding(SkPaint::kUTF8_TextEncoding);

        if (pUvText->pFontFace)
        {
            paint.setFakeBoldText(pUvText->pFontFace->isBold());
            if (pUvText->pFontFace->isItalic())
                paint.setTextSkewX(-0.25f);

            sk_sp<SkTypeface> typeface;

            size_t data_len;
            CUvFont *pUvFontParent = pUvText->pFontFace->getParent();
            if (pUvFontParent) // embeded font
            {
                char *pData = (char*)pUvFontParent->getData(data_len);
                if (pData && data_len > 0)
                {
                    sk_sp<SkData> skData = SkData::MakeWithoutCopy(pData, data_len);
                    typeface = SkTypeface::MakeFromData(skData, 0);
                    //FILE *fp;
                    //char tempName[] = "C:\\Work\\Test\\tmp2\\font.cff";
                    //fopen_s( &fp, tempName, "wb" );
                    //fwrite( pData, 1, data_len, fp );
                    //fclose( fp );
                    //typeface = SkTypeface::MakeFromFile( tempName );

                    //typeface = SkTypeface::MakeFromFile( "C:\\Simon\\VC\\FontFactory\\Fonts\\base_ABCDEE+Cambria,Bold-f8e23307_1.ttf" );
                    //sk_sp < SkTypeface_File> cust_typeface = SkTypeface_File::MakeFromFile( "C:\\Work\\Test\\tmp2\\font.cff" );
                    //typeface = SkTypeface_FreeType::MakeFromFile( "C:\\Work\\Test\\tmp2\\font.cff" );

                    //FILE *fp;
                    //fopen_s( &fp, "C:\\Simon\\VC\\FontFactory\\Fonts\\base_ABCDEE+Cambria,Bold-f8e23307_1.ttf", "rb" );
                    //fseek( fp, 0, SEEK_END );
                    //int size = (int)ftell( fp );
                    //fseek( fp, 0, SEEK_SET );
                    //char *buf = new char[size];
                    //fread( buf, 1, size, fp );
                    //fclose( fp );
                    //sk_sp<SkData> skData = SkData::MakeWithoutCopy( buf, size );
                    //typeface = SkTypeface::MakeFromData(skData, 0);
                    ////delete[] buf;
                }
                else
                {
                    char *pUrl = (char*)pUvFontParent->getHref();
                    //ConvertBinaryFontToHexTextFromFile(pUrl);
                    if (pUrl)
                        typeface = SkTypeface::MakeFromFile(pUrl);
                }
            }

            if (typeface == nullptr) // using system default font or mapped font
            {
                sk_sp<SkFontMgr> fontMgr;
                SkTypeface *pTypeface = fontMgr->RefDefault()->matchFamilyStyle((const char*)pUvText->pFontFace->getFamilyName(), SkFontStyle::Normal());

                //static int aaa = 0;
                //if ( aaa++ == 0)
                //{
                //    int count = fontMgr->RefDefault()->countFamilies();
                //    SkString familyName;
                //    FILE *fp = 0;
                //    fopen_s(&fp, "f:\\test\\tmp1\\fontList.txt", "w");
                //    const char * str = nullptr;
                //    for (int i = 0; i < count; i++)
                //    {
                //        fontMgr->RefDefault()->getFamilyName(i, &familyName);
                //        str = familyName.c_str();
                //        fwrite(str, strlen(str), 1, fp);
                //        fwrite("\n", 1, 1, fp);
                //    }
                //    if (fp) fclose(fp);
                //}

                if (pTypeface == nullptr)
                {
                    wchar_t pWchar[256];
                    UV_Char2Wchar(pWchar, 256, (char*)pUvText->pFontFace->getFamilyName());

                    CUvFontRemap *pRemapFont = UV_RemapFont(pWchar);
                    if (pRemapFont)
                    {
                        char sRemapName[256];
                        UV_Wchar2Char(sRemapName, 256, pRemapFont->remapName);
                        pTypeface = fontMgr->RefDefault()->matchFamilyStyle(sRemapName, SkFontStyle::Normal());
                        if (pTypeface)
                        {
                            paint.setTextSize((SkScalar)pUvText->fontSize * pRemapFont->scaleY);
                            paint.setTextScaleX((SkScalar)pRemapFont->scaleX * pUvText->scaleX);
                        }
                    }
                }

                if(pTypeface)
                    typeface = sk_sp<SkTypeface>(pTypeface);
                else
                {
                    //typeface = sk_sp<SkTypeface>(fontMgr->RefDefault()->matchFamilyStyleCharacter((const char*)pUvText->pFontFace->getFamilyName(), SkFontStyle::Normal(),
                    //    nullptr, 0, (SkUnichar)pUvText->text[2]));


                    // default font is "Segoe UI" pronounced as "SEE-go" in Windows
                    if(!typeface)
                        typeface = SkTypeface::MakeFromName((const char*)pUvText->pFontFace->getFamilyName(), SkFontStyle::Normal());
                }
            }

            if (typeface)
                paint.setTypeface(typeface);
        }
        */
    }
}

CUvPointF GetOffsetByOrientation(SkFont &font, SkPaint &paint, CUvText *pUvText)
{
    CUvPointF ptOffset;
    if (pUvText->orientation != 0.0f)
    {
        SkFontMetrics metrics;
        font.getMetrics( &metrics );
        //SkScalar alignWidth = metrics.fDescent - metrics.fAscent + metrics.fLeading;
        SkScalar alignWidth = metrics.fXMax;// fabs(metrics.fBottom - metrics.fTop);

        CUvMatrix tmpMat;
        tmpMat.setRotation(pUvText->orientation);
        ptOffset = CUvPointF(alignWidth, 0) * tmpMat;
    }

    return ptOffset;
}

float textCalculateRect(SkPaint &paint, CUvText *pUvText, CUvRectF *rect)
{
    //SkRect skRect;
    CUvPointF pts[4];

    SkScalar advanceWidth = 0.0f;

    /*paint.setTextEncoding(SkPaint::kGlyphID_TextEncoding);
    int glyphCount = paint.countText(pUvText->text, pUvText->textLen);
    if (glyphCount > 0)
    {
        std::vector<SkGlyphID> glyphs(glyphCount);
        (void)paint.textToGlyphs(pUvText->text, pUvText->textLen, &glyphs[0]);

        advanceWidth = paint.measureText(&glyphs[0], glyphs.size() * sizeof(SkGlyphID), &skRect);
    }

    //advanceWidth = paint.measureText(pUvText->text, pUvText->textLen, &skRect);

    if (rect)
    {
        CUvPointF ptOffset = GetOffsetByOrientation(paint, pUvText);

        pts[0] = CUvPointF(0, 0);
        pts[1] = CUvPointF(skRect.width(), 0);
        pts[2] = CUvPointF(skRect.width(), -skRect.height());
        pts[3] = CUvPointF(0, -skRect.height());

        rect->Set(pts, 4);

        float xOffset = 0;
        if (pUvText->align == uvRight_TextAlign) xOffset = sqrtf((pts[0].x - pts[1].x) * (pts[0].x - pts[1].x) + (pts[0].y - pts[1].y) * (pts[0].y - pts[1].y));
        else if (pUvText->align == uvCenter_TextAlign) xOffset = sqrtf((pts[0].x - pts[1].x) * (pts[0].x - pts[1].x) + (pts[0].y - pts[1].y) * (pts[0].y - pts[1].y)) / 2;

        rect->AddX(pUvText->x - rect->left - xOffset - ptOffset.x);
        rect->AddY(pUvText->y - rect->top - rect->Height() - ptOffset.y);
    }*/

    return advanceWidth;
}

bool uvDrawText(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    if (pShape == nullptr) return false;

    CUvText *pUvText = (CUvText*)pShape;

    SkPaint paint;
    SetTextStyle(paint, pUvText);

    SkTextEncoding skEncoding = GetEncoding( pUvText );

    int text_len = 0;
    unsigned char *pText = nullptr;
    if(pUvText->convertedText)
    {
        text_len = pUvText->convertedTextLen;
        pText = pUvText->convertedText;
        //skEncoding = SkTextEncoding::kGlyphID;
    }
    else
    {
        text_len = pUvText->textLen;
        pText = pUvText->text;
    }

    SkFont font;
    SetTextFont( font, pUvText );

    CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
    UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
    if (pDrawType)
    {
        SaveCanvas(canvas, &pUvText->mat);

        CUvPointF ptOffset = GetOffsetByOrientation(font, paint, pUvText);

        if (*pDrawType == uvStrokeAndFill_StrokeFill || *pDrawType == uvFill_StrokeFill)
        {
            SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), nullptr);
            if(!bDrawPoint)
                canvas->drawSimpleText(pText, text_len, skEncoding, (SkScalar)pUvText->x - ptOffset.x, (SkScalar)pUvText->y - ptOffset.y, font, paint );
        }
        if (*pDrawType == uvStroke_StrokeFill)// || *pDrawType == uvStrokeAndFill_StrokeFill)
        {
            SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), nullptr);
            if (!bDrawPoint)
                canvas->drawSimpleText( pText, text_len, skEncoding, (SkScalar)pUvText->x - ptOffset.x, (SkScalar)pUvText->y - ptOffset.y, font, paint );
        }

        // draw underline and strikeout line
        if(bDrawPoint || ( ( pUvText->pFontFace && ( pUvText->pFontFace->isUnderline() || pUvText->pFontFace->isStrikeOut() ) ) ))
        {
            //int glyphCount = font.countText( pUvText->text, pUvText->textLen, GetEncoding( pUvText ) );
            int glyphCount = font.countText( pText, text_len, GetEncoding( pUvText ) );
            if(glyphCount > 0)
            {
                std::vector<SkGlyphID> glyphs( glyphCount );
                //(void)font.textToGlyphs( pUvText->text, pUvText->textLen, GetEncoding( pUvText ), &glyphs[0], glyphCount );
                (void)font.textToGlyphs( pText, text_len, GetEncoding( pUvText ), &glyphs[0], glyphCount );

                SkRect skRect;
                SkScalar width = font.measureText( &glyphs[0], glyphs.size() * sizeof( SkGlyphID ), GetEncoding( pUvText ), &skRect );

                SkPaint strikePaint;
                strikePaint.setAntiAlias( true );
                strikePaint.setStrokeCap( SkPaint::kRound_Cap );

                if(!bDrawPoint)
                {
                    SkColor color = paint.getColor();
                    strikePaint.setColor( color );

                    SkFontMetrics metrics;
                    font.getMetrics( &metrics );
                    SkScalar thickness = 0, position = 0;
                    if(pUvText->pFontFace->isStrikeOut() && metrics.hasStrikeoutThickness( &thickness ) && metrics.hasStrikeoutPosition( &position ))
                    {
                        strikePaint.setStrokeWidth( thickness );
                        canvas->drawLine( { pUvText->x, pUvText->y + position }, { pUvText->x + width, pUvText->y + position }, strikePaint );
                    }
                    if(pUvText->pFontFace->isUnderline() && metrics.hasUnderlineThickness( &thickness ) && metrics.hasUnderlinePosition( &position ))
                    {
                        strikePaint.setStrokeWidth( thickness );
                        canvas->drawLine( { pUvText->x, pUvText->y + position }, { pUvText->x + width, pUvText->y + position }, strikePaint );
                    }
                }

                if(bDrawPoint)
                {
                    SkScalar advanceWidth = font.measureText( &glyphs[0], glyphs.size() * sizeof( SkGlyphID ), GetEncoding( pUvText ), &skRect );
                    skRect.fLeft += pUvText->x;
                    skRect.fRight += pUvText->x;
                    skRect.fTop += pUvText->y;
                    skRect.fBottom += pUvText->y;
                    //gOutlinePaint.setStyle(SkPaint::kStroke_Style);
                    float strokeWidth = 2.0f / canvas->getTotalMatrix().getMaxScale();
                    gOutlinePaint.setStrokeWidth( (SkScalar)strokeWidth );
                    canvas->drawRect( skRect, gOutlinePaint );

                    // draw handles
                    U32 w, h;
                    float x = skRect.fLeft;
                    float y = skRect.fTop;
                    float xStep = skRect.width() / 2;
                    float yStep = skRect.height() / 2;
                    int handleFactor = 2;
                    for(h = 0; h < 3; h++, y += yStep)
                    {
                        for(w = 0, x = skRect.fLeft; w < 3; w++, x += xStep)
                        {
                            SkRect rect;
                            //if (h == 1 && w == 1) continue; // drag and drap handle (1,1)
                            rect.setLTRB( x - handleFactor * strokeWidth, y - handleFactor * strokeWidth, x + handleFactor * strokeWidth, y + handleFactor * strokeWidth );
                            canvas->drawRect( rect, gHandleFillPaint );
                            canvas->drawRect( rect, gHandleStrokePaint );
                        }
                    }
                }
            }
        }

        RestoreCanvas(canvas, &pUvText->mat);
    }

    //delete[] pText;

    return true;
}

bool uvDrawText2(SkCanvas* canvas, CUvShape *pShape, CUvStrokeFill *pStrokeFillArgs, bool bDrawPoint)
{
    /*if (pShape == nullptr) return false;

    CUvText *pUvText = (CUvText*)pShape;

    SkPaint paint;
    SetTextStyle(paint, pUvText);

    CUvStrokeFill *pStrokeFill = pStrokeFillArgs ? pStrokeFillArgs : pShape->pStrokeFill;
    UvStrokeFill *pDrawType = pStrokeFill ? pStrokeFill->GetStrokeFill() : nullptr;
    if (pDrawType)
    {
        SaveCanvas(canvas, &pUvText->mat);

        //if (bDrawPoint)
        //{
        //    CUvRectF rect;

        //    float strokeWidth = 2.0f / canvas->getTotalMatrix().getMaxScale();
        //    gOutlinePaint.setStrokeWidth((SkScalar)strokeWidth);

        //    SetPaint(paint, pUvText->pStrokeFill, uvStroke_StrokeFill, canvas->getTotalMatrix().getMaxScale(), nullptr);

        //    textCalculateRect(paint, pUvText, &rect);

        //    SkRect skRect;
        //    skRect.set(rect.left, rect.top, rect.right, rect.bottom);

        //    // draw outline
        //    canvas->drawRect(skRect, gOutlinePaint);

        //    // draw handles
        //    U32 w, h;
        //    float x = rect.left;
        //    float y = rect.top;
        //    float xStep = rect.Width() / 2;
        //    float yStep = rect.Height() / 2;
        //    int handleFactor = 2;
        //    for (h = 0; h < 3; h++, y += yStep)
        //    {
        //        for (w = 0, x = rect.left; w < 3; w++, x += xStep)
        //        {
        //            //if (h == 1 && w == 1) continue; // drag and drap handle (1,1)
        //            skRect.set(x - handleFactor * strokeWidth, y - handleFactor * strokeWidth, x + handleFactor * strokeWidth, y + handleFactor * strokeWidth);
        //            canvas->drawRect(skRect, gHandleFillPaint);
        //            canvas->drawRect(skRect, gHandleStrokePaint);
        //        }
        //    }
        //}
        //else
        {
            //SkScalar alignWidth = 0;
            //if (*pDrawType == uvStrokeAndFill_StrokeFill || *pDrawType == uvFill_StrokeFill)
            //{
                //SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), nullptr);
            //    canvas->drawString((char*)pUvText->text, (SkScalar)pUvText->x - alignWidth, (SkScalar)pUvText->y, paint);
            //}
            //if (*pDrawType == uvStroke_StrokeFill)// || *pDrawType == uvStrokeAndFill_StrokeFill)
            //{
            //    SetPaint(paint, pStrokeFill, uvStroke_StrokeFill, gTotalCanvasMat.getScaleX(), nullptr);
            //    canvas->drawString((char*)pUvText->text, (SkScalar)pUvText->x - alignWidth, (SkScalar)pUvText->y, paint);
            //}

            SetPaint(paint, pStrokeFill, uvFill_StrokeFill, gTotalCanvasMat.getScaleX(), nullptr);

            canvas->drawText(pUvText->text, pUvText->textLen, (SkScalar)pUvText->x, (SkScalar)pUvText->y + 40, paint);

            int glyphCount = paint.countText(pUvText->text, pUvText->textLen);

            SkGlyphID *glyphs = NEW SkGlyphID[glyphCount];
            paint.textToGlyphs(pUvText->text, pUvText->textLen, glyphs);

            //SkUnichar *uniChars = NEW SkUnichar[glyphCount];
            //paint.glyphsToUnichars(glyphs, glyphCount, uniChars);

            //SkPath skPath;
            ////paint.getTextPath(pUvText->text, pUvText->textLen, (SkScalar)pUvText->x, (SkScalar)pUvText->y, &skPath);
            ////canvas->drawPath(skPath, paint);
            //SkScalar xPos = pUvText->x;
            //SkString skString;
            //skString.append((char*)pUvText->text);
            //for (int i = 0; i < glyphCount; i++)
            //{
            //    SkString ch;
            //    ch.append(skString[i]);
            //    canvas->drawString(ch, xPos, (SkScalar)pUvText->y, paint);
            //    //paint.getTextPath(&uniChars[i], sizeof(SkUnichar), xPos, (SkScalar)pUvText->y, &skPath);
            //    //canvas->drawPath(skPath, paint);
            //    xPos += paint.measureText(ch.c_str(), ch.size());
            //}

            //delete[] glyphs;
            //delete[] uniChars;

            paint.setTextEncoding(SkPaint::kGlyphID_TextEncoding);

            SkScalar width = paint.measureText(glyphs, glyphCount*sizeof(SkGlyphID));

            SkScalar xPos = pUvText->x - width;
            for (int i = 0; i < glyphCount; i++)
            {
                canvas->drawText(&glyphs[i], sizeof(SkGlyphID), xPos, (SkScalar)pUvText->y, paint);

                //CUvPointF pt(paint.measureText(&glyphs[i], sizeof(SkGlyphID)), 0);
                //CUvMatrix uvMat = pUvText->mat;
                //uvMat.mat[2][0] = uvMat.mat[2][1] = 0;
                //pt = pt * uvMat;
                //xPos = sqrtf(pt.x * pt.x + pt.y * pt.y);
                xPos += paint.measureText(&glyphs[i], sizeof(SkGlyphID));
            }

            delete[] glyphs;

            //// draw underline and strikeout line
            //if (pUvText->pFontFace && (pUvText->pFontFace->isUnderline() || pUvText->pFontFace->isStrikeOut()))
            //{
                //int glyphCount = paint.countText(pUvText->text, pUvText->textLen);
                //std::vector<SkGlyphID> glyphs(glyphCount);
                //(void)paint.textToGlyphs(pUvText->text, pUvText->textLen, &glyphs[0]);
                //paint.setTextEncoding(SkPaint::kGlyphID_TextEncoding);

                //SkPaint::FontMetrics metrics;
                //paint.getFontMetrics(&metrics);
                //SkScalar width = paint.measureText(&glyphs[0], glyphs.size() * sizeof(SkGlyphID));
            //    SkPaint strikePaint;
            //    strikePaint.setAntiAlias(true);
            //    strikePaint.setStrokeCap(SkPaint::kRound_Cap);

            //    SkColor color = paint.getColor();
            //    strikePaint.setColor(color);

            //    SkScalar thickness = 0, position = 0;
            //    if (pUvText->pFontFace->isStrikeOut() && metrics.hasStrikeoutThickness(&thickness) && metrics.hasStrikeoutPosition(&position))
            //    {
            //        strikePaint.setStrokeWidth(thickness);
            //        canvas->drawLine({ pUvText->x, pUvText->y + position }, { pUvText->x + width, pUvText->y + position }, strikePaint);
            //    }
            //    if (pUvText->pFontFace->isUnderline() && metrics.hasUnderlineThickness(&thickness) && metrics.hasUnderlinePosition(&position))
            //    {
            //        strikePaint.setStrokeWidth(thickness);
            //        canvas->drawLine({ pUvText->x, pUvText->y + position }, { pUvText->x + width, pUvText->y + position }, strikePaint);
            //    }

            //}
        }

        RestoreCanvas(canvas, &pUvText->mat);
    }*/

    return true;
}

void SaveCanvas(SkCanvas* canvas, CUvMatrix *pMat)
{
    if (canvas)
    {
        // save canvas matrix and clip
        canvas->save();

        if (pMat && !pMat->isIdentity())
        {
            // apply new matrix
            SkMatrix skMatrix;
            skMatrix.setAll(pMat->mat[0][0], pMat->mat[1][0], pMat->mat[2][0],
                pMat->mat[0][1], pMat->mat[1][1], pMat->mat[2][1],
                pMat->mat[0][2], pMat->mat[1][2], pMat->mat[2][2]);
            canvas->concat(skMatrix);
            // save total canvas matrix
            gTotalCanvasMat = canvas->getTotalMatrix();
        }
    }
}
void RestoreCanvas(SkCanvas* canvas, CUvMatrix *pMat)
{
    if (canvas)
    {
        // restore canvas matrix and clip
        canvas->restore();
        if(pMat && !pMat->isIdentity())
            gTotalCanvasMat = canvas->getTotalMatrix();
    }
}

void uvDrawPointHandle(SkCanvas* canvas, CUvPointF point)
{
    float strokeWidth = 2.0f / canvas->getTotalMatrix().getMaxScale();
    gHandleStrokePaint.setStrokeWidth((SkScalar)strokeWidth);

    float handleFactor = 2;
    SkRect skRect;
    skRect.setLTRB(point.x - handleFactor * strokeWidth, point.y - handleFactor * strokeWidth, point.x + handleFactor * strokeWidth, point.y + handleFactor * strokeWidth);
    canvas->drawRect(skRect, gHandleFillPaint);
    canvas->drawRect(skRect, gHandleStrokePaint);
}

void uvDrawPointsHandle(SkCanvas* canvas, CUvPointF *pts, int count)
{
    float strokeWidth = 2.0f / canvas->getTotalMatrix().getMaxScale();
    gHandleStrokePaint.setStrokeWidth((SkScalar)strokeWidth);

    float handleFactor = 2 * strokeWidth;
    SkRect skRect;
    for (int i = 0; i < count; i++)
    {
        skRect.setLTRB(pts[i].x - handleFactor, pts[i].y - handleFactor, pts[i].x + handleFactor, pts[i].y + handleFactor);
        canvas->drawRect(skRect, gHandleFillPaint);
        canvas->drawRect(skRect, gHandleStrokePaint);
    }
}

void uvDrawLinePointHandle(SkCanvas* canvas, CUvPointF &pt1, CUvPointF &pt2)
{
    uvDrawPointHandle(canvas, pt1);
    uvDrawPointHandle(canvas, pt2);
    uvDrawPointHandle(canvas, CUvPointF((pt1.x + pt2.x) / 2, (pt1.y + pt2.y) / 2));
}

void uvDrawRectPointHandle(SkCanvas* canvas, CUvRectF *rect)
{
    uvDrawPointHandle(canvas, CUvPointF(rect->left, rect->top));
    uvDrawPointHandle(canvas, CUvPointF(rect->right, rect->top));
    uvDrawPointHandle(canvas, CUvPointF(rect->left, rect->bottom));
    uvDrawPointHandle(canvas, CUvPointF(rect->right, rect->bottom));
    uvDrawPointHandle(canvas, CUvPointF((rect->left + rect->right) / 2, rect->top));
    uvDrawPointHandle(canvas, CUvPointF((rect->left + rect->right) / 2, rect->bottom));
    uvDrawPointHandle(canvas, CUvPointF(rect->left, (rect->top + rect->bottom) / 2));
    uvDrawPointHandle(canvas, CUvPointF(rect->right, (rect->top + rect->bottom) / 2));
}

void uvDrawRectOutline(SkCanvas* canvas, SkRect *skRect)
{
    float strokeWidth = 2.0f / canvas->getTotalMatrix().getMaxScale();
    gOutlinePaint.setStrokeWidth((SkScalar)strokeWidth);
    canvas->drawRect(*skRect, gOutlinePaint);
}

void uvDrawRectOutline(SkCanvas* canvas, CUvRectF *rect)
{
    SkRect skRect;
    skRect.fLeft = rect->left;
    skRect.fRight = rect->right;
    skRect.fTop = rect->top;
    skRect.fBottom = rect->bottom;
    uvDrawRectOutline(canvas, &skRect);
}
