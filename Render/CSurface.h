#pragma once
#include "RenderHeader.h"
#include "CUvPage.h"
#include "CUvText.h"
#include "CUvMatrix.h"
#include "Vector.h"

RENDER_API bool uvRenderInit(const wchar_t* dllDir);
RENDER_API void uvRenderClose();

typedef enum _uvAlphaType {
    uvUnknown_SkAlphaType,
    uvOpaque_SkAlphaType,
    uvPremul_SkAlphaType,
    uvUnpremul_SkAlphaType,
    uvLastEnum_SkAlphaType = uvUnpremul_SkAlphaType,
} UvAlphaType;

typedef enum _uvColorType {
    uvUnknown_SkColorType,
    uvAlpha_8_SkColorType,
    uvRGB_565_SkColorType,
    uvARGB_4444_SkColorType,
    uvRGBA_8888_SkColorType,
    uvRGB_888x_SkColorType,
    uvBGRA_8888_SkColorType,
    uvRGBA_1010102_SkColorType,
    uvRGB_101010x_SkColorType,
    uvGray_8_SkColorType,
    uvRGBA_F16_SkColorType,
    uvRGBA_F32_SkColorType,
    uvLastEnum_SkColorType = uvRGBA_F32_SkColorType,
    uvN32_SkColorType = uvBGRA_8888_SkColorType, // linux
    //uvN32_SkColorType = uvRGBA_8888_SkColorType, // windows
} UvColorType;

typedef void* HSURFACE;

class RENDER_API CUvSkImage
{
public:
    CUvSkImage()
        :width(0), height(0), rowWidth(0), bytePerPixel(0), colorType(uvUnknown_SkColorType), bits(0)
    {
    };
    CUvSkImage(int _width, int _height, int _rowWidth, int _bytePerPixel, UvColorType _colorType)
        :width(_width), height(_height), rowWidth(_rowWidth), bytePerPixel(_bytePerPixel), colorType(_colorType), bits(0)
    {
    };
    ~CUvSkImage()
    {
        //if (bits)
        //    delete[] bits;
    };
public:
    int width, height, rowWidth;
    int bytePerPixel;
    UvColorType colorType;
    unsigned char *bits;
};

class RENDER_API CUvSurface
{
public:
    CUvSurface();
    //CUvSurface(HSURFACE surface);
    CUvSurface(int _width, int _height, UvColorType _colorType);
    ~CUvSurface();

    //bool MakeRasterN32Premul(int width, int height, bool reMake = false);

    CUvSkImage* DrawPage(CUvPage *pPage, CUvMatrix &mat, bool bNotApplyClip, UvColorType colorType = uvBGRA_8888_SkColorType, UvAlphaType alphaType = uvPremul_SkAlphaType);
    CUvSkImage* DrawTransparentLayer(CUvPage *pPage, CUvShape *pObj, CUvMatrix &mat, bool bDrawObj = false, bool bDrawOutline = false, UvColorType colorType = uvBGRA_8888_SkColorType, UvAlphaType alphaType = uvPremul_SkAlphaType);
    CUvSkImage* TestSurface();


    virtual bool SwapBuffer();


public:
    //HSURFACE hSurface;

    int width;
    int height;
    UvColorType colorType;

};

// Skia font manager
RENDER_API void MakeRenderFonts( CUvFonts *pSrcFonts, void **ppRenderFonts );
RENDER_API void FreeRenderFonts( void *pRenderFonts );

