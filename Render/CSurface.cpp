#include "stdafx.h"
#include "CSurface.h"
#include "SkSurface.h"
#include "SkCanvas.h"
#include "CUvRect.h"
#include "Canvas.h"
#include "DebugNew.h"
#include "SkDashPathEffect.h"
#include "FontRemap.h"
#include "CUvImage.h"
#include "CRenderFonts.h"
#include "Codepage.h"

extern SkMatrix gTotalCanvasMat;
extern CUvPage *gCurrentPage;

extern bool g_bHandleDrawInited;
extern SkPaint gOutlinePaint;
extern SkPaint gHandleStrokePaint;
extern SkPaint gHandleFillPaint;

RENDER_API bool uvRenderInit(const wchar_t* dllDir)
{
    bool res = UV_LoadFontConfig(dllDir, L"font-config.xml");
    if (res)
        UV_InitDefaultFonts();

    return res;
}

RENDER_API void uvRenderClose()
{
    UV_ClearDefaultFonts();
}

///////////////////// Help functions ///////////////////////////
SkAlphaType MapUvAlphaType2Skia(UvAlphaType uvType)
{
    switch (uvType)
    {
    case uvUnknown_SkAlphaType:
        return kUnknown_SkAlphaType;
    case uvOpaque_SkAlphaType:
        return kOpaque_SkAlphaType;
    case uvPremul_SkAlphaType:
        return kPremul_SkAlphaType;
    case uvUnpremul_SkAlphaType:
        return kUnpremul_SkAlphaType;
    default:
        return (SkAlphaType)uvType;
    }
}
SkColorType MapUvColorType2Skia(UvColorType uvType)
{
    switch (uvType)
    {
    case uvUnknown_SkColorType:
        return kUnknown_SkColorType;
    case uvAlpha_8_SkColorType:
        return kAlpha_8_SkColorType;
    case uvRGB_565_SkColorType:
        return kRGB_565_SkColorType;
    case uvARGB_4444_SkColorType:
        return kARGB_4444_SkColorType;
    case uvRGBA_8888_SkColorType:
        return kRGBA_8888_SkColorType;
    case uvRGB_888x_SkColorType:
        return kRGB_888x_SkColorType;
    case uvBGRA_8888_SkColorType:
        return kBGRA_8888_SkColorType;
    case uvRGBA_1010102_SkColorType:
        return kRGBA_1010102_SkColorType;
    case uvRGB_101010x_SkColorType:
        return kRGB_101010x_SkColorType;
    case uvGray_8_SkColorType:
        return kGray_8_SkColorType;
    case uvRGBA_F16_SkColorType:
        return kRGBA_F16_SkColorType;
    case uvRGBA_F32_SkColorType:
        return kRGBA_F32_SkColorType;
    default:
        return (SkColorType)uvType;
    }
}

////////////////////////////////////////////////////////////////
class CSurface
{
public:
    CSurface();
    CSurface(sk_sp<SkSurface> _pSurface);
    ~CSurface();

    sk_sp<SkSurface> GetSkSurface();

private:
    sk_sp<SkSurface> skSurface;
};

CSurface::CSurface()
    :skSurface(nullptr)
{
}
CSurface::CSurface(sk_sp<SkSurface> _pSurface)
    : skSurface(_pSurface)
{

}

CSurface::~CSurface()
{
}

sk_sp<SkSurface> CSurface::GetSkSurface()
{
    return skSurface;
}


///////////////////////////////////////////////////////////
CUvSurface::CUvSurface()
    //:hSurface(nullptr)
{

}
//CUvSurface::CUvSurface(HSURFACE surface)
//    : hSurface(surface)
//{
//
//}

CUvSurface::CUvSurface(int _width, int _height, UvColorType _colorType)
    :width(_width), height(_height), colorType(_colorType)
{

}

CUvSurface::~CUvSurface()
{
    //if (hSurface)
    //    delete (CSurface*)hSurface;
}

//bool CUvSurface::MakeRasterN32Premul(int width, int height, bool reMake)
//{
//    if (hSurface && reMake == false) return false; // TODO: log error
//
//    hSurface = (HSURFACE) new CSurface(sk_sp<SkSurface>(SkSurface::MakeRasterN32Premul(width, height)));
//    return true;
//}

CUvSkImage * GetImageFromCanvas(SkCanvas *skCanvas)
{
    if (skCanvas == nullptr) return nullptr;
    const SkImageInfo imageInfo = skCanvas->imageInfo();

    int minRowBytes = (int)imageInfo.minRowBytes();

    CUvSkImage *pImage = NEW CUvSkImage(imageInfo.width(), imageInfo.height(), minRowBytes, imageInfo.bytesPerPixel(), (UvColorType)imageInfo.colorType());

    pImage->bits = NEW unsigned char[minRowBytes * imageInfo.height()];

    if (skCanvas->readPixels(imageInfo, (void*)pImage->bits, minRowBytes, 0, 0))
    {
        return pImage;
    }
    else
    {
        delete pImage;
        return nullptr;
    }
}

CUvSkImage * GetImageFromSurface(sk_sp<SkSurface> skSurface)
{
    if (skSurface == nullptr) return nullptr;

    return GetImageFromCanvas(skSurface->getCanvas());
}

CUvSkImage* CUvSurface::DrawPage(CUvPage *pPage, CUvMatrix &mat, bool bNotApplyClip, UvColorType colorType /*= uvBGRA_8888_SkColorType*/, UvAlphaType alphaType /*= uvPremul_SkAlphaType*/)
{
    CUvPointF dimension(pPage->Width(), pPage->Height());
    dimension = dimension * mat;

    int canvasWidth = (int)(dimension.x + 0.5f);
    int canvasHeight = (int)(dimension.y + 0.5f);

    gTotalCanvasMat.setIdentity();

    SkImageInfo imageInfo = SkImageInfo::Make(canvasWidth, canvasHeight, MapUvColorType2Skia(colorType), MapUvAlphaType2Skia(alphaType));
    sk_sp<SkSurface> skSurface = SkSurface::MakeRaster(imageInfo);

    if(skSurface == nullptr) return nullptr;

    //sk_sp<SkSurface> skSurface = SkSurface::MakeRasterN32Premul(canvasWidth, canvasHeight);

    SkCanvas* canvas = skSurface->getCanvas();

    SkMatrix skMatrix;
    skMatrix.setAll(mat.mat[0][0], mat.mat[1][0], mat.mat[2][0], mat.mat[0][1], mat.mat[1][1], mat.mat[2][1], 0, 0, 1);// mat.mat[0][2], mat.mat[1][2], mat.mat[2][2]);
    canvas->setMatrix(skMatrix);

    gCurrentPage = pPage;

    // draw background
    canvas->clear(SkColorSetARGB(pPage->bkColor.a, pPage->bkColor.r, pPage->bkColor.g, pPage->bkColor.b));

    uvDrawGroup(canvas, pPage, nullptr, false, bNotApplyClip);

    CUvSkImage *pSkImage = GetImageFromCanvas(canvas);

    return pSkImage;
}

CUvSkImage* CUvSurface::DrawTransparentLayer(CUvPage *pPage, CUvShape *pObj, CUvMatrix &mat, bool bDrawObj, bool bDrawOutline, UvColorType colorType, UvAlphaType alphaType)
{
    CUvPointF dimension(pPage->Width(), pPage->Height());
    dimension = dimension * mat;

    int canvasWidth = (int)(dimension.x + 0.5f);
    int canvasHeight = (int)(dimension.y + 0.5f);

    gTotalCanvasMat.setIdentity();

    SkImageInfo imageInfo = SkImageInfo::Make(canvasWidth, canvasHeight, MapUvColorType2Skia(colorType), MapUvAlphaType2Skia(alphaType));
    sk_sp<SkSurface> skSurface = SkSurface::MakeRaster(imageInfo);

    if (skSurface == nullptr) return nullptr;

    SkCanvas* canvas = skSurface->getCanvas();

    SkMatrix skMatrix;
    skMatrix.setAll(mat.mat[0][0], mat.mat[1][0], mat.mat[2][0], mat.mat[0][1], mat.mat[1][1], mat.mat[2][1], 0, 0, 1);
    canvas->setMatrix(skMatrix);

    gCurrentPage = pPage;

    // draw background
    canvas->clear(SkColorSetARGB(0, 255, 255, 255));

    if (bDrawObj || bDrawOutline)
    {
        if (bDrawOutline && g_bHandleDrawInited == false)
        {
            gOutlinePaint.setStyle(SkPaint::kStroke_Style);
            gOutlinePaint.setStrokeWidth((SkScalar)0);
            gOutlinePaint.setColor(SkColorSetARGB(255, 0, 0, 255));
            SkScalar intervals[2];
            intervals[0] = 5; intervals[1] = 5;
            gOutlinePaint.setPathEffect(SkDashPathEffect::Make(intervals, 2, 10));
            gOutlinePaint.setStrokeCap(SkPaint::kButt_Cap);

            gHandleStrokePaint.setStyle(SkPaint::kStroke_Style);
            gHandleStrokePaint.setColor(SkColorSetARGB(255, 0, 0, 255));
            gHandleStrokePaint.setStrokeWidth((SkScalar)0);// strokeWidth);
            gHandleStrokePaint.setStrokeCap(SkPaint::kButt_Cap);
            gHandleStrokePaint.setStrokeJoin(SkPaint::kRound_Join);

            gHandleFillPaint.setStyle(SkPaint::kFill_Style);
            gHandleFillPaint.setColor(SkColorSetARGB(255, 255, 0, 0));

            g_bHandleDrawInited = true;
        }

        // apply matrix
        CUvMatrix mat;
        CUvShape *pRoot = pObj;
        while (pRoot && pRoot->parent)// && pRoot->parent->type != uvPage_ShapeType)
        {
            switch (pRoot->parent->type)
            {
            case uvPage_ShapeType:
            case uvGroup_ShapeType:
                mat = ((CUvGroup*)pRoot->parent)->mat * mat;
                break;
            case uvRefer_ShapeType:
                mat = ((CUvRefer*)pRoot->parent)->mat * mat;
                break;
            case uvText_ShapeType:
                mat =((CUvText*)pRoot->parent)->mat * mat;
                break;
            //case uvImage_ShapeType:
            //    mat =((CUvImage*)pRoot->parent)->mat * mat;
            //    break;
            case uvPath_ShapeType:
                {
                    CUvPath *pPath = (CUvPath*)pRoot->parent;
                    mat = pPath->mat;
                    std::vector<CUvPathOp*> *pOps = pPath->getOps();
                    for (size_t j = 0; j < pOps->size(); j++)
                    {
                        if ((*pOps)[j]->pShape == pRoot)
                        {
                            mat = (*pOps)[j]->matrix * mat;
                            break;
                        }
                    }
                }
                break;
            }
            pRoot = pRoot->parent;
        }
        SaveCanvas(canvas, &mat);


        if(bDrawObj && (pObj->type == uvPath_ShapeType || pObj->parent == nullptr || pObj->parent->type != uvPath_ShapeType))
            uvDrawShape(canvas, pObj, nullptr, false, false);
        if (bDrawOutline)
        {
            if (pObj->type != uvPath_ShapeType && (pObj->parent != nullptr && pObj->parent->type == uvPath_ShapeType)) // sub-element of path
                uvDrawPathElementPointsHandle(canvas, (CUvPath*)pObj->parent, pObj);
            else
                uvDrawShape(canvas, pObj, nullptr, true, false);
        }

        // restore matrix
        RestoreCanvas(canvas, &mat);
    }


    CUvSkImage *pSkImage = GetImageFromCanvas(canvas);

    return pSkImage;
}

bool CUvSurface::SwapBuffer()
{
    return true;
}

CUvSkImage* CUvSurface::TestSurface()
{
    //SkImageInfo imageInfo = SkImageInfo::Make(width, height, kRGBA_8888_SkColorType, kOpaque_SkAlphaType);
    sk_sp<SkSurface> skSurface = SkSurface::MakeRasterN32Premul(width, height);
    //sk_sp<SkSurface> skSurface = SkSurface::MakeRaster(imageInfo);
    SkCanvas* canvas = skSurface->getCanvas();
    canvas->clear(SK_ColorBLUE);

    SkPaint paint;
    paint.setColor(0xFFFFFF00);
    paint.setStrokeWidth(20);
    canvas->skew(1, 0);
    canvas->drawLine(32, 96, 32, 160, paint);
    canvas->skew(-2, 0);
    canvas->drawLine(288, 96, 288, 160, paint);


    return GetImageFromCanvas(canvas);
}


RENDER_API void MakeRenderFonts( CUvFonts *pSrcFonts, void **ppRenderFonts )
{
    if(!ppRenderFonts || !pSrcFonts) return;

    if(*ppRenderFonts) FreeRenderFonts( *ppRenderFonts );

    CRenderFonts *pFonts = new CRenderFonts();
    *ppRenderFonts = pFonts;

    std::vector<CUvFont*>* uvFonts = pSrcFonts->getFonts();
    size_t len = 0;
    char *data;
    CRenderFont *pFont = nullptr;
    for(size_t i = 0; i < pSrcFonts->size(); i++)
    {
        data = (char*)((*uvFonts )[i]->getData(len));
        char *fname = (char*)( ( *uvFonts )[i]->getFamilyName() );
        if(fname)
        {
            if(data && len > 0)
                pFont = pFonts->insert( fname, data, (int)len );
            else if(( *uvFonts )[i]->getHref())
                pFont = pFonts->insert( fname, (char*)( *uvFonts )[i]->getHref() );

            if(pFont)
                ( *uvFonts )[i]->setRenderFont( pFont );
        }
    }
}

RENDER_API void FreeRenderFonts( void *pRenderFonts )
{
    if(!pRenderFonts) return;

    CRenderFonts *pFonts = (CRenderFonts*)pRenderFonts;

    delete pFonts;
}

